package cn.gov.xnc.admin.freight.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightDetailsTerritoryEntity;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;

/**   
 * @Title: Controller
 * @Description: 运费信息
 * @author zero
 * @date 2017-02-03 10:14:35
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/freightDetailsController")
public class FreightDetailsController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(FreightDetailsController.class);

	@Autowired
	private FreightDetailsServiceI freightDetailsService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 运费信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView freightDetails(HttpServletRequest request) {
		return new ModelAndView("admin/abc/freightDetailsList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(FreightDetailsEntity freightDetails,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(FreightDetailsEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, freightDetails, request.getParameterMap());
		this.freightDetailsService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除运费信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(FreightDetailsEntity freightDetails, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		freightDetails = systemService.getEntity(FreightDetailsEntity.class, freightDetails.getId());
		try {
			
			List<FreightDetailsTerritoryEntity> freightDetailsTerritoryOlds =  freightDetails.getFreightDetailsTerritoryList();
			
			if(freightDetailsTerritoryOlds.size() > 0){
				
				for (FreightDetailsTerritoryEntity freightDetailsTerritoryEntity : freightDetailsTerritoryOlds) {
					freightDetailsTerritoryEntity.setFreighdetailsid(null);
				}
				
				systemService.batchUpdate(freightDetailsTerritoryOlds);
				systemService.deleteAllEntitie(freightDetailsTerritoryOlds);
				freightDetails.setFreightDetailsTerritoryList(null);
			}
			//freightDetails.setFreightid(null);
			message = "运费信息删除成功";
			j.setSuccess(true);
			freightDetailsService.delete(freightDetails);
			
		} catch (Exception e) {
			message = e.toString();
			j.setSuccess(false);
		}
		
		
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加运费信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(FreightDetailsEntity freightDetails, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(freightDetails.getId())) {
			message = "运费信息更新成功";
			FreightDetailsEntity t = freightDetailsService.get(FreightDetailsEntity.class, freightDetails.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(freightDetails, t);
				freightDetailsService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "运费信息更新失败";
			}
		} else {
			message = "运费信息添加成功";
			freightDetailsService.save(freightDetails);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 运费信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(FreightDetailsEntity freightDetails, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(freightDetails.getId())) {
			freightDetails = freightDetailsService.getEntity(FreightDetailsEntity.class, freightDetails.getId());
			req.setAttribute("freightDetailsPage", freightDetails);
		}
		return new ModelAndView("admin/abc/freightDetails");
	}
}
