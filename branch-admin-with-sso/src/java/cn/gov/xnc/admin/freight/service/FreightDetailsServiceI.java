package cn.gov.xnc.admin.freight.service;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface FreightDetailsServiceI extends CommonService{
	
	/**
	 * 构建区域运费详细数据
	 * @param freightEntity
	 * @param req
	 * @return
	 */
	public List<FreightDetailsEntity> buildFreightDetail(final FreightEntity freightEntity, HttpServletRequest req);
	
	/**
	 * 获取区域运费详细数据json
	 * @param freightDetailsList
	 * @param reg
	 * @return
	 * @throws Exception
	 */
	public String freightDetail2jsonStr(List<FreightDetailsEntity> freightDetailsList, HttpServletRequest reg) throws Exception;
	
	
	public void batchUpdatefreightDetails(final FreightEntity freightEntity, HttpServletRequest req) throws Exception;
	/**
	 * 计算订购商品运费
	 * @param product 订购商品
	 * @param provinceid 发送地址
	 * @param orderNUm 订购件数
	 * @return
	 * @throws Exception
	 */
	public 	BigDecimal getProductFreightByProvinceId(ProductEntity product, String provinceid, int orderNUm) throws Exception;
	
	/**
	 * 计算订购商品运费
	 * @param product 产品
	 * @param fullAddr 地址全称
	 * @param orderNUm 订购数量
	 * @return 运费
	 * @throws Exception
	 */
	public 	BigDecimal getProductFreightByFullAddr(ProductEntity product, String fullAddr, int orderNUm) throws Exception;
}
