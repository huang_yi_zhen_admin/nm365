package cn.gov.xnc.admin.freight.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;

/**   
 * @Title: Entity
 * @Description: 地区运费信息
 * @author zero
 * @date 2017-02-03 10:15:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_freight_details_territory", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class FreightDetailsTerritoryEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**运费详情id*/
	private FreightDetailsEntity freighdetailsid;
	/**省份id*/
	private java.lang.String territoryid;
	/**省份名称*/
	private java.lang.String territoryname;
	/**所属区域*/
	private java.lang.String district;
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  运费详情id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "freighdetailsid")
	public FreightDetailsEntity getFreighdetailsid(){
		return this.freighdetailsid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  运费详情id
	 */
	public void setFreighdetailsid(FreightDetailsEntity freighdetailsid){
		this.freighdetailsid = freighdetailsid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  省份id
	 */
	@Column(name ="TERRITORYID",nullable=true,length=32)
	public java.lang.String getTerritoryid(){
		return this.territoryid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  省份id
	 */
	public void setTerritoryid(java.lang.String territoryid){
		this.territoryid = territoryid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  省份名称
	 */
	@Column(name ="TERRITORYNAME",nullable=true,length=200)
	public java.lang.String getTerritoryname(){
		return this.territoryname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  省份名称
	 */
	public void setTerritoryname(java.lang.String territoryname){
		this.territoryname = territoryname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属区域
	 */
	@Column(name ="DISTRICT",nullable=true,length=200)
	public java.lang.String getdistrict(){
		return this.district;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属区域
	 */
	public void setdistrict(java.lang.String district){
		this.district = district;
	}
}
