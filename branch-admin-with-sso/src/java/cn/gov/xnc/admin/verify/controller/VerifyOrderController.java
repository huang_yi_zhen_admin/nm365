package cn.gov.xnc.admin.verify.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.verify.entity.VerifyOrderLogEntity;
import cn.gov.xnc.admin.verify.service.VerifyOrderService;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

/**
 * 订单核单
 * 
 * @author Leiante
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/verifyOrderController")
public class VerifyOrderController extends BaseController {
	private static final Logger logger = Logger.getLogger(VerifyOrderController.class);
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private VerifyOrderService verifyOrderService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private DictionaryService dictionaryService;
	
	private Set<String> payBillsOrderIds;
	
	private static Integer ERROR_COUNT = 0;
	/**
	 * 扫码核单界面
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "toScanVerifyList")
	public ModelAndView toScanVerifyList(HttpServletRequest request) {
		return new ModelAndView("admin/verify/scanVerifyList");
	}
	
	/**
	 * 手动核单界面
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "toManualVerifyList")
	public ModelAndView toManualVerifyList(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("admin/verify/manualVerifyList");
		
		String orderIds = ResourceUtil.getParameter("ids");
		if(!StringUtil.isNotEmpty(orderIds)){
			return mav;
		}
		
		List<String> orderIdList = StringUtil.splitToList(",", orderIds);
		if(!ListUtil.isNotEmpty(orderIdList)){
			return mav;
		}
		
		List<OrderEntity> orderList = new ArrayList<OrderEntity>();
		
		Integer logisticNumberError = 0;
		Integer logisticInfoError = 0;
		Integer freightStateError = 0;
		try {
			for (String orderId : orderIdList) {
				
				OrderEntity orderEntity = systemService.findUniqueByProperty(OrderEntity.class, "id", orderId);
				if(orderEntity == null){
					continue;
				}
				
				String logisticNumber = orderEntity.getLogisticsnumber();
				if(!StringUtil.isNotEmpty(logisticNumber)){
					//物流单号为空
					logisticNumberError++;
					continue;
				}
				
				String logisticCompany = orderEntity.getLogisticscompany();
				if(!StringUtil.isNotEmpty(logisticCompany)){
					//物流信息不完整
					logisticInfoError++;
					continue;
				}
				
				//物流状态
				TSDictionary freightState = orderEntity.getFreightState();
				
				if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_DELIVER.getValue())){
					//已发货物流状态，不能再次进行核单操作
					freightStateError++;
					continue;
				}
				
				if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_SIGN.getValue())){
					//已签收物流状态，不能再次进行核单操作
					freightStateError++;
					continue;
				}
				
				if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.PART_DELIVER.getValue())){
					//部分发货
					orderList.add(orderEntity);
				}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_PACK.getValue())){
					//待打包
					orderList.add(orderEntity);
				}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_DELIVER.getValue())){
					//待发货
					orderList.add(orderEntity);
				}
			}
			
			StringBuffer message = new StringBuffer();
			
			if(logisticNumberError != 0){
				message.append("提示：有").append(logisticNumberError).append("个订单的物流单号为空~");
			}else if(logisticInfoError != 0){
				message.append("提示：有").append(logisticInfoError).append("个订单的物流信息是不完整~");
			}else if(freightStateError != 0){
				message.append("提示：").append("物流状态为").append("已发货").append("和").append("已签收").append("的订单不能核单~");
			}
			
			if(StringUtil.isNotEmpty(message.toString())){
				//问题提示
				request.setAttribute("messageTip", message.toString());
			}
			
			//物流单号
			request.setAttribute("orderList", orderList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 扫码核单 - 搜索订单列表
	 * @param order
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "scanVerifyQuery")
	@ResponseBody
	public AjaxJson scanVerifyQuery(OrderEntity orderEntity,HttpServletRequest request, HttpServletResponse response) {
		StringBuffer message = new StringBuffer();
		AjaxJson result = new AjaxJson();
		boolean auth = false;
		try{
			//物流单号
			String logisticsNumber = orderEntity.getLogisticsnumber();
			if(!StringUtil.isNotEmpty(logisticsNumber)){
				message.append("请扫码录入物流单号");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
			
			//检查"核销单记录表"是否核销过物流单号
			VerifyOrderLogEntity verifyOrderLogEntity = new VerifyOrderLogEntity();
			verifyOrderLogEntity.setLogisticsId(logisticsNumber);
			boolean checkVerify = verifyOrderService.checkVerifyOrderLog(verifyOrderLogEntity);
			if(checkVerify){
				message.append("该物流单号").append(logisticsNumber).append("已核销");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
				
			//根据物流单号查找订单详情
			Map<String, Object> params = orderService.getParams(orderEntity);
			List<OrderEntity> list = orderService.findListByParams(params);
			if(!ListUtil.isNotEmpty(list)){
				message.append("该物流单号").append(logisticsNumber).append("查找订单没有记录");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
			
			JSONArray jsonArray = new JSONArray();
			for (OrderEntity order : list) {
				if(order.getId() == null){
					continue;
				}
				if(order.getIdentifieror() == null){
					continue;
				}
				if(order.getProductid() == null){
					continue;
				}
				if(order.getLogisticsnumber() == null){
					continue;
				}
				if(order.getLogisticscompany() == null){
					continue;
				}
				if(order.getFreightState() == null){
					continue;
				}
				if(order.getNumber() == null){
					continue;
				}
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", order.getId());
				jsonObject.put("identifieror", order.getIdentifieror());
				jsonObject.put("productName", order.getProductid().getName());
				jsonObject.put("logisticsnumber", order.getLogisticsnumber());
				jsonObject.put("logisticscompany", order.getLogisticscompany());
				jsonObject.put("freightState_id", order.getFreightState().getId());
				jsonObject.put("freightState_dictionaryName", order.getFreightState().getDictionaryName());
				jsonObject.put("number", order.getNumber());
				jsonArray.put(jsonObject);
			}
			
			auth = true;
			result.setObj(jsonArray.toString());
			result.setSuccess(auth);
			
		}catch(Exception e){
			e.printStackTrace();
			message.append("扫码核单，搜索功能，出现系统异常，请联系管理");
			result.setMsg(message.toString());
			result.setSuccess(false);
			return result;
		}
		
		return result;
	}
	
	/**
	 * 核单 - 提交
	 * @param companyStockOrder
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "submit")
	@ResponseBody
	public AjaxJson submit(HttpServletRequest request) {
		payBillsOrderIds = new HashSet<String>();
		AjaxJson result = new AjaxJson();
		Integer errorCount = 0;
		Integer successCount = 0;
		try{
			//构建核单参数
			result = this.buildVerifyOrderParams(request);
			if(!result.isSuccess()){
				result.setSuccess(false);
				result.setMsg("订单信息不完整，请核实信息再操作");
				return result;
			}
			
			List<Map<String,Object>> params = (List<Map<String, Object>>) result.getObj();
			if(params == null || params.size() <= 0){
				result.setSuccess(false);
				result.setMsg("订单信息不完整，请核实信息再操作");
				return result;
			}
			
			//提交核单处理
			result = verifyOrderService.commitVerifyOrder(params);
			if(!result.isSuccess()){
				return result;
			}
			
			Map<String, Object> problem = (Map<String, Object>) result.getObj();
			errorCount = (Integer) problem.get("errorCount");
			successCount = (Integer) problem.get("successCount");
			
			//更新支付订单物流状态
			result = updatePayBillsOrderState(request);
			if(!result.isSuccess()){
				return result;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg("核单提交失败，系统出现异常，请联系管理员");
			systemService.opLog(request, OperationLogUtil.VERIFY_ORDER_LOG_COMMIT, null, "核单提交失败，系统出现异常，请联系管理员" + e);
			return result;
		}
		
		result.setSuccess(true);
		result.setMsg("核单成功" + successCount + "个，失败" + errorCount + "个");
		return result;
	}
	
	/**
	 * 更新支付订单物流状态
	 * @return
	 */
	private AjaxJson updatePayBillsOrderState(HttpServletRequest req){
		AjaxJson result = new AjaxJson();
		try{
			//更新支付订单物流状态
			for (Iterator<String> it = payBillsOrderIds.iterator(); it.hasNext();) {
				String payBillsOrderId = it.next().toString();
				if(!StringUtil.isNotEmpty(payBillsOrderId)){
					ERROR_COUNT++;
					continue;
				}
				
				PayBillsOrderEntity  payBillsOrderEntity = payBillsOrderService.findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrderId);
				if(payBillsOrderEntity == null){
					ERROR_COUNT++;
					String message = "提交核销操作后，更新支付订单物流状态失败，支付订单号" + payBillsOrderId;
					systemService.opLog(req, OperationLogUtil.VERIFY_ORDER_LOG_COMMIT, payBillsOrderEntity, message);
					continue;
				}
				//更新支付订单物流状态
				TSDictionary freightState = payBillsOrderService.getPayBillsFreightStatus(payBillsOrderEntity);
				payBillsOrderEntity.setFreightState(freightState);
				payBillsOrderService.updateEntitie(payBillsOrderEntity);
			}
			
		}catch(Exception e){
			result.setSuccess(false);
			result.setMsg("提交核单，更新支付订单物流状态，系统异常，请联系管理员");
			systemService.opLog(req, OperationLogUtil.VERIFY_ORDER_LOG_COMMIT, null, "提交核单，更新支付订单物流状态，系统异常，请联系管理员" + e);
			return result;
		}
		result.setSuccess(true);
		return result;
	}
	
	/**
	 * 构建核单信息
	 * @param request
	 * @return
	 */
	private AjaxJson buildVerifyOrderParams(HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		List<Map<String, Object>> lists = null;
		try {
			lists = new ArrayList<Map<String,Object>>();
			//核单数量
			Integer rowDataSize = ConvertTool.toInt(ResourceUtil.getParameter("rowDataSize"));
			
			for (int i = 0; i < rowDataSize; i++) {
				//订单编号
				String id = request.getParameter("id-" + i);
				OrderEntity order = systemService.findUniqueByProperty(OrderEntity.class, "id",id);
				if(order == null){
					ERROR_COUNT++;
					String message = "该订单号" + id + "查询订单信息为空";
					systemService.opLog(request, OperationLogUtil.VERIFY_ORDER_LOG_COMMIT, order, message);
					continue;
				}
				
				PayBillsOrderEntity payBillsOrderEntity = order.getBillsorderid();
				if(payBillsOrderEntity == null){
					ERROR_COUNT++;
					String message = "该订单编号" + order.getIdentifieror() + "订单号" + order.getId() + "相关联的支付订单为空";
					systemService.opLog(request, OperationLogUtil.VERIFY_ORDER_LOG_COMMIT, payBillsOrderEntity, message);
					continue;
				}
				payBillsOrderIds.add(payBillsOrderEntity.getId());
				
				//物流单号
				String logisticsNumber = request.getParameter("logisticsnumber-" + i);
				if(!StringUtil.isNotEmpty(logisticsNumber)){
					ERROR_COUNT++;
					continue;
				}
				
				//多个物流单号
				List<String> logisticsNumbers = StringUtil.splitToList(",", logisticsNumber);
				if(!ListUtil.isNotEmpty(logisticsNumbers)){
					ERROR_COUNT++;
					continue;
				}
				
				//订单号
				String identifieror = request.getParameter("identifieror-" + i);
				if(!StringUtil.isNotEmpty(identifieror)){
					ERROR_COUNT++;
					continue;
				}
				
				for (String single : logisticsNumbers) {
					//物流单号
					if(!StringUtil.isNotEmpty(single)){
						ERROR_COUNT++;
						continue;
					}
					
					//检查"核销单记录表"是否核销过物流单号
					VerifyOrderLogEntity verifyOrderLogEntity = new VerifyOrderLogEntity();
					verifyOrderLogEntity.setLogisticsId(single);
					boolean skip = verifyOrderService.checkVerifyOrderLog(verifyOrderLogEntity);
					if(skip){
						ERROR_COUNT++;
						String message = "该物流单号" + single + "已核销";
						systemService.opLog(request, OperationLogUtil.VERIFY_ORDER_LOG_COMMIT, verifyOrderLogEntity, message);
						continue;
					}
					
					Map<String, Object> params = new HashMap<String,Object>();
					params.put("orderId", id);
					params.put("identifieror", identifieror);
					params.put("logisticsId", single);
					
					lists.add(params);
				}
			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("核单提交，构建核单信息信息有误,请联系管理员");
			systemService.opLog(request, OperationLogUtil.VERIFY_ORDER_LOG_COMMIT, null, "核单提交，构建核单信息信息有误,请联系管理员" + e);
			return result;
		}
		
		result.setSuccess(true);
		result.setObj(lists);
		return result;
	}
}
