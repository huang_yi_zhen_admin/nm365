package cn.gov.xnc.admin.verify.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Entity
@Table(name = "xnc_verify_order_log", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class VerifyOrderLogEntity implements Serializable{

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	private String id;
	//公司编码
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	private TSCompany company;
	 //订单
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderId")
	private OrderEntity order;
	//物流单号	
	@Column(name ="logisticsId",nullable=true,length=255)
	private String logisticsId;
	//核销人
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "operateUser")
	private TSUser operateUser;
	//日期
	@Column(name ="createTime",nullable=true)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
	
	public VerifyOrderLogEntity() {
	}
	
	public VerifyOrderLogEntity(String id, TSCompany company,
			OrderEntity order, String logisticsId, TSUser operateUser,
			Date createTime) {
		this.id = id;
		this.company = company;
		this.order = order;
		this.logisticsId = logisticsId;
		this.operateUser = operateUser;
		this.createTime = createTime;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 物流单号
	 * @return
	 */
	public String getLogisticsId() {
		return logisticsId;
	}
	/**
	 * 物流单号
	 * @param logisticsId
	 */
	public void setLogisticsId(String logisticsId) {
		this.logisticsId = logisticsId;
	}
	/**
	 * 操作人/核销人
	 * @return
	 */
	public TSUser getOperateUser() {
		return operateUser;
	}
	/**
	 * 操作人/核销人
	 * @param logisticsId
	 */
	public void setOperateUser(TSUser operateUser) {
		this.operateUser = operateUser;
	}
	/**
	 * 创建时间
	 * @return
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 创建时间
	 * @param logisticsId
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public TSCompany getCompany() {
		return company;
	}

	public void setCompany(TSCompany company) {
		this.company = company;
	}
	/**
	 * 订单
	 */
	public OrderEntity getOrder() {
		return order;
	}
	/**
	 * 订单
	 */
	public void setOrder(OrderEntity order) {
		this.order = order;
	}
	
}
