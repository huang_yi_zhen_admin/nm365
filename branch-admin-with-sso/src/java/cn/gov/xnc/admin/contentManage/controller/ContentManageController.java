package cn.gov.xnc.admin.contentManage.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.contentManage.entity.ArticleEntity;
import cn.gov.xnc.admin.contentManage.entity.SectionEntity;
import cn.gov.xnc.admin.contentManage.service.impl.ContentManageServiceImpl;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DisplayMsgUtil;
import cn.gov.xnc.system.core.util.ParamUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;


@Controller
@RequestMapping("/contentManageController")
public class ContentManageController {
	@Resource(name="contentManageService")
	private ContentManageServiceImpl contentManageService;
	
	/**
	 * 栏目管理
	 * */
	@RequestMapping(value = "sectionManage")
	public ModelAndView sectionManage(HttpServletRequest request, HttpServletResponse response,  ModelMap modelMap){
		String id = ParamUtil.getParameter(request, "id", "");
		SectionEntity section = new SectionEntity();
		if(!id.equals("")){
			section.setId(id);
			section = contentManageService.getSectionSingleData(section);
		}
		modelMap.put("section", section);
		return new ModelAndView("admin/contentManage/sectionManage");
	}
	
	/**
	 * 列表
	 * */
	@RequestMapping(value = "sectionList")
	public ModelAndView sectionList(HttpServletRequest request, HttpServletResponse response){
		return new ModelAndView("admin/contentManage/sectionList");
	}
	
	
	@RequestMapping(value = "getSectionListPage")
	@ResponseBody 
	public void getSectionListPage(HttpServletRequest request, HttpServletResponse response){
		DataGrid dataGrid = new DataGrid();
		SectionEntity section = new SectionEntity();
		String[] ids;
		String field = ParamUtil.getParameter(request, "field", "");
		if(field.indexOf(",")>=0){
			ids = field.split(",");
		}else{
			ids = ParamUtil.getArrayParameter(request, "field");
		}
		if(!ParamUtil.getParameter(request, "section_name","").equals("")){
			section.setSection_name(ParamUtil.getParameter(request, "section_name", null));
		}
		
		dataGrid.setPageNum(ParamUtil.getIntParameter(request, "pageNum", 1));
		dataGrid.setShowNum(ParamUtil.getIntParameter(request, "showNum", 10));
		dataGrid = contentManageService.getSectionListPage(dataGrid, section);
		DisplayMsgUtil.printMsg(response, DataConstruction(ids, dataGrid));
	}
	
	@RequestMapping(value = "sectionUpdateById", method = RequestMethod.POST)
	@ResponseBody 
	public AjaxJson sectionUpdateById(SectionEntity sectionEntity){
		AjaxJson j = new AjaxJson();
		j.setMsg("更新成功");
		SectionEntity section = new SectionEntity();
		section.setId(sectionEntity.getId());
		section.setSection_name(sectionEntity.getSection_name());
		section.setLogo(sectionEntity.getLogo());
		section.setSort(sectionEntity.getSort());
		contentManageService.sectionUpdateById(section);
		return j;
	}
	@RequestMapping(value = "sectionSave", method = RequestMethod.POST)
	@ResponseBody 
	public AjaxJson sectionSave(SectionEntity sectionEntity){
		AjaxJson j = new AjaxJson();
		SectionEntity section = new SectionEntity();
		section.setSection_name(sectionEntity.getSection_name());
		section.setLogo(sectionEntity.getLogo());
		section.setSort(sectionEntity.getSort());
		if(sectionEntity.getId() == null || sectionEntity.getId().equals("")){
			Serializable id = contentManageService.sectionSave(section);
			if(!id.equals("") && !id.equals("0")){
				j.setMsg("保存成功");
			}else{
				j.setMsg("保存失败");
				j.setSuccess(false);
			}
		}else{
			Integer result = 0;
			section.setId(sectionEntity.getId());
			result = contentManageService.sectionUpdateById(section);
			if(result>0){
				j.setMsg("更新成功");
			}else{
				j.setMsg("更新失败");
				j.setSuccess(false);
			}
			
		}
		
		return j;
	}
	//#################文章区域##########################
	/**
	 * 详情
	 * */
	@RequestMapping(value = "showMsg")
	public ModelAndView showMsg(ArticleEntity article, ModelMap modelMap){
		article = contentManageService.getArticleData(article);
		modelMap.put("article", article);
		return new ModelAndView("admin/contentManage/showMsg");
	}
	/**
	 * 内容管理
	 * */
	@RequestMapping(value = "articleManage")
	public ModelAndView articleManage(HttpServletRequest request, HttpServletResponse response,  ModelMap modelMap){
		String id = ParamUtil.getParameter(request, "id", "");
		ArticleEntity article = new ArticleEntity();
		SectionEntity section = new SectionEntity();
		DataGrid dataGrid = new DataGrid();
		if(!id.equals("")){
			article.setId(id);
			article = contentManageService.getArticleData(article);
		}
		List<SectionEntity> list = contentManageService.getSectionList(dataGrid, section);
		modelMap.put("sectionList", list);
		modelMap.put("article", article);
		return new ModelAndView("admin/contentManage/articleManage");
	}
	
	/**
	 * 内容列表
	 * */
	@RequestMapping(value = "articleList")
	public ModelAndView articleList(HttpServletRequest request, HttpServletResponse response,  ModelMap modelMap){
		return new ModelAndView("admin/contentManage/articleList");
	}
	
	@RequestMapping(value = "getArticleListPage")
	@ResponseBody 
	public void getArticleListPage(HttpServletRequest request, HttpServletResponse response){
		DataGrid dataGrid = new DataGrid();
		ArticleEntity article = new ArticleEntity();
		String[] ids;
		String field = ParamUtil.getParameter(request, "field", "");
		if(field.indexOf(",")>=0){
			ids = field.split(",");
		}else{
			ids = ParamUtil.getArrayParameter(request, "field");
		}
		article.setTitle(ParamUtil.getParameter(request, "title", ""));
		article.setStatus((byte) 1);
		dataGrid.setPageNum(ParamUtil.getIntParameter(request, "pageNum", 1));
		dataGrid.setShowNum(ParamUtil.getIntParameter(request, "showNum", 10));
		dataGrid.setOrder(SortDirection.desc);
		dataGrid.setSort("create_time");
		dataGrid = contentManageService.getArticleListPage(dataGrid, article);
		DisplayMsgUtil.printMsg(response, DataConstruction(ids, dataGrid));
	}
	@RequestMapping(value = "getArticleData")
	@ResponseBody 
	public AjaxJson getArticleData(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		ArticleEntity article = new ArticleEntity();
		article.setId(ParamUtil.getParameter(request, "id", "0"));
		article = contentManageService.getArticleData(article);
		j.setObj(article);
		j.setMsg("查询成功");
		return j;
	}
	@RequestMapping(value = "articleSave")
	@ResponseBody 
	public AjaxJson articleSave(ArticleEntity articleEntity){
		AjaxJson j = new AjaxJson();
		j.setMsg("保存成功");
		ArticleEntity article = new ArticleEntity();
		article.setSection_id(articleEntity.getSection_id());
		article.setTitle(articleEntity.getTitle());
		article.setContent(articleEntity.getContent());
		article.setSource_name(articleEntity.getSource_name());
		article.setAuthor(articleEntity.getAuthor());
		article.setSummary(articleEntity.getSummary());
		article.setKeyword(articleEntity.getKeyword());
		article.setLink(articleEntity.getLink());
		article.setPic(articleEntity.getPic());
		article.setPriority(articleEntity.getPriority());
		Serializable id = contentManageService.articleSave(article);
		if(id.toString().equals("") || id == null){
			j.setSuccess(false);
			j.setMsg("保存失败");
		}
		return j;
	}
	@RequestMapping(value = "articleUpdateById")
	@ResponseBody 
	public AjaxJson articleUpdateById(ArticleEntity articleEntity){
		AjaxJson j = new AjaxJson();
		j.setMsg("更新成功");
		ArticleEntity article = new ArticleEntity();
		article.setId(articleEntity.getId());
		article.setSection_id(articleEntity.getSection_id());
		article.setTitle(articleEntity.getTitle());
		article.setContent(articleEntity.getContent());
		article.setSource_name(articleEntity.getSource_name());
		article.setAuthor(articleEntity.getAuthor());
		article.setSummary(articleEntity.getSummary());
		article.setLink(articleEntity.getLink());
		article.setKeyword(articleEntity.getKeyword());
		article.setPic(articleEntity.getPic());
		article.setPriority(articleEntity.getPriority());
		Integer id = contentManageService.articleUpdateById(article);
		if(id == null || id<=0){
			j.setSuccess(false);
			j.setMsg("更新失败");
		}
		return j;
	}
	
	@RequestMapping(value = "articleDelById")
	@ResponseBody 
	public AjaxJson articleDelById(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		j.setMsg("删除成功");
		String[] ids;
		if(ParamUtil.getParameter(request, "ids", "").indexOf(",") > 0){
			ids = ParamUtil.getParameter(request, "ids", "").split(",");
		}else{
			ids = ParamUtil.getArrayParameter(request, "ids");
		}
		contentManageService.articleDelById(ids);
		return j;
	}
	//#################函数处理区##########################
	private static String DataConstruction(String[] field, DataGrid dataGrid){
		StringBuilder sbd = new StringBuilder();
		StringBuilder data = new StringBuilder();
		StringBuilder rows = new StringBuilder();
		List<Map<String, Object>> list;
		String msg = "获取数据成功";
		Integer success = 1;
		int i = 0;
		
		list = dataGrid.getResults();
		
		rows.append("[");
		if(list!=null && list.size()>0){
			for(Map<String, Object> m : list){
				if(i>0){
					rows.append(",");
				}
				rows.append("[");
				int j = 0;
				for(String s : field){
					if(j>0){
						rows.append(",");
					}
					if(m.get(s) == null || m.get(s).toString().equals("")){
						rows.append("\"\"");
					}else{
						rows.append("\"" + m.get(s) + "\"");
					}
					j++;
				}
				rows.append("]");
				i++;
			}
		}
		rows.append("]");
		data.append("\"page\"");
		data.append(":");
		data.append("\"" + dataGrid.getPageNum() +"\"");
		data.append(",");
		data.append("\"total\"");
		data.append(":");
		data.append("\"" + dataGrid.getTotal() +"\"");
		data.append(",");
		data.append("\"rows\"");
		data.append(":");
		data.append(rows);
		
		sbd.append("{");
		sbd.append("\"msg\"");
		sbd.append(":");
		sbd.append("\"" + msg +"\"");
		sbd.append(",");
		sbd.append("\"success\"");
		sbd.append(":");
		sbd.append(success);
		sbd.append(",");
		sbd.append("\"data\"");
		sbd.append(":");
		sbd.append("{" + data +"}");
		sbd.append("}"); 
		return sbd.toString();
	}
	
}
