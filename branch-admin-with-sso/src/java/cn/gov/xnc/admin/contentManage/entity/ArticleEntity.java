package cn.gov.xnc.admin.contentManage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * @Title: Entity
 * @Description: 内容表
 * @author jason
 *
 */
@Entity
@Table(name = "xnc_article", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ArticleEntity implements java.io.Serializable{
	private java.lang.String id;
	private java.lang.String section_id;//栏目ID
	private java.lang.String title;//标题
	private java.lang.String content;//内容
	private java.lang.String source_name;//来源
	private java.lang.String author;//作者
	private java.lang.String summary;//摘要
	private java.lang.String keyword;//关键词
	private java.lang.String pic;//导图
	private java.lang.Byte priority;//权重
	private java.lang.Byte status;//状态
	private java.util.Date create_time;//创建日期
	private java.util.Date update_time;//更新日期
	private java.lang.String create_user;//创建人员
	private java.lang.String link;//跳链
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
	@Column(name ="section_id",nullable=true,length=32)
	public java.lang.String getSection_id() {
		return section_id;
	}
	public void setSection_id(java.lang.String section_id) {
		this.section_id = section_id;
	}
	@Column(name ="title",nullable=true,length=100)
	public java.lang.String getTitle() {
		return title;
	}
	public void setTitle(java.lang.String title) {
		this.title = title;
	}
	@Column(name ="content",nullable=true)
	public java.lang.String getContent() {
		return content;
	}
	public void setContent(java.lang.String content) {
		this.content = content;
	}
	@Column(name ="source_name",nullable=true,length=20)
	public java.lang.String getSource_name() {
		return source_name;
	}
	public void setSource_name(java.lang.String source_name) {
		this.source_name = source_name;
	}
	@Column(name ="author",nullable=true,length=200)
	public java.lang.String getAuthor() {
		return author;
	}
	public void setAuthor(java.lang.String author) {
		this.author = author;
	}
	@Column(name ="summary",nullable=true,length=500)
	public java.lang.String getSummary() {
		return summary;
	}
	public void setSummary(java.lang.String summary) {
		this.summary = summary;
	}
	@Column(name ="keyword",nullable=true,length=30)
	public java.lang.String getKeyword() {
		return keyword;
	}
	public void setKeyword(java.lang.String keyword) {
		this.keyword = keyword;
	}
	@Column(name ="pic",nullable=true,length=130)
	public java.lang.String getPic() {
		return pic;
	}
	public void setPic(java.lang.String pic) {
		this.pic = pic;
	}
	@Column(name ="priority",nullable=true,length=4)
	public java.lang.Byte getPriority() {
		return priority;
	}
	public void setPriority(java.lang.Byte priority) {
		this.priority = priority;
	}
	public java.lang.Byte getStatus() {
		return status;
	}
	@Column(name ="status",nullable=true,length=4)
	public void setStatus(java.lang.Byte status) {
		this.status = status;
	}
	
	
	public java.util.Date getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(java.util.Date update_time) {
		this.update_time = update_time;
	}
	public java.util.Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(java.util.Date create_time) {
		this.create_time = create_time;
	}
	public java.lang.String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(java.lang.String create_user) {
		this.create_user = create_user;
	}
	public java.lang.String getLink() {
		return link;
	}
	public void setLink(java.lang.String link) {
		this.link = link;
	}
	
		
}
