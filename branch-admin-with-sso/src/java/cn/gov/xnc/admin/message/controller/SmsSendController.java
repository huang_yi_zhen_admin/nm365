package cn.gov.xnc.admin.message.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.message.entity.SmsSendEntity;
import cn.gov.xnc.admin.message.service.SmsTypeEnum;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Scope("prototype")
@Controller
@RequestMapping("/smsSendController")
public class SmsSendController {
	@Autowired
	private SystemService systeService;
	
	/**
	 * 系统消息列表
	 * */
	@RequestMapping(value = "smsSendList")
	public ModelAndView smsSendList(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/message/smsSendList");
	}
	/**
	 * 支付记录列表
	 * */
	@RequestMapping(value = "getSmsSendListPage")
	@ResponseBody
	public void getPayListPage(SmsSendEntity smsSend, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid){
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(SmsSendEntity.class, dataGrid);
		cq.eq("companyId", user.getCompany().getId());
		if(StringUtil.isNotEmpty(smsSend.getReceiveMobile())){
			cq.like("receiveMobile", "%" + smsSend.getReceiveMobile() + "%");
			smsSend.setReceiveMobile(null);
		}
		cq.add();
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, smsSend, request.getParameterMap());
		systeService.getDataGridReturn(cq, true);
		
		List<SmsSendEntity> smsSendList = dataGrid.getResults();
		for (SmsSendEntity smsSendEntity : smsSendList) {
			smsSendEntity.setType(SmsTypeEnum.getNameByVal(smsSendEntity.getType()));
		}
		
		TagUtil.datagrid(response, dataGrid);
	}
	
}
