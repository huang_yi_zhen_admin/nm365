package cn.gov.xnc.admin.message.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * @Title: Entity
 * @Description: 短信发送记录
 * @author jason
 *
 */
@Entity
@Table(name = "xnc_sms_send", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class SmsSendEntity implements java.io.Serializable{
	private java.lang.String id;
	private java.lang.String type;//消息类型
	private java.lang.String content;//短信内容
	private java.lang.String companyId;//企业ID
	private java.lang.String sendMobile;//发送者手机
	private java.lang.String receiveMobile;//接收者手机
	private java.lang.Integer number;
	private java.util.Date createDate;//创建日期
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
	@Column(name ="type",nullable=false,length=4)
	public java.lang.String getType() {
		return type;
	}
	public void setType(java.lang.String type) {
		this.type = type;
	}
	@Column(name ="content",nullable=false,length=3000)
	public java.lang.String getContent() {
		return content;
	}
	public void setContent(java.lang.String content) {
		this.content = content;
	}
	@Column(name ="company_id",nullable=false,length=32)
	public java.lang.String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(java.lang.String companyId) {
		this.companyId = companyId;
	}
	@Column(name ="send_mobile",nullable=false,length=13)
	public java.lang.String getSendMobile() {
		return sendMobile;
	}
	public void setSendMobile(java.lang.String sendMobile) {
		this.sendMobile = sendMobile;
	}
	@Column(name ="receive_mobile",nullable=false,length=13)
	public java.lang.String getReceiveMobile() {
		return receiveMobile;
	}
	public void setReceiveMobile(java.lang.String receiveMobile) {
		this.receiveMobile = receiveMobile;
	}
	@Column(name ="number",nullable=false,length=6)
	public java.lang.Integer getNumber() {
		return number;
	}
	public void setNumber(java.lang.Integer number) {
		this.number = number;
	}
	@Column(name ="createDate",nullable=true)
	public java.util.Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	
	
}
