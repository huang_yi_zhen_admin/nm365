package cn.gov.xnc.admin.message.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.advertising.entity.AdvertisingPositionEntity;
import cn.gov.xnc.admin.message.entity.MessageTemplateEntity;



/**   
 * @Title: Controller
 * @Description: 消息发送模板管理
 * @author zero
 * @date 2016-10-10 18:58:42
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/messageTemplateController")
public class MessageTemplateController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MessageTemplateController.class);

	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 消息发送模板管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView messageTemplate(MessageTemplateEntity messageTemplate,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		

		CriteriaQuery cq = new CriteriaQuery(MessageTemplateEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, messageTemplate, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, false);
		
		List<MessageTemplateEntity> messageTemplateList = dataGrid.getResults();
		
		request.setAttribute("messageTemplateList", messageTemplateList);
		
		
		return new ModelAndView("admin/message/messageTemplateList");
	}

//	/**
//	 * easyui AJAX请求数据
//	 * 
//	 * @param request
//	 * @param response
//	 * @param dataGrid
//	 * @param user
//	 */
//
//	@RequestMapping(value = "datagrid")
//	public void datagrid(MessageTemplateEntity messageTemplate,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
//		CriteriaQuery cq = new CriteriaQuery(MessageTemplateEntity.class, dataGrid);
//		//查询条件组装器
//		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, messageTemplate, request.getParameterMap());
//		this.systemService.getDataGridReturn(cq, true);
//		TagUtil.datagrid(response, dataGrid);
//	}

	/**
	 * 添加消息发送模板管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(MessageTemplateEntity messageTemplate, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(messageTemplate.getId())) {
			message = "消息发送模板管理更新成功";
			MessageTemplateEntity t = systemService.get(MessageTemplateEntity.class, messageTemplate.getId());
			try {
				messageTemplate.setType(t.getType());
				messageTemplate.setSmsnumber(t.getSmsnumber());
				messageTemplate.setCompany(t.getCompany());
				MyBeanUtils.copyBeanNotNull2Bean(messageTemplate, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "消息发送模板管理更新失败";
			}
		} 
		j.setMsg(message);
		return j;
	}

	/**
	 * 消息发送模板管理列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(MessageTemplateEntity messageTemplate, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(messageTemplate.getId())) {
			messageTemplate = systemService.getEntity(MessageTemplateEntity.class, messageTemplate.getId());
			req.setAttribute("messageTemplatePage", messageTemplate);
		}
		return new ModelAndView("admin/message/messageTemplate");
	}
}
