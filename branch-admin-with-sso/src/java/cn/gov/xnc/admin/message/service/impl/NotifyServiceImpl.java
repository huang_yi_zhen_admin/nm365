package cn.gov.xnc.admin.message.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.message.controller.NotifyConfigCongroller;
import cn.gov.xnc.admin.message.entity.NotifyEntity;
import cn.gov.xnc.admin.message.entity.NotifyTextEntity;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ParamUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("NotifyService")
public class NotifyServiceImpl {
	@Resource(name="systemService")
	private SystemService systemService;
	private final String TBNAME1 = "xnc_notify";
	private final String TBNAME2 = "xnc_notify_text";
	private NotifyServiceImpl(){
		
	}
	
	/**
	 * 消息基本信息 
	 * */
	public NotifyEntity getNotify(NotifyEntity notifyEntity){
		return systemService.findUniqueByProperty(NotifyEntity.class, "id", notifyEntity.getId());
	}
	
	/**
	 * 消息内容信息 
	 * */
	public NotifyTextEntity getNotifyText(NotifyTextEntity notifyTextEntity){
		return systemService.findUniqueByProperty(NotifyTextEntity.class, "id", notifyTextEntity.getId());
	}
	
	/**
	 * 消息内容保存
	 * */
	private Serializable notifyTextSave(NotifyTextEntity notifyTextEntity){
		Serializable id = systemService.save(notifyTextEntity);
		return id;
	}
	/**
	 * 消息基本信息保存
	 * */
	private Serializable notifySave(NotifyEntity notifyEntity){
		Serializable id = systemService.save(notifyEntity);
		return id;
	}
	/**
	 * 消息保存逻辑,一对多
	 * */
	public void notifySave(NotifyTextEntity notifyTextEntity ,List<NotifyEntity> notifyEntityList){
		TSUser user = ResourceUtil.getSessionUserName();
		Serializable id = this.notifyTextSave(notifyTextEntity);
		if(id != null && !id.toString().equals("")){
			for(NotifyEntity notifyEntity : notifyEntityList){
				notifyEntity.setNotify_text_id(id.toString());
				notifyEntity.setSender_id(user.getId());
				notifyEntity.setUserid(notifyEntity.getSender_id()+","+notifyEntity.getReceiver_id());
				notifyEntity.setCreatetime(DateUtils.gettimestamp());
				this.notifySave(notifyEntity);
			}
		}
	}
	/**
	 * 消息保存逻辑,一对一
	 * @param notifyEntity, notifyTextEntity
	 * */
	public void notifySystemSave(NotifyEntity notifyEntity, NotifyTextEntity notifyTextEntity){
		Serializable id = this.notifyTextSave(notifyTextEntity);
		if(id != null && !id.toString().equals("")){
			notifyEntity.setNotify_text_id(id.toString());
			notifyEntity.setNotifytype((byte) NotifyConfigCongroller.NOTIFYNOTICE);
			notifyEntity.setCreatetime(DateUtils.gettimestamp());
			this.notifySave(notifyEntity);
		}
	}
	
	/**
	 * 消息保存逻辑,一对一
	 * @param content, receiverId
	 * */
	public void notifySystemSave(String title, String content, String receiverId){
		TSUser user = ResourceUtil.getSessionUserName();
		NotifyTextEntity notifyTextEntity = new NotifyTextEntity();
		NotifyEntity notifyEntity = new NotifyEntity();
		notifyTextEntity.setContent(content);
		Serializable id = this.notifyTextSave(notifyTextEntity);
		if(id != null && !id.toString().equals("")){
			notifyEntity.setTitle(title);
			notifyEntity.setSender_id(user.getId());
			notifyEntity.setReceiver_id(receiverId);
			notifyEntity.setNotifytype((byte) NotifyConfigCongroller.NOTIFYNOTICE);
			notifyEntity.setNotify_text_id(id.toString());
			notifyEntity.setCreatetime(DateUtils.gettimestamp());
			this.notifySave(notifyEntity);
		}
	}
	
	/**
	 * 删除
	 * */
	public int notifyDelete(String ids){
		String sql = "delete from " + TBNAME1 + " where id in (" + ids + ")";
		System.out.println("###"+sql);
		return systemService.updateBySqlString(sql);
	}
	
	/**
	 * 更新状态
	 * */
	public void updateStatusByIds(String ids){
		String sql = "update " + TBNAME1 + " set status=1, readtime='" + DateUtils.gettimestamp() + "' where id in (" + ids + ")";
		System.out.println("###"+sql);
		systemService.updateBySqlString(sql);
	}
	
	public long getCountUnreadNotify(Integer notifytype){
		TSUser user = ResourceUtil.getSessionUserName();
		long result;
		String sql = "SELECT count(1) FROM " + TBNAME1 + " WHERE receiver_id='" + user.getId() + "' and status=0 and notifytype=" + notifytype;
		result = systemService.getCountForJdbc(sql);
		return result;
	}
	
	public Map<String, Object> getData(Map<String, Object> where){
		StringBuilder sql = new StringBuilder();
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> map = new HashMap<String, Object>();
		sql.append("SELECT b.content");
		sql.append(" from " + TBNAME1 + " as a INNER JOIN " + TBNAME2 + " as b ");
		sql.append(" on a.notify_text_id=b.id");
		sql.append(" WHERE receiver_id='" + user.getId() + "'");
		for(String k : where.keySet()){
			if(where.get(k) != null){
				sql.append(" and " + k + "='" + where.get(k) + "'");
				
			}
		}
		sql.append(" order by a.createtime desc LIMIT 1");
		map = systemService.findOneForJdbc(sql.toString(),null);
		return map;
	}
	
	public String getDataList(DataGrid dataGrid, Map<String, Object> where){
		TSUser user = ResourceUtil.getSessionUserName();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		//NotifyTextEntity notifyTextEntity = null;
		TSUser tSUser = null;
		StringBuilder sbd = new StringBuilder();
		StringBuilder data = new StringBuilder();
		StringBuilder rows = new StringBuilder();
		String title = "";
		String msg = "获取数据成功";
		Integer success = 1;
		int i = 0;
		dataGrid.setField("a.id, a.title, a.sender_id, a.status, a.createtime, b.content");
		long count = 0;
		sql.append("SELECT field");
		sql.append(" from " + TBNAME1 + " as a INNER JOIN " + TBNAME2 + " as b ");
		sql.append(" on a.notify_text_id=b.id");
		sql.append(" WHERE receiver_id='" + user.getId() + "'");
		for(String k : where.keySet()){
			if(where.get(k) != null){
				if(k.equals("key")){
					sql.append(" and title like '%" + where.get(k) + "%'");
				}else{
					sql.append(" and " + k + "='" + where.get(k) + "'");
				}
				
			}
		}
		count = systemService.getCountForJdbc(sql.toString().replace("field", "count(1)"));
		sql.append(" order by a.createtime desc");
		System.out.println("####################################"+sql.toString().replace("field", dataGrid.getField()));
		mapList = systemService.findForJdbc(sql.toString().replace("field", dataGrid.getField()), dataGrid.getPageNum(), dataGrid.getShowNum());
		rows.append("[");
		if(mapList != null && mapList.size()>0){
			for(Map<String, Object> m : mapList){
				String content = "";
				String realname = "";
				tSUser = systemService.findUniqueByProperty(TSUser.class, "id", m.get("sender_id"));
				if(tSUser == null || tSUser.getRealname()==null){
					realname = m.get("sender_id") + "";
				}else{
					realname = tSUser.getRealname();
				}
				if(i>0){
					rows.append(",");
				}
				rows.append("[");
				rows.append("\"" + m.get("id") + "\"");
				rows.append(",");
				if(m.get("title") != null){
					title = m.get("title").toString().replace("\"", "'") + "";
				}else{
					title = m.get("content").toString().replace("\"", "'") + "";
				}
				if(m.get("content").toString() != null){
					content = m.get("content").toString().replace("\"", "'") + "";
					content = content.replaceAll("\"", "\\\"");  
					content = content.replaceAll("\n", "<br/>");
					content = content.replaceAll("\r\n", "<br/>");
					content = content.replaceAll("\r", "<br/>");
				}
				rows.append("\"" + title + "\"");
				rows.append(",");
				rows.append("\"" + content + "\"");
				rows.append(",");
				rows.append("\"" + realname + "\"");
				rows.append(",");
				rows.append("\"" + m.get("createtime") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("status") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("sender_id") + "\"");
				rows.append("]");
				i++;
			}
		}
		rows.append("]");
		data.append("\"page\"");
		data.append(":");
		data.append("\"" + dataGrid.getPageNum() +"\"");
		data.append(",");
		data.append("\"total\"");
		data.append(":");
		data.append("\"" + count +"\"");
		data.append(",");
		data.append("\"rows\"");
		data.append(":");
		data.append(rows);
		
		sbd.append("{");
		sbd.append("\"msg\"");
		sbd.append(":");
		sbd.append("\"" + msg +"\"");
		sbd.append(",");
		sbd.append("\"success\"");
		sbd.append(":");
		sbd.append(success);
		sbd.append(",");
		sbd.append("\"data\"");
		sbd.append(":");
		sbd.append("{" + data +"}");
		sbd.append("}"); 
		return sbd.toString();
	}
	/**
	 * 返回同一家公司用户列表
	 * */
	public String getUserDataListByCompany(DataGrid dataGrid, Map<String, Object> where){
		TSUser user = ResourceUtil.getSessionUserName();
		StringBuilder sbd = new StringBuilder();
		StringBuilder data = new StringBuilder();
		StringBuilder rows = new StringBuilder();
		List<Map<String, Object>> mapList = null;
		dataGrid.setField("id, type, userName, realname, mobilePhone");
		long count = 0;
		String msg = "获取数据成功";
		Integer success = 1;
		int i=0;
		String mobilePhone = "";
		String realname = "";
		String type = "";
		sbd.append("SELECT field from t_s_user where 1=1");
		if(user.getCompany() != null){
			if(user.getCompany().getId() != null){
				sbd.append(" and company='" + user.getCompany().getId() + "'");
			}else{
				sbd.append(" and company=''");
			}
		}
		if(where.get("type") != null){
			sbd.append(" and type=" + where.get("type"));
		}
		
		if(where.get("typein") != null){
			sbd.append(" and type in (" + where.get("typein") + ")");
		}
		
		if(where.get("username") != null){
			sbd.append(" and username like'%" + where.get("username") + "%'");
		}
		if(where.get("realname") != null){
			sbd.append(" and realname like'%" + where.get("realname") + "%'");
		}
		count = systemService.getCountForJdbc(sbd.toString().replace("field", "count(1)"));
		sbd.append(" order by id desc");
		dataGrid.setShowNum(100);
		mapList = systemService.findForJdbc(sbd.toString().replace("field", dataGrid.getField()), dataGrid.getPageNum(), dataGrid.getShowNum());
		sbd = new StringBuilder();
		rows.append("[");
		if(mapList!=null && mapList.size()>0){
			for(Map<String, Object> m : mapList){
				mobilePhone = m.get("mobilePhone") == null ? "" : m.get("mobilePhone") + "";
				realname = m.get("realname") == null ? "" : m.get("realname") + "";
				type = m.get("type")+"";
				if(type.equals("1")){
					type = "管理员";
				}else if(type.equals("3")){
					type = "采购商";
				}else if(type.equals("6")){
					type = "发货商";
				}else{
					type = "企业员工";
				}
				if(i>0){
					rows.append(",");
				}
				rows.append("[");
				rows.append("\"" + m.get("id") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("userName") + "\"");
				rows.append(",");
				rows.append("\"" + realname + "\"");
				rows.append(",");
				rows.append("\"" + mobilePhone + "\"");
				rows.append(",");
				rows.append("\"" + type + "\"");
				rows.append("]");
				i++;
			}
		}
		rows.append("]");
		data.append("\"page\"");
		data.append(":");
		data.append("\"" + dataGrid.getPageNum() +"\"");
		data.append(",");
		data.append("\"total\"");
		data.append(":");
		data.append("\"" + count +"\"");
		data.append(",");
		data.append("\"rows\"");
		data.append(":");
		data.append(rows);
		
		sbd.append("{");
		sbd.append("\"msg\"");
		sbd.append(":");
		sbd.append("\"" + msg +"\"");
		sbd.append(",");
		sbd.append("\"success\"");
		sbd.append(":");
		sbd.append(success);
		sbd.append(",");
		sbd.append("\"data\"");
		sbd.append(":");
		sbd.append("{" + data +"}");
		sbd.append("}"); 
		return sbd.toString();
	}
	/**
	 * TO全作伙伴
	 * @param content, receiverId
	 * */
	public void notifyToPartnerrSave(String orderId, String cases, String array){
		TSUser user = ResourceUtil.getSessionUserName();
		StringBuilder sbd = new StringBuilder();
		List<Map<String, Object>> maplist = null;
		NotifyTextEntity notifyTextEntity = new NotifyTextEntity();
		NotifyEntity notifyEntity = new NotifyEntity();
		String title = "";
		String content = "";
		String receiverId = "";
		String realname = "";
		String identifier = "";
			maplist = systemService.findForJdbc("SELECT id,identifier from xnc_pay_bills_order where id in (" + orderId + ")", null);
			if(maplist != null && maplist.size()>0){
				for(Map<String,Object> m : maplist){
					sbd.append("<a href=\"/payBillsOrderController/addorupdate?id=" + m.get("id") + "\">" + m.get("identifier") + "</a>");
					sbd.append(",");
				}
			}else{
				sbd = new StringBuilder();
				maplist = systemService.findForJdbc("SELECT identifierOr from xnc_order where id in (" + orderId + ")", null);
				for(Map<String,Object> m : maplist){
					sbd.append("<a href=\"/orderController/addorupdate?id=" + m.get("id") + "\">" + m.get("identifierOr") + "</a>");
					sbd.append(",");
				}
			}
		if(cases.equals("paidan")){
			identifier = sbd.toString();
			receiverId = array;
			realname = user.getRealname();
			title = "发货订单";
			content = "平台方：" + realname + "发来新的待发货订单，快去看看吧！订单号：" + identifier;
			
		}
		if(!receiverId.equals("") && !content.equals("")){
			notifyTextEntity.setContent(content);
			Serializable id = this.notifyTextSave(notifyTextEntity);
			if(id != null && !id.toString().equals("")){
				notifyEntity.setTitle(title);
				notifyEntity.setSender_id(user.getId());
				notifyEntity.setReceiver_id(receiverId);
				notifyEntity.setNotifytype((byte) NotifyConfigCongroller.NOTIFYNOTICE);
				notifyEntity.setNotify_text_id(id.toString());
				notifyEntity.setCreatetime(DateUtils.gettimestamp());
				this.notifySave(notifyEntity);
			}
		}
	}
	/**
	 * TO管理员
	 * @param content, receiverId
	 * */
	public void notifyToAdminSave(String orderId, String paytype, String other){
		TSUser user = ResourceUtil.getSessionUserName();
		
		OrderEntity orderEntity = null;
		PayBillsEntity payBillsEntity = null;
		NotifyTextEntity notifyTextEntity = new NotifyTextEntity();
		NotifyEntity notifyEntity = new NotifyEntity();
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		String title = "";
		String content = "";
		String receiverId = "";
		String realname = "";
		String identifier = "";
		String orderlink = "";
		String callbackReceiverId = "";
		if(user != null){
			map = systemService.findOneForJdbc("select id from t_s_user where company='" + user.getCompany().getId() + "' and type=1");
			if(map != null){
				receiverId = map.get("id")+"";
			}
		}
		if(!orderId.equals("")){
			PayBillsOrderEntity payBillsOrderEntity = systemService.findUniqueByProperty(PayBillsOrderEntity.class, "id", orderId);
			if(payBillsOrderEntity != null){
				realname = payBillsOrderEntity.getClientid().getRealname();
				identifier = payBillsOrderEntity.getIdentifier();
				callbackReceiverId = payBillsOrderEntity.getClientid().getId();
				orderlink = "<a href=\"/payBillsOrderController/addorupdate?id=" + payBillsOrderEntity.getId() + 	"\">" + identifier + "</a>";
			}else{
				orderEntity = systemService.findUniqueByProperty(OrderEntity.class, "id", orderId);
				realname = orderEntity.getClientid().getRealname();
				identifier = orderEntity.getIdentifieror();
				callbackReceiverId = orderEntity.getClientid().getId();
				orderlink = "<a href=\"/orderController/addorupdate?id=" + orderEntity.getId() + 	"\">" + identifier + "</a>";
			}
			if(paytype.equals("1")){
				title="预付款";
				content="主人！采购商：" + realname + "订单已发起支付，请发货，订单号：" + orderlink;
			}else if(paytype.equals("2")){
				title="预付款";
				content="主人！采购商：" + realname + "订单已发起支付，请发货，订单号：" + orderlink;
			}else if(paytype.equals("3")){
				title="线下支付";
				content="主人！采购商：" + realname + "使用线下支付付款，快去审核吧！订单号：" + orderlink;
			}else if(paytype.equals("4")){
				title="预付款";
				content="主人！采购商：" + realname + "订单已发起支付，请发货，订单号：" + orderlink;
			}else if(paytype.equals("5")){
				title="先货后款审核";
				content="主人！采购商：" + realname + "申请了先货后款，快去审核吧！订单号：" + orderlink;
			}else if(paytype.equals("cancelorder")){
				title="订单取消";
				content="主人！采购商：" + realname + "已取消了订单，快去看看吧！订单号：" + orderlink;
			}else if(paytype.equals("sendFirstPaymentAudit")){
				title="先货后款审核";
				content="主人！采购商：" + realname + "的订单通过了先货后款审核，快去看看吧！订单号：" + orderlink;
				this.toBuyers(user.getId(), callbackReceiverId, "先货后款订单", "亲，您的先货后款订单已经审核通过，快去看看吧！订单号："+orderlink);
			}else if(paytype.equals("reciever")){
				title="先货后款审核";
			}
		}
		if(!other.equals("")){
			realname = user.getRealname();
			
			if(other.equals("offline")){
				title = "线下支付充值";
				content = "主人！采购商：" + realname + "使用线下支付充值，快去审核吧！";
			}else if(other.indexOf("applicationmoney,") >= 0){
				if(user.getType().equals("4")){
					title = "申请提现";
					content = "主人！业务员：" + realname + "申请提现，提现金额：¥" + other.split(",")[1] + "，请审核！";
				}else if(user.getType().equals("6")){
					title = "申请发货款结算";
					content = "主人！发货商：" + realname + "申请发货款结算，结算金额：¥" + other.split(",")[1] + "，请审核！";
				}
				
			}else if(other.indexOf("orderstate,") >= 0){
				String[] orderstateArray = other.split(",");
				String id = orderstateArray[1];
				orderEntity = systemService.findUniqueByProperty(OrderEntity.class, "id", id);
				if(orderEntity != null){
					if(orderEntity.getState().equals("3")){
						title = "订单发货";
						content = "主人！采购商：" + orderEntity.getClientid().getRealname() + "的订单已发货!";
						this.toBuyers(user.getId(), orderEntity.getClientid().getId(), "订单已发货", "亲，您的订单已发货，快去看看吧！订单号：" + orderEntity.getIdentifieror());
					}
				}
			}else if(other.indexOf("paybillAudit,") >=0 ){
				String[] orderstateArray = other.split(",");
				String id = orderstateArray[1];
				String state = orderstateArray[2];
				payBillsEntity = systemService.findUniqueByProperty(PayBillsEntity.class, "id", id);
				if(payBillsEntity != null){
					callbackReceiverId = payBillsEntity.getClientid().getId();
					if(payBillsEntity.getType().equals("1")){
						if(state.equals("1")){
							title = "支付审核";
							content = "主人！采购商：" + payBillsEntity.getClientid().getRealname() + "的订单通过了支付审核，快去看看吧！";
							this.toBuyers(user.getId(), callbackReceiverId, "支付审核", "亲，您的线下支付已经审核通过，快去看看吧！");
						}else if(state.equals("3")){
							this.toBuyers(user.getId(), callbackReceiverId, "支付审核", "亲，您的线下支付未能审核通过，快去看看吧！");
						}
					}else if(payBillsEntity.getType().equals("2")){
						if(state.equals("1")){
							this.toBuyers(user.getId(), callbackReceiverId, "充值审核", "亲，您使用线下支付充值，已经审核通过！");
						}else if(state.equals("3")){
							this.toBuyers(user.getId(), callbackReceiverId, "充值审核", "亲，您使用线下支付充值，未能审核通过，快去看看吧！");
						}
					}
					
					
				}
			}else if(other.indexOf("buyersRegStatus,") >=0 ){
				String[] orderstateArray = other.split(",");
				String id = orderstateArray[1];
				title = "申请注册";
				content = "主人！新采购商：" + id + "已申请注册，请审核！";
			}
			
		}
		
		if(!receiverId.equals("") && !content.equals("")){
			notifyTextEntity.setContent(content);
			Serializable id = this.notifyTextSave(notifyTextEntity);
			if(id != null && !id.toString().equals("")){
				notifyEntity.setTitle(title);
				notifyEntity.setSender_id(user.getId());
				notifyEntity.setReceiver_id(receiverId);
				notifyEntity.setNotifytype((byte) NotifyConfigCongroller.NOTIFYNOTICE);
				notifyEntity.setNotify_text_id(id.toString());
				notifyEntity.setCreatetime(DateUtils.gettimestamp());
				this.notifySave(notifyEntity);
			}
		}
		
	}
	/**
	 * 做个回调
	 * */
	private void toBuyers(String SenderId , String receiverId, String title, String content){
		NotifyTextEntity notifyTextEntity = new NotifyTextEntity();
		NotifyEntity notifyEntity = new NotifyEntity();
		notifyTextEntity.setContent(content);
		Serializable id = this.notifyTextSave(notifyTextEntity);
		notifyEntity.setTitle(title);
		notifyEntity.setSender_id(SenderId);
		notifyEntity.setReceiver_id(receiverId);
		notifyEntity.setNotifytype((byte) NotifyConfigCongroller.NOTIFYNOTICE);
		notifyEntity.setNotify_text_id(id.toString());
		notifyEntity.setCreatetime(DateUtils.gettimestamp());
		this.notifySave(notifyEntity);
	} 
}
