package cn.gov.xnc.admin.commercial.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.commercial.entity.BusinessShareEntity;
import cn.gov.xnc.admin.commercial.entity.BusinessShareFeedbackLogEntity;
import cn.gov.xnc.admin.commercial.service.BusinessShareFeedbackLogServiceI;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 公司招商分享信息反馈表
 * @author zero
 * @date 2018-03-30 12:05:57
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/businessShareFeedbackLogController")
public class BusinessShareFeedbackLogController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BusinessShareFeedbackLogController.class);

	@Autowired
	private BusinessShareFeedbackLogServiceI businessShareFeedbackLogService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private MessageTemplateServiceI messageTemplateService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 公司招商分享信息反馈表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView listBusinessShareFeedbackLog(HttpServletRequest request) {
		return new ModelAndView("admin/commercial/businessShareFeedbackLogList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(BusinessShareFeedbackLogEntity businessShareFeedbackLog,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(BusinessShareFeedbackLogEntity.class, dataGrid);
		cq.eq("sendType", BusinessShareFeedbackLogServiceI.TYPE_MSG_ACCEPT);
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, businessShareFeedbackLog, request.getParameterMap());
		this.businessShareFeedbackLogService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除公司招商分享信息反馈表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(BusinessShareFeedbackLogEntity businessShareFeedbackLog, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		businessShareFeedbackLog = systemService.getEntity(BusinessShareFeedbackLogEntity.class, businessShareFeedbackLog.getId());
		message = "公司招商分享信息反馈表删除成功";
		businessShareFeedbackLogService.delete(businessShareFeedbackLog);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加公司招商分享信息反馈表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(BusinessShareFeedbackLogEntity businessShareFeedbackLog, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(businessShareFeedbackLog.getId())) {
			message = "公司招商分享信息反馈表更新成功";
			BusinessShareFeedbackLogEntity t = businessShareFeedbackLogService.get(BusinessShareFeedbackLogEntity.class, businessShareFeedbackLog.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(businessShareFeedbackLog, t);
				businessShareFeedbackLogService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "公司招商分享信息反馈表更新失败";
			}
		} else {
			message = "公司招商分享信息反馈表添加成功";
			businessShareFeedbackLogService.save(businessShareFeedbackLog);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 公司招商分享信息反馈表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(BusinessShareFeedbackLogEntity businessShareFeedbackLog, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(businessShareFeedbackLog.getId())) {
			businessShareFeedbackLog = businessShareFeedbackLogService.getEntity(BusinessShareFeedbackLogEntity.class, businessShareFeedbackLog.getId());
			
		}else{
			businessShareFeedbackLog = new BusinessShareFeedbackLogEntity();
		}
		req.setAttribute("businessShareFeedbackLogPage", businessShareFeedbackLog);
		
		return new ModelAndView("admin/commercial/businessShareReply");
	}
	
	@RequestMapping(value = "shareReply")
	@ResponseBody
	public AjaxJson shareReply(BusinessShareFeedbackLogEntity businessShareFeedback, HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		try {
			BusinessShareFeedbackLogEntity businessShareFeedbackVO = businessShareFeedbackLogService.findUniqueByProperty(BusinessShareFeedbackLogEntity.class, "id", businessShareFeedback.getId());
			BusinessShareEntity businessShare = businessShareFeedbackVO.getBusinessShareId();
			TSCompany company = businessShare.getCompanyid();
			StringBuffer msgbuf = new StringBuffer();
			msgbuf.append("【").append(company.getCompanyName()).append("】").append(businessShare.getTitle()).append(" 答复:\r\n").append(businessShareFeedback.getSendMsg());
			
			BusinessShareFeedbackLogEntity businessShareFeedbackDO= new BusinessShareFeedbackLogEntity();
			businessShareFeedbackDO.setBusinessShareId(businessShareFeedbackVO.getBusinessShareId());
			businessShareFeedbackDO.setCreatetime(DateUtils.getDate());
			businessShareFeedbackDO.setRecieveId(businessShareFeedbackVO.getSendId());
			businessShareFeedbackDO.setRecieveName(businessShareFeedbackVO.getSendName());
			businessShareFeedbackDO.setSendId(businessShareFeedbackVO.getRecieveId());
			businessShareFeedbackDO.setSendName(businessShareFeedbackVO.getRecieveName());
			businessShareFeedbackDO.setSendMsg(msgbuf.toString());
			businessShareFeedbackDO.setSendType(BusinessShareFeedbackLogServiceI.TYPE_MSG_REPLY);//相对平台来说，是收到消息
			businessShareFeedbackLogService.save(businessShareFeedbackDO);
			
			
			businessShareFeedbackVO.setReply(BusinessShareFeedbackLogServiceI.REPLY_YES);
			businessShareFeedbackLogService.updateEntitie(businessShareFeedbackVO);
			
			messageTemplateService.sendSMSTTMM(null, null, businessShareFeedbackVO.getSendId(), "招商分享", msgbuf.toString());
			
			message = "信息发送成功！";
		} catch (Exception e) {
			j.setSuccess(false);
			message = "信息发送失败" + e.getMessage();
		}
		
		j.setMsg(message);
		return j;
	}
}
