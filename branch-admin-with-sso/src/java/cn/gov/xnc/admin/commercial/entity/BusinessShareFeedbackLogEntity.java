package cn.gov.xnc.admin.commercial.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 公司招商分享信息反馈表
 * @author zero
 * @date 2018-03-30 12:05:58
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_business_share_feedback_log", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class BusinessShareFeedbackLogEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**招商分享关联id*/
	private BusinessShareEntity businessShareId;
	/**发送消息人id，可以是电话号码或者其它消息工具id*/
	private java.lang.String sendId;
	/**发送消息的人姓名*/
	private java.lang.String sendName;
	/**接收消息人id，可以是电话或者其它消息工具id*/
	private java.lang.String recieveId;
	/**接收消息人姓名*/
	private java.lang.String recieveName;
	/**消息主体*/
	private java.lang.String sendMsg;
	/**消息类型： 'A' accept 接收 ‘R’reply 回复*/
	private java.lang.String sendType;
	/**是否回复： 'Y' 已回复 'N' 未回复*/
	private java.lang.String reply;
	/**createtime*/
	private java.util.Date createtime;
	/**更新时间*/
	private java.util.Date updatetime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  招商分享关联id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BUSINESS_SHARE_ID")
	public BusinessShareEntity getBusinessShareId(){
		return this.businessShareId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  招商分享关联id
	 */
	public void setBusinessShareId(BusinessShareEntity businessShareId){
		this.businessShareId = businessShareId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发送消息人id，可以是电话号码或者其它消息工具id
	 */
	@Column(name ="SEND_ID",nullable=false,length=32)
	public java.lang.String getSendId(){
		return this.sendId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发送消息人id，可以是电话号码或者其它消息工具id
	 */
	public void setSendId(java.lang.String sendId){
		this.sendId = sendId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发送消息的人姓名
	 */
	@Column(name ="SEND_NAME",nullable=false,length=255)
	public java.lang.String getSendName(){
		return this.sendName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发送消息的人姓名
	 */
	public void setSendName(java.lang.String sendName){
		this.sendName = sendName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  接收消息人id，可以是电话或者其它消息工具id
	 */
	@Column(name ="RECIEVE_ID",nullable=false,length=32)
	public java.lang.String getRecieveId(){
		return this.recieveId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  接收消息人id，可以是电话或者其它消息工具id
	 */
	public void setRecieveId(java.lang.String recieveId){
		this.recieveId = recieveId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  接收消息人姓名
	 */
	@Column(name ="RECIEVE_NAME",nullable=false,length=255)
	public java.lang.String getRecieveName(){
		return this.recieveName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  接收消息人姓名
	 */
	public void setRecieveName(java.lang.String recieveName){
		this.recieveName = recieveName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  消息主体
	 */
	@Column(name ="SEND_MSG",nullable=false,length=200)
	public java.lang.String getSendMsg(){
		return this.sendMsg;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  消息主体
	 */
	public void setSendMsg(java.lang.String sendMsg){
		this.sendMsg = sendMsg;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  消息类型： 'A' accept 接收 ‘R’reply 回复
	 */
	@Column(name ="SEND_TYPE",nullable=false,length=2)
	public java.lang.String getSendType(){
		return this.sendType;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  消息类型： 'A' accept 接收 ‘R’reply 回复
	 */
	public void setSendType(java.lang.String sendType){
		this.sendType = sendType;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  createtime
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  createtime
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATETIME",nullable=false)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}

	public java.lang.String getReply() {
		return reply;
	}

	public void setReply(java.lang.String reply) {
		this.reply = reply;
	}
}
