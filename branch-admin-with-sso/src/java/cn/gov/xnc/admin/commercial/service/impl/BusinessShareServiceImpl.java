package cn.gov.xnc.admin.commercial.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.commercial.service.BusinessShareServiceI;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("businessShareService")
@Transactional
public class BusinessShareServiceImpl extends CommonServiceImpl implements BusinessShareServiceI {

	@Override
	public List<TSDictionary> getListBusinessShareType() {
		CriteriaQuery cq1 = new CriteriaQuery(TSDictionary.class);
		cq1.eq("dictionaryType", BusinessShareServiceI.BUSINESS_SHARE_DICT_TYPE);
		cq1.eq("isdel", "N");
		cq1.add();
		
		return getListByCriteriaQuery(cq1, false);
	}

	@Override
	public String getBusinessShareTypeName(String businessTypeId) {
		String bufName = null;
		if(StringUtil.isNotEmpty(businessTypeId)){
			String[] businessArray = businessTypeId.split(",");
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT  GROUP_CONCAT(a.dictionaryName SEPARATOR ',') FROM t_s_dictionary a WHERE a.dictionaryType = 'DIC_TYPE_BUSINESS_SHARE' AND a.id in (");
			for (int i = 0; i < businessArray.length; i++) {
				if(i == 0){
					sql.append("'").append(businessArray[i]).append("'");
				}else{
					sql.append(",'").append(businessArray[i]).append("'");
				}
			}
			sql.append(" )");
			sql.append(" GROUP BY a.dictionaryType");
			
			List<String> typeNameList = findListbySql(sql.toString());
			if(null != typeNameList && typeNameList.size() > 0){
				bufName = typeNameList.get(0);
			}
		}
		
		return bufName;
	}
	
	
}