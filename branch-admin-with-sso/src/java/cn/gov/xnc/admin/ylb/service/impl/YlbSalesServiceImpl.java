package cn.gov.xnc.admin.ylb.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.ylb.service.YlbSalesServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("ylbSalesService")
@Transactional
public class YlbSalesServiceImpl extends CommonServiceImpl implements YlbSalesServiceI {
	
}