package cn.gov.xnc.admin.product.service;

public enum ProductEnumMap {
	ASSOCIATE_STOCK_YES("商品关联仓库自动出库", "Y"), 
	ASSOCIATE_STOCK_NO("商品销售不关联仓库", "N"), 
	STATE_ONLINE("商品上架","1"), 
	STATE_OFFLINE("商品下架", "2"),
	NOTSALE_YES("非销售商品", "Y"),
	NOTSALE_NOT("非销售商品", "N"),
	SALE_STATUS_NORMAL("正常销售", "1"),
	SALE_STATUS_PRE("预售", "2"),
	SALE_STATUS_STOP("停售", "3");

	private String name;
	private String value;

	private ProductEnumMap(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String index) {
		this.value = index;
	}
}
