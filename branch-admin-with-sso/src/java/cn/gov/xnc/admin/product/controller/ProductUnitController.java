package cn.gov.xnc.admin.product.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.product.service.ProductUnitServiceI;

/**   
 * @Title: Controller
 * @Description: 产品单位
 * @author zero
 * @date 2017-02-27 15:35:12
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/productUnitController")
public class ProductUnitController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductUnitController.class);

	@Autowired
	private ProductUnitServiceI productUnitService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 产品单位列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView productUnit(HttpServletRequest request) {
		return new ModelAndView("admin/product/productUnitList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(ProductUnitEntity productUnit,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ProductUnitEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, productUnit, request.getParameterMap());
		this.productUnitService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除产品单位
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(ProductUnitEntity productUnit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		productUnit = systemService.getEntity(ProductUnitEntity.class, productUnit.getId());
		message = "产品单位删除成功";
		productUnitService.delete(productUnit);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加产品单位
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ProductUnitEntity productUnit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(productUnit.getId())) {
			message = "产品单位更新成功";
			ProductUnitEntity t = productUnitService.get(ProductUnitEntity.class, productUnit.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(productUnit, t);
				productUnitService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "产品单位更新失败";
			}
		} else {
			message = "产品单位添加成功";
			productUnitService.save(productUnit);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 产品单位列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ProductUnitEntity productUnit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productUnit.getId())) {
			productUnit = productUnitService.getEntity(ProductUnitEntity.class, productUnit.getId());
			req.setAttribute("productUnitPage", productUnit);
		}
		return new ModelAndView("admin/product/productUnit");
	}
}
