package cn.gov.xnc.admin.salesman.service;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface SalesmanCommissionServiceI extends CommonService{
	
	
	public AjaxJson SalesmanCommissionSave(OrderEntity  order  );
	
}
