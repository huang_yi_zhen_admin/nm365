package cn.gov.xnc.admin.salesman.service;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

public interface SalesmanAuditServiceI extends CommonService{

	public StatisPageVO statisSalemanAudit(TSCompany company,HttpServletRequest request);
}
