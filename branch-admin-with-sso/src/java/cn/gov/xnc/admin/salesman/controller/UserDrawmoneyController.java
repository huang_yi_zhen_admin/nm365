package cn.gov.xnc.admin.salesman.controller;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.salesman.entity.UserDrawmoneyEntity;
import cn.gov.xnc.admin.salesman.service.UserDrawmoneyServiceI;

/**   
 * @Title: Controller
 * @Description: 提现申请
 * @author zero
 * @date 2016-11-28 00:34:45
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/userDrawmoneyController")
public class UserDrawmoneyController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserDrawmoneyController.class);

	@Autowired
	private UserDrawmoneyServiceI userDrawmoneyService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 提现申请列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView userDrawmoney(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/userDrawmoneyList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(UserDrawmoneyEntity userDrawmoney,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(UserDrawmoneyEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userDrawmoney, request.getParameterMap());
		this.userDrawmoneyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}




	/**
	 * 添加提现申请
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(UserDrawmoneyEntity userDrawmoney, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userDrawmoney.getId())) {
			message = "提现申请更新成功";
			UserDrawmoneyEntity t = userDrawmoneyService.get(UserDrawmoneyEntity.class, userDrawmoney.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userDrawmoney, t);
				userDrawmoneyService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "提现申请更新失败";
				j.setSuccess(false);
			}
		} else {
			
			TSUser user = ResourceUtil.getSessionUserName();
			userDrawmoney.setUser(user);
			userDrawmoney.setCompany(user.getCompany());
			userDrawmoney.setState("1");
			
			if(userDrawmoney.getApplicationmoney().compareTo(new BigDecimal("0.00")) == -1  ){
				message = "申请金额必须大于0.00！";
				j.setMsg(message);
				j.setSuccess(false);
				return j;
			}
			
			
			UserSalesmanEntity tsuserSalesman  = systemService.get(UserSalesmanEntity.class, user.getTsuserSalesman().getId());
			if( (tsuserSalesman.getWithdrawalsmoney()).compareTo(userDrawmoney.getApplicationmoney()) == -1 ){
				message = "申请金额不得大于可提现金额！";
				j.setMsg(message);
				j.setSuccess(false);
				return j;
			}
			
			
			message = "提现申请添加成功";
			userDrawmoneyService.save(userDrawmoney);
			
				tsuserSalesman.setWithdrawalsmoney(tsuserSalesman.getWithdrawalsmoney().subtract(userDrawmoney.getApplicationmoney()));
			systemService.saveOrUpdate(tsuserSalesman);//更新个人账号信息
			
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 提现申请列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(UserDrawmoneyEntity userDrawmoney, HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		UserSalesmanEntity tsuserSalesman  = systemService.get(UserSalesmanEntity.class, user.getTsuserSalesman().getId());
		
		if (StringUtil.isNotEmpty(userDrawmoney.getId())) {
			userDrawmoney = userDrawmoneyService.getEntity(UserDrawmoneyEntity.class, userDrawmoney.getId());
			
		}
		userDrawmoney.setWithdrawalsmoney(tsuserSalesman.getWithdrawalsmoney());
		req.setAttribute("userDrawmoneyPage", userDrawmoney);
		return new ModelAndView("admin/salesman/drawmoney");
	}
	
	
	
	
	/**
	 * 提现申请列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "checkaddorupdate")
	public ModelAndView checkaddorupdate(UserDrawmoneyEntity userDrawmoney, HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(userDrawmoney.getId())) {
			userDrawmoney = userDrawmoneyService.getEntity(UserDrawmoneyEntity.class, userDrawmoney.getId());
		}
		req.setAttribute("userDrawmoneyPage", userDrawmoney);
		return new ModelAndView("admin/salesman/userDrawmoneyAudit");
	}
	
	
	
	/**
	 * 提现申请审核
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "checksave")
	@ResponseBody
	public AjaxJson checksave(UserDrawmoneyEntity userDrawmoney, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userDrawmoney.getId())) {
			message = "提现申请更新成功";
			
			TSUser checkuser = ResourceUtil.getSessionUserName();
			
			userDrawmoney.setCheckuser(checkuser);
			userDrawmoney.setCheckdate(DateUtils.getDate());
			
			
			
			UserDrawmoneyEntity t = userDrawmoneyService.get(UserDrawmoneyEntity.class, userDrawmoney.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userDrawmoney, t);
				userDrawmoneyService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "提现申请更新失败";
			}
		} 
		j.setMsg(message);
		return j;
	}
	
	
	@RequestMapping(value= "statisUserDrawmoney")
	@ResponseBody
	public AjaxJson statisUserDrawmoney(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计提现审核单失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = userDrawmoneyService.statisUserDrawmoney(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计提现审核成功！");
			
		}
		
		return json;
	}
	
}
