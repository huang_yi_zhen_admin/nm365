package cn.gov.xnc.admin.salesman.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.salesman.service.SalesmanAuditServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

@Service("salesmanAuditService")
@Transactional
public class SalesmanAuditServiceImpl extends CommonServiceImpl implements SalesmanAuditServiceI {

	@Override
	public StatisPageVO statisSalemanAudit(TSCompany company, HttpServletRequest request) {
		String state = request.getParameter("state");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.applicationmoney),0) value2 FROM xnc_salesman_audit a ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& "1".equals(state)){
			Sql.append(" AND a.state not in ( '2','3') ");//待审核单
			
		}else if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
				&& "2".equals(state)){
			
			Sql.append(" AND a.state ='2' ");//已审核单
			
		}else{
			//Sql.append(" AND a.state in ('2','3') ");//默认全部
		}

		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		
		return statisPageVO;
	}
	
}