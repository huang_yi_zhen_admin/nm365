package cn.gov.xnc.admin.salesman.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 提现申请
 * @author zero
 * @date 2016-11-28 00:34:45
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_user_drawmoney", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class UserDrawmoneyEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**用户信息*/
	private TSUser user;
	/**公司信息*/
	private TSCompany  company;
	/**申请时间*/
	private java.util.Date createdate;
	/**申请_1,已通过_2*/
	private java.lang.String state;
	/**申请金额*/
	private BigDecimal applicationmoney;
	/**可提现金额*/
	private BigDecimal withdrawalsmoney;
	/**方式  支付宝_1,微信_2,银行_3*/
	private java.lang.String mode;
	/**账号*/
	private java.lang.String bankaccount;
	/**账号名称*/
	private java.lang.String bankname;
	/**姓名*/
	private java.lang.String username;
	/**审核人*/
	private TSUser checkuser;
	/**审核时间*/
	private java.util.Date checkdate;
	/**备注*/
	private java.lang.String remarks;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER")
	public TSUser getUser(){
		return this.user;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户信息
	 */
	public void setUser(TSUser user){
		this.user = user;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany(){
		return this.company;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company){
		this.company = company;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  申请时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  申请时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  申请_1,已通过_2
	 */
	@Column(name ="STATE",nullable=true,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  申请_1,已通过_2
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  申请金额
	 */
	@Column(name ="APPLICATIONMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getApplicationmoney(){
		return this.applicationmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  申请金额
	 */
	public void setApplicationmoney(BigDecimal applicationmoney){
		this.applicationmoney = applicationmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  可提现金额
	 */
	@Column(name ="WITHDRAWALSMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getWithdrawalsmoney(){
		return this.withdrawalsmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  可提现金额
	 */
	public void setWithdrawalsmoney(BigDecimal withdrawalsmoney){
		this.withdrawalsmoney = withdrawalsmoney;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  方式  支付宝_1,微信_2,银行_3
	 */
	@Column(name ="MODE",nullable=true,length=2)
	public java.lang.String getMode(){
		return this.mode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  方式  支付宝_1,微信_2,银行_3
	 */
	public void setMode(java.lang.String mode){
		this.mode = mode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  账号
	 */
	@Column(name ="BANKACCOUNT",nullable=true,length=255)
	public java.lang.String getBankaccount(){
		return this.bankaccount;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  账号
	 */
	public void setBankaccount(java.lang.String bankaccount){
		this.bankaccount = bankaccount;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  账号名称
	 */
	@Column(name ="BANKNAME",nullable=true,length=255)
	public java.lang.String getBankname(){
		return this.bankname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  账号名称
	 */
	public void setBankname(java.lang.String bankname){
		this.bankname = bankname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  姓名
	 */
	@Column(name ="USERNAME",nullable=true,length=255)
	public java.lang.String getUsername(){
		return this.username;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  姓名
	 */
	public void setUsername(java.lang.String username){
		this.username = username;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  审核人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHECKUSER")
	public TSUser getCheckuser(){
		return this.checkuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  审核人
	 */
	public void setCheckuser(TSUser checkuser){
		this.checkuser = checkuser;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  审核时间
	 */
	@Column(name ="CHECKDATE",nullable=true)
	public java.util.Date getCheckdate(){
		return this.checkdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  审核时间
	 */
	public void setCheckdate(java.util.Date checkdate){
		this.checkdate = checkdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARKS",nullable=true,length=10000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
}
