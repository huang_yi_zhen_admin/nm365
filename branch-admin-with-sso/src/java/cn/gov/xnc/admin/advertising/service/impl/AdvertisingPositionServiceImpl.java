package cn.gov.xnc.admin.advertising.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.advertising.service.AdvertisingPositionServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("advertisingPositionService")

public class AdvertisingPositionServiceImpl extends CommonServiceImpl implements AdvertisingPositionServiceI {
	
}