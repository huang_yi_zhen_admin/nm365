package cn.gov.xnc.admin.thirdorder.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderChannelEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderConfigureEntity;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderConfigureServiceI;

/**   
 * @Title: Controller
 * @Description: 第三方订单导入配置
 * @author zero
 * @date 2017-07-21 17:46:38
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/thirdorderConfigureController")
public class ThirdorderConfigureController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ThirdorderConfigureController.class);

	@Autowired
	private ThirdorderConfigureServiceI thirdorderConfigureService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 第三方订单导入配置列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView thirdorderConfigure(HttpServletRequest request) {
		return new ModelAndView("admin/thirdorder/thirdorderConfigureList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ThirdorderConfigureEntity thirdorderConfigure,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ThirdorderConfigureEntity.class, dataGrid);
		cq.eq("status", ThirdorderConfigureEntity.STATUS_NORMAL);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, thirdorderConfigure, request.getParameterMap());
		this.thirdorderConfigureService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除第三方订单导入配置
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ThirdorderConfigureEntity thirdorderConfigure, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		thirdorderConfigure = systemService.getEntity(ThirdorderConfigureEntity.class, thirdorderConfigure.getId());
		thirdorderConfigure.setStatus(ThirdorderConfigureEntity.STATUS_DEL);
		message = "第三方订单导入配置删除成功";
		thirdorderConfigureService.updateEntitie(thirdorderConfigure);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}

	/**
	 * 根据id获取配置内容
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getConfigById")
	@ResponseBody
	public AjaxJson getConfigById(ThirdorderConfigureEntity thirdorderConfigure, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		ThirdorderConfigureEntity t = thirdorderConfigureService.get(ThirdorderConfigureEntity.class, thirdorderConfigure.getId());
		
		j.setObj(t);
		j.setMsg("成功");
		j.setSuccess(true);
		
		return j;
		
	}

	/**
	 * 添加第三方订单导入配置
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ThirdorderConfigureEntity thirdorderConfigure, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		thirdorderConfigure.setStatus(ThirdorderConfigureEntity.STATUS_NORMAL);
		thirdorderConfigure.setCompany(user.getCompany());
		
		if (StringUtil.isNotEmpty(thirdorderConfigure.getId())) {
			message = "第三方订单导入配置更新成功";
			ThirdorderConfigureEntity t = thirdorderConfigureService.get(ThirdorderConfigureEntity.class, thirdorderConfigure.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(thirdorderConfigure, t);
				thirdorderConfigureService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "第三方订单导入配置更新失败";
			}
		} else {
			Map<String, Object> attributes = new HashMap<String, Object>();
			thirdorderConfigure.setCreatetime(DateUtils.getDate());
			thirdorderConfigure.setCreateuser(user);
			message = "第三方订单导入配置添加成功";
			thirdorderConfigureService.save(thirdorderConfigure);
			attributes.put("id", thirdorderConfigure.getId());
			attributes.put("name", thirdorderConfigure.getName());
			j.setAttributes(attributes);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 第三方订单导入配置列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ThirdorderConfigureEntity thirdorderConfigure, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(thirdorderConfigure.getId())) {
			thirdorderConfigure = thirdorderConfigureService.getEntity(ThirdorderConfigureEntity.class, thirdorderConfigure.getId());
			req.setAttribute("thirdorderConfigurePage", thirdorderConfigure);
		}else{
			thirdorderConfigure = new ThirdorderConfigureEntity();
			req.setAttribute("thirdorderConfigurePage", thirdorderConfigure);
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		//获取公司商品品牌列表
		CriteriaQuery cb = new CriteriaQuery(ThirdorderChannelEntity.class);
		cb.eq("company", user.getCompany());
		cb.eq("status", ThirdorderChannelEntity.STATUS_NORMAL);
		cb.add();
		List<ThirdorderChannelEntity> channellist = systemService.getListByCriteriaQuery(cb, false);
		req.setAttribute("channellist", channellist);
				
				
		return new ModelAndView("admin/thirdorder/thirdorderConfigure");
	}
	
	
	
	
	@RequestMapping(value = "configuredetails")
	public ModelAndView configuredetails(ThirdorderConfigureEntity thirdorderConfigure, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(thirdorderConfigure.getId())) {
			thirdorderConfigure = thirdorderConfigureService.getEntity(ThirdorderConfigureEntity.class, thirdorderConfigure.getId());
			req.setAttribute("thirdorderConfigurePage", thirdorderConfigure);
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		//获取公司商品品牌列表
		CriteriaQuery cb = new CriteriaQuery(ThirdorderChannelEntity.class);
		cb.eq("company", user.getCompany());
		cb.eq("status", ThirdorderChannelEntity.STATUS_NORMAL);
		cb.add();
		List<ThirdorderChannelEntity> channellist = systemService.getListByCriteriaQuery(cb, false);
		req.setAttribute("channellist", channellist);
				
				
		return new ModelAndView("admin/thirdorder/thirdorderConfigureDetail");
	}
}
