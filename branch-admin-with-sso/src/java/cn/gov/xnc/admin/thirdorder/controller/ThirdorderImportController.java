package cn.gov.xnc.admin.thirdorder.controller;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderImportEntity;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderImportServiceI;

/**   
 * @Title: Controller
 * @Description: 第三方订单导入记录
 * @author zero
 * @date 2017-07-21 17:47:42
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/thirdorderImportController")
public class ThirdorderImportController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ThirdorderImportController.class);

	@Autowired
	private ThirdorderImportServiceI thirdorderImportService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 第三方订单导入记录列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView thirdorderImport(HttpServletRequest request) {
		return new ModelAndView("admin/thirdorder/thirdorderImportList");
	}
	
	
	/**
	 * 第三方订单批次详情
	 * 
	 * @return
	 */
	@RequestMapping(value = "importdetails")
	public ModelAndView importdetails(ThirdorderImportEntity importEntity, HttpServletRequest request) {
		
		request.setAttribute("importid", importEntity.getId());
		
		return new ModelAndView("admin/thirdorder/thirdorderImportDetail");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ThirdorderImportEntity thirdorderImport,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();	
		
		CriteriaQuery cq = new CriteriaQuery(ThirdorderImportEntity.class, dataGrid);
		//查询条件组装器
		cq.eq("company", user.getCompany());
		cq.eq("status", ThirdorderImportEntity.STATUS_NORMAL  );
		
		String createdate1 = request.getParameter("dateBegin");
		String createdate2 = request.getParameter("dateEnd");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createtime", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createtime", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createtime", DateUtils.str2Date(createdate2,sdf));
		}
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, thirdorderImport, request.getParameterMap());
		this.thirdorderImportService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除第三方订单导入记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "第三方订单导入记录删除成功";
		j = thirdorderImportService.batchDel(ids);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		return j;
	}


	/**
	 * 添加第三方订单导入记录
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ThirdorderImportEntity thirdorderImport, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(thirdorderImport.getId())) {
			message = "第三方订单导入记录更新成功";
			ThirdorderImportEntity t = thirdorderImportService.get(ThirdorderImportEntity.class, thirdorderImport.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(thirdorderImport, t);
				thirdorderImportService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "第三方订单导入记录更新失败";
			}
		} else {
			message = "第三方订单导入记录添加成功";
			thirdorderImportService.save(thirdorderImport);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 第三方订单导入记录列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ThirdorderImportEntity thirdorderImport, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(thirdorderImport.getId())) {
			thirdorderImport = thirdorderImportService.getEntity(ThirdorderImportEntity.class, thirdorderImport.getId());
			req.setAttribute("thirdorderImportPage", thirdorderImport);
		}
		return new ModelAndView("admin/thirdorder/thirdorderImport");
	}
}
