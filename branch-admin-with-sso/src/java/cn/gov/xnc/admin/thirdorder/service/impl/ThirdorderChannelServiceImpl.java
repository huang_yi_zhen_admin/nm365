package cn.gov.xnc.admin.thirdorder.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.thirdorder.service.ThirdorderChannelServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("thirdorderChannelService")
@Transactional
public class ThirdorderChannelServiceImpl extends CommonServiceImpl implements ThirdorderChannelServiceI {
	
}