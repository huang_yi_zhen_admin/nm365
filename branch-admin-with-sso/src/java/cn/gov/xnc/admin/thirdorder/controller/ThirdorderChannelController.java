package cn.gov.xnc.admin.thirdorder.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderChannelEntity;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderChannelServiceI;

/**   
 * @Title: Controller
 * @Description: 第三方订单渠道管理
 * @author zero
 * @date 2017-07-21 17:35:09
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/thirdorderChannelController")
public class ThirdorderChannelController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ThirdorderChannelController.class);

	@Autowired
	private ThirdorderChannelServiceI thirdorderChannelService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 第三方订单渠道管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView thirdorderChannel( HttpServletRequest request) {
		
		
		
		return new ModelAndView("admin/thirdorder/thirdorderChannelList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ThirdorderChannelEntity thirdorderChannel,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ThirdorderChannelEntity.class, dataGrid);
		cq.eq("status", thirdorderChannel.STATUS_NORMAL);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, thirdorderChannel, request.getParameterMap());
		this.thirdorderChannelService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除第三方订单渠道管理
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ThirdorderChannelEntity thirdorderChannel, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		thirdorderChannel = systemService.getEntity(ThirdorderChannelEntity.class, thirdorderChannel.getId());
		thirdorderChannel.setStatus(ThirdorderChannelEntity.STATUS_DEL);
		message = "第三方订单渠道管理删除成功";
		thirdorderChannelService.updateEntitie(thirdorderChannel);
		//thirdorderChannelService.delete(thirdorderChannel);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加第三方订单渠道管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ThirdorderChannelEntity thirdorderChannel, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		thirdorderChannel.setStatus(ThirdorderChannelEntity.STATUS_NORMAL);
		
		if (StringUtil.isNotEmpty(thirdorderChannel.getId())) {
			message = "第三方订单渠道管理更新成功";
			ThirdorderChannelEntity t = thirdorderChannelService.get(ThirdorderChannelEntity.class, thirdorderChannel.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(thirdorderChannel, t);
				thirdorderChannelService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "第三方订单渠道管理更新失败";
			}
		} else {
			Map<String, Object> attributes = new HashMap<String, Object>();
			thirdorderChannel.setCreatetime(DateUtils.getDate());
			thirdorderChannel.setCreateuser(user);
			message = "第三方订单渠道管理添加成功";
			thirdorderChannelService.save(thirdorderChannel);
			attributes.put("id", thirdorderChannel.getId());
			attributes.put("channelname", thirdorderChannel.getChannelname());
			j.setAttributes(attributes);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 第三方订单渠道管理列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ThirdorderChannelEntity thirdorderChannel, HttpServletRequest req) {
		
		if( StringUtil.isNotEmpty(thirdorderChannel.getId())){
			ThirdorderChannelEntity channelPage = thirdorderChannelService.getEntity(ThirdorderChannelEntity.class, thirdorderChannel.getId());
			req.setAttribute("channelPage", channelPage);
		}else{
			ThirdorderChannelEntity channelPage = new ThirdorderChannelEntity();
			req.setAttribute("channelPage", channelPage);
		}
		
		return new ModelAndView("admin/thirdorder/thirdorderChannel");
	}
}
