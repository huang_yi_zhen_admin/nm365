package cn.gov.xnc.admin.express.controller;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.express.entity.CompanyExpressEntity;
import cn.gov.xnc.admin.express.entity.ExpressEntity;
import cn.gov.xnc.admin.express.service.ExpressServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.express.BaseRecvExpress;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.Commodity;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.express.impl.DBL_RecvExpress;
import cn.gov.xnc.system.core.express.impl.DBL_SendExpress;
import cn.gov.xnc.system.core.express.impl.EMS_RecvExpress;
import cn.gov.xnc.system.core.express.impl.EMS_SendExpress;
import cn.gov.xnc.system.core.express.impl.FAST_RecvExpress;
import cn.gov.xnc.system.core.express.impl.FAST_SendExpress;
import cn.gov.xnc.system.core.express.impl.HTKY_RecvExpress;
import cn.gov.xnc.system.core.express.impl.HTKY_SendExpress;
import cn.gov.xnc.system.core.express.impl.QFKD_RecvExpress;
import cn.gov.xnc.system.core.express.impl.QFKD_SendExpress;
import cn.gov.xnc.system.core.express.impl.SF_RecvExpress;
import cn.gov.xnc.system.core.express.impl.SF_SendExpress;
import cn.gov.xnc.system.core.express.impl.STO_RecvExpress;
import cn.gov.xnc.system.core.express.impl.STO_SendExpress;
import cn.gov.xnc.system.core.express.impl.UC_RecvExpress;
import cn.gov.xnc.system.core.express.impl.UC_SendExpress;
import cn.gov.xnc.system.core.express.impl.YD_RecvExpress;
import cn.gov.xnc.system.core.express.impl.YD_SendExpress;
import cn.gov.xnc.system.core.express.impl.YTO_RecvExpress;
import cn.gov.xnc.system.core.express.impl.YTO_SendExpress;
import cn.gov.xnc.system.core.express.impl.ZJS_RecvExpress;
import cn.gov.xnc.system.core.express.impl.ZJS_SendExpress;
import cn.gov.xnc.system.core.express.impl.ZTO_RecvExpress;
import cn.gov.xnc.system.core.express.impl.ZTO_SendExpress;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

import com.alibaba.fastjson.JSONObject;

/**   
 * @Title: Controller
 * @Description: 电子面单信息
 * @author zero
 * @date 2017-03-11 10:17:40
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/expressController")
public class ExpressController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ExpressController.class);

	@Autowired
	private ExpressServiceI expressService;
	@Autowired
	private SystemService systemService;
	private String message;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private DictionaryService dictionaryService;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * 运单打印测试首页
	 * 
	 * @return
	 */
	@RequestMapping(value = "index")
	public String index(HttpServletRequest req) {
		return "findmyfarm/express/index";
	}
	
	/**
	 * 接收快递鸟推送物流跟踪信息
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "acceptExpress")
	@ResponseBody
	public String acceptExpress(@RequestBody String requestBody, String RequestData,String RequestType,String DataSign){
		AjaxJson result = new AjaxJson();
		try {
			
			result = expressService.acceptExpress(RequestData);
			if(!result.isSuccess()){
				logger.debug("订单物流信息更新失败");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestBody;
	}
	
	/**
	 * 下载打印控件页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "downloadlodop")
	public String downloadlodop(HttpServletRequest req) {
		return "admin/express/downloadlodop";
	}
	
	/**
	 * 快捷快递运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "zjs")
	@ResponseBody
	public AjaxJson zjs( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		ZJS_SendExpress send = new ZJS_SendExpress();
		ZJS_RecvExpress recv = new ZJS_RecvExpress();
		
		//宅急送不支持测试环境，只能正式环境，可以不用面单账号
		//send.setCustomerName("testzjs");
		//send.setCustomerPwd("testzjspwd");
		
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(ZJS_SendExpress.ZJS_ExpType_1);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 快捷快递运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "fast")
	@ResponseBody
	public AjaxJson fast( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		FAST_SendExpress send = new FAST_SendExpress();
		FAST_RecvExpress recv = new FAST_RecvExpress();
		

		send.setCustomerName("testfast");
		
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(FAST_SendExpress.FAST_ExpType_1);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 德邦运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "dbl")
	@ResponseBody
	public AjaxJson dbl( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		DBL_SendExpress send = new DBL_SendExpress();
		DBL_RecvExpress recv = new DBL_RecvExpress();
		

		send.setCustomerName("testdbl");
		
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(DBL_SendExpress.DBL_ExpType_2);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 优速运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "uc")
	@ResponseBody
	public AjaxJson uc( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		UC_SendExpress send = new UC_SendExpress();
		UC_RecvExpress recv = new UC_RecvExpress();
		

		send.setCustomerName("testuc");
		send.setCustomerPwd("testucpwd");
		
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(UC_SendExpress.UC_ExpType_1);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * ems运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "ems")
	@ResponseBody
	public AjaxJson ems( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		EMS_SendExpress send = new EMS_SendExpress();
		EMS_RecvExpress recv = new EMS_RecvExpress();
		
		//send.setCustomerName("testems");
		//send.setCustomerPwd("testydems");
		
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(EMS_SendExpress.EMS_ExpType_1);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 韵达运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "yd")
	@ResponseBody
	public AjaxJson yd( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		YD_SendExpress send = new YD_SendExpress();
		YD_RecvExpress recv = new YD_RecvExpress();
		
		
		send.setCustomerName("testyd");
		send.setCustomerPwd("testydpwd");
		
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(YD_SendExpress.YD_ExpType_1);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 圆通运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "yto")
	@ResponseBody
	public AjaxJson yto( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		YTO_SendExpress send = new YTO_SendExpress();
		YTO_RecvExpress recv = new YTO_RecvExpress();
		
		
		send.setCustomerName("testyto");

		
		send.setMonthCode("testytomonthcode");
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(YTO_SendExpress.YTO_ExpType_2);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 中通运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "sto")
	@ResponseBody
	public AjaxJson sto( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		STO_SendExpress send = new STO_SendExpress();
		STO_RecvExpress recv = new STO_RecvExpress();
		
		
		send.setCustomerName("teststo");
		send.setCustomerPwd("teststopwd");
		send.setSendSite("teststosendsite");
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(STO_SendExpress.STO_ExpType_1);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 中通运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "zto")
	@ResponseBody
	public AjaxJson zto( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		ZTO_SendExpress send = new ZTO_SendExpress();
		ZTO_RecvExpress recv = new ZTO_RecvExpress();
		
		
		send.setCustomerName("testzto");
		send.setCustomerPwd("testztopwd");
		send.setOrderCode("1011");
		send.setPayType(1);
		send.setExpType(ZTO_SendExpress.ZTO_ExpType_3);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		send.setRemark("1005");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 百世快递运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "htky")
	@ResponseBody
	public AjaxJson htky( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		HTKY_SendExpress send = new HTKY_SendExpress();
		HTKY_RecvExpress recv = new HTKY_RecvExpress();
		
		//只能用正式环境
		//send.setCustomerName("星农场");
		//send.setCustomerPwd("123456");
		send.setOrderCode("10011");
		send.setPayType(1);
		send.setExpType(HTKY_SendExpress.HTKY_ExpType_1);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 顺丰运单打印
	 * 
	 * @return
	 */
	@RequestMapping(value = "sf")
	@ResponseBody
	public AjaxJson sf( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		SF_RecvExpress recv = new SF_RecvExpress();
		SF_SendExpress send = new SF_SendExpress();
		
		send.setMonthCode("1111");
		send.setOrderCode("1010");
		send.setPayType(1);
		send.setExpType(SF_SendExpress.SF_ExpType_10);
		send.setIsNotice(1);
		send.setReceiverName("张三");
		send.setReceiverMobile("18689872880");
		send.setReceiverProvinceName("广东省");
		send.setReceiverCityName("广州市");
		send.setReceiverAddress("南方花园电子面单测试");
		send.setSenderName("面单测试");
		send.setSenderMobile("15607693268");
		send.setSenderProvinceName("海南省");
		send.setSenderCityName("海口市");
		send.setSenderAddress("金都花园");
		
		Commodity c = new Commodity();
		c.setGoodsName("芒果");
		c.setGoodsquantity(1);
		c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		j.setObj(map);
		return j;
	}
	
	
	/*@RequestMapping(value = "getAllExpressPrintTemplate")
	@ResponseBody
	public AjaxJson getAllExpressPrintTemplate(  HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		String payBillsOrderId = request.getParameter("payBillsOrderId");
		String companyexpressid = request.getParameter("companyexpressid");
		TSUser user = ResourceUtil.getSessionUserName();
		
		//获取订单数据
		CriteriaQuery cq_o = new CriteriaQuery(OrderEntity.class);
		cq_o.eq("billsOrderID", payBillsOrderId);
		cq_o.eq("company", user.getCompany());
		List<OrderEntity> orderlist = systemService.getListByCriteriaQuery(cq_o,false);//获取全部商品列表
		
		
		
		return j;
	}*/
	
	
	/**
	 * 设置订单状态,临时修复工具，一次性使用
	 * */
	/*@RequestMapping(value = "setLogitisStat")
	@ResponseBody
	public AjaxJson setLogitisStat(  HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		int count = 0;
		String sql = "select a.id id,a.logisticsNumber logisticsNumber from xnc_order a where a.id in (select t.orderid from xnc_express t where t.success=1 and t.monthCode='8980922748' and t.update_time >= '2017-06-16 21:10:00' and t.update_time <= '2017-06-17 18:12:00'  GROUP BY t.orderid) ;";
		List<OrderEntity> orderlist = systemService.queryListByJdbc(sql,OrderEntity.class);
		System.out.println("---------- " + orderlist.size() + " -----------------");
		if( null != orderlist && orderlist.size() == 446 ){//一共446条
			for(OrderEntity o : orderlist){
				o = systemService.getEntity(OrderEntity.class, o.getId());
				String logistic = o.getLogisticsnumber();
				String[] logisticlist = logistic.split(",");
				for( int i = 0; i < logisticlist.length; i++ ){
					String tmpnum = logisticlist[i];
					if( StringUtil.isNotEmpty(tmpnum)){
						CriteriaQuery cq_e = new CriteriaQuery(ExpressEntity.class);
						cq_e.eq("orderid", o);
						cq_e.eq("logisticcode", tmpnum);
						List<ExpressEntity> expresslist = systemService.getListByCriteriaQuery(cq_e,false);
						if( null == expresslist || expresslist.size() != 1 ){
							j.setMsg("数据不正确");
							j.setSuccess(false);
							return j;
						}
						ExpressEntity express = expresslist.get(0);
						express.setSetorderlogistic(1);
						systemService.updateEntitie(express);
						System.out.println("正在设置第"+count+"个电子面单");
						count++;
						
					}
				}
			}
		}
		
		return j;
	}*/
	
	
	
	@RequestMapping(value = "orderExpressManage")
	public ModelAndView orderExpressManage(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		request.setAttribute("orderid", request.getParameter("id"));
		
		return new ModelAndView("admin/express/orderExpressManage");
	}
	
	
	/**
	 * 重新打印运单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getOrderExpressInfo")
	@ResponseBody
	public void getOrderExpressInfo(  HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		String orderid = request.getParameter("orderid");
		
		StringBuffer sql = new StringBuffer();
		sql.append("select id,shipperCode,logisticCode,update_time, expresstype from xnc_express ");
		sql.append(" where orderid='").append(orderid).append("' ");
		sql.append(" and setorderlogistic=1 ");
		
		List<ExpressEntity> expresslist = systemService.queryListByJdbc(sql.toString(),ExpressEntity.class);
		for( ExpressEntity express : expresslist){
			String shippercode = ExpressCompany.getExpressCompanyName(express.getShippercode());
			if( StringUtil.isNotEmpty(shippercode) ){
				express.setShippercode(shippercode);
			}
		}
		
		//根据分页构建分页list
		dataGrid.setTotal(expresslist.size());
		dataGrid.setResults(expresslist);
		
		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 重新打印运单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "reprintExpress")
	@ResponseBody
	public AjaxJson reprintExpress(  HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String orderid = request.getParameter("orderid");
		String expressids = request.getParameter("expressids");
		TSUser user = ResourceUtil.getSessionUserName();
		
		if( StringUtil.isEmpty(orderid) ){
			j.setMsg("订单id参数有误，请稍后再试");
			j.setSuccess(false);
			return j;
		}
		
		if( StringUtil.isEmpty(expressids) ){
			j.setMsg("面单id参数有误，请选择后再试");
			j.setSuccess(false);
			return j;
		}
		
		OrderEntity orderEntity = systemService.getEntity(CompanyExpressEntity.class, orderid);
		
		
		String[] expresslist = expressids.split(",");
		
		List<String> recvlist = new ArrayList<String>();
		
		CriteriaQuery cq_e = new CriteriaQuery(ExpressEntity.class);
		cq_e.eq("orderid", orderEntity);
//		cq_e.eq("success", 1);
//		cq_e.eq("setorderlogistic", 1);
		cq_e.in("id", expresslist);
		cq_e.addOrder("updateTime", SortDirection.desc);
		List<ExpressEntity> list = systemService.getListByCriteriaQuery(cq_e,false);//
		for( ExpressEntity e : list ){
			
			if(e.getExpresstype() == 2){
				//手动面单号，不给打印
				j.setSuccess(false);
				j.setMsg("手动录入的电子面单号，本系统暂不支持打印~");
				return j;
			}
			
			if(e.getSetorderlogistic() == 0){
				j.setSuccess(false);
				j.setMsg("手动录入的电子面单号，本系统暂不支持打印~");
				return j;
			}
			
			if( StringUtil.isNotEmpty(e.getRecvdata())){
				recvlist.add(e.getRecvdata());
			}
		}
		
		
		if( recvlist.size() > 0 ){
			j.setSuccess(true);
			j.setMsg("成功");
			j.setObj(recvlist);

		}else{
			j.setMsg("面单信息有误");
			j.setSuccess(false);
			
		}
		
		
		return j;
	}
	
	
	
	/**
	 * 重新打印运单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getOldPrintTemplate")
	@ResponseBody
	public AjaxJson getOldPrintTemplate(  HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String orderid = request.getParameter("orderid");
		TSUser user = ResourceUtil.getSessionUserName();
		//获取订单数据
		CriteriaQuery cq_o = new CriteriaQuery(OrderEntity.class);
		cq_o.eq("id", orderid);
		cq_o.eq("company", user.getCompany());
		//TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		List<OrderEntity> orderlist = systemService.getListByCriteriaQuery(cq_o,false);//获取全部商品列表
		if( null == orderlist || orderlist.size() != 1){
			j.setSuccess(false);
			j.setMsg("请确认该订单信息是否正确");
			return j;
		}
		//判断订单状态
		OrderEntity orderentity = orderlist.get(0);
		if( !"3".equals(orderentity.getState()) ){
			j.setSuccess(false);
			j.setMsg("只有处于已发货状态的订单才能重新打印运单");
			return j;
		}
		
		
		//ExpressEntity express = new ExpressEntity();
		//获取发货地址数据
		CriteriaQuery cq_e = new CriteriaQuery(ExpressEntity.class);
		cq_e.eq("orderid", orderentity.getId());
		cq_e.eq("success", 1);
		cq_e.addOrder("update_time", SortDirection.desc);
		List<ExpressEntity> expresslist = systemService.getListByCriteriaQuery(cq_e,false);//
		if( null == expresslist || expresslist.size() <= 1 ){
			j.setSuccess(false);
			j.setMsg("只有进行过面单打印的订单才能够重新打印面单");
			return j;
		}
		ExpressEntity express = expresslist.get(0);//取最后日期的一个最新的
		if(StringUtil.isEmpty(express.getPrinttemplate())){
			j.setSuccess(false);
			j.setMsg("只有进行过面单打印的订单才能够重新打印面单");
			return j;
		}
		
		j.setSuccess(true);
		j.setMsg("成功");
		Map<String,String> map = new HashMap<String,String>();
		map.put("printtemplate", express.getPrinttemplate());
		
		j.setObj(map);
		
		return j;
	}
	
	/**
	 * 打印快递模版
	 * @param orderids 订单
	 * @param companyexpressid 公司发货地址
	 * @return
	 */
	private AjaxJson printExpressTemplate(String orderids, String companyexpressid){
		boolean auth = false;
		AjaxJson result = new AjaxJson();
		
		List<String> recvlist = new ArrayList<String>();
		
		StringBuffer message = new StringBuffer();
		
		
		if(!StringUtil.isNotEmpty(orderids)){
			result.setMsg("请添加订单");
			result.setSuccess(auth);
			return result;
		}
		
		List<String> orderList = StringUtil.splitToList(",", orderids);
		if(!ListUtil.isNotEmpty(orderList)){
			result.setMsg("请添加订单");
			result.setSuccess(auth);
			return result;
		}
		
		//打印次数
		Integer printNum = 0;
		
		for(String orderId : orderList){
			
			OrderEntity orderEntity = systemService.findUniqueByProperty(OrderEntity.class, "id", orderId);
			
			//自提类型的不需要打面单，直接过滤掉
			if( "1".equals(orderEntity.getSendtype()) ){
				message.append("订单编号：").append(orderEntity.getIdentifieror()).append(" \n");
				message.append("属于自提类型，无需打印");
				result.setSuccess(auth);
				result.setMsg(message.toString());
				return result;
			}
			
			TSDictionary freightState = orderEntity.getFreightState();
			if(!StringUtil.isNotEmpty(freightState)){//订单状态为空的不能打印
				
				message.append("订单编号：").append(orderEntity.getIdentifieror()).append("状态不正确！\n");
				message.append("打印订单的前提条件，物流状态为：");
				message.append(OrderServiceI.FreightState.WAIT_PACK.getName()).append("、");
				message.append(OrderServiceI.FreightState.WAIT_DELIVER.getName()).append("、");
				message.append(OrderServiceI.FreightState.ALREADY_DELIVER.getName());
				result.setSuccess(auth);
				result.setMsg(message.toString());
				return result;
			}
		   
		    //商品数量
		    Integer number = orderEntity.getNumber();
		    if(number <= 0){
			   message.append("订单编号：").append(orderEntity.getIdentifieror()).append(" \n");
			   message.append("商品数量不能小于 1：");
			   result.setSuccess(auth);
			   result.setMsg(message.toString());
			   return result;
		    }
			
		   
		    boolean checkFreightState = false;
		    
			if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_PACK.getValue())){
				//待打包
				checkFreightState = true;
			}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_DELIVER.getValue())){
				//待发货
				checkFreightState = true;
			}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_DELIVER.getValue())){
				//已发货
				checkFreightState = true;
			}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.PART_DELIVER.getValue())){
				//部分发货
				checkFreightState = true;
			}
			
			if(!checkFreightState){
				message.append("订单编号：").append(orderEntity.getIdentifieror()).append("状态不正确！\n");
				message.append("打印订单的前提条件，物流状态为：");
				message.append(OrderServiceI.FreightState.WAIT_PACK.getName()).append("、");
				message.append(OrderServiceI.FreightState.WAIT_DELIVER.getName()).append("、");
				message.append(OrderServiceI.FreightState.PART_DELIVER.getName()).append("、");
				message.append(OrderServiceI.FreightState.ALREADY_DELIVER.getName());
				result.setSuccess(auth);
				result.setMsg(message.toString());
				return result;
			}
			
			//已发货的订单，如果已经有的物流单号数量大于等于货品数量也不允许打
			//订单里面已经有的物流单号数量
			
			//获取公司发货地址数据
			CompanyExpressEntity companyExpressEntity = systemService.getEntity(CompanyExpressEntity.class, companyexpressid);
			if(companyExpressEntity == null){
				message.append("订单编号：").append(orderEntity.getIdentifieror()).append(" \n");
				message.append("公司发货地址有误，请确认发货地址");
				result.setSuccess(auth);
				result.setMsg(message.toString());
				return result;
			}
			
			//快递下单
			BaseRecvExpress recv = null;
			BaseSendExpress send = null;
			if( ExpressCompany.DBL.equals(companyExpressEntity.getShippercode() )){
				recv = new DBL_RecvExpress();
				send = new DBL_SendExpress();
			}else if( ExpressCompany.EMS.equals(companyExpressEntity.getShippercode())){
				recv = new EMS_RecvExpress();
				send = new EMS_SendExpress();
			}else if( ExpressCompany.FAST.equals(companyExpressEntity.getShippercode())){
				recv = new FAST_RecvExpress();
				send = new FAST_SendExpress();
			}else if( ExpressCompany.HTKY.equals(companyExpressEntity.getShippercode())){
				recv = new HTKY_RecvExpress();
				send = new HTKY_SendExpress();
			}else if( ExpressCompany.QFKD.equals(companyExpressEntity.getShippercode())){
				recv = new QFKD_RecvExpress();
				send = new QFKD_SendExpress();
			}else if( ExpressCompany.SF.equals(companyExpressEntity.getShippercode())){
				recv = new SF_RecvExpress();
				send = new SF_SendExpress();
			}else if( ExpressCompany.STO.equals(companyExpressEntity.getShippercode())){
				recv = new STO_RecvExpress();
				send = new STO_SendExpress();
			}else if( ExpressCompany.UC.equals(companyExpressEntity.getShippercode())){
				recv = new UC_RecvExpress();
				send = new UC_SendExpress();
			}else if(ExpressCompany.YD.equals(companyExpressEntity.getShippercode()) ){
				recv = new YD_RecvExpress();
				send = new YD_SendExpress();
			}else if( ExpressCompany.YTO.equals(companyExpressEntity.getShippercode())){
				recv = new YTO_RecvExpress();
				send = new YTO_SendExpress();
			}else if( ExpressCompany.ZJS.equals(companyExpressEntity.getShippercode())){
				recv = new ZJS_RecvExpress();
				send = new ZJS_SendExpress();
			}else if ( ExpressCompany.ZTO.equals(companyExpressEntity.getShippercode())){
				recv = new ZTO_RecvExpress();
				send = new ZTO_SendExpress();
			}
			
			if( StringUtil.isNotEmpty(companyExpressEntity.getMonthcode() )){
				send.setMonthCode(companyExpressEntity.getMonthcode());
			}
			if( StringUtil.isNotEmpty(companyExpressEntity.getCustomername() )){
				send.setCustomerName(companyExpressEntity.getCustomername());
			}
			if( StringUtil.isNotEmpty(companyExpressEntity.getCustomerpwd())){
				send.setCustomerPwd(companyExpressEntity.getCustomerpwd());
			}
			if( StringUtil.isNotEmpty(companyExpressEntity.getSendsite())){
				send.setSendSite(companyExpressEntity.getSendsite());
			}
			
			send.setPayType(companyExpressEntity.getPaytype());
			send.setExpType(companyExpressEntity.getExptype());
			send.setIsNotice(1);//默认不通知快递员
			send.setCost(orderEntity.getFreightpic().doubleValue());
			send.setReceiverName(orderEntity.getCustomername());
			send.setReceiverMobile(orderEntity.getTelephone());
			if( StringUtil.isEmpty(orderEntity.getProvince()) || StringUtil.isEmpty(orderEntity.getCity() )){
				setTerritory(orderEntity);
			}
			send.setReceiverProvinceName(orderEntity.getProvince());
			send.setReceiverCityName(orderEntity.getCity());
			send.setReceiverExpAreaName(orderEntity.getArea());
			send.setReceiverAddress(orderEntity.getAddress());
			send.setSenderName(companyExpressEntity.getName());
			
			if( StringUtil.isNotEmpty(companyExpressEntity.getMobile()) ){
				send.setSenderMobile(companyExpressEntity.getMobile());
			}
			if( StringUtil.isNotEmpty(companyExpressEntity.getTel())){
				send.setSenderTel(companyExpressEntity.getTel());
			}
			
			send.setSenderProvinceName(companyExpressEntity.getProvince());
			send.setSenderCityName(companyExpressEntity.getCity());
			send.setSenderExpAreaName(companyExpressEntity.getArea());
			send.setSenderAddress(companyExpressEntity.getAddress());
			send.setSenderCompany(companyExpressEntity.getCompany().getCompanyName());
			
			ProductEntity product = orderEntity.getProductid();
			String specifications = "";
			if( null != product && product.getSpecifications() != null ){
				specifications = product.getSpecifications();
			}
			Commodity c = new Commodity();
			c.setGoodsName(orderEntity.getProductname()+"" +specifications);
			c.setGoodsquantity(1);
			//c.setGoodsWeight(1);
			send.addCommodity(c);
			send.setIsReturnPrintTemplate("1");//默认需要返回面单模板
			
			send.setOrderid(orderId);
			
			//获取该订单已经打印的面单数量
			CriteriaQuery cq_e = new CriteriaQuery(ExpressEntity.class);
			cq_e.eq("orderid", orderEntity);
			List<ExpressEntity> expresslist = systemService.getListByCriteriaQuery(cq_e,false);
			int startidx = 0;
			if( null != expresslist ){
				startidx = expresslist.size();
			}

			//订单物流单号
			Integer logisticNumberSize = 0;
			String logisticsNumber = StringUtil.formatNull(orderEntity.getLogisticsnumber());
			List<String> logisticsNumbers = StringUtil.isNotEmpty(logisticsNumber) ? StringUtil.splitToList(",",logisticsNumber) : null;
			if(ListUtil.isNotEmpty(logisticsNumbers)){
				logisticNumberSize = logisticsNumbers.size();
			}
		   
		    if(logisticNumberSize > number){
			    message.append("订单编号：").append(orderEntity.getIdentifieror()).append(" \n");
			    message.append("物流单号的数量：").append(logisticNumberSize).append(" 大于");
			    message.append("商品数量：").append(number).append("");
			    message.append("请调整后再操作打印！");
				result.setSuccess(auth);
				result.setMsg(message.toString());
				return result;
		    }
			
			Integer expnum = number - logisticNumberSize;
			if(expnum <= 0){
				message.append("订单编号：").append(orderEntity.getIdentifieror()).append(" \n");
			    message.append("打印面单次数已打完，如需打印可选重新打印");
				result.setSuccess(auth);
				result.setMsg(message.toString());
				return result;
			}
			
			for( int i = 0; i < expnum; i++ ){
				int pos = startidx+i+1000;//1000为测试用
				send.setOrderCode(""+orderEntity.getIdentifieror()+"("+pos+")");
				recv.doExpress(systemService,send);
				if( recv.isSuccess() ){
					recvlist.add(recv.getJsonstr());

				}
				result.setMsg(recv.getReason());
			}
			
			//更新打印次数
			printNum = orderEntity.getPrintNum();
			orderEntity.setPrintNum(++printNum);
			systemService.updateEntitie(orderEntity);
		}
		
		if(recvlist.size() <= 0 ){
			result.setSuccess(auth);
			if(!StringUtil.isNotEmpty(result.getMsg())){
				result.setMsg("系统打印有误，请联系管理员");
			}
			return result;
		}
		
		result.setSuccess(true);
		result.setMsg("处理完成");
		result.setObj(recvlist);
		return result;
	}
	
	
	/**
	 * 打印机打印完毕回调设置订单发货信息
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "setOrderLogistcs")
	@ResponseBody
	public AjaxJson setOrderLogistcs(  HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		String printtype = request.getParameter("printtype");//打印模式
		String shippercode = request.getParameter("shippercode");//快递编码
		if( StringUtil.isEmpty(shippercode)){
			j.setSuccess(false);
			j.setMsg("请确认回传shippercode是否正确");
			return j;
		}
		String logisticcode = request.getParameter("logisticcode");//电子面单号
		if( StringUtil.isEmpty(logisticcode)){
			j.setSuccess(false);
			j.setMsg("请确认回传logisticcode是否正确");
			return j;
		}
		String ordercode = request.getParameter("ordercode");//express表的ordercode，传给快递公司的订单号
		if( StringUtil.isEmpty(ordercode)){
			j.setSuccess(false);
			j.setMsg("请确认回传ordercode是否正确");
			return j;
		}
		try {
			j = expressService.setOrderLogistcs(shippercode, logisticcode, ordercode,printtype);
		} catch (Exception e) {
			e.printStackTrace();
			j.setSuccess(false);
			j.setMsg(e.getMessage());
		}
		
		System.out.println("打印回调：ordercode="+ordercode+" logisticcode="+logisticcode + " shippercode=" + shippercode);
		
		return j;
	}
	
	public String getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder) {
		String freightStatus = "1"; //未发货_1,已发货_2,部分发货_3
		Integer totalOrder = 1;
		Integer hasFreightOrderNum = 0;
		if(null != paybillsOrder){
			PayBillsOrderEntity payBills = systemService.findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
			List<OrderEntity> orderList = payBills.getOrderS();
			totalOrder = orderList.size();
			String isSend = null;
			for (OrderEntity orderEntity : orderList) {
				isSend = orderEntity.getState();//  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7
				if("3".equals(isSend) || "4".equals(isSend)){
					hasFreightOrderNum++;
				}
			}
			
		}
		if(totalOrder > 0){
			
			if( totalOrder == hasFreightOrderNum ){
				
				freightStatus = "2";
			}
			else if( hasFreightOrderNum > 0 && totalOrder > hasFreightOrderNum ){
				
				freightStatus = "3";
			}
		}
		
		return freightStatus;
	}
	
	//对处于未发货状态的订单进行快递下单
	@RequestMapping(value = "getMultiExpressPrintTemplate")
	@ResponseBody
	public AjaxJson getMultiExpressPrintTemplate(  HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		try {
			//订单
			String orderids = request.getParameter("orderids");
			if(!StringUtil.isNotEmpty(orderids)){
				result.setMsg("请添加订单");
				result.setSuccess(false);
				return result;
			}
			//公司发货地址
			String companyexpressid = request.getParameter("companyexpressid");
			if(!StringUtil.isNotEmpty(companyexpressid)){
				result.setMsg("请添加公司发货地址");
				result.setSuccess(false);
				return result;
			}
			
			//面单数量
//			Integer expressnum = ConvertTool.toInt(request.getParameter("expressnum"));
//			if(expressnum <= 0){
//				result.setMsg("请添加快递数量");
//				result.setSuccess(false);
//				return result;
//			}
		
			//打印快递模版
			result = printExpressTemplate(orderids, companyexpressid);
			
		} catch (Exception e) {
			result.setMsg("系统出错，请稍后再试");
			result.setSuccess(false);
			return result;
		}
		
		return result;
	}
	
	//对处于未发货状态的订单进行快递下单
	@RequestMapping(value = "getExpressPrintTemplate")
	@ResponseBody
	public AjaxJson getExpressPrintTemplate(  HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		String orderid = request.getParameter("orderid");
		String companyexpressid = request.getParameter("companyexpressid");
		//int expressnum = 1;
		
		//expressnum = Integer.parseInt(request.getParameter("expressnum"));
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		//获取订单数据
		CriteriaQuery cq_o = new CriteriaQuery(OrderEntity.class);
		cq_o.eq("id", orderid);
		cq_o.eq("company", user.getCompany());
		//TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		List<OrderEntity> orderlist = systemService.getListByCriteriaQuery(cq_o,false);//获取全部商品列表
		if( null == orderlist || orderlist.size() != 1){
			j.setSuccess(false);
			j.setMsg("请确认该订单信息是否正确");
			return j;
		}
		//判断订单状态
		OrderEntity orderentity = orderlist.get(0);
		if( !"2".equals(orderentity.getState()) ){
			j.setSuccess(false);
			j.setMsg("只有未发货状态的订单才能打印运单");
			return j;
		}
		
		//获取发货地址数据
		//CriteriaQuery cq_ce = new CriteriaQuery(CompanyExpressEntity.class);
		//cq_ce.eq("id", companyexpressid);
		CompanyExpressEntity companyExpressEntity = systemService.getEntity(CompanyExpressEntity.class, companyexpressid);
		if( null == companyExpressEntity ){
			j.setSuccess(false);
			j.setMsg("请确认该发货地址有效！");
			return j;
		}
		
		//快递下单
		BaseRecvExpress recv = null;
		BaseSendExpress send = null;
		if( ExpressCompany.DBL.equals(companyExpressEntity.getShippercode() )){
			recv = new DBL_RecvExpress();
			send = new DBL_SendExpress();
		}else if( ExpressCompany.EMS.equals(companyExpressEntity.getShippercode())){
			recv = new EMS_RecvExpress();
			send = new EMS_SendExpress();
		}else if( ExpressCompany.FAST.equals(companyExpressEntity.getShippercode())){
			recv = new FAST_RecvExpress();
			send = new FAST_SendExpress();
		}else if( ExpressCompany.HTKY.equals(companyExpressEntity.getShippercode())){
			recv = new HTKY_RecvExpress();
			send = new HTKY_SendExpress();
		}else if( ExpressCompany.QFKD.equals(companyExpressEntity.getShippercode())){
			recv = new QFKD_RecvExpress();
			send = new QFKD_SendExpress();
		}else if( ExpressCompany.SF.equals(companyExpressEntity.getShippercode())){
			recv = new SF_RecvExpress();
			send = new SF_SendExpress();
		}else if( ExpressCompany.STO.equals(companyExpressEntity.getShippercode())){
			recv = new STO_RecvExpress();
			send = new STO_SendExpress();
		}else if( ExpressCompany.UC.equals(companyExpressEntity.getShippercode())){
			recv = new UC_RecvExpress();
			send = new UC_SendExpress();
		}else if(ExpressCompany.YD.equals(companyExpressEntity.getShippercode()) ){
			recv = new YD_RecvExpress();
			send = new YD_SendExpress();
		}else if( ExpressCompany.YTO.equals(companyExpressEntity.getShippercode())){
			recv = new YTO_RecvExpress();
			send = new YTO_SendExpress();
		}else if( ExpressCompany.ZJS.equals(companyExpressEntity.getShippercode())){
			recv = new ZJS_RecvExpress();
			send = new ZJS_SendExpress();
		}else if ( ExpressCompany.ZTO.equals(companyExpressEntity.getShippercode())){
			recv = new ZTO_RecvExpress();
			send = new ZTO_SendExpress();
		}
		
		if( StringUtil.isNotEmpty(companyExpressEntity.getMonthcode() )){
			send.setMonthCode(companyExpressEntity.getMonthcode());
		}
		if( StringUtil.isNotEmpty(companyExpressEntity.getCustomername() )){
			send.setCustomerName(companyExpressEntity.getCustomername());
		}
		if( StringUtil.isNotEmpty(companyExpressEntity.getCustomerpwd())){
			send.setCustomerPwd(companyExpressEntity.getCustomerpwd());
		}
		send.setOrderCode(orderentity.getIdentifieror());
		send.setPayType(companyExpressEntity.getPaytype());
		send.setExpType(companyExpressEntity.getExptype());
		send.setIsNotice(1);//默认不通知快递员
		send.setCost(orderentity.getFreightpic().doubleValue());
		send.setReceiverName(orderentity.getCustomername());
		send.setReceiverMobile(orderentity.getTelephone());
		if( StringUtil.isEmpty(orderentity.getProvince()) ){
			setTerritory(orderentity);
		}
		send.setReceiverProvinceName(orderentity.getProvince());
		send.setReceiverCityName(orderentity.getCity());
		send.setReceiverExpAreaName(orderentity.getArea());
		send.setReceiverAddress(orderentity.getAddress());
		send.setSenderName(companyExpressEntity.getName());
		
		if( StringUtil.isNotEmpty(companyExpressEntity.getMobile()) ){
			send.setSenderMobile(companyExpressEntity.getMobile());
		}
		if( StringUtil.isNotEmpty(companyExpressEntity.getTel())){
			send.setSenderTel(companyExpressEntity.getTel());
		}
		
		send.setSenderProvinceName(companyExpressEntity.getProvince());
		send.setSenderCityName(companyExpressEntity.getCity());
		send.setSenderExpAreaName(companyExpressEntity.getArea());
		send.setSenderAddress(companyExpressEntity.getAddress());
		send.setSenderCompany(companyExpressEntity.getCompany().getCompanyName());
		
		Commodity c = new Commodity();
		c.setGoodsName(orderentity.getProductname());
		c.setGoodsquantity(orderentity.getNumber());
		//c.setGoodsWeight(1);
		send.addCommodity(c);
		send.setIsReturnPrintTemplate("1");//默认需要返回面单模板
		
		recv.doExpress(systemService,send);
		
		j.setSuccess(recv.isSuccess());
		j.setMsg(recv.getReason());
		Map<String,String> map = new HashMap<String,String>();
		map.put("recv", recv.getJsonstr());
		System.out.println(recv.getJsonstr());
		j.setObj(map);
		
		
		if( recv.isSuccess() ){
		//设置订单状态
			orderentity.setState("3");//设置为发货状态
			orderentity.setLogisticscompany(ExpressCompany.getExpressCompanyName(companyExpressEntity.getShippercode()));
			orderentity.setLogisticsnumber(recv.getOrder().getLogisticCode());
			systemService.save(orderentity);
		}
		
		
		return j;
		
	}
	
	private void setTerritory(OrderEntity orderentity){
		

		CriteriaQuery cq_t = new CriteriaQuery(TSTerritory.class);
		cq_t.eq("territoryLevel", Short.parseShort("0"));
		List<TSTerritory> provincelist = systemService.getListByCriteriaQuery(cq_t,false);//
		TSTerritory provinceTerritory = null;
		for( int i = 0; i < provincelist.size(); i++ ){
			if( orderentity.getAddress().startsWith(provincelist.get(i).getTerritoryName().substring(0, 2)) ){
				provinceTerritory = provincelist.get(i);
				String province = provinceTerritory.getTerritoryName();
				if( StringUtil.isEmpty(orderentity.getProvince() )){
					orderentity.setProvince(province);
				}
				
				break;
			}
		}

		
		CriteriaQuery cq_c = new CriteriaQuery(TSTerritory.class);
		cq_c.eq("TSTerritory", provinceTerritory);
		List<TSTerritory> citylist = systemService.getListByCriteriaQuery(cq_c,false);//
		TSTerritory cityTerritory = null;
		for( int i = 0; i < citylist.size(); i++ ){
			if( citylist.get(i).getTerritoryName().length() >= 2){
				if( orderentity.getAddress().contains(citylist.get(i).getTerritoryName().substring(0, 2) )){
					cityTerritory = citylist.get(i);
					String city = cityTerritory.getTerritoryName();
					if( StringUtil.isEmpty(orderentity.getCity() )){
						orderentity.setCity(city);
					}
					break;
				}
			}
		}
		
		if( StringUtil.isEmpty( orderentity.getCity() ) ){
			for( int i = 0; i < citylist.size(); i++ ){
				if( orderentity.getAddress().contains(citylist.get(i).getTerritoryName() )){
					cityTerritory = citylist.get(i);
					String city = cityTerritory.getTerritoryName();
					if( StringUtil.isEmpty(orderentity.getCity() )){
						orderentity.setCity(city);
					}
					break;
				}
			}
		}
		
		if( StringUtil.isEmpty(orderentity.getCity()) ){
			orderentity.setCity("市辖区");
		}
		
		CriteriaQuery cq_a = new CriteriaQuery(TSTerritory.class);
		cq_a.eq("TSTerritory", cityTerritory);
		List<TSTerritory> arealist = systemService.getListByCriteriaQuery(cq_a,false);//
		TSTerritory areaTerritory = null;
		for( int i = 0; i < arealist.size(); i++ ){
			if( orderentity.getAddress().contains(arealist.get(i).getTerritoryName())){
				areaTerritory = arealist.get(i);
				String area = areaTerritory.getTerritoryName();
				if( StringUtil.isEmpty(orderentity.getArea() )){
					orderentity.setArea(area);
				}
				break;
			}
		}
		
	}


	/**
	 * 电子面单信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView express(HttpServletRequest request) {
		return new ModelAndView("cn/gov/xnc/admin/express/jsp/expressList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(ExpressEntity express,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, express, request.getParameterMap());
		this.expressService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除电子面单信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(ExpressEntity express, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		express = systemService.getEntity(ExpressEntity.class, express.getId());
		message = "电子面单信息删除成功";
		expressService.delete(express);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加电子面单信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ExpressEntity express, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(express.getId())) {
			message = "电子面单信息更新成功";
			ExpressEntity t = expressService.get(ExpressEntity.class, express.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(express, t);
				expressService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "电子面单信息更新失败";
			}
		} else {
			message = "电子面单信息添加成功";
			expressService.save(express);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 电子面单信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ExpressEntity express, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(express.getId())) {
			express = expressService.getEntity(ExpressEntity.class, express.getId());
			req.setAttribute("expressPage", express);
		}
		return new ModelAndView("cn/gov/xnc/admin/express/jsp/express");
	}
}
