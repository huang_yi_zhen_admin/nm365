package cn.gov.xnc.admin.express.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.order.entity.OrderEntity;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 电子面单信息
 * @author zero
 * @date 2017-03-11 10:17:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_express", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ExpressEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**用户自定义回调信息*/
	private java.lang.String callback;
	/**会员标识平台方与快递鸟统一用户标识的商家ID*/
	private java.lang.String memberid;
	/**电子面单客户账号*/
	private java.lang.String customername;
	/**电子面单密码*/
	private java.lang.String customerpwd;
	/**收件网点标识*/
	private java.lang.String sendsite;
	/**快递公司编码*/
	private java.lang.String shippercode;
	/**快递单号*/
	private java.lang.String logisticcode;
	/**第三方订单号*/
	private java.lang.String thrordercode;
	/**订单号*/
	private java.lang.String ordercode;
	/**月结编码*/
	private java.lang.String monthcode;
	/**邮费支付方式:*/
	private java.lang.Integer paytype;
	/**快递类型：1-标准快件*/
	private java.lang.Integer exptype;
	/**是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0*/
	private java.lang.Integer isnotice;
	/**寄件费运费*/
	private BigDecimal cost;
	/**其他费用*/
	private BigDecimal othercost;
	/**收件人公司*/
	private java.lang.String receivercompany;
	/**收件人姓名*/
	private java.lang.String receivername;
	/**电话与手机，必填一个*/
	private java.lang.String receivertel;
	/**电话与手机，必填一个*/
	private java.lang.String receivermobile;
	/**收件人邮编*/
	private java.lang.String receiverpostcode;
	/**收件省（如广东省，不要缺少“省”）*/
	private java.lang.String receiverprovincename;
	/**收件市（如深圳市，不要缺少“市”）*/
	private java.lang.String receivercityname;
	/**收件区（如福田区，不要缺少“区”或“县”）*/
	private java.lang.String receiverexpareaname;
	/**收件人详细地址*/
	private java.lang.String receiveraddress;
	/**发件人公司*/
	private java.lang.String sendercompany;
	/**发件人姓名*/
	private java.lang.String sendername;
	/**电话与手机，必填一个*/
	private java.lang.String sendertel;
	/**电话与手机，必填一个*/
	private java.lang.String sendermobile;
	/**发件人邮编*/
	private java.lang.String senderpostcode;
	/**发件省（如广东省，不要缺少“省”）*/
	private java.lang.String senderprovincename;
	/**发件市（如深圳市，不要缺少“市”）*/
	private java.lang.String sendercityname;
	/**发件区（如福田区，不要缺少“区”或“县”）*/
	private java.lang.String senderexpareaname;
	/**发件详细地址*/
	private java.lang.String senderaddress;
	/**上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同*/
	private java.util.Date startdate;
	/**上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同*/
	private java.util.Date enddate;
	/**物品总重量kg*/
	private BigDecimal weight;
	/**件数/包裹数*/
	private java.lang.Integer quantity;
	/**物品总体积m3*/
	private BigDecimal volume;
	/**备注*/
	private java.lang.String remark;
	/**增值服务json*/
	private java.lang.String addservice;
	/**商品信息json*/
	private java.lang.String commodity;
	/**返回电子面单模板：0-不需要；1-需要*/
	private java.lang.Integer isreturnprinttemplate;
	/**订单数据json*/
	private java.lang.String order;
	/**是否成功*/
	private java.lang.Integer success;
	/**错误编码*/
	private java.lang.String resultcode;
	/**错误原因*/
	private java.lang.String reason;
	/**唯一标识*/
	private java.lang.String uniquerrequestnumber;
	/**面单打印模板*/
	private java.lang.String printtemplate;
	/**订单预计到货时间yyyy-mm-dd*/
	private java.util.Date estimateddeliverytime;
	/**子单数量*/
	private java.lang.String subcount;
	/**子单号*/
	private java.lang.String suborders;
	/**子单模板*/
	private java.lang.String subprinttemplates;
	/**收件人安全电话*/
	private java.lang.String receiversafephone;
	/**寄件人安全电话*/
	private java.lang.String sendersafephone;
	/**拨号页面网址（转换成二维码可扫描拨号）*/
	private java.lang.String dialpage;
	/**发送信息json*/
	private java.lang.String senddata;
	/**接收信息json*/
	private java.lang.String recvdata;
	/**order表关联id*/
	private OrderEntity orderid;
	/**是否已经设置回去订单物流信息*/
	private java.lang.Integer setorderlogistic;
	/**是否是系统自动打印的电子面单，1为电子面单，2为手工录入面单*/
	private java.lang.Integer expresstype;
	
	/**创建日期*/
	private Date updateTime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户自定义回调信息
	 */
	@Column(name ="CALLBACK",nullable=true,length=255)
	public java.lang.String getCallback(){
		return this.callback;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户自定义回调信息
	 */
	public void setCallback(java.lang.String callback){
		this.callback = callback;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  会员标识平台方与快递鸟统一用户标识的商家ID
	 */
	@Column(name ="MEMBERID",nullable=true,length=32)
	public java.lang.String getMemberid(){
		return this.memberid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  会员标识平台方与快递鸟统一用户标识的商家ID
	 */
	public void setMemberid(java.lang.String memberid){
		this.memberid = memberid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电子面单客户账号
	 */
	@Column(name ="CUSTOMERNAME",nullable=true,length=32)
	public java.lang.String getCustomername(){
		return this.customername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电子面单客户账号
	 */
	public void setCustomername(java.lang.String customername){
		this.customername = customername;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电子面单密码
	 */
	@Column(name ="CUSTOMERPWD",nullable=true,length=32)
	public java.lang.String getCustomerpwd(){
		return this.customerpwd;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电子面单密码
	 */
	public void setCustomerpwd(java.lang.String customerpwd){
		this.customerpwd = customerpwd;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件网点标识
	 */
	@Column(name ="SENDSITE",nullable=true,length=255)
	public java.lang.String getSendsite(){
		return this.sendsite;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件网点标识
	 */
	public void setSendsite(java.lang.String sendsite){
		this.sendsite = sendsite;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递公司编码
	 */
	@Column(name ="SHIPPERCODE",nullable=true,length=16)
	public java.lang.String getShippercode(){
		return this.shippercode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递公司编码
	 */
	public void setShippercode(java.lang.String shippercode){
		this.shippercode = shippercode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递单号
	 */
	@Column(name ="LOGISTICCODE",nullable=true,length=255)
	public java.lang.String getLogisticcode(){
		return this.logisticcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递单号
	 */
	public void setLogisticcode(java.lang.String logisticcode){
		this.logisticcode = logisticcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  第三方订单号
	 */
	@Column(name ="THRORDERCODE",nullable=true,length=255)
	public java.lang.String getThrordercode(){
		return this.thrordercode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  第三方订单号
	 */
	public void setThrordercode(java.lang.String thrordercode){
		this.thrordercode = thrordercode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单号
	 */
	@Column(name ="ORDERCODE",nullable=true,length=64)
	public java.lang.String getOrdercode(){
		return this.ordercode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单号
	 */
	public void setOrdercode(java.lang.String ordercode){
		this.ordercode = ordercode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  月结编码
	 */
	@Column(name ="MONTHCODE",nullable=true,length=64)
	public java.lang.String getMonthcode(){
		return this.monthcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  月结编码
	 */
	public void setMonthcode(java.lang.String monthcode){
		this.monthcode = monthcode;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  邮费支付方式:
	 */
	@Column(name ="PAYTYPE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getPaytype(){
		return this.paytype;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  邮费支付方式:
	 */
	public void setPaytype(java.lang.Integer paytype){
		this.paytype = paytype;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  快递类型：1-标准快件
	 */
	@Column(name ="EXPTYPE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getExptype(){
		return this.exptype;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  快递类型：1-标准快件
	 */
	public void setExptype(java.lang.Integer exptype){
		this.exptype = exptype;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 */
	@Column(name ="ISNOTICE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getIsnotice(){
		return this.isnotice;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 */
	public void setIsnotice(java.lang.Integer isnotice){
		this.isnotice = isnotice;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  寄件费运费
	 */
	@Column(name ="COST",nullable=true,precision=20,scale=5)
	public BigDecimal getCost(){
		return this.cost;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  寄件费运费
	 */
	public void setCost(BigDecimal cost){
		this.cost = cost;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  其他费用
	 */
	@Column(name ="OTHERCOST",nullable=true,precision=20,scale=5)
	public BigDecimal getOthercost(){
		return this.othercost;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  其他费用
	 */
	public void setOthercost(BigDecimal othercost){
		this.othercost = othercost;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件人公司
	 */
	@Column(name ="RECEIVERCOMPANY",nullable=true,length=255)
	public java.lang.String getReceivercompany(){
		return this.receivercompany;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件人公司
	 */
	public void setReceivercompany(java.lang.String receivercompany){
		this.receivercompany = receivercompany;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件人姓名
	 */
	@Column(name ="RECEIVERNAME",nullable=true,length=255)
	public java.lang.String getReceivername(){
		return this.receivername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件人姓名
	 */
	public void setReceivername(java.lang.String receivername){
		this.receivername = receivername;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电话与手机，必填一个
	 */
	@Column(name ="RECEIVERTEL",nullable=true,length=32)
	public java.lang.String getReceivertel(){
		return this.receivertel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电话与手机，必填一个
	 */
	public void setReceivertel(java.lang.String receivertel){
		this.receivertel = receivertel;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电话与手机，必填一个
	 */
	@Column(name ="RECEIVERMOBILE",nullable=true,length=32)
	public java.lang.String getReceivermobile(){
		return this.receivermobile;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电话与手机，必填一个
	 */
	public void setReceivermobile(java.lang.String receivermobile){
		this.receivermobile = receivermobile;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件人邮编
	 */
	@Column(name ="RECEIVERPOSTCODE",nullable=true,length=16)
	public java.lang.String getReceiverpostcode(){
		return this.receiverpostcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件人邮编
	 */
	public void setReceiverpostcode(java.lang.String receiverpostcode){
		this.receiverpostcode = receiverpostcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件省（如广东省，不要缺少“省”）
	 */
	@Column(name ="RECEIVERPROVINCENAME",nullable=true,length=32)
	public java.lang.String getReceiverprovincename(){
		return this.receiverprovincename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件省（如广东省，不要缺少“省”）
	 */
	public void setReceiverprovincename(java.lang.String receiverprovincename){
		this.receiverprovincename = receiverprovincename;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件市（如深圳市，不要缺少“市”）
	 */
	@Column(name ="RECEIVERCITYNAME",nullable=true,length=32)
	public java.lang.String getReceivercityname(){
		return this.receivercityname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件市（如深圳市，不要缺少“市”）
	 */
	public void setReceivercityname(java.lang.String receivercityname){
		this.receivercityname = receivercityname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件区（如福田区，不要缺少“区”或“县”）
	 */
	@Column(name ="RECEIVEREXPAREANAME",nullable=true,length=32)
	public java.lang.String getReceiverexpareaname(){
		return this.receiverexpareaname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件区（如福田区，不要缺少“区”或“县”）
	 */
	public void setReceiverexpareaname(java.lang.String receiverexpareaname){
		this.receiverexpareaname = receiverexpareaname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件人详细地址
	 */
	@Column(name ="RECEIVERADDRESS",nullable=true,length=255)
	public java.lang.String getReceiveraddress(){
		return this.receiveraddress;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件人详细地址
	 */
	public void setReceiveraddress(java.lang.String receiveraddress){
		this.receiveraddress = receiveraddress;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件人公司
	 */
	@Column(name ="SENDERCOMPANY",nullable=true,length=255)
	public java.lang.String getSendercompany(){
		return this.sendercompany;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件人公司
	 */
	public void setSendercompany(java.lang.String sendercompany){
		this.sendercompany = sendercompany;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件人姓名
	 */
	@Column(name ="SENDERNAME",nullable=true,length=255)
	public java.lang.String getSendername(){
		return this.sendername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件人姓名
	 */
	public void setSendername(java.lang.String sendername){
		this.sendername = sendername;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电话与手机，必填一个
	 */
	@Column(name ="SENDERTEL",nullable=true,length=32)
	public java.lang.String getSendertel(){
		return this.sendertel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电话与手机，必填一个
	 */
	public void setSendertel(java.lang.String sendertel){
		this.sendertel = sendertel;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电话与手机，必填一个
	 */
	@Column(name ="SENDERMOBILE",nullable=true,length=32)
	public java.lang.String getSendermobile(){
		return this.sendermobile;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电话与手机，必填一个
	 */
	public void setSendermobile(java.lang.String sendermobile){
		this.sendermobile = sendermobile;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件人邮编
	 */
	@Column(name ="SENDERPOSTCODE",nullable=true,length=16)
	public java.lang.String getSenderpostcode(){
		return this.senderpostcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件人邮编
	 */
	public void setSenderpostcode(java.lang.String senderpostcode){
		this.senderpostcode = senderpostcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件省（如广东省，不要缺少“省”）
	 */
	@Column(name ="SENDERPROVINCENAME",nullable=true,length=32)
	public java.lang.String getSenderprovincename(){
		return this.senderprovincename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件省（如广东省，不要缺少“省”）
	 */
	public void setSenderprovincename(java.lang.String senderprovincename){
		this.senderprovincename = senderprovincename;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件市（如深圳市，不要缺少“市”）
	 */
	@Column(name ="SENDERCITYNAME",nullable=true,length=32)
	public java.lang.String getSendercityname(){
		return this.sendercityname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件市（如深圳市，不要缺少“市”）
	 */
	public void setSendercityname(java.lang.String sendercityname){
		this.sendercityname = sendercityname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件区（如福田区，不要缺少“区”或“县”）
	 */
	@Column(name ="SENDEREXPAREANAME",nullable=true,length=32)
	public java.lang.String getSenderexpareaname(){
		return this.senderexpareaname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件区（如福田区，不要缺少“区”或“县”）
	 */
	public void setSenderexpareaname(java.lang.String senderexpareaname){
		this.senderexpareaname = senderexpareaname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件详细地址
	 */
	@Column(name ="SENDERADDRESS",nullable=true,length=255)
	public java.lang.String getSenderaddress(){
		return this.senderaddress;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件详细地址
	 */
	public void setSenderaddress(java.lang.String senderaddress){
		this.senderaddress = senderaddress;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 */
	@Column(name ="STARTDATE",nullable=true)
	public java.util.Date getStartdate(){
		return this.startdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 */
	public void setStartdate(java.util.Date startdate){
		this.startdate = startdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 */
	@Column(name ="ENDDATE",nullable=true)
	public java.util.Date getEnddate(){
		return this.enddate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 */
	public void setEnddate(java.util.Date enddate){
		this.enddate = enddate;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  物品总重量kg
	 */
	@Column(name ="WEIGHT",nullable=true,precision=15,scale=5)
	public BigDecimal getWeight(){
		return this.weight;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  物品总重量kg
	 */
	public void setWeight(BigDecimal weight){
		this.weight = weight;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  件数/包裹数
	 */
	@Column(name ="QUANTITY",nullable=true,precision=10,scale=0)
	public java.lang.Integer getQuantity(){
		return this.quantity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  件数/包裹数
	 */
	public void setQuantity(java.lang.Integer quantity){
		this.quantity = quantity;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  物品总体积m3
	 */
	@Column(name ="VOLUME",nullable=true,precision=10,scale=2)
	public BigDecimal getVolume(){
		return this.volume;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  物品总体积m3
	 */
	public void setVolume(BigDecimal volume){
		this.volume = volume;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=255)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  增值服务json
	 */
	@Column(name ="ADDSERVICE",nullable=true,length=5000)
	public java.lang.String getAddservice(){
		return this.addservice;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  增值服务json
	 */
	public void setAddservice(java.lang.String addservice){
		this.addservice = addservice;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品信息json
	 */
	@Column(name ="COMMODITY",nullable=true,length=5000)
	public java.lang.String getCommodity(){
		return this.commodity;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品信息json
	 */
	public void setCommodity(java.lang.String commodity){
		this.commodity = commodity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  返回电子面单模板：0-不需要；1-需要
	 */
	@Column(name ="ISRETURNPRINTTEMPLATE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getIsreturnprinttemplate(){
		return this.isreturnprinttemplate;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  返回电子面单模板：0-不需要；1-需要
	 */
	public void setIsreturnprinttemplate(java.lang.Integer isreturnprinttemplate){
		this.isreturnprinttemplate = isreturnprinttemplate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单数据json
	 */
	@Column(name ="ORDERJSON",nullable=true,length=5000)
	public java.lang.String getOrder(){
		return this.order;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单数据json
	 */
	public void setOrder(java.lang.String order){
		this.order = order;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  是否成功
	 */
	@Column(name ="SUCCESS",nullable=true,precision=10,scale=0)
	public java.lang.Integer getSuccess(){
		return this.success;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  是否成功
	 */
	public void setSuccess(java.lang.Integer success){
		this.success = success;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  错误编码
	 */
	@Column(name ="RESULTCODE",nullable=true,length=32)
	public java.lang.String getResultcode(){
		return this.resultcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  错误编码
	 */
	public void setResultcode(java.lang.String resultcode){
		this.resultcode = resultcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  错误原因
	 */
	@Column(name ="REASON",nullable=true,length=255)
	public java.lang.String getReason(){
		return this.reason;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  错误原因
	 */
	public void setReason(java.lang.String reason){
		this.reason = reason;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  唯一标识
	 */
	@Column(name ="UNIQUERREQUESTNUMBER",nullable=true,length=255)
	public java.lang.String getUniquerrequestnumber(){
		return this.uniquerrequestnumber;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  唯一标识
	 */
	public void setUniquerrequestnumber(java.lang.String uniquerrequestnumber){
		this.uniquerrequestnumber = uniquerrequestnumber;
	}
	/**
	 *方法: 取得java.lang.Object
	 *@return: java.lang.Object  面单打印模板
	 */
	@Column(name ="PRINTTEMPLATE",nullable=true,length=65535)
	public java.lang.String getPrinttemplate(){
		return this.printtemplate;
	}

	/**
	 *方法: 设置java.lang.Object
	 *@param: java.lang.Object  面单打印模板
	 */
	public void setPrinttemplate(java.lang.String printtemplate){
		this.printtemplate = printtemplate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  订单预计到货时间yyyy-mm-dd
	 */
	@Column(name ="ESTIMATEDDELIVERYTIME",nullable=true)
	public java.util.Date getEstimateddeliverytime(){
		return this.estimateddeliverytime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  订单预计到货时间yyyy-mm-dd
	 */
	public void setEstimateddeliverytime(java.util.Date estimateddeliverytime){
		this.estimateddeliverytime = estimateddeliverytime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  子单数量
	 */
	@Column(name ="SUBCOUNT",nullable=true,length=255)
	public java.lang.String getSubcount(){
		return this.subcount;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  子单数量
	 */
	public void setSubcount(java.lang.String subcount){
		this.subcount = subcount;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  子单号
	 */
	@Column(name ="SUBORDERS",nullable=true,length=255)
	public java.lang.String getSuborders(){
		return this.suborders;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  子单号
	 */
	public void setSuborders(java.lang.String suborders){
		this.suborders = suborders;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  子单模板
	 */
	@Column(name ="SUBPRINTTEMPLATES",nullable=true,length=255)
	public java.lang.String getSubprinttemplates(){
		return this.subprinttemplates;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  子单模板
	 */
	public void setSubprinttemplates(java.lang.String subprinttemplates){
		this.subprinttemplates = subprinttemplates;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收件人安全电话
	 */
	@Column(name ="RECEIVERSAFEPHONE",nullable=true,length=32)
	public java.lang.String getReceiversafephone(){
		return this.receiversafephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收件人安全电话
	 */
	public void setReceiversafephone(java.lang.String receiversafephone){
		this.receiversafephone = receiversafephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  寄件人安全电话
	 */
	@Column(name ="SENDERSAFEPHONE",nullable=true,length=32)
	public java.lang.String getSendersafephone(){
		return this.sendersafephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  寄件人安全电话
	 */
	public void setSendersafephone(java.lang.String sendersafephone){
		this.sendersafephone = sendersafephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  拨号页面网址（转换成二维码可扫描拨号）
	 */
	@Column(name ="DIALPAGE",nullable=true,length=255)
	public java.lang.String getDialpage(){
		return this.dialpage;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  拨号页面网址（转换成二维码可扫描拨号）
	 */
	public void setDialpage(java.lang.String dialpage){
		this.dialpage = dialpage;
	}
	/**
	 *方法: 取得java.lang.Object
	 *@return: java.lang.Object  发送信息json
	 */
	@Column(name ="SENDDATA",nullable=true,length=65535)
	public java.lang.String getSenddata(){
		return this.senddata;
	}

	/**
	 *方法: 设置java.lang.Object
	 *@param: java.lang.Object  发送信息json
	 */
	public void setSenddata(java.lang.String senddata){
		this.senddata = senddata;
	}
	/**
	 *方法: 取得java.lang.Object
	 *@return: java.lang.Object  接收信息json
	 */
	@Column(name ="RECVDATA",nullable=true,length=65535)
	public java.lang.String getRecvdata(){
		return this.recvdata;
	}

	/**
	 *方法: 设置java.lang.Object
	 *@param: java.lang.Object  接收信息json
	 */
	public void setRecvdata(java.lang.String recvdata){
		this.recvdata = recvdata;
	}

	/**
	 * @return the update_time
	 */
	@Column(name ="update_time",nullable=true)
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param update_time the update_time to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	/**
	 *方法: 取得java.lang.Object
	 *@return: java.lang.Object  发送信息json
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDERID")
	public OrderEntity getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.Object
	 *@param: java.lang.Object  发送信息json
	 */
	public void setOrderid(OrderEntity orderid){
		this.orderid = orderid;
	}

	/**
	 * @return the setorderlogistic
	 */
	@Column(name ="SETORDERLOGISTIC",nullable=true,precision=10,scale=0)
	public java.lang.Integer getSetorderlogistic() {
		return setorderlogistic;
	}

	/**
	 * @param setorderlogistic the setorderlogistic to set
	 */
	public void setSetorderlogistic(java.lang.Integer setorderlogistic) {
		this.setorderlogistic = setorderlogistic;
	}

	/**
	 * @return the expresstype
	 */
	@Column(name ="EXPRESSTYPE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getExpresstype() {
		return expresstype;
	}

	/**
	 * @param expresstype the expresstype to set
	 */
	public void setExpresstype(java.lang.Integer expresstype) {
		this.expresstype = expresstype;
	}
}
