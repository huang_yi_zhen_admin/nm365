package cn.gov.xnc.admin.copartner.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 商业合作伙伴提现账户
 * @author zero
 * @date 2017-01-18 14:21:53
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_copartner_withdraw_account", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class UserCopartnerAccountEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**合作伙伴用户id*/
	private TSUser usercopartnerid;
	/**账户总额*/
	private BigDecimal totalmoney;
	/**账户冻结额度*/
	private BigDecimal frozenmoney;
	/**账户可用额度*/
	private BigDecimal usablemoney;
	/**createtime*/
	private java.util.Date createtime;
	/**updatetime*/
	private java.util.Date updatetime;
	/**账户状态 1 正常 2 锁定*/
	private java.lang.String status;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作伙伴用户id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERCOPARTNERID")
	public TSUser getUsercopartnerid(){
		return this.usercopartnerid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作伙伴用户id
	 */
	public void setUsercopartnerid(TSUser usercopartnerid){
		this.usercopartnerid = usercopartnerid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  账户总额
	 */
	@Column(name ="TOTALMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getTotalmoney(){
		return this.totalmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  账户总额
	 */
	public void setTotalmoney(BigDecimal totalmoney){
		this.totalmoney = totalmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  账户冻结额度
	 */
	@Column(name ="FROZENMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getFrozenmoney(){
		return this.frozenmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  账户冻结额度
	 */
	public void setFrozenmoney(BigDecimal frozenmoney){
		this.frozenmoney = frozenmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  账户可用额度
	 */
	@Column(name ="USABLEMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getUsablemoney(){
		return this.usablemoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  账户可用额度
	 */
	public void setUsablemoney(BigDecimal usablemoney){
		this.usablemoney = usablemoney;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  createtime
	 */
	@Column(name ="CREATETIME",nullable=true)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  createtime
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  updatetime
	 */
	@Column(name ="UPDATETIME",nullable=true)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  updatetime
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  账户状态 1 正常 2 锁定
	 */
	@Column(name ="STATUS",nullable=true,length=2)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  账户状态 1 正常 2 锁定
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
}
