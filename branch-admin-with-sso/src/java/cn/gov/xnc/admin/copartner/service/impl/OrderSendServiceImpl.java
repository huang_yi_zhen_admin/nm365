package cn.gov.xnc.admin.copartner.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerEntity;
import cn.gov.xnc.admin.copartner.service.OrderSendServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("orderSendService")
@Transactional
public class OrderSendServiceImpl extends CommonServiceImpl implements OrderSendServiceI {

	private static final String SEND_ORDER = "1";
	private static final String CANCLE_ORDER = "3";
	private static final String FINISH_ORDER = "2";
	
	@Override
	public String sendPaybillsOrder(List<PayBillsOrderEntity> billsOrderList, HttpServletRequest req) {
		String result = "派单成功";
		int failNum = 0;
		int successNum = 0;
		String reciever = ResourceUtil.getParameter("reciever");
		
		if(StringUtil.isEmpty(reciever)){
			result = "您没有选择订单发货人";
		}
		if(StringUtil.isNotEmpty(reciever) && null != billsOrderList && billsOrderList.size() > 0){
			TSUser userCopartner = findUniqueByProperty(TSUser.class, "id", reciever);
			UserCopartnerEntity copartner = userCopartner.getTsuserCopartner();
			if(null != copartner){
				
				TSUser user = ResourceUtil.getSessionUserName();
				TSCompany distributeCompany = findUniqueByProperty(TSCompany.class, "id",user.getCompany().getId());
				
				for (PayBillsOrderEntity payBillsOrderEntity : billsOrderList) {
					
					try{
						
						//开始派发订单
						int resultnum = sendOrderList(payBillsOrderEntity.getOrderS(), userCopartner, user, distributeCompany);
						successNum = successNum + resultnum;
						failNum = failNum + payBillsOrderEntity.getOrderS().size() - resultnum;
					}catch(Exception e){
						failNum++;
					}
					
				}
			}else{
				result = "抱歉，系统没有找到您要的发货人";
			}
			
		}
		if(null == billsOrderList || billsOrderList.size() < 1){
			result = "请选择需要发货的订单";
		}
		
		if(successNum > 0 && failNum > 0 && failNum < billsOrderList.size()){
			result = "共" + successNum + "件派单成功，其中有" + failNum + "件订单派发失败！";
		}
		if(successNum == 0){
			result = "派单失败，请检查订单是否在发货状态，否则不允许派单！";
		}
		return result;
	}

	@Override
	public String sendOrders(List<OrderEntity> orderList, HttpServletRequest req) {
		
		String result = "派单成功";
		int failNum = 0;
		int successNum = 0;
		String reciever = ResourceUtil.getParameter("reciever");
		
		if(StringUtil.isEmpty(reciever)){
			result = "您没有选择订单发货人";
		}
		if(StringUtil.isNotEmpty(reciever) && null != orderList && orderList.size() > 0){
			
			TSUser userCopartner = findUniqueByProperty(TSUser.class, "id", reciever);
			UserCopartnerEntity copartner = userCopartner.getTsuserCopartner();
			
			if(null != copartner){
				
				TSUser user = ResourceUtil.getSessionUserName();
				TSCompany distributeCompany = findUniqueByProperty(TSCompany.class, "id",user.getCompany().getId());
				
					
					try{
						
						//开始派发订单
						successNum = sendOrderList(orderList, userCopartner, user, distributeCompany);
						failNum = orderList.size() - successNum;
					}catch(Exception e){
						result ="订单派发失败";
					}
					
			}else{
				result = "抱歉，系统没有找到您要的发货人";
			}
			
		}
		if(null == orderList || orderList.size() < 1){
			result = "请选择需要发货的订单";
		}
		if(successNum > 0 && failNum > 0 && failNum < orderList.size()){
			result = "共" + successNum + "件派单成功，其中有" + failNum + "件订单派发失败！";
		}
		if(successNum == 0){
			result = "派单失败，请检查订单是否在发货状态，否则不允许派单！";
		}
		return result;
		
	}
	
	//订单派单
	private void sendSingleOrder(OrderEntity order, TSUser receiver, 
			TSUser distributer, TSCompany distributecompany) throws Exception {

		OrderSendEntity orderSend = new OrderSendEntity();
		orderSend.setId(IdWorker.generateSequenceNo());
		orderSend.setDistributer(distributer);
		orderSend.setDistributecompanyid(distributecompany);
		orderSend.setReceiver(receiver);
		orderSend.setOrderid(order);
		orderSend.setStatus(OrderSendServiceI.Status.ORDER_SEND.getValue());
		
		this.save(orderSend);
		
	}
	
	//构建派单对象
	private OrderSendEntity buildSendOrder(OrderEntity order, TSUser receiver, 
			TSUser distributer, TSCompany distributecompany) throws Exception {

		OrderSendEntity orderSend = new OrderSendEntity();
		orderSend.setId(IdWorker.generateSequenceNo());
		orderSend.setDistributer(distributer);
		orderSend.setDistributecompanyid(distributecompany);
		orderSend.setReceiver(receiver);
		orderSend.setOrderid(order);
		orderSend.setCreatetime(DateUtils.getDate());
		orderSend.setStatus(OrderSendServiceI.Status.ORDER_SEND.getValue());
		
		return orderSend;
		
	}
	
	
	//构建派单对象
	private int sendOrderList(List<OrderEntity> orderlist, TSUser receiver, 
			TSUser distributer, TSCompany distributecompany) throws Exception {

		List<OrderSendEntity> orderSendList = new ArrayList<OrderSendEntity>();
		List<OrderSendEntity> orderSendHitoryList = new ArrayList<OrderSendEntity>();
		CriteriaQuery cq = null;
		
		int sendnum = 0;
		for (OrderEntity orderEntity : orderlist) {
			
			if(OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(orderEntity.getFreightState().getDictionaryValue())){
				//查找是否有派单历史
				cq = new CriteriaQuery(OrderSendEntity.class);
				cq.eq("orderid", orderEntity);
				cq.add();
				List<OrderSendEntity> historysend = getListByCriteriaQuery(cq, false);
				if(null != historysend && historysend.size() > 0){
					orderSendHitoryList.addAll(historysend);
				}
				
				orderSendList.add(buildSendOrder(orderEntity, receiver, distributer, distributecompany));
				
				orderEntity.setOrdersend("Y");//更新订单派单标志为“派单处理”状态
				sendnum++;
			}
		}
		
		//取消已经派单的历史记录，重新派单
		for (OrderSendEntity orderSendEntity : orderSendHitoryList) {
			
			orderSendEntity.setStatus(OrderSendServiceI.Status.ORDER_CANCEL.getValue());//取消派单
		}
		
		if( sendnum > 0 ){
			
			if(orderSendHitoryList.size() > 0){
				batchUpdate(orderSendHitoryList);
			}
			
			batchUpdate(orderlist);//更新订单派单标志为“派单处理”状态
			
			batchSave(orderSendList);
		}
		
		return sendnum;
	}

	@Override
	public OrderSendEntity getSendOrder(OrderEntity order, HttpServletRequest req) {
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class);
		cq.eq("orderid", order);
		cq.eq("status", OrderSendServiceI.Status.ORDER_SEND.getValue());//选择处在派单环节的订单
		cq.add();
		
		List<OrderSendEntity> OrderSendList = this.getListByCriteriaQuery(cq, false);
		
		if(null != OrderSendList && OrderSendList.size() > 0){
			return OrderSendList.get(0);
		}
		return null;
	}

	@Override
	public boolean cancelSendOrder(OrderEntity order) {
		//查找是否有派单历史
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class);
		cq.eq("orderid", order);
		cq.add();
		List<OrderSendEntity> historysend = getListByCriteriaQuery(cq, false);
		if(null != historysend && historysend.size() > 0){
			OrderSendEntity orderSend = historysend.get(0);
			
			orderSend.setStatus(OrderSendServiceI.Status.ORDER_CANCEL.getValue());
			
			updateEntitie(orderSend);
		}
		return true;
	}
	
}