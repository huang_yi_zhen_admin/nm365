package cn.gov.xnc.admin.copartner.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderSendServiceI extends CommonService{
	
	public enum Status {

		ORDER_SEND("订单派发", "1"),
		ORDER_DEAL("正在处理", "2"),
		ORDER_CANCEL("已取消", "3");

		private String name;
		private String value;

		private Status(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String index) {
			this.value = index;
		}

	}

	public String sendPaybillsOrder(List<PayBillsOrderEntity> billsOrderList, HttpServletRequest req);
	
	public String sendOrders(List<OrderEntity> orderList, HttpServletRequest req);
	
	public OrderSendEntity getSendOrder(OrderEntity order, HttpServletRequest req);
	
	/**
	 * 取消派单
	 * @param order
	 * @return
	 */
	public boolean cancelSendOrder(OrderEntity order);
}
