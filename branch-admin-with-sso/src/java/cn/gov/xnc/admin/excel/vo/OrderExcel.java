package cn.gov.xnc.admin.excel.vo;


import cn.gov.xnc.system.excel.annotation.Excel;


public class OrderExcel {
		
	
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private String excelId;
	/**订单编号*/
	@Excel(exportName="订单编号",orderNum="1")
	private String identifieror;
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="2")
	private String productname;
	/**数量*/
	@Excel(exportName="数量",orderNum="3")
	private String number;
	/**收货人*/
	@Excel(exportName="收货人",orderNum="4")
	private String customername;
	/**电话*/
	@Excel(exportName="电话",orderNum="5")
	private String telephone;
	/**省份*/
	@Excel(exportName="省份",orderNum="6")
	private String province;
	/**收货地址*/
	@Excel(exportName="收货地址",orderNum="7")
	private String address;
	
	/**发货时间*/
	@Excel(exportName="发货时间",orderNum="8")
	private String departuredate;
	/**下单时间*/
	@Excel(exportName="下单时间",orderNum="8")
	private String orderdatetime;
	
	/**物流公司*/
	@Excel(exportName="物流公司",orderNum="9")
	private String logisticscompany;
	/**物流单号*/
	@Excel(exportName="物流单号",orderNum="10")
	private String logisticsnumber;
	/**备注*/
	@Excel(exportName="备注",orderNum="11")
	private String remarks;
	
	/**订货价格*/
	@Excel(exportName="订货价格",orderNum="12")
	private String price;
	
	/**商品运费*/
	@Excel(exportName="商品运费",orderNum="13")
	private String freightpic;
	
	/**订单状态  待付款_1,已取消_5,待审核_6,未通过_7 */
	@Excel(exportName="订单状态",orderNum="14")
	private String state;
	
	/**物流状态*/
	@Excel(exportName="物流状态",orderNum="14")
	private String freightState;
	
	/**客户名称*/
	@Excel(exportName="客户名称",orderNum="15")
	private String  clientid;
	
	/**业务员*/
	@Excel(exportName="业务员",orderNum="16")
	private String  yewu;
	
	/**
	 *方法: 取得String
	 *@return: String  导入的表格id
	 */
	public String getExcelId() {
		return excelId;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  导入的表格id
	 */
	public void setExcelId(String excelId) {
		this.excelId = excelId;
	}
	
	
	/**
	 * @return the identifieror
	 */
	public String getIdentifieror() {
		return identifieror;
	}

	/**
	 * @param identifieror the identifieror to set
	 */
	public void setIdentifieror(String identifieror) {
		this.identifieror = identifieror;
	}

	/**
	 * @return the productname
	 */
	public String getProductname() {
		return productname;
	}

	/**
	 * @param productname the productname to set
	 */
	public void setProductname(String productname) {
		this.productname = productname;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the customername
	 */
	public String getCustomername() {
		return customername;
	}

	/**
	 * @param customername the customername to set
	 */
	public void setCustomername(String customername) {
		this.customername = customername;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the departuredate
	 */
	public String getDeparturedate() {
		return departuredate;
	}

	/**
	 * @param departuredate the departuredate to set
	 */
	public void setDeparturedate(String departuredate) {
		this.departuredate = departuredate;
	}

	/**
	 * @return the logisticscompany
	 */
	public String getLogisticscompany() {
		return logisticscompany;
	}

	/**
	 * @param logisticscompany the logisticscompany to set
	 */
	public void setLogisticscompany(String logisticscompany) {
		this.logisticscompany = logisticscompany;
	}

	/**
	 * @return the logisticsnumber
	 */
	public String getLogisticsnumber() {
		return logisticsnumber;
	}

	/**
	 * @param logisticsnumber the logisticsnumber to set
	 */
	public void setLogisticsnumber(String logisticsnumber) {
		this.logisticsnumber = logisticsnumber;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the clientid
	 */
	public String getClientid() {
		return clientid;
	}

	/**
	 * @param clientid the clientid to set
	 */
	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	/**
	 * @return the yewu
	 */
	public String getYewu() {
		return yewu;
	}

	/**
	 * @param yewu the yewu to set
	 */
	public void setYewu(String yewu) {
		this.yewu = yewu;
	}

	public String getOrderdatetime() {
		return orderdatetime;
	}

	public void setOrderdatetime(String orderdatetime) {
		this.orderdatetime = orderdatetime;
	}

	public String getFreightpic() {
		return freightpic;
	}

	public void setFreightpic(String freightpic) {
		this.freightpic = freightpic;
	}

	public String getFreightState() {
		return freightState;
	}

	public void setFreightState(String freightState) {
		this.freightState = freightState;
	}
	
}
