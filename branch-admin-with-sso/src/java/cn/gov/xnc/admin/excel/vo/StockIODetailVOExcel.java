package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class StockIODetailVOExcel {

	public StockIODetailVOExcel() {
		// TODO Auto-generated constructor stub
	}
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**仓库*/
	@Excel(exportName="仓库",orderNum="1")
	private java.lang.String stockorderid_stockid_stockname;
	
	/**经办人*/
	@Excel(exportName="经办人",orderNum="2")
	private java.lang.String stockorderid_operator_realname;
	
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="3")
	private java.lang.String productid_name;
	
	/**商品编码*/
	@Excel(exportName="商品编码",orderNum="4")
	private java.lang.String productid_code;
	
	/**类型*/
	@Excel(exportName="类型",orderNum="5")
	private java.lang.String stockorderid_stocktypeid_type;
	
	/**出入库类型*/
	@Excel(exportName="出入库类型",orderNum="6")
	private java.lang.String stockorderid_stocktypeid_typecode;
	
	/**数量*/
	@Excel(exportName="数量",orderNum="7")
	private java.lang.String iostocknum;
	
	/**单位*/
	@Excel(exportName="单位",orderNum="8")
	private java.lang.String unitid_name;
	
	/**运费*/
	@Excel(exportName="运费",orderNum="9")
	private java.lang.String freightprice;
	
	/**出入库单价*/
	@Excel(exportName="出入库单价",orderNum="10")
	private java.lang.String unitprice;
	
	/**总价*/
	@Excel(exportName="总价",orderNum="11")
	private java.lang.String totalprice;
	
	/**库存总价*/
	@Excel(exportName="库存总价",orderNum="14")
	private java.lang.String rightnowtotalprice;
	
	/**供应商*/
	@Excel(exportName="供应商",orderNum="15")
	private java.lang.String supplierid_name;
	
	/**时间*/
	@Excel(exportName="时间",orderNum="16")
	private java.lang.String updatetime;

	/**
	 * @return the productid_code
	 */
	public java.lang.String getProductid_code() {
		return productid_code;
	}

	/**
	 * @param productid_code the productid_code to set
	 */
	public void setProductid_code(java.lang.String productid_code) {
		this.productid_code = productid_code;
	}

	/**
	 * @return the rightnowtotalprice
	 */
	public java.lang.String getRightnowtotalprice() {
		return rightnowtotalprice;
	}

	/**
	 * @param rightnowtotalprice the rightnowtotalprice to set
	 */
	public void setRightnowtotalprice(java.lang.String rightnowtotalprice) {
		this.rightnowtotalprice = rightnowtotalprice;
	}

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the stockorderid_stockid_stockname
	 */
	public java.lang.String getStockorderid_stockid_stockname() {
		return stockorderid_stockid_stockname;
	}

	/**
	 * @param stockorderid_stockid_stockname the stockorderid_stockid_stockname to set
	 */
	public void setStockorderid_stockid_stockname(java.lang.String stockorderid_stockid_stockname) {
		this.stockorderid_stockid_stockname = stockorderid_stockid_stockname;
	}

	/**
	 * @return the stockorderid_operator_realname
	 */
	public java.lang.String getStockorderid_operator_realname() {
		return stockorderid_operator_realname;
	}

	/**
	 * @param stockorderid_operator_realname the stockorderid_operator_realname to set
	 */
	public void setStockorderid_operator_realname(java.lang.String stockorderid_operator_realname) {
		this.stockorderid_operator_realname = stockorderid_operator_realname;
	}

	/**
	 * @return the productid_name
	 */
	public java.lang.String getProductid_name() {
		return productid_name;
	}

	/**
	 * @param productid_name the productid_name to set
	 */
	public void setProductid_name(java.lang.String productid_name) {
		this.productid_name = productid_name;
	}

	/**
	 * @return the stockorderid_stocktypeid_typecode
	 */
	public java.lang.String getStockorderid_stocktypeid_typecode() {
		return stockorderid_stocktypeid_typecode;
	}

	/**
	 * @param stockorderid_stocktypeid_typecode the stockorderid_stocktypeid_typecode to set
	 */
	public void setStockorderid_stocktypeid_typecode(java.lang.String stockorderid_stocktypeid_typecode) {
		this.stockorderid_stocktypeid_typecode = stockorderid_stocktypeid_typecode;
	}

	/**
	 * @return the iostocknum
	 */
	public java.lang.String getIostocknum() {
		return iostocknum;
	}

	/**
	 * @param iostocknum the iostocknum to set
	 */
	public void setIostocknum(java.lang.String iostocknum) {
		this.iostocknum = iostocknum;
	}

	/**
	 * @return the unitid_name
	 */
	public java.lang.String getUnitid_name() {
		return unitid_name;
	}

	/**
	 * @param unitid_name the unitid_name to set
	 */
	public void setUnitid_name(java.lang.String unitid_name) {
		this.unitid_name = unitid_name;
	}

	/**
	 * @return the freightprice
	 */
	public java.lang.String getFreightprice() {
		return freightprice;
	}

	/**
	 * @param freightprice the freightprice to set
	 */
	public void setFreightprice(java.lang.String freightprice) {
		this.freightprice = freightprice;
	}

	/**
	 * @return the unitprice
	 */
	public java.lang.String getUnitprice() {
		return unitprice;
	}

	/**
	 * @param unitprice the unitprice to set
	 */
	public void setUnitprice(java.lang.String unitprice) {
		this.unitprice = unitprice;
	}

	/**
	 * @return the totalprice
	 */
	public java.lang.String getTotalprice() {
		return totalprice;
	}

	/**
	 * @param totalprice the totalprice to set
	 */
	public void setTotalprice(java.lang.String totalprice) {
		this.totalprice = totalprice;
	}

	/**
	 * @return the supplierid_name
	 */
	public java.lang.String getSupplierid_name() {
		return supplierid_name;
	}

	/**
	 * @param supplierid_name the supplierid_name to set
	 */
	public void setSupplierid_name(java.lang.String supplierid_name) {
		this.supplierid_name = supplierid_name;
	}

	/**
	 * @return the updatetime
	 */
	public java.lang.String getUpdatetime() {
		return updatetime;
	}

	/**
	 * @param updatetime the updatetime to set
	 */
	public void setUpdatetime(java.lang.String updatetime) {
		this.updatetime = updatetime;
	}

	/**
	 * @return the stockorderid_stocktypeid_type
	 */
	public java.lang.String getStockorderid_stocktypeid_type() {
		return stockorderid_stocktypeid_type;
	}

	/**
	 * @param stockorderid_stocktypeid_type the stockorderid_stocktypeid_type to set
	 */
	public void setStockorderid_stocktypeid_type(java.lang.String stockorderid_stocktypeid_type) {
		this.stockorderid_stocktypeid_type = stockorderid_stocktypeid_type;
	}
	

}
