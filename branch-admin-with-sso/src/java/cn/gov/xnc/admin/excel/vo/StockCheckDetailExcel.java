package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class StockCheckDetailExcel {

	public StockCheckDetailExcel() {
		// TODO Auto-generated constructor stub
	}
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**盘点名称*/
	@Excel(exportName="盘点名称",orderNum="1")
	private java.lang.String checkorderid_name;
	
	/**仓库*/
	@Excel(exportName="仓库",orderNum="2")
	private java.lang.String checkorderid_stockid_stockname;
	
	/**经办人*/
	@Excel(exportName="经办人",orderNum="3")
	private java.lang.String checkorderid_operator_realname;
	
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="4")
	private java.lang.String stockproductid_productid_name;
	
	/**实盘数量*/
	@Excel(exportName="实盘数量",orderNum="5")
	private java.lang.String checkstocknum;
	
	/**抄库数量*/
	@Excel(exportName="抄库数量",orderNum="6")
	private java.lang.String currentstocknum;
	
	/**调整数量*/
	@Excel(exportName="调整数量",orderNum="7")
	private java.lang.String adjuststocknum;
	
	/**单位*/
	@Excel(exportName="单位",orderNum="8")
	private java.lang.String unitid_name;
	
	/**单位价格*/
	@Excel(exportName="单位价格",orderNum="9")
	private java.lang.String averageunitprice;
	
	/**盈亏金额*/
	@Excel(exportName="盈亏金额",orderNum="10")
	private java.lang.String totalprice;
	
	/**时间*/
	@Excel(exportName="时间",orderNum="11")
	private java.lang.String updatetime;

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the checkorderid_name
	 */
	public java.lang.String getCheckorderid_name() {
		return checkorderid_name;
	}

	/**
	 * @param checkorderid_name the checkorderid_name to set
	 */
	public void setCheckorderid_name(java.lang.String checkorderid_name) {
		this.checkorderid_name = checkorderid_name;
	}

	/**
	 * @return the checkorderid_stockid_stockname
	 */
	public java.lang.String getCheckorderid_stockid_stockname() {
		return checkorderid_stockid_stockname;
	}

	/**
	 * @param checkorderid_stockid_stockname the checkorderid_stockid_stockname to set
	 */
	public void setCheckorderid_stockid_stockname(java.lang.String checkorderid_stockid_stockname) {
		this.checkorderid_stockid_stockname = checkorderid_stockid_stockname;
	}

	/**
	 * @return the checkorderid_operator_realname
	 */
	public java.lang.String getCheckorderid_operator_realname() {
		return checkorderid_operator_realname;
	}

	/**
	 * @param checkorderid_operator_realname the checkorderid_operator_realname to set
	 */
	public void setCheckorderid_operator_realname(java.lang.String checkorderid_operator_realname) {
		this.checkorderid_operator_realname = checkorderid_operator_realname;
	}

	/**
	 * @return the stockproductid_productid_name
	 */
	public java.lang.String getStockproductid_productid_name() {
		return stockproductid_productid_name;
	}

	/**
	 * @param stockproductid_productid_name the stockproductid_productid_name to set
	 */
	public void setStockproductid_productid_name(java.lang.String stockproductid_productid_name) {
		this.stockproductid_productid_name = stockproductid_productid_name;
	}

	/**
	 * @return the checkstocknum
	 */
	public java.lang.String getCheckstocknum() {
		return checkstocknum;
	}

	/**
	 * @param checkstocknum the checkstocknum to set
	 */
	public void setCheckstocknum(java.lang.String checkstocknum) {
		this.checkstocknum = checkstocknum;
	}

	/**
	 * @return the currentstocknum
	 */
	public java.lang.String getCurrentstocknum() {
		return currentstocknum;
	}

	/**
	 * @param currentstocknum the currentstocknum to set
	 */
	public void setCurrentstocknum(java.lang.String currentstocknum) {
		this.currentstocknum = currentstocknum;
	}

	/**
	 * @return the adjuststocknum
	 */
	public java.lang.String getAdjuststocknum() {
		return adjuststocknum;
	}

	/**
	 * @param adjuststocknum the adjuststocknum to set
	 */
	public void setAdjuststocknum(java.lang.String adjuststocknum) {
		this.adjuststocknum = adjuststocknum;
	}

	/**
	 * @return the unitid_name
	 */
	public java.lang.String getUnitid_name() {
		return unitid_name;
	}

	/**
	 * @param unitid_name the unitid_name to set
	 */
	public void setUnitid_name(java.lang.String unitid_name) {
		this.unitid_name = unitid_name;
	}

	/**
	 * @return the averageunitprice
	 */
	public java.lang.String getAverageunitprice() {
		return averageunitprice;
	}

	/**
	 * @param averageunitprice the averageunitprice to set
	 */
	public void setAverageunitprice(java.lang.String averageunitprice) {
		this.averageunitprice = averageunitprice;
	}

	/**
	 * @return the totalprice
	 */
	public java.lang.String getTotalprice() {
		return totalprice;
	}

	/**
	 * @param totalprice the totalprice to set
	 */
	public void setTotalprice(java.lang.String totalprice) {
		this.totalprice = totalprice;
	}

	/**
	 * @return the updatetime
	 */
	public java.lang.String getUpdatetime() {
		return updatetime;
	}

	/**
	 * @param updatetime the updatetime to set
	 */
	public void setUpdatetime(java.lang.String updatetime) {
		this.updatetime = updatetime;
	}
	
	

}
