package cn.gov.xnc.admin.excel.vo;

import java.math.BigDecimal;
import cn.gov.xnc.system.excel.annotation.Excel;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;


/**   
 * @Title: Entity
 * @Description: 下单产品录入
 * @author zero
 * @date 2016-04-13 20:22:17
 * @version V1.0   
 *
 */

public class XiadanProductVO  {
	
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private String excelId;
	/**下单日期*/
	@Excel(exportName="下单日期",orderNum="1")
	private String createdate;
	/**产品名称*/
	@Excel(exportName="下单产品",orderNum="2")
	private String productname;
	/**件数*/
	@Excel(exportName="件数",orderNum="3")
	private java.lang.Integer number;
	/**规格*/
	@Excel(exportName="规格",orderNum="4")
	private String specifications;
	/**客户姓名*/
	@Excel(exportName="客户姓名",orderNum="5")
	private String customername;
	/**电话*/
	@Excel(exportName="电话",orderNum="6")
	private String telephone;
	/**收货地址*/
	@Excel(exportName="收货地址",orderNum="7")
	private String address;
	/**备注*/
	@Excel(exportName="备注",orderNum="8")
	private String remarks;
	
	/**价格*/
	@Excel(exportName="价格",orderNum="9")
	private BigDecimal price;

	/**商品编码*/
	@Excel(exportName="商品编码",orderNum="10")
	private String code;
	
	/**采购商商*/
	@Excel(exportName="采购商唯一编码",orderNum="11")
	private String userBuyerId;
	
	/**公司id*/
	private  String  companyId ;;

	/**所属地区*/
	private String TSTerritory ;// 地址
	
	/**发货日期*/
	private java.util.Date departuredate;
	
	
	
	
	
//	TSUser TSUser = new TSUser();
//	
//	/**
//	 *方法: 取得String
//	 *@return: String  农场ID、用户ID
//	 */
//	public TSUser getTSUser() {
//		return TSUser;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  农场ID、用户ID
//	 */
//	public void setTSUser(TSUser tSUser) {
//		TSUser = tSUser;
//	}
	
	
//	/**
//	 *方法: 取得String
//	 *@return: String  id
//	 */
//
//	public String getId(){
//		return this.id;
//	}
//
//	/**
//	 *方法: 设置String
//	 *@param: String  id
//	 */
//	public void setId(String id){
//		this.id = id;
//	}
	
	
	
	
	
	/**
	 *方法: 取得String
	 *@return: String  产品名称
	 */

	public String getProductname(){
		return this.productname;
	}

	/**
	 *方法: 设置String
	 *@param: String  产品名称
	 */
	public void setProductname(String productname){
		this.productname = productname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  件数
	 */

	public java.lang.Integer getNumber(){
		return this.number;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  件数
	 */
	public void setNumber(java.lang.Integer number){
		this.number = number;
	}
	/**
	 *方法: 取得String
	 *@return: String  规格
	 */

	public String getSpecifications(){
		return this.specifications;
	}

	/**
	 *方法: 设置String
	 *@param: String  规格
	 */
	public void setSpecifications(String specifications){
		this.specifications = specifications;
	}


	/**
	 *方法: 取得String
	 *@return: String  客户姓名
	 */

	public String getCustomername(){
		return this.customername;
	}

	/**
	 *方法: 设置String
	 *@param: String  客户姓名
	 */
	public void setCustomername(String customername){
		this.customername = customername;
	}
	/**
	 *方法: 取得String
	 *@return: String  电话
	 */

	public String getTelephone(){
		return this.telephone;
	}

	/**
	 *方法: 设置String
	 *@param: String  电话
	 */
	public void setTelephone(String telephone){
		this.telephone = telephone;
	}
	/**
	 *方法: 取得String
	 *@return: String  收货地址
	 */

	public String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置String
	 *@param: String  收货地址
	 */
	public void setAddress(String address){
		this.address = address;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  发货日期
	 */

	public java.util.Date getDeparturedate(){
		return this.departuredate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  发货日期
	 */
	public void setDeparturedate(java.util.Date departuredate){
		this.departuredate = departuredate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  下单日期
	 */

	public String getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  下单日期
	 */
	public void setCreatedate(String createdate){
		this.createdate = createdate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */

	public String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  订货价
	 */
	public BigDecimal getPrice(){
		return this.price;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  订货价
	 */
	public void setPrice(BigDecimal price){
		this.price = price;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  导入的表格id
	 */
	public String getExcelId() {
		return excelId;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  导入的表格id
	 */
	public void setExcelId(String excelId) {
		this.excelId = excelId;
	}
	


	
	/**
	 *方法: 取得String
	 *@return: String  商品编码
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 *方法: 设置String
	 *@param: String   商品编码
	 */
	public void setCode(String code) {
		this.code = code;
	}
	

	
	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the tSTerritory
	 */
	public String getTSTerritory() {
		return TSTerritory;
	}

	/**
	 * @param tSTerritory the tSTerritory to set
	 */
	public void setTSTerritory(String tSTerritory) {
		TSTerritory = tSTerritory;
	}

	public String getUserBuyerId() {
		return userBuyerId;
	}

	public void setUserBuyerId(String userBuyerId) {
		this.userBuyerId = userBuyerId;
	}
	
	
	
}
