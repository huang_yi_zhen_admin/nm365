package cn.gov.xnc.admin.excel.vo;


import cn.gov.xnc.system.excel.annotation.Excel;


public class OrderToRecieveExcel {
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private String excelId;
	/**订单编号*/
	@Excel(exportName="订单编号",orderNum="1")
	private String identifieror;
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="2")
	private String productname;
	/**收货人*/
	@Excel(exportName="收货人",orderNum="3")
	private String customername;
	/**电话*/
	@Excel(exportName="电话",orderNum="4")
	private String telephone;
	/**收货地址*/
	@Excel(exportName="收货地址",orderNum="5")
	private String address;
	/**发货时间*/
	@Excel(exportName="发货时间",orderNum="6")
	private String departuredate;
	/**下单时间*/
	@Excel(exportName="下单时间",orderNum="7")
	private String createdate;
	/**物流公司*/
	@Excel(exportName="物流公司",orderNum="8")
	private String logisticscompany;
	/**物流单号*/
	@Excel(exportName="物流单号",orderNum="9")
	private String logisticsnumber;
	/**数量*/
	@Excel(exportName="数量",orderNum="10")
	private String number;
	/**单位成本*/
	@Excel(exportName="单位成本",orderNum="11")
	private String priceu;
	/**采购成本*/
	@Excel(exportName="采购成本",orderNum="12")
	private String pricec;
	/**销售额*/
	@Excel(exportName="销售额",orderNum="13")
	private String totalPrice;
//	/**订单状态  UNPAID("待付款", "1"),WAIT2RECEIPT("待收货", "2"),CANCEL("已取消", "3"),REBACK("已退单", "4"),EVALUATE("待评价", "5"); */
//	@Excel(exportName="订单状态",orderNum="14")
//	private String state;
	/**物流状态*/
	@Excel(exportName="订单状态",orderNum="14")
	private String freightState;
	/**客户名称*/
	@Excel(exportName="订单来源",orderNum="15")
	private String  clientName;
	
	
	/**
	 *方法: 取得String
	 *@return: String  导入的表格id
	 */
	public String getExcelId() {
		return excelId;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  导入的表格id
	 */
	public void setExcelId(String excelId) {
		this.excelId = excelId;
	}
	
	
	/**
	 * @return the identifieror
	 */
	public String getIdentifieror() {
		return identifieror;
	}

	/**
	 * @param identifieror the identifieror to set
	 */
	public void setIdentifieror(String identifieror) {
		this.identifieror = identifieror;
	}

	/**
	 * @return the productname
	 */
	public String getProductname() {
		return productname;
	}

	/**
	 * @param productname the productname to set
	 */
	public void setProductname(String productname) {
		this.productname = productname;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the customername
	 */
	public String getCustomername() {
		return customername;
	}

	/**
	 * @param customername the customername to set
	 */
	public void setCustomername(String customername) {
		this.customername = customername;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the departuredate
	 */
	public String getDeparturedate() {
		return departuredate;
	}

	/**
	 * @param departuredate the departuredate to set
	 */
	public void setDeparturedate(String departuredate) {
		this.departuredate = departuredate;
	}

	/**
	 * @return the logisticscompany
	 */
	public String getLogisticscompany() {
		return logisticscompany;
	}

	/**
	 * @param logisticscompany the logisticscompany to set
	 */
	public void setLogisticscompany(String logisticscompany) {
		this.logisticscompany = logisticscompany;
	}

	/**
	 * @return the logisticsnumber
	 */
	public String getLogisticsnumber() {
		return logisticsnumber;
	}

	/**
	 * @param logisticsnumber the logisticsnumber to set
	 */
	public void setLogisticsnumber(String logisticsnumber) {
		this.logisticsnumber = logisticsnumber;
	}

	/**
	 * @return the price
	 */
	public String getPriceu() {
		return priceu;
	}

	/**
	 * @param price the price to set
	 */
	public void setPriceu(String priceu) {
		this.priceu = pricec;
	}
	/**
	 * @return the price
	 */
	public String getPricec() {
		return pricec;
	}

	/**
	 * @param price the price to set
	 */
	public void setPricec(String pricec) {
		this.pricec = pricec;
	}

	/**
	 * @return the clientName
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * @param clientid the clientName to set
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}


	public String getFreightState() {
		return freightState;
	}

	public void setFreightState(String freightState) {
		this.freightState = freightState;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	
}
