package cn.gov.xnc.admin.excel.vo;


import cn.gov.xnc.system.excel.annotation.Excel;


/**   
 * @Title: Entity
 * @Description: 玉绿宝真扶贫导出数据
 * @author zero
 * @date 2018-01-04 16:01:32
 * @version V1.0   
 *
 */

public class YlbSalesExcel{
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private String excelId;
	/**编号*/
	@Excel(exportName="编号",orderNum="1")
	private java.lang.String identifier;
	/**销售网点*/
	@Excel(exportName="销售网点",orderNum="2")
	private java.lang.String salesoutlets;
	/**销售员*/
	@Excel(exportName="销售员",orderNum="3")
	private java.lang.String salesstaff;
	/**销售时间*/
	@Excel(exportName="销售时间",orderNum="4")
	private java.lang.String salestime;
	/**客户名称*/
	@Excel(exportName="客户名称",orderNum="5")
	private java.lang.String customername;
	/**客户电话*/
	@Excel(exportName="客户电话",orderNum="6")
	private java.lang.String telephone;
	/**收货人*/
	@Excel(exportName="收货人",orderNum="7")
	private java.lang.String deliveryname;
	/**省*/
	@Excel(exportName="省",orderNum="8")
	private java.lang.String province;
	/**市*/
	@Excel(exportName="市",orderNum="9")
	private java.lang.String city;
	/**区*/
	@Excel(exportName="区",orderNum="10")
	private java.lang.String area;
	/**送货地址*/
	@Excel(exportName="送货地址",orderNum="11")
	private java.lang.String deliveryaddress;
	/**收货电话*/
	@Excel(exportName="收货电话",orderNum="12")
	private java.lang.String deliverytelephone;
	/**核销方式*/
	@Excel(exportName="核销方式",orderNum="13")
	private java.lang.String mode;
	/**创建人*/
	@Excel(exportName="创建人",orderNum="14")
	private java.lang.String createuser;
	
	
	public String getExcelId() {
		return excelId;
	}
	public void setExcelId(String excelId) {
		this.excelId = excelId;
	}
	public java.lang.String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(java.lang.String identifier) {
		this.identifier = identifier;
	}
	public java.lang.String getSalesoutlets() {
		return salesoutlets;
	}
	public void setSalesoutlets(java.lang.String salesoutlets) {
		this.salesoutlets = salesoutlets;
	}
	public java.lang.String getSalesstaff() {
		return salesstaff;
	}
	public void setSalesstaff(java.lang.String salesstaff) {
		this.salesstaff = salesstaff;
	}
	public java.lang.String getSalestime() {
		return salestime;
	}
	public void setSalestime(java.lang.String salestime) {
		this.salestime = salestime;
	}
	public java.lang.String getCustomername() {
		return customername;
	}
	public void setCustomername(java.lang.String customername) {
		this.customername = customername;
	}
	public java.lang.String getTelephone() {
		return telephone;
	}
	public void setTelephone(java.lang.String telephone) {
		this.telephone = telephone;
	}
	public java.lang.String getDeliveryname() {
		return deliveryname;
	}
	public void setDeliveryname(java.lang.String deliveryname) {
		this.deliveryname = deliveryname;
	}
	public java.lang.String getProvince() {
		return province;
	}
	public void setProvince(java.lang.String province) {
		this.province = province;
	}
	public java.lang.String getCity() {
		return city;
	}
	public void setCity(java.lang.String city) {
		this.city = city;
	}
	public java.lang.String getArea() {
		return area;
	}
	public void setArea(java.lang.String area) {
		this.area = area;
	}
	public java.lang.String getDeliveryaddress() {
		return deliveryaddress;
	}
	public void setDeliveryaddress(java.lang.String deliveryaddress) {
		this.deliveryaddress = deliveryaddress;
	}
	public java.lang.String getDeliverytelephone() {
		return deliverytelephone;
	}
	public void setDeliverytelephone(java.lang.String deliverytelephone) {
		this.deliverytelephone = deliverytelephone;
	}
	public java.lang.String getMode() {
		return mode;
	}
	public void setMode(java.lang.String mode) {
		this.mode = mode;
	}
	public java.lang.String getCreateuser() {
		return createuser;
	}
	public void setCreateuser(java.lang.String createuser) {
		this.createuser = createuser;
	}

	
	
	
	
	
}
