package cn.gov.xnc.admin.versiontransfer.enity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 模板信息
 * @author zero
 * @date 2016-09-28 16:17:33
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_freight_old", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class FreightOldEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**模板名称*/
	private java.lang.String name;
	/**快递公司*/
	private java.lang.String freightname;
	/**计量类型*/
	private java.lang.String type;
	/**状态*/
	private java.lang.String state;
	/**北京市*/
	private BigDecimal beijingshi;
	/**天津市*/
	private BigDecimal tianjinshi;
	/**河北省*/
	private BigDecimal hebeisheng;
	/**山西省*/
	private BigDecimal shanxisheng;
	/**内蒙古自治区*/
	private BigDecimal neimenggu;
	/**辽宁省*/
	private BigDecimal liaoningsheng;
	/**吉林省*/
	private BigDecimal jilinsheng;
	/**黑龙江省*/
	private BigDecimal heilongjiangsheng;
	/**上海市*/
	private BigDecimal shanghaishi;
	/**江苏省*/
	private BigDecimal jiangsusheng;
	/**浙江省*/
	private BigDecimal zhejiangsheng;
	/**安徽省*/
	private BigDecimal anhuisheng;
	/**福建省*/
	private BigDecimal fujiansheng;
	/**江西省*/
	private BigDecimal jiangxisheng;
	/**山东省*/
	private BigDecimal shandongsheng;
	/**河南省*/
	private BigDecimal henansheng;
	/**湖北省*/
	private BigDecimal hubeisheng;
	/**湖南省*/
	private BigDecimal hunansheng;
	/**广东省*/
	private BigDecimal guangdongsheng;
	/**广西壮族自治区*/
	private BigDecimal guangxi;
	/**海南省*/
	private BigDecimal hainansheng;
	/**重庆市*/
	private BigDecimal chongqingshi;
	/**四川省*/
	private BigDecimal sichuansheng;
	/**贵州省*/
	private BigDecimal guizhousheng;
	/**云南省*/
	private BigDecimal yunansheng;
	/**西藏自治区*/
	private BigDecimal xizang;
	/**陕西省*/
	private BigDecimal shanxisheng2;
	/**甘肃省*/
	private BigDecimal gansusheng;
	/**青海省*/
	private BigDecimal qinghaisheng;
	/**宁夏回族自治区*/
	private BigDecimal ningxia;
	/**新疆维吾尔自治区*/
	private BigDecimal xinjiang;
	/**台湾省*/
	private BigDecimal taiwansheng;
	/**香港特别行政区*/
	private BigDecimal xianggang;
	/**澳门特别行政区*/
	private BigDecimal anmen;
	/**公司信息*/
	private TSCompany company;
	/**创建时间*/
	private java.util.Date createdate;
	/**更新时间*/
	private java.util.Date updatedate;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  模板名称
	 */
	@Column(name ="NAME",nullable=false,length=500)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  模板名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递公司
	 */
	@Column(name ="FREIGHTNAME",nullable=true,length=500)
	public java.lang.String getFreightname(){
		return this.freightname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递公司
	 */
	public void setFreightname(java.lang.String freightname){
		this.freightname = freightname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  计量类型
	 */
	@Column(name ="TYPE",nullable=false,length=2)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  计量类型
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态
	 */
	@Column(name ="STATE",nullable=false,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  北京市
	 */
	@Column(name ="BEIJINGSHI",nullable=true,precision=10,scale=2)
	public BigDecimal getBeijingshi(){
		return this.beijingshi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  北京市
	 */
	public void setBeijingshi(BigDecimal beijingshi){
		this.beijingshi = beijingshi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  天津市
	 */
	@Column(name ="TIANJINSHI",nullable=true,precision=10,scale=2)
	public BigDecimal getTianjinshi(){
		return this.tianjinshi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  天津市
	 */
	public void setTianjinshi(BigDecimal tianjinshi){
		this.tianjinshi = tianjinshi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  河北省
	 */
	@Column(name ="HEBEISHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getHebeisheng(){
		return this.hebeisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  河北省
	 */
	public void setHebeisheng(BigDecimal hebeisheng){
		this.hebeisheng = hebeisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  山西省
	 */
	@Column(name ="SHANXISHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getShanxisheng(){
		return this.shanxisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  山西省
	 */
	public void setShanxisheng(BigDecimal shanxisheng){
		this.shanxisheng = shanxisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  内蒙古自治区
	 */
	@Column(name ="NEIMENGGU",nullable=true,precision=10,scale=2)
	public BigDecimal getNeimenggu(){
		return this.neimenggu;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  内蒙古自治区
	 */
	public void setNeimenggu(BigDecimal neimenggu){
		this.neimenggu = neimenggu;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  辽宁省
	 */
	@Column(name ="LIAONINGSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getLiaoningsheng(){
		return this.liaoningsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  辽宁省
	 */
	public void setLiaoningsheng(BigDecimal liaoningsheng){
		this.liaoningsheng = liaoningsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  吉林省
	 */
	@Column(name ="JILINSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getJilinsheng(){
		return this.jilinsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  吉林省
	 */
	public void setJilinsheng(BigDecimal jilinsheng){
		this.jilinsheng = jilinsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  黑龙江省
	 */
	@Column(name ="HEILONGJIANGSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getHeilongjiangsheng(){
		return this.heilongjiangsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  黑龙江省
	 */
	public void setHeilongjiangsheng(BigDecimal heilongjiangsheng){
		this.heilongjiangsheng = heilongjiangsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  上海市
	 */
	@Column(name ="SHANGHAISHI",nullable=true,precision=10,scale=2)
	public BigDecimal getShanghaishi(){
		return this.shanghaishi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  上海市
	 */
	public void setShanghaishi(BigDecimal shanghaishi){
		this.shanghaishi = shanghaishi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  江苏省
	 */
	@Column(name ="JIANGSUSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getJiangsusheng(){
		return this.jiangsusheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  江苏省
	 */
	public void setJiangsusheng(BigDecimal jiangsusheng){
		this.jiangsusheng = jiangsusheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  浙江省
	 */
	@Column(name ="ZHEJIANGSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getZhejiangsheng(){
		return this.zhejiangsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  浙江省
	 */
	public void setZhejiangsheng(BigDecimal zhejiangsheng){
		this.zhejiangsheng = zhejiangsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  安徽省
	 */
	@Column(name ="ANHUISHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getAnhuisheng(){
		return this.anhuisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  安徽省
	 */
	public void setAnhuisheng(BigDecimal anhuisheng){
		this.anhuisheng = anhuisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  福建省
	 */
	@Column(name ="FUJIANSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getFujiansheng(){
		return this.fujiansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  福建省
	 */
	public void setFujiansheng(BigDecimal fujiansheng){
		this.fujiansheng = fujiansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  江西省
	 */
	@Column(name ="JIANGXISHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getJiangxisheng(){
		return this.jiangxisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  江西省
	 */
	public void setJiangxisheng(BigDecimal jiangxisheng){
		this.jiangxisheng = jiangxisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  山东省
	 */
	@Column(name ="SHANDONGSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getShandongsheng(){
		return this.shandongsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  山东省
	 */
	public void setShandongsheng(BigDecimal shandongsheng){
		this.shandongsheng = shandongsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  河南省
	 */
	@Column(name ="HENANSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getHenansheng(){
		return this.henansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  河南省
	 */
	public void setHenansheng(BigDecimal henansheng){
		this.henansheng = henansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  湖北省
	 */
	@Column(name ="HUBEISHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getHubeisheng(){
		return this.hubeisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  湖北省
	 */
	public void setHubeisheng(BigDecimal hubeisheng){
		this.hubeisheng = hubeisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  湖南省
	 */
	@Column(name ="HUNANSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getHunansheng(){
		return this.hunansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  湖南省
	 */
	public void setHunansheng(BigDecimal hunansheng){
		this.hunansheng = hunansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  广东省
	 */
	@Column(name ="GUANGDONGSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getGuangdongsheng(){
		return this.guangdongsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  广东省
	 */
	public void setGuangdongsheng(BigDecimal guangdongsheng){
		this.guangdongsheng = guangdongsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  广西壮族自治区
	 */
	@Column(name ="GUANGXI",nullable=true,precision=10,scale=2)
	public BigDecimal getGuangxi(){
		return this.guangxi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  广西壮族自治区
	 */
	public void setGuangxi(BigDecimal guangxi){
		this.guangxi = guangxi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  海南省
	 */
	@Column(name ="HAINANSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getHainansheng(){
		return this.hainansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  海南省
	 */
	public void setHainansheng(BigDecimal hainansheng){
		this.hainansheng = hainansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  重庆市
	 */
	@Column(name ="CHONGQINGSHI",nullable=true,precision=10,scale=2)
	public BigDecimal getChongqingshi(){
		return this.chongqingshi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  重庆市
	 */
	public void setChongqingshi(BigDecimal chongqingshi){
		this.chongqingshi = chongqingshi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  四川省
	 */
	@Column(name ="SICHUANSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getSichuansheng(){
		return this.sichuansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  四川省
	 */
	public void setSichuansheng(BigDecimal sichuansheng){
		this.sichuansheng = sichuansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  贵州省
	 */
	@Column(name ="GUIZHOUSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getGuizhousheng(){
		return this.guizhousheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  贵州省
	 */
	public void setGuizhousheng(BigDecimal guizhousheng){
		this.guizhousheng = guizhousheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  云南省
	 */
	@Column(name ="YUNANSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getYunansheng(){
		return this.yunansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  云南省
	 */
	public void setYunansheng(BigDecimal yunansheng){
		this.yunansheng = yunansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  西藏自治区
	 */
	@Column(name ="XIZANG",nullable=true,precision=10,scale=2)
	public BigDecimal getXizang(){
		return this.xizang;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  西藏自治区
	 */
	public void setXizang(BigDecimal xizang){
		this.xizang = xizang;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  陕西省
	 */
	@Column(name ="SHANXISHENG2",nullable=true,precision=10,scale=2)
	public BigDecimal getShanxisheng2(){
		return this.shanxisheng2;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  陕西省
	 */
	public void setShanxisheng2(BigDecimal shanxisheng2){
		this.shanxisheng2 = shanxisheng2;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  甘肃省
	 */
	@Column(name ="GANSUSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getGansusheng(){
		return this.gansusheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  甘肃省
	 */
	public void setGansusheng(BigDecimal gansusheng){
		this.gansusheng = gansusheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  青海省
	 */
	@Column(name ="QINGHAISHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getQinghaisheng(){
		return this.qinghaisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  青海省
	 */
	public void setQinghaisheng(BigDecimal qinghaisheng){
		this.qinghaisheng = qinghaisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  宁夏回族自治区
	 */
	@Column(name ="NINGXIA",nullable=true,precision=10,scale=2)
	public BigDecimal getNingxia(){
		return this.ningxia;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  宁夏回族自治区
	 */
	public void setNingxia(BigDecimal ningxia){
		this.ningxia = ningxia;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  新疆维吾尔自治区
	 */
	@Column(name ="XINJIANG",nullable=true,precision=10,scale=2)
	public BigDecimal getXinjiang(){
		return this.xinjiang;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  新疆维吾尔自治区
	 */
	public void setXinjiang(BigDecimal xinjiang){
		this.xinjiang = xinjiang;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  台湾省
	 */
	@Column(name ="TAIWANSHENG",nullable=true,precision=10,scale=2)
	public BigDecimal getTaiwansheng(){
		return this.taiwansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  台湾省
	 */
	public void setTaiwansheng(BigDecimal taiwansheng){
		this.taiwansheng = taiwansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  香港特别行政区
	 */
	@Column(name ="XIANGGANG",nullable=true,precision=10,scale=2)
	public BigDecimal getXianggang(){
		return this.xianggang;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  香港特别行政区
	 */
	public void setXianggang(BigDecimal xianggang){
		this.xianggang = xianggang;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  澳门特别行政区
	 */
	@Column(name ="ANMEN",nullable=true,precision=10,scale=2)
	public BigDecimal getAnmen(){
		return this.anmen;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  澳门特别行政区
	 */
	public void setAnmen(BigDecimal anmen){
		this.anmen = anmen;
	}
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}
	
	
	
	   /** 
     *  
     *  
     * 对应的物流模板转换为map 只获取前2做key 
     * @param <T> 
     * @param map   
     * @param class1 
     * @return 
     */  
    public  Map<String, BigDecimal> FreighToMap () {
    	Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();
    	
    	map.put("北京", this.beijingshi);
    	map.put("天津", this.tianjinshi);
    	map.put("河北", this.hebeisheng);
    	map.put("山西", this.shanxisheng);
    	map.put("内蒙", this.neimenggu);
    	map.put("辽宁", this.liaoningsheng);
    	map.put("吉林", this.jilinsheng);
    	map.put("黑龙", this.heilongjiangsheng);
    	map.put("上海", this.shanghaishi);
    	map.put("江苏", this.jiangsusheng);
    	map.put("浙江", this.zhejiangsheng);
    	map.put("安徽", this.anhuisheng);
    	map.put("福建", this.fujiansheng);
    	map.put("江西", this.jiangxisheng);
    	map.put("山东", this.shandongsheng);
    	map.put("河南", this.henansheng);
    	map.put("湖北", this.hubeisheng);
    	map.put("湖南", this.hunansheng);
    	map.put("广东", this.guangdongsheng);
    	map.put("广西", this.guangxi);
    	map.put("海南", this.hainansheng);
    	map.put("重庆", this.chongqingshi);
    	map.put("四川", this.sichuansheng);
    	map.put("贵州", this.guizhousheng);
    	map.put("云南", this.yunansheng);
    	map.put("西藏", this.xizang);
    	map.put("陕西", this.shanxisheng2);
    	map.put("甘肃", this.gansusheng);
    	map.put("青海", this.qinghaisheng);
    	map.put("宁夏", this.ningxia);
    	map.put("新疆", this.xinjiang);

		return map;  

    } 
	
    
    
    
    /** 
     *  
     *  
     * 对于的地址分析，记录对于的省份信息
     * @param <T> 
     * @param map   
     * @param class1 
     * @return 
     */  
    public  Map<String, String> territoryToMap () {
    	Map<String, String> map = new HashMap<String, String>();
    	
    	map.put("北京", "北京市");
    	map.put("天津", "天津市");
    	map.put("河北", "河北省");
    	map.put("山西", "山西省");
    	map.put("内蒙", "内蒙古自治区");
    	map.put("辽宁", "辽宁省");
    	map.put("吉林", "吉林省");
    	map.put("黑龙", "黑龙江省");
    	map.put("上海", "上海市");
    	map.put("江苏", "江苏省");
    	map.put("浙江", "浙江省");
    	map.put("安徽", "安徽省");
    	map.put("福建", "福建省");
    	map.put("江西", "江西省");
    	map.put("山东", "山东省");
    	map.put("河南", "河南省");
    	map.put("湖北", "湖北省");
    	map.put("湖南", "湖南省");
    	map.put("广东", "广东省");
    	map.put("广西", "广西壮族自治区");
    	map.put("海南", "海南省");
    	map.put("重庆", "重庆市");
    	map.put("四川", "四川省");
    	map.put("贵州", "贵州省");
    	map.put("云南", "云南省");
    	map.put("西藏", "西藏自治区");
    	map.put("陕西", "陕西省");
    	map.put("甘肃", "甘肃省");
    	map.put("青海", "青海省");
    	map.put("宁夏", "宁夏回族自治区");
    	map.put("新疆", "新疆维吾尔自治区");

		return map;  

    }
	
}
