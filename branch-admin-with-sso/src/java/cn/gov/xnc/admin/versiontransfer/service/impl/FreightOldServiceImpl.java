package cn.gov.xnc.admin.versiontransfer.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightDetailsTerritoryEntity;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.admin.versiontransfer.enity.FreightOldEntity;
import cn.gov.xnc.admin.versiontransfer.service.FreightOldServiceI;

@Service("freightOldService")

public class FreightOldServiceImpl extends CommonServiceImpl implements FreightOldServiceI {

	@Override
	public List<FreightDetailsEntity> buildFreightDetail(FreightEntity freightEntity, FreightOldEntity freightOldEntity) {
		List<FreightDetailsEntity> freightDetaillist = new ArrayList<FreightDetailsEntity>();
		List<FreightDetailsTerritoryEntity> freightDetailsTerritoryList = new ArrayList<FreightDetailsTerritoryEntity>();
		
		if(null != freightOldEntity){
			
//			for (FreightOldEntity freightOldEntity : freightOldList) {
				
				//安徽
				FreightDetailsEntity freightDetail = new FreightDetailsEntity();
				freightDetail.setId(IdWorker.generateSequenceNo());
				freightDetail.setFreightid(freightEntity);
				freightDetail.setQyfreightunit(1);
				freightDetail.setQyfreightpic(freightOldEntity.getAnhuisheng());
				freightDetail.setQyfreightmoreunit(1);
				freightDetail.setQyfreightmorepic(freightOldEntity.getAnhuisheng());
				freightDetaillist.add(freightDetail);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory.setFreighdetailsid(freightDetail);
				freightDetailsTerritory.setTerritoryid("40288030540efeeb01540eff531b0070");
				freightDetailsTerritory.setTerritoryname("安徽省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory);
				
				//澳门
				FreightDetailsEntity freightDetail1 = new FreightDetailsEntity();
				freightDetail1.setId(IdWorker.generateSequenceNo());
				freightDetail1.setFreightid(freightEntity);
				freightDetail1.setQyfreightunit(1);
				freightDetail1.setQyfreightpic(freightOldEntity.getAnmen());
				freightDetail1.setQyfreightmoreunit(1);
				freightDetail1.setQyfreightmorepic(freightOldEntity.getAnmen());
				freightDetaillist.add(freightDetail1);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory1 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory1.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory1.setFreighdetailsid(freightDetail1);
				freightDetailsTerritory1.setTerritoryid("40288030540efeeb01540eff7576017b");
				freightDetailsTerritory1.setTerritoryname("澳门特别行政区");
				freightDetailsTerritoryList.add(freightDetailsTerritory1);
				
				//getBeijingshi
				FreightDetailsEntity freightDetail2 = new FreightDetailsEntity();
				freightDetail2.setId(IdWorker.generateSequenceNo());
				freightDetail2.setFreightid(freightEntity);
				freightDetail2.setQyfreightunit(1);
				freightDetail2.setQyfreightpic(freightOldEntity.getBeijingshi());
				freightDetail2.setQyfreightmoreunit(1);
				freightDetail2.setQyfreightmorepic(freightOldEntity.getBeijingshi());
				freightDetaillist.add(freightDetail2);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory2 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory2.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory2.setFreighdetailsid(freightDetail2);
				freightDetailsTerritory2.setTerritoryid("40288030540efeeb01540eff49df0001");
				freightDetailsTerritory2.setTerritoryname("北京市");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory2);
				
				//重庆市
				FreightDetailsEntity freightDetail3 = new FreightDetailsEntity();
				freightDetail3.setId(IdWorker.generateSequenceNo());
				freightDetail3.setFreightid(freightEntity);
				freightDetail3.setQyfreightunit(1);
				freightDetail3.setQyfreightpic(freightOldEntity.getChongqingshi());
				freightDetail3.setQyfreightmoreunit(1);
				freightDetail3.setQyfreightmorepic(freightOldEntity.getChongqingshi());
				freightDetaillist.add(freightDetail3);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory3 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory3.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory3.setFreighdetailsid(freightDetail3);
				freightDetailsTerritory3.setTerritoryid("40288030540efeeb01540eff63ca0103");
				freightDetailsTerritory3.setTerritoryname("重庆市");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory3);
				
				//Fujiansheng
				FreightDetailsEntity freightDetail4 = new FreightDetailsEntity();
				freightDetail4.setId(IdWorker.generateSequenceNo());
				freightDetail4.setFreightid(freightEntity);
				freightDetail4.setQyfreightunit(1);
				freightDetail4.setQyfreightpic(freightOldEntity.getFujiansheng());
				freightDetail4.setQyfreightmoreunit(1);
				freightDetail4.setQyfreightmorepic(freightOldEntity.getFujiansheng());
				freightDetaillist.add(freightDetail4);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory4 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory4.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory4.setFreighdetailsid(freightDetail4);
				freightDetailsTerritory4.setTerritoryid("40288030540efeeb01540eff54da0082");
				freightDetailsTerritory4.setTerritoryname("福建省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory4);
				
				//Gansusheng
				FreightDetailsEntity freightDetail5 = new FreightDetailsEntity();
				freightDetail5.setId(IdWorker.generateSequenceNo());
				freightDetail5.setFreightid(freightEntity);
				freightDetail5.setQyfreightunit(1);
				freightDetail5.setQyfreightpic(freightOldEntity.getGansusheng());
				freightDetail5.setQyfreightmoreunit(1);
				freightDetail5.setQyfreightmorepic(freightOldEntity.getGansusheng());
				freightDetaillist.add(freightDetail5);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory5 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory5.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory5.setFreighdetailsid(freightDetail5);
				freightDetailsTerritory5.setTerritoryid("40288030540efeeb01540eff6de3014b");
				freightDetailsTerritory5.setTerritoryname("甘肃省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory5);
				
				//Guangdongsheng
				FreightDetailsEntity freightDetail6 = new FreightDetailsEntity();
				freightDetail6.setId(IdWorker.generateSequenceNo());
				freightDetail6.setFreightid(freightEntity);
				freightDetail6.setQyfreightunit(1);
				freightDetail6.setQyfreightpic(freightOldEntity.getGuangdongsheng());
				freightDetail6.setQyfreightmoreunit(1);
				freightDetail6.setQyfreightmorepic(freightOldEntity.getGuangdongsheng());
				freightDetaillist.add(freightDetail6);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory6 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory6.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory6.setFreighdetailsid(freightDetail6);
				freightDetailsTerritory6.setTerritoryid("40288030540efeeb01540eff5e8d00da");
				freightDetailsTerritory6.setTerritoryname("广东省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory6);
				
				
				//getGuangxi
				FreightDetailsEntity freightDetail7 = new FreightDetailsEntity();
				freightDetail7.setId(IdWorker.generateSequenceNo());
				freightDetail7.setFreightid(freightEntity);
				freightDetail7.setQyfreightunit(1);
				freightDetail7.setQyfreightpic(freightOldEntity.getGuangxi());
				freightDetail7.setQyfreightmoreunit(1);
				freightDetail7.setQyfreightmorepic(freightOldEntity.getGuangxi());
				freightDetaillist.add(freightDetail7);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory7 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory7.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory7.setFreighdetailsid(freightDetail7);
				freightDetailsTerritory7.setTerritoryid("40288030540efeeb01540eff615d00f0");
				freightDetailsTerritory7.setTerritoryname("广西壮族自治区");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory7);
				
				//getGuizhousheng
				FreightDetailsEntity freightDetail8 = new FreightDetailsEntity();
				freightDetail8.setId(IdWorker.generateSequenceNo());
				freightDetail8.setFreightid(freightEntity);
				freightDetail8.setQyfreightunit(1);
				freightDetail8.setQyfreightpic(freightOldEntity.getGuizhousheng());
				freightDetail8.setQyfreightmoreunit(1);
				freightDetail8.setQyfreightmorepic(freightOldEntity.getGuizhousheng());
				freightDetaillist.add(freightDetail8);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory8 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory8.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory8.setFreighdetailsid(freightDetail8);
				freightDetailsTerritory8.setTerritoryid("40288030540efeeb01540eff6764011d");
				freightDetailsTerritory8.setTerritoryname("贵州省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory8);
				
				//getHainansheng
				FreightDetailsEntity freightDetail9 = new FreightDetailsEntity();
				freightDetail9.setId(IdWorker.generateSequenceNo());
				freightDetail9.setFreightid(freightEntity);
				freightDetail9.setQyfreightunit(1);
				freightDetail9.setQyfreightpic(freightOldEntity.getHainansheng());
				freightDetail9.setQyfreightmoreunit(1);
				freightDetail9.setQyfreightmorepic(freightOldEntity.getHainansheng());
				freightDetaillist.add(freightDetail9);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory9 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory9.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory9.setFreighdetailsid(freightDetail9);
				freightDetailsTerritory9.setTerritoryid("40288030540efeeb01540eff633e00ff");
				freightDetailsTerritory9.setTerritoryname("海南省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory9);
				
				//getHebeisheng
				FreightDetailsEntity freightDetail10 = new FreightDetailsEntity();
				freightDetail10.setId(IdWorker.generateSequenceNo());
				freightDetail10.setFreightid(freightEntity);
				freightDetail10.setQyfreightunit(1);
				freightDetail10.setQyfreightpic(freightOldEntity.getHebeisheng());
				freightDetail10.setQyfreightmoreunit(1);
				freightDetail10.setQyfreightmorepic(freightOldEntity.getHebeisheng());
				freightDetaillist.add(freightDetail10);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory10 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory10.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory10.setFreighdetailsid(freightDetail10);
				freightDetailsTerritory10.setTerritoryid("40288030540efeeb01540eff4a5b0007");
				freightDetailsTerritory10.setTerritoryname("河北省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory10);
				
				
				//getHeilongjiangsheng
				FreightDetailsEntity freightDetail11 = new FreightDetailsEntity();
				freightDetail11.setId(IdWorker.generateSequenceNo());
				freightDetail11.setFreightid(freightEntity);
				freightDetail11.setQyfreightunit(1);
				freightDetail11.setQyfreightpic(freightOldEntity.getHeilongjiangsheng());
				freightDetail11.setQyfreightmoreunit(1);
				freightDetail11.setQyfreightmorepic(freightOldEntity.getHeilongjiangsheng());
				freightDetaillist.add(freightDetail11);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory11 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory11.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory11.setFreighdetailsid(freightDetail11);
				freightDetailsTerritory11.setTerritoryid("40288030540efeeb01540eff4f430045");
				freightDetailsTerritory11.setTerritoryname("黑龙江省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory11);
				
				//getHenansheng
				FreightDetailsEntity freightDetail12 = new FreightDetailsEntity();
				freightDetail12.setId(IdWorker.generateSequenceNo());
				freightDetail12.setFreightid(freightEntity);
				freightDetail12.setQyfreightunit(1);
				freightDetail12.setQyfreightpic(freightOldEntity.getHenansheng());
				freightDetail12.setQyfreightmoreunit(1);
				freightDetail12.setQyfreightmorepic(freightOldEntity.getHenansheng());
				freightDetaillist.add(freightDetail12);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory12 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory12.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory12.setFreighdetailsid(freightDetail12);
				freightDetailsTerritory12.setTerritoryid("40288030540efeeb01540eff58f200aa");
				freightDetailsTerritory12.setTerritoryname("河南省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory12);
				
				//getHubeisheng
				FreightDetailsEntity freightDetail13 = new FreightDetailsEntity();
				freightDetail13.setId(IdWorker.generateSequenceNo());
				freightDetail13.setFreightid(freightEntity);
				freightDetail13.setQyfreightunit(1);
				freightDetail13.setQyfreightpic(freightOldEntity.getHubeisheng());
				freightDetail13.setQyfreightmoreunit(1);
				freightDetail13.setQyfreightmorepic(freightOldEntity.getHubeisheng());
				freightDetaillist.add(freightDetail13);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory13 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory13.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory13.setFreighdetailsid(freightDetail13);
				freightDetailsTerritory13.setTerritoryid("40288030540efeeb01540eff5af100bc");
				freightDetailsTerritory13.setTerritoryname("湖北省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory13);
				
				
				//getHunansheng
				FreightDetailsEntity freightDetail14 = new FreightDetailsEntity();
				freightDetail14.setId(IdWorker.generateSequenceNo());
				freightDetail14.setFreightid(freightEntity);
				freightDetail14.setQyfreightunit(1);
				freightDetail14.setQyfreightpic(freightOldEntity.getHunansheng());
				freightDetail14.setQyfreightmoreunit(1);
				freightDetail14.setQyfreightmorepic(freightOldEntity.getHunansheng());
				freightDetaillist.add(freightDetail14);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory14 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory14.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory14.setFreighdetailsid(freightDetail14);
				freightDetailsTerritory14.setTerritoryid("40288030540efeeb01540eff5cc100cb");
				freightDetailsTerritory14.setTerritoryname("湖南省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory14);
				
				
				//getJiangsusheng
				FreightDetailsEntity freightDetail15 = new FreightDetailsEntity();
				freightDetail15.setId(IdWorker.generateSequenceNo());
				freightDetail15.setFreightid(freightEntity);
				freightDetail15.setQyfreightunit(1);
				freightDetail15.setQyfreightpic(freightOldEntity.getJiangsusheng());
				freightDetail15.setQyfreightmoreunit(1);
				freightDetail15.setQyfreightmorepic(freightOldEntity.getJiangsusheng());
				freightDetaillist.add(freightDetail15);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory15 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory15.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory15.setFreighdetailsid(freightDetail15);
				freightDetailsTerritory15.setTerritoryid("40288030540efeeb01540eff50da0056");
				freightDetailsTerritory15.setTerritoryname("江苏省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory15);
				
				//getJiangxisheng
				FreightDetailsEntity freightDetail16 = new FreightDetailsEntity();
				freightDetail16.setId(IdWorker.generateSequenceNo());
				freightDetail16.setFreightid(freightEntity);
				freightDetail16.setQyfreightunit(1);
				freightDetail16.setQyfreightpic(freightOldEntity.getJiangxisheng());
				freightDetail16.setQyfreightmoreunit(1);
				freightDetail16.setQyfreightmorepic(freightOldEntity.getJiangxisheng());
				freightDetaillist.add(freightDetail16);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory16 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory16.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory16.setFreighdetailsid(freightDetail16);
				freightDetailsTerritory16.setTerritoryid("40288030540efeeb01540eff55d0008c");
				freightDetailsTerritory16.setTerritoryname("江西省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory16);
				
				
				//getJilinsheng
				FreightDetailsEntity freightDetail17 = new FreightDetailsEntity();
				freightDetail17.setId(IdWorker.generateSequenceNo());
				freightDetail17.setFreightid(freightEntity);
				freightDetail17.setQyfreightunit(1);
				freightDetail17.setQyfreightpic(freightOldEntity.getJilinsheng());
				freightDetail17.setQyfreightmoreunit(1);
				freightDetail17.setQyfreightmorepic(freightOldEntity.getJilinsheng());
				freightDetaillist.add(freightDetail17);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory17 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory17.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory17.setFreighdetailsid(freightDetail17);
				freightDetailsTerritory17.setTerritoryid("40288030540efeeb01540eff4e73003b");
				freightDetailsTerritory17.setTerritoryname("吉林省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory17);
				
				
				//getLiaoningsheng
				FreightDetailsEntity freightDetail18 = new FreightDetailsEntity();
				freightDetail18.setId(IdWorker.generateSequenceNo());
				freightDetail18.setFreightid(freightEntity);
				freightDetail18.setQyfreightunit(1);
				freightDetail18.setQyfreightpic(freightOldEntity.getLiaoningsheng());
				freightDetail18.setQyfreightmoreunit(1);
				freightDetail18.setQyfreightmorepic(freightOldEntity.getLiaoningsheng());
				freightDetaillist.add(freightDetail18);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory18 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory18.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory18.setFreighdetailsid(freightDetail18);
				freightDetailsTerritory18.setTerritoryid("40288030540efeeb01540eff4d5c002c");
				freightDetailsTerritory18.setTerritoryname("辽宁省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory18);
				
				
				//getNeimenggu
				FreightDetailsEntity freightDetail19 = new FreightDetailsEntity();
				freightDetail19.setId(IdWorker.generateSequenceNo());
				freightDetail19.setFreightid(freightEntity);
				freightDetail19.setQyfreightunit(1);
				freightDetail19.setQyfreightpic(freightOldEntity.getNeimenggu());
				freightDetail19.setQyfreightmoreunit(1);
				freightDetail19.setQyfreightmorepic(freightOldEntity.getNeimenggu());
				freightDetaillist.add(freightDetail19);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory19 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory19.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory19.setFreighdetailsid(freightDetail19);
				freightDetailsTerritory19.setTerritoryid("40288030540efeeb01540eff4c30001f");
				freightDetailsTerritory19.setTerritoryname("内蒙古自治区");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory19);
				
				
				//getNingxia
				FreightDetailsEntity freightDetail20 = new FreightDetailsEntity();
				freightDetail20.setId(IdWorker.generateSequenceNo());
				freightDetail20.setFreightid(freightEntity);
				freightDetail20.setQyfreightunit(1);
				freightDetail20.setQyfreightpic(freightOldEntity.getNingxia());
				freightDetail20.setQyfreightmoreunit(1);
				freightDetail20.setQyfreightmorepic(freightOldEntity.getNingxia());
				freightDetaillist.add(freightDetail20);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory20 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory20.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory20.setFreighdetailsid(freightDetail20);
				freightDetailsTerritory20.setTerritoryid("40288030540efeeb01540eff71640163");
				freightDetailsTerritory20.setTerritoryname("宁夏回族自治区");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory20);
				
				//getQinghaisheng
				FreightDetailsEntity freightDetail21 = new FreightDetailsEntity();
				freightDetail21.setId(IdWorker.generateSequenceNo());
				freightDetail21.setFreightid(freightEntity);
				freightDetail21.setQyfreightunit(1);
				freightDetail21.setQyfreightpic(freightOldEntity.getQinghaisheng());
				freightDetail21.setQyfreightmoreunit(1);
				freightDetail21.setQyfreightmorepic(freightOldEntity.getQinghaisheng());
				freightDetaillist.add(freightDetail21);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory21 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory21.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory21.setFreighdetailsid(freightDetail21);
				freightDetailsTerritory21.setTerritoryid("40288030540efeeb01540eff700d015a");
				freightDetailsTerritory21.setTerritoryname("青海省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory21);
				
				
				//getZhejiangsheng
				FreightDetailsEntity freightDetail22 = new FreightDetailsEntity();
				freightDetail22.setId(IdWorker.generateSequenceNo());
				freightDetail22.setFreightid(freightEntity);
				freightDetail22.setQyfreightunit(1);
				freightDetail22.setQyfreightpic(freightOldEntity.getZhejiangsheng());
				freightDetail22.setQyfreightmoreunit(1);
				freightDetail22.setQyfreightmorepic(freightOldEntity.getZhejiangsheng());
				freightDetaillist.add(freightDetail22);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory22 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory22.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory22.setFreighdetailsid(freightDetail22);
				freightDetailsTerritory22.setTerritoryid("40288030540efeeb01540eff52000064");
				freightDetailsTerritory22.setTerritoryname("浙江省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory22);
				
				//getYunansheng
				FreightDetailsEntity freightDetail23 = new FreightDetailsEntity();
				freightDetail23.setId(IdWorker.generateSequenceNo());
				freightDetail23.setFreightid(freightEntity);
				freightDetail23.setQyfreightunit(1);
				freightDetail23.setQyfreightpic(freightOldEntity.getYunansheng());
				freightDetail23.setQyfreightmoreunit(1);
				freightDetail23.setQyfreightmorepic(freightOldEntity.getYunansheng());
				freightDetaillist.add(freightDetail23);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory23 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory23.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory23.setFreighdetailsid(freightDetail23);
				freightDetailsTerritory23.setTerritoryid("40288030540efeeb01540eff68cc0127");
				freightDetailsTerritory23.setTerritoryname("云南省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory23);
				
				//getXizang
				FreightDetailsEntity freightDetail24 = new FreightDetailsEntity();
				freightDetail24.setId(IdWorker.generateSequenceNo());
				freightDetail24.setFreightid(freightEntity);
				freightDetail24.setQyfreightunit(1);
				freightDetail24.setQyfreightpic(freightOldEntity.getXizang());
				freightDetail24.setQyfreightmoreunit(1);
				freightDetail24.setQyfreightmorepic(freightOldEntity.getXizang());
				freightDetaillist.add(freightDetail24);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory24 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory24.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory24.setFreighdetailsid(freightDetail24);
				freightDetailsTerritory24.setTerritoryid("40288030540efeeb01540eff68cc0127");
				freightDetailsTerritory24.setTerritoryname("西藏自治区");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory24);
				
				
				//getXinjiang
				FreightDetailsEntity freightDetail25 = new FreightDetailsEntity();
				freightDetail25.setId(IdWorker.generateSequenceNo());
				freightDetail25.setFreightid(freightEntity);
				freightDetail25.setQyfreightunit(1);
				freightDetail25.setQyfreightpic(freightOldEntity.getXinjiang());
				freightDetail25.setQyfreightmoreunit(1);
				freightDetail25.setQyfreightmorepic(freightOldEntity.getXinjiang());
				freightDetaillist.add(freightDetail25);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory25 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory25.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory25.setFreighdetailsid(freightDetail25);
				freightDetailsTerritory25.setTerritoryid("40288030540efeeb01540eff723f0169");
				freightDetailsTerritory25.setTerritoryname("新疆维吾尔自治区");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory25);
				
				
				//getXianggang
				FreightDetailsEntity freightDetail26 = new FreightDetailsEntity();
				freightDetail26.setId(IdWorker.generateSequenceNo());
				freightDetail26.setFreightid(freightEntity);
				freightDetail26.setQyfreightunit(1);
				freightDetail26.setQyfreightpic(freightOldEntity.getXianggang());
				freightDetail26.setQyfreightmoreunit(1);
				freightDetail26.setQyfreightmorepic(freightOldEntity.getXianggang());
				freightDetaillist.add(freightDetail26);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory26 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory26.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory26.setFreighdetailsid(freightDetail26);
				freightDetailsTerritory26.setTerritoryid("40288030540efeeb01540eff7554017a");
				freightDetailsTerritory26.setTerritoryname("香港特别行政区");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory26);
				
				//getTianjinshi
				FreightDetailsEntity freightDetail27 = new FreightDetailsEntity();
				freightDetail27.setId(IdWorker.generateSequenceNo());
				freightDetail27.setFreightid(freightEntity);
				freightDetail27.setQyfreightunit(1);
				freightDetail27.setQyfreightpic(freightOldEntity.getTianjinshi());
				freightDetail27.setQyfreightmoreunit(1);
				freightDetail27.setQyfreightmorepic(freightOldEntity.getTianjinshi());
				freightDetaillist.add(freightDetail27);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory27 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory27.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory27.setFreighdetailsid(freightDetail27);
				freightDetailsTerritory27.setTerritoryid("40288030540efeeb01540eff4a2a0004");
				freightDetailsTerritory27.setTerritoryname("天津市");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory27);
				
				
				//getTaiwansheng
				FreightDetailsEntity freightDetail28 = new FreightDetailsEntity();
				freightDetail28.setId(IdWorker.generateSequenceNo());
				freightDetail28.setFreightid(freightEntity);
				freightDetail28.setQyfreightunit(1);
				freightDetail28.setQyfreightpic(freightOldEntity.getTaiwansheng());
				freightDetail28.setQyfreightmoreunit(1);
				freightDetail28.setQyfreightmorepic(freightOldEntity.getTaiwansheng());
				freightDetaillist.add(freightDetail28);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory28 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory28.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory28.setFreighdetailsid(freightDetail28);
				freightDetailsTerritory28.setTerritoryid("40288030540efeeb01540eff75270179");
				freightDetailsTerritory28.setTerritoryname("台湾省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory28);
				
				
				//getSichuansheng
				FreightDetailsEntity freightDetail29 = new FreightDetailsEntity();
				freightDetail29.setId(IdWorker.generateSequenceNo());
				freightDetail29.setFreightid(freightEntity);
				freightDetail29.setQyfreightunit(1);
				freightDetail29.setQyfreightpic(freightOldEntity.getSichuansheng());
				freightDetail29.setQyfreightmoreunit(1);
				freightDetail29.setQyfreightmorepic(freightOldEntity.getSichuansheng());
				freightDetaillist.add(freightDetail29);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory29 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory29.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory29.setFreighdetailsid(freightDetail29);
				freightDetailsTerritory29.setTerritoryid("40288030540efeeb01540eff64590107");
				freightDetailsTerritory29.setTerritoryname("四川省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory29);
				
				
				//getShanxisheng2
				FreightDetailsEntity freightDetail30 = new FreightDetailsEntity();
				freightDetail30.setId(IdWorker.generateSequenceNo());
				freightDetail30.setFreightid(freightEntity);
				freightDetail30.setQyfreightunit(1);
				freightDetail30.setQyfreightpic(freightOldEntity.getShanxisheng2());
				freightDetail30.setQyfreightmoreunit(1);
				freightDetail30.setQyfreightmorepic(freightOldEntity.getShanxisheng2());
				freightDetaillist.add(freightDetail30);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory30 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory30.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory30.setFreighdetailsid(freightDetail30);
				freightDetailsTerritory30.setTerritoryid("40288030540efeeb01540eff6c490140");
				freightDetailsTerritory30.setTerritoryname("陕西省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory30);
				
				//getShanxisheng
				FreightDetailsEntity freightDetail31 = new FreightDetailsEntity();
				freightDetail31.setId(IdWorker.generateSequenceNo());
				freightDetail31.setFreightid(freightEntity);
				freightDetail31.setQyfreightunit(1);
				freightDetail31.setQyfreightpic(freightOldEntity.getShanxisheng());
				freightDetail31.setQyfreightmoreunit(1);
				freightDetail31.setQyfreightmorepic(freightOldEntity.getShanxisheng());
				freightDetaillist.add(freightDetail31);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory31 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory31.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory31.setFreighdetailsid(freightDetail31);
				freightDetailsTerritory31.setTerritoryid("40288030540efeeb01540eff4b480013");
				freightDetailsTerritory31.setTerritoryname("山西省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory31);
				
				
				//getShanghaishi
				FreightDetailsEntity freightDetail32 = new FreightDetailsEntity();
				freightDetail32.setId(IdWorker.generateSequenceNo());
				freightDetail32.setFreightid(freightEntity);
				freightDetail32.setQyfreightunit(1);
				freightDetail32.setQyfreightpic(freightOldEntity.getShanghaishi());
				freightDetail32.setQyfreightmoreunit(1);
				freightDetail32.setQyfreightmorepic(freightOldEntity.getShanghaishi());
				freightDetaillist.add(freightDetail32);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory32 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory32.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory32.setFreighdetailsid(freightDetail32);
				freightDetailsTerritory32.setTerritoryid("40288030540efeeb01540eff50980053");
				freightDetailsTerritory32.setTerritoryname("上海市");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory32);
				
				
				//getShandongsheng
				FreightDetailsEntity freightDetail33 = new FreightDetailsEntity();
				freightDetail33.setId(IdWorker.generateSequenceNo());
				freightDetail33.setFreightid(freightEntity);
				freightDetail33.setQyfreightunit(1);
				freightDetail33.setQyfreightpic(freightOldEntity.getShandongsheng());
				freightDetail33.setQyfreightmoreunit(1);
				freightDetail33.setQyfreightmorepic(freightOldEntity.getShandongsheng());
				freightDetaillist.add(freightDetail33);
				
				
				FreightDetailsTerritoryEntity  freightDetailsTerritory33 = new FreightDetailsTerritoryEntity();
				freightDetailsTerritory33.setId(IdWorker.generateSequenceNo());
				freightDetailsTerritory33.setFreighdetailsid(freightDetail33);
				freightDetailsTerritory33.setTerritoryid("40288030540efeeb01540eff57090098");
				freightDetailsTerritory33.setTerritoryname("山东省");
				
				freightDetailsTerritoryList.add(freightDetailsTerritory33);
				
//			}
		}
		
		if(freightDetaillist.size() > 0 && freightDetailsTerritoryList.size() > 0){
			
			batchSave(freightDetaillist);
			
			batchSave(freightDetailsTerritoryList);
		}
		
		return freightDetaillist;
	}
	
}