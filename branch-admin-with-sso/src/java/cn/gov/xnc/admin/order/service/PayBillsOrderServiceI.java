package cn.gov.xnc.admin.order.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface PayBillsOrderServiceI extends CommonService{
	//订单状态枚举
	public enum  State{
		UNPAID("待付", "1"), PAID("已付", "2"), NOTALLPAID("未付清", "3"), CANCEL("已取消", "4"), REBACK("已退单", "5");
	    
		private String name ;
	    private String value ;
	     
	    private State( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
	
	//订单货运状态枚举
	public enum  FreightStatus{
//		UNDELIVERED("待发货", "2"), DELIVERED("已发货", "3"), NOTALLDELIVERED("部分发货", "5"), WAIT2PACK("等待打包", "1"),  STOCKOUT("已出库","6"), RECEIVED("已签收","4");
		UNDELIVERED("待发货", "1"), STOCKOUT("已出库","2"), WAIT2PACK("正在打包", "3"), NOTALLDELIVERED("部分发货", "4"), DELIVERED("已发货", "5"),  RECEIVED("已签收","6");

		private String name ;
	    private String value ;
	     
	    private FreightStatus( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
	
	//批量创建产品订单list
	
	/**
	 * 获取采购商支付流水统计数据
	 * @param company
	 * @param user
	 * @return
	 */
	public StatisPageVO statisUserPaybills(TSCompany company, TSUser user, HttpServletRequest request);
	
	/**
	 * 根据订单id获取订单列表
	 * @param billsIds
	 * @return
	 */
	public List<PayBillsOrderEntity> getBillsOrderListByIds(String billsIds);
	
	/**
	 * 先货后款
	 * @param payBillsOrder
	 * @param request
	 * @return
	 */
	public AjaxJson sendFirstPaymentAudit(PayBillsOrderEntity payBillsOrder, HttpServletRequest request);
	
	/**
	 * 获取订单物流状态
	 * @param paybillsOrder
	 * @return
	 */
	public TSDictionary getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder);
	
	
	public AjaxJson cancelPayBillsOrder(PayBillsOrderEntity paybillsOrder, HttpServletRequest request) throws Exception;
	
	/**
	 * 判断商品数量全部退完则更新大订单状态
	 * @param payBillsOrder
	 */
	public void setRebackStatus(PayBillsOrderEntity payBillsOrder);
	
	/**
	 * 下单自动出库
	 * 根据支付流水
	 * @param payBills
	 * @return
	 */
	public AjaxJson payBillsOrderStockOut(PayBillsEntity payBills,HttpServletRequest request);
	
	/**
	 * 下单自动出库
	 * 根据支付订单
	 * @param payBillsOrder
	 * @return
	 */
	public AjaxJson payBillsOrderStockOut(PayBillsOrderEntity payBillsOrder,HttpServletRequest request);
	
	/**
	 * 检查订单内商品是否充足
	 * @param payBillsOrder
	 * @return
	 * @throws Exception
	 */
	public AjaxJson checkBillsOrderProductStockEnough(PayBillsOrderEntity payBillsOrder) throws Exception;
	
	/**
	 * 订单变更为已签收
	 * @param payBillsOrder
	 * @return
	 * @throws Exception
	 */
	public AjaxJson billsOrder2Received(PayBillsOrderEntity payBillsOrder) throws Exception;
	
}
