package cn.gov.xnc.admin.order.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.service.SystemService;

import cn.gov.xnc.admin.order.entity.PayBillsFlowEntity;
import cn.gov.xnc.admin.order.service.PayBillsFlowServiceI;

/**   
 * @Title: Controller
 * @Description: 支付流水订单对应关系
 * @author zero
 * @date 2016-10-05 16:33:09
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/payBillsFlowController")
public class PayBillsFlowController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PayBillsFlowController.class);

	@Autowired
	private PayBillsFlowServiceI payBillsFlowService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 支付流水订单对应关系列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView payBillsFlow(HttpServletRequest request) {
		
		String payBillsId = request.getParameter("payBillsId");
		
		request.setAttribute("payBills_id", payBillsId );
		
		
		return new ModelAndView("admin/order/payBillsFlowList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(PayBillsFlowEntity payBillsFlow,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(PayBillsFlowEntity.class, dataGrid);
		//查询条件组装器
		//cq.addOrder("createdate", SortDirection.desc);
		
//		cq.createAlias("billsorderid", "b"); 
//	    cq.createAlias("b.clientid", "c") ;
//	    cq.add( Restrictions.eq("c.id", user.getId()) ) ;
	
	    cq.addOrder("billsid.createdate", SortDirection.desc);
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, payBillsFlow, request.getParameterMap());
		this.payBillsFlowService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

//	/**
//	 * 删除支付流水订单对应关系
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "del")
//	@ResponseBody
//	public AjaxJson del(PayBillsFlowEntity payBillsFlow, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		payBillsFlow = systemService.getEntity(PayBillsFlowEntity.class, payBillsFlow.getId());
//		message = "支付流水订单对应关系删除成功";
//		payBillsFlowService.delete(payBillsFlow);
//		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
//		
//		j.setMsg(message);
//		return j;
//	}
//
//
//	/**
//	 * 添加支付流水订单对应关系
//	 * 
//	 * @param ids
//	 * @return
//	 */
//	@RequestMapping(params = "save")
//	@ResponseBody
//	public AjaxJson save(PayBillsFlowEntity payBillsFlow, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		if (StringUtil.isNotEmpty(payBillsFlow.getId())) {
//			message = "支付流水订单对应关系更新成功";
//			PayBillsFlowEntity t = payBillsFlowService.get(PayBillsFlowEntity.class, payBillsFlow.getId());
//			try {
//				MyBeanUtils.copyBeanNotNull2Bean(payBillsFlow, t);
//				payBillsFlowService.saveOrUpdate(t);
//				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
//			} catch (Exception e) {
//				e.printStackTrace();
//				message = "支付流水订单对应关系更新失败";
//			}
//		} else {
//			message = "支付流水订单对应关系添加成功";
//			payBillsFlowService.save(payBillsFlow);
//			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
//		}
//		j.setMsg(message);
//		return j;
//	}
//
//	/**
//	 * 支付流水订单对应关系列表页面跳转
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "addorupdate")
//	public ModelAndView addorupdate(PayBillsFlowEntity payBillsFlow, HttpServletRequest req) {
//		if (StringUtil.isNotEmpty(payBillsFlow.getId())) {
//			payBillsFlow = payBillsFlowService.getEntity(PayBillsFlowEntity.class, payBillsFlow.getId());
//			req.setAttribute("payBillsFlowPage", payBillsFlow);
//		}
//		return new ModelAndView("admin/order/payBillsFlow");
//	}
}
