package cn.gov.xnc.admin.order.controller;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.express.service.ExpressServiceI;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticSubscribeServiceI;
import cn.gov.xnc.admin.order.service.OrderLogisticsServiceI;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanCommissionServiceI;
import cn.gov.xnc.admin.verify.service.VerifyOrderService;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;

/**   
 * @Title: Controller
 * @Description: 订单明细
 * @author zero
 * @date 2016-10-02 02:00:47
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderController")
public class OrderController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderController.class);
	@Autowired
	private MessageTemplateServiceI messageTemplateService ;
	@Autowired
	private SalesmanCommissionServiceI salesmanCommissionService ;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private FreightDetailsServiceI freightDetailsService;
	@Autowired
	private UserGradePriceServiceI userGradePriceService;
	@Autowired
	private ExpressServiceI expressService;
	@Autowired
	private OrderLogisticsServiceI orderLogisticsService;
	@Autowired
	private OrderLogisticSubscribeServiceI orderLogisticSubscribeService;
	@Autowired
	private VerifyOrderService verifyOrderService;
	@Autowired
	private DictionaryService dictionaryService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView order(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		
		//字典 - 物流状态
		List<TSDictionary> dictionarys = orderService.checkFreightState(null);
		request.setAttribute("dictionarys", dictionarys);
		
		if("6".equals(user.getType())){//发货商进入
			
			//待打包
			request.setAttribute("orderWaitPack", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.WAIT_PACK.getValue(), user));
			//待发货
			request.setAttribute("orderWaitDeliver", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.WAIT_DELIVER.getValue(), user));
			//已发货
			request.setAttribute("orderAlreadyDeliver", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.ALREADY_DELIVER.getValue(), user));
			//已签收
			request.setAttribute("orderAlreadySingn", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.ALREADY_SIGN.getValue(), user));
			//部分发货
			request.setAttribute("orderParitDeliver", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.PART_DELIVER.getValue(), user));
			
			return new ModelAndView("admin/order/copartnerOrderList");
			
		}else{//非发货商进入
			
			//待打包
			request.setAttribute("orderWaitPack", orderService.statisOrderByFreightState(company, OrderServiceI.FreightState.WAIT_PACK.getValue()));
			//待发货
			request.setAttribute("orderWaitDeliver", orderService.statisOrderByFreightState(company, OrderServiceI.FreightState.WAIT_DELIVER.getValue()));
			//已发货
			request.setAttribute("orderAlreadyDeliver", orderService.statisOrderByFreightState(company, OrderServiceI.FreightState.ALREADY_DELIVER.getValue()));
			//已签收
			request.setAttribute("orderAlreadySingn", orderService.statisOrderByFreightState(company, OrderServiceI.FreightState.ALREADY_SIGN.getValue()));
			//部分发货
			request.setAttribute("orderParitDeliver", orderService.statisOrderByFreightState(company, OrderServiceI.FreightState.PART_DELIVER.getValue()));
			
			return new ModelAndView("admin/order/orderList");
		}
		
	}
	
	@RequestMapping(value = "orderToRecieveList")
	public ModelAndView orderToRecieve(HttpServletRequest request) {
		//字典 - 物流状态
		List<TSDictionary> dictionarys = orderService.checkFreightState(null);
		request.setAttribute("dictionarys", dictionarys);
		
		return new ModelAndView("admin/order/orderToRecieveList");
		
	}
	
	
	
	/**
	 * 特殊状态订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderListState")
	public ModelAndView orderListState(HttpServletRequest request) {
		
		
		String state = request.getParameter("state");
		if(StringUtil.isNotEmpty(state)){
			request.setAttribute("state", state);
		}
		String orderDate = request.getParameter("orderDate"); 
		if(StringUtil.isNotEmpty(orderDate)){
			request.setAttribute("orderDate", orderDate);
		}
		
		if( "2".equals(state) ){
			return new ModelAndView("admin/order/orderListStatefahuo");
		}
		
		return new ModelAndView("admin/order/orderListState");
	}
	
	@RequestMapping(value = "printExpress")
	public String printExpress(OrderEntity order, HttpServletRequest request) {
		
		request.setAttribute("printtype", "single");
		request.setAttribute("orderids", order.getId());
		return "admin/order/printExpress";
	}
	
	@RequestMapping(value = "printAllExpress")
	public String printAllExpress(PayBillsOrderEntity paybillsorder, HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
		cq.eq("company", user.getCompany());
		cq.eq("billsorderid", paybillsorder);
		List<OrderEntity> orderlist = systemService.getListByCriteriaQuery(cq, false);
		StringBuffer orderids = new StringBuffer();
		for( int i = 0; i <  orderlist.size(); i++ ){
			OrderEntity order= orderlist.get(i);
			orderids.append(order.getId());
			if( i != (orderlist.size()-1)){
				orderids.append(",");
			}
		}
		
		
		request.setAttribute("printtype", "all");
		request.setAttribute("orderids", orderids);
		return "admin/order/printExpress";
	}
	
	@RequestMapping(value = "printAllSelectedExpress")
	public String printAllSelectedExpress(String orderids, HttpServletRequest request) {
		request.setAttribute("printtype", "all");
		request.setAttribute("orderids", orderids);
		return "admin/order/printExpress";
	}
	
	
	

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderEntity order,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		CriteriaQuery cq = orderService.getOrderListCriteriaQry(order, request, dataGrid);
		cq.setDataGrid(dataGrid);
		
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, order, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

//	/**
//	 * 删除订单明细
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "del")
//	@ResponseBody
//	public AjaxJson del(OrderEntity order, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//			order = systemService.get(OrderEntity.class, order.getId());
//			order.setState("5");
//			
//		message = "订单取消成功！";
//		systemService.saveOrUpdate(order);
//		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
//		
//		j.setMsg(message);
//		return j;
//	}


	
	
	/**
	 * 删除取消订单明细
	 * 
	 * @return
	 */
	@RequestMapping(value  = "cancelorder")
	@ResponseBody
	public AjaxJson cancelorder(OrderEntity order, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
			order = systemService.get(OrderEntity.class, order.getId());

		   //判断订单状态，如果未付款可以直接取消，如果是已付款，则发送取消审核单，由财务处理
			if("1".equals(order.getState())){//未付款
				message = "未付款订单取消成功！";
				order.setState("5");
				systemService.saveOrUpdate(order);
			//更新对应大单情况，金额做修改计算
				 PayBillsOrderEntity billsorderid = order.getBillsorderid();
			 	 BigDecimal moneyb = billsorderid.getObligationsmoney();//购买总结算
			 	 BigDecimal moneyp = billsorderid.getPayablemoney();//购买应付款

			 	   moneyb = moneyb.subtract((order.getPrice().add(order.getFreightpic())));
			 	   moneyp = moneyp.subtract((order.getPrice().add(order.getFreightpic())));
			 	   if( moneyb .compareTo( new BigDecimal("0.00") ) <= 0 ){//如果存在
			 		  billsorderid.setState("5");
			 	   }
				   billsorderid.setObligationsmoney(moneyb);
				   billsorderid.setPayablemoney(moneyp);
				   systemService.saveOrUpdate(billsorderid);
				   j.setSuccess(true);
			} else if("2".equals(order.getState())){//已付款
				message = "已付款订单提交取消申请成功，请等待审核！";
				order.setState("6");
				systemService.saveOrUpdate(order);
				//发送退单财务信息
				messageTemplateService.mosSendSmsDelivery_7(order);
				j.setSuccess(true);
			}else {
				message = "不符合订单取消状态，不能取消订单！";
				j.setSuccess(true);
			}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 添加订单明细,修改订单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderEntity order, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(order.getId())) {
			message = "订单明细更新成功";
			
			OrderEntity t = systemService.get(OrderEntity.class, order.getId());
			PayBillsOrderEntity billsorderid = t.getBillsorderid();
			String orderState = t.getState();
			String billsOrderStatus = billsorderid.getState();
			String freightState = t.getFreightState().getDictionaryValue();
			
			//只有大单未支付的情况（包括先货后款）才可以修改订单价格
			if( (OrderServiceI.State.UNPAID.getValue().equals(orderState) || OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(freightState)) 
					&& PayBillsOrderServiceI.State.UNPAID.getValue().equals(billsOrderStatus) ){
				String useraddress = request.getParameter("useraddress");
				if(StringUtil.isNotEmpty(useraddress)){
					order.setAddress(useraddress +"_"+ order.getAddress());
				}
				ProductEntity p = systemService.findUniqueByProperty (ProductEntity.class,"id" ,t.getProductid().getId());
				if( (StringUtil.isNotEmpty(order.getAddress()) && !order.getAddress().equals(t.getAddress()))
						|| order.getNumber().compareTo(t.getNumber()) !=0 ){
					
				   /** 功能：重新计算商品运费
					 * 
					 * 通过比较页面传回的运费和原运费，判断运费是否人工干涉
					 * 1、如果页面传回的运费不等于元订单运费，则说明人工干涉，此时采用页面传回的运费
					 * 2、如果页面传回的运费和元订单运费相同，则说明没有人工干涉，此时重新计算系统运费 */
					
					if( order.getFreightpic().compareTo(t.getFreightpic()) ==0 ){
						BigDecimal freightMoney = new BigDecimal("0.00"); // 运费价格
						try {
							freightMoney = freightDetailsService.getProductFreightByFullAddr(p, order.getAddress(), order.getNumber());
							
						} catch (Exception ex) {
							freightMoney = new BigDecimal("0.00");
						}
						order.setFreightpic(freightMoney);//写入新的运费信息	
					}
				}
				/** 功能：如果价格 发生价格变化 ，按输入的标准更新  计算修改后的价格，与原价格差，更新支付单
				 * 1.计算原订单总价格
				 * 2.计算新订单价格
				 * 3.计算价格变化差异，变更订单待支付和应付款金额
				 * */
				//1.原订单总价格
				BigDecimal totalPayMoneyO = t.getPrice().multiply(new BigDecimal(t.getNumber())).add(t.getFreightpic()); 
				//页面传回订单价格和系统订单价格相同，说明人工并未干预商品价格，重新计算等级价格
				if(order.getPrice().compareTo(t.getPrice()) ==0 && order.getNumber().compareTo(t.getNumber()) != 0){
					//计算用户等级价格
					TSUser client = systemService.findUniqueByProperty(TSUser.class, "id", order.getClientid().getId());
					BigDecimal productprice = userGradePriceService.getProductFinalPrice(p, client);
					order.setPrice( productprice.multiply(new BigDecimal(order.getNumber())));
				}
				//2.重新计算订单价格
				BigDecimal totalPayMoneyN = order.getPrice().multiply(new BigDecimal(order.getNumber())).add(order.getFreightpic());
				order.setTotalPrice(order.getPrice().multiply(new BigDecimal(order.getNumber())));//重新计算商品订单总价，不包含运费
				if( totalPayMoneyO.compareTo(totalPayMoneyN) !=0 ){ //存在价格差
					//读取支付单修改支付价格
					if( totalPayMoneyN.compareTo(totalPayMoneyO) == 1){ //金额价格变大
						//变化金额
						BigDecimal picMoneyB = totalPayMoneyN.subtract(totalPayMoneyO);
						  //payablemoney   应付款 增加 
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().add(picMoneyB)) ;//
						  //obligationsmoney 待付金额 增加
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().add(picMoneyB));//
						
					}else if(  totalPayMoneyN.compareTo(totalPayMoneyO) == -1 ){//金额价格减小
						  //payablemoney   应付款 减少
						BigDecimal picMoneyB = totalPayMoneyO.subtract(totalPayMoneyN);
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().subtract(picMoneyB)) ;//
						  //obligationsmoney 待付金额  减少
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().subtract(picMoneyB));//
					}
					systemService.saveOrUpdate(billsorderid);
				}
			}
			
			/**
			 * 检查物流单号和物流公司两边信息是否完整
			 */
			String logisticsnumber = order.getLogisticsnumber();
			String logisticscompany = order.getLogisticscompany();
			
			Integer numberSize = 0;
			Integer companySize = 0;
			if(StringUtil.isNotEmpty(logisticsnumber)){
				List<String> numberList = ConvertTool.toList(logisticsnumber);
				numberSize = numberList.size();
				
				if(!OrderServiceI.State.WAIT2RECEIPT.getValue().equals(t.getState())){
					j.setMsg("发货失败！您的订单还未支付或者先货后款还未审核");
					j.setSuccess(false);
					return j;
				}
			}
			if(StringUtil.isNotEmpty(logisticscompany)){
				List<String> companyList = ConvertTool.toList(logisticscompany);
				companySize = companyList.size();
			}
			
			if(numberSize !=companySize){
				j.setMsg("请检查物流单号和物流公司两边信息是否匹配");
				j.setSuccess(false);
				return j;
			}
			
			
			try {
				 
				boolean messageSend = false ;
				//发货信息，状态为待发货   同时填写物流公司  物流单号   自动修改为  发货状态
				if(!OrderServiceI.FreightState.ALREADY_SIGN.getValue().equals(t.getFreightState().getDictionaryValue()) &&  
						StringUtil.isNotEmpty(order.getLogisticscompany()) && 
						StringUtil.isNotEmpty(order.getLogisticsnumber() )  ){
					int ordernum = order.getNumber();
					//更新待打包物流状态
					if(ordernum == numberSize){
						order.setFreightState(dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.ALREADY_DELIVER.getValue()));
					}else{
						order.setFreightState(dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.PART_DELIVER.getValue()));
					}
					
					order.setDeparturedate(DateUtils.getDate());
					
					messageSend = true;
				}
				else{
					
					if(StringUtil.isNotEmpty(t.getLogisticsnumber()) 
							&& ( (null != order.getLogisticsnumber() && StringUtil.isEmpty(order.getLogisticsnumber().trim())) || StringUtil.isEmpty(order.getLogisticsnumber()) ) ){
						throw new Exception("更新失败:不能全部清空物流单号，请修改或者部分删除！");
					}
				}
				
				//更新小单
				MyBeanUtils.copyBeanNotNull2Bean(order, t);
				systemService.updateEntitie(t);
				
				//order 是页面传入对象
				expressService.updateManualExpress(order);
				
				orderLogisticsService.saveOrderLogistics(t);
				
				//更新大订单物流状态
				TSDictionary payBillsFreightState = payBillsOrderService.getPayBillsFreightStatus(billsorderid);
				billsorderid.setFreightState(payBillsFreightState);
				
				systemService.saveOrUpdate(billsorderid);
				//发货通知下单客户
				messageTemplateService.mosSendSmsDelivery_6(t);
				//发货通知收货人
				messageTemplateService.mosSendSmsDelivery_5(t);
				
				salesmanCommissionService.SalesmanCommissionSave(t);
				
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
				
			} catch (Exception e) {
				j.setSuccess(false);
				message = "订单明细更新失败:" + e.getMessage();
			}
		} 
		j.setMsg(message);
		systemService.addLog(message, Globals.Log_Leavel_INFO, Globals.Log_Type_UPDATE);
		return j;
	}

	/**
	 * 订单明细列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderEntity order, HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		req.setAttribute("user", user);
		if (StringUtil.isNotEmpty(order.getId())) {
			order = systemService.getEntity(OrderEntity.class, order.getId());

			//存在地址信息的，状态为1的情况  对地址进行拆分
			String useraddress ="";
			if( "1".equals(order.getState()) ){//未付款订单允许修改地址
				String address =  order.getAddress();
				
				if(  StringUtil.isNotEmpty(address)){
					if( address.indexOf("_") > 0 ){
						useraddress = address.substring(0,address.lastIndexOf("_"));
					}
					order.setAddress( order.getAddress().replaceAll(useraddress, "").replace("_", "") );
				}
			}
			 TSTerritory territory = order.getTSTerritory();
			 if(null == territory){
				 territory = new TSTerritory();
			 }
			 //查询物流状态
			List<TSDictionary> freightsStates = orderService.checkFreightState(null);
			req.setAttribute("freightsStates", freightsStates);
			req.setAttribute("useraddress", useraddress);
			req.setAttribute("orderPage", order);
		}
		return new ModelAndView("admin/order/order");
	}
	
	
	/**
	 * 财务订单审核列表跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderAuditList")
	public ModelAndView orderAuditList(OrderEntity order, HttpServletRequest req) {
		return new ModelAndView("admin/order/orderAuditList");
	}
	
	/**
	 * 财务订单审核跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderAuditdDatagrid")
	public void orderAuditdDatagrid(OrderEntity order,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class, dataGrid);
		
		String []states = "5,6,7".split(",");
		if( !"5".equals(order.getState()) && !"6".equals(order.getState()) && !"7".equals(order.getState()) ){
			cq.in("state", states);
			order.setState(null);
		}
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.like("identifieror", "%"+order.getIdentifieror()+"%");
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.like("productname", "%"+order.getProductname()+"%");
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.like("customername", "%"+order.getCustomername()+"%");
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.like("telephone", "%"+order.getTelephone()+"%");
			order.setTelephone(null);
		}
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, order, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);

		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 财务订单审核跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderAudit")
	public ModelAndView orderAudit(OrderEntity order, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(order.getId())) {
			order = systemService.getEntity(OrderEntity.class, order.getId());
			req.setAttribute("orderPage", order);
		}
		return new ModelAndView("admin/order/orderAudit");
	}
	

	/**
	 * 财务订单审核处理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "orderAuditSave")
	@ResponseBody
	public AjaxJson orderAuditSave(OrderEntity order, HttpServletRequest req) {
		
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		if (StringUtil.isNotEmpty(order.getId())) {
			message = "退单审核修改更新成功";
			OrderEntity t = systemService.get(OrderEntity.class, order.getId());
			if( !"6".equals(t.getState()) &&  !"7".equals(t.getState())  ){
				message = "不符合审核状态订单,修改失败！";
				j.setMsg(message);
				return j;
			}
			if( "5".equals(order.getState()) && ( "6".equals(t.getState())  || "7".equals(t.getState()) )){
				PayBillsOrderEntity billsorderid = t.getBillsorderid();
				BigDecimal cancelMoney = new BigDecimal("0.00");
				if(billsorderid.getCancelMoney() != null ){
					cancelMoney = billsorderid.getCancelMoney();
				}
				billsorderid.setCancelMoney( t.getPrice().add(t.getFreightpic()).add( cancelMoney)  );
				systemService.saveOrUpdate(billsorderid);
				//通过状态才发送审核通知
				messageTemplateService.mosSendSmsDelivery_8(t);
			}
			try {
				
				if( "7".equals(order.getState())){
					order.setState("2");
				}
				t.setCanceldate(DateUtils.getDate());
				t.setState(order.getState());
				t.setRemarks(order.getRemarks());
				//MyBeanUtils.copyBeanNotNull2Bean(order, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "退单审核修改失败！";
			}
		} 
		j.setMsg(message);

		return j;
	}
	

	
	
	
//	/**
//	 * 快递物流查询
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "kuaidi")
//	public String kuaidi(HttpServletRequest req) {
//		req.setAttribute("orderid", req.getParameter("id"));
//		return "express/expresslist";
//	}
	
	/**
	 * 快递物流查询
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "expressinfo")
	@ResponseBody
	public AjaxJson kuaidi(OrderEntity order, HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(order.getId())) {
			try {
				Map<String, Object> attributes = new HashMap<String, Object>();
				order = systemService.getEntity(OrderEntity.class, order.getId());
				String logisticscompany = order.getLogisticscompany();
				String logisticsresp = null;
				String state = null;
				String date = null;
				List<OrderLogisticsEntity> orderlogisticslist = order.getOrderLogisticsList();
				logger.error("######kuaidi##########orderlogisticslist##########"+orderlogisticslist);
				if(null != orderlogisticslist && orderlogisticslist.size() > 0){
					for (OrderLogisticsEntity orderLogisticsEntity : orderlogisticslist) {
						String logisticsnumber = orderLogisticsEntity.getLogisticsnumber();
						String company = orderLogisticsEntity.getLogisticscompany();
						String type = ExpressCompany.getCompanyCode(company);
						
						//订阅物流信息
						orderLogisticSubscribeService.orderLogisticSubscribe(order, logisticsnumber, type);
						//快递即时查询
						AjaxJson singlejson = orderLogisticsService.getExpressInfo(order.getId(), orderLogisticsEntity.getLogisticsnumber(), logisticscompany);
						attributes.put(logisticscompany + logisticsnumber, singlejson.getObj());
					}
					
					systemService.batchUpdate(orderlogisticslist);
				}else{
					String logisticsnum = order.getLogisticsnumber();
					String logisticscom = order.getLogisticscompany();
					
					if(null != logisticsnum && StringUtil.isNotEmpty(order.getLogisticsnumber())){
						
						String[] arrayLogisticsCode = logisticsnum.split(",");
						String[] arrayLogisticsCom = logisticscom.split(",");
						
						if(arrayLogisticsCode.length <= 0){
							j.setSuccess(false);
							j.setMsg("没有物流单号");
							return j;
						}else if(arrayLogisticsCom.length <= 0){
							j.setSuccess(false);
							j.setMsg("没有物流公司");
							return j;
						}
						
						for (int i = 0; i < arrayLogisticsCode.length; i++) {
							
							//物流单号是否存在
							if(arrayLogisticsCode[i] == null && !StringUtil.isNotEmpty(arrayLogisticsCode[i])){
								continue;
							}
							
							//物流公司是否存在
							if(arrayLogisticsCom[i] == null && !StringUtil.isNotEmpty(arrayLogisticsCom[i])){
								continue;
							}
							
							String logisticsnumber = arrayLogisticsCode[i];
							String type = ExpressCompany.getCompanyCode(arrayLogisticsCom[i]);
							
							//订阅物流信息
							orderLogisticSubscribeService.orderLogisticSubscribe(order, logisticsnumber, type);
							
							AjaxJson singlejson = orderLogisticsService.getExpressInfo(order.getId(), arrayLogisticsCode[i], logisticscompany);
							attributes.put(arrayLogisticsCom[i] + arrayLogisticsCode[i], singlejson.getObj());
						}
					}
				}
				
				req.setAttribute("expressinfo", attributes);
				
				j.setAttributes(attributes);
				
			} catch (Exception e) {
				systemService.addLog("快递查询出错", Globals.Log_Leavel_ERROR, Globals.Log_Type_OTHER);
			}
			
		}
		return j;
	}

	/**
	 * 查看订单详情列表页
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderListSet")
	public ModelAndView orderListSet(HttpServletRequest request) {
		//增加产品列表查询
		String payBills_id = request.getParameter("payBillsId");
		if(StringUtil.isNotEmpty(payBills_id)){
			request.setAttribute("payBills_id", payBills_id);
		}
		return new ModelAndView("admin/order/orderListSet");
	}
	
	
	
	/**
	 * 查看订单详情列表页
	 * 
	 * @return
	 */
	@RequestMapping(value = "details")
	public ModelAndView details(OrderEntity order,HttpServletRequest request) {
		if (StringUtil.isNotEmpty(order.getId())) {
			order = systemService.getEntity(OrderEntity.class, order.getId());

			//存在地址信息的，状态为1的情况  对地址进行拆分
			String useraddress ="";
			if( "1".equals(order.getState()) ){//未付款订单允许修改地址
				String address =  order.getAddress();
				
				if(  StringUtil.isNotEmpty(address)){
					if( address.indexOf("_") > 0 ){
						useraddress = address.substring(0,address.lastIndexOf("_"));
					}
					order.setAddress( order.getAddress().replaceAll(useraddress, "").replace("_", "") );
				}
			}
			 TSTerritory territory = order.getTSTerritory();
			 if(null == territory){
				 territory = new TSTerritory();
			 }
			
			 request.setAttribute("useraddress", useraddress);
			 request.setAttribute("orderPage", order);
		}else{
			request.setAttribute("orderPage", order);
		}
		return new ModelAndView("admin/order/orderDetails");
	}
	
	
	@RequestMapping(value= "statisCancelOrders")
	@ResponseBody
	public AjaxJson statisCancelOrders(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计退单失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = orderService.statisCancelOrder(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计退单成功！");
			
		}
		
		return json;
	}
	
	@RequestMapping(value= "statisOrderDetails")
	@ResponseBody
	public AjaxJson statisOrderDetails(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计订单详情失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = orderService.statisOrderByRequest(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计订单详情成功！");
			
		}
		
		return json;
	}
	
	
	@RequestMapping(value= "statisCopartnerOrders")
	@ResponseBody
	public AjaxJson statisCopartnerOrdersByReq(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计订单详情失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = orderService.statisCopartnerOrders(company, req, user);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计订单详情成功！");
			
		}
		
		return json;
	}
	
	
	/**
	 * 特殊状态订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "copartnerOrdersState")
	public ModelAndView copartnerOrdersState(HttpServletRequest request) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		String state = request.getParameter("state");
		if(StringUtil.isNotEmpty(state)){
			request.setAttribute("state", state);
		}
		String orderDate = request.getParameter("orderDate"); 
		if(StringUtil.isNotEmpty(orderDate)){
			request.setAttribute("orderDate", orderDate);
		}
		
		CriteriaQuery cq = new CriteriaQuery(TSUser.class);
		cq.eq("company.id", user.getCompany().getId());
		cq.eq("type", "6");
		cq.add();
		
		List<TSUser> userCopartnerList = systemService.getListByCriteriaQuery(cq, false);
		
		request.setAttribute("userCopartnerList", userCopartnerList);
		
		if( "2".equals(state) ){//在派单中查找需要发货的订单
			return new ModelAndView("admin/order/copartnerOrdersStateSend");
		}
		if(!"6".equals(user.getType())){
			return new ModelAndView("admin/order/adminOrderSendList");
			
		}else{
			return new ModelAndView("admin/order/copartnerReceiveOrders");
		}
		
	}
	
	@RequestMapping(value = "copartnerDatagrid")
	public void copartnerDatagrid(OrderSendEntity orderSend, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		String freightStateValue = ResourceUtil.getParameter("freightStateValue");
		
		OrderEntity order = orderSend.getOrderid();
		if(null == order){
			order = new OrderEntity();
			orderSend.setOrderid(order);
		}
/*		String ordertype = ResourceUtil.getParameter("ordertype");
		if(StringUtil.isNotEmpty(ordertype)){
			order.setState(ordertype);
		}*/
		
		
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class, dataGrid);
		cq.createAlias("orderid", "order");
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.add(Restrictions.like("order.identifieror", "%"+order.getIdentifieror()+"%"));
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.add(Restrictions.like("order.productname", "%"+order.getProductname()+"%"));
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.add(Restrictions.like("order.customername", "%"+order.getCustomername()+"%"));
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.add(Restrictions.like("order.telephone", "%"+order.getTelephone()+"%"));
			order.setTelephone(null);
		}
		if(StringUtil.isNotEmpty(order.getAddress()) ){
			cq.add(Restrictions.like("order.address", "%"+order.getAddress()+"%"));
			order.setAddress(null);
		}
		if( StringUtil.isNotEmpty(order.getSendtype())){
			cq.add(Restrictions.eq("order.sendtype", order.getSendtype()));
			order.setSendtype(null);
		}
		if(StringUtil.isNotEmpty(order.getState()) ){
			String[] orders= order.getState().split(",");
			if( orders.length > 0 ){
				cq.add(Restrictions.in("order.state", orders));
				order.setState(null);
			}
		}else{
			String[] statelist = new String[]{OrderServiceI.State.UNPAID.getValue()
					,OrderServiceI.State.WAIT2RECEIPT.getValue()
					,OrderServiceI.State.REBACK.getValue()};
			cq.add(Restrictions.in("order.state", statelist));
		}
		cq.createAlias("order.freightState", "dictionary");
		cq.eq("dictionary.dictionaryType", "freightStates");
		if(StringUtil.isNotEmpty(freightStateValue)){
			cq.eq("dictionary.dictionaryValue", freightStateValue);
		}
//		else{
//			cq.eq("dictionary.dictionaryValue", OrderServiceI.FreightState.WAIT_DELIVER.getValue());
//		}
		
		String status = orderSend.getStatus();
		if(StringUtil.isEmpty(status)){
			cq.in("status", "1,2".split(","));
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		String createdate3 = request.getParameter("createdate3");
		String createdate4 = request.getParameter("createdate4");
		//派单时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.add(Restrictions.between("createtime", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat), DateUtils.str2Date(createdate2,DateUtils.datetimeFormat)));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.add(Restrictions.ge("createtime", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat)));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.add(Restrictions.le("createtime", DateUtils.str2Date(createdate2,DateUtils.datetimeFormat)));
		}
		//下单时间
		if(StringUtil.isNotEmpty(createdate3)&&StringUtil.isNotEmpty(createdate4)){
			cq.add(Restrictions.between("order.createdate", DateUtils.str2Date(createdate3,DateUtils.datetimeFormat), DateUtils.str2Date(createdate4,DateUtils.datetimeFormat)));
		}else if( StringUtil.isNotEmpty(createdate3) ){
			cq.add(Restrictions.ge("order.createdate", DateUtils.str2Date(createdate3,DateUtils.datetimeFormat)));
		}else if( StringUtil.isNotEmpty(createdate4) ){
			cq.add(Restrictions.le("order.createdate", DateUtils.str2Date(createdate4,DateUtils.datetimeFormat)));
		}
		
		cq.addOrder("createtime", SortDirection.desc);
		if("6".equals(user.getType())){
			cq.eq("receiver", user);
		}
		cq.eq("distributecompanyid", user.getCompany());
		cq.add();
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderSend, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	
	/**
	 * 订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "copartnerOrderList")
	public ModelAndView copartnerOrderList(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		
		//字典 - 物流状态
		List<TSDictionary> dictionarys = orderService.checkFreightState(null);
		request.setAttribute("dictionarys", dictionarys);
		
		//待打包
		request.setAttribute("orderWaitPack", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.WAIT_PACK.getValue(), user));
		//待发货
		request.setAttribute("orderWaitDeliver", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.WAIT_DELIVER.getValue(), user));
		//已发货
		request.setAttribute("orderAlreadyDeliver", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.ALREADY_DELIVER.getValue(), user));
		//已签收
		request.setAttribute("orderAlreadySingn", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.ALREADY_SIGN.getValue(), user));
		//部分发货
		request.setAttribute("orderParitDeliver", orderService.statisCopartnerOrderByFreightState(company, OrderServiceI.FreightState.PART_DELIVER.getValue(), user));
		
		/*request.setAttribute("statisOrder2Send", orderService.statisCopartnerOrderByFreightState(company, "2", user));
		request.setAttribute("statisOrderSendOK", orderService.statisCopartnerOrderByFreightState(company, "3", user));
		request.setAttribute("statisOrderSigned", orderService.statisCopartnerOrderByFreightState(company, "4", user));
		request.setAttribute("statisOrderCancel", orderService.statisCopartnerOrderByFreightState(company, "5", user));*/
		
		
		return new ModelAndView("admin/order/copartnerOrderList");
	}
	
	
	/**
	 * 需要传入一组订单号ids
	 * @param req
	 * @return
	 */
	@RequestMapping(value= "batchOrder2Received")
	@ResponseBody
	public AjaxJson batchUpdateOrder2Received(HttpServletRequest req){
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		
		String orderids = req.getParameter("ids");
		if(StringUtil.isNotEmpty(orderids)){
			try {
				json.setMsg(orderService.batchUpdateOrder2Received(orderids));
				json.setSuccess(true);
			} catch (Exception e) {
				json.setSuccess(false);
				json.setMsg("订单批量签收失败！" + e);
			}
			
		}
		
		return json;
	}
	
	@RequestMapping(value = "datagridStatis")
	@ResponseBody
	public AjaxJson datagridStatis(OrderEntity order,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		AjaxJson j = new AjaxJson();
		j.setSuccess(false);
		j.setMsg("查询失败");
		
		CriteriaQuery cq = orderService.getOrderListCriteriaQry(order, request, dataGrid);
		
		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.sum("totalPrice"));
		projectionList.add(Projections.sum("pricec"));
		projectionList.add(Projections.sum("unitCost"));
		projectionList.add(Projections.sum("number"));

		
		cq.setProjectionList(projectionList);
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, order, request.getParameterMap());
		orderService.getDataGridReturn(cq, true);
		
		dataGrid = cq.getDataGrid();
		
		List<Object[]> list = dataGrid.getResults();
		
		if( null!= list && list.size() > 0 ){
			Object[] objlist = list.get(0);
			Map<String,Object> map = new HashMap<>();
			map.put("totalPrice", (BigDecimal)objlist[0]);
			map.put("totalBaseCost",(BigDecimal) (null == objlist[2] ? new BigDecimal(0.00) : objlist[2]));
			map.put("totalUnitCost",(BigDecimal) (null == objlist[2] ? new BigDecimal(0.00) : objlist[2]));
			map.put("totalNumber",(long) objlist[3]);
			
			j.setObj(map);
			j.setSuccess(true);
			j.setMsg("查询成功");
		}
		
		
		return j;
	}
}
