package cn.gov.xnc.admin.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 订单物流订阅信息
 * @author zero
 * @date 2018-03-12 16:46:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_order_logistics_subscribe", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class OrderLogisticSubscribeEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**商品订单号*/
	private OrderEntity orderid;
	/**物流单号*/
	private java.lang.String logisticscode;
	/**订阅平台*/
	private java.lang.String platformcode;
	/**订阅成功与否 1 成功 0 失败*/
	private java.lang.String status;
	/**创建时间*/
	private java.util.Date createtime;
	/**更新时间*/
	private java.util.Date updatetime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  orderid
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDERID")
	public OrderEntity getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  orderid
	 */
	public void setOrderid(OrderEntity orderid){
		this.orderid = orderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  logisticcode
	 */
	@Column(name ="LOGISTICSCODE",nullable=false,length=32)
	public java.lang.String getLogisticcode(){
		return this.logisticscode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  logisticcode
	 */
	public void setLogisticcode(java.lang.String logisticcode){
		this.logisticscode = logisticcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  platformcode
	 */
	@Column(name ="PLATFORMCODE",nullable=true,length=32)
	public java.lang.String getPlatformcode(){
		return this.platformcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  platformcode
	 */
	public void setPlatformcode(java.lang.String platformcode){
		this.platformcode = platformcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订阅成功与否
	 */
	@Column(name ="STATUS",nullable=false,length=2)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订阅成功与否
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  createtime
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  createtime
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATETIME",nullable=false)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
}
