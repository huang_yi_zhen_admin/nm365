package cn.gov.xnc.admin.order.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.admin.order.entity.OrderRebackDetailEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackDetailEntity;
import cn.gov.xnc.admin.order.service.OrderRebackDetailServiceI;

/**   
 * @Title: Controller
 * @Description: 退单信息
 * @author zero
 * @date 2017-12-27 11:54:27
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderRebackDetailController")
public class OrderRebackDetailController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderRebackDetailController.class);

	@Autowired
	private OrderRebackDetailServiceI orderRebackDetailService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 申请退单详细，记录定关联信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView orderCancelDetail(HttpServletRequest request) {
		return new ModelAndView("admin/order/orderRebackDetailList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderRebackDetailEntity orderCancelDetail,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(OrderRebackDetailEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderCancelDetail, request.getParameterMap());
		this.orderRebackDetailService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除申请退单详细，记录定关联信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(OrderRebackDetailEntity orderCancelDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		orderCancelDetail = systemService.getEntity(OrderRebackDetailEntity.class, orderCancelDetail.getId());
		message = "申请退单详细，记录定关联信息删除成功";
		orderRebackDetailService.delete(orderCancelDetail);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加申请退单详细，记录定关联信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderRebackDetailEntity orderCancelDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(orderCancelDetail.getId())) {
			message = "申请退单详细，记录定关联信息更新成功";
			OrderRebackDetailEntity t = orderRebackDetailService.get(OrderRebackDetailEntity.class, orderCancelDetail.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(orderCancelDetail, t);
				orderRebackDetailService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "申请退单详细，记录定关联信息更新失败";
			}
		} else {
			message = "申请退单详细，记录定关联信息添加成功";
			orderRebackDetailService.save(orderCancelDetail);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 申请退单详细，记录定关联信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderRebackDetailEntity orderCancelDetail, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(orderCancelDetail.getId())) {
			
			CriteriaQuery cq = new CriteriaQuery(OrderRebackDetailEntity.class);
			cq.eq("cancelid.id", orderCancelDetail.getCancelid());
			cq.add();
			
			List<OrderRebackDetailEntity> orderCancelDetailList = orderRebackDetailService.getListByCriteriaQuery(cq, false);
			
			req.setAttribute("orderCancelDetailList", orderCancelDetailList);
			
			orderCancelDetail = orderRebackDetailService.getEntity(OrderRebackDetailEntity.class, orderCancelDetail.getId());
			req.setAttribute("orderCancelDetailPage", orderCancelDetail);
		}
		return new ModelAndView("admin/order/orderRebackDetail");
	}
	
	/**
	 * 申请退单详细，记录定关联信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderCancelDetailList")
	public ModelAndView orderCancelDetailList(OrderRebackEntity orderCancel, HttpServletRequest req) {
			
		OrderRebackEntity OrderRebackEntity = orderRebackDetailService.findUniqueByProperty(OrderRebackEntity.class, "id", orderCancel.getId());
		
		CriteriaQuery cq = new CriteriaQuery(OrderRebackDetailEntity.class);
		cq.eq("cancelid.id", orderCancel.getId());
		cq.add();
		
		List<OrderRebackDetailEntity> orderCancelDetailList = orderRebackDetailService.getListByCriteriaQuery(cq, false);
		
		req.setAttribute("orderCancelDetailList", orderCancelDetailList);
		req.setAttribute("orderCancelPage", OrderRebackEntity);
		req.setAttribute("orderReceiver", orderCancelDetailList.get(0).getOrderid().getCustomername());
		req.setAttribute("orderTelphone", orderCancelDetailList.get(0).getOrderid().getTelephone());
		req.setAttribute("orderAddress", orderCancelDetailList.get(0).getOrderid().getAddress());
		req.setAttribute("logisticsnumber", orderCancelDetailList.get(0).getOrderid().getLogisticsnumber());
		req.setAttribute("logisticsphone", orderCancelDetailList.get(0).getOrderid().getLogisticscompany());
		
		return new ModelAndView("admin/order/orderRebackDetail");
	}
}
