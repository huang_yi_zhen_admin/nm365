package cn.gov.xnc.admin.order.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.admin.express.entity.ExpressEntity;
import cn.gov.xnc.admin.kuaidi.service.Kuaid100ServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsDetailEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticSubscribeServiceI;
import cn.gov.xnc.admin.order.service.OrderLogisticsServiceI;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ExpressSearchUtil;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.JsonUtil;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;

@Service("orderLogisticsService")

public class OrderLogisticsServiceImpl extends CommonServiceImpl implements OrderLogisticsServiceI {
	
	@Autowired
	private Kuaid100ServiceI kuaid100ServiceI;
	@Autowired
	private OrderLogisticSubscribeServiceI orderLogisticSubscribeService;
	@Autowired
	private DictionaryService dictionaryService;
	
	
	
	public AjaxJson getExpressInfo(String orderid, String logisticsCode, String logisticsCompany) throws Exception{
		
		OrderEntity order = getEntity(OrderEntity.class, orderid);
		
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		
		//1、检查是否有记录
		OrderLogisticsEntity orderLogistics = null;
		CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
		cq.eq("orderid", order);
		cq.eq("logisticsnumber", logisticsCode);
		String company = logisticsCompany;
		if( logisticsCompany.length() > 2 ){
			company = logisticsCompany.substring(0,2);
		}
		cq.like("logisticscompany", "%"+company+"%");
		cq.addOrder("updatetime", SortDirection.desc);
		List<OrderLogisticsEntity> list = getListByCriteriaQuery(cq, false);
		if( null != list && 0 != list.size() ){
			//如果在有效期内
			orderLogistics = list.get(0);
			int interval = 1000*60*60*2;//缓存时间为两个小时
			Date updatetime = list.get(0).getUpdatetime();
			Date datenow = DateUtils.getDate();
			if( datenow.getTime() - updatetime.getTime() <= interval && "200".equals(orderLogistics.getStatus())){
				List<OrderLogisticsDetailEntity> detaillist = decodeJson(orderLogistics);
				j.setObj(detaillist);
				j.setMsg("查询成功");
				j.setSuccess(true);
				return j;
			}
		}else{
			j.setMsg("查询失败，请检查物流单号是否正确");
			j.setSuccess(false);
			return j;
		}
		
		//判断是否为本系统电子面单
		CriteriaQuery ce = new CriteriaQuery(ExpressEntity.class);
		ce.eq("orderid", order);
		ce.eq("logisticcode", logisticsCode);
		ce.eq("setorderlogistic", 1);
		ce.eq("success", 1);
		ce.addOrder("updateTime", SortDirection.desc);
		List<ExpressEntity> expresslist = getListByCriteriaQuery(ce, false);
		if( null == expresslist || expresslist.size() == 0 ){
			j.setMsg("查询失败，请检查物流单号是否正确");
			j.setSuccess(false);
			return j;
		}
		
		ExpressEntity express = expresslist.get(0);
		if( ExpressCompany.HHTT.equals(express.getShippercode()) || ExpressCompany.STO.equals(express.getShippercode()) ){//属于天天和申通
			List<OrderLogisticsDetailEntity> detaillist = getLogisticsByKD100(orderLogistics, logisticsCode, logisticsCompany);
			if( "200".equals(orderLogistics.getStatus())){
				updateEntitie(orderLogistics);
				j.setObj(detaillist);
				j.setMsg("查询成功");
				j.setSuccess(true);
				return j;
			}else{
				j.setMsg("暂无物流信息");
				j.setSuccess(false);
				return j;
			}
		}else {
			List<OrderLogisticsDetailEntity> detaillist  = getLogisticsByKND(orderLogistics, logisticsCode, logisticsCompany);
			if( "200".equals(orderLogistics.getStatus())){
				updateEntitie(orderLogistics);
				j.setObj(detaillist);
				j.setMsg("查询成功");
				j.setSuccess(true);
				return j;
			}else{
				detaillist  = getLogisticsByKD100(orderLogistics, logisticsCode, logisticsCompany);
				if( "200".equals(orderLogistics.getStatus())){
					updateEntitie(orderLogistics);
					j.setObj(detaillist);
					j.setMsg("查询成功");
					j.setSuccess(true);
					return j;
				}else{
					j.setMsg("暂无物流信息");
					j.setSuccess(false);
					return j;
				}
			}
		}
		
	}
	

	/**
	 * 从快递100获取物流数据
	 * */
	public List<OrderLogisticsDetailEntity> getLogisticsByKD100(OrderLogisticsEntity entity, String logisticsCode, String logisticsCompany) throws Exception{
		
		AjaxJson j = kuaid100ServiceI.getkuaidi100(logisticsCompany, logisticsCode);
		
		Map<String,Object> map = j.getAttributes();//logisticscompany+postid
		String message = (String) map.get(logisticsCompany+logisticsCode);
		
		JSONObject obj = new JSONObject();
		entity.setMessage(message);
		entity.setChannel(OrderLogisticsEntity.CHANNEL_KD100);
		
		List<OrderLogisticsDetailEntity> list = decodeJson(entity);

		return list;
	}
	
	
	/**
	 * 从快递鸟接口获取回来数据
	 * @throws Exception 
	 * 
	 * */
	public List<OrderLogisticsDetailEntity> getLogisticsByKND(OrderLogisticsEntity entity, String logisticsCode, String logisticsCompany) throws Exception{
		
		String logisticsresp = ExpressSearchUtil.getOrderTracesByCompany(logisticsCompany, logisticsCode);
		
		JSONObject obj = new JSONObject();
		
		entity.setMessage( logisticsresp );
		entity.setChannel(OrderLogisticsEntity.CHANNEL_KDN);
		
		List<OrderLogisticsDetailEntity> list = decodeJson(entity);

		return list;
	}
	
	
	public List<OrderLogisticsDetailEntity> decodeJson(OrderLogisticsEntity orderLogistics) throws ParseException{
				
		JSONObject obj = new JSONObject();
		obj = obj.parseObject(orderLogistics.getMessage());
		orderLogistics.setState( obj.getString("State"));
		List<OrderLogisticsDetailEntity> detaillist = new ArrayList<OrderLogisticsDetailEntity>();
		
		if( OrderLogisticsEntity.CHANNEL_KDN.equals(orderLogistics.getChannel()) ){
			if(obj.getJSONArray("Traces") != null && obj.getJSONArray("Traces").size()>0){
				
				List<Map<String, String>> list = JsonUtil.toBean(obj.getJSONArray("Traces")+"", ArrayList.class);
				for(Map<String, String> m : list){
					OrderLogisticsDetailEntity detail = new OrderLogisticsDetailEntity();
					if(StringUtil.isNotEmpty(m.get("AcceptTime") )){
						detail.setTraceDetailTime(m.get("AcceptTime"));
					}
					if(StringUtil.isNotEmpty(m.get("AcceptStation"))){
						detail.setTraceDetail(m.get("AcceptStation"));
					}
					detaillist.add(detail);
				}
				
				if( detaillist.size() > 0 ){
					orderLogistics.setLogisticsdate(DateUtils.parseDate(detaillist.get(detaillist.size()-1).getTraceDetailTime(), "yyyy-MM-dd HH:mm:ss"));
					orderLogistics.setStatus("200");//200表示获取数据成功
					return detaillist;
				}else{
					orderLogistics.setStatus("404");//404表示没有数据
				}
			}else{
				orderLogistics.setStatus("500");//500表示没有获取到数据
			}
		}else if( OrderLogisticsEntity.CHANNEL_KD100.equals(orderLogistics.getChannel()) ){
			
			if(obj.getJSONArray("data") != null && obj.getJSONArray("data").size()>0){
				List<Map<String, String>> list = JsonUtil.toBean(obj.getJSONArray("data")+"", ArrayList.class);
				for(Map<String, String> m : list){
					OrderLogisticsDetailEntity detail = new OrderLogisticsDetailEntity();
					
					if(StringUtil.isNotEmpty(m.get("time") )){
						detail.setTraceDetailTime(m.get("time"));
					}
					if(StringUtil.isNotEmpty(m.get("context"))){
						detail.setTraceDetail(m.get("context"));
					}
					detaillist.add(detail);
				}
				
				if( detaillist.size() > 0 ){
					orderLogistics.setLogisticsdate(DateUtils.parseDate(detaillist.get(detaillist.size()-1).getTraceDetailTime(), "yyyy-MM-dd HH:mm:ss"));
					orderLogistics.setStatus("200");//200表示获取数据成功
					return detaillist;
				}else{
					orderLogistics.setStatus("404");//404表示没有数据
				}
				
			}else{
				orderLogistics.setStatus("500");//500表示没有获取到数据
			}
			
		}
		
		return null;
	}
	
	

	@Override
	public void saveOrderLogistics(OrderEntity order) throws Exception{
		//订单物流号
		String logisticCode = order.getLogisticsnumber();
		String orderid = order.getId();
		
		if(StringUtil.isNotEmpty(logisticCode)){
			
			//订单关联物流记录
			List<String> oldArrayLogistisCode = getArrayOrderLogistcs(order.getId());
			
			//来自页面的新物流单号（手动添加）
			List<String> newLogisticCodes = StringUtil.splitToList(",", order.getLogisticsnumber());
			
			List<String> nArrayLogisticCompanys = StringUtil.splitToList(",",  order.getLogisticscompany());
			
			//新物流单号
//			List<String> newArrayLogisticsCode = Arrays.asList(newLogisticCodes);
			
			//保存新物流单号
			for (int i = 0; i < newLogisticCodes.size(); i++) {
				String logisticsCode = newLogisticCodes.get(i);
				if(!StringUtil.isNotEmpty(logisticsCode)){
					continue;
				}
				
				if(!hasDuplicateLogistcsCode(orderid, logisticsCode)){
					if(!ListUtil.isNotEmpty(nArrayLogisticCompanys)){
						continue;
					}
					String logisticCompany = StringUtil.formatNull(nArrayLogisticCompanys.get(i));
					if(!StringUtil.isNotEmpty(logisticCompany)){
						continue;
					}
					
					if(!oldArrayLogistisCode.contains(logisticsCode) ){
						OrderLogisticsEntity ol = new OrderLogisticsEntity();
						ol.setOrderid(order);
						ol.setLogisticscompany(logisticCompany);
						ol.setLogisticsnumber(logisticsCode);
						ol.setLogisticsdate(DateUtils.getDate());
						
						save(ol);
					}else{
						updateOrderLogistics(orderid, logisticsCode, nArrayLogisticCompanys.get(i));
					}
				}else{
					throw new Exception("物理单号：" + logisticsCode + "更新失败，物流单号不能在其他订单中同时出现！");
				}
				//物流订阅
				orderLogisticSubscribeService.orderLogisticSubscribe(order, logisticCode, ExpressCompany.getCompanyCode((nArrayLogisticCompanys.get(i))));
			}
			
			//删除废弃的物流单号
			for (int k = 0; k < oldArrayLogistisCode.size(); k++) {
				String oldLogictisCode = oldArrayLogistisCode.get(k);
				if(!newLogisticCodes.contains(oldLogictisCode) ){
					deleteOrderLogistics(orderid, oldLogictisCode);
				}
			}
		}else{
			deleteAllOrderLogistics(order);
		}
	}
	
	private boolean hasDuplicateLogistcsCode(String orderid, String logisticsCode) throws Exception {
		if(StringUtil.isNotEmpty(orderid) && StringUtil.isNotEmpty(logisticsCode)){
			CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
			cq.notEq("orderid.id", orderid);
			cq.eq("logisticsnumber", logisticsCode);
			cq.add();
			
			List<OrderLogisticsEntity> orderLogistics = getListByCriteriaQuery(cq, false);
			
			if(null != orderLogistics && orderLogistics.size() > 0){
				return true;
			}
		}
		return false;
	}
	
	
	public List<String> getArrayOrderLogistcs(String orderid) throws Exception {
		List<String> oLogistics = new ArrayList<String>();
		CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
		cq.eq("orderid.id", orderid);
		cq.add();
		
		List<OrderLogisticsEntity> orderLogisticsList = getListByCriteriaQuery(cq, false);
		
		if(null != orderLogisticsList && orderLogisticsList.size() > 0){
			for (OrderLogisticsEntity orderLogistics : orderLogisticsList) {
				oLogistics.add(orderLogistics.getLogisticsnumber());
			}
		}
		return oLogistics;
	}


	@Override
	public void deleteAllOrderLogistics(OrderEntity order) {
		if(null != order){
			CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
			cq.eq("orderid.id", order.getId());
			cq.add();
			
			List<OrderLogisticsEntity> orderLogistics = getListByCriteriaQuery(cq, false);
			
			if(null != orderLogistics && orderLogistics.size() > 0){
				deleteAllEntitie(orderLogistics);
			}
		}
	}
	
	@Override
	public void deleteOrderLogistics(String orderid, String logisticsCode){
		if(StringUtil.isNotEmpty(orderid) && StringUtil.isNotEmpty(logisticsCode)){
			CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
			cq.eq("orderid.id", orderid);
			cq.eq("logisticsnumber", logisticsCode);
			cq.add();
			
			List<OrderLogisticsEntity> orderLogistics = getListByCriteriaQuery(cq, false);
			
			if(null != orderLogistics && orderLogistics.size() > 0){
				deleteAllEntitie(orderLogistics);
			}
		}
	}


	@Override
	public void updateOrderLogistics(String orderid, String logisticsCode, String logisticsCompany) throws Exception {
		if(StringUtil.isNotEmpty(orderid) 
				&& StringUtil.isNotEmpty(logisticsCode)
				&& StringUtil.isNotEmpty(logisticsCompany)){
			CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
			cq.eq("orderid.id", orderid);
			cq.eq("logisticsnumber", logisticsCode);
			cq.add();
			
			List<OrderLogisticsEntity> orderLogistics = getListByCriteriaQuery(cq, false);
			
			if(null != orderLogistics && orderLogistics.size() > 0){
				OrderLogisticsEntity logisticsEntity = orderLogistics.get(0);
				logisticsEntity.setLogisticscompany(logisticsCompany);
				updateEntitie(logisticsEntity);
			}
		}
	}


	/**
	 * 根据物流单号，获取订单物流信息
	 * @param logisticNumber 物流订单号
	 * @return 订单物流信息(可能会有多条数据)
	 * @throws Exception
	 */
	public List<OrderLogisticsEntity> getOrderLogisticByLogisticNumber(String logisticNumber) throws Exception {
		CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
		if(logisticNumber == null){
			return null;
		}
		
		cq.like("logisticsnumber", "%"+logisticNumber+"%");
		cq.add();
		
		List<OrderLogisticsEntity> list = this.getListByCriteriaQuery(cq, false);
		return list;
	}


	@Override
	public List<Map<String,Object>> getCountOrderLogisticsState(String orderid) {
		
		StringBuffer sqlbuf = new StringBuffer();
		sqlbuf.append(" select state, count(1) from xnc_order_Logistics t where t.orderId = ? group by state ");
		
		return findForJdbc(sqlbuf.toString(), orderid);
	}


	@Override
	public TSDictionary changeLogisticState2OrderFreightState(String orderid) {
		TSDictionary freightState = null;
		int deliverCount = 0, alreadySignCount = 0;
		
		OrderEntity order = findUniqueByProperty(OrderEntity.class, "id", orderid);
		int orderNum = order.getNumber();
		
		List<Map<String,Object>> maplist = getCountOrderLogisticsState(orderid);
		
		for (Map<String, Object> map : maplist) {
			
			for (String key : map.keySet()) {
				
				if(OrderLogisticsServiceI.State.ALREADY_SIGN.getValue().equals(key)){
					
					alreadySignCount = alreadySignCount + (int) map.get(key);
					
				}else{
					deliverCount = deliverCount + (int) map.get(key);
				}
			}
		}
		
		if(orderNum == alreadySignCount){
			
			freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.ALREADY_SIGN.getValue());
			
		}else if(orderNum == (deliverCount + alreadySignCount) 
				&& alreadySignCount >= 0 
				&& deliverCount > 0){
			
			freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.ALREADY_DELIVER.getValue());
			
		}
		return freightState;
	}
	
}