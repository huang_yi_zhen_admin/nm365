package cn.gov.xnc.admin.order.service;

import java.util.List;
import java.util.Map;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;

public interface OrderLogisticsServiceI extends CommonService{
	
	public enum State {
		
		WHERE("在途中", "0"), 
		ALREADY_DELIVER("已发货", "1"), 
		PROBLEM("疑难件", "2"), 
		ALREADY_SIGN("已签收", "3"), 
		RETURN("已退货", "4"), 
		EXCEPTION("异常", "5"), 
		NOWHERE("无轨迹", "6"),
		TERMINUS("到达派件城市", "7");

		private String name;
		private String value;

		private State(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String index) {
			this.value = index;
		}

	}

	public void saveOrderLogistics(OrderEntity order)  throws Exception;
	
	public void deleteAllOrderLogistics(OrderEntity order)  throws Exception;
	
	public void deleteOrderLogistics(String orderid, String logisticsCode)  throws Exception;
	
	public void updateOrderLogistics(String orderid, String logisticsCode, String logisticsCompany)  throws Exception;
	
	public AjaxJson getExpressInfo(String orderid, String logisticsCode, String logisticsCompany) throws Exception;
	
	/**
	 * 根据物流单号，获取订单物流信息
	 * @param logisticNumber 物流订单号
	 * @return 订单物流信息(可能会有多条数据)
	 * @throws Exception
	 */
	public List<OrderLogisticsEntity> getOrderLogisticByLogisticNumber(String logisticNumber)throws Exception;
	
	/**
	 * 获取已经派送订单物流单各状态数量
	 * @return
	 */
	public List<Map<String,Object>> getCountOrderLogisticsState(String orderid);
	
	/**
	 * 获取订单打包之后的物流状态
	 * @param orderid
	 * @return
	 */
	public TSDictionary changeLogisticState2OrderFreightState(String orderid);
}
