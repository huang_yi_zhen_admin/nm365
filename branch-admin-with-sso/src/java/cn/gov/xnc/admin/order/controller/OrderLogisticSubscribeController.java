package cn.gov.xnc.admin.order.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.order.entity.OrderLogisticSubscribeEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticSubscribeServiceI;

/**   
 * @Title: Controller
 * @Description: 订单物流订阅信息
 * @author zero
 * @date 2018-03-12 16:46:45
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderLogisticSubscribeController")
public class OrderLogisticSubscribeController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderLogisticSubscribeController.class);

	@Autowired
	private OrderLogisticSubscribeServiceI orderLogisticSubscribeService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 订单物流订阅信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView orderLogisticSubscribe(HttpServletRequest request) {
		return new ModelAndView("admin/order/orderLogisticSubscribeList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderLogisticSubscribeEntity orderLogisticSubscribe,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(OrderLogisticSubscribeEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderLogisticSubscribe, request.getParameterMap());
		this.orderLogisticSubscribeService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除订单物流订阅信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(OrderLogisticSubscribeEntity orderLogisticSubscribe, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		orderLogisticSubscribe = systemService.getEntity(OrderLogisticSubscribeEntity.class, orderLogisticSubscribe.getId());
		message = "订单物流订阅信息删除成功";
		orderLogisticSubscribeService.delete(orderLogisticSubscribe);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加订单物流订阅信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderLogisticSubscribeEntity orderLogisticSubscribe, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(orderLogisticSubscribe.getId())) {
			message = "订单物流订阅信息更新成功";
			OrderLogisticSubscribeEntity t = orderLogisticSubscribeService.get(OrderLogisticSubscribeEntity.class, orderLogisticSubscribe.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(orderLogisticSubscribe, t);
				orderLogisticSubscribeService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "订单物流订阅信息更新失败";
			}
		} else {
			message = "订单物流订阅信息添加成功";
			orderLogisticSubscribeService.save(orderLogisticSubscribe);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 订单物流订阅信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderLogisticSubscribeEntity orderLogisticSubscribe, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(orderLogisticSubscribe.getId())) {
			orderLogisticSubscribe = orderLogisticSubscribeService.getEntity(OrderLogisticSubscribeEntity.class, orderLogisticSubscribe.getId());
			req.setAttribute("orderLogisticSubscribePage", orderLogisticSubscribe);
		}
		return new ModelAndView("admin/order/orderLogisticSubscribe");
	}
}
