package cn.gov.xnc.admin.order.entity;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 收支流水
 * @author zero
 * @date 2016-10-02 02:02:45
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_pay_bills", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class PayBillsEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**支付编号*/
	private java.lang.String identifier;
	/**客户名称*/
	private TSUser clientid;
	/**公司信息*/
	private TSCompany company;
	/**收支类型  支付_1,充值_2*/
	private java.lang.String type;
	/**状态 确认_1,待审核_2,未通过_3,系统确认_4,支付失败_5*/
	private java.lang.String state;
	/**支付方式 支付宝_1,微信支付_2,线下转账_3,预存款支付_4*/
	private java.lang.String paymentmethod;
	/**收款*/
	private BigDecimal payablemoney;
	/**付款*/
	private BigDecimal alreadypaidmoney;
	/**余额*/
	private BigDecimal balancemoney;
	/**支付凭证 支付宝 微信流水号  银行备注流水id*/
	private java.lang.String voucher;
	/**银行账号*/
	private java.lang.String bankAccount;
	/**开户银行*/
	private java.lang.String bank;
	/**银行转账时间*/
	private java.util.Date bankDate;
	/**转账凭证*/
	private java.lang.String bankUrl;
	/**remark*/
	private java.lang.String remark;
	
	/**支付名称*/
	private java.lang.String payName;
	/**updatedate*/
	private java.util.Date updatedate;
	/**createdate*/
	private java.util.Date createdate;
	/**updateuser*/
	private java.lang.String updateuser;
	/**createuser*/
	private java.lang.String createuser;
	
	/**审核人*/
	private TSUser audituser;
	/**审核时间*/
	private java.util.Date auditdate;

	
	/**支付流水对应关系list*/
	private List<PayBillsFlowEntity> payBillsFlowS = new ArrayList<PayBillsFlowEntity>();
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  支付编号
	 */
	@Column(name ="IDENTIFIER",nullable=true,length=32)
	public java.lang.String getIdentifier(){
		return this.identifier;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  支付编号
	 */
	public void setIdentifier(java.lang.String identifier){
		this.identifier = identifier;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户名称
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLIENTID")
	public TSUser getClientid(){
		return this.clientid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户名称
	 */
	public void setClientid(TSUser clientid){
		this.clientid = clientid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司信息
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany(){
		return this.company;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company){
		this.company = company;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收支类型  支付_1,充值_2
	 */
	@Column(name ="TYPE",nullable=true,length=255)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收支类型  支付_1,充值_2
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态 确认_1,待审核_2,未通过_3,系统确认_4,支付失败_5
	 */
	@Column(name ="STATE",nullable=true,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态 确认_1,待审核_2,未通过_3,系统确认_4,支付失败_5
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  支付方式 支付宝_1,微信支付_2,线下转账_3,预存款支付_4,线下转账_5
	 */
	@Column(name ="PAYMENTMETHOD",nullable=true,length=255)
	public java.lang.String getPaymentmethod(){
		return this.paymentmethod;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  支付方式 支付宝_1,微信支付_2,线下转账_3,预存款支付_4
	 */
	public void setPaymentmethod(java.lang.String paymentmethod){
		this.paymentmethod = paymentmethod;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  收款
	 */
	@Column(name ="PAYABLEMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getPayablemoney(){
		return this.payablemoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  收款
	 */
	public void setPayablemoney(BigDecimal payablemoney){
		this.payablemoney = payablemoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  付款
	 */
	@Column(name ="ALREADYPAIDMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getAlreadypaidmoney(){
		return this.alreadypaidmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  付款
	 */
	public void setAlreadypaidmoney(BigDecimal alreadypaidmoney){
		this.alreadypaidmoney = alreadypaidmoney;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  余额
	 */
	@Column(name ="BALANCEMONEY",nullable=true,length=255)
	public BigDecimal getBalancemoney(){
		return this.balancemoney;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  余额
	 */
	public void setBalancemoney(BigDecimal balancemoney){
		this.balancemoney = balancemoney;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  支付凭证 支付宝 微信流水号  银行备注流水id
	 */
	@Column(name ="VOUCHER",nullable=true,length=1000)
	public java.lang.String getVoucher(){
		return this.voucher;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  支付凭证 支付宝 微信流水号  银行备注流水id
	 */
	public void setVoucher(java.lang.String voucher){
		this.voucher = voucher;
	}
	
	
	
	
	
	/**
	 * @return 开户行账号
	 */
	@Column(name ="BANKACCOUNT",nullable=true,length=1000)
	public java.lang.String getBankAccount() {
		return bankAccount;
	}

	/**
	 * @param 开户行账号
	 */
	public void setBankAccount(java.lang.String bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 * @return 开户行账号
	 */
	@Column(name ="BANK",nullable=true,length=1000)
	public java.lang.String getBank() {
		return bank;
	}

	/**
	 * @param 开户行账号
	 */
	public void setBank(java.lang.String bank) {
		this.bank = bank;
	}

	/**
	 * @return 转账时间
	 */
	@Temporal(TemporalType.DATE)
	@Column(name ="BANKDATE",nullable=true)
	public java.util.Date getBankDate() {
		return bankDate;
	}

	/**
	 * @param 转账时间
	 */
	public void setBankDate(java.util.Date bankDate) {
		this.bankDate = bankDate;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  remark
	 */
	@Column(name ="REMARK",nullable=true,length=6000)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  remark
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  updatedate
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  updatedate
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  createdate
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  createdate
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	
	
	
	/**
	 * @return the createuser
	 */
	@Column(name ="CREATEUSER",nullable=true,length=32)
	public java.lang.String getCreateuser() {
		return createuser;
	}

	/**
	 * @param createuser the createuser to set
	 */
	public void setCreateuser(java.lang.String createuser) {
		this.createuser = createuser;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  updateuser
	 */
	@Column(name ="UPDATEUSER",nullable=true,length=32)
	public java.lang.String getUpdateuser(){
		return this.updateuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  updateuser
	 */
	public void setUpdateuser(java.lang.String updateuser){
		this.updateuser = updateuser;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  审核人
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUDITUSER")
	public TSUser getAudituser(){
		return this.audituser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  审核人
	 */
	public void setAudituser(TSUser audituser){
		this.audituser = audituser;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  审核时间
	 */
	@Column(name ="AUDITDATE",nullable=true)
	public java.util.Date getAuditdate(){
		return this.auditdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  审核时间
	 */
	public void setAuditdate(java.util.Date auditdate){
		this.auditdate = auditdate;
	}

	/**
	 * @return 支付流水对应关系list
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "billsid")
	public List<PayBillsFlowEntity> getPayBillsFlowS() {
		return payBillsFlowS;
	}

	/**
	 * @param 支付流水对应关系list
	 */
	public void setPayBillsFlowS(List<PayBillsFlowEntity> payBillsFlowS) {
		this.payBillsFlowS = payBillsFlowS;
	}
	
	@Column(name ="BANKURL",nullable=true,length=500)
	public java.lang.String getBankUrl() {
		return bankUrl;
	}

	public void setBankUrl(java.lang.String bankUrl) {
		this.bankUrl = bankUrl;
	}
	
	@Transient
	public java.lang.String getPayName() {
		return payName;
	}

	public void setPayName(java.lang.String payName) {
		this.payName = payName;
	}
	
	
}
