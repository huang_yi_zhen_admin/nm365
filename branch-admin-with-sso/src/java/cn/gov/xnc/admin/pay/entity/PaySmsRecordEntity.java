package cn.gov.xnc.admin.pay.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * @Title: Entity
 * @Description:  短信购买记录
 * @author jason
 *
 */
@Entity
@Table(name = "xnc_pay_sms_record", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class PaySmsRecordEntity implements java.io.Serializable{
	private java.lang.String id;
	private java.lang.String payBillsId;//收支流水ID
	private java.lang.String user_id;//用户ID
	private java.lang.String company_id;//交易号
	private BigDecimal trade_amount;//交易金额
	private Integer number;//数量
	private java.lang.String returnParams;//返回的参数
	private java.util.Date createDate;//创建日期
	private java.util.Date updateDate;//更新日期
	

	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
	@Column(name ="payBillsId",nullable=false,length=32)
	public java.lang.String getPayBillsId() {
		return payBillsId;
	}
	public void setPayBillsId(java.lang.String payBillsId) {
		this.payBillsId = payBillsId;
	}
	@Column(name ="user_id",nullable=false,length=32)
	public java.lang.String getUser_id() {
		return user_id;
	}
	public void setUser_id(java.lang.String user_id) {
		this.user_id = user_id;
	}
	@Column(name ="company_id",nullable=false,length=32)
	public java.lang.String getCompany_id() {
		return company_id;
	}
	public void setCompany_id(java.lang.String company_id) {
		this.company_id = company_id;
	}
	@Column(name ="trade_amount",nullable=true,length=32)
	public BigDecimal getTrade_amount() {
		return trade_amount;
	}
	public void setTrade_amount(BigDecimal trade_amount) {
		this.trade_amount = trade_amount;
	}
	@Column(name ="number",nullable=false,length=32)
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	@Column(name ="returnParams",nullable=false,length=50)
	public java.lang.String getReturnParams() {
		return returnParams;
	}
	public void setReturnParams(java.lang.String returnParams) {
		this.returnParams = returnParams;
	}
	@Column(name ="createDate",nullable=true,length=32)
	public java.util.Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	@Column(name ="updateDate",nullable=true,length=32)
	public java.util.Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}
	
		
}
