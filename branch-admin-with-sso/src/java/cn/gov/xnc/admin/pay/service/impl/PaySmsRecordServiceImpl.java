package cn.gov.xnc.admin.pay.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.mobile.util.JsonUtil;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.pay.config.CommonConfig;
import cn.gov.xnc.admin.pay.dao.PaySmsRecordDAO;
import cn.gov.xnc.admin.pay.entity.PaySmsRecordEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

@Service("paySmsRecordService")
public class PaySmsRecordServiceImpl {
	@Resource(name="paySmsRecordDAO")
	private PaySmsRecordDAO paySmsRecordDAO;
	@Resource(name="smsRelateService")
	private SmsRelateServiceImpl smsRelateService;
	private static final Logger logger = Logger.getLogger(PaySmsRecordServiceImpl.class);
	/**
	 * 支付查询，根据订单号
	 * */
	public PaySmsRecordEntity getPaymentRecordBypayBid(String payBillsId){
		PaySmsRecordEntity paymentRecord = new PaySmsRecordEntity();
		paymentRecord.setPayBillsId(payBillsId);
		return paySmsRecordDAO.getPaymentRecordBypayBid(paymentRecord);
	}
	
	/**
	 * 设置保存属性
	 * */
	public void paymentRecordSave(String payBillsId, BigDecimal tradeAmount, Integer number, String des, String alipayid){
		PaySmsRecordEntity paymentRecord = new PaySmsRecordEntity();
		paymentRecord.setPayBillsId(payBillsId);
		paymentRecord.setTrade_amount(tradeAmount);
		paymentRecord.setNumber(number);
		if(this.getPaymentRecordBypayBid(payBillsId) == null){
			this.paymentRecordSave(paymentRecord);
		}
	}
	/**
	 * 支付保存
	 * */
	public Serializable paymentRecordSave(PaySmsRecordEntity paymentRecord){
		return paySmsRecordDAO.paymentRecordSave(paymentRecord);
	}
	/**
	 * 支付记录列表
	 * */
	public DataGrid getPaymentRecordListPage(DataGrid dataGrid, PaySmsRecordEntity paymentRecord, TSCompany tSCompany){
		dataGrid = paySmsRecordDAO.getPaymentRecordListPage(dataGrid, paymentRecord, tSCompany);
		return dataGrid;
	}
	
	/**
	 * 短信购买记录，回调更新
	 * @return 
	 * */
	public Integer paymentRecordUpdate(Map<String, String> params, PayBillsEntity payment){
		logger.error("######################################################################");
		logger.error("########成功回调执行逻辑#####################");
		logger.error("params:"+params.toString());
		
		Integer result = 0;
		if(payment != null){
			logger.error("payment.getId():"+payment.getId());
			logger.error("payment.getType():"+payment.getType());
			if(payment.getType().equals(CommonConfig.PayApp.SMS.toString())){
				this.paymentRecordUpdateByPayBillsId(payment.getId(), initReturnParams(params));//支付成功后更新短信购买记录表
				result = smsRelateService.companyReviewedUpdateById(payment.getId());//支付成功后更新短信条数
			}
		}
		logger.error("######################################################################");
		return result;
	}
	
	/**
	 * 支付记录更新,by id
	 * @return 
	 * */
	public Integer paymentRecordUpdateById(PaySmsRecordEntity paymentRecord){
		return paySmsRecordDAO.paymentRecordUpdateById(paymentRecord);
	}
	/**
	 * 设置更新属性
	 * */
	public void paymentRecordUpdateByPayBillsId(String payBillsId, String params){
		PaySmsRecordEntity paymentRecord = new PaySmsRecordEntity();
		paymentRecord.setPayBillsId(payBillsId);
		paymentRecord.setReturnParams(params);
		this.paymentRecordUpdateByPayBillsId(paymentRecord);
	}
	/**
	 * 支付记录更新,by IdentifierOr
	 * @return 
	 * */
	public Integer paymentRecordUpdateByPayBillsId(PaySmsRecordEntity paymentRecord){
		return paySmsRecordDAO.paymentRecordUpdateByPayBillsId(paymentRecord);
	}
	/**
	 * 返回参数进行封装
	 * */
	private static String initReturnParams(Map<String, String> params){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("tradeStatus", params.get("trade_status"));
		map.put("buyerId", params.get("buyer_id"));
		map.put("buyerEmail", params.get("buyer_email"));
		map.put("sellerId", params.get("seller_id"));
		map.put("sellerEmail", params.get("seller_email"));
		map.put("notifyId", params.get("notify_id"));
		map.put("notifyTime", params.get("notify_time"));
		return JsonUtil.toJson(map);
	}
}
