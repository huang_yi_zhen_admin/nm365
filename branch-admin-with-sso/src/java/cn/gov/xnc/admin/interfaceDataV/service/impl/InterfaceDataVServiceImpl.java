package cn.gov.xnc.admin.interfaceDataV.service.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.admin.contentManage.dao.ContentManageDAO;
import cn.gov.xnc.admin.contentManage.entity.ArticleEntity;
import cn.gov.xnc.admin.contentManage.entity.SectionEntity;
import cn.gov.xnc.admin.interfaceDataV.dao.InterfaceDataVDAO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;


@Service("interfaceDataVService")
public class InterfaceDataVServiceImpl {
	private StringBuilder sbd;
	private static Calendar c = Calendar.getInstance();
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Resource(name="orderService")
	private OrderServiceI orderService;
	@Resource(name="interfaceDataVDAO")
	private InterfaceDataVDAO interfaceDataVDAO;
	
	/**
	 * 查询订单状态的订单量：1待付、2已付、3未付、4,5已取消
	 * */
	public List<Map<String, String>> getPayBillsOrderByState(Integer m1, Integer m2, Integer s){
		List<Map<String, String>> maplist = new ArrayList<Map<String,String>>();
		String begin= m1<=9 ? "0"+m1 : m1+"";
		String end = m2<=9 ? "0"+m2 : m2+"";
		Map<String, Object> map = interfaceDataVDAO.getPayBillsOrderCount(begin, end);
		Map<String, String> outMap = null;
		for (String key : map.keySet()) {  
			outMap = new HashMap<String,String>();
			outMap.put("x", key);
			outMap.put("y", map.get(key)+"");
			outMap.put("s", s+"");
			maplist.add(outMap);
		}
		return maplist;
	}
	/**
	 * 显示商品订单概况，查询特定状态下订单量的总合：2待发货、3已发货、4已签收、5,6已取消
	 * */
	public List<Map<String, Object>> getOrderCount(Integer m1, Integer m2, Integer cases){
		List<Map<String, Object>> maplist = new ArrayList<Map<String,Object>>();
		String begin= m1<=9 ? "0"+m1 : m1+"";
		String end = m2<=9 ? "0"+m2 : m2+"";
		Map<String, Object> map = interfaceDataVDAO.getOrderCount(begin, end, cases);
		maplist.add(map);
		return maplist;
	}
	
	/**
	* 产品销售统计,商品的总数量、总销售金额、总运费统计
	*/
	public List<Map<String, Object>> getSalesCount(Integer m1, Integer m2, Integer showNum) {
		List<Map<String, Object>> maplist = new ArrayList<Map<String,Object>>();
		String begin= m1<=9 ? "0"+m1 : m1+"";
		String end = m2<=9 ? "0"+m2 : m2+"";
		List<OrderEntity> orderList = interfaceDataVDAO.getSalesCount(begin, end, showNum);
		Map<String, Object> map = null;
		if(orderList!=null && orderList.size()>0){
			for(OrderEntity o : orderList){
				map = new HashMap<String, Object>();
				map.put("商品名称", o.getProductname());
				map.put("总数量", o.getNumber());
				map.put("总销售金额", o.getPrice());
				maplist.add(map);
			}
		}
		return maplist;
	}
	
	/**
	* 获取仓库相关数据
	*/
	public List<Map<String, Object>> getStockList(Integer showNum) {
		List<Map<String, Object>> mapList = interfaceDataVDAO.getStockList(showNum);
		List<Map<String, Object>> outMaplist = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = null;
		String status = "";
		for(Map<String,Object> m : mapList){
			status = m.get("status")+"";
			if(status.equals("1")){
				status = "充足";
			}else if(status.equals("2")){
				status = "缺货";
			}else if(status.equals("3")){
				status = "补货";
			}
			map = new HashMap<String, Object>();
			map.put("商品名称", m.get("productName"));
			map.put("品牌名称", m.get("productBrandName") == null ? "" : m.get("productBrandName"));
			map.put("仓库名称", m.get("stockname"));
			map.put("当前库存", m.get("stocknum"));
			map.put("库存状态", status);
			outMaplist.add(map);
		}
		return outMaplist;
	}
	/**
	* 获取派单相关数据
	*/
	public List<Map<String, Object>> getCopartnerList(Integer m1, Integer m2, Integer showNum){
		String begin= m1<=9 ? "0"+m1 : m1+"";
		String end = m2<=9 ? "0"+m2 : m2+"";
		List<Map<String, Object>> mapList = interfaceDataVDAO.getCopartnerList(begin, end, showNum);
		List<Map<String, Object>> outMaplist = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = null;
		for(Map<String,Object> m : mapList){
			map = new HashMap<String, Object>();
			map.put("x", m.get("receiverUserName"));
			map.put("y", m.get("Price"));
			outMaplist.add(map);
		}
		return outMaplist;
	}
	/**
	 * 热销省份统计
	 * */
	public List<Map<String, Object>> getAreaSalesCount(Integer m1, Integer m2, Integer showNum){
		String begin= m1<=9 ? "0"+m1 : m1+"";
		String end = m2<=9 ? "0"+m2 : m2+"";
		List<OrderEntity> orderList = interfaceDataVDAO.getAreaSalesCount(begin, end);
		List<Map<String, Object>> outMaplist = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = null;
		int count = 1;
		Integer r = 0;
		for(OrderEntity o : orderList){
				map = new HashMap<String, Object>();
				if( StringUtil.isNotEmpty(o.getProvince()) ){
					o.setProvince(o.getProvince().substring(0,2));
					if("黑龙".equals(o.getProvince())){
						o.setProvince("黑龙江");
					}else if ( "内蒙".equals(o.getProvince()) ){
						o.setProvince("内蒙古");
					}
				}else {
					o.setProvince("自提");
				}
				if(!o.getProvince().equals("自提")){
					//System.out.println("o.getProvince():"+o.getProvince());
					//System.out.println("o.getPrice():"+o.getPrice());
					//System.out.println("count:"+count);
					//System.out.println("showNum:"+showNum);
					map.put("x", o.getProvince());
					map.put("y", o.getPrice());
					if(o.getPrice() != null){
						r = o.getPrice().intValue()/4;
					}
					map.put("r", r);
					outMaplist.add(map);
					if(count>=showNum){
						break;
					}
					count++;
				}

			
		}
		return outMaplist;
	}
	
	/**
	 * 款项明细
	 * */
	public List<Map<String, Object>> getPaymentDetails(String state, Integer m1, Integer m2){
		Integer year = c.get(Calendar.YEAR);
		String startTime = "";
		String endTime = "";
		if(m1>0 && m2>0){
			String begin= m1<=9 ? "0"+m1 : m1+"";
			String end = m2<=9 ? "0"+m2 : m2+"";
			startTime = year + "-" + begin + "-" + "01" + " 00:00:00";
			endTime = year + "-" + end + "-" + "31" + " 24:00:00";
		}
		return interfaceDataVDAO.getPaymentDetails(state, startTime, endTime);
	}
	/**
	 * 预存款
	 * */
	public List<Map<String, Object>> getPreDeposit(){
		return interfaceDataVDAO.getPreDeposit();
	}
	/**
	 * 采购商数据统计
	 **/
	public List<Map<String, Object>> qdxs(Map<String,Object> map) {
		List<Map<String, Object>> maplist = new ArrayList<Map<String,Object>>();
		Map<String, Object> orderMap;
		String companyId = map.get("companyId") != null ? map.get("companyId") + "" : "";
		String userid = map.get("userid") != null ? map.get("userid") + "" : "";
		String startTime = c.get(Calendar.YEAR)+"-01-01 00:00:00";
		String endTime = sdf.format(c.getTime());
		Integer showNum = map.get("showNum") != null ? (Integer)map.get("showNum") : 10;
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		
		List<OrderEntity> salesOrderList  = interfaceDataVDAO.salesQDXS( companyId ,userid ,  startTime, endTime );
		for(OrderEntity o : salesOrderList){
			orderMap = new HashMap<String, Object>();
			orderMap.put("x", o.getProductname());
			orderMap.put("y", o.getPrice());
			orderMap.put("z", o.getNumber());
			maplist.add(orderMap);
		}
		return maplist;
		/*if( salesOrderList != null && salesOrderList.size() > 0){
			for( OrderEntity o:salesOrderList ){
				TSUser t = new TSUser();
				if( StringUtil.isNotEmpty(o.getId())){
					t = systemService.findUniqueByProperty (TSUser.class,"id" ,o.getId());
			    }
				o.setClientid(t);
			}
		}*/
	}
	
	
	/**
	 * 地区销售情况
	 * */
	public List<Map<String, Object>> yh(Map<String,Object> map){
		List<Map<String, Object>> maplist = new ArrayList<Map<String,Object>>();
		Map<String, Object> orderMap;
		String companyId = map.get("companyId") != null ? map.get("companyId") + "" : "";
		String province = map.get("province") != null ? map.get("province") + "" : "";// 省份信息
		
		String userCLid = map.get("userCLid") != null ? map.get("userCLid") + "" : "";
		
		String startTime = c.get(Calendar.YEAR)+"-01-01 00:00:00";
		String endTime = sdf.format(c.getTime());
	
		Integer showNum = map.get("showNum") != null ? (Integer)map.get("showNum") : 10;
		
		List<OrderEntity> salesOrderList  = interfaceDataVDAO.salesYH( companyId , userCLid , province,  startTime, endTime );
		for(OrderEntity o : salesOrderList){
			if( StringUtil.isNotEmpty(o.getProvince()) ){
				province = o.getProvince().substring(0,2);
				if("黑龙".equals(province)){
					province = "黑龙江";
				}else if ( "内蒙".equals(province) ){
					province = "内蒙古";
				}
			}else {
				province = "自提";
			}

			orderMap = new HashMap<String, Object>();
			orderMap.put("x", province);
			orderMap.put("y", o.getPrice());
			orderMap.put("z", o.getNumber());
			maplist.add(orderMap);
		}
		return maplist;
	}
	/**
	 * 发货区域统计
	 * */
	public List<Map<String, Object>> invoice(Map<String,Object> map){
		List<Map<String, Object>> maplist = new ArrayList<Map<String,Object>>();
		Map<String, Object> orderMap;
		String companyId = map.get("companyId") != null ? map.get("companyId") + "" : "";
		String productid = map.get("productid") != null ? map.get("productid") + "" : "";
		String userCLid = map.get("userCLid") != null ? map.get("userCLid") + "" : "";
		String province = "";
		String startTime = c.get(Calendar.YEAR)+"-01-01 00:00:00";
		String endTime = sdf.format(c.getTime());
		
		Integer showNum = map.get("showNum") != null ? (Integer)map.get("showNum") : 10;
		
		List<OrderEntity> salesOrderList  = interfaceDataVDAO.salesdeparture( companyId , userCLid , productid,  startTime, endTime );
		for(OrderEntity o : salesOrderList){
			if( StringUtil.isNotEmpty(o.getProvince()) ){
				province = o.getProvince().substring(0,2);
				if("黑龙".equals(province)){
					province = "黑龙江";
				}else if ( "内蒙".equals(province) ){
					province = "内蒙古";
				}
			}

			orderMap = new HashMap<String, Object>();
			orderMap.put("x", province);
			orderMap.put("y", o.getPrice());
			orderMap.put("z", o.getNumber());
			maplist.add(orderMap);
		}
		return maplist;
	}
	
}
