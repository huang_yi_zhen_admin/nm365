package cn.gov.xnc.admin.interfaceDataV.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.core.util.StringUtil;
@Service("interfaceDataVDAO")
public class InterfaceDataVDAO {
	@Resource(name="commonService")
	private CommonService commonService;
	private static Calendar c = Calendar.getInstance();
	private String companyId = "4028802f58149a320158149cdd570004";
	/**
	 * .显示订单概况，查询订单支付状态维度：1待付、2已付、3未付、4,5已取消
	 * */
	public Map<String, Object> getPayBillsOrderCount(String begin, String end){
		Integer year = c.get(Calendar.YEAR);
		long count = 0;
		String beginTime = year + "-" + begin + "-" + "01" + " 00:00:00";
		String endTime = year + "-" + end + "-" + "31" + " 24:00:00";
		Map<String, Object> state = new HashMap<String,Object>();
		System.out.println("###SELECT count(1) from xnc_pay_bills_order where company='4028802f58149a320158149cdd570004' and state=1 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
		count = commonService.getCountForJdbc("SELECT count(1) from xnc_pay_bills_order where company='"+companyId+"' and state=1 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
		state.put("待付", count);
		count = commonService.getCountForJdbc("SELECT count(1) from xnc_pay_bills_order where company='"+companyId+"' and state=2 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
		state.put("已付", count);
		count = commonService.getCountForJdbc("SELECT count(1) from xnc_pay_bills_order where company='"+companyId+"' and state=3 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
		state.put("未付", count);
		count = commonService.getCountForJdbc("SELECT count(1) from xnc_pay_bills_order where company='"+companyId+"' and state in(4,5) and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
		state.put("已取消", count);
		return state;
	}
	/**
	 * 显示商品订单概况，查询特定状态下订单量的总合：2待发货、3已发货、4已签收、5,6已取消
	 * */
	public Map<String, Object> getOrderCount(String begin, String end, Integer cases){
		Integer year = c.get(Calendar.YEAR);
		long count = 0;
		String beginTime = year + "-" + begin + "-" + "01" + " 00:00:00";
		String endTime = year + "-" + end + "-" + "31" + " 24:00:00";
		Map<String, Object> state = new HashMap<String,Object>();
		switch(cases){
		case 2 : count = commonService.getCountForJdbc("SELECT count(1) from xnc_order where company='"+companyId+"' and state=2 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
				System.out.println("###SELECT count(1) from xnc_order where company='"+companyId+"' and state=2 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
				break;
		case 3 : count = commonService.getCountForJdbc("SELECT count(1) from xnc_order where company='"+companyId+"' and state=3 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
				break;
		case 4 : count = commonService.getCountForJdbc("SELECT count(1) from xnc_order where company='"+companyId+"' and state=4 and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");
				break;
		case 5: count = commonService.getCountForJdbc("SELECT count(1) from xnc_order where company='"+companyId+"' and state in(5,6) and createDate>='" + beginTime + "' and createDate<='" + endTime + "'");		
				break;
		}
		state.put("value", count);
		return state;
	}
	
	/**
	* 产品销售统计,商品的总数量、总销售金额、总运费统计
	*/
	public List<OrderEntity> getSalesCount(String begin, String end, Integer showNum) {
		Integer year = c.get(Calendar.YEAR);
		String beginTime = year + "-" + begin + "-" + "01" + " 00:00:00";
		String endTime = year + "-" + end + "-" + "31" + " 24:00:00";
		//String Sql ="SELECT    productname ,  SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)     and xnc_order.createDate >='" + beginTime + "' and xnc_order.createDate <='" + endTime + "' and  xnc_pay_bills_order.company = '"+companyId+"'  GROUP BY    xnc_order.productname  ORDER BY Price   desc  limit " + showNum ;
		String Sql ="SELECT    productname ,  SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)     and xnc_order.createDate >='" + beginTime + "' and xnc_order.createDate <='" + endTime + "' and  xnc_pay_bills_order.company = '"+companyId+"'  GROUP BY    xnc_order.productname  ORDER BY Price   desc";
		System.out.println("#"+Sql);
		
		List<OrderEntity>  list = commonService.queryListByJdbc(Sql, OrderEntity.class);
		
		return list;
	}
	
	/**
	* 获取仓库相关数据
	*/
	public List<Map<String, Object>> getStockList(Integer showNum) {
		List<Map<String, Object>> mapList = null;
		Map<String, Object> resultMap = null;
		String productid = "";
		String stockid = "";
		String Sql = "select productid, stockid, stocknum, status from xnc_company_stock_product where companyid='"+companyId+"' limit " + showNum;
		System.out.println("#"+Sql);
		
		mapList = commonService.findForJdbc(Sql, null);
		if(mapList!=null && mapList.size()>0){
			for(Map<String, Object> m : mapList){
				productid = m.get("productid")+"";
				stockid  = m.get("stockid")+"";
				Sql = "select name,brandId from xnc_product where id='" + productid + "'";
				resultMap = commonService.findOneForJdbc(Sql, null);
				if(resultMap!=null){
					m.put("productName", resultMap.get("name"));
					Sql = "select brandName from xnc_product_brand where id='" + resultMap.get("brandId") + "'";
					resultMap = commonService.findOneForJdbc(Sql, null);
					if(resultMap!=null){
						m.put("productBrandName", resultMap.get("brandName"));
					}
				}
				Sql = "select stockname from xnc_company_stock where id='" + stockid + "'";
				resultMap = commonService.findOneForJdbc(Sql, null);
				if(resultMap!=null){
					m.put("stockname", resultMap.get("stockname"));
				}
			}
		}
		return mapList;
	}
	/**
	* 获取派单相关数据
	*/
	public List<Map<String, Object>> getCopartnerList(String begin, String end, Integer showNum){
		List<Map<String, Object>> mapList = null;
		Map<String, Object> resultMap = null;
		Integer year = c.get(Calendar.YEAR);
		String receiver = "";
		String beginTime = year + "-" + begin + "-" + "01" + " 00:00:00";
		String endTime = year + "-" + end + "-" + "31" + " 24:00:00";

		String Sql ="select xnc_order_send.receiver, SUM(xnc_order.Price) as Price "
				+ "from xnc_order_send  inner join xnc_order on xnc_order_send.ORDERID = xnc_order.id "
				+ "where xnc_order_send.status in(1,2) "
				+ "and distributeCompanyId='"+companyId+"' "
						+ "and xnc_order_send.createtime >= '"+beginTime+"' and xnc_order_send.createtime <= '"+endTime+"'"
						//+ "order by xnc_order_send.createtime desc limit " + showNum ;
						+ "GROUP BY xnc_order_send.receiver ";
						//+ "order by xnc_order_send.createtime desc";
		System.out.println("#"+Sql);
		
		mapList = commonService.findForJdbc(Sql, null);
		if(mapList!=null && mapList.size()>0){
			for(Map<String, Object> m : mapList){
				receiver = m.get("receiver")+"";
				Sql = "SELECT userName from t_s_user where id='" + receiver + "'";
				resultMap = commonService.findOneForJdbc(Sql, null);
				if(resultMap!=null){
					m.put("receiverUserName", resultMap.get("userName"));
				}
			}
		}
		return mapList;
	}
	
	/**
	 * 热销省份统计
	 * */
	public List<OrderEntity> getAreaSalesCount(String begin, String end){
		Integer year = c.get(Calendar.YEAR);
		String beginTime = year + "-" + begin + "-" + "01" + " 00:00:00";
		String endTime = year + "-" + end + "-" + "31" + " 24:00:00";
		//String Sql ="SELECT    productname ,  SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)     and xnc_order.createDate >='" + beginTime + "' and xnc_order.createDate <='" + endTime + "' and  xnc_pay_bills_order.company = '"+companyId+"'  GROUP BY    xnc_order.productname  ORDER BY Price   desc  limit " + showNum ;
		String Sql ="SELECT  province, SUM(Price) as Price , SUM(Number) as Number "
				+ "FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id "
				+ "WHERE  xnc_order.state in(2,3,4) "
				+ "and xnc_order.createDate >='"+beginTime+"' and xnc_order.createDate <='"+endTime+"' "
				+ "and  xnc_pay_bills_order.company = '"+companyId+"' "
				+ "GROUP BY  province  ORDER BY Price desc";
		System.out.println("#"+Sql);
		
		List<OrderEntity>  list = commonService.queryListByJdbc(Sql, OrderEntity.class);
		
		return list;
	}
	
	/**
	* 采购商数据统计
	* 
	* String productId 商品id
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesQDXS(String  companyId , String userid, String startTime, String endTime) {
		
		
		String productSQL ="";//产品
		/*if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}*/
		if(StringUtil.isNotEmpty(userid) && userid.indexOf(",") > 0){
			productSQL = " and  xnc_order.clientId in("+userid+")    ";
		}else if(StringUtil.isNotEmpty(userid) ){
			productSQL = " and  xnc_order.clientId = '"+userid+"' ";
		}

		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}
		
		if(!companyId.equals("")){
			createDateSQL = createDateSQL + "and  xnc_pay_bills_order.company = '"+companyId+"'";
		}
		
		//String Sql =" SELECT     xnc_pay_bills_order.clientId as id ,  SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " + productSQL + createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"'     GROUP BY     xnc_pay_bills_order.clientId    ORDER BY Price   desc " ;
		//String Sql =" SELECT  productName, Price   , Number   , freightPic , xnc_order.createDate as createdate  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " + productSQL + createDateSQL + "  ORDER BY Price   desc LIMIT 0,10" ;
		String Sql =" SELECT  productName, SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " + productSQL + createDateSQL + " GROUP BY  productname  ORDER BY Price   desc " ;
		//System.out.println("#"+Sql);
		List<OrderEntity>  list = commonService.queryListByJdbc(Sql,OrderEntity.class);		
		return list;
	}
	/**
	* 地区销售情况
	* 
	* String userCLid 采购商id
	* String province 省份中文名称
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesYH( String companyId ,String userCLid ,String province, String startTime, String endTime) {
		
		
		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		
		String provinceSQL ="";//省份
		if(StringUtil.isNotEmpty(province) ){
			provinceSQL = " and  xnc_order.province = '"+province+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}
		
		if(!companyId.equals("")){
			createDateSQL = createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"' ";
		}
		
		String Sql =" SELECT  province , SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " +provinceSQL+ userCLidSQL+ createDateSQL + "  and LENGTH(province)>0    GROUP BY  province  ORDER BY Price   desc   LIMIT 0,10" ;
		System.out.println("#"+Sql);
		List<OrderEntity>  list = commonService.queryListByJdbc(Sql,OrderEntity.class);	
		
		return list;
	}
	
	/**
	* 发货区域统计
	* 
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/

	public List<OrderEntity> salesdeparture( String companyId ,String userCLid ,String productId, String startTime, String endTime) {
	

		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		
		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.departuredate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.departuredate >='"+startTime+"' and xnc_order.departuredate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.departuredate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.departuredate <='"+endTime+"' ";
		}
		
		if(!companyId.equals("")){
			createDateSQL = createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"' ";
		}
		
		String Sql =" SELECT  province , SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " +productSQL+ userCLidSQL+ createDateSQL + " and LENGTH(province)>0  GROUP BY  province  ORDER BY Price   desc   LIMIT 0,10" ;
		System.out.println("#"+Sql);
		List<OrderEntity>  list = commonService.queryListByJdbc(Sql,OrderEntity.class);	
		
		return list;

	}
	/**
	 * 款项明细
	 * */
	public List<Map<String, Object>> getPaymentDetails(String state, String startTime, String endTime){
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = null;
		StringBuffer sql = new StringBuffer();
		String stateWhere = "";
		sql.append("select");
		if(state.equals("2")){
			sql.append(" IFNULL(SUM(a.alreadyPaidMoney), 0) value ");
			stateWhere = " AND a.state = " + state + " ";
		}else if(state.equals("1,2,3")){
			sql.append(" IFNULL(SUM(a.payableMoney), 0) value ");
			stateWhere = " AND a.state in (" + state + ") ";
		}else if(state.equals("1,3")){
			sql.append(" IFNULL(SUM(a.obligationsMoney), 0) value ");
			stateWhere = " AND a.state in (" + state + ") ";
		}
		sql.append(" FROM xnc_pay_bills_order a ");
		sql.append(" WHERE ");
		sql.append(" a.company = '" + companyId + "' ");
		sql.append(stateWhere);
		if(!startTime.equals("") && !endTime.equals("")){
			sql.append(" AND a.createDate >='" + startTime + "' AND a.createDate<='" + endTime + "'");
		}
		System.out.println(sql);
		map = commonService.findOneForJdbc(sql.toString(), null);
		if(map != null){
			map.put("value", map.get("value"));
		}
		mapList.add(map);
		return mapList;
	}
	/**
	 * 预存款
	 * */
	public List<Map<String, Object>> getPreDeposit(){
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = null;
		String sql = "SELECT SUM(b.useMoney) value FROM t_s_user a,xnc_user_salesman b  WHERE a.company = '" + companyId + "' AND a.tsuserSalesman = b.id";
		map = commonService.findOneForJdbc(sql, null);
		if(map != null){
			map.put("value", map.get("value"));
		}
		mapList.add(map);
		return mapList;
	}
}
