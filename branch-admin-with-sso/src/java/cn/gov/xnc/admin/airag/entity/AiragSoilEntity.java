package cn.gov.xnc.admin.airag.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 大气候土壤数据
 * @author zero
 * @date 2017-06-12 16:37:56
 * @version V1.0   
 *
 */
@Entity
@Table(name = "airag_soil_data", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class AiragSoilEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**aeid*/
	private java.lang.String aeid;
	/**土壤传感器id*/
	private java.lang.String sensorId;
	/**采集时间*/
	private java.lang.Long collectTime;
	/**collect_datetime*/
	private Date collectDatetime;
	/**soilTem*/
	private BigDecimal soilTem;
	/**soilTemThreshold*/
	private java.lang.String soilTemThreshold;
	/**soilHum*/
	private BigDecimal soilHum;
	/**soilHumThreshold*/
	private java.lang.String soilHumThreshold;
	/**gpsX*/
	private java.lang.String gpsX;
	/**gpsY*/
	private java.lang.String gpsY;
	/**sensor*/
	private java.lang.String sensor;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  aeid
	 */
	@Column(name ="AEID",nullable=false,length=32)
	public java.lang.String getAeid(){
		return this.aeid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  aeid
	 */
	public void setAeid(java.lang.String aeid){
		this.aeid = aeid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  土壤传感器id
	 */
	@Column(name ="SENSOR_ID",nullable=false,length=32)
	public java.lang.String getSensorId(){
		return this.sensorId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  土壤传感器id
	 */
	public void setSensorId(java.lang.String sensorId){
		this.sensorId = sensorId;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  采集时间
	 */
	@Column(name ="COLLECT_TIME",nullable=false,precision=19,scale=0)
	public java.lang.Long getCollectTime(){
		return this.collectTime;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  采集时间
	 */
	public void setCollectTime(java.lang.Long collectTime){
		this.collectTime = collectTime;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  soilTem
	 */
	@Column(name ="SOIL_TEM",nullable=true,precision=10,scale=0)
	public BigDecimal getSoilTem(){
		return this.soilTem;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  soilTem
	 */
	public void setSoilTem(BigDecimal soilTem){
		this.soilTem = soilTem;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  soilTemThreshold
	 */
	@Column(name ="SOIL_TEM_THRESHOLD",nullable=true,length=255)
	public java.lang.String getSoilTemThreshold(){
		return this.soilTemThreshold;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  soilTemThreshold
	 */
	public void setSoilTemThreshold(java.lang.String soilTemThreshold){
		this.soilTemThreshold = soilTemThreshold;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  soilHum
	 */
	@Column(name ="SOIL_HUM",nullable=true,precision=10,scale=0)
	public BigDecimal getSoilHum(){
		return this.soilHum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  soilHum
	 */
	public void setSoilHum(BigDecimal soilHum){
		this.soilHum = soilHum;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  soilHumThreshold
	 */
	@Column(name ="SOIL_HUM_THRESHOLD",nullable=true,length=255)
	public java.lang.String getSoilHumThreshold(){
		return this.soilHumThreshold;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  soilHumThreshold
	 */
	public void setSoilHumThreshold(java.lang.String soilHumThreshold){
		this.soilHumThreshold = soilHumThreshold;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  gpsX
	 */
	@Column(name ="GPS_X",nullable=true,length=32)
	public java.lang.String getGpsX(){
		return this.gpsX;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  gpsX
	 */
	public void setGpsX(java.lang.String gpsX){
		this.gpsX = gpsX;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  gpsY
	 */
	@Column(name ="GPS_Y",nullable=true,length=32)
	public java.lang.String getGpsY(){
		return this.gpsY;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  gpsY
	 */
	public void setGpsY(java.lang.String gpsY){
		this.gpsY = gpsY;
	}
	/**
	 *方法: 取得java.lang.Object
	 *@return: java.lang.Object  sensor
	 */
	@Column(name ="SENSOR",nullable=true,length=65535)
	public java.lang.String getSensor(){
		return this.sensor;
	}

	/**
	 *方法: 设置java.lang.Object
	 *@param: java.lang.Object  sensor
	 */
	public void setSensor(java.lang.String sensor){
		this.sensor = sensor;
	}
	
	/**
	 * @return the collectDatetime
	 */
	@Column(name ="COLLECT_DATETIME",nullable=true)
	public Date getCollectDatetime() {
		return collectDatetime;
	}

	/**
	 * @param collectDatetime the collectDatetime to set
	 */
	public void setCollectDatetime(Date collectDatetime) {
		this.collectDatetime = collectDatetime;
	}
}
