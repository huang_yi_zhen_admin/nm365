package cn.gov.xnc.admin.airag.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 大气候图片数据
 * @author zero
 * @date 2017-06-12 16:39:32
 * @version V1.0   
 *
 */
@Entity
@Table(name = "airag_pictrue_data", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class AiragPictrueEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**aeid*/
	private java.lang.String aeid;
	/**collectTime*/
	private java.lang.Long collectTime;
	
	/**collect_datetime*/
	private Date collectDatetime;
	
	/**图片压缩率*/
	private java.lang.Integer ratio;
	/**图片1*/
	private java.lang.String pic1;
	/**图片2*/
	private java.lang.String pic2;
	/**图片3*/
	private java.lang.String pic3;
	/**图片4*/
	private java.lang.String pic4;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  aeid
	 */
	@Column(name ="AEID",nullable=false,length=32)
	public java.lang.String getAeid(){
		return this.aeid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  aeid
	 */
	public void setAeid(java.lang.String aeid){
		this.aeid = aeid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  collectTime
	 */
	@Column(name ="COLLECT_TIME",nullable=false,precision=19,scale=0)
	public java.lang.Long getCollectTime(){
		return this.collectTime;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  collectTime
	 */
	public void setCollectTime(java.lang.Long collectTime){
		this.collectTime = collectTime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  图片压缩率
	 */
	@Column(name ="RATIO",nullable=true,precision=10,scale=0)
	public java.lang.Integer getRatio(){
		return this.ratio;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  图片压缩率
	 */
	public void setRatio(java.lang.Integer ratio){
		this.ratio = ratio;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  图片1
	 */
	@Column(name ="PIC1",nullable=true,length=255)
	public java.lang.String getPic1(){
		return this.pic1;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  图片1
	 */
	public void setPic1(java.lang.String pic1){
		this.pic1 = pic1;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  图片2
	 */
	@Column(name ="PIC2",nullable=true,length=255)
	public java.lang.String getPic2(){
		return this.pic2;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  图片2
	 */
	public void setPic2(java.lang.String pic2){
		this.pic2 = pic2;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  图片3
	 */
	@Column(name ="PIC3",nullable=true,length=255)
	public java.lang.String getPic3(){
		return this.pic3;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  图片3
	 */
	public void setPic3(java.lang.String pic3){
		this.pic3 = pic3;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  图片4
	 */
	@Column(name ="PIC4",nullable=true,length=255)
	public java.lang.String getPic4(){
		return this.pic4;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  图片4
	 */
	public void setPic4(java.lang.String pic4){
		this.pic4 = pic4;
	}

	/**
	 * @return the collectDatetime
	 */
	@Column(name ="COLLECT_DATETIME",nullable=true)
	public Date getCollectDatetime() {
		return collectDatetime;
	}

	/**
	 * @param collectDatetime the collectDatetime to set
	 */
	public void setCollectDatetime(Date collectDatetime) {
		this.collectDatetime = collectDatetime;
	}
}
