package cn.gov.xnc.admin.airag.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 大气候天气数据
 * @author zero
 * @date 2017-06-12 16:35:17
 * @version V1.0   
 *
 */
@Entity
@Table(name = "airag_weather_data", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class AiragWeatherEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**农眼id*/
	private java.lang.String aeid;
	/**采集时间*/
	private java.lang.Long collectTime;
	/**collect_datetime*/
	private Date collectDatetime;
	/**雨量*/
	private BigDecimal rainfall;
	/**空气温度*/
	private BigDecimal airTem;
	/**airTemThreshold*/
	private java.lang.String airTemThreshold;
	/**空气湿度*/
	private BigDecimal airHum;
	/**airHumThreshold*/
	private java.lang.String airHumThreshold;
	/**光照*/
	private BigDecimal lightIntensity;
	/**气压*/
	private BigDecimal airPressure;
	/**风向值*/
	private java.lang.String windDirection;
	/**风向描述*/
	private java.lang.String windDirectionDesc;
	/**风速*/
	private java.lang.String windSpeed;
	/**风力等级*/
	private java.lang.String windScale;
	/**最大风速*/
	private java.lang.String maxWindSpeed;
	/**露点温度*/
	private java.lang.String dewPointTem;
	/**gpsxReal*/
	private java.lang.String gpsxReal;
	/**gpsyReal*/
	private java.lang.String gpsyReal;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  农眼id
	 */
	@Column(name ="AEID",nullable=false,length=32)
	public java.lang.String getAeid(){
		return this.aeid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  农眼id
	 */
	public void setAeid(java.lang.String aeid){
		this.aeid = aeid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  采集时间
	 */
	@Column(name ="COLLECT_TIME",nullable=true,precision=19,scale=0)
	public java.lang.Long getCollectTime(){
		return this.collectTime;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  采集时间
	 */
	public void setCollectTime(java.lang.Long collectTime){
		this.collectTime = collectTime;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  雨量
	 */
	@Column(name ="RAINFALL",nullable=true,precision=10,scale=0)
	public BigDecimal getRainfall(){
		return this.rainfall;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  雨量
	 */
	public void setRainfall(BigDecimal rainfall){
		this.rainfall = rainfall;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  空气温度
	 */
	@Column(name ="AIR_TEM",nullable=true,precision=10,scale=0)
	public BigDecimal getAirTem(){
		return this.airTem;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  空气温度
	 */
	public void setAirTem(BigDecimal airTem){
		this.airTem = airTem;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  airTemThreshold
	 */
	@Column(name ="AIR_TEM_THRESHOLD",nullable=true,length=255)
	public java.lang.String getAirTemThreshold(){
		return this.airTemThreshold;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  airTemThreshold
	 */
	public void setAirTemThreshold(java.lang.String airTemThreshold){
		this.airTemThreshold = airTemThreshold;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  空气湿度
	 */
	@Column(name ="AIR_HUM",nullable=true,precision=10,scale=0)
	public BigDecimal getAirHum(){
		return this.airHum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  空气湿度
	 */
	public void setAirHum(BigDecimal airHum){
		this.airHum = airHum;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  airHumThreshold
	 */
	@Column(name ="AIR_HUM_THRESHOLD",nullable=true,length=255)
	public java.lang.String getAirHumThreshold(){
		return this.airHumThreshold;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  airHumThreshold
	 */
	public void setAirHumThreshold(java.lang.String airHumThreshold){
		this.airHumThreshold = airHumThreshold;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  光照
	 */
	@Column(name ="LIGHT_INTENSITY",nullable=true,precision=10,scale=0)
	public BigDecimal getLightIntensity(){
		return this.lightIntensity;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  光照
	 */
	public void setLightIntensity(BigDecimal lightIntensity){
		this.lightIntensity = lightIntensity;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  气压
	 */
	@Column(name ="AIR_PRESSURE",nullable=true,precision=10,scale=0)
	public BigDecimal getAirPressure(){
		return this.airPressure;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  气压
	 */
	public void setAirPressure(BigDecimal airPressure){
		this.airPressure = airPressure;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  风向值
	 */
	@Column(name ="WIND_DIRECTION",nullable=true,length=32)
	public java.lang.String getWindDirection(){
		return this.windDirection;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  风向值
	 */
	public void setWindDirection(java.lang.String windDirection){
		this.windDirection = windDirection;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  风向描述
	 */
	@Column(name ="WIND_DIRECTION_DESC",nullable=true,length=32)
	public java.lang.String getWindDirectionDesc(){
		return this.windDirectionDesc;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  风向描述
	 */
	public void setWindDirectionDesc(java.lang.String windDirectionDesc){
		this.windDirectionDesc = windDirectionDesc;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  风速
	 */
	@Column(name ="WIND_SPEED",nullable=true,length=32)
	public java.lang.String getWindSpeed(){
		return this.windSpeed;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  风速
	 */
	public void setWindSpeed(java.lang.String windSpeed){
		this.windSpeed = windSpeed;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  风力等级
	 */
	@Column(name ="WIND_SCALE",nullable=true,length=32)
	public java.lang.String getWindScale(){
		return this.windScale;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  风力等级
	 */
	public void setWindScale(java.lang.String windScale){
		this.windScale = windScale;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  最大风速
	 */
	@Column(name ="MAX_WIND_SPEED",nullable=true,length=32)
	public java.lang.String getMaxWindSpeed(){
		return this.maxWindSpeed;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  最大风速
	 */
	public void setMaxWindSpeed(java.lang.String maxWindSpeed){
		this.maxWindSpeed = maxWindSpeed;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  露点温度
	 */
	@Column(name ="DEW_POINT_TEM",nullable=true,length=32)
	public java.lang.String getDewPointTem(){
		return this.dewPointTem;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  露点温度
	 */
	public void setDewPointTem(java.lang.String dewPointTem){
		this.dewPointTem = dewPointTem;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  gpsxReal
	 */
	@Column(name ="GPSX_REAL",nullable=true,length=32)
	public java.lang.String getGpsxReal(){
		return this.gpsxReal;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  gpsxReal
	 */
	public void setGpsxReal(java.lang.String gpsxReal){
		this.gpsxReal = gpsxReal;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  gpsyReal
	 */
	@Column(name ="GPSY_REAL",nullable=true,length=32)
	public java.lang.String getGpsyReal(){
		return this.gpsyReal;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  gpsyReal
	 */
	public void setGpsyReal(java.lang.String gpsyReal){
		this.gpsyReal = gpsyReal;
	}
	
	/**
	 * @return the collectDatetime
	 */
	@Column(name ="COLLECT_DATETIME",nullable=true)
	public Date getCollectDatetime() {
		return collectDatetime;
	}

	/**
	 * @param collectDatetime the collectDatetime to set
	 */
	public void setCollectDatetime(Date collectDatetime) {
		this.collectDatetime = collectDatetime;
	}
}
