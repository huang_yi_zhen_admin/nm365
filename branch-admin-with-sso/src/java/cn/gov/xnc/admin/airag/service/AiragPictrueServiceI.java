package cn.gov.xnc.admin.airag.service;

import java.util.List;

import cn.gov.xnc.admin.airag.entity.AiragPictrueEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface AiragPictrueServiceI extends CommonService{
	
	
	public void savePictrueList(List<AiragPictrueEntity> list, QuartzConfigAiragEntity config);

}
