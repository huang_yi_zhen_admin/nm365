package cn.gov.xnc.admin.airag.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.admin.airag.service.QuartzConfigAiragServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("quartzConfigAiragService")
@Transactional
public class QuartzConfigAiragServiceImpl extends CommonServiceImpl implements QuartzConfigAiragServiceI {
	
	public List<QuartzConfigAiragEntity> getJobList(){

		CriteriaQuery cq = new CriteriaQuery(QuartzConfigAiragEntity.class);
		
		
		List<QuartzConfigAiragEntity> list = getListByCriteriaQuery(cq, false);
		
		for( QuartzConfigAiragEntity q : list ){
			
			//System.out.println(q.getInterfaceUrl());
			
		}
		
		
		return list;
	}
	
}