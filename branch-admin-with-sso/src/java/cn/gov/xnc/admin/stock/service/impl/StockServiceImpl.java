package cn.gov.xnc.admin.stock.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.StringUtil;

@Service("stockService")
@Transactional
public class StockServiceImpl extends CommonServiceImpl implements StockServiceI {

	@Override
	public List<StockEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		String companyid = (String) params.get("companyid");
		if(StringUtil.isNotEmpty(companyid)){
			cq.eq("companyid.id", companyid);
		}
		String status = (String) params.get("status");
		if(StringUtil.isNotEmpty(status)){
			cq.notEq("status", ConvertTool.toInt(status));
		}
		String dutypersonid = (String) params.get("dutypersonid");
		if(StringUtil.isNotEmpty(dutypersonid)){
			cq.eq("dutyperson.id", dutypersonid);
		}
		//...
		cq.add();
		List<StockEntity> stockList = this.getListByCriteriaQuery(cq,false);
		return stockList;
	}
	
	public void saveWithSegment( StockEntity stock ){
		save(stock);
		
		//新增仓库区域表的节点位置
		StockSegmentEntity seg = new StockSegmentEntity();
		seg.setId(IdWorker.generateSequenceNo());
		seg.setName(stock.getStockname());
		seg.setFullpathname(stock.getStockname() );
		seg.setStockid(stock);
		save(seg);
		
	}
}