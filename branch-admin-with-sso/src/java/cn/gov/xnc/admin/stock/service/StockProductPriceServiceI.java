package cn.gov.xnc.admin.stock.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockProductPriceServiceI extends CommonService{
	
	public StockProductPriceEntity updateStockProductPrice( ProductEntity product, StockEntity stock,HttpServletRequest request);
	
	public AjaxJson savePrice(String stockProductId,String stockProductPriceId,BigDecimal avgprice ,HttpServletRequest request);
	
	/**
	 * 获取商品的库存状况
	 * @param productid
	 * @return
	 * @throws Exception
	 */
	public StockProductPriceEntity getStockProductPriceInfo( String productid) throws Exception;
	
	public List<StockProductPriceEntity> findListByParams(Map<String, Object> params);
	
	public Map<String, Object> getParams(ProductEntity product, StockEntity stock);
}
