package cn.gov.xnc.admin.stock.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.RichTextString;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockIOTypeServiceI;

/**   
 * @Title: Controller
 * @Description: 进销存出入库类型：I 入库 O 出库
 * @author zero
 * @date 2017-01-23 15:02:12
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockIOTypeController")
public class StockIOTypeController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockIOTypeController.class);

	@Autowired
	private StockIOTypeServiceI stockTypeService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private DictionaryService dictionaryService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 进销存出入库类型：I 入库 O 出库列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView companyStockType(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockIOTypeList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	/*public void datagrid(StockIOTypeEntity companyStockType,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockIOTypeEntity.class, dataGrid);
		cq.eq("companyid", user.getCompany());
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyStockType, request.getParameterMap());
		this.stockTypeService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}*/
	public void datagrid(TSDictionary dict, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(TSDictionary.class, dataGrid);
		cq.eq("company", user.getCompany());
		if(StringUtil.isNotEmpty(dict.getDictionaryName())){
			cq.like("dictionaryName", "%"+ dict.getDictionaryName() + "%");
			dict.setDictionaryName(null);
		}
		cq.add(Restrictions.or(Restrictions.eq("dictionaryType", StockIOType.IN), Restrictions.eq("dictionaryType",  StockIOType.OUT)));
		cq.eq("isdel", "N");
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, dict, request.getParameterMap());
		this.stockTypeService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除进销存出入库类型：I 入库 O 出库
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	/*public AjaxJson del(StockIOTypeEntity companyStockType, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyStockType = systemService.getEntity(StockIOTypeEntity.class, companyStockType.getId());
		message = "进销存出入库类型：I 入库 O 出库删除成功";
		stockTypeService.delete(companyStockType);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}*/
	public AjaxJson del(TSDictionary dict, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		dict = systemService.getEntity(TSDictionary.class, dict.getId());
		message = "进销存出入库类型：DIC_TYPE_STOCK_IN 入库 DIC_TYPE_STOCK_OUT 出库删除成功";
		dict.setIsdel("Y");
		stockTypeService.updateEntitie(dict);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加进销存出入库类型：I 入库 O 出库
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	/*public AjaxJson save(StockIOTypeEntity companyStockType, HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(companyStockType.getId())) {
			message = "进销存出入库类型更新成功";
			StockIOTypeEntity t = stockTypeService.get(StockIOTypeEntity.class, companyStockType.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(companyStockType, t);
				stockTypeService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "进销存出入库类型：I 入库 O 出库更新失败";
			}
		} else {
			message = "进销存出入库类型添加成功";
			
			companyStockType.setCompanyid(user.getCompany());
			companyStockType.setId(IdWorker.generateSequenceNo());
			stockTypeService.save(companyStockType);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}*/
	public AjaxJson save(TSDictionary dict, HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(dict.getId())) {
			message = "进销存出入库类型更新成功";
			TSDictionary t = stockTypeService.get(TSDictionary.class, dict.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(dict, t);
				stockTypeService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "进销存出入库类型：I 入库 O 出库更新失败";
			}
		} else {
			message = "进销存出入库类型添加成功";
			
			dict.setCompany(user.getCompany());
			dict.setDictionaryValue(IdWorker.generateSequenceNo());
			dict.setIsdel("N");
			stockTypeService.save(dict);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 进销存出入库类型：I 入库 O 出库列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	/*public ModelAndView addorupdate(StockIOTypeEntity companyStockType, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(companyStockType.getId())) {
			companyStockType = stockTypeService.getEntity(StockIOTypeEntity.class, companyStockType.getId());
			req.setAttribute("stockIOTypePage", companyStockType);
		}
		return new ModelAndView("admin/stock/stockIOType");
	}*/
	public ModelAndView addorupdate(TSDictionary dict, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(dict.getId())) {
			dict = stockTypeService.getEntity(TSDictionary.class, dict.getId());
			req.setAttribute("dictPage", dict);
		}
		return new ModelAndView("admin/stock/stockIOType");
	}
}
