package cn.gov.xnc.admin.stock.controller;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserRoleService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.product.entity.ProductClassifyEntity;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;

/**   
 * @Title: Controller
 * @Description: 库存相关联商品表
 * @author zero
 * @date 2017-03-22 00:52:46
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockProductController")
public class StockProductController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockProductController.class);

	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserRoleService userRoleService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 盘点仓库商品选择
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "stockCheckSelectProduct")
	public ModelAndView stockCheckSelectProduct(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		String stockid = request.getParameter("stockid");
		request.setAttribute("stockid", stockid);
		
		//获取公司商品品牌列表
		CriteriaQuery cb = new CriteriaQuery(ProductBrandEntity.class);
		cb.eq("company", user.getCompany());
		cb.add();
		List<ProductBrandEntity> brandlist = systemService.getListByCriteriaQuery(cb, false);
		request.setAttribute("brandlist", brandlist);
				
		return new ModelAndView("admin/stock/stockCheckSelectProduct");
	}
	
	/**
	 * 库存相关联商品表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockProduct(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		//根据用户权限获取相关权限
		TSRole role = userRoleService.getRoleByUserRole(user);
		
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		//如果不是管理员类型，添加仓库负责人条件
		if(!role.getRoleCode().equals("admin")){
			cq.eq("dutyperson.id", user.getId());
		}
		cq.notEq("status", 1);
		cq.add();
		
		List<StockEntity> stocklist = systemService.getListByCriteriaQuery(cq, false);
		request.setAttribute("stocklist", stocklist);
		
		CriteriaQuery cc = new CriteriaQuery(ProductClassifyEntity.class);
		cc.eq("company", user.getCompany());
		cc.add();
		List<ProductClassifyEntity> classifylist = systemService.getListByCriteriaQuery(cc, false);
		request.setAttribute("classifyList", classifylist);
		
		return new ModelAndView("admin/stock/stockProductInfoList");
	}
	
	/**
	 * 库存相关联商品表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "detaillist")
	public ModelAndView detaillist(String id,HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		String[] tmp = id.split("-");
		String productid = tmp[0];
		String stockid = tmp[1];
		String stockSegmentId = tmp[2];
		
		request.setAttribute("stockid", stockid);
		request.setAttribute("productid", productid);
		//CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		//cq.eq("companyid", user.getCompany());
		//cq.add();
		
		//List<StockEntity> stocklist = systemService.getListByCriteriaQuery(cq, false);
		//request.setAttribute("stocklist", stocklist);
		
		return new ModelAndView("admin/stock/stockProductDetailInfoList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockProductEntity stockProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class, dataGrid);
		cq.eq("companyid.id", user.getCompany().getId());
		if(null != stockProduct.getStockid() && StringUtil.isNotEmpty(stockProduct.getStockid().getId())){
			cq.eq("stockid.id", stockProduct.getStockid().getId());
		}
		
		if( null != stockProduct.getProductid() && StringUtil.isNotEmpty(stockProduct.getProductid().getId())){
			cq.eq("productid.id", stockProduct.getProductid().getId());
		}
		
		if( null!= stockProduct.getProductid() && StringUtil.isNotEmpty(stockProduct.getProductid().getName() )){
			cq.like("productid.name", "%" + stockProduct.getProductid().getName() + "%");
			stockProduct.getProductid().setName(null);
			
		}
		
		cq.createAlias("stockSegment", "seg");
		cq.add(Restrictions.eq("seg.status",0));
		
		cq.addOrder("update_time", SortDirection.desc);
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockProduct, request.getParameterMap());
		this.stockProductService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(value = "detaildatagrid")
	public void detaildatagrid(StockProductEntity stockProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		TSUser user = ResourceUtil.getSessionUserName();

		CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class, dataGrid);
		cq.eq("companyid", user.getCompany());
		
		if( null != stockProduct.getProductid()){
			stockProduct.getProductid().setIsDelete(null);
			stockProduct.getProductid().setNotsale(null);
		}
		
		if( null != stockProduct.getProductid() && StringUtil.isNotEmpty( stockProduct.getProductid().getName()) ){
			cq.createAlias("productid", "product");
			cq.add(Restrictions.like("product.name", "%"+stockProduct.getProductid().getName()+"%"));
			stockProduct.getProductid().setName(null);
		}
		cq.add();
		//查询条件组装器
		HqlGenerateUtil.installHql(cq, stockProduct, request.getParameterMap());
		this.stockProductService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 设置预警值
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "stockalert")
	public ModelAndView stockalert(String id,HttpServletRequest request) {
		StockProductEntity stockProduct = stockProductService.getEntity(StockProductEntity.class, id);
		request.setAttribute("stockProduct", stockProduct);
		return new ModelAndView("admin/stock/stockalert");
	}
	
	@RequestMapping(value = "saveAlert")
	@ResponseBody
	public AjaxJson saveAlert(StockProductEntity stockProduct, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		StringBuffer msg = new StringBuffer();
		BigDecimal alertnum = stockProduct.getAlertnum();
		stockProduct = stockProductService.getEntity(StockProductEntity.class, stockProduct.getId());
		stockProduct.setAlertnum(alertnum);
		systemService.addLog(msg.toString(), Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		result.setSuccess(true);
		result.setMsg(msg.toString());
		
		return result;
	}
	
	/**
	 * 库存状况统计
	 * @param stockProduct
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @return
	 */
	@RequestMapping(value = "datagridStatis")
	@ResponseBody
	public AjaxJson datagridStatis( StockProductEntity stockProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		AjaxJson j = new AjaxJson();
		j.setSuccess(false);
		j.setMsg("查询失败");
		
		StringBuffer sql = new StringBuffer();
		
		sql.append(" select sum(stocknum) as stocknum, sum(totalprice) as totalprice from ( ");
		sql.append(getDatagridSql(stockProduct));
		sql.append( " ) as tb ");
		
		if( StringUtil.isNotEmpty(stockProduct.getStatus()) ){
			sql.append(" where status like '%").append(stockProduct.getStatus()).append("%' ");
		}
		
		List<StockProductEntity> list = stockProductService.queryListByJdbc(sql.toString(),StockProductEntity.class);
		if( list!=null && list.size() > 0 ){
			StockProductEntity sp = list.get(0);
			Map<String,Object> map = new HashMap<>();
			map.put("stocknum", sp.getStocknum());
			map.put("totalprice", sp.getTotalprice());
			
			j.setObj(map);
			j.setSuccess(true);
			j.setMsg("查询成功");
		}
		
		return j;
	}
	
	/**
	 * 库存合计页面
	 * @return
	 */
	@RequestMapping(value = "stockStatistics")
	public ModelAndView stockStatistics(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockStatistics");
	}
	
	/**
	 * easyui AJAX请求数据
	 * 库存合计功能，列表加载
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(value = "statisticsDatagrid")
	public void statisticsDatagrid(StockProductEntity stockProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		//根据用户权限获取相关权限
		TSRole role = userRoleService.getRoleByUserRole(user);
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("select IFNULL(bb.totalprice,0) totalprice,k.id id from xnc_company_stock k LEFT JOIN (");
		sql.append("select  SUM(t.totalprice) totalprice, t.stockid from xnc_company_stock_product t ");
		sql.append(" where t.companyid = '").append(user.getCompany().getId()).append("' ");
		sql.append(" GROUP BY t.stockid) bb on k.id = bb.stockid ");
		sql.append(" where k.companyid = '").append(user.getCompany().getId()).append("' ");;
		
		//如果不是管理员类型，添加仓库负责人条件
		if(!role.getRoleCode().equals("admin")){
			sql.append(" and k.dutyperson = '").append(user.getId()).append("' ");
		}
		
		List<StockProductEntity> list = stockProductService.queryListByJdbc(sql.toString(),StockProductEntity.class);
		for( int i = 0 ; i < list.size(); i++ ){
			StockProductEntity stockproduct = list.get(i);
			String id = stockproduct.getId();
			StockEntity stock = stockProductService.getEntity(StockEntity.class, id);
			stockproduct.setStockid(stock);
		}
		
		dataGrid.setResults(list);
		dataGrid.setTotal(list.size());
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除库存相关联商品表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockProductEntity stockProduct, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockProduct = systemService.getEntity(StockProductEntity.class, stockProduct.getId());
		message = "库存相关联商品表删除成功";
		stockProductService.delete(stockProduct);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加库存相关联商品表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockProductEntity stockProduct, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(stockProduct.getId())) {
			message = "库存相关联商品表更新成功";
			StockProductEntity t = stockProductService.get(StockProductEntity.class, stockProduct.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockProduct, t);
				stockProductService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "库存相关联商品表更新失败";
			}
		} else {
			message = "库存相关联商品表添加成功";
			stockProductService.save(stockProduct);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 库存相关联商品表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockProductEntity stockProduct, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockProduct.getId())) {
			stockProduct = stockProductService.getEntity(StockProductEntity.class, stockProduct.getId());
			req.setAttribute("stockProductPage", stockProduct);
		}
		return new ModelAndView("admin/stock/stockProduct");
	}
	
	private String getDatagridSql( StockProductEntity stockProduct ){
		
		TSUser user = ResourceUtil.getSessionUserName();
		//根据用户权限获取相关权限
		TSRole role = userRoleService.getRoleByUserRole(user);
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("select CONCAT(a.productid,'-',a.stockid,'-',a.stockSegmentId) as id, SUM(a.stocknum) as stocknum, SUM(a.totalprice) as totalprice, (SUM(a.totalprice)/SUM(a.stocknum)) as averageprice ,group_concat(a.`status`) as status ");
		sql.append(" from  xnc_company_stock_product a INNER JOIN xnc_product b where a.productid=b.id ");
		
		if( null != stockProduct.getProductid() && StringUtil.isNotEmpty( stockProduct.getProductid().getName()) ){
			sql.append(" and b.name like '%").append(stockProduct.getProductid().getName()).append("%' ");
		}
		
		sql.append(" and a.companyid='").append(user.getCompany().getId()).append("' ");
		//如果不是管理员类型，添加仓库负责人条件
		if(!role.getRoleCode().equals("admin")){
			sql.append(" and a.stockid in (select t.id from xnc_company_stock t where t.dutyperson = '").append(user.getId()).append("') ");
		}
		
		if( null != stockProduct.getStockid() && StringUtil.isNotEmpty(stockProduct.getStockid().getId())){
			sql.append(" and a.stockid='").append(stockProduct.getStockid().getId()).append("' ");
		}
		
		
		//过滤不显示库存为0且一个月内没有补充库存的商品
		int expireday = 30;
		sql.append(" and (a.stocknum > 0 or ( a.stocknum = 0 and datediff(curdate(), date(a.updateTime)) < ").append(expireday).append(")) ");
		
		sql.append(" GROUP BY a.productid,a.stockid ORDER BY a.updateTime DESC ");
		
		return sql.toString();
	}
}
