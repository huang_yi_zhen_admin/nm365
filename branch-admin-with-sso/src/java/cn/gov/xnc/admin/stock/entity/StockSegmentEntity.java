package cn.gov.xnc.admin.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 仓库分区表
 * @author zero
 * @date 2017-04-20 14:38:39
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_segment", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockSegmentEntity implements java.io.Serializable {
	/**仓储区域或者货柜编码*/
	private java.lang.String id;
	/**仓库分区名称或者是货柜名称*/
	private java.lang.String name;
	/**上级仓储区域或者货柜编码*/
	private java.lang.String preid;
	
	/**所属仓库编码（仓库最高级别节点）*/
	private StockEntity stockid;
	/**备注*/
	private java.lang.String remarks;
	/**updateTime*/
	private java.util.Date updateTime;
	
	/**从跟节点到当前节点的完成路径，以->间隔*/
	private java.lang.String fullpathname;
	
	/**状态，0正常，1已删除*/
	private int status = 0;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓储区域或者货柜编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  仓储区域或者货柜编码
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓库分区名称或者是货柜名称
	 */
	@Column(name ="NAME",nullable=false,length=160)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  仓库分区名称或者是货柜名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得StockSegmentEntity
	 *@return: StockSegmentEntity  上级仓储区域或者货柜编码
	 */
	@Column(name ="PREID",nullable=false,length=32)
	public java.lang.String getPreid(){
		return this.preid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  上级仓储区域或者货柜编码
	 */
	public void setPreid(java.lang.String preid){
		this.preid = preid;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属仓库编码（仓库最高级别节点）
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKID")
	public StockEntity getStockid(){
		return this.stockid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属仓库编码（仓库最高级别节点）
	 */
	public void setStockid(StockEntity stockid){
		this.stockid = stockid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARKS",nullable=true,length=4000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  updateTime
	 */
	@Column(name ="UPDATE_TIME",nullable=false)
	public java.util.Date getUpdateTime(){
		return this.updateTime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime){
		this.updateTime = updateTime;
	}

	/**
	 * @return the fullpathname
	 */
	@Column(name ="FULLPATHNAME",nullable=false,length=4000)
	public java.lang.String getFullpathname() {
		return fullpathname;
	}

	/**
	 * @param fullpathname the fullpathname to set
	 */
	public void setFullpathname(java.lang.String fullpathname) {
		this.fullpathname = fullpathname;
	}

	/**
	 * @return the status
	 */
	@Column(name ="status",nullable=false)
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
}
