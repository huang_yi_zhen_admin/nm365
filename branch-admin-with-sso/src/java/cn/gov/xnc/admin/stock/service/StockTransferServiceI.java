package cn.gov.xnc.admin.stock.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.stock.entity.StockTransferDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockTransferEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockTransferServiceI extends CommonService{

	public AjaxJson saveStockTransfer(StockTransferEntity stockTransfer, HttpServletRequest req) throws Exception;
	
	public AjaxJson saveStockTransfer(StockTransferEntity stockTransfer, List<StockTransferDetailEntity> stockTransferDetailList, HttpServletRequest req) throws Exception;
}
