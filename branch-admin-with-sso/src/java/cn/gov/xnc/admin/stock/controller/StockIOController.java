package cn.gov.xnc.admin.stock.controller;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.entity.StockSupplierEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.admin.stock.service.StockIOServiceI;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 出入库表单，记录出入库总体信息
 * @author zero
 * @date 2017-01-23 15:03:19
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockIOController")
public class StockIOController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockIOController.class);

	@Autowired
	private StockIOServiceI stockIOService;
	@Autowired
	private StockIODetailServiceI stockIODetailService;
	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	/**
	 * 出入库明细
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockhistory")
	public ModelAndView companyStockOrderHistory(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockOrderHistory");
	}
	
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "historygrid")
	public void historygrid(StockIOEntity companyStockOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		String type= request.getParameter("type");
		CriteriaQuery cq = new CriteriaQuery(StockIOEntity.class, dataGrid);
		cq.createCriteria("stocktypeid", "stocktype");
		if(StockIOType.IN.equals(type)){
			cq.add(Restrictions.eq("stocktype.dictionaryType", StockIOType.IN));
		}else{
			cq.add(Restrictions.eq("stocktype.dictionaryType", StockIOType.OUT));
		}
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyStockOrder, request.getParameterMap());
		this.stockIOService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 出入库表单，记录出入库总体信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView companyStockOrder(HttpServletRequest request) {
		String type= request.getParameter("type");
		request.setAttribute("type", type);
		TSUser user = ResourceUtil.getSessionUserName();
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		request.setAttribute("stocklist", stockList);
		
		CriteriaQuery cquser = new CriteriaQuery(TSUser.class);
		cquser.eq("company", user.getCompany());
		cquser.in("type", "1,2".split(","));
		cquser.add();
		List<TSUser> operatorlist = systemService.getListByCriteriaQuery(cquser,false);
		request.setAttribute("operatorlist", operatorlist);
		
		if(StockIOType.IN.equals(type)){
			//获取所有进销存入库类型
			CriteriaQuery cq1 = new CriteriaQuery(TSDictionary.class);
			cq1.eq("dictionaryType", StockIOType.IN);
			cq1.eq("company.id", user.getCompany().getId());
			cq1.eq("isdel", "N");
			cq1.add();
			List<TSDictionary> stockTypeList = systemService.getListByCriteriaQuery(cq1, false);
			request.setAttribute("stocktypelist", stockTypeList);
			
			return new ModelAndView("admin/stock/stockInList");
		}else{
			//获取所有进销存出库类型
			CriteriaQuery cq1 = new CriteriaQuery(TSDictionary.class);
			cq1.eq("dictionaryType", StockIOType.OUT);
			cq1.eq("company.id", user.getCompany().getId());
			cq1.eq("isdel", "N");
			cq1.add();
			List<TSDictionary> stocktypelistOut = systemService.getListByCriteriaQuery(cq1, false);
			request.setAttribute("stocktypelist", stocktypelistOut);
			
			return new ModelAndView("admin/stock/stockOutList");
		}
		
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockIOEntity companyStockOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(StockIOEntity.class, dataGrid);
		cq.eq("companyid.id", user.getCompany().getId());
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createtime", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createtime", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createtime", DateUtils.str2Date(createdate2,sdf));
		}
		cq.addOrder("createtime", SortDirection.desc);
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyStockOrder, request.getParameterMap());
		this.stockIOService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除出入库表单，记录出入库总体信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockIOEntity companyStockOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyStockOrder = systemService.getEntity(StockIOEntity.class, companyStockOrder.getId());
		message = "出入库表单，记录出入库总体信息删除成功";
		stockIOService.delete(companyStockOrder);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}

	/**
	 * 入库-保存
	 * @param ids
	 * @return
	 */
//	@RequestMapping(value = "saveInStock")
//	@ResponseBody
//	public AjaxJson saveInStock(StockIOEntity companyStockOrder, HttpServletRequest request) {
//		return stockIOService.saveInStock(companyStockOrder, request, stockIODetailService, stockProductService, systemService);
//	}
	
	/**
	 * 出库-保存
	 * @param ids
	 * @return
	 */
//	@RequestMapping(value = "saveOutStock")
//	@ResponseBody
//	public AjaxJson saveOutStock(StockIOEntity companyStockOrder, HttpServletRequest request) {
//		return stockIOService.saveOutStock(companyStockOrder, request, stockIODetailService, stockProductService, systemService);
//	}

	@RequestMapping(value = "saveStockIO")
	@ResponseBody
	public AjaxJson saveStockIO(StockIOEntity companyStockOrder, HttpServletRequest request) {
		return stockIOService.saveStockIO(companyStockOrder, request);
	}
	
	/**
	 * 出入库表单，记录出入库总体信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "iolist")
	public ModelAndView iolist(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		String id= request.getParameter("id");
		String[] tmp = id.split("-");
		String productid = tmp[0];
		String stockid = tmp[1];
		String stockSegmentId = tmp[2];
		
		request.setAttribute("productid", productid);
		request.setAttribute("stockid", stockid);
		request.setAttribute("stockSegmentId", stockSegmentId);
		
		return new ModelAndView("admin/stock/stockIOList");
	}
	
	@RequestMapping(value = "productIODatagrid")
	public void productIODatagrid(String productid, String stockid,String stockSegmentId, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		ProductEntity product = systemService.getEntity(ProductEntity.class, productid);
		StockEntity stock = systemService.getEntity(StockEntity.class, stockid);
		StockSegmentEntity stockSegment = systemService.getEntity(StockSegmentEntity.class, stockSegmentId);
		
		Map<String,Object> stockIODetailParams = stockIODetailService.getParams(product, stock, stockSegment);
		List<StockIODetailEntity> stockIODetails = stockIODetailService.findListByParams(stockIODetailParams);
		
		List<StockIOEntity> list = new ArrayList<StockIOEntity>();
		if(stockIODetails != null && stockIODetails.size() > 0){
			for (StockIODetailEntity stockIODetailEntity : stockIODetails) {
				if(stockIODetailEntity.getStockorderid() != null){
					String id = stockIODetailEntity.getStockorderid().getId();
					StockIOEntity stockIOEntity = systemService.getEntity(StockIOEntity.class, id);
					list.add(stockIOEntity);
				}
			}
		}
		
		
		dataGrid.setResults(list);
		//查询条件组装器
//		HqlGenerateUtil.installHql(cq, request.getParameterMap());
//		this.stockIOService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);

	}
	
	/**
	 * 添加出入库表单，记录出入库总体信息
	 * 
	 * @param ids
	 * @return
	 */
	/*@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockIOEntity companyStockOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		String iotype = request.getParameter("type");
		if (StringUtil.isNotEmpty(companyStockOrder.getId())) {
			message = "出入库表单，记录出入库总体信息更新成功";
			StockIOEntity t = stockIOService.get(StockIOEntity.class, companyStockOrder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(companyStockOrder, t);
				stockIOService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				j.setSuccess(false);
				message = "出入库表单，记录出入库总体信息更新失败" + e;
			}
		} else {
			
			try {
				
				TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
				TSUser operator = systemService.findUniqueByProperty(TSUser.class, "id", companyStockOrder.getOperator().getId());
				StockEntity stock = systemService.findUniqueByProperty(StockEntity.class, "id", companyStockOrder.getStockid().getId());
				StockIOTypeEntity stockiotype = systemService.findUniqueByProperty(StockIOTypeEntity.class, "id", companyStockOrder.getStocktypeid().getId());
				//创建基础数据
				StockIOEntity stockIOEntity = new StockIOEntity();
				stockIOEntity.setIdentifier(IdWorker.generateSequenceNo());
				stockIOEntity.setCompanyid(company);
				stockIOEntity.setCreatetime(DateUtils.getDate());
				stockIOEntity.setOperator(operator);
				stockIOEntity.setRemark(companyStockOrder.getRemark());
				stockIOEntity.setStockid(stock);
				stockIOEntity.setStocktypeid(stockiotype);
				stockIOEntity.setStockIODetailList(null);
				
				stockIOService.save(stockIOEntity);
				
				//创建出入库详细数据
				List<StockIODetailEntity> stockIODetailList = stockIODetailService.buildStockInDetail(stockIOEntity, request);
				if(stockIODetailList.size() > 0){
					stockIOEntity.setStockIODetailList(stockIODetailList);
					stockIOService.updateEntitie(stockIOEntity);
					
					//更新或添加库存商品
					for (StockIODetailEntity stockIODetail : stockIODetailList) {
						try {
							
							ProductEntity product = systemService.findUniqueByProperty(ProductEntity.class, "id", stockIODetail.getProductid().getId());
							stockProductService.saveOrUpdateStockProduct(product, iotype, stock, stockIODetail.getIostocknum());
							
						} catch (Exception e) {
							systemService.addLog("更新或添加库存商品失败" + e, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
						}
						
					}
					
					message = "出入库表单添加成功";
				}else{
					j.setSuccess(false);
					message = "出入库表单添加失败，您没有操作任何商品！";
					stockIOService.delete(stockIOEntity);//回滚
					stockIOEntity.setStockIODetailList(null);
				}
				
			} catch (Exception e) {
				message = "出入库表单添加失败" + e;
				j.setSuccess(false);
				logger.error(message);
			}
			
		}
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		j.setMsg(message);
		return j;
	}*/

	/**
	 * 出入库表单，记录出入库总体信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockIOEntity companyStockOrder, HttpServletRequest req) {
		String type= req.getParameter("type");
		TSUser user = ResourceUtil.getSessionUserName();
		req.setAttribute("operater", user);
		
		if (StringUtil.isNotEmpty(companyStockOrder.getId())) {
			companyStockOrder = stockIOService.getEntity(StockIOEntity.class, companyStockOrder.getId());
			req.setAttribute("companyStockOrderPage", companyStockOrder);
		}
		
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		req.setAttribute("stocklist", stockList);
		
		if(StockIOType.IN.equals(type)){
			
			//读取供应商列表
			CriteriaQuery cr = new CriteriaQuery(StockSupplierEntity.class);
			cr.eq("company", user.getCompany());
			cr.eq("status", 0);
			cr.addOrder("code", SortDirection.asc);
			cr.add();
			List<StockSupplierEntity> supplierList = systemService.getListByCriteriaQuery(cr, false);
			req.setAttribute("supplierlist", supplierList);
			
			//获取所有进销存入库类型
			CriteriaQuery cq1 = new CriteriaQuery(TSDictionary.class);
			cq1.eq("dictionaryType", StockIOType.IN);
			cq1.eq("company", user.getCompany());
			cq1.eq("isdel", "N");
			cq1.add();
			List<TSDictionary> stockTypeList = systemService.getListByCriteriaQuery(cq1, false);
			req.setAttribute("stocktypelist", stockTypeList);
			
			return new ModelAndView("admin/stock/stockInAdd");
		}else{
			
			//获取所有进销存出库类型
			CriteriaQuery cq1 = new CriteriaQuery(TSDictionary.class);
			cq1.eq("dictionaryType", StockIOType.OUT);
			cq1.eq("company", user.getCompany());
			cq1.eq("isdel", "N");
			cq1.add();
			List<TSDictionary> stockTypeList = systemService.getListByCriteriaQuery(cq1, false);
			req.setAttribute("stocktypelist", stockTypeList);
			
			return new ModelAndView("admin/stock/stockOutAdd");
		}
		
		
	}
	
	/**
	 * 出入库表单，记录出入库总体信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiceStockOut")
	public ModelAndView choiceStockOut(StockIOEntity companyStockOrder, HttpServletRequest req) {
			String stockID = req.getParameter("stockID");
			req.setAttribute("stockID", stockID);
			return new ModelAndView("admin/stock/choiceStockOut");
		
	}
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagridchoice")
	public void datagridchoice(StockIOEntity companyStockOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		//查询转换入库的类型ID
		
		
		
		
		CriteriaQuery cq = new CriteriaQuery(StockIOEntity.class, dataGrid);
		cq.eq("companyid.id", user.getCompany().getId());
		
		
		cq.addOrder("createtime", SortDirection.desc);
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyStockOrder, request.getParameterMap());
		this.stockIOService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
}
