package cn.gov.xnc.admin.stock.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.service.StockTransferDetailServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("stockTransferDetailService")
@Transactional
public class StockTransferDetailServiceImpl extends CommonServiceImpl implements StockTransferDetailServiceI {
	
}