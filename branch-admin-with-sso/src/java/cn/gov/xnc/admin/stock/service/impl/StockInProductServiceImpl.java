package cn.gov.xnc.admin.stock.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.service.StockInProductServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("stockinProductService")
@Transactional
public class StockInProductServiceImpl extends CommonServiceImpl implements StockInProductServiceI {
	
}