package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.entity.StockSupplierEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("stockIODetailService")
@Transactional
public class StockIODetailServiceImpl extends CommonServiceImpl implements StockIODetailServiceI {
	@Autowired
	private StockProductServiceI stockProductServiceI;
	
	public StockIODetailEntity getLastStockInDetail(String productid){
		TSUser user = ResourceUtil.getSessionUserName();
				
		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class);
		cq.createAlias("stockorderid", "so");
		cq.add( Restrictions.eq("so.companyid", user.getCompany() ));
		cq.createAlias("productid", "p");
		cq.add( Restrictions.eq("p.id", productid ));
		
		cq.createAlias("stockorderid.stocktypeid", "type");
		cq.add( Restrictions.eq("type.dictionaryType", StockIOType.IN ));
		
		cq.addOrder("updatetime", SortDirection.desc);
		cq.setPageSize(1);
		cq.setCurPage(0);
		List<StockIODetailEntity> list = getListByCriteriaQuery(cq,true);
		if( null != list && list.size() > 0 ){
			return list.get(0);
		}
		
		return null;
	}
	

	@Override
	public Map<String, Object> statisCount(Map<String, Object> params) {
		
		StringBuffer sql = new StringBuffer();
		sql.append("select sum(t.totalprice) totalprice from ");
		sql.append(" xnc_company_stock_io_detail t ");
		sql.append(" inner join xnc_company_stock_io s on t.stockorderid = s.id ");
		sql.append(" inner join xnc_company_stock d on s.stockid = d.id ");
		//sql.append(" inner join xnc_company_stock_io_type b on s.stocktypeid = b.id ");
		sql.append(" inner join t_s_user u on u.id = s.operator ");
		sql.append(" inner join xnc_product p on t.productid = p.id ");
		if(params!=null){
			String companyid = (String) params.get("companyid");
			if(StringUtil.isNotEmpty(companyid)){
				sql.append(" where d.companyid = '").append(companyid).append("' ");
			}
			
			String roleCode = (String) params.get("roleCode");
			String userId = (String) params.get("userId");
			//如果不是管理员类型，添加仓库负责人条件
			if(!roleCode.equals("admin") && StringUtil.isNotEmpty(userId)){
				sql.append(" and dutyperson = '").append(userId).append("' ");
			}
			//出入库类型I/O
			String typeCode = (String) params.get("typeCode");
			if(StringUtil.isNotEmpty(typeCode)){
				sql.append(" and b.typecode = '").append(typeCode).append("' ");
			}
			//仓库
			String stockId = (String) params.get("stockId");
			if(StringUtil.isNotEmpty(stockId)){
				sql.append(" and d.id = '").append(stockId).append("' ");
			}
			//经办人
			String realname = (String) params.get("realname");
			if(StringUtil.isNotEmpty(realname)){
				sql.append(" and u.userName = '").append(realname).append("' ");
			}
			//
			String stockIOOrderId = (String) params.get("stockIOOrderId");
			if(StringUtil.isNotEmpty(stockIOOrderId)){
				sql.append(" and t.stockorderid ='").append(stockIOOrderId).append("' ");
			}
			//商品名称
			String productName = (String) params.get("productName");
			if(StringUtil.isNotEmpty(productName)){
				sql.append(" and p.name like '%").append(productName).append("%' ");
			}
			
			String startTime = (String) params.get("startTime");
			String endTime = (String) params.get("endTime");
			if(StringUtil.isNotEmpty(startTime)){
				sql.append("and t.updatetime >= '").append(startTime).append("' ");
			}
			if(StringUtil.isNotEmpty(endTime)){
				sql.append("and t.updatetime <= '").append(endTime).append("' ");
			}
		}
		
		
		List<StockIODetailEntity> list = this.queryListByJdbc(sql.toString(),StockIODetailEntity.class);
		Map<String,Object> result = null;
		if( list!=null && list.size() > 0 ){
			result = new HashMap<String,Object>();
			StockIODetailEntity sp = list.get(0);
			result.put("totalprice", sp.getTotalprice());
		}
		return result;
	}
	
	@Override
	public List<StockIODetailEntity> createStockIODetail(StockIOEntity stockIOEntity, List<Map<String, Object>> params) {
		List<StockIODetailEntity> stockIODetailList = new ArrayList<StockIODetailEntity>();
		
		for (Map<String, Object> param : params) {
			//商品编号
			String productid = (String) param.get("id");
			if(productid == null){
				return null;
			}
			//单位
			String unitid = (String) param.get("unitid");
			if(unitid == null){
				return null;
			}
			//区域
			String stockSegment = (String) param.get("segmentid");
			if(stockSegment == null){
				return null;
			}
			
			//出入库总价
			BigDecimal totalprice  = ConvertTool.toBigDecimal(param.get("totalprice"));
			//出入库数量
			BigDecimal iostocknum = ConvertTool.toBigDecimal(param.get("iostocknum"));
			//出入库单价
			BigDecimal unitPrice = totalprice.divide(iostocknum, 4, BigDecimal.ROUND_HALF_UP);
			//供应商
			String supplierid = (String) param.get("supplierid");
			//运费
			BigDecimal freightprice = ConvertTool.toBigDecimal(param.get("freightprice"));
			//备注
			String remark = (String) param.get("detailremark");
			
			StockIODetailEntity stockIODetail = new StockIODetailEntity();
			
			ProductEntity productEntity = findUniqueByProperty(ProductEntity.class, "id", productid);
			if(StringUtil.isNotEmpty(supplierid)){
				StockSupplierEntity supplier = findUniqueByProperty(StockSupplierEntity.class, "id", supplierid);
				stockIODetail.setSupplierid(supplier);
			}
			
			ProductUnitEntity unit = findUniqueByProperty(ProductUnitEntity.class, "id", unitid);
			StockSegmentEntity segmentEntity = findUniqueByProperty(StockSegmentEntity.class, "id", stockSegment);
			
			stockIODetail.setUnitprice(unitPrice);
			stockIODetail.setTotalprice(totalprice);
			stockIODetail.setIostocknum(iostocknum);
			stockIODetail.setUnitprice(unitPrice);
			stockIODetail.setStockSegment(segmentEntity);
			stockIODetail.setProductid(productEntity);
			stockIODetail.setRemark(remark);
			stockIODetail.setFreightprice(freightprice);
			stockIODetail.setStockorderid(stockIOEntity);
			stockIODetail.setUnitid(unit);
			
			Map<String, Object> stockProductParams = stockProductServiceI.getParams(null,productEntity, segmentEntity.getStockid(), segmentEntity,null,null,null);
			List<StockProductEntity> list = stockProductServiceI.findListByParams(stockProductParams);
			if(list.size() > 0 && list != null){
				StockProductEntity stockProductEntity = list.get(0);
				BigDecimal beforeTotal = stockProductEntity.getTotalprice();
				BigDecimal beforeNum = stockProductEntity.getStocknum();
				BigDecimal beforeAverage = ConvertTool.toBigDecimal(0.00);
				
				if(beforeNum.compareTo(ConvertTool.toBigDecimal(0.00)) > 0){
					beforeAverage = beforeTotal.divide(beforeNum, 4, BigDecimal.ROUND_HALF_UP);
				}
				//之前库存数量
				stockIODetail.setBeforeNum(beforeNum);
				//之前库存总价
				stockIODetail.setBeforeTotalPrice(beforeTotal);
				//之前库存均价
				stockIODetail.setBeforeAveragePrice(beforeAverage);
			}
			
			stockIODetailList.add(stockIODetail);
		}
		
		return stockIODetailList;
	}
	
	public List<Map<String, Object>> buildStockIODetailParams(HttpServletRequest request){
		Integer exps = ConvertTool.toInt(ResourceUtil.getParameter("Len"));
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < exps; i++) {
			Map<String, Object> params = new HashMap<String,Object>();
			String productid = request.getParameter("id-" + i);
			if(!StringUtil.isNotEmpty(productid)){
				return list;
			}
			//商品编号
			params.put("id", productid);
			//商品名称
			String productName = request.getParameter("name-" + i);
			if(StringUtil.isNotEmpty(productName)){
				params.put("name", productName);
			}
			//备注
			String remark = request.getParameter("detailremark-" + i);
			if(StringUtil.isNotEmpty(remark)){
				params.put("detailremark", remark);
			}
			//单位
			String unitid = request.getParameter("unitid-" + i);
			if(StringUtil.isNotEmpty(unitid)){
				params.put("unitid", unitid);
			}
			//区域
			String stockSegment = request.getParameter("segmentid-" + i);
			if(StringUtil.isNotEmpty(stockSegment)){
				params.put("segmentid", stockSegment);
			}
			//供应商
			String supplierid = request.getParameter("supplierid");
			if(StringUtil.isNotEmpty(supplierid)){
				params.put("supplierid", supplierid);
			}
			//运费
			BigDecimal freightprice = ConvertTool.toBigDecimal(request.getParameter("freightprice-" + i));
			if(freightprice.compareTo(new BigDecimal(0.00)) > 0){
				params.put("freightprice", freightprice);
			}
			//出入库单价
			BigDecimal unitprice = ConvertTool.toBigDecimal(request.getParameter("unitprice-" + i));
			if(unitprice.compareTo(new BigDecimal(0.00)) > 0){
				params.put("unitprice", unitprice);
			}
			//出入库总价
			BigDecimal totalprice  = ConvertTool.toBigDecimal(request.getParameter("totalprice-" + i));
			if(totalprice.compareTo(new BigDecimal(0.00)) > 0){
				params.put("totalprice", totalprice);
			}
			//出入库数量
			BigDecimal iostocknum = ConvertTool.toBigDecimal(request.getParameter("iostocknum-" + i));
			if(iostocknum.compareTo(new BigDecimal(0.00)) > 0){
				params.put("iostocknum", iostocknum);
			}
			list.add(params);
		}
		return list;
	}

	@Override
	public List<StockIODetailEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class);
		String productId = (String) params.get("productId");
		if(productId != null){
			cq.eq("productid.id", productId);
		}
		
		String stockId = (String) params.get("stockId");
		if(stockId != null){
			cq.eq("stockid.id", stockId);
		}
		
		String stockSegmentId = (String) params.get("stockSegmentId");
		if(stockSegmentId != null){
			cq.eq("stockSegment.id", stockSegmentId);
		}
		cq.add();
		List<StockIODetailEntity> list = this.getListByCriteriaQuery(cq, false);
		return list;
	}


	@Override
	public Map<String, Object> getParams(ProductEntity product,StockEntity stock, StockSegmentEntity segment) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		if(product != null){
			params.put("productid", product.getId());
		}
		if(stock != null){
			params.put("stockid", stock.getId());
		}
		if(segment != null){
			params.put("stockSegmentId", segment.getId());
		}
		if(user != null){
			params.put("companyid", user.getCompany().getId());
		}
		return params;
	}
}