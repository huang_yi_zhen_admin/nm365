package cn.gov.xnc.admin.stock.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGridReturn;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockCheckServiceI extends CommonService{
	
	/**
	 * 添加盘点单：保存，提交
	 * @param stockCheckOrder
	 * @param request
	 * @return
	 */
	public AjaxJson saveCommit(StockCheckEntity stockCheckOrder, HttpServletRequest request);
	
	/**
	 * 添加盘点单：保存，不提交
	 * @param stockCheckOrder
	 * @param request
	 * @return
	 */
	public AjaxJson saveCheck(StockCheckEntity stockCheckOrder, HttpServletRequest request);
	
	public AjaxJson mergeCommit(HttpServletRequest request,String checkids, String name );
	
	public AjaxJson delCheckOrders(HttpServletRequest request,String checkids);
	
}
