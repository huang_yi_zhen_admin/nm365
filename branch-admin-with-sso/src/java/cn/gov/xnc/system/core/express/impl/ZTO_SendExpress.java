/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.ExpressCompany;

import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * @author fyy
 *
 */
public class ZTO_SendExpress extends BaseSendExpress {
	
	//快递类型  普通订单
	public final static int ZTO_ExpType_1 = 1;//快递类型 普通订单
	//快递类型  线下订单
	public final static int ZTO_ExpType_2 = 2;//快递类型 线下订单
	//快递类型  COD订单
	public final static int ZTO_ExpType_3 = 3;//快递类型 COD订单
	//快递类型  限时物流
	public final static int ZTO_ExpType_4 = 4;//快递类型 限时物流
	//快递类型 快递保障订单
	public final static int ZTO_ExpType_5 = 5;//快递类型快递保障订单

	/**
	 * 
	 */
	public ZTO_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.ZTO);
	}
	
	
	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_1 = new HashMap<String,String>();
		exptype_1.put("exptype", "1");
		exptype_1.put("exptypename", "普通订单");
		results.add(exptype_1);
		
		HashMap<String,String> exptype_2 = new HashMap<String,String>();
		exptype_2.put("exptype", "2");
		exptype_2.put("exptypename", "线下订单");
		results.add(exptype_2);
		
		HashMap<String,String> exptype_3 = new HashMap<String,String>();
		exptype_3.put("exptype", "3");
		exptype_3.put("exptypename", "COD订单");
		results.add(exptype_3);
		
		HashMap<String,String> exptype_4 = new HashMap<String,String>();
		exptype_4.put("exptype", "4");
		exptype_4.put("exptypename", "限时物流");
		results.add(exptype_4);
		
		HashMap<String,String> exptype_5 = new HashMap<String,String>();
		exptype_5.put("exptype", "5");
		exptype_5.put("exptypename", "快递保障订单");
		results.add(exptype_5);
		
		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(5);
		data.setPageNum(1);
		data.setShowNum(5);
		data.setResults(results);	
		//data.setField("exptypename,exptype");
		
		return data;
	}
	
	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		if( StringUtil.isEmpty(getCustomerName()) ){
			errMsg.append("请输入商家ID");
			return false;
		}
		if(StringUtil.isEmpty(getCustomerPwd())){
			errMsg.append("请输入商家密钥");
			return false;
		}
		return super.checkdata(errMsg);
	}


}
