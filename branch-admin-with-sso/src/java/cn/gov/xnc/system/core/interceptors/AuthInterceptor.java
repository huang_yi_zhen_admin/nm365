package cn.gov.xnc.system.core.interceptors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.core.util.oConvertUtils;
import cn.gov.xnc.system.sso.client.SSOHelper;
import cn.gov.xnc.system.sso.client.TokenHelper;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.TSFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;


/**
 * 权限拦截器
 * 
 * @author  hyzh
 * 
 */
public class AuthInterceptor implements HandlerInterceptor {
	 
	//private static final Logger logger = Logger.getLogger(AuthInterceptor.class);
	@Autowired
	private SystemService systemService;

	private List<String> excludeUrls;//例外的url
	private static List<TSFunction> functionList;

	public List<String> getExcludeUrls() {
		return excludeUrls;
	}

	public void setExcludeUrls(List<String> excludeUrls) {
		this.excludeUrls = excludeUrls;
	}

	/**
	 * 在controller后拦截
	 */
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception exception) throws Exception {
		
	
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView modelAndView) throws Exception {
		
		
		
		
	}

	/**
	 * 在controller前拦截
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		
		
		
		
		
		//返回服务路径供静态页面使用
		String base =request.getScheme() +"://" + request.getServerName()  + request.getContextPath();
        String sourceUrl =  base + "/plug-in";
        
        request.setAttribute("staticUrl", sourceUrl);
        request.setAttribute("base", base);

		String requestPath = ResourceUtil.getRequestPath(request);// 请求地址	
		if (matchExclude(excludeUrls,requestPath)) {
			return true;
			
		} else {
			
			TSUser user = SSOHelper.loginValidate(request);
			
			//判断是否登录
			if(null == user){
				TokenHelper.removeTokenInCache();
				SSOHelper.redirect2Login(response);
				return false;
			}
			
			user = systemService.findUniqueByProperty(TSUser.class, "id", user.getId());
			//初始化会话信息
			SSOHelper.initClient(request, user);
			
			if (null != user && null != user.getCompany()) {
				
				//判断菜单权限
				if(!hasMenuAuth(request)){
					response.sendRedirect(base + "/loginController/noAuth");
					return false;
				} 
				
				String functionId=oConvertUtils.getString(request.getParameter("clickFunctionId"));		
				if(!oConvertUtils.isEmpty(functionId)){
					Set<String> operationCodes = systemService.getOperationCodesByUserIdAndFunctionId(user.getId(), functionId);
					request.setAttribute("operationCodes", operationCodes);
				}
				if(!oConvertUtils.isEmpty(functionId)){
					List<String> allOperation=this.systemService.findListbySql("SELECT operationcode FROM t_s_operation  WHERE functionid='"+functionId+"'"); 
					List<String> newall = new ArrayList<String>();
					if(allOperation.size()>0){
						for(String s:allOperation){ 
						    s=s.replaceAll(" ", "");
							newall.add(s); 
						}						 
						String hasOperSql="SELECT operation FROM t_s_role_function fun, t_s_role_user role WHERE  " + "fun.functionid='"+functionId+"' AND fun.operation!=''  AND fun.roleid=role.roleid AND role.userid='"+ user.getId()+"' ";
						List<String> hasOperList = this.systemService.findListbySql(hasOperSql); 
					    for(String strs:hasOperList){
							    for(String s:strs.split(",")){
							        s=s.replaceAll(" ", "");
							    	newall.remove(s);
							    } 
						} 
					}
					 request.setAttribute("noauto_operationCodes", newall);
				}
				
				return true;
			} else {
				return false;
			}
		}
	}
	private boolean hasMenuAuth(HttpServletRequest request){
		String requestPath = ResourceUtil.getRequestPath(request);// 用户访问的资源地址
		// 是否是功能表中管理的url
		boolean bMgrUrl = false;
		if (functionList == null) {
			functionList = systemService.loadAll(TSFunction.class);
		}
		for (TSFunction function : functionList) {
			if (function.getFunctionUrl() != null && function.getFunctionUrl().startsWith(requestPath)) {
				bMgrUrl = true;
				break;
			}
		}
		if (!bMgrUrl) {
			return true;
		}
		 
		//这句判断在前一个判断作用下已经没有用
		/*String funcid=oConvertUtils.getString(request.getParameter("clickFunctionId"));
		if(!bMgrUrl && (requestPath.indexOf("loginController.do")!=-1||funcid.length()==0)){
			return true;
		} */
		String userid = ClientManager.getInstance().getClient(
				ContextHolderUtils.getSession().getId()).getUser().getId();
		//requestPath=requestPath.substring(0, requestPath.indexOf("?")+1);
		String sql = "SELECT DISTINCT f.id FROM t_s_function f,t_s_role_function  rf,t_s_role_user ru " +
					" WHERE f.id=rf.functionid AND rf.roleid=ru.roleid AND " +
					"ru.userid='"+userid+"' AND f.functionurl like '"+requestPath+"%'"; 
		List list = this.systemService.findListbySql(sql);
		if(list.size()==0){
			return false;
		}else{
			return true;
		}
	}
	/**
	 * 转发
	 * 
	 * @param user
	 * @param req
	 * @return
	 */
	@RequestMapping(params = "forword")
	public ModelAndView forword(HttpServletRequest request) {
		return new ModelAndView(new RedirectView("loginController.do?login"));
	}

	private void forward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("webpage/login/timeout.jsp").forward(request, response);
	}
	
	private boolean matchExclude(List<String> excludeUrls, String testUrl){
		boolean match = false;
		
		if(StringUtil.isNotEmpty(testUrl)){
			
			Pattern pattern = null;
			Matcher matcher = null;
			for (String url : excludeUrls) {
				
				if(url.equals(testUrl)){
					match = true;
					break;
				}
				
				if(!match){
					if(url.indexOf("?") > 0){
						String fstStr = url.substring(0,url.indexOf("?"));
						String secStr = url.substring(url.indexOf("?") + 1);
						url = fstStr + "\\?" + secStr;
					}
					
					pattern = Pattern.compile(url);
					matcher = pattern.matcher(testUrl);
					
					if(matcher.matches()){
						match = true;
						break;
					}
				}
			}
		}
		
		return match;
	}

}
