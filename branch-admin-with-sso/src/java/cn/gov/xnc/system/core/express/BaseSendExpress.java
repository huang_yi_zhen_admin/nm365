package cn.gov.xnc.system.core.express;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.util.ExpressUtil;

/**
 * 发送给快递鸟请求数据
 * 
 * */
public class BaseSendExpress {
	
	public BaseSendExpress(){
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
	}


	/*
	 * 必填！请求信息
	 */
	private RequestData requestData;//必填！请求信息
	
	/*
	 * 必填！商户ID
	 */
	private String EBusinessID;//必填！商户ID

	/*
	 * 必填！请求指令类型：1007电子面单
	 */
	private String RequestType;//必填！请求指令类型：1007电子面单
	
	/*
	 * 必填！数据内容签名
	 */
	private String DataSign;// 必填！数据内容签名
	
	/*
	 * 请求、返回数据类型： 2-json
	 */
	private String DataType;//请求、返回数据类型： 2-json
	
	private String orderid;//order表关联id
	
	/*
	 * 下单获取电子面但
	 */
	/*public BaseRecvExpress doExpress(){
		BaseRecvExpress recvExpress = new BaseRecvExpress();
		StringBuffer errMsg = new StringBuffer();
		if( !checkdata(errMsg) ){
			recvExpress.setResultCode("INNERERROR");
			recvExpress.setReason(errMsg.toString());
			return recvExpress;
		}
		
		Map<String, String> params = null;
		try {
			params = packData(errMsg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			recvExpress.setResultCode("INNERERROR");
			recvExpress.setReason(errMsg.toString());
			return recvExpress;
		}
		
		String result=ExpressUtil.sendPost(ExpressUtil.ReqURL, params);
		
		recvExpress.parseJson(result);
		
		return recvExpress;
	}*/
	
	
	
	public Map<String, String> packData(StringBuffer errMsg) throws Exception{
		Map<String, String> params = new HashMap<String, String>();
		String requestDataJson = requestData.toJsonStr();
		System.out.println("requestDataJson"+requestDataJson);
		params.put("RequestData", ExpressUtil.urlEncoder(requestData.toJsonStr(), "UTF-8"));
		params.put("EBusinessID", EBusinessID);
		params.put("RequestType", RequestType);
		DataSign=ExpressUtil.encrypt(requestDataJson, ExpressUtil.AppKey, "UTF-8");
		params.put("DataSign", ExpressUtil.urlEncoder(DataSign, "UTF-8"));
		params.put("DataType", DataType);
		
		return params;
	}
	
	public boolean checkdata(StringBuffer errMsg)
	{
		boolean result = false;
		if( null == requestData ){
			errMsg.append("requestData is null");
			return result;
		}else if( null == requestData.getShipperCode()){
			errMsg.append("requestData.getShipperCode() is null");
			return result;
		}else if( null == requestData.getOrderCode() ){
			errMsg.append("requestData.getOrderCode() is null");
			return result;
		}else if( null == requestData.getReceiver() ){
			errMsg.append("requestData.getReceiver() is null");
			return result;
		}else if( null == requestData.getReceiver().getName() ){
			errMsg.append("requestData.getReceiver().getName() is null");
			return result;
		}else if( null == requestData.getReceiver().getTel() && null == requestData.getReceiver().getMobile() ){
			errMsg.append("requestData.getReceiver().getTel() and requestData.getReceiver().getMobile() is null");
			return result;
		}else if( null == requestData.getReceiver().getProvinceName() ){
			errMsg.append("requestData.getReceiver().getProvinceName() is null");
			return result;
		}else if( null == requestData.getReceiver().getCityName() ){
			errMsg.append("requestData.getReceiver().getCityName() is null");
			return result;
		}else if( null == requestData.getReceiver().getAddress() ){
			errMsg.append("requestData.getReceiver().getAddress() is null");
			return result;
		}else if( null == requestData.getSender() ){
			errMsg.append("requestData.getSender() is null");
			return result;
		}else if( null == requestData.getSender().getName() ){
			errMsg.append("requestData.getSender().getName() is null");
			return result;
		}else if( null == requestData.getSender().getTel() && null == requestData.getSender().getMobile() ){
			errMsg.append("requestData.getSender().getTel() and requestData.getSender().getMobile() is null");
			return result;
		}else if( null == requestData.getSender().getProvinceName() ){
			errMsg.append("requestData.getSender().getProvinceName() is null");
			return result;
		}else if( null == requestData.getSender().getCityName() ){
			errMsg.append("requestData.getSender().getCityName() is null");
			return result;
		}else if( null == requestData.getSender().getAddress() ){
			errMsg.append("requestData.getSender().getAddress() is null");
			return result;
		}else if( null == requestData.getCommodity() ){
			errMsg.append("requestData.getCommodity() is null");
			return result;
		}else if( null == EBusinessID ){
			errMsg.append("EBusinessID is null");
			return result;
		}else if( null == RequestType ){
			errMsg.append("RequestType is null");
			return result;
		}else if( null == DataType ){
			errMsg.append("DataType is null");
			return result;
		}
		
		
		result = true;
		return result;
	}
	
	
	/*
	 * 必填！请求信息
	 */
	protected RequestData getRequestData() {
		return requestData;
	}

	/*
	 * 必填！请求信息
	 */
	protected void setRequestData(RequestData requestData) {
		this.requestData = requestData;
	}

	/*
	 * 必填！商户ID
	 */
	protected String getEBusinessID() {
		return EBusinessID;
	}
	/*
	 * 必填！商户ID
	 */
	protected void setEBusinessID(String eBusinessID) {
		EBusinessID = eBusinessID;
	}

	/*
	 * 必填！请求指令类型：1007电子面单
	 */
	protected String getRequestType() {
		return RequestType;
	}
	/*
	 * 必填！请求指令类型：1007电子面单
	 */
	protected void setRequestType(String requestType) {
		RequestType = requestType;
	}

	/*
	 * 必填！数据内容签名
	 */
	protected String getDataSign() {
		return DataSign;
	}
	/*
	 * 必填！数据内容签名
	 */
	protected void setDataSign(String dataSign) {
		DataSign = dataSign;
	}

	/*
	 * 请求、返回数据类型： 2-json
	 */
	protected String getDataType() {
		return DataType;
	}
	/*
	 * 请求、返回数据类型： 2-json
	 */
	protected void setDataType(String dataType) {
		DataType = dataType;
	}
	
	
	
	
	
	
	
	
	/**
	 * 用户自定义回调信息
	 * @return the callBack
	 */
	public String getCallBack() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getCallBack();
		}
	}

	/**
	 * 用户自定义回调信息
	 * @param callBack the callBack to set
	 */
	public void setCallBack(String callBack) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		
		this.getRequestData().setCallBack(callBack);
	}

	/**
	 * 会员标识平台方与快递鸟统一用户标识的商家ID
	 * @return the memberID
	 */
	public String getMemberID() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getMemberID();
		}
	}

	/**
	 * 会员标识平台方与快递鸟统一用户标识的商家ID
	 * @param memberID the memberID to set
	 */
	public void setMemberID(String memberID) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setMemberID(memberID);
	}

	/**
	 * 电子面单客户账号（与快递网点申请）
	 * @return the customerName
	 */
	public String getCustomerName() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getCustomerName();
		}
	}

	/**
	 * 电子面单客户账号（与快递网点申请）
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setCustomerName(customerName);
	}

	/**
	 * 电子面单密码
	 * @return the customerPwd
	 */
	public String getCustomerPwd() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getCustomerPwd();
		}
	}

	/**
	 * 电子面单密码
	 * @param customerPwd the customerPwd to set
	 */
	public void setCustomerPwd(String customerPwd) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setCustomerPwd(customerPwd);
	}

	/**
	 * 收件网点标识
	 * @return the sendSite
	 */
	public String getSendSite() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getSendSite();
		}
	}

	/**
	 * 收件网点标识
	 * @param sendSite the sendSite to set
	 */
	public void setSendSite(String sendSite) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setSendSite(sendSite);
	}

	/**
	 * 必填！快递公司编码
	 * @return the shipperCode
	 */
	public String getShipperCode() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getShipperCode();
		}
	}

	/**
	 * 必填！快递公司编码
	 * @param shipperCode the shipperCode to set
	 */
	public void setShipperCode(String shipperCode) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setShipperCode(shipperCode);
	}

	/**
	 * 快递单号,一般不填
	 * @return the logisticCode
	 */
	public String getLogisticCode() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getLogisticCode();
		}
	}

	/**
	 * 快递单号,一般不填
	 * @param logisticCode the logisticCode to set
	 */
	public void setLogisticCode(String logisticCode) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setLogisticCode(logisticCode);
	}

	/**
	 * 第三方订单号
	 * @return the thrOrderCode
	 */
	public String getThrOrderCode() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getThrOrderCode();
		}
	}

	/**
	 * 第三方订单号
	 * @param thrOrderCode the thrOrderCode to set
	 */
	public void setThrOrderCode(String thrOrderCode) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setThrOrderCode(thrOrderCode);
	}

	/**
	 * 必填！订单编号
	 * @return the orderCode
	 */
	public String getOrderCode() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getOrderCode();
		}

	}

	/**
	 * 必填！订单编号
	 * @param orderCode the orderCode to set
	 */
	public void setOrderCode(String orderCode) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setOrderCode(orderCode);
	}

	/**
	 * 月结编码
	 * @return the monthCode
	 */
	public String getMonthCode() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getMonthCode();
		}
	}

	/**
	 * 月结编码
	 * @param monthCode the monthCode to set
	 */
	public void setMonthCode(String monthCode) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setMonthCode(monthCode);
	}

	/**
	 * 必填！邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
	 * @return the payType
	 */
	public int getPayType() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getPayType();
		}
	}

	/**
	 * 必填！邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
	 * @param payType the payType to set
	 */
	public void setPayType(int payType) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setPayType(payType);
	}

	/**
	 * 必填！快递类型：1-标准快件
	 * @return the expType
	 */
	public int getExpType() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getExpType();
		}
	}

	/**
	 * 必填！快递类型：1-标准快件
	 * @param expType the expType to set
	 */
	public void setExpType(int expType) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setExpType(expType);
	}

	/**
	 * 是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 * @return the isNotice
	 */
	public int getIsNotice() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getIsNotice();
		}
	}

	/**
	 * 是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 * @param isNotice the isNotice to set
	 */
	public void setIsNotice(int isNotice) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setIsNotice(isNotice);
	}

	/**
	 * 寄件费（运费）
	 * @return the cost
	 */
	public double getCost() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getCost();
		}
	}

	/**
	 * 寄件费（运费）
	 * @param cost the cost to set
	 */
	public void setCost(double cost) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setCost(cost);
	}

	/**
	 * 其他费用
	 * @return the otherCost
	 */
	public double getOtherCost() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getOtherCost();
		}
	}

	/**
	 * 其他费用
	 * @param otherCost the otherCost to set
	 */
	public void setOtherCost(double otherCost) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setOtherCost(otherCost);
	}

	/**
	 * 收件人信息
	 * @return the receiver
	 */
	public Receiver getReceiver() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getReceiver();
		}
	}
	
	/**
	 * 收件人信息
	 * @param receiver the receiver to set
	 */
	public void setReceiver(Receiver receiver) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setReceiver(receiver);
	}
	
	/**
	 * 收件人公司
	 * @return the company
	 */
	public String getReceiverCompany() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getCompany();
		}
	}

	/**
	 * 收件人公司
	 * @param company the company to set
	 */
	public void setReceiverCompany(String company) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setCompany(company);
	}

	/**
	 * 必填！收件人
	 * @return the name
	 */
	public String getReceiverName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getName();
		}
	}

	/**
	 * 必填！收件人
	 * @param name the name to set
	 */
	public void setReceiverName(String name) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setName(name);
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @return the tel
	 */
	public String getReceiverTel() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getTel();
		}
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @param tel the tel to set
	 */
	public void setReceiverTel(String tel) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setTel(tel);
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @return the mobile
	 */
	public String getReceiverMobile() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getMobile();
		}
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @param mobile the mobile to set
	 */
	public void setReceiverMobile(String mobile) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setMobile(mobile);
	}

	/**
	 * 收件人邮编
	 * @return the postCode
	 */
	public String getReceiverPostCode() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getPostCode();
		}
	}

	/**
	 * 收件人邮编
	 * @param postCode the postCode to set
	 */
	public void setReceiverPostCode(String postCode) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setPostCode(postCode);
	}

	/**
	 * 必填！收件省（如广东省，不要缺少“省”）
	 * @return the provinceName
	 */
	public String getReceiverProvinceName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getProvinceName();
		}
	}

	/**
	 * 必填！收件省（如广东省，不要缺少“省”）
	 * @param provinceName the provinceName to set
	 */
	public void setReceiverProvinceName(String provinceName) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setProvinceName(provinceName);
	}

	/**
	 * 必填！收件市（如深圳市，不要缺少“市”）
	 * @return the cityName
	 */
	public String getReceiverCityName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getCityName();
		}
	}

	/**
	 * 必填！收件市（如深圳市，不要缺少“市”）
	 * @param cityName the cityName to set
	 */
	public void setReceiverCityName(String cityName) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setCityName(cityName);
	}

	/**
	 * 收件区（如福田区，不要缺少“区”或“县”）
	 * @return the expAreaName
	 */
	public String getReceiverExpAreaName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getExpAreaName();
		}
	}

	/**
	 * 收件区（如福田区，不要缺少“区”或“县”）
	 * @param expAreaName the expAreaName to set
	 */
	public void setReceiverExpAreaName(String expAreaName) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setExpAreaName(expAreaName);
	}

	/**
	 * 必填！收件人详细地址
	 * @return the address
	 */
	public String getReceiverAddress() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getReceiver() ){
			return null;
		}else{
			return this.getRequestData().getReceiver().getAddress();
		}
	}

	/**
	 * 必填！收件人详细地址
	 * @param address the address to set
	 */
	public void setReceiverAddress(String address) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getReceiver() ){
			Receiver receiver = new Receiver();
			this.getRequestData().setReceiver(receiver);
		}
		this.getRequestData().getReceiver().setAddress(address);
	}

	

	/**
	 * 发件人信息
	 * @return the sender
	 */
	public Sender getSender() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getSender();
		}
	}

	/**
	 * 发件人信息
	 * @param sender the sender to set
	 */
	public void setSender(Sender sender) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setSender(sender);
	}
	
	/**
	 * 发件人公司
	 * @return the company
	 */
	public String getSenderCompany() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getCompany();
		}
	}

	/**
	 * 发件人公司
	 * @param company the company to set
	 */
	public void setSenderCompany(String company) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setCompany(company);
	}

	/**
	 * 必填！发件人
	 * @return the name
	 */
	public String getSenderName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getName();
		}
	}

	/**
	 * 必填！发件人
	 * @param name the name to set
	 */
	public void setSenderName(String name) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setName(name);
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @return the tel
	 */
	public String getSenderTel() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getTel();
		}
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @param tel the tel to set
	 */
	public void setSenderTel(String tel) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setTel(tel);
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @return the mobile
	 */
	public String getSenderMobile() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getMobile();
		}
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @param mobile the mobile to set
	 */
	public void setSenderMobile(String mobile) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setMobile(mobile);
	}

	/**
	 * 发件人邮编
	 * @return the postCode
	 */
	public String getSenderPostCode() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getPostCode();
		}
	}

	/**
	 * 发件人邮编
	 * @param postCode the postCode to set
	 */
	public void setSenderPostCode(String postCode) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setPostCode(postCode);
	}

	/**
	 * 必填！发件省（如广东省，不要缺少“省”）
	 * @return the provinceName
	 */
	public String getSenderProvinceName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getProvinceName();
		}
	}

	/**
	 * 必填！发件省（如广东省，不要缺少“省”）
	 * @param provinceName the provinceName to set
	 */
	public void setSenderProvinceName(String provinceName) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setProvinceName(provinceName);
	}

	/**
	 * 必填！发件市（如深圳市，不要缺少“市”）
	 * @return the cityName
	 */
	public String getSenderCityName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getCityName();
		}
	}

	/**
	 * 必填！发件市（如深圳市，不要缺少“市”）
	 * @param cityName the cityName to set
	 */
	public void setSenderCityName(String cityName) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setCityName(cityName);
	}

	/**
	 * 发件区（如福田区，不要缺少“区”或“县”）
	 * @return the expAreaName
	 */
	public String getSenderExpAreaName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getExpAreaName();
		}
	}

	/**
	 * 发件区（如福田区，不要缺少“区”或“县”）
	 * @param expAreaName the expAreaName to set
	 */
	public void setSenderExpAreaName(String expAreaName) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setExpAreaName(expAreaName);
	}

	/**
	 * 必填！发件详细地址
	 * @return the address
	 */
	public String getSenderAddress() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getSender() ){
			return null;
		}else{
			return this.getRequestData().getSender().getAddress();
		}
	}

	/**
	 * 必填！发件详细地址
	 * @param address the address to set
	 */
	public void setSenderAddress(String address) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getSender() ){
			Sender sender = new Sender();
			this.getRequestData().setSender(sender);
		}
		this.getRequestData().getSender().setAddress(address);
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @return the startDate
	 */
	public String getStartDate() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getStartDate();
		}
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setStartDate(startDate);
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @return the endDate
	 */
	public String getEndDate() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getEndDate();
		}
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setEndDate(endDate);
	}

	/**
	 * 物品总重量kg
	 * @return the weight
	 */
	public double getWeight() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getWeight();
		}
	}

	/**
	 * 物品总重量kg
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setWeight(weight);
	}

	/**
	 * 件数/包裹数
	 * @return the quantity
	 */
	public int getQuantity() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getQuantity();
		}
	}

	/**
	 * 件数/包裹数
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setQuantity(quantity);
	}

	/**
	 * 物品总体积m3
	 * @return the volume
	 */
	public double getVolume() {
		if( null == this.getRequestData() ){
			return 0;
		}else{
			return this.getRequestData().getVolume();
		}
	}

	/**
	 * 物品总体积m3
	 * @param volume the volume to set
	 */
	public void setVolume(double volume) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setVolume(volume);
	}

	/**
	 * 备注
	 * @return the remark
	 */
	public String getRemark() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getRemark();
		}
	}

	/**
	 * 备注
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setRemark(remark);
	}

	/**
	 * 增值服务
	 * @return the addService
	 */
	public AddService getAddService() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getAddService();
		}
	}

	/**
	 * 增值服务
	 * @param addService the addService to set
	 */
	public void setAddService(AddService addService) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setAddService(addService);
	}
	
	/**
	 * 增值服务名称
	 * @return the name
	 */
	public String getAddServiceName() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getAddService() ){
			return null;
		}else{
			return this.getRequestData().getAddService().getName();
		}
	}

	/**
	 * 增值服务名称
	 * @param name the name to set
	 */
	public void setAddServiceName(String name) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getAddService() ){
			AddService addService = new AddService();
			this.getRequestData().setAddService(addService);
		}
		this.getRequestData().getAddService().setName(name);
	}

	/**
	 * 增值服务值
	 * @return the value
	 */
	public String getAddServiceValue() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getAddService() ){
			return null;
		}else{
			return this.getRequestData().getAddService().getValue();
		}
	}

	/**
	 * 增值服务值
	 * @param value the value to set
	 */
	public void setAddServiceValue(String value) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getAddService() ){
			AddService addService = new AddService();
			this.getRequestData().setAddService(addService);
		}
		this.getRequestData().getAddService().setValue(value);
	}

	/**
	 * 客户标识(选填)
	 * @return the customerID
	 */
	public String getAddServiceCustomerID() {
		if( null == this.getRequestData() ){
			return null;
		}else if( null == this.getRequestData().getAddService() ){
			return null;
		}else{
			return this.getRequestData().getAddService().getCustomerID();
		}
	}

	/**
	 * 客户标识(选填)
	 * @param customerID the customerID to set
	 */
	public void setAddServiceCustomerID(String customerID) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getAddService() ){
			AddService addService = new AddService();
			this.getRequestData().setAddService(addService);
		}
		this.getRequestData().getAddService().setCustomerID(customerID);
	}

	/**
	 * 货物信息
	 * @return the commodity
	 */
	public List<Commodity> getCommodity() {
		if( null == this.getRequestData() ){
			return null;
		}
		return this.getRequestData().getCommodity();
	}

	/**
	 * 货物信息
	 * @param commodity the commodity to add
	 */
	public void addCommodity(Commodity commodity) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		if( null == this.getRequestData().getCommodity() ){
			List<Commodity> commodityList = new ArrayList<Commodity>();
			this.getRequestData().setCommodity(commodityList);
		}
		this.getRequestData().getCommodity().add(commodity);
	}

	
	
	/**
	 * 返回电子面单模板：0-不需要；1-需要
	 * @return the isReturnPrintTemplate
	 */
	public String getIsReturnPrintTemplate() {
		if( null == this.getRequestData() ){
			return null;
		}else{
			return this.getRequestData().getIsReturnPrintTemplate();
		}
	}

	/**
	 * 返回电子面单模板：0-不需要；1-需要
	 * @param isReturnPrintTemplate the isReturnPrintTemplate to set
	 */
	public void setIsReturnPrintTemplate(String isReturnPrintTemplate) {
		if( null == this.getRequestData() ){
			RequestData requestData = new RequestData();
			this.setRequestData(requestData);
		}
		this.getRequestData().setIsReturnPrintTemplate(isReturnPrintTemplate);
	}

	/**
	 * @return the orderid
	 */
	public String getOrderid() {
		return orderid;
	}

	/**
	 * @param orderid the orderid to set
	 */
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
}
