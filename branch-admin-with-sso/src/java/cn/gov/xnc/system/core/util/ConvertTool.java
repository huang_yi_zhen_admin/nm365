package cn.gov.xnc.system.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 类型转换工具
 */
public class ConvertTool {
	/**
	 * 转换成整数
	 * @param obj
	 * @return
	 */
	public static int toInt(Object obj){
		return toInt(obj,0);
	}
	
	public static int toInt(Object obj,int deVal){
		try{
			return Integer.parseInt(obj.toString());
		}catch(Exception ex){
			 return deVal;
		}
	}
	
	/**
	 * 转换成长整形
	 * @param obj
	 * @return
	 */
	public static long toLong(Object obj){
		return toLong(obj,0);
	}
	
	public static long toLong(Object obj,long deVal){
		try{
			return Long.parseLong(obj.toString());
		}catch(Exception ex){
			 return deVal;
		}
	}
	
	/**
	 * 转换成浮点型
	 * @param obj
	 * @return
	 */
	public static double toDouble(Object obj){
		return toDouble(obj,0);
	}
	
	public static double toDouble(Object obj,double deVal){
		try{
			return Double.parseDouble(obj.toString());
		}catch(Exception ex){
			 return deVal;
		}
	}
	
	/**
	 * 转换成布尔型
	 * @param obj
	 * @return
	 */
	public static boolean toBoolean(Object obj){
			String v = obj.toString();
			if(v.toLowerCase().equals("true")|| v.equals("1")) 
				return true; 
			else
				return false;
	}
	
	/**
	 * 转换成数组
	 * @param str
	 * @param separator
	 * @param removeStr
	 * @return
	 */
	public static String[] toArray(String str,String separator,String removeStr){
		if(removeStr!=null && !removeStr.equals("")) str = str.replace(removeStr, "");
		return str.split(separator);
	}
	
	/**
	 * 转换成字符串列表
	 * @param str
	 * @param separator
	 * @param removeStr
	 * @return
	 */
	public static List<String> toList(String str,String separator,String removeStr){
		String[] list = toArray(str,separator,removeStr);
		List<String> result = new ArrayList<String>();
		for(String s : list){
			result.add(s);
		}
		return result;
	}
	
	public static List<String> toList(String str){
		return toList(str,",",null);
	}
	
	/**
	 * 转换成整形列表
	 * @param str
	 * @param separator
	 * @param removeStr
	 * @return
	 */
	public static List<Integer> toIntList(String str,String separator,String removeStr){
		String[] list = toArray(str,separator,removeStr);
		List<Integer> result = new ArrayList<Integer>();
		for(String s : list){
			result.add(toInt(s));
		}
		return result;
	}
	
	public static List<Integer> toIntList(String str){
		return toIntList(str,",",null);
	}
	
	/**
	 * 转换成长整形列表
	 * @param str
	 * @param separator
	 * @param removeStr
	 * @return
	 */
	public static List<Long> toLongList(String str,String separator,String removeStr){
		String[] list = toArray(str,separator,removeStr);
		List<Long> result = new ArrayList<Long>();
		for(String s : list){
			result.add(toLong(s));
		}
		return result;
	}
	
	public static List<Long> toLongList(String str){
		return toLongList(str,",",null);
	}
	
	/**
	 * 转换成实数形列表
	 * @param str
	 * @param separator
	 * @param removeStr
	 * @return
	 */
	public static List<Double> toDoubleList(String str,String separator,String removeStr){
		String[] list = toArray(str,separator,removeStr);
		List<Double> result = new ArrayList<Double>();
		for(String s : list){
			result.add(toDouble(s));
		}
		return result;
	}
	
	public static List<Double> toDoubleList(String str){
		return toDoubleList(str,",",null);
	}
	
	/**
	 * 转换成布尔形列表
	 * @param str
	 * @param separator
	 * @param removeStr
	 * @return
	 */
	public static List<Boolean> toBooleanList(String str,String separator,String removeStr){
		String[] list = toArray(str,separator,removeStr);
		List<Boolean> result = new ArrayList<Boolean>();
		for(String s : list){
			result.add(toBoolean(s));
		}
		return result;
	}
	
	public static List<Boolean> toBooleanList(String str){
		return toBooleanList(str,",",null);
	}
	
	/**
	 * 将列表转换成 字符串。默认已逗号隔开
	 * @param list
	 * @return
	 */
	public static String toString(Collection<?> list){
		return toString(list,",");
	}
	
	/**
	 * 将列表转换成字符串，指定分隔符
	 * @param list
	 * @param separator
	 * @return
	 */
	public static String toString(Collection<?> list,String separator){
		return toString(list,separator,""); 
	}
	
	/**
	 * 将列表转换成字符串，指定列表每项字符的左右两边字符
	 * @param list	要转换的列表
	 * @param separator	每项之间的分隔符
	 * @param lrStr	每项左右两边的字符
	 * @return
	 */
	public static String toString(Collection<?> list,String separator,String lrStr){
		String result="";
		for(Object o:list){
			if(!result.equals("")) result += separator;
			result+=lrStr+o.toString()+lrStr;
		}
		return result;
	}
	
	public static BigDecimal toBigDecimal(Object value){
		BigDecimal ret = new BigDecimal(0.00);
		try{
	        if( value != null ) {  
	            if( value instanceof BigDecimal ) {  
	                ret = (BigDecimal) value;  
	            } else if( value instanceof String ) {  
	                ret = new BigDecimal( (String) value );  
	            } else if( value instanceof BigInteger ) {  
	                ret = new BigDecimal( (BigInteger) value );  
	            } else if( value instanceof Number ) {  
	                ret = new BigDecimal( ((Number)value).doubleValue() );  
	            }else{
	            	return new BigDecimal(0.00); 
	            }
	        }  
		}catch(Exception e){
			System.out.println("Not possible to coerce ["+value+"] from class "+value.getClass()+" into a BigDecimal.");
			return new BigDecimal(0.00); 
		}
		return ret;
	}
}
