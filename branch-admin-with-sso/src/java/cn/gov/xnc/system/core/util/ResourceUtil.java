package cn.gov.xnc.system.core.util;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;



/**
 * 项目参数工具类
 * @author zero
 */
public class ResourceUtil {

	private static final ResourceBundle bundle = java.util.ResourceBundle.getBundle("sysConfig");
	/**
	 * 用户角色的key
	 */
	public static final String userRoleKey = "user_role_key";
	
	@Autowired
	private static SystemService systemService;
	
	/**
	 * 获取session定义名称
	 * 
	 * @return
	 */
	public static final String getSessionattachmenttitle(String sessionName) {
		return bundle.getString(sessionName);
	}
	
	
	/**
	 * 获取session定义名称
	 * 
	 * @return
	 */
	public static   TSUser getSessionUserName() {
		
		HttpSession session = ContextHolderUtils.getSession();
		if(ClientManager.getInstance().getClient(session.getId().toString()) != null ){
			TSUser user = ClientManager.getInstance().getClient(session.getId()).getUser();
				   
					return user;
		}
		return null;
	}
	
	public static final TSRole getSessionUserRole() {
		HttpSession session = ContextHolderUtils.getSession();
		if(ClientManager.getInstance().getClient(session.getId())!=null){
			return ClientManager.getInstance().getClient(session.getId()).getTsRole();
		}
		return null;
		//return (TSRole) session.getAttribute(userRoleKey);
	}
	
	
	
	@Deprecated
	public static final List<TSRoleFunction> getSessionTSRoleFunction(String roleId) {
		HttpSession session = ContextHolderUtils.getSession();
		if (session.getAttributeNames().hasMoreElements()) {
			List<TSRoleFunction> TSRoleFunctionList = (List<TSRoleFunction>)session.getAttribute(roleId);
			if (TSRoleFunctionList != null) {
				return TSRoleFunctionList;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * 获得请求路径
	 * 
	 * @param request
	 * @return
	 */
	public static String getRequestPath(HttpServletRequest request) {
		String requestPath = request.getRequestURI() ;
		
//		if(  StringUtil.isNotEmpty(request.getQueryString() ) ){
//			requestPath = requestPath  + "?" + request.getQueryString();
//		}
		
		if ( StringUtil.isNotEmpty(request.getQueryString()) && requestPath.indexOf("?") > -1 && requestPath.indexOf(".do?") == -1 ) {// 去掉其他参数
			requestPath = requestPath.substring(0, requestPath.indexOf("?"));
		}else if (StringUtil.isNotEmpty(request.getQueryString()) &&  requestPath.indexOf(".do?") > -1  ) {
			//兼容以前的.do方法
			requestPath = requestPath  + "?" + request.getQueryString();
			if( requestPath.indexOf("&") > -1 ){
				requestPath = requestPath.substring(0, requestPath.indexOf("&"));
			}
		}
		
		//requestPath = requestPath.substring(request.getContextPath().length() + 1);// 去掉项目路径
		return requestPath;
	}
	
	/**
	 * 没有登录，跳转到登陆界面，获得登录前的url
	 * @param request
	 * @return
	 */
//	public static String getRedirUrl(HttpServletRequest request){
//		String requestPath = request.getRequestURI() + "?" + request.getQueryString();
//		requestPath = requestPath.substring(request.getContextPath().length() + 1);// 去掉项目路径
//		return requestPath;
//	}

	/**
	 * 获取配置文件参数
	 * 
	 * @param name
	 * @return
	 */
	public static final String getConfigByName(String name) {
		return bundle.getString(name);
	}

	/**
	 * 获取配置文件参数
	 * 
	 * @param name
	 * @return
	 */
	public static final Map<Object, Object> getConfigMap(String path) {
		ResourceBundle bundle = ResourceBundle.getBundle(path);
		Set set = bundle.keySet();
		return oConvertUtils.SetToMap(set);
	}

	
	
	public static String getSysPath() {
		String path = Thread.currentThread().getContextClassLoader().getResource("").toString();
		String temp = path.replaceFirst("file:/", "").replaceFirst("WEB-INF/classes/", "");
		String separator = System.getProperty("file.separator");
		String resultPath = temp.replaceAll("/", separator + separator).replaceAll("%20", " ");
		return resultPath;
	}

	/**
	 * 根据参数前缀获取参数
	 * @param request
	 * @param prefix
	 * @return
	 */
	public static Map getParametersStartingWith(ServletRequest request, String prefix)  
    {  
        Enumeration paramNames = request.getParameterNames();  
        Map params = new TreeMap();  
        if(prefix == null)prefix = "";  
        while(paramNames != null && paramNames.hasMoreElements())   
        {  
            String paramName = (String)paramNames.nextElement();  
            if("".equals(prefix) || paramName.startsWith(prefix))  
            {  
                String unprefixed = paramName.substring(prefix.length());  
                String values[] = request.getParameterValues(paramName);  
                if(values != null && values.length != 0)  
                    if(values.length > 1)  
                        params.put(unprefixed, values);  
                    else  
                        params.put(unprefixed, values[0]);  
            }  
        }  
        return params;  
    }
	
	/**
	 * 获取项目根目录
	 * 
	 * @return
	 */
	public static String getPorjectPath() {
		String nowpath; // 当前tomcat的bin目录的路径 如
						// D:\java\software\apache-tomcat-6.0.14\bin
		String tempdir;
		nowpath = System.getProperty("user.dir");
		tempdir = nowpath.replace("bin", "webapps"); // 把bin 文件夹变到 webapps文件里面
		tempdir += "\\"; // 拼成D:\java\software\apache-tomcat-6.0.14\webapps\sz_pro
		return tempdir;
	}

	public static String getClassPath() {
		String path = Thread.currentThread().getContextClassLoader().getResource("").toString();
		String temp = path.replaceFirst("file:/", "");
		String separator = System.getProperty("file.separator");
		String resultPath = temp.replaceAll("/", separator + separator);
		return resultPath;
	}

	public static String getSystempPath() {
		return System.getProperty("java.io.tmpdir");
	}

	public static String getSeparator() {
		return System.getProperty("file.separator");
	}

	public static String getParameter(String field) {
		HttpServletRequest request = ContextHolderUtils.getRequest();
		return request.getParameter(field);
	}

	/**
	 * 获取数据库类型
	 * 
	 * @return
	 * @throws Exception 
	 */
	public static final String getJdbcUrl() {
		return DBTypeUtil.getDBType().toLowerCase();
	}

    /**
     * 获取随机码的长度
     *
     * @return 随机码的长度
     */
    public static String getRandCodeLength() {
        return bundle.getString("randCodeLength");
    }

    /**
     * 获取随机码的类型
     *
     * @return 随机码的类型
     */
    public static String getRandCodeType() {
        return bundle.getString("randCodeType");
    }
    
    public static String getCmHost() {
        return bundle.getString("CmHost");
    }
    
    public static String getCmHostPort() {
        return bundle.getString("CmHost.port");
    }
    
    public static String getWsHost() {
        return bundle.getString("WsHost");
    }
    
    public static String getWsHostPort() {
        return bundle.getString("WsHost.port");
    }
    /**
     * @return 获取小图高度
     */
    public static String getSmallImageHeight() {
        return bundle.getString("smallimageheight");
    }
    /**
     * @return 获取小图宽度
     */
    public static String getSmallImageWidth() {
        return bundle.getString("smallimagewidth");
    }
	public static void main(String[] args) {
		System.out.println();
		cn.gov.xnc.system.core.util.LogUtil.info(getPorjectPath());
		cn.gov.xnc.system.core.util.LogUtil.info(getSysPath());

	}
}
