package cn.gov.xnc.system.core.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ImageUtil {

	private boolean resizeable = true;
	private int resizebaseline = 0;
	
	private int resizewidth;
	private int resizeheight;
	
	//判断图片是否需要缩小
	private void determineResizeable(int width, int specificationsWidth,
			int height, int specificationsHeight) {
		/*** determine if image can be cropped ***/
		// height
		int y = height - specificationsHeight;
		// width
		int x = width - specificationsWidth;

		if (x < 0 || y < 0) {
			resizeable = false;
		}

		if (x == 0 && y == 0) {
			resizeable = false;
		}
	}

	//判断图片比例方式，竖图、横图、正方图
	private void determineBaseline(int width, int height) {
		/*** determine crop area calculation baseline ***/
		if (width < height) {
			this.setResizebaseline(0);// width
		}
		if (height < width) {
			this.setResizebaseline(1);// height
		}
		if (width == height) {
			this.setResizebaseline(0);
		}
	}

	//计算缩小后图片尺寸
	private void calculateResizeArea(int width, int specificationsWidth,
			int height, int specificationsHeight) {

		resizewidth = width;
		resizeheight = height;

		// crop factor
		double factor = 1;
		if (this.getResizebaseline() == 0 && isResizeable()) {// width
//			factor = new Integer(width).doubleValue()
//					/ new Integer(specificationsWidth).doubleValue();
			
			factor = new Integer(specificationsWidth).doubleValue() 
					/ new Integer(width).doubleValue();
		} else {// height
//			factor = new Integer(height).doubleValue()
//					/ new Integer(specificationsHeight).doubleValue();
			factor = new Integer(specificationsHeight).doubleValue() / 
					new Integer(height).doubleValue();
		}

		//根据规定尺寸重设计算图片宽度缩小图片
		if(factor != 1){
			double w = factor * width;
			double h = factor * height;
			resizewidth = (int) w;
			resizeheight = (int) h;
		}
	}

	/**
	 * 获取缩放图片 并 生成前缀small的图片到员图片路径，返回文件
	 * @param originalFile 原图片
	 * @param specificationsWidth 缩放宽
	 * @param specificationsHeight 缩放高
	 * @param width 
	 * @param height
	 * @return
	 */
	public File getResizeImageFile(File originalImage, int specificationsWidth, int specificationsHeight) throws Exception{
		//获取原图片尺寸
		BufferedImage image = ImageIO.read(originalImage);
		int width = image.getWidth();
		int height = image.getHeight();
		
		//判断是否需要缩放
		determineResizeable(width, specificationsWidth, height, specificationsHeight);
		//计算图片形状：竖图、横图、正方形
		determineBaseline(width, height);
		//计算图片缩放后的尺寸
		calculateResizeArea(width, specificationsWidth, height, specificationsHeight);
		
		BufferedImage resizedImage = resize(image, resizewidth, resizeheight);
		File temp = File.createTempFile("small", ".png", originalImage.getParentFile());
		ImageIO.write(resizedImage, "png", temp);
		
		String originalfilename = originalImage.getName();
		originalfilename = originalfilename.substring(0, originalfilename.lastIndexOf("."));
		File smallimgfile = new File(originalImage.getParent() + "/small" + originalfilename + ".png");
		temp.renameTo(smallimgfile);
		return smallimgfile;
	}
	
	/**
	 * 获取缩放图片，返回 BufferedImage
	 * @param originalImage
	 * @param specificationsWidth
	 * @param specificationsHeight
	 * @return
	 * @throws Exception
	 */
	public BufferedImage getResizeBufferedImage(File originalImage, int specificationsWidth, int specificationsHeight) throws Exception{
		//获取原图片尺寸
		BufferedImage image = ImageIO.read(originalImage);
		int width = image.getWidth();
		int height = image.getHeight();
		
		//判断是否需要缩放
		determineResizeable(width, specificationsWidth, height, specificationsHeight);
		//计算图片形状：竖图、横图、正方形
		determineBaseline(width, height);
		//计算图片缩放后的尺寸
		calculateResizeArea(width, specificationsWidth, height, specificationsHeight);
		
		BufferedImage resizeImage = resize(image, resizewidth, resizeheight);
		return resizeImage;
	}
		
	/**
	 * 根据原图获取切割之后的图片
	 * @param originalFile
	 * @param x1 切割x轴起始坐标
	 * @param y1切割y轴起始坐标
	 * @param width
	 * @param height
	 * @return
	 * @throws Exception
	 */
	public File getClipseImage(File originalFile, int x1, int y1, int width,
			int height) throws Exception {
		BufferedImage image = ImageIO.read(originalFile);
		BufferedImage out = image.getSubimage(x1, y1, width, height);
		File tempFile = File.createTempFile("temp", ".jpg");
		tempFile.deleteOnExit();
		ImageIO.write(out, "jpg", tempFile);
		return tempFile;
	}
	
	/**
	 * 重新绘制图片大小并生成前缀指定前缀的图片到原图片路径
	 * @param BufferedImage image
	 * @param width
	 * @param height
	 * @return
	 * @throws Exception
	 */
	public File resizeImage(BufferedImage image, int width, int height,String prefix)
			throws Exception {
		// BufferedImage readImage = ImageIO.read(image);

		BufferedImage resizedImage = resize(image, width, height);
		File temp = File.createTempFile(prefix, ".png");
		ImageIO.write(resizedImage, "png", temp);
		return temp;
	}
	
	/**
	 * 重新绘制图片大小
	 * @param image
	 * @param width
	 * @param height
	 * @return
	 */
	public BufferedImage resize(BufferedImage image, int width, int height) {
		int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image
				.getType();
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(image, 0, 0, width, height, null);
		g.dispose();
		return resizedImage;
	}
	
	/**
	 * 柔化照片（可以理解为稍微模糊，是图片文字或者线条更加柔和，跟锐化相反）
	 * @param image
	 * @return
	 */
	public BufferedImage blurImage(BufferedImage image) {
		float ninth = 1.0f / 9.0f;
		float[] blurKernel = { ninth, ninth, ninth, ninth, ninth, ninth, ninth,
				ninth, ninth };
		Map<Key, Object> map = new HashMap<Key, Object>();
		map.put(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		map.put(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		map.put(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		RenderingHints hints = new RenderingHints(map);
		BufferedImageOp op = new ConvolveOp(new Kernel(3, 3, blurKernel),
				ConvolveOp.EDGE_NO_OP, hints);
		return op.filter(image, null);
	}

	/**
	 * 图片添加水印
	 * @param originalImage
	 * @param markContent
	 * @param markColor
	 * @param fontSize
	 */
	public void getMarkImage(File originalImage, String markContent, Color markColor, int fontSize) {  
        try {  
            // 读取原图片信息  
        	BufferedImage srcImg = ImageIO.read(originalImage);  
            int srcImgWidth = srcImg.getWidth();  
            int srcImgHeight = srcImg.getHeight();  
            // 创建水印图片副本  
            BufferedImage markImage = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);  
            Graphics2D g = markImage.createGraphics();  
            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);  
            Font font = new Font("宋体", Font.PLAIN, fontSize);    
            g.setColor(markColor); //根据图片的背景设置水印颜色  
            g.setFont(font);  
            //设置水印位置
            int x = srcImgWidth - getWatermarkLength(markContent, g) - 10;  
            int y = srcImgHeight - 10;  
            g.drawString(markContent, x, y);  
            g.dispose();  
            // 输出图片到原路径，图片名称增加前缀mark 
            File temp = File.createTempFile("mark", ".png", originalImage.getParentFile());
    		ImageIO.write(markImage, "png", temp);
    		
            System.out.println("fsdfdsfdsfds====");
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
      
    /** 
     * 获取水印文字总长度 
     * @param markContent 水印的文字 
     * @param g 
     * @return 水印文字总长度 
     */  
    private int getWatermarkLength(String markContent, Graphics2D g) {  
        return g.getFontMetrics(g.getFont()).charsWidth(markContent.toCharArray(), 0, markContent.length());  
    }  
    
	public boolean isResizeable() {
		return resizeable;
	}

	public void setResizeable(boolean resizeable) {
		this.resizeable = resizeable;
	}

	public int getResizebaseline() {
		return resizebaseline;
	}

	public void setResizebaseline(int resizebaseline) {
		this.resizebaseline = resizebaseline;
	}

	public int getResizewidth() {
		return resizewidth;
	}

	public void setResizewidth(int resizewidth) {
		this.resizewidth = resizewidth;
	}

	public int getResizeheight() {
		return resizeheight;
	}

	public void setResizeheight(int resizeheight) {
		this.resizeheight = resizeheight;
	}
}
