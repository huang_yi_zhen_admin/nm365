package cn.gov.xnc.system.sso.client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.HttpGetPost;
import cn.gov.xnc.system.core.util.HttpRequestHelper;
import cn.gov.xnc.system.core.util.LogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.Client;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;


public class SSOHelper{

	private static final String SSO_SERVER_URI = ResourceUtil.getConfigByName("SSO_SERVER_URI");
	
	/**
	 * 登录访问验证
	 * @param req
	 * @return
	 */
	public static TSUser loginValidate(HttpServletRequest req){
		TSUser user = null;
		String token = TokenHelper.getTokenFromCache();
		if(StringUtil.isEmpty(token)){
			String ticket = TokenHelper.getTicketFromHttpRequest(req);
			if(null != ticket){
				token = TokenHelper.tokenGenerator(ticket);
			}
			
		}
		
		
		if(StringUtil.isNotEmpty(token)){
			
			try {
				TokenHelper.writeTokenEncache(token);
			} catch (Exception e) {
				LogUtil.error("-----登录认证错误：\r\n",e);
			}
			
			
			StringBuffer urlBuf = new StringBuffer();
			urlBuf.append(SSOHelper.SSO_SERVER_URI).append("/loginController/loginValidattion?token=").append(token)
					.append("&redirectUrl=").append(HttpRequestHelper.getHttpRequestFullPath());
			
			String jsonStr = HttpGetPost.sendGet(urlBuf.toString());
			
			if(null != jsonStr){
				JSONObject json = JSONObject.parseObject(jsonStr);
				if(json.getBooleanValue("success")){
					if(null != json.get("obj")){
						json = JSONObject.parseObject(json.get("obj").toString());
						user = new TSUser();
						user.setId(json.getString("userId"));
					}
				}
			}
		}
		
		return user;
	}
	
	/**
	 * 初始化会话信息
	 * @param req
	 * @param user
	 */
	public static void initClient(HttpServletRequest req, TSUser user){
		HttpSession session = ContextHolderUtils.getSession();
		Client client = ClientManager.getInstance().getClient(session.getId());
		
		if(client == null){
			client = new Client();
	        client.setLogindatetime(DateUtils.getDate());
	        client.setUser(user);
	        
	        ClientManager.getInstance().removeClinet(session.getId());
	        ClientManager.getInstance().addClinet(session.getId(), client);
		}
	}
	/**跳转单点登录*/
	public static void redirect2Login(HttpServletResponse resp) throws Exception{
		
		StringBuffer urlBuf = new StringBuffer();
		urlBuf.append(SSOHelper.SSO_SERVER_URI).append("/loginController/login?").append("redirectUrl=").append(HttpRequestHelper.getHttpRequestFullPath());
		
		resp.sendRedirect(urlBuf.toString());
	}
	/**
	 * 当单点登录系统的用户有注销行为时，接收单点登录通知注销服务
	 */
	public static void logoutBySso(){
		/**清除访问令牌 */
		TokenHelper.removeTokenInCache();
		
		/**清除会话信息*/
		HttpSession session = ContextHolderUtils.getSession();
		String sessionid = session.getId();
		ClientManager.getInstance().removeClinet( sessionid );
		
	}
	
	/**
	 * 通知单点登录用户退出
	 */
	public static void sendLogout2Sso(){
		/**清除访问令牌 */
		TokenHelper.removeTokenInCache();
		
		/**清除会话信息*/
		HttpSession session = ContextHolderUtils.getSession();
		String sessionid = session.getId();
		ClientManager.getInstance().removeClinet( sessionid );
		
		/**通知单点登录用户退出*/
		StringBuffer urlBuf = new StringBuffer();
		urlBuf.append(SSOHelper.SSO_SERVER_URI).append("/loginController/logout");
		
		StringBuffer param = new StringBuffer();
		param.append("redirectUrl=").append(HttpRequestHelper.getHttpRequestFullPath());
		
		HttpGetPost.sendPost(urlBuf.toString(), param.toString());
	}
	
	/**
	 * 注册
	 */
	public static void sendRegister2Sso(){
		StringBuffer urlBuf = new StringBuffer();
		urlBuf.append(SSOHelper.SSO_SERVER_URI).append("/loginController/register");
		
		StringBuffer param = new StringBuffer();
		param.append("redirectUrl=").append(HttpRequestHelper.getHttpRequestFullPath());
		
		HttpGetPost.sendPost(urlBuf.toString(), param.toString());
	}
}
