package cn.gov.xnc.system.sso.client;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cn.gov.xnc.system.core.encache.EncacheUtil;
import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.HttpRequestHelper;
import cn.gov.xnc.system.core.util.LogUtil;
import cn.gov.xnc.system.core.util.PasswordUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;

public class TokenHelper {

	public static final String TOKEN_ENCACHE_NAME = "TOKEN_ENCACHE_NAME";
	
	/**
	 * 根据访问票据和加密盐生成访问令牌
	 * @param ticket
	 * @return String token
	 */
	public static final String tokenGenerator(String ticket){
		
		String tokenkey = ticket + ResourceUtil.getConfigByName("SSO_SERVER_SALT");
		
		return PasswordUtil.md5( tokenkey );
	};
	
	/**
	 * TOKEN 写缓存
	 * @param token
	 * @throws Exception
	 */
	public static void writeTokenEncache(String token) throws Exception{
		HttpSession session = ContextHolderUtils.getSession();
		String sessionid = session.getId();
		if(StringUtil.isNotEmpty(token) && StringUtil.isNotEmpty(sessionid)){
			String tokenStr = (String) EncacheUtil.get(TokenHelper.TOKEN_ENCACHE_NAME, sessionid);
			
			if(null == tokenStr 
					|| (StringUtil.isNotEmpty(tokenStr) && !token.equals(tokenStr))){
				
				EncacheUtil.remove(TokenHelper.TOKEN_ENCACHE_NAME, sessionid);
				EncacheUtil.put(TokenHelper.TOKEN_ENCACHE_NAME, sessionid, token);
				
			}
			
		}else{
			throw new Exception("令牌或者会话ID不能为空！");
		}
	}
	/**
	 * 从缓存中获取访问令牌
	 * @return String token
	 */
	public static String getTokenFromCache(){
		HttpSession session = ContextHolderUtils.getSession();
		String sessionid = session.getId();
		
		String token = (String) EncacheUtil.get(TokenHelper.TOKEN_ENCACHE_NAME, sessionid);
		
		return token;
	};
	
	/**
	 * 清除缓存中的访问令牌
	 * @return String token
	 */
	public static void removeTokenInCache(){
		HttpSession session = ContextHolderUtils.getSession();
		String sessionid = session.getId();
		
		EncacheUtil.remove(TokenHelper.TOKEN_ENCACHE_NAME, sessionid);
	};
	
	public static String getTicketFromHttpRequest(HttpServletRequest req){
		String ticket = null;
		try {
			//根据传参获取ticket
			ticket = ResourceUtil.getParameter("ticket");
			//从http请求头部获取
			if( StringUtil.isEmpty(ticket) ){
				Map<String, String> map = HttpRequestHelper.getRequestHeadersInfo(req);
				
				String keyVal = (String) map.get("ticket");
				if(StringUtil.isNotEmpty(keyVal)){
					ticket = keyVal;
				}
				
			}
			
			//从cookie获取
			if( StringUtil.isEmpty(ticket) ){
				Cookie cookie = HttpRequestHelper.getCookieByName(req, "ticket");
				
				String keyVal = cookie.getValue();
				if(StringUtil.isNotEmpty(keyVal)){
					ticket = keyVal;
				}
				
			}
			
		} catch (Exception e) {
			LogUtil.error("--------获取访问票据失败: \r\n", e);
		}
				
		return ticket;
	}
	
}
