package cn.gov.xnc.system.web.system.controller.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

//import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.message.service.impl.MessageTemplateServiceImpl;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.extend.datasource.DataSourceContextHolder;
import cn.gov.xnc.system.core.extend.datasource.DataSourceType;
import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.NumberComparator;
import cn.gov.xnc.system.core.util.PasswordUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.sso.client.SSOHelper;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.Client;
import cn.gov.xnc.system.web.system.pojo.base.CompanyIndustryEntity;
import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSType;
import cn.gov.xnc.system.web.system.pojo.base.TSTypegroup;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.CompanyService;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserAccessServiceI;
import cn.gov.xnc.system.web.system.service.UserService;
import cn.gov.xnc.system.web.system.servlet.RandCodeImageServlet;
import cn.gov.xnc.system.web.system.servlet.RandCodeMosServlet;


/**
 * 登陆初始化控制器
 * @author ZERO
 * 
 */
@Scope("prototype")
@Controller
@RequestMapping("/loginController")
public class LoginController extends BaseController{
	//private Logger log = Logger.getLogger(LoginController.class);
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserAccessServiceI userAccessService;
	@Autowired
	private CompanyService companyService;
	
	
	private String message = null;
	
	/**
	 * 检查用户名称
	 * 
	 * @param user
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/getUserFunctionIdList")
	@ResponseBody
	public AjaxJson getUserFunctionIdList(TSUser user, HttpServletRequest req, HttpServletResponse resp) {
		AjaxJson j = new AjaxJson();
		
		TSUser sessionuser = ResourceUtil.getSessionUserName();
		if( null == sessionuser ){
			j.setSuccess(false);
			j.setMsg("登陆信息有误");
			return j;
		}
		Map<String, TSFunction> functionMap = getUserFunction(sessionuser);
		List<String> funcIdList = new ArrayList<String>();
		for (Map.Entry<String, TSFunction> entry : functionMap.entrySet()) {  
			
			funcIdList.add(entry.getKey());
		    //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());  
		}  
		
		j.setObj(funcIdList);
		j.setSuccess(true);
		j.setMsg("查询成功");
		
		return j;
	}
	
	
	/**
	 * 检查用户名称
	 * 
	 * @param user
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/checkuser")
	@ResponseBody
	public AjaxJson checkuser(TSUser user, HttpServletRequest req, HttpServletResponse resp) {
	
		HttpSession session = ContextHolderUtils.getSession();
		DataSourceContextHolder.setDataSourceType(DataSourceType.dataSource_project);
		String loginType = ResourceUtil.getParameter("type");
		AjaxJson j = new AjaxJson();
        String randCode = req.getParameter("randCode");
        
        //boolean randCodeB =false ;// 去除验证码测试
        
        if (  StringUtils.isEmpty(randCode) && !"m".equals(loginType)) {
            j.setMsg("请输入验证码");
            j.setSuccess(false);
        } else if (   !"m".equals(loginType) && !randCode.equalsIgnoreCase(String.valueOf(session.getAttribute("randCode")))) {
            j.setMsg("验证码错误！");
            j.setSuccess(false);
        } else {
        	
        	
            //验证该域名下的用户信息    
        	String url =  req.getScheme() +"://" + req.getServerName();
        	//检测输入的域名是否存在不存在跳出yuming.jsp 
			TSCompany company = companyService.companyByUrl(url);
			if( company == null   ){
				j.setMsg("域名不正确，请重新输出正确的域名后进行登陆！");
                j.setSuccess(false);
			}
			
			
        	TSUser u = null ;
			try {
				u = userService.checkUserExits(user, company);
			} catch (Exception e) {
				
				e.printStackTrace();
			} 
    		
                
                
                if(u == null) {
                    j.setMsg("用户名或密码错误!");
                    j.setSuccess(false);
                    return j;
                }
                
                if(u.getStatus()==4){
               	 j.setMsg("待审核账号请耐心等待站方开通！");
                    j.setSuccess(false);
                    return j;
               }
               
                if(u.getStatus()==3){
                	 j.setMsg("您的账户已经过期");
                     j.setSuccess(false);
                     return j;
                }
                if(u.getStatus()==2){
                	 j.setMsg("您的账户已经被冻结!");
                     j.setSuccess(false);
                     return j;
                }

                if(u.getStatus()!=1){
               	    j.setMsg("异常账户请联系站方处理!");
                    j.setSuccess(false);
                    return j;
               }
                
                u = userService.getEntity(TSUser.class, u.getId());
               
                if( !"1".equals(u.getCompany().getCompanyreviewed().getState())   ){
               	 	j.setMsg("企业审核信息中，请耐心等待");
                    j.setSuccess(false);
                    return j;
                }

                if("3".equals(u.getType())){
               	    j.setMsg("您好，3秒后将为您跳转到采购商登录界面");
                    j.setSuccess(false);
                    j.setObj(u.getCompany().getCompanyplatform().getWebsite());
                    return j;
               }
                if (u != null && u.getStatus()!=0) {
                	
                    if (true) {
                        message = "用户: " + user.getUserName() +  "登录成功";
                        
                        try {
                        	
                        	userAccessService.initClient(u, session, req, resp);
                        	
						} catch (Exception e) {
							message = e.getMessage();
							
							j.setSuccess(false);
							j.setMsg(message);
						}
                        
                        
                        /*Client client = new Client();
                        client.setLogindatetime(new Date());
                        client.setUser(u);
                        ClientManager.getInstance().addClinet(session.getId(), client);*/
                        // 添加登陆日志
                        systemService.addLog(message, Globals.Log_Type_LOGIN,Globals.Log_Leavel_INFO);
                    } 
                } else {
                    j.setMsg("用户名或密码错误!");
                    j.setSuccess(false);
                }
        }
		return j;
	}

	/**
	 * 用户登录
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/login" , method = RequestMethod.GET )
	public String login(ModelMap modelMap,HttpServletRequest request ) {
		
				DataSourceContextHolder.setDataSourceType(DataSourceType.dataSource_project);
				TSUser user = ResourceUtil.getSessionUserName();
				//必须用公司
				String roles = "";
				//获取访问域名
				String url =  request.getScheme() +"://" + request.getServerName();
				TSCompany company = companyService.companyByUrl(url); 
						
				//检测输入的域名是否存在不存在跳出yuming.jsp 
				if( url.indexOf("www.nongmao365.com") == -1  &&  company == null   ){
					return "login/yuming";
				}
				
				companystyle(url,company, request);

				String userAgent = request.getHeader("user-agent");
				boolean isMobile = checkMobile(userAgent);
				
				try {
					
					if (user != null) {
						List<TSRoleUser> rUsers = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());
						for (TSRoleUser ru : rUsers) {
							TSRole role = ru.getTSRole();
							roles += role.getRoleName() + ",";
							Client client = ClientManager.getInstance().getClient(ContextHolderUtils.getSession().getId());
							client.setTsRole(role);
						}
						
						if (roles.length() > 0) {
							roles = roles.substring(0, roles.length() - 1);
						}
						/**
						 * 把权限的datascope数据权限给保存下来  以后查询的时候过滤条件需要用到
						 */
			            modelMap.put("roleName", roles);
			            modelMap.put("logo", company.getCompanyplatform().getLogourl());
			            modelMap.put("user", user);
			            TSUser entyUser = systemService.findUniqueByProperty(TSUser.class, "id", user.getId());
			            modelMap.put("company", entyUser.getCompany());
			            modelMap.put("platform", null != entyUser.getCompany() ? entyUser.getCompany().getCompanyplatform() : null);
			            request.getSession().setAttribute("CKFinder_UserRole", "admin");
						// 默认风格 风格页面
						String indexStyle = "shortcut";

						if (!"3".equals(user.getType()) && StringUtils.isNotEmpty(indexStyle)&& indexStyle.equalsIgnoreCase("shortcut")) {
							menu(request,user);
							
							if(isMobile){
								return "redirect:/m/home"; 
							}
							return "main/main";
						}else if("3".equals(user.getType())){//非供货端客户跳到注册页面
							//清除会话信息
							try {
								userAccessService.removeClient(request);
							} catch (Exception e2) {
								// TODO: handle exception
							}
							return "redirect:/loginController/register"; 
						}
						menu(request,user);
						return "main/main";
					} else {
						
						
						/*if(isMobile){
							CompanyPlatformEntity companyplatForm = StringUtil.isNotEmpty(company) ? company.getCompanyplatform() : null;
							//获取用户的公司信息
							String testUrl = url + request.getRequestURI() + "?" + request.getQueryString();;
							if(	(matchMobileShareUrl(testUrl) && null != companyplatForm 
										&& StringUtil.isNotEmpty(companyplatForm.getShareprice())
										&& !"0".equals(companyplatForm.getShareprice()))){
								
								return "redirect:/shoppingController/pindex.do"; 
							}
							return "shop/page/login";
								
						}*/
						
						
						return "login/login";
					}
					
				} catch (Exception e) {
					//清除会话信息
					try {
						userAccessService.removeClient(request);
					} catch (Exception e2) {
						// TODO: handle exception
					}
					
					
				}
				
				return "redirect:/webpage/errorpage/500.html";
	}
	
	/**
	 *公司首页样式输出 
	 */
	public void companystyle(String url , TSCompany company, HttpServletRequest request ){
		//获取公司对应
		

		String companyLoginBanner="attach/system/default_bg.jpg";
		String companyLoginlogo ="attach/system/default_logo.png";
		String companyLoginTitle="天天农贸";
		
		if(company != null && company.getCompanyplatform() !=null  && StringUtils.isNotEmpty(company.getCompanyplatform().getCompanypicture()) ){
			companyLoginBanner = company.getCompanyplatform().getCompanypicture();
		}
		if(company != null && company.getCompanyplatform() !=null  && StringUtils.isNotEmpty(company.getCompanyplatform().getLogourl()) ){
			companyLoginlogo = company.getCompanyplatform().getLogourl();
		}
		if(company != null && company.getCompanyplatform() !=null  && StringUtils.isNotEmpty(company.getCompanyplatform().getPlatformname()) ){
			companyLoginTitle = company.getCompanyplatform().getPlatformname();
		}
		
		

		if( url.indexOf("www.nongmao365.com") > -1  ){
			request.getSession().setAttribute("zhuche", "1");//注册成为供应商
		}else {
			request.getSession().setAttribute("zhuche", "2");//开发注册成功该域名下的采购商
		}

		
		request.getSession().setAttribute("companyLoginBanner", companyLoginBanner);
		request.getSession().setAttribute("loginLogo", companyLoginlogo);
		request.getSession().setAttribute("companyLoginTitle", companyLoginTitle);
		
	}
	
	/**
	 * 退出系统
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "logout")
	public void logout(HttpServletRequest request) {

		//通知单点登录系统注销服务
		SSOHelper.sendLogout2Sso();
		
	}

	/**
	 * 注册页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "register")
	public String register(HttpServletRequest request) {
		/**
		 * 用户类型编码
		 */

		List<TSType> list = (List<TSType>) TSTypegroup.allTypes.get("users_type".toLowerCase());
		
		if( list != null && list.size() > 0 ){
			for(int i = 0 ; i < list.size() ; i++){
				TSType TSType = list.get(i);
				if( "3".equals(TSType.getTypecode()) ){
					list.remove(i);
				}
			}
		}
		
			request.setAttribute("userTypeList", list);
		
		//获取注册类型所属行业信息	
			List<CompanyIndustryEntity> companyIndustryList =  systemService.getList(CompanyIndustryEntity.class);	
			request.setAttribute("companyIndustryList", companyIndustryList);
		return "login/register";
	}
	

	/**
	 * 找回密码
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "renewpassword")
	public String renewPassword(HttpServletRequest request) {
		//根据域名对应的采购信息输出到页面，公司信息展示
				String url =  request.getScheme() +"://" + request.getServerName();
				TSCompany company = companyService.companyByUrl(url);
				//检测输入的域名是否存在不存在跳出yuming.jsp 
				if( url.indexOf("www.nongmao365.com") == -1 && company == null   ){
					//return "common/yuming";
				}
				
				companystyle(url,company, request);
		return "login/renewpassword";
	}
	
	
	
	/**
	 * 找回密码发生短信验证码
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "randCodePassword")
	@ResponseBody
	public AjaxJson randCodePassword(HttpServletRequest request,  HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		String userName = request.getParameter("userName");
		String url =  request.getScheme() +"://" + request.getServerName();
		String ty =request.getParameter("ty");
		
		TSCompany company = companyService.companyByUrl(url); 
		
		TSUser user = null;
		try {
			 	CriteriaQuery cq = new CriteriaQuery(TSUser.class);
			 		cq.eq("userName", userName);
			 		cq.eq("company", company);
			 	user = (TSUser) systemService.getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色

		} catch (Exception e) {
			 e.printStackTrace(); 
		}
		
		if(null == user){
       	 	j.setMsg("帐号不存在！");
            j.setSuccess(false);
            return j;
        }
		
		if(user.getStatus()!=1){
        	 j.setMsg("异常账户请联系站方处理!");
             j.setSuccess(false);
             return j;
         }
		
		if( "2".equals(ty)){//验证短信密码的正确性
			String randCode = request.getParameter("randCode");
			HttpSession session = ContextHolderUtils.getSession();
	        if (StringUtils.isEmpty(randCode) ) {
	            j.setMsg("请输入验证码");
	            j.setSuccess(false);
	        } else  if (!randCode.equalsIgnoreCase(String.valueOf(session.getAttribute("randCodeMos")))) {//短信验证码randCodeMos
	            j.setMsg("验证码错误！");				            
	            j.setSuccess(false);
		    }else {
		    	//发送重置密码到对应的手机
		    	if(  company != null && user != null && user.getCompany().getId().equals(company.getId())   ){
		    		//重置用户密码
		    		//生成随机的密码 2位字母加4位数字
					RandCodeImageServlet RandCodeImageServlet = new RandCodeImageServlet();     
					String passwordRandCode  =  RandCodeImageServlet.passwordRandCode();
					user.setPassword(PasswordUtil.md5(passwordRandCode));
					systemService.saveOrUpdate(user);//更新用户密码信息
					 String msg ="尊敬的用户你好，你的登陆密码已经重置为："+passwordRandCode+"，请勿泄露密码信息登陆后请及时修改你的登陆密码！";
						System.out.println(msg);
					//发送短信验证
					 MessageTemplateServiceI messageTemplateServiceI = new MessageTemplateServiceImpl();
					 	
					if( "3".equals(user.getType())){
						//客户的话直接使用账号为作为手机号发送验证码
						  messageTemplateServiceI.sendSMSTTMM("", "", user.getUserName(), "天天农贸", msg);
					}else {
						if(StringUtils.isNotEmpty(user.getMobilephone())){
						  messageTemplateServiceI.sendSMSTTMM("", "", user.getMobilephone(), "天天农贸", msg);
						}
					}
				}
		    }

		}else {
			
				if(  company != null && user != null && user.getCompany().getId().equals(company.getId())   ){
					if( "3".equals(user.getType())){
						//客户的话直接使用账号为作为手机号发送验证码
						RandCodeMosServlet RandCodeMosServlet  = new RandCodeMosServlet();
						//发送短信验证码
						RandCodeMosServlet.randCodePassword(user.getUserName(), request, response);
					}else {
						if(StringUtils.isNotEmpty(user.getMobilephone())){
							RandCodeMosServlet RandCodeMosServlet  = new RandCodeMosServlet();
							//发送短信验证码
							RandCodeMosServlet.randCodePassword(user.getMobilephone(), request, response);
						}else {
							j.setMsg("账号信息未设置手机号，请联系站方管理员！");
							j.setSuccess(false);
						}
					}
				}else {
					j.setMsg("账号信息未存在，请确认后从新输入！");
					j.setSuccess(false);
				}
		}
		return j;
	}
	
	
	
	
	
	/**
	 * 客户自主申请注册
	 * 
	 * @return
	 */
	@RequestMapping(value = "registerClients")
	public String registerClients(HttpServletRequest request) {
		
		String url =  request.getScheme() +"://" + request.getServerName();
		TSCompany company = companyService.companyByUrl(url);
		CompanyPlatformEntity platform = company.getCompanyplatform();
		companystyle(url,company, request);
		request.setAttribute("company", company);
		request.setAttribute("platform", platform);
		
		return "login/registerClients";
	}
	
	
	
	
//	/**
//	 * 菜单跳转
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "left")
//	public ModelAndView left(HttpServletRequest request) {
//		TSUser user = ResourceUtil.getSessionUserName();
//		HttpSession session = ContextHolderUtils.getSession();
//        ModelAndView modelAndView = new ModelAndView();
//		// 登陆者的权限
//		if (user.getId() == null) {
//			session.removeAttribute(Globals.USER_SESSION);
//            modelAndView.setView(new RedirectView("loginController.do?login"));
//		}else{
//           /* List<TSConfig> configs = userService.loadAll(TSConfig.class);
//            for (TSConfig tsConfig : configs) {
//                request.setAttribute(tsConfig.getCode(), tsConfig.getContents());
//            }*/
//            modelAndView.setViewName("main/left");
//            request.setAttribute("menuMap", getFunctionMap(user));
//        }
//		return modelAndView;
//	}

	
	
	/**
	 * 构建用户菜单列表
	 * 
	 * @param user
	 * @return
	 **/
	private void menu( HttpServletRequest request ,  TSUser user  ) {
		
		shortcut_top(request , user);
		
	}
	
	
//	/**
//	 * @Description: bootstrap头部菜单请求
//	 * @throws
//	 */
//	private void top(HttpServletRequest request) {
//		TSUser user = ResourceUtil.getSessionUserName();
//		HttpSession session = ContextHolderUtils.getSession();
//		// 登陆者的权限
//		if (user.getId() == null) {
//			session.removeAttribute(Globals.USER_SESSION);
//		}
//		request.setAttribute("menuMap", getFunctionMap(user));
//		
//		
//	}
	
	/**
	 * @Description: shortcut头部菜单请求
	 * @throws
	 */
	private void shortcut_top(HttpServletRequest request , TSUser user ) {
		Map<Integer, List<TSFunction>> allMenu = getFunctionMap(user);
		
		if(null != allMenu && !allMenu.isEmpty()){
			
			List<TSFunction> parentMenuList = allMenu.get(0);//所有父级菜单
			List<TSFunction> childMenuList = allMenu.get(1);//所有子菜单
			
			//重新整合菜单，不让hibernate根据关联从数据库取数据，避免产生不用的菜单
			for (TSFunction tsFunction : parentMenuList) {
				List<TSFunction> childFunction = new ArrayList<TSFunction>();
				tsFunction.setTSFunctions(childFunction);//清空子菜单
				
				for (TSFunction childMenuFunction : childMenuList) {
					TSFunction pMenuFunc = childMenuFunction.getTSFunction();
					if(null != pMenuFunc && StringUtil.isNotEmpty(pMenuFunc.getId()) && pMenuFunc.getId().equals(tsFunction.getId())){
						childFunction.add(childMenuFunction);
					}
				}
			}
			
			
			if( parentMenuList != null && parentMenuList.size() > 0){
				request.setAttribute("menuTopList", parentMenuList);
			}
		}
		
		
		
	}
	
	
	/**
	 * 获取权限的map
	 * 
	 * @param user
	 * @return
	 */
	private Map<Integer, List<TSFunction>> getFunctionMap(TSUser user) {
		Map<Integer, List<TSFunction>> functionMap = new HashMap<Integer, List<TSFunction>>();
		Map<String, TSFunction> loginActionlist = getUserFunction(user);
		if (loginActionlist.size() > 0) {
			Collection<TSFunction> allFunctions = loginActionlist.values();
			
			for (TSFunction function : allFunctions) {
				if (!functionMap.containsKey(function.getFunctionLevel() + 0)) {
					functionMap.put(function.getFunctionLevel() + 0,new ArrayList<TSFunction>());
				}
				functionMap.get(function.getFunctionLevel() + 0).add(function);
			}
			// 菜单栏排序
			Collection<List<TSFunction>> c = functionMap.values();
			for (List<TSFunction> list : c) {
				Collections.sort(list, new NumberComparator());
			}
		}
		return functionMap;
	}
	
	
	/**
	 * 获取用户菜单列表
	 * 
	 * @param user
	 * @return
	 */
	private Map<String, TSFunction> getUserFunction(TSUser user) {
		HttpSession session = ContextHolderUtils.getSession();
		Client client = ClientManager.getInstance().getClient(session.getId());
		if (client.getFunctions() == null || client.getFunctions().size() == 0) {
			Map<String, TSFunction> loginActionlist = new HashMap<String, TSFunction>();
			
			List<TSRoleUser> rUsers =systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());
			List<TSRole> TSRoleS =new ArrayList<TSRole>();
			for (TSRoleUser ru : rUsers) {
				if( ru.getTSRole() != null && StringUtil.isNotEmpty(ru.getTSRole().getId())){
					TSRole role = ru.getTSRole();
					TSRoleS.add(role);
	        		List<TSRoleFunction> roleFunctionList = systemService.findByProperty(TSRoleFunction.class, "TSRole.id",	role.getId());
					for (TSRoleFunction roleFunction : roleFunctionList) {
						TSFunction function = systemService.getEntity(TSFunction.class, roleFunction.getTSFunction().getId());
						
						loginActionlist.put(function.getId(), function);
					}
				}
				
			}
			
			client.setFunctions(loginActionlist);
		}
		return client.getFunctions();
	}
	
	
	/**
	 * 无权限页面提示跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "noAuth")
	public ModelAndView noAuth(HttpServletRequest request) {
		return new ModelAndView("common/noAuth");
	}
	
	
	
	

	
	
	

	
	
//	/**
//	 * @Title: top
//	 * @author:gaofeng
//	 * @Description: shortcut头部菜单一级菜单列表，并将其用ajax传到页面，实现动态控制一级菜单列表
//	 * @return AjaxJson
//	 * @throws
//	 */
//    @RequestMapping(value = "primaryMenu")
//    @ResponseBody
//	public String getPrimaryMenu() {
//		List<TSFunction> primaryMenu = getFunctionMap(ResourceUtil.getSessionUserName()).get(0);
//        String floor = "";
//        int i = 1;
//        if(primaryMenu!=null&&primaryMenu.size()>0){
//	        for (TSFunction function : primaryMenu) {
//	        	
//	        	if(i==1){
//	        		floor +=" <li id='li"+i+"' onclick='on("+i+");' class='on'><a href='javascript:void(0);'><span>"+function.getFunctionName()+"</span></a></li>";
//	        	}else{
//	        		floor +=" <li id='li"+i+"' onclick='on("+i+");' ><a href='javascript:void(0);'><span>"+function.getFunctionName()+"</span></a></li>";
//	        	}
//	        	i= i+1;
//	        	
//	        }
//        }
//		return floor;
//	}
	
    /**
     * 判断移动端或者pc端登录
     * @param userAgent
     * @return
     */
    public  boolean checkMobile(String userAgent){    
    	// b 是单词边界(连着的两个(字母字符 与 非字母字符) 之间的逻辑上的间隔),    
        // 字符串在编译时会被转码一次,所以是 "\b"    
        // B 是单词内部逻辑间隔(连着的两个字母字符之间的逻辑上的间隔)    
        /*String phoneReg = "\b(ip(hone|od)|android|opera m(ob|in)i"    
                +"|windows (phone|ce)|blackberry"    
                +"|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp"    
                +"|laystation portable)|nokia|fennec|htc[-_]"    
                +"|mobile|up.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b";    
        String tableReg = "\b(ipad|tablet|(Nexus 7)|up.browser"    
                +"|[1-4][0-9]{2}x[1-4][0-9]{2})\b";    
          
        //移动设备正则匹配：手机端、平板  
        Pattern phonePat = Pattern.compile(phoneReg, Pattern.CASE_INSENSITIVE);    
        Pattern tablePat = Pattern.compile(tableReg, Pattern.CASE_INSENSITIVE);    
        
        if(null == userAgent){    
            userAgent = "";    
        }    
        // 匹配    
        Matcher matcherPhone = phonePat.matcher(userAgent);    
        Matcher matcherTable = tablePat.matcher(userAgent);    
        if(matcherPhone.find() || matcherTable.find()){    
            return true;    
        } else {    
            return false;    
        }    */
        
        
        /**
         * android : 所有android设备
         * mac os : iphone手机
         * windows phone:Nokia等windows系统的手机
         */
        String[] deviceArray = new String[]{"android","iPhone","windows phone"};
        if(null != userAgent){
        	userAgent = userAgent.toLowerCase();
        	for(int i=0;i<deviceArray.length;i++){
                if(userAgent.indexOf(deviceArray[i])>0){
                    return true;
                }
            }
        }
        /*mac os*/
        String phoneReg = "AppleWebKit.*Mobile.*";
        //苹果移动设备  
        Pattern iphonePat = Pattern.compile(phoneReg, Pattern.CASE_INSENSITIVE);  
        // 匹配    
        Matcher matcheriPhone = iphonePat.matcher(userAgent);    
        if(matcheriPhone.find()){    
            return true;    
        } 
        		
        return false;
    }  
	/**
	 * 验证访问地址是否符合商城分享招商地址
	 * @param testUrl
	 * @return
	 */
    private boolean matchMobileShareUrl(String testUrl){
    	boolean isMatch = false;
    	if(StringUtil.isNotEmpty(testUrl)){
    		
    			/**
    			 * 移动端满足直接跳转的地址
    			 * 也即是session验证需要排除的地址
    			 */
    			List<String> mExcludeUrl = new ArrayList<String>();
    			mExcludeUrl.add("http:\\S+ywwl.com$");
    			mExcludeUrl.add("http:\\S+ywwl.com/loginController.do$");
    			mExcludeUrl.add("http:\\S+ywwl.com/loginController.do?login$");
    			mExcludeUrl.add("http:\\S+order.com$");
    			mExcludeUrl.add("http:\\S+order.com/loginController.do$");
    			mExcludeUrl.add("http:\\S+order.com/loginController.do?login$");
    			mExcludeUrl.add("http:\\S+nongmao365.com$");
    			mExcludeUrl.add("http:\\S+nongmao365.com/loginController.do$");
    			mExcludeUrl.add("http:\\S+nongmao365.com/loginController.do?login$");
    			mExcludeUrl.add("http:\\S+shoppingController/pindex.do$");
    			mExcludeUrl.add("http:\\S+shoppingController/pproductlist.do");
    			mExcludeUrl.add("http:\\S+shoppingController/pshowProduct.do?id=[0-9a-z]+?");
    			mExcludeUrl.add("http:\\S+shoppingController/pgoodstype.do");
    			
    			isMatch = StringUtil.matchUrlExclude(mExcludeUrl, testUrl);
    			mExcludeUrl = null;
    			
    	}
    	return isMatch;
    }
}
