package cn.gov.xnc.system.web.system.service;

import java.math.BigDecimal;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserTypeEntity;

public interface UserGradePriceServiceI extends CommonService{
	
	public boolean updateGradePriceByUserType(UserTypeEntity userType);
	
	public boolean delGradePriceByUserType(UserTypeEntity userType);
	
	public BigDecimal getProductFinalPrice(ProductEntity product, TSUser client);
	
	public void addUserGradePrice(final ProductEntity product, final UserTypeEntity userType);
	
	public void initUserGradePrice(final UserTypeEntity userType);
}
