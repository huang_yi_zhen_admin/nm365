package cn.gov.xnc.system.web.system.service;

import java.util.List;
import java.util.Map;

import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSComponent;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;

/**
 * 组件服务接口
 */
public interface ComponentService  extends CommonService{
	
	/**
	 * 检查服务类型
	 * @return
	 */
	public List<TSDictionary> checkServiceType();
	/**
	 * 检查服务开关
	 * @return
	 */
	public List<TSDictionary> checkServiceSwitch();
	
	/**
	 * 检查下单出库服务开关
	 * @return
	 */
	public boolean checkOrderStockOutSwitch();
	
	/**
	 * 动态添加下单出库服务类型
	 */
	public void addOrderStockOutServiceType();
	
	/**
	 * 动态添加下单出库服务开关
	 */
	public void addOrderStockOutServiceSwitch();

	/**
	 * 根据条件查询列表
	 * @param params
	 * @return
	 */
	public List<TSComponent> findListByParams(Map<String, Object> params);
	
	/**
	 * AJAX 根据条件查询列表并分页
	 * @param params
	 */
	public void findListPageByParams(Map<String, Object> params);
	
	/**
	 * 根据对象封装参数
	 * @param component
	 * @return
	 */
	public Map<String, Object> getParams(TSComponent component);
}
