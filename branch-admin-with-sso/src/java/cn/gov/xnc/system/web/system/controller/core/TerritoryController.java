package cn.gov.xnc.system.web.system.controller.core;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.ComboTree;
//import cn.gov.xnc.system.core.common.model.json.TreeGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.encache.EncacheUtil;
import cn.gov.xnc.system.core.util.JSONHelper;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.tag.vo.easyui.ComboTreeModel;
import cn.gov.xnc.system.tag.vo.easyui.TreeGridModel;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.TerritoryService;


/**
 * 地域处理类
 * @author ljz
 */
@Scope("prototype")
@Controller
@RequestMapping("/territoryController")
public class TerritoryController extends BaseController {
	
	private String message = null;
	
	@Autowired
	private SystemService systemService;

	@Autowired
	private TerritoryService territoryService;
	
	/**
	 * 地域列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "territory")
	public ModelAndView function() {
		return new ModelAndView("system/territory/territoryList");
	}

	
//	/**
//	 * 地域列表
//	 */
//	@RequestMapping(params = "territoryGrid")
//	@ResponseBody
//	public List<TreeGrid> territoryGrid(HttpServletRequest request, TreeGrid treegrid) {
//		CriteriaQuery cq = new CriteriaQuery(TSTerritory.class);
//			if (treegrid.getId() != null) {
//				cq.eq("TSTerritory.id", treegrid.getId());
//			}
//			if (treegrid.getId() == null) {
//				//cq.eq("TSTerritory.id","1");//这个是全国最高级
//				cq.isNull("TSTerritory");
//			}
//		
//		cq.addOrder("territorySort", SortDirection.asc);
//		cq.add();
//		
//			List<TSTerritory> territoryList = systemService.getListByCriteriaQuery(cq, false);
//			
//			
//			List<TreeGrid> treeGrids = new ArrayList<TreeGrid>();
//			TreeGridModel treeGridModel = new TreeGridModel();
//			treeGridModel.setIcon("");
//			treeGridModel.setTextField("territoryName");
//			treeGridModel.setParentText("TSTerritory_territoryName");
//			treeGridModel.setParentId("TSTerritory_id");
//			treeGridModel.setSrc("territoryCode");
//			treeGridModel.setIdField("id");
//			treeGridModel.setChildList("TSTerritorys");
//			treeGridModel.setOrder("territorySort");
//			treeGrids = systemService.treegrid(territoryList, treeGridModel);
//			
//		return treeGrids;
//	}
	/**
	 *地域列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(TSTerritory territory, HttpServletRequest req) {
		String functionid = req.getParameter("id");
		if (functionid != null) {
			territory = systemService.getEntity(TSTerritory.class, functionid);
			req.setAttribute("territory", territory);
		}
		if(territory.getTSTerritory()!=null && territory.getTSTerritory().getId()!=null){
			territory.setTSTerritory((TSTerritory)systemService.getEntity(TSTerritory.class, territory.getTSTerritory().getId()));
			/**
			 * 主要为了让前端选择下拉框 选中 下级区域
			 */
			territory.setTerritoryLevel(Short.valueOf("1"));
			req.setAttribute("territory", territory);
		}
		EncacheUtil.getInstance().removeAll();
		return new ModelAndView("system/territory/territory");
	}
//	/**
//	 * 地域父级下拉菜单
//	 */
//	@RequestMapping(params = "setPTerritory")
//	@ResponseBody
//	public List<ComboTree> setPTerritory(HttpServletRequest request, ComboTree comboTree) {
//		CriteriaQuery cq = new CriteriaQuery(TSTerritory.class);
//		
//		if (comboTree.getId() != null) {
//			cq.eq("TSTerritory.id", comboTree.getId());
//		}
//		if (comboTree.getId() == null) {
//			cq.isNull("TSTerritory");
//		}
//		cq.add();
//		List<TSTerritory> territoryList = systemService.getListByCriteriaQuery(cq, false);
//		List<ComboTree> comboTrees = new ArrayList<ComboTree>();
//		ComboTreeModel comboTreeModel = new ComboTreeModel("id", "territoryName", "TSTerritorys");
//		comboTrees = systemService.ComboTree(territoryList, comboTreeModel, null);
//		EncacheUtil.getInstance().removeAll();
//		return comboTrees;
//	}
	/**
	 * 地域保存
	 */
	@RequestMapping(value = "saveTerritory")
	@ResponseBody
	public AjaxJson saveTerritory(TSTerritory territory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String functionOrder = territory.getTerritorySort();
		if(StringUtils.isEmpty(functionOrder)){
			territory.setTerritorySort("0");
		}
		if (territory.getTSTerritory().getId().equals("")) {
			territory.setTSTerritory(null);
		}else{
			TSTerritory parent = systemService.getEntity(TSTerritory.class, territory.getTSTerritory().getId());
			territory.setTerritoryLevel(Short.valueOf(parent.getTerritoryLevel()+1+""));
		}
		if (StringUtil.isNotEmpty(territory.getId())) {
			message = "地域: " + territory.getTerritoryName() + "被更新成功";
			systemService.saveOrUpdate(territory);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			territory.setTerritorySort(territory.getTerritorySort());
			message = "地域: " + territory.getTerritoryName() + "被添加成功";
			systemService.save(territory);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);

		}
		EncacheUtil.getInstance().removeAll();
		return j;
	}

	/**
	 * 地域删除
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(TSTerritory territory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		territory = systemService.getEntity(TSTerritory.class, territory.getId());
		message = "地域: " + territory.getTerritoryName() + "被删除成功";
		systemService.delete(territory);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		EncacheUtil.getInstance().removeAll();
		return j;
	}
	
	/**
	 * 级联城市
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "cascadeArea")
	@ResponseBody
	public List<TSTerritory> cascadeArea(HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		String id = req.getParameter("id");
		List<TSTerritory> list = null;
		if(StringUtils.isEmpty(id)){
			list = territoryService.findRootTerritoryByJbdc();
		}else{
			list = territoryService.findTerritoryByJdbc(id);
		}
		return list;
		//message = JSONHelper.toJSONString(list);
		//return message;
	}
	
	


}
