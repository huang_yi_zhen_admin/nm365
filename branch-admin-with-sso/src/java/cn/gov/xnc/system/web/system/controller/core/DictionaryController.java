package cn.gov.xnc.system.web.system.controller.core;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSComponent;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.ComponentService;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

/**
 * 字典管理
 */
@Scope("prototype")
@Controller
@RequestMapping("/dictionaryController")
public class DictionaryController extends BaseController {
	private static final Logger logger = Logger.getLogger(DictionaryController.class);
	@Autowired
	private SystemService systemService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private ComponentService componentService;
	
	
	@RequestMapping(value = "list")
	public ModelAndView dictionaryList(HttpServletRequest request) {
		return new ModelAndView("admin/dictionary/list");
	}
	
	/**
	 * AJAX 加载服务组件
	 * @param component
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagrid")
	public void datagrid(TSDictionary dictionary,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		Map<String, Object> params = dictionaryService.getParams(dictionary);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("isdel", "N");
		params.put("dataGrid", dataGrid);
		dictionaryService.findListPageByParams(params);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 表单界面/保存/更新
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "form")
	public ModelAndView form(TSDictionary dictionary,HttpServletRequest request, HttpServletResponse response){
		if(StringUtil.isNotEmpty(dictionary.getId())){
			dictionary = systemService.getEntity(TSDictionary.class, dictionary.getId());
		}
		request.setAttribute("dictionary", dictionary);
		return new ModelAndView("admin/dictionary/form");
	}
	
	/**
	 * 提交
	 * @param dictionary
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "submit")
	@ResponseBody
	public AjaxJson submit(TSDictionary dictionary, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		try {
			if(StringUtil.isNotEmpty(dictionary.getId())){
				String name = dictionary.getDictionaryName();
				String value = dictionary.getDictionaryValue();
				String desc = dictionary.getDictionaryDesc();
				String type = dictionary.getDictionaryType();
				dictionary = systemService.getEntity(TSDictionary.class, dictionary.getId());
				dictionary.setDictionaryDesc(desc);
				dictionary.setDictionaryType(type);
				dictionary.setDictionaryValue(value);
				dictionary.setDictionaryName(name);
				dictionary.setUpdateTime(DateUtils.getDate());
				systemService.updateEntitie(dictionary);
			}else{
				dictionary.setIsdel("N");
				dictionary.setCreateTime(DateUtils.getDate());
				dictionary.setUpdateTime(DateUtils.getDate());
				systemService.save(dictionary);
			}
			result.setSuccess(true);
			result.setMsg("操作完成");
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("保存服务组件出错：" + e);
			systemService.addLog("保存服务组件出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return result;
	}
	
	/**
	 * 删除信息
	 * @param component
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson delete(TSDictionary dictionary, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		try {
			if(StringUtil.isNotEmpty(dictionary.getId())){
				dictionary = systemService.getEntity(TSDictionary.class, dictionary.getId());
				dictionary.setIsdel("Y");
				systemService.updateEntitie(dictionary);
				result.setSuccess(true);
				result.setMsg("操作完成");
			}else{
				result.setSuccess(false);
				result.setMsg("操作失败");
			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("更新字典出错：" + e);
			systemService.addLog("更新字典出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return result;
	}
}
