package cn.gov.xnc.system.web.system.controller.core;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyIndustryEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;



/**   
 * @Title: Controller
 * @Description: 公司行业信息
 * @author zero
 * @date 2016-09-26 02:54:40
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/companyIndustryController")
public class CompanyIndustryController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CompanyIndustryController.class);


	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 公司行业信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView companyIndustry(HttpServletRequest request) {
		return new ModelAndView("system/user/companyIndustryList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(CompanyIndustryEntity companyIndustry,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(CompanyIndustryEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyIndustry, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除公司行业信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(CompanyIndustryEntity companyIndustry, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyIndustry = systemService.getEntity(CompanyIndustryEntity.class, companyIndustry.getId());
		message = "公司行业信息删除成功";
		systemService.delete(companyIndustry);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加公司行业信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(CompanyIndustryEntity companyIndustry, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(companyIndustry.getId())) {
			message = "公司行业信息更新成功";
			CompanyIndustryEntity t = systemService.get(CompanyIndustryEntity.class, companyIndustry.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(companyIndustry, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "公司行业信息更新失败";
			}
		} else {
			message = "公司行业信息添加成功";
			systemService.save(companyIndustry);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 公司行业信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(CompanyIndustryEntity companyIndustry, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(companyIndustry.getId())) {
			companyIndustry = systemService.getEntity(CompanyIndustryEntity.class, companyIndustry.getId());
			req.setAttribute("companyIndustryPage", companyIndustry);
		}
		return new ModelAndView("system/user/companyIndustry");
	}
}
