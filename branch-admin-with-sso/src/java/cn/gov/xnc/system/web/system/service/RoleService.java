package cn.gov.xnc.system.web.system.service;

import cn.gov.xnc.system.core.util.ApplicationContextUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public class RoleService {
	private static SystemService systemService;
	
	static{
		systemService = (SystemService) ApplicationContextUtil.getContext().getBean("systemService");
	}
	
	/**
	 * 获取该用户是否是管理员角色
	 * @return
	 */
	public static boolean getRoleCode(TSUser tsUser ){
		
		/*List<TSRoleUser> rUsers = systemService.findByProperty(TSRoleUser.class, "TSUser.id", tsUser.getId());
		boolean isAdmin = false;
		for (TSRoleUser ru : rUsers) {
			TSRole role = ru.getTSRole();
			if("admin".equals(role.getRoleCode())){
				isAdmin = true;
				break;
			}
		}
		
		return isAdmin;*/
		boolean isAdmin = false;
		TSRole ru = ResourceUtil.getSessionUserRole();		
		if("admin".equals(ru.getRoleCode())){
			isAdmin = true;			
		}
		return isAdmin;
	}
}
