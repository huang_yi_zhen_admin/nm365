package cn.gov.xnc.system.web.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSComponent;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.ComponentService;
import cn.gov.xnc.system.web.system.service.DictionaryService;

@Service("componentService")
public class ComponentServiceImpl extends CommonServiceImpl implements ComponentService{

	@Autowired
	private DictionaryService dictionaryService;
	
	/**
	 * AJAX 根据条件查询列表并分页
	 * @param params
	 */
	public void findListPageByParams(Map<String, Object> params) {
		DataGrid dataGrid = (DataGrid) params.get("dataGrid");
		CriteriaQuery cq = new CriteriaQuery(TSComponent.class,dataGrid);
		if(StringUtil.isNotEmpty(params.get("company"))){
			cq.eq("company", params.get("company"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentType"))){
			cq.eq("componentType", params.get("componentType"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentName"))){
			cq.eq("componentName", params.get("componentName"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentValue"))){
			cq.eq("componentValue", params.get("componentValue"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentURL"))){
			cq.eq("componentURL", params.get("componentURL"));
		}
		
		if(StringUtil.isNotEmpty(params.get("isdel"))){
			cq.eq("isdel", params.get("isdel"));
		}
		
		String startTime = (String) params.get("startTime");
		String endTime = (String) params.get("endTime");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(startTime)&&StringUtil.isNotEmpty(endTime)){
			cq.between("createTime", DateUtils.str2Date(startTime,sdf), DateUtils.str2Date(endTime,sdf));
		}else if( StringUtil.isNotEmpty(startTime) ){
			cq.ge("createTime", DateUtils.str2Date(startTime,sdf));
		}else if( StringUtil.isNotEmpty(endTime) ){
			cq.le("createTime", DateUtils.str2Date(endTime,sdf));
		}
		cq.addOrder("createTime", SortDirection.desc);
		cq.add();
		this.getDataGridReturn(cq, true);
	}
	
	/**
	 * 根据条件查询列表
	 * @param params
	 * @return
	 */
	public List<TSComponent> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(TSComponent.class);
		if(StringUtil.isNotEmpty(params.get("company"))){
			cq.eq("company", params.get("company"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentType"))){
			cq.eq("componentType", params.get("componentType"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentName"))){
			cq.eq("componentName", params.get("componentName"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentValue"))){
			cq.eq("componentValue", params.get("componentValue"));
		}
		
		if(StringUtil.isNotEmpty(params.get("componentURL"))){
			cq.eq("componentURL", params.get("componentURL"));
		}
		
		if(StringUtil.isNotEmpty(params.get("isdel"))){
			cq.eq("isdel", params.get("isdel"));
		}
		
		String startTime = (String) params.get("startTime");
		String endTime = (String) params.get("endTime");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(startTime)&&StringUtil.isNotEmpty(endTime)){
			cq.between("createTime", DateUtils.str2Date(startTime,sdf), DateUtils.str2Date(endTime,sdf));
		}else if( StringUtil.isNotEmpty(startTime) ){
			cq.ge("createTime", DateUtils.str2Date(startTime,sdf));
		}else if( StringUtil.isNotEmpty(endTime) ){
			cq.le("createTime", DateUtils.str2Date(endTime,sdf));
		}
		cq.addOrder("createTime", SortDirection.desc);
		
		cq.add();
		List<TSComponent> list = this.getListByCriteriaQuery(cq, false);
		return list;
	}

	/**
	 * 根据对象封装参数
	 * @param component
	 * @return
	 */
	public Map<String, Object> getParams(TSComponent component) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		if(component != null){
			if(StringUtil.isNotEmpty(user.getCompany())){
				params.put("company", user.getCompany());
			}
			if(StringUtil.isNotEmpty(component.getComponentType())){
				params.put("componentType", component.getComponentType());
			}
			if(StringUtil.isNotEmpty(component.getComponentName())){
				params.put("componentName", component.getComponentName());
			}
			if(StringUtil.isNotEmpty(component.getComponentValue())){
				params.put("componentValue", component.getComponentValue());
			}
			if(StringUtil.isNotEmpty(component.getComponentURL())){
				params.put("componentURL", component.getComponentURL());
			}
			if(StringUtil.isNotEmpty(component.getIsdel())){
				params.put("isdel", component.getIsdel());
			}
		}
		return params;
	}
	
	/**
	 * 动态添加下单出库服务类型
	 */
	public void addOrderStockOutServiceType() {
		TSUser user = ResourceUtil.getSessionUserName();
		TSDictionary type = new TSDictionary();
		type.setCompany(user.getCompany());
		type.setDictionaryType("systemServiceType");
		type.setDictionaryName("下单出库服务");
		type.setDictionaryDesc("下单自动出库服务");
		type.setDictionaryValue("orderStockOutService");
		type.setCreateTime(DateUtils.getDate());
		type.setUpdateTime(DateUtils.getDate());
		type.setIsdel("N");
		dictionaryService.save(type);
	}

	/**
	 * 检查服务类型
	 * @return
	 */
	public List<TSDictionary> checkServiceType() {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dictionaryType", "systemServiceType");
		params.put("company", user.getCompany());
		params.put("isdel", "N");
		List<TSDictionary> list = dictionaryService.findListByParams(params);
		if(list == null || list.size() <= 0){
			addOrderStockOutServiceType();
			list = dictionaryService.findListByParams(params);
		}
		return list;
	}

	/**
	 * 检查服务开关
	 * @return
	 */
	public List<TSDictionary> checkServiceSwitch() {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dictionaryType", "orderStockOutSwitch");
		params.put("company", user.getCompany());
		params.put("isdel", "N");
		List<TSDictionary> list = dictionaryService.findListByParams(params);
		if(list == null || list.size() <= 0){
			addOrderStockOutServiceSwitch();
			list = dictionaryService.findListByParams(params);
		}
		return list;
	}

	/**
	 * 检查下单出库服务开关
	 * @return
	 */
	public boolean checkOrderStockOutSwitch() {
		boolean serviceSwitch = false;
		
		Map<String, Object> dictionaryParams = new HashMap<String, Object>();
		dictionaryParams.put("dictionaryType", "systemServiceType");
		dictionaryParams.put("dictionaryValue", "orderStockOutService");
		//检查服务类型
		List<TSDictionary> dictionarys = dictionaryService.findListByParams(dictionaryParams);
		if(dictionarys.size() <= 0){
			//动态添加服务配置
			this.addOrderStockOutServiceType();
			dictionarys = dictionaryService.findListByParams(dictionaryParams);
		}
		
		//检查服务开关
		List<TSDictionary> switchs = this.checkServiceSwitch();
		
		Map<String, Object> componentParams = new HashMap<String, Object>();
		componentParams.put("componentType", dictionarys.get(0));
		List<TSComponent> components = this.findListByParams(componentParams);
		if(components.size() <= 0){
			//动态添加开关配置
			TSComponent component = new TSComponent();
			for (TSDictionary tsDictionary : switchs) {
				if(tsDictionary.getDictionaryValue().equals("0")){
					component.setComponentValue(tsDictionary);
				}
			}
			component.setComponentType(dictionarys.get(0));
			component.setComponentName("下单自动出库");
			component.setIsdel("N");
			this.save(component);
			
			components = this.findListByParams(componentParams);
		}
		
		for (TSComponent component : components) {
			if(component.getComponentValue().getDictionaryValue().equals("0")){
				serviceSwitch = true;
			}
		}
		
		return serviceSwitch;
	}

	/**
	 * 动态添加下单出库服务开关
	 */
	public void addOrderStockOutServiceSwitch() {
		TSUser user = ResourceUtil.getSessionUserName();
		TSDictionary start = new TSDictionary();
		start.setCompany(user.getCompany());
		start.setDictionaryType("orderStockOutSwitch");
		start.setDictionaryName("开启");
		start.setDictionaryDesc("下单自动出库服务开关");
		start.setDictionaryValue("0");
		start.setCreateTime(DateUtils.getDate());
		start.setUpdateTime(DateUtils.getDate());
		start.setIsdel("N");
		dictionaryService.save(start);
		
		TSDictionary stop = new TSDictionary();
		start.setCompany(user.getCompany());
		stop.setDictionaryType("orderStockOutSwitch");
		stop.setDictionaryName("关闭");
		stop.setDictionaryDesc("下单自动出库服务开关");
		stop.setDictionaryValue("1");
		stop.setCreateTime(DateUtils.getDate());
		stop.setUpdateTime(DateUtils.getDate());
		stop.setIsdel("N");
		dictionaryService.save(stop);
	}

}
