package cn.gov.xnc.system.web.system.controller.core;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.ArrayList;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyThirdpartyEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;



/**   
 * @Title: Controller
 * @Description: 支付设置信息
 * @author zero
 * @date 2016-09-26 03:01:12
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/companyThirdpartyController")
public class CompanyThirdpartyController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CompanyThirdpartyController.class);


	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 支付设置信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView companyThirdparty(HttpServletRequest request) {
		return new ModelAndView("system/user/companyThirdpartyList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(CompanyThirdpartyEntity companyThirdparty,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		if(  StringUtil.isNotEmpty(user.getCompany().getId()) ){//如果不公司信息 这返回空
			TSCompany tScompany = systemService.getEntity(TSCompany.class, user.getCompany().getId());
			companyThirdparty.setId(tScompany.getCompanythirdparty().getId());
			CriteriaQuery cq = new CriteriaQuery(CompanyThirdpartyEntity.class, dataGrid);
			//查询条件组装器
			cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyThirdparty, request.getParameterMap());
			this.systemService.getDataGridReturn(cq, true);
			TagUtil.datagrid(response, dataGrid);
		}
	}
	
	
	/**
	 * 线下转账银行选择
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiceOfflinePayment")
	public ModelAndView choiceOfflinePayment(HttpServletRequest request) {
		return new ModelAndView("admin/order/choiceOfflinePayment");
		
	}
	
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "getbank")
	public void getbank(CompanyThirdpartyEntity companyThirdparty,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		if(  StringUtil.isNotEmpty(user.getCompany().getId()) ){//如果不公司信息 这返回空
			TSCompany tScompany = systemService.getEntity(TSCompany.class, user.getCompany().getId());
			companyThirdparty.setId(tScompany.getCompanythirdparty().getId());
			CriteriaQuery cq = new CriteriaQuery(CompanyThirdpartyEntity.class, dataGrid);
			//查询条件组装器
			cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyThirdparty, request.getParameterMap());
			this.systemService.getDataGridReturn(cq, true);
			
			companyThirdparty = tScompany.getCompanythirdparty();
			
			List<CompanyThirdpartyEntity> bankList = new ArrayList<CompanyThirdpartyEntity>() ;
			if(companyThirdparty != null ){
				if(StringUtil.isNotEmpty(companyThirdparty.getBankA()) && StringUtil.isNotEmpty(companyThirdparty.getBankaccountA()) ){
					CompanyThirdpartyEntity A= new CompanyThirdpartyEntity();
					A.setBankA(companyThirdparty.getBankA());
					A.setBankaccountA(companyThirdparty.getBankaccountA());
					A.setId(companyThirdparty.getBankaccountA());
					bankList.add(A);
				}
				if(StringUtil.isNotEmpty(companyThirdparty.getBankB()) && StringUtil.isNotEmpty(companyThirdparty.getBankaccountB()) ){
					CompanyThirdpartyEntity B= new CompanyThirdpartyEntity();
					B.setBankA(companyThirdparty.getBankB());
					B.setBankaccountA(companyThirdparty.getBankaccountB());
					B.setId(companyThirdparty.getBankaccountB());
					bankList.add(B);
				}
				if(StringUtil.isNotEmpty(companyThirdparty.getBankC()) && StringUtil.isNotEmpty(companyThirdparty.getBankaccountC()) ){
					CompanyThirdpartyEntity C= new CompanyThirdpartyEntity();
					C.setBankA(companyThirdparty.getBankC());
					C.setBankaccountA(companyThirdparty.getBankaccountC());
					C.setId(companyThirdparty.getBankaccountC());
					bankList.add(C);
				}
				
			}
			
			dataGrid.setResults(bankList);
			dataGrid.setTotal(bankList.size());
			
			TagUtil.datagrid(response, dataGrid);
		}
	}
	
	
	
	
	

	/**
	 * 删除支付设置信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(CompanyThirdpartyEntity companyThirdparty, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyThirdparty = systemService.getEntity(CompanyThirdpartyEntity.class, companyThirdparty.getId());
		message = "支付设置信息删除成功";
		systemService.delete(companyThirdparty);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加支付设置信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(CompanyThirdpartyEntity companyThirdparty, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(companyThirdparty.getId())) {
			message = "支付设置信息更新成功";
			
			String updateT = request.getParameter("updateT");
			
			
			
			CompanyThirdpartyEntity t = systemService.get(CompanyThirdpartyEntity.class, companyThirdparty.getId());
			try {
				//MyBeanUtils.copyBeanNotNull2Bean(companyThirdparty, t);
				if(StringUtil.isNotEmpty(updateT) && "1".equals(updateT) ){//支付宝更新
					t.setAlipayid(companyThirdparty.getAlipayid());
					t.setAlipaykey(companyThirdparty.getAlipaykey());
				}else if (StringUtil.isNotEmpty(updateT) && "2".equals(updateT) ) {//微信更新
					t.setAppid(companyThirdparty.getAppid());
					t.setAppidsh(companyThirdparty.getAppidsh());
					t.setAppsecret(companyThirdparty.getAppsecret());
					t.setAppsecretsh(companyThirdparty.getAppsecretsh());
				}else if (StringUtil.isNotEmpty(updateT) && "3".equals(updateT) ) {//银行账号设置
					
					t.setBankAname(companyThirdparty.getBankAname());
					t.setBankA(companyThirdparty.getBankA());
					t.setBankaccountA(companyThirdparty.getBankaccountA());
					t.setBankBname(companyThirdparty.getBankBname());
					t.setBankB(companyThirdparty.getBankB());
					t.setBankaccountB(companyThirdparty.getBankaccountB());
					t.setBankCname(companyThirdparty.getBankCname());
					t.setBankC(companyThirdparty.getBankC());
					t.setBankaccountC(companyThirdparty.getBankaccountC());
					//t.setBankUrl(companyThirdparty.getBankUrl());
				}

				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "支付设置信息更新失败";
			}
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 支付设置信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(HttpServletRequest req) {
		
		String updateT = req.getParameter("updateT");
		TSUser user = ResourceUtil.getSessionUserName();
		if (StringUtil.isNotEmpty(user.getCompany().getCompanythirdparty().getId())) {
			CompanyThirdpartyEntity companyThirdparty = systemService.getEntity(CompanyThirdpartyEntity.class, user.getCompany().getCompanythirdparty().getId());
			req.setAttribute("companyThirdpartyPage", companyThirdparty);
		}
		req.setAttribute("updateT", updateT);
		return new ModelAndView("system/user/companyThirdparty");
	}
	
	/**
	 * 线下转账银行选择
	 * 
	 * @return
	 */
	@RequestMapping(value = "choicePaymentBank")
	public ModelAndView choicePaymentBank(HttpServletRequest request) {
		
		return new ModelAndView("cash/choiseBank");
		
	}
}
