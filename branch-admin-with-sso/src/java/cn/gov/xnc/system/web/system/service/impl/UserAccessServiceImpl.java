package cn.gov.xnc.system.web.system.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.HttpRequestHelper;
import cn.gov.xnc.system.core.util.HttpResponseHelper;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.IpUtil;
import cn.gov.xnc.system.core.util.MobileDeviceHelper;
import cn.gov.xnc.system.core.util.PasswordUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.Client;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.TSUserAccessTokenEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSUserAccesslogEntity;
import cn.gov.xnc.system.web.system.service.UserAccessServiceI;

@Service("userAccessService")
@Transactional
public class UserAccessServiceImpl extends CommonServiceImpl implements UserAccessServiceI {

	@Override
	public String generateAccessToken() throws Exception {
		
		return PasswordUtil.md5( DateUtils.getDate("yyyyMMddHHmmss") );
	}

	@Override
	public String getRequestAccessToken(HttpServletRequest req) throws Exception {
		//根据传参获取token
		String token = ResourceUtil.getParameter("ssid");
		//从http请求头部获取
		if( StringUtil.isEmpty(token) ){
			Map<String, String> map = HttpRequestHelper.getRequestHeadersInfo(req);
			
			String keyVal = (String) map.get("ssid");
			if(StringUtil.isNotEmpty(keyVal)){
				token = keyVal;
			}
			
		}
		
		//从cookie获取
		if( StringUtil.isEmpty(token) ){
			Cookie cookie = HttpRequestHelper.getCookieByName(req, "ssid");
			
			String keyVal = cookie.getValue();
			if(StringUtil.isNotEmpty(keyVal)){
				token = keyVal;
			}
			
		}
		
		return token;
	}

	@Override
	public TSUserAccesslogEntity saveAccessLog(HttpServletRequest req) throws Exception {
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		TSUserAccesslogEntity userAccessEntity = new TSUserAccesslogEntity();
		if(null != user){
			userAccessEntity.setId(IdWorker.generateSequenceNo());
			userAccessEntity.setIp(IpUtil.getRealIp());
			userAccessEntity.setUrl(req.getRequestURI()+"?"+ (null == req.getQueryString() ? "" : req.getQueryString()));
			userAccessEntity.setUserid( findUniqueByProperty(TSUser.class, "id", user.getId()));
			userAccessEntity.setCreatetime(DateUtils.getDate());
			
			save(userAccessEntity);
			
		}else{
			
			throw new Exception("抱歉，您还不是系统用户");
		}
		
		
		
		return userAccessEntity;
	}

	@Override
	public void activeAccessToken(String token, TSUserAccesslogEntity lastAccesslog) throws Exception {
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		if(null != user){
			
			TSUserAccessTokenEntity accessTokenEntity = getUserAccessTokenInfo(token);
			if(null != accessTokenEntity){
				accessTokenEntity.setUserid(user);
				accessTokenEntity.setToken(token);
				accessTokenEntity.setLastaccessid(lastAccesslog);
				accessTokenEntity.setStatus(UserAccessServiceI.Status.VALID.getValue());
				
				updateEntitie(accessTokenEntity);
				
			}else{
				accessTokenEntity = new TSUserAccessTokenEntity();
				accessTokenEntity.setId(IdWorker.generateSequenceNo());
				accessTokenEntity.setCreatetime(DateUtils.getDate());
				accessTokenEntity.setExpiretime(DateUtils.str2Date((DateUtils.getAfterNTime("yyyyMMdd", 1) + "235959"),DateUtils.yyyymmddhhmmss));
				accessTokenEntity.setToken(token);
				accessTokenEntity.setUserid(user);
				accessTokenEntity.setLastaccessid(lastAccesslog);
				accessTokenEntity.setStatus(UserAccessServiceI.Status.VALID.getValue());
				
				save(accessTokenEntity);
			}
			
		}else{
			
			throw new Exception("抱歉，您没有访问的权限");
		}
		
	}

	@Override
	public void writeAccessToken2client(String token, HttpServletRequest req, HttpServletResponse resp) throws Exception {
		
		//http header 添加token
		if(StringUtil.isNotEmpty(token)){
			resp.setHeader("ssid", token);
		}
		
		//cookie 添加token
		HttpResponseHelper.setCookie(resp, "ssid", token, 30*24*60*60);
		
	}

	@Override
	public boolean authenUserAccess(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		boolean access = false;
		
		//获取会话
		HttpSession session = ContextHolderUtils.getSession();
		//根据访问令牌
		String token = req.getParameter("ssid");//从请求参数获取访问令牌
		if(StringUtil.isEmpty(token))//从请求头部获取访问令牌
		{
			token = HttpRequestHelper.getRequestHeadersInfo(req).get("ssid");
		}
		if(StringUtil.isEmpty(token)){//从cookie获取访问令牌
			Cookie cookie = HttpRequestHelper.getCookieByName(req, "ssid");
			token = (null != cookie ? cookie.getValue() : null);
		}
		
		if(StringUtil.isNotEmpty(token)){
			
			//移动端访问，判断token是否有效，如果token有效但是会话数据已经不存在，则重新构造会话数据
			if(MobileDeviceHelper.isMobileAccess(req)){
				
				TSUserAccessTokenEntity tokeninfo = getUserAccessTokenInfo(token);
				
				if(isTokenValid(tokeninfo)){
					
					renewClient(tokeninfo, session, req, resp);
				}else{
					removeClient(req);
				}
			}
		}
		
		Client client = ClientManager.getInstance().getClient(token);
		
		if(null != client && null != client.getUser()){
			
			
			access = true;
		}
		return access;
		
	}

	@Override
	public TSUserAccessTokenEntity getUserAccessTokenInfo(String token) throws Exception {
		TSUserAccessTokenEntity accessToken = null;
		
		if(StringUtil.isNotEmpty(token)){
			
			long l = getCountForJdbc("select count(1) from t_s_access_token where token = '" + token + "'");
			
			if(l > 0){
				CriteriaQuery cq = new CriteriaQuery(TSUserAccessTokenEntity.class);
				cq.eq("token", token);
				cq.add();
				//cq.eq("status", UserAccessServiceI.Status.VALID.getValue());
				
				List<TSUserAccessTokenEntity> accesstokenlist = getListByCriteriaQuery(cq, false);
				
				if(null != accesstokenlist && accesstokenlist.size() > 0){
					
					accessToken = accesstokenlist.get(0);
				}
			}
			
		}
		return accessToken;
	}

	@Override
	public boolean isTokenValid(TSUserAccessTokenEntity tokeninfo) throws Exception {
		//判断用户有没有访问令牌
		if(null != tokeninfo){
			
			//令牌状态失效，需要重新登录获取访问权限
			String tokenStatus = tokeninfo.getStatus();
			if(UserAccessServiceI.Status.INVALID.getValue().equals( tokenStatus )){
				return false;
			}
			TSUserAccesslogEntity lastAccesslog = tokeninfo.getLastaccessid();
			//判断访问前后ip地址上是否不一致，不一致则需要重新登录获取访问权限
			String lastip = lastAccesslog.getIp();
			String currentip = IpUtil.getRealIp();
			if(StringUtil.isNotEmpty(lastip) && !lastip.equals(currentip)){
				return false;
			}
			//访问令牌时间超过一个月，需要重新登录获取访问权限
			Date currentdate = DateUtils.getDate();
			Date lastaccessdate = lastAccesslog.getCreatetime();
			if(DateUtils.daysBetween(lastaccessdate, currentdate) > 30){
				return false;
			}
			
			return true;
		}
		else{
			return false;
		}
		
	}

	@Override
	public Client renewClient(TSUserAccessTokenEntity lastToken, HttpSession session, HttpServletRequest req, HttpServletResponse resp) throws Exception {
		
		Client client = null;
		if(null != session && null != lastToken && null != lastToken.getUserid()){
			String newToken = session.getId();
			
			//创建会话数据
			TSUser user = lastToken.getUserid();
			
			client = new Client();
	        client.setLogindatetime(new Date());
	        client.setUser(user);
	        ClientManager.getInstance().addClinet(session.getId(), client);
	        
	        //把访问令牌返回客户
	        writeAccessToken2client(newToken, req, resp);
	        
	        TSUserAccesslogEntity accesslog = saveAccessLog(req);
	        
	        activeAccessToken(newToken, accesslog);
		}
		
		return client;
	}

	@Override
	public Client initClient(TSUser user, HttpSession session, HttpServletRequest req, HttpServletResponse resp) throws Exception {
		Client client = null;
		if(null != session ){
			String newToken = session.getId();
			
			//创建会话数据
			client = new Client();
	        client.setLogindatetime(new Date());
	        client.setUser(user);
	        ClientManager.getInstance().removeClinet(session.getId());
	        ClientManager.getInstance().addClinet(session.getId(), client);
	        
	        //把访问令牌返回客户
	        writeAccessToken2client(newToken, req, resp);
	        
	        TSUserAccesslogEntity accesslog = saveAccessLog(req);
	        
	        activeAccessToken(newToken, accesslog);
		}
		
		return client;
	}

	@Override
	public void removeClient(HttpServletRequest req) throws Exception {
//		TSUser user = ResourceUtil.getSessionUserName();
		//暂时允许一个用户在多会话下登陆，不根据用户id清除所有回话信息
		/*if(null !=user){
			List<TSUserAccessTokenEntity> accessTokenList = getUserActiveTokenByUserId(user.getId());
			
			if(null != accessTokenList && accessTokenList.size() > 0){
				
				for (TSUserAccessTokenEntity tokenInfo : accessTokenList) {
					String token = tokenInfo.getToken();
					ClientManager.getInstance().removeClinet( token );
					
					tokenInfo.setStatus(UserAccessServiceI.Status.INVALID.getValue());
				}
				
				batchUpdate(accessTokenList);
			}
		}*/
//		else{//用户信息没有访问令牌的从请求中获取
		//获取会话
			HttpSession session = ContextHolderUtils.getSession();
			String token = session.getId();
			TSUserAccessTokenEntity accessToken = getUserAccessTokenInfo( token );
			
			if(null == accessToken){
				token = getRequestAccessToken(req);
				accessToken = getUserAccessTokenInfo( token );
			}
			//让token失效
			if(null != accessToken){
				accessToken.setStatus(UserAccessServiceI.Status.INVALID.getValue());
				
				updateEntitie(accessToken);
				
				ClientManager.getInstance().removeClinet( token );
			}
//		}
		
	}

	@Override
	public List<TSUserAccessTokenEntity> getUserActiveTokenByUserId(String userid) throws Exception {
		List<TSUserAccessTokenEntity> accessTokenList = null;
		
		if(StringUtil.isNotEmpty(userid)){
			
			long l = getCountForJdbc("select count(1) from t_s_access_token where userid = '" + userid + "' and status = '" + UserAccessServiceI.Status.VALID.getValue() + "'");
			
			if(l > 0){
				CriteriaQuery cq = new CriteriaQuery(TSUserAccessTokenEntity.class);
				cq.eq("userid.id", userid);
				cq.eq("status", UserAccessServiceI.Status.VALID.getValue());
				cq.add();
				
				accessTokenList = getListByCriteriaQuery(cq, false);
				
			}
			
		}
		return accessTokenList;
	}
	
}