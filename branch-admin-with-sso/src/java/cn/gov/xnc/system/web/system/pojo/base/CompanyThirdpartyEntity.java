package cn.gov.xnc.system.web.system.pojo.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import cn.gov.xnc.system.core.util.StringUtil;


/**   
 * @Title: Entity
 * @Description: 支付设置信息
 * @author zero
 * @date 2016-09-26 03:01:12
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_company_thirdparty", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CompanyThirdpartyEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**微信应用ID*/
	private java.lang.String appid;
	/**微信应用密钥*/
	private java.lang.String appsecret;
	/**微信商户ID*/
	private java.lang.String appidsh;
	/**微信商户秘钥*/
	private java.lang.String appsecretsh;
	
	
	/**支付宝支付ID*/
	private java.lang.String alipayid;
	/**支付宝支付key*/
	private java.lang.String alipaykey;
	
	
	/**账户名称A*/
	private java.lang.String bankAname;
	/**开户银行A*/
	private java.lang.String bankA;
	/**银行账户A*/
	private java.lang.String bankaccountA;
	
	/**账户名称B*/
	private java.lang.String bankBname;
	/**开户银行B*/
	private java.lang.String bankB;
	/**银行账户B*/
	private java.lang.String bankaccountB;
	
	/**账户名称C*/
	private java.lang.String bankCname;
	/**账户名称C*/
	private java.lang.String bankC;
	/**账户名称C*/
	private java.lang.String bankaccountC;
	

	 
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  应用ID
	 */
	@Column(name ="APPID",nullable=true,length=50)
	public java.lang.String getAppid(){
		return this.appid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  应用ID
	 */
	public void setAppid(java.lang.String appid){
		this.appid = appid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  应用密钥
	 */
	@Column(name ="APPSECRET",nullable=true,length=200)
	public java.lang.String getAppsecret(){
		return this.appsecret;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  应用密钥
	 */
	public void setAppsecret(java.lang.String appsecret){
		this.appsecret = appsecret;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商户ID
	 */
	@Column(name ="APPIDSH",nullable=true,length=200)
	public java.lang.String getAppidsh(){
		return this.appidsh;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商户ID
	 */
	public void setAppidsh(java.lang.String appidsh){
		this.appidsh = appidsh;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商户秘钥
	 */
	@Column(name ="APPSECRETSH",nullable=true,length=200)
	public java.lang.String getAppsecretsh(){
		return this.appsecretsh;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商户秘钥
	 */
	public void setAppsecretsh(java.lang.String appsecretsh){
		this.appsecretsh = appsecretsh;
	}

	/**
	 * @return 支付宝支付ID
	 */
	@Column(name ="ALIPAYID",nullable=true,length=60)
	public java.lang.String getAlipayid() {
		return alipayid;
	}

	/**
	 * @param 支付宝支付ID
	 */
	public void setAlipayid(java.lang.String alipayid) {
		this.alipayid = alipayid;
	}

	/**
	 * @return 支付宝支付key
	 */
	@Column(name ="ALIPAYKEY",nullable=true,length=60)
	public java.lang.String getAlipaykey() {
		return alipaykey;
	}

	/**
	 * @param 支付宝支付key
	 */
	public void setAlipaykey(java.lang.String alipaykey) {
		this.alipaykey = alipaykey;
	}
	
	
	
	
	
	@Column(name ="BANKANAME",nullable=true,length=500)
	public java.lang.String getBankAname() {
		return bankAname;
	}

	public void setBankAname(java.lang.String bankAname) {
		this.bankAname = bankAname;
	}
	/**
	 * @return the bankA
	 */
	@Column(name ="BANKA",nullable=true,length=500)
	public java.lang.String getBankA() {
		return bankA;
	}

	/**
	 * @param bankA the bankA to set
	 */
	public void setBankA(java.lang.String bankA) {
		this.bankA = bankA;
	}
	/**
	 * @return the bankaccountA
	 */
	@Column(name ="BANKACCOUNTA",nullable=true,length=500)
	public java.lang.String getBankaccountA() {
		return bankaccountA;
	}

	/**
	 * @param bankaccountA the bankaccountA to set
	 */
	public void setBankaccountA(java.lang.String bankaccountA) {
		this.bankaccountA = bankaccountA;
	}

	
	
	@Column(name ="BANKBNAME",nullable=true,length=500)
	public java.lang.String getBankBname() {
		return bankBname;
	}

	public void setBankBname(java.lang.String bankBname) {
		this.bankBname = bankBname;
	}

	/**
	 * @return the bankB
	 */
	@Column(name ="BANKB",nullable=true,length=500)
	public java.lang.String getBankB() {
		return bankB;
	}

	/**
	 * @param bankB the bankB to set
	 */
	public void setBankB(java.lang.String bankB) {
		this.bankB = bankB;
	}

	/**
	 * @return the bankaccountB
	 */
	@Column(name ="BANKACCOUNTB",nullable=true,length=500)
	public java.lang.String getBankaccountB() {
		return bankaccountB;
	}

	/**
	 * @param bankaccountB the bankaccountB to set
	 */
	public void setBankaccountB(java.lang.String bankaccountB) {
		this.bankaccountB = bankaccountB;
	}
	
	
	
	

	/**
	 * @return the bankC
	 */
	@Column(name ="BANKC",nullable=true,length=500)
	public java.lang.String getBankC() {
		return bankC;
	}

	/**
	 * @param bankC the bankC to set
	 */
	public void setBankC(java.lang.String bankC) {
		this.bankC = bankC;
	}

	/**
	 * @return the bankaccountC
	 */
	@Column(name ="BANKACCOUNTC",nullable=true,length=500)
	public java.lang.String getBankaccountC() {
		return bankaccountC;
	}

	/**
	 * @param bankaccountC the bankaccountC to set
	 */
	public void setBankaccountC(java.lang.String bankaccountC) {
		this.bankaccountC = bankaccountC;
	}
	
	
	@Column(name ="BANKCNAME",nullable=true,length=500)
	public java.lang.String getBankCname() {
		return bankCname;
	}

	public void setBankCname(java.lang.String bankCname) {
		this.bankCname = bankCname;
	}
	
	


	public  boolean alipay(String json) {
		
	    return false;
	    
	}
	
	
	/**
	 * @param 微信支付是否开通
	 */
	public  boolean wxMPpay() {
		
		if(  StringUtil.isNotEmpty(this.appid) 
			 && StringUtil.isNotEmpty(this.appsecret)
			 && StringUtil.isNotEmpty(this.appidsh)
			 && StringUtil.isNotEmpty(this.appsecretsh) ){
			return true ;
	 	}
	    return false;
	    
	}

	/**
	 * @param 微信服务是否开通
	 */
	public  boolean wxMP() {
		
		if(  StringUtil.isNotEmpty(this.appid)  && StringUtil.isNotEmpty(this.appsecret)  ){
			return true ;
	 	}
	    return false;
	    
	}
	
	
}
