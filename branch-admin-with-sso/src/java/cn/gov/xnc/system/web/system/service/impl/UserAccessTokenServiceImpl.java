package cn.gov.xnc.system.web.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.system.web.system.service.UserAccessTokenServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("userAccessTokenService")
@Transactional
public class UserAccessTokenServiceImpl extends CommonServiceImpl implements UserAccessTokenServiceI {
	
}