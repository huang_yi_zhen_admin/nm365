package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 公司认证信息
 * @author zero
 * @date 2016-09-26 02:51:35
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_company_authentication", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CompanyAuthenticationEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**营业执照图片地址*/
	private java.lang.String licenseurl;
	/**营业执照号码*/
	private java.lang.String licensenum;
	/**1 未认证  2已认证 3 待审核 4 未通过*/
	private java.lang.String state;
	/**认证注册手机*/
	private java.lang.String mobilephone;
	/**税务号*/
	private java.lang.String taxno;
	/**企业法人*/
	private java.lang.String legalperson;
	/**法人代表身份证图片地址*/
	private java.lang.String cardedurl;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  营业执照图片地址
	 */
	@Column(name ="LICENSEURL",nullable=true,length=300)
	public java.lang.String getLicenseurl(){
		return this.licenseurl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  营业执照图片地址
	 */
	public void setLicenseurl(java.lang.String licenseurl){
		this.licenseurl = licenseurl;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  营业执照号码
	 */
	@Column(name ="LICENSENUM",nullable=true,length=80)
	public java.lang.String getLicensenum(){
		return this.licensenum;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  营业执照号码
	 */
	public void setLicensenum(java.lang.String licensenum){
		this.licensenum = licensenum;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  1 未认证  2已认证 3 待审核 4 未通过
	 */
	@Column(name ="STATE",nullable=true,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  1 未认证  2已认证 3 待审核 4 未通过
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  认证注册手机
	 */
	@Column(name ="MOBILEPHONE",nullable=true,length=15)
	public java.lang.String getMobilephone(){
		return this.mobilephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  认证注册手机
	 */
	public void setMobilephone(java.lang.String mobilephone){
		this.mobilephone = mobilephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  税务号
	 */
	@Column(name ="TAXNO",nullable=true,length=80)
	public java.lang.String getTaxno(){
		return this.taxno;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  税务号
	 */
	public void setTaxno(java.lang.String taxno){
		this.taxno = taxno;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  企业法人
	 */
	@Column(name ="LEGALPERSON",nullable=true,length=80)
	public java.lang.String getLegalperson(){
		return this.legalperson;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  企业法人
	 */
	public void setLegalperson(java.lang.String legalperson){
		this.legalperson = legalperson;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  法人代表身份证图片地址
	 */
	@Column(name ="CARDEDURL",nullable=true,length=300)
	public java.lang.String getCardedurl(){
		return this.cardedurl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  法人代表身份证图片地址
	 */
	public void setCardedurl(java.lang.String cardedurl){
		this.cardedurl = cardedurl;
	}
}
