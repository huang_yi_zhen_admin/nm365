

package cn.gov.xnc.system.web.system.pojo.base;

/**
 * 类 名 称： DictEntity
 * 类 描 述： 
 * 创 建 人： ljz
 *
 * 
 */
public class DictEntity {
	private String typecode;
	private String typename;
	
	public String getTypecode() {
		return typecode;
	}
	public void setTypecode(String typecode) {
		this.typecode = typecode;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
}
