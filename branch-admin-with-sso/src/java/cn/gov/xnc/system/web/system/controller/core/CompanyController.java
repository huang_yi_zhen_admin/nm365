package cn.gov.xnc.system.web.system.controller.core;




import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.message.entity.MessageTemplateEntity;
//import cn.gov.xnc.admin.message.entity.MessageTemplateEntity;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
//import cn.gov.xnc.shop.core.basket.entity.UserBasketEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.PasswordUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.core.util.oConvertUtils;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyAuthenticationEntity;
import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.CompanyReviewedEntity;
import cn.gov.xnc.system.web.system.pojo.base.CompanyThirdpartyEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserStaffEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserTypeEntity;
import cn.gov.xnc.system.web.system.service.SystemService;


/**   
 * @Title: Controller
 * @Description: 系统公司信息表
 * @author huangyz
 * @date 2016-03-03 15:34:17
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/companyController")
public class CompanyController extends BaseController {
	/**
	 * Logger for this class
	 * 
	 */
	private static final Logger logger = Logger.getLogger(CompanyController.class);


	@Autowired
	private SystemService systemService;
	
	@Autowired
	private MessageTemplateServiceI messageTemplateServiceI ;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 系统公司信息表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView tScompany(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		if(  StringUtil.isNotEmpty(user.getCompany().getId()) ){//如果不公司信息 这返回空
			TSCompany tScompany = systemService.getEntity(TSCompany.class, user.getCompany().getId());
			request.setAttribute("companyPage", tScompany);	
			CompanyPlatformEntity companyPlatform = systemService.getEntity(CompanyPlatformEntity.class, tScompany.getCompanyplatform().getId());
			request.setAttribute("companyPlatformPage", companyPlatform);	
		}else {
			TSCompany tScompany = new TSCompany();
			request.setAttribute("companyPage", tScompany);
			CompanyPlatformEntity companyPlatform = new CompanyPlatformEntity();
			request.setAttribute("companyPlatformPage", companyPlatform);	
		}
		return new ModelAndView("system/user/companyList");
	}
	

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(TSCompany tScompany,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		if(  StringUtil.isNotEmpty(user.getCompany().getId()) ){//如果不公司信息 这返回空
				tScompany.setId(user.getCompany().getId());//添加当前用户所属公司
			CriteriaQuery cq = new CriteriaQuery(TSCompany.class, dataGrid);
			
			cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tScompany);
			this.systemService.getDataGridReturn(cq, true);
			TagUtil.datagrid(response, dataGrid);
		}
		
	}

	
	/**
	 * 添加系统公司信息表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(TSCompany tScompany, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(tScompany.getId())) {
			message = "系统公司信息表更新成功";
			TSCompany t = systemService.get(TSCompany.class, tScompany.getId());
			
			if( null != tScompany.getTSTerritory() ){
				TSTerritory t1 = systemService.getEntity(TSTerritory.class, tScompany.getTSTerritory().getId());
				TSTerritory t2 = null;
				TSTerritory t3 = null;
				if( t1.getTSTerritory() != null ){
					t2 = t1.getTSTerritory();
					if( t2.getTSTerritory() != null){
						t3 = t2.getTSTerritory();
					}
				}
				String address = "";
				if( t3 != null ){
					address = t3.getTerritoryName()+"_"+t2.getTerritoryName()+"_"+t1.getTerritoryName();
				}else if( t2!=null){
					address = t2.getTerritoryName()+"_"+t1.getTerritoryName();
				}else{
					address = t1.getTerritoryName();
				}
				tScompany.setAddress(address);
			}
			
			try {
				MyBeanUtils.copyBeanNotNull2Bean(tScompany, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				
				message = "系统公司信息表更新失败";
			}
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 系统公司信息表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate( HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		if (StringUtil.isNotEmpty(user.getCompany().getId())) {
			TSCompany tScompany = systemService.getEntity(TSCompany.class, user.getCompany().getId());
			req.setAttribute("companyPage", tScompany);
			
			TSTerritory top1Territory = tScompany.getTSTerritory();
			TSTerritory top2Territory = null;
			TSTerritory top3Territory = null;
			
			if(null != top1Territory && StringUtil.isNotEmpty(top1Territory.getId())){
				top2Territory = top1Territory.getTSTerritory();
				req.setAttribute("province", top1Territory.getId());
			}
			if(null != top2Territory && StringUtil.isNotEmpty(top2Territory.getId())){
				top3Territory = top2Territory.getTSTerritory();
				req.setAttribute("province", top2Territory.getId());
				req.setAttribute("city", top1Territory.getId());
			}
			if(null != top3Territory && StringUtil.isNotEmpty(top3Territory.getId())){
				req.setAttribute("province", top3Territory.getId());
				req.setAttribute("city", top2Territory.getId());
				req.setAttribute("area", top1Territory.getId());
			}
		}
		return new ModelAndView("system/user/company");
	}

	
	
	/**
	 * 公司信息录入-申请
	 * 
	 * @param user
	 * @param req
	 * @return
	 */

	@RequestMapping(value = "saveCompany")
	@ResponseBody
	public AjaxJson saveCompany(HttpServletRequest req, TSUser user , TSCompany company) {
		AjaxJson j = new AjaxJson();

			/**
			 * 不知道为啥多一个逗号
			 */
			user.setUserName(user.getUserName().replaceAll(",", "").trim());
			String password = oConvertUtils.getString(req.getParameter("password"));
			//公司信息是否存在，用户信息是否存在
			TSUser users = null ;
			TSCompany companyOld = null ;
			TSCompany usersMobilephone = null ;
			try {
				users = systemService.findUniqueByProperty(TSUser.class, "userName",user.getUserName());
			} catch (Exception e) {
				
			}
			try {
				companyOld = systemService.findUniqueByProperty(TSCompany.class, "companyName",company.getCompanyName());
			} catch (Exception e) {
				
			}
			try {
				usersMobilephone = systemService.findUniqueByProperty(TSCompany.class, "mobilephone",company.getMobilephone());
			} catch (Exception e) {
				
			}
					
			if (users != null) {
				message = "公司登录账号: " + users.getUserName() + "已经存在！";
				j.setMsg(message);				            
	            j.setSuccess(false);
	            return j;
			} else if ( usersMobilephone != null ){
				message = "注册手机号: " + usersMobilephone.getMobilephone() + "已经存在！";
				j.setMsg(message);				            
	            j.setSuccess(false);
	            return j;
			}else if ( companyOld != null ){
				message = "公司名称: " + company.getCompanyName() + "已经存在！";
				j.setMsg(message);				            
	            j.setSuccess(false);
	            return j;
			}else {
				HttpSession session = ContextHolderUtils.getSession();
				String randCode = req.getParameter("randCode");
			    if (!randCode.equalsIgnoreCase(String.valueOf(session.getAttribute("randCodeMos")))) {//短信验证码randCodeMos
		            j.setMsg("验证码错误！");				            
		            j.setSuccess(false);
		            return j;
			    }
			    
			    if (!(company.getMobilephone()).equalsIgnoreCase(String.valueOf(session.getAttribute("randCodeMosPhonenum")))) {//接收短信验证码randCodeMos 手机号
		            j.setMsg("手机号码非接收验证码手机号！");				            
		            j.setSuccess(false);
		            return j;
			    }
			    

			    //创建公司  公司管理付费信息
			    CompanyReviewedEntity companyReviewed = new CompanyReviewedEntity();
			    	companyReviewed.setState("3");
			    	companyReviewed.setNumber(20);
			    	companyReviewed.setMsgnumber(0);
			    	companyReviewed.setMsgnumberTotal(800);
			    	Serializable companyReviewedId =	systemService.save(companyReviewed);
			    	companyReviewed.setId((String) companyReviewedId);
			    //创建公司  公司第三方接口信息
			    CompanyThirdpartyEntity companyThirdparty = new CompanyThirdpartyEntity();
			    	Serializable companyThirdpartyId = systemService.save(companyThirdparty);
			    	companyThirdparty.setId((String)companyThirdpartyId);
			    //创建公司   公司认证信息
			    CompanyAuthenticationEntity companyauthentication = new CompanyAuthenticationEntity();
			    	Serializable companyauthenticationId = systemService.save(companyauthentication);
			    	companyauthentication.setId((String) companyauthenticationId);
			    //创建公司   平台信息
			    CompanyPlatformEntity companyplatform = new CompanyPlatformEntity();
			    	Serializable companyplatformId = systemService.save(companyplatform);
			    	companyplatform.setId((String) companyplatformId);
			    
			    //创建公司信息
			    	company.setCompanyreviewed(companyReviewed);
			    	company.setCompanythirdparty(companyThirdparty);
			    	company.setCompanyauthentication(companyauthentication);
			    	company.setCompanyplatform(companyplatform);
			   Serializable companyid = systemService.save(company);//添加公司信息	
			   		company.setId((String) companyid);
			   
			   //员工账号基本信息 	
			   UserStaffEntity tsuserstaff = new UserStaffEntity();	
			   		tsuserstaff.setDepartment("管理");
			   		tsuserstaff.setPosition("管理");
			   		tsuserstaff.setUsertpyrid("创始账号");
			   		tsuserstaff.setRemarks("创始账号");
			   Serializable tsuserstaffId = systemService.save(tsuserstaff);
			   		tsuserstaff.setId((String) tsuserstaffId);
			  //账户信息 		
			  UserSalesmanEntity tsuserSalesman =new UserSalesmanEntity();
			  Serializable userSalesmanId = systemService.save(tsuserSalesman);
			  		tsuserSalesman.setId((String) userSalesmanId);
			  	
			    
			  	user.setTsuserSalesman(tsuserSalesman);
			  	user.setTsuserstaff(tsuserstaff);
			  		
			    //创建公司  管理员账号
					user.setPassword(PasswordUtil.md5(password));
					user.setStatus(Globals.User_status_1);
					user.setType(Globals.User_type_1);
					user.setCompany(company);
					user.setRealname("管理员");
					
				Serializable userid = systemService.save(user);//添加注册用户为企业管理员
					user.setId((String) userid);
				
				
					try {
						//初始化公司模板数据	
						List<MessageTemplateEntity> addmtList = new ArrayList<MessageTemplateEntity>();
						TSCompany companyMessageTemplate = new TSCompany();
						companyMessageTemplate.setId("402881e757a4fb650157a4fddf730004");
						List<MessageTemplateEntity> messageTemplateList = systemService.findByProperty(MessageTemplateEntity.class, "company",companyMessageTemplate);	
						if( messageTemplateList != null && messageTemplateList.size() > 0 ){
							for(  MessageTemplateEntity   t :  messageTemplateList){
								MessageTemplateEntity newTemlate = new MessageTemplateEntity() ;
								newTemlate = new MessageTemplateEntity();
								newTemlate.setType(t.getType());
								newTemlate.setSettype(t.getSettype());
								newTemlate.setState(t.getState());
								newTemlate.setSmsnumber(0);
								newTemlate.setContent(t.getContent());
								newTemlate.setInstructions(t.getInstructions());
								newTemlate.setCompany(company);
								addmtList.add(newTemlate);
							}
						}
						systemService.batchSave(addmtList);
						
					} catch (Exception e) {
						systemService.addLog("初始化公司短信模板出错，" + e.toString(), Globals.Log_Leavel_ERROR, Globals.Log_Type_INSERT);
					}
				
					//发送短信通知：：
					String msg ="恭喜您已经成功注册云农贸订单系统，请耐心等待审核！有疑问请登录官网联系在线客服。";
					
					messageTemplateServiceI.sendSMSTTMM("", "", company.getMobilephone(), "云农贸", msg);
					//发送后台处理短信
					
					String msgAdmin =company.getCompanyName()+"申请开通系统，请及时处理。";
					
					String adminMobilephone[] ="15338964955,13698908198".split(","); 
					
					for( int i= 0 ; i<adminMobilephone.length ; i++  ){
						messageTemplateServiceI.sendSMSTTMM("", "", adminMobilephone[i].toString(), "天天农贸", msgAdmin);
					}
					
				
					
				message = "公司名称: " + company.getCompanyName() + "创建成功！请等待后台的审核！";
				systemService.addLog(message, Globals.Log_Leavel_INFO, Globals.Log_Type_INSERT);
			}
		j.setMsg(message);
		return j;
	}
	
	
	//公司是否开通支付
	/**
	 * 公司信息录入-申请
	 * 
	 * @param user
	 * @param req
	 * @return
	 */

	@RequestMapping(value = "zhifuCompany")
	@ResponseBody
	public AjaxJson zhifuCompany(HttpServletRequest req, TSCompany company) {
		AjaxJson j = new AjaxJson();
		
		String cpid = req.getParameter("cpid");//公司id
		String tyid = req.getParameter("tyid");//支付类型
		
		
		return j;
		
	}
	
	@RequestMapping(value = "getUserGradeList")
	@ResponseBody
	public AjaxJson getUserGrade(HttpServletRequest request) {
		AjaxJson json = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = user.getCompany();
		CriteriaQuery cq = new CriteriaQuery(UserTypeEntity.class);
	 	cq.eq("company", company); 
	 	cq.eq("type", "1");
	 	cq.add() ;
		List<UserTypeEntity> userTypelist = systemService.getListByCriteriaQuery(cq, false);
		json.setObj(userTypelist);
		
		return json;
	}
}
