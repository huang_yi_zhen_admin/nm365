package cn.gov.xnc.system.web.system.service.impl;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.PasswordUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;
import cn.gov.xnc.system.web.system.service.UserService;

/**
 * 
 * @author  zero
 *
 */
@Service("userService")

public class UserServiceImpl extends CommonServiceImpl implements UserService {

//	public TSUser checkUserExits(TSUser user  ){
//		return this.commonDao.getUserByUserIdAndUserNameExits(user );
//	}
	
	public String getUserRole(TSUser user){
		return this.commonDao.getUserRole(user);
	}
	
	public void pwdInit(TSUser user,String newPwd) {
			this.commonDao.pwdInit(user,newPwd);
	}
	
	public int getUsersOfThisRole(String id) {
		Criteria criteria = getSession().createCriteria(TSRoleUser.class);
		criteria.add(Restrictions.eq("TSRole.id", id));
		int allCounts = ((Long) criteria.setProjection(
				Projections.rowCount()).uniqueResult()).intValue();
		return allCounts;
	}

	
	public List<TSUser> findListUserByYW(String companyid  ) {
		
		String sql = "select * from t_s_user where companyID  ='"+companyid+"' and  type='2' and status=1";	
		return this.queryListByJdbc(sql,TSUser.class);
	}
	/**
	 *检测账号是否存在 
	 * 
	 */
	public  boolean checkuseradd( String companyid , String  userName ){
		
		TSUser user = null;
		try {
			TSCompany company = new TSCompany();
				company.setId(companyid);
			CriteriaQuery cq = new CriteriaQuery(TSUser.class);
			 		cq.eq("userName", userName);
			 		cq.eq("company", company);
			 		cq.add();
			 	user = (TSUser) getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色

		} catch (Exception e) {
			 e.printStackTrace(); 
		}
		
		if(user == null ){
			return false;
		}else {
			return true;
	   }	
	}
	
	
	/**
	 * 登陆用户检查
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public TSUser checkUserExits(TSUser user , TSCompany  company) throws Exception {
		
		
   			
   			String password = PasswordUtil.md5(user.getPassword());
   			CriteriaQuery cq = new CriteriaQuery(TSUser.class);
   			 		cq.eq("userName", user.getUserName());
   			 		cq.eq("company", company);
   			 		cq.eq("password", password);
   			 		cq.add();
   			TSUser u  = (TSUser) getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色
   		
		
		return u;

	}

	@Override
	public StatisPageVO staticsUserClientAccountMoney(TSCompany company, TSUser user, HttpServletRequest request) {
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append(" SELECT SUM(b.useMoney) value1,count(1) value4 FROM t_s_user a,xnc_user_salesman b ");
		Sql.append(" WHERE a.company = '" + company.getId() + "'");
		Sql.append(" AND a.tsuserSalesman = b.id");
		
		if(null != user && StringUtil.isNotEmpty(user.getId())){
			Sql.append(" AND a.id ='" + user.getId() + "'");
		}
		 

		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		
		return statisPageVO;
	}

	@Override
	public List<TSUser> findUserList(String companyid, String[] userType) {
		CriteriaQuery cq = new CriteriaQuery(TSUser.class);
		if(StringUtil.isNotEmpty(companyid)){
			cq.eq("company.id", companyid);
		}
		
		if(userType != null){
			cq.in("type", userType);
		}
		
		cq.add();
		List<TSUser> list = this.getListByCriteriaQuery(cq,false);
		return list;
	}
	
	
//	/**
//	 * 查询该区域下所有的用户
//	 * @return
//	 */
//	public List<TSUser> findListUserByTerritory(String territoryId){		
//		String sql = "select username from t_s_base_user where territoryid  = ?";
//		return this.findListByJdbc(sql, TSBaseUser.class, new Object[]{territoryId});				
//	}
}
