package cn.gov.xnc.test;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public abstract class BaseLoginJunit extends BaseJunit{
	
    private MockHttpSession session;

    protected MockHttpSession getSession()
    {
        return session;
    }

    /**
     * 测试前，初始化系统登录
     * @see
     */
    @Before
    public void setUp()
    {
        super.setUp();
        this.session = (MockHttpSession) getLoginSession();
    }

    /**
     * 完成登录功能，返回当前登录会话
     * 
     * @return HttpSession
     * @see
     */
    private HttpSession getLoginSession()
    {
        String url = "/loginController/login";
        String params = "{\"userName\":\"adminxnc\",\"password\":\"123456h\",\"verifyCode\":\"1256\"}";
        MvcResult result = null;
        try
        {
            result = getMockMvc().perform(MockMvcRequestBuilders.post(url).accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON).content(params))
            	.andExpect(MockMvcResultMatchers.status().isOk())
            	.andDo(MockMvcResultHandlers.print()).andReturn();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return result.getRequest().getSession();
    }
}
