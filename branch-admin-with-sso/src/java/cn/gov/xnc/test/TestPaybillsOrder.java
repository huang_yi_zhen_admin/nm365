package cn.gov.xnc.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration({"classpath*:/spring-mvc-aop.xml","classpath*:/spring-mvc-context.xml","classpath*:/spring-mvc-hibernate.xml", "classpath*:/spring-mvc.xml"}) 
@Transactional 
public class TestPaybillsOrder {

	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	
	@Test
	public void testCancelBillsorder(){
		PayBillsOrderEntity paybillsOrder = new PayBillsOrderEntity();
		
		try {
			paybillsOrder.setId("8af4a3da58175067015819b18da10081");
			payBillsOrderService.cancelPayBillsOrder(paybillsOrder, null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
