package cn.gov.xnc.test;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class) 
@WebAppConfiguration
@ContextConfiguration({"classpath*:/spring-mvc-aop.xml",
	"classpath*:/spring-mvc-context.xml",
	"classpath*:/spring-mvc-hibernate.xml", 
	"classpath*:/spring-mvc.xml"}) 
public abstract class BaseJunit {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    protected WebApplicationContext getWac()
    {
        return this.wac;
    }

    protected MockMvc getMockMvc()
    {
        return this.mockMvc;
    }

    /**
     * 初始化mocMvc
     */
    @Before
    public void setUp()
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
}
