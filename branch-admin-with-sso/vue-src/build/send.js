var path = require('path')
var webpack = require('webpack')
var ora = require('ora')
var merge = require('webpack-merge')
var chalk = require('chalk')
var CopyWebpackPlugin = require('copy-webpack-plugin')



const distDir = '/Users/zhangkun/Desktop/work/nm365/branch1/vue-src/';
const proDir = '/Users/zhangkun/Desktop/work/nm365/branch1/WebContent/';
var spinner = ora('building for production...')
spinner.start();

var webpackConfig = {

    plugins: [
        new CopyWebpackPlugin([{
            from: path.join(__dirname, '../dist/index.html'),
            to: proDir + 'webpage/m/',
            ignore: ['.*']
        }, {
            from: path.join(__dirname, '../dist/'),
            to: proDir + 'mobile/',
            toType: 'dir',
            ignore: ['.*']

        }])
    ]
};
webpack(webpackConfig, function(err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
        colors: true,
        modules: false,
        children: false,
        chunks: false,
        chunkModules: false
    }) + '\n\n')

    console.log(chalk.cyan('  Build complete.\n'))

})