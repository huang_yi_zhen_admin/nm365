import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '@/views/homeView'

Vue.use(Router)

const scrollBehavior = (to, from, savedPosition) => {
    if (savedPosition) {
        // savedPosition is only available for popstate navigations.
        return savedPosition
    } else {
        const position = {}
            // new navigation.
            // scroll to anchor by returning the selector
        if (to.hash) {
            position.selector = to.hash
        }
        // check if any matched route config has meta that requires scrolling to top
        if (to.matched.some(m => m.meta.scrollToTop)) {
            // cords will be used if no selector is provided,
            // or if the selector didn't match any element.
            position.x = 0
            position.y = 0
        }
        // if the returned position is falsy or an empty object,
        // will retain current scroll position.)

        return position
    }
}

export default new Router({
    mode: 'history',
    //saveScrollPosition: true,
    scrollBehavior,
    routes: [{
            path: '/m/home',
            name: 'home',
            component: HomeView
        },
        {
            path: '/m/goodsList',
            name: 'goodsList',
            component: resolve => require(['../views/goods/goodsList.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m/goodsDetail',
            name: 'goodsDetail',
            component: resolve => require(['../views/goods/detail.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m/goodsBuy',
            name: 'goodsBuy',
            component: resolve => require(['../views/goods/buy.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m/cart',
            name: 'cart',
            component: resolve => require(['../views/cart/cart.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m/order',
            name: 'order',
            component: resolve => require(['../views/order/order.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m/orderDetail',
            name: 'orderDetail',
            component: resolve => require(['../views/order/orderDetail.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m',
            redirect: '/m/home'
        }
    ]
})