import { fetch, fetchProvice, fetchZoneById, getSaleMan, getLev, getDetailById, getListByType, getListById, getCopartner, submitData, getStatDataByType } from './fetch'
//获取列表信息
export function FETCH_STATE_LIST_DATA({ commit, dispatch, state }, { data, listName }) {
    let listType = listName;
    if (listName == "salersAudit" || listName == "payAudit" || listName == 'moneyAudit' || listName == "orderAudit") {
        listType = listName + "_" + data.state;
        if (data.state == 0) {
            data.state = "";
        }
    }
    if (listName == 'cashList') {
        listType = listName + "_" + data.ordertype
    }
    let arg = state[listType];

    data['showNum'] = arg['pageSize'];
    data['pageNum'] = arg['pageNo'];

    return new Promise((resolve, reject) => {
        function ok(ret) {
            commit('SET_STATE_LIST_DATA', {
                data: ret.data,
                listName: listType
            });
            resolve(ret);
        }

        if (data.getListById) { //根据id获取详情
            getListById(listName, { id: data.id, pageNo: arg['pageNo'], pageSize: arg['pageSize'] }).then(ret => {
                ok(ret);
            })
        } else {
            //获取列表
            getListByType(listName, data).then(ret => {
                ok(ret);
            })
        }
    })
}
//提交数据
export function SUBMIT_DATA({ dispatch }, { type, data }) {
    return submitData(type, data);
}

//获取省份信息，并缓存
export function FETCH_STATE_PROVICE_DATA({ commit, dispatch, state }) {

    return new Promise((resolve, reject) => {
        if (state.proviceData.length) {
            resolve(state.proviceData);
        } else {
            fetchProvice().then(ret => {
                commit('SET_STATE_PROVINCE_DATA', {
                    data: ret
                });
                resolve(ret)
            })
        }
    })
}
//根据区域id，获取区域信息
export function FETCH_ZONE_DATA_BY_ID({ state }, { id }) {
    return fetchZoneById(id)
}
//获取业务员数据
export function FETCH_SALEMAN_DATA() {
    return getSaleMan();
}
//获取用户等级
export function FETCH_LEVEL_DATA() {
    return getLev();
}
//获取详情接口
export function FETCH_DETAIL_DATA({ state }, { id, type }) {
    return getDetailById({ id, type });
}
//获取发货商
export function FETCH_COPARTNER_DATA() {
    return getCopartner();
}
//获取统计数据 
export function FETCH_STATE_DATA({ state }, { type, data }) {
    return getStatDataByType(type, data)
}
//获取用户的权限信息
export function FETCH_USER_POWER_DATA({ state, commit }) {
    if (state['power']['waitCheck']) { //尚未校验
        state['power']['waitCheck'] = 0;
        getListByType('powerList').then(ret => {
            commit('SET_USER_POWER', { 'data': ret })
        });
    }
}