<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>发货地址</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="companyExpressController.do?save">
		<input id="id" name="id" type="hidden" value="${companyExpressPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">company表id:</label>
		      <input class="inputxt" id="companyid" name="companyid" ignore="ignore"
					   value="${companyExpressPage.companyid}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">省份，不能漏省字:</label>
		      <input class="inputxt" id="province" name="province" ignore="ignore"
					   value="${companyExpressPage.province}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">城市，不能漏市字:</label>
		      <input class="inputxt" id="city" name="city" ignore="ignore"
					   value="${companyExpressPage.city}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">不能漏区字:</label>
		      <input class="inputxt" id="area" name="area" ignore="ignore"
					   value="${companyExpressPage.area}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">详细地址:</label>
		      <input class="inputxt" id="address" name="address" ignore="ignore"
					   value="${companyExpressPage.address}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">快递商编码:</label>
		      <input class="inputxt" id="shippercode" name="shippercode" ignore="ignore"
					   value="${companyExpressPage.shippercode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">快递类型:</label>
		      <input class="inputxt" id="exptype" name="exptype" ignore="ignore"
					   value="${companyExpressPage.exptype}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">快递网点标识_由快递网点提供:</label>
		      <input class="inputxt" id="sendsite" name="sendsite" ignore="ignore"
					   value="${companyExpressPage.sendsite}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电子面单账号 由快递网点提供 :</label>
		      <input class="inputxt" id="customername" name="customername" ignore="ignore"
					   value="${companyExpressPage.customername}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电子面单密码 由快递网点提供 :</label>
		      <input class="inputxt" id="customerpwd" name="customerpwd" ignore="ignore"
					   value="${companyExpressPage.customerpwd}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">月结编码_由快递网点提供 :</label>
		      <input class="inputxt" id="monthcode" name="monthcode" ignore="ignore"
					   value="${companyExpressPage.monthcode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">支付类型1-现付，2-到付，3-月结，4-第三方支付:</label>
		      <input class="inputxt" id="paytype" name="paytype" ignore="ignore"
					   value="${companyExpressPage.paytype}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0:</label>
		      <input class="inputxt" id="isnotice" name="isnotice" ignore="ignore"
					   value="${companyExpressPage.isnotice}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件人公司:</label>
		      <input class="inputxt" id="company" name="company" ignore="ignore"
					   value="${companyExpressPage.company}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件人姓名:</label>
		      <input class="inputxt" id="name" name="name" ignore="ignore"
					   value="${companyExpressPage.name}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件人电话:</label>
		      <input class="inputxt" id="tel" name="tel" ignore="ignore"
					   value="${companyExpressPage.tel}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件人手机:</label>
		      <input class="inputxt" id="mobile" name="mobile" ignore="ignore"
					   value="${companyExpressPage.mobile}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发货点邮政编码:</label>
		      <input class="inputxt" id="postcode" name="postcode" ignore="ignore"
					   value="${companyExpressPage.postcode}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>