<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>出库详细单</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="stockIOOrderDetailController.do?save">
		<input id="id" name="id" type="hidden" value="${stockIOOrderDetailPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">出入库商品编码:</label>
		      <input class="inputxt" id="productid" name="productid" ignore="ignore"
					   value="${stockIOOrderDetailPage.productid}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">出、入库数量:</label>
		      <input class="inputxt" id="iostocknum" name="iostocknum" 
					   value="${stockIOOrderDetailPage.iostocknum}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">进货单价:</label>
		      <input class="inputxt" id="unitprice" name="unitprice" 
					   value="${stockIOOrderDetailPage.unitprice}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">总成本:</label>
		      <input class="inputxt" id="totalprice" name="totalprice" 
					   value="${stockIOOrderDetailPage.totalprice}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">备注:</label>
		      <input class="inputxt" id="remark" name="remark" ignore="ignore"
					   value="${stockIOOrderDetailPage.remark}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">出入库单编码:</label>
		      <input class="inputxt" id="stockorderid" name="stockorderid" 
					   value="${stockIOOrderDetailPage.stockorderid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>