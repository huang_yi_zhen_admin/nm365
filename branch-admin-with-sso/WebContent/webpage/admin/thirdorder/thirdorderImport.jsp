<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>第三方订单导入记录</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="thirdorderImportController.do?save">
		<input id="id" name="id" type="hidden" value="${thirdorderImportPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">文件名:</label>
		      <input class="inputxt" id="filename" name="filename" ignore="ignore"
					   value="${thirdorderImportPage.filename}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">文件存储服务器全路径:</label>
		      <input class="inputxt" id="fullpath" name="fullpath" ignore="ignore"
					   value="${thirdorderImportPage.fullpath}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">渠道id:</label>
		      <input class="inputxt" id="channelid" name="channelid" ignore="ignore"
					   value="${thirdorderImportPage.channelid}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">创建时间:</label>
		      <input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="createtime" name="createtime" ignore="ignore"
					     value="<fmt:formatDate value='${thirdorderImportPage.createtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">创建者:</label>
		      <input class="inputxt" id="createuser" name="createuser" ignore="ignore"
					   value="${thirdorderImportPage.createuser}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>