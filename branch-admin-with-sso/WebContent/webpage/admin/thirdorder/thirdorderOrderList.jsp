<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="thirdorderOrderList" title="第三方订单" actionUrl="thirdorderOrderController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="渠道id" field="channelid" ></z:dgCol>
   <z:dgCol title="导入记录表id" field="importid" ></z:dgCol>
   <z:dgCol title="导入的订单id" field="ogOrderid" ></z:dgCol>
   <z:dgCol title="导入的产品编码" field="ogProductcode" ></z:dgCol>
   <z:dgCol title="导入的产品名称" field="ogProductname" ></z:dgCol>
   <z:dgCol title="导入的产品数量" field="ogProductnum" ></z:dgCol>
   <z:dgCol title="导入的订单总价格" field="ogOrderprice" ></z:dgCol>
   <z:dgCol title="导入的下单时间" field="ogOrdertime" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="导入的订单支付时间" field="ogOrderpaytime" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="导入的购买人" field="ogBuyer" ></z:dgCol>
   <z:dgCol title="导入的收货地址" field="ogBuyeraddress" ></z:dgCol>
   <z:dgCol title="导入的购买人电话" field="ogBuyermobile" ></z:dgCol>
   <z:dgCol title="导入的运费" field="ogFreight" ></z:dgCol>
   <z:dgCol title="导入的物流公司" field="ogFreightcompany" ></z:dgCol>
   <z:dgCol title="导入的快递编码" field="ogFreightcode" ></z:dgCol>
   <z:dgCol title="导入的订单状态" field="ogStatus" ></z:dgCol>
   <z:dgCol title="本系统物流公司编码" field="shippercode" ></z:dgCol>
   <z:dgCol title="电子面单号码" field="shipernum" ></z:dgCol>
   <z:dgCol title="创建时间" field="createtime" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="更新时间" field="updatetime" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="状态 1导入，2快递下单，3已揽收，4签收，5已删除" field="status" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="thirdorderOrderController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="thirdorderOrderController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="thirdorderOrderController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="thirdorderOrderController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>