

jQuery("#validatepicture").click(function () { RefreshImage() });


//点击登录
jQuery('#BT_Login').click(function(e) {
	if(checkform()){
		submitForm();
	}
});

//回车登录
jQuery(document).keydown(function(e){
	if (e.keyCode == 13) { 
		if(checkform()){
			submitForm();
		}
		
	}

});


function submitForm(){
	
	/**
	 * 设置按钮不可用
	 */
	jQuery('#BT_Login').attr('disabled',true);
	
	var actionurl=jQuery('form').attr('action');//提交路径
	var checkurl=jQuery('form').attr('check');//验证路径
	 var formData = new Object();
	var data=jQuery(":input").each(function() {
		 formData[this.name] =jQuery("#"+this.name ).val();
	});
	jQuery.ajax({
		async : false,
		cache : false,
		type : 'POST',		
		url : checkurl,// 请求的action路径
		data : formData,
		error : function (xhr, type, exception) {// 请求失败处理函数
			alertMsg("系统有误,请联系管理员....."); 
			jQuery('#BT_Login').attr('disabled',false);
		},
		success : function(data) {
			if (data.success) {
				setTimeout("window.location.href='"+actionurl+"'", 1000);
			} else {
				alertMsg(data.msg); 
				if(data.msg && data.msg.indexOf("采购商") > -1){
					setTimeout("window.location.href='"+data.obj+"'", 3000);
				}
				if(data.msg && data.msg.indexOf("管理端") > -1){
					setTimeout("window.location.href='"+data.obj+"'", 3000);
				}
				jQuery("#userName").focus();
				jQuery('#BT_Login').attr('disabled',false);
			}
		}
	});
}

function checkform() {
	var UserName = jQuery("#userName").val();
	var password = jQuery("#password").val();
	var validate = jQuery("#randCode").val();
	
	
	
	if (UserName == "") {
		alertMsg("请输入管理帐号!");
		jQuery("#userName").focus();
		return false;
	}
	if (password == "") {
		alertMsg("请输入登录密码!");
		jQuery("#password").focus();
		return false;
	}
	if (validate == "") {
		alertMsg("请输入验证码!");
		jQuery("#randCode").focus();
		return false;
	}
	return true;
}
function RefreshImage() {
	 var date = new Date();
	var el = document.getElementById("validatepicture");
	el.src='/randCodeImage?a=' + date.getTime();
}
function alertMsg( msg ){
	jQuery(".tips").html(msg);
	var div = jQuery(".tips").closest('.form-group');
		div.addClass('has-error');

	jQuery('#alertinfo').fadeIn(500,function(){});
	setTimeout( function(){jQuery('#alertinfo').fadeOut(function(){});div.removeClass('has-error')},4000);
}
function tip(_tID){
	jQuery(_tID).fadeIn(500,function(){});
	setTimeout( function(){jQuery(_tID).fadeOut(function(){})},4000);
	
}