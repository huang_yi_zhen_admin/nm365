<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<html>
	<head>
		<title></title>		
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<link rel="stylesheet" href="plug-in/login/css/error.css" type="text/css" />	
	</head>
	<body>
	    <div class="errorbd">
			<h2><%=request.getAttribute("msg") %></h2>
		</div>		
	</body>
</html>
