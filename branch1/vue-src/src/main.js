// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { sync } from 'vuex-router-sync'
import store from './store'
import mixins from './mixins'
import * as filters from './filters'
// register global mixins.
Vue.mixin(mixins);

// register global utility filters.
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

import './assets/style.scss'


Vue.config.productionTip = false

sync(store, router);

router.afterEach((to, from, next) => {
    store.dispatch('FETCH_USER_POWER_DATA');
})

/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    router,
    template: '<App/>',
    components: { App }
})

;