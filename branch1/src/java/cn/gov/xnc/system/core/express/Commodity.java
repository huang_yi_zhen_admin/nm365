/**
 * 
 */
package cn.gov.xnc.system.core.express;

/**
 * @author Administrator
 *
 */
public class Commodity {

	/*
	 * 必填，商品名称
	 */
	private String GoodsName;//必填，商品名称
	
	/*
	 * 商品编码
	 */
	private String GoodsCode;//商品编码
	
	/*
	 * 件数
	 */
	private int Goodsquantity;//件数
	
	/*
	 * 商品价格
	 */
	private double GoodsPrice;//商品价格
	
	/*
	 * 商品重量kg
	 */
	private double GoodsWeight;//商品重量kg
	
	/*
	 * 商品描述
	 */
	private String GoodsDesc;//商品描述
	
	/*
	 * 商品体积m3
	 */
	private double GoodsVol;//商品体积m3

	/**
	 * 必填，商品名称
	 * @return the goodsName
	 */
	public String getGoodsName() {
		return GoodsName;
	}

	/**
	 * 必填，商品名称
	 * @param goodsName the goodsName to set
	 */
	public void setGoodsName(String goodsName) {
		GoodsName = goodsName;
	}

	/**
	 * 商品编码
	 * @return the goodsCode
	 */
	public String getGoodsCode() {
		return GoodsCode;
	}

	/**
	 * 商品编码
	 * @param goodsCode the goodsCode to set
	 */
	public void setGoodsCode(String goodsCode) {
		GoodsCode = goodsCode;
	}

	/**
	 * 件数
	 * @return the goodsquantity
	 */
	public int getGoodsquantity() {
		return Goodsquantity;
	}

	/**
	 * 件数
	 * @param goodsquantity the goodsquantity to set
	 */
	public void setGoodsquantity(int goodsquantity) {
		Goodsquantity = goodsquantity;
	}

	/**
	 * 商品价格
	 * @return the goodsPrice
	 */
	public double getGoodsPrice() {
		return GoodsPrice;
	}

	/**
	 * 商品价格
	 * @param goodsPrice the goodsPrice to set
	 */
	public void setGoodsPrice(double goodsPrice) {
		GoodsPrice = goodsPrice;
	}

	/**
	 * 商品重量kg
	 * @return the goodsWeight
	 */
	public double getGoodsWeight() {
		return GoodsWeight;
	}

	/**
	 * 商品重量kg
	 * @param goodsWeight the goodsWeight to set
	 */
	public void setGoodsWeight(double goodsWeight) {
		GoodsWeight = goodsWeight;
	}

	/**
	 * 商品描述
	 * @return the goodsDesc
	 */
	public String getGoodsDesc() {
		return GoodsDesc;
	}

	/**
	 * 商品描述
	 * @param goodsDesc the goodsDesc to set
	 */
	public void setGoodsDesc(String goodsDesc) {
		GoodsDesc = goodsDesc;
	}

	/**
	 * 商品体积m3
	 * @return the goodsVol
	 */
	public double getGoodsVol() {
		return GoodsVol;
	}

	/**
	 * 商品体积m3
	 * @param goodsVol the goodsVol to set
	 */
	public void setGoodsVol(double goodsVol) {
		GoodsVol = goodsVol;
	}
	
}
