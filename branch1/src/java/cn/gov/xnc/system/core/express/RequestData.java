package cn.gov.xnc.system.core.express;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class RequestData {

	/*
	 * 用户自定义回调信息
	 */
	private String CallBack;//用户自定义回调信息
	
	/*
	 * 会员标识平台方与快递鸟统一用户标识的商家ID
	 */
	private String MemberID;//会员标识平台方与快递鸟统一用户标识的商家ID
	
	/*
	 * 电子面单客户账号（与快递网点申请）
	 */
	private String CustomerName;//电子面单客户账号（与快递网点申请）
	
	/*
	 * 电子面单密码
	 */
	private String CustomerPwd;//电子面单密码
	
	/*
	 * 收件网点标识
	 */
	private String SendSite;//收件网点标识
	
	/*
	 * 必填！快递公司编码
	 */
	private String ShipperCode;//必填！快递公司编码
	
	/*
	 * 快递单号,一般不填
	 */
	private String LogisticCode;//快递单号,一般不填
	
	/*
	 * 第三方订单号
	 */
	private String ThrOrderCode;//第三方订单号
	
	/*
	 * 必填！订单编号
	 */
	private String OrderCode;//必填！订单编号
	
	/*
	 * 月结编码
	 */
	private String MonthCode;//月结编码
	
	/*
	 * 必填！邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
	 */
	private int PayType;//必填！邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
	
	/*
	 * 必填！快递类型：1-标准快件
	 */
	private int ExpType;//必填！快递类型：1-标准快件
	
	/*
	 * 是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 */
	private int IsNotice;//是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	
	/*
	 * 寄件费（运费）
	 */
	private double Cost;//寄件费（运费）
	
	/*
	 * 其他费用
	 */
	private double OtherCost;//其他费用
	
	/*
	 * 收件人信息
	 */
	private Receiver receiver;//收件人信息
	
	/*
	 * 发件人信息
	 */
	private Sender sender;//发件人信息
	
	/*
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 */
	private String StartDate;//上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	
	/*
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 */
	private String EndDate;//上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	
	/*
	 * 物品总重量kg
	 */
	private double Weight;//物品总重量kg
	
	/*
	 * 件数/包裹数
	 */
	private int Quantity;//件数/包裹数
	
	/*
	 * 物品总体积m3
	 */
	private double Volume;//物品总体积m3
	
	/*
	 * 备注
	 */
	private String Remark;//备注
	
	/*
	 * 增值服务
	 */
	private AddService addService;//增值服务
	
	/*
	 * 货物信息
	 */
	private List<Commodity> commodityList;//货物信息
	
	/*
	 * 返回电子面单模板：0-不需要；1-需要
	 */
	private String IsReturnPrintTemplate;//返回电子面单模板：0-不需要；1-需要
	
	
	
	
	
	public String toJsonStr(){
		JSONObject obj = new JSONObject();
		if( CallBack != null ){
			obj.put("CallBack", CallBack);
		}
		if( MemberID != null ){
			obj.put("MemberID", MemberID);
		}
		if( CustomerName != null ){
			obj.put("CustomerName", CustomerName);
		}
		if( CustomerPwd != null ){
			obj.put("CustomerPwd", CustomerPwd);
		}
		if( SendSite != null ){
			obj.put("SendSite", SendSite);
		}
		if( ShipperCode != null ){
			obj.put("ShipperCode", ShipperCode);
		}
		if( LogisticCode != null ){
			obj.put("LogisticCode", LogisticCode);
		}
		if( ThrOrderCode != null ){
			obj.put("ThrOrderCode", ThrOrderCode);
		}
		if( OrderCode != null ){
			obj.put("OrderCode", OrderCode);
		}
		if( MonthCode != null ){
			obj.put("MonthCode", MonthCode);
		}
		obj.put("PayType", PayType);
		
		obj.put("ExpType", ExpType);
		
		obj.put("IsNotice", IsNotice);
		obj.put("Cost", Cost);
		obj.put("OtherCost", OtherCost);
		
		if( null!=receiver){
			JSONObject receiverObj = new JSONObject();
			receiverObj.put("Company", receiver.getCompany() );
			receiverObj.put("Name", receiver.getName() );
			receiverObj.put("Tel", receiver.getTel());
			receiverObj.put("Mobile", receiver.getMobile());
			receiverObj.put("ProvinceName", receiver.getProvinceName());
			receiverObj.put("CityName", receiver.getCityName());
			receiverObj.put("ExpAreaName", receiver.getExpAreaName());
			receiverObj.put("Address", receiver.getAddress());
			obj.put("Receiver", receiverObj);
		}
		
		if(null!=sender){
			JSONObject senderObj = new JSONObject();
			senderObj.put("Company", sender.getCompany());
			senderObj.put("Name", sender.getName() );
			senderObj.put("Tel", sender.getTel());
			senderObj.put("Mobile", sender.getMobile());
			senderObj.put("ProvinceName", sender.getProvinceName());
			senderObj.put("CityName", sender.getCityName());
			senderObj.put("ExpAreaName", sender.getExpAreaName());
			senderObj.put("Address", sender.getAddress());
			obj.put("Sender", senderObj);
		}
		
		if( StartDate != null ){
			obj.put("StartDate", StartDate);
		}
		if( EndDate!=null ){
			obj.put("EndDate", EndDate);
		}
		if( Weight > 0 ){
			obj.put("Weight", Weight);
		}
		if( Quantity>0 ){
			obj.put("Quantity", Quantity);
		}
		if( Volume>0){
			obj.put("Volume", Volume);
		}
		if(Remark!=null){
			obj.put("Remark", Remark);
		}
		
		if( addService != null ){
			JSONObject AddServiceObj = new JSONObject();
			if( addService.getName() !=null ){
				AddServiceObj.put("Name", addService.getName());
			}
			if(addService.getValue() != null){
				AddServiceObj.put("Value", addService.getValue());
			}
			if(addService.getCustomerID() != null){
				AddServiceObj.put("CustomerID", addService.getCustomerID());
			}
			obj.put("AddService", AddServiceObj);
		}
		
		if( commodityList != null && commodityList.size()>0){
			JSONArray commodityListObj = new JSONArray();
			commodityListObj.addAll(commodityList);
			/*if( commodity.getGoodsName() != null ){
				commodityObj.put("GoodsName", commodity.getGoodsName());
			}
			if( commodity.getGoodsCode() != null ){
				commodityObj.put("GoodsCode", commodity.getGoodsCode());
			}
			if( commodity.getGoodsquantity() > 0 ){
				commodityObj.put("Goodsquantity", commodity.getGoodsquantity());
			}
			if( commodity.getGoodsPrice() > 0 ){
				commodityObj.put("GoodsPrice", commodity.getGoodsPrice());
			}
			if( commodity.getGoodsWeight() > 0){
				commodityObj.put("GoodsWeight", commodity.getGoodsWeight());
			}
			if( commodity.getGoodsDesc() != null ){
				commodityObj.put("GoodsDesc", commodity.getGoodsDesc());
			}
			if( commodity.getGoodsVol() > 0 ){
				commodityObj.put("GoodsVol", commodity.getGoodsVol());
			}*/
			obj.put("Commodity", commodityListObj);
		}
		
		
		obj.put("IsReturnPrintTemplate", IsReturnPrintTemplate);
		
		return obj.toJSONString();
	}

	/**
	 * 用户自定义回调信息
	 * @return the callBack
	 */
	public String getCallBack() {
		return CallBack;
	}

	/**
	 * 用户自定义回调信息
	 * @param callBack the callBack to set
	 */
	public void setCallBack(String callBack) {
		CallBack = callBack;
	}

	/**
	 * 会员标识平台方与快递鸟统一用户标识的商家ID
	 * @return the memberID
	 */
	public String getMemberID() {
		return MemberID;
	}

	/**
	 * 会员标识平台方与快递鸟统一用户标识的商家ID
	 * @param memberID the memberID to set
	 */
	public void setMemberID(String memberID) {
		MemberID = memberID;
	}

	/**
	 * 电子面单客户账号（与快递网点申请）
	 * @return the customerName
	 */
	public String getCustomerName() {
		return CustomerName;
	}

	/**
	 * 电子面单客户账号（与快递网点申请）
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	/**
	 * 电子面单密码
	 * @return the customerPwd
	 */
	public String getCustomerPwd() {
		return CustomerPwd;
	}

	/**
	 * 电子面单密码
	 * @param customerPwd the customerPwd to set
	 */
	public void setCustomerPwd(String customerPwd) {
		CustomerPwd = customerPwd;
	}

	/**
	 * 收件网点标识
	 * @return the sendSite
	 */
	public String getSendSite() {
		return SendSite;
	}

	/**
	 * 收件网点标识
	 * @param sendSite the sendSite to set
	 */
	public void setSendSite(String sendSite) {
		SendSite = sendSite;
	}

	/**
	 * 必填！快递公司编码
	 * @return the shipperCode
	 */
	public String getShipperCode() {
		return ShipperCode;
	}

	/**
	 * 必填！快递公司编码
	 * @param shipperCode the shipperCode to set
	 */
	public void setShipperCode(String shipperCode) {
		ShipperCode = shipperCode;
	}

	/**
	 * 快递单号,一般不填
	 * @return the logisticCode
	 */
	public String getLogisticCode() {
		return LogisticCode;
	}

	/**
	 * 快递单号,一般不填
	 * @param logisticCode the logisticCode to set
	 */
	public void setLogisticCode(String logisticCode) {
		LogisticCode = logisticCode;
	}

	/**
	 * 第三方订单号
	 * @return the thrOrderCode
	 */
	public String getThrOrderCode() {
		return ThrOrderCode;
	}

	/**
	 * 第三方订单号
	 * @param thrOrderCode the thrOrderCode to set
	 */
	public void setThrOrderCode(String thrOrderCode) {
		ThrOrderCode = thrOrderCode;
	}

	/**
	 * 必填！订单编号
	 * @return the orderCode
	 */
	public String getOrderCode() {
		return OrderCode;
	}

	/**
	 * 必填！订单编号
	 * @param orderCode the orderCode to set
	 */
	public void setOrderCode(String orderCode) {
		OrderCode = orderCode;
	}

	/**
	 * 月结编码
	 * @return the monthCode
	 */
	public String getMonthCode() {
		return MonthCode;
	}

	/**
	 * 月结编码
	 * @param monthCode the monthCode to set
	 */
	public void setMonthCode(String monthCode) {
		MonthCode = monthCode;
	}

	/**
	 * 必填！邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
	 * @return the payType
	 */
	public int getPayType() {
		return PayType;
	}

	/**
	 * 必填！邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
	 * @param payType the payType to set
	 */
	public void setPayType(int payType) {
		PayType = payType;
	}

	/**
	 * 必填！快递类型：1-标准快件
	 * @return the expType
	 */
	public int getExpType() {
		return ExpType;
	}

	/**
	 * 必填！快递类型：1-标准快件
	 * @param expType the expType to set
	 */
	public void setExpType(int expType) {
		ExpType = expType;
	}

	/**
	 * 是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 * @return the isNotice
	 */
	public int getIsNotice() {
		return IsNotice;
	}

	/**
	 * 是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 * @param isNotice the isNotice to set
	 */
	public void setIsNotice(int isNotice) {
		IsNotice = isNotice;
	}

	/**
	 * 寄件费（运费）
	 * @return the cost
	 */
	public double getCost() {
		return Cost;
	}

	/**
	 * 寄件费（运费）
	 * @param cost the cost to set
	 */
	public void setCost(double cost) {
		Cost = cost;
	}

	/**
	 * 其他费用
	 * @return the otherCost
	 */
	public double getOtherCost() {
		return OtherCost;
	}

	/**
	 * 其他费用
	 * @param otherCost the otherCost to set
	 */
	public void setOtherCost(double otherCost) {
		OtherCost = otherCost;
	}

	/**
	 * 收件人信息
	 * @return the receiver
	 */
	public Receiver getReceiver() {
		return receiver;
	}

	/**
	 * 收件人信息
	 * @param receiver the receiver to set
	 */
	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}

	/**
	 * 发件人信息
	 * @return the sender
	 */
	public Sender getSender() {
		return sender;
	}

	/**
	 * 发件人信息
	 * @param sender the sender to set
	 */
	public void setSender(Sender sender) {
		this.sender = sender;
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @return the startDate
	 */
	public String getStartDate() {
		return StartDate;
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @return the endDate
	 */
	public String getEndDate() {
		return EndDate;
	}

	/**
	 * 上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	/**
	 * 物品总重量kg
	 * @return the weight
	 */
	public double getWeight() {
		return Weight;
	}

	/**
	 * 物品总重量kg
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		Weight = weight;
	}

	/**
	 * 件数/包裹数
	 * @return the quantity
	 */
	public int getQuantity() {
		return Quantity;
	}

	/**
	 * 件数/包裹数
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	/**
	 * 物品总体积m3
	 * @return the volume
	 */
	public double getVolume() {
		return Volume;
	}

	/**
	 * 物品总体积m3
	 * @param volume the volume to set
	 */
	public void setVolume(double volume) {
		Volume = volume;
	}

	/**
	 * 备注
	 * @return the remark
	 */
	public String getRemark() {
		return Remark;
	}

	/**
	 * 备注
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		Remark = remark;
	}

	/**
	 * 增值服务
	 * @return the addService
	 */
	public AddService getAddService() {
		return addService;
	}

	/**
	 * 增值服务
	 * @param addService the addService to set
	 */
	public void setAddService(AddService addService) {
		this.addService = addService;
	}

	/**
	 * 货物信息
	 * @return the commodity
	 */
	public List<Commodity> getCommodity() {
		return commodityList;
	}

	/**
	 * 货物信息
	 * @param commodity the commodity to set
	 */
	public void setCommodity(List<Commodity> commodityList) {
		this.commodityList = commodityList;
	}

	/**
	 * 返回电子面单模板：0-不需要；1-需要
	 * @return the isReturnPrintTemplate
	 */
	public String getIsReturnPrintTemplate() {
		return IsReturnPrintTemplate;
	}

	/**
	 * 返回电子面单模板：0-不需要；1-需要
	 * @param isReturnPrintTemplate the isReturnPrintTemplate to set
	 */
	public void setIsReturnPrintTemplate(String isReturnPrintTemplate) {
		IsReturnPrintTemplate = isReturnPrintTemplate;
	}
	
	
	
	
}
