package cn.gov.xnc.system.core.util;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 计算节假日
 * Creator:ljz
 */
public class HolidayUtils {

    private static final String SOLAR[] = {"0101", "0404", "0501", "1001", "1002", "1003","1004","1005","1006","1007"};
    private static final String LUNAR[] = {"0101", "0102", "0103","0104","0105","0106", "0505", "0815"};


    private final static NumberFormat NumFmt = NumberFormat.getInstance();

    static {
        NumFmt.setMaximumFractionDigits(0);
        NumFmt.setMinimumIntegerDigits(2);
    }
    
    /**
     * @param cal 时间
     * @param SOLAR  阳历
     * @param LUNAR  阴历 
     * @return
     */
    public static Boolean isHoliday(Calendar cal) {
        long[] ds = LunarCalendar.get(cal);
        String nongli = NumFmt.format(ds[1]) + NumFmt.format(ds[2]);
        String yangli = NumFmt.format(cal.get(Calendar.MONTH) + 1) +
                NumFmt.format(cal.get(Calendar.DATE));
        Boolean holiday = false;
 
     
        //阳历
        if (isInThoseDays(SOLAR, yangli)) {
            holiday = true;
        }


        //阴历
        if (isInThoseDays(LUNAR, nongli)) {
            holiday = true;
        }


        //双休
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            holiday = true;
        }

        return holiday;
    }

    private static boolean isInThoseDays(String[] holidays, String theDay) {
        boolean flag = false;
        for (String holiday : holidays) {
            if (holiday.equals(theDay)) {
                flag = true;
                return flag;
            }
        }
        return flag;
    }

    public static int numberOfWorkdays(Calendar startDate, Calendar endDate) {
        int numberOfWorkdays = 0;
        for (; startDate.before(endDate); startDate.add(Calendar.DAY_OF_YEAR, 1)){
          if (isHoliday(startDate))
              numberOfWorkdays++;
        }
        return numberOfWorkdays;
    }

    public static void main(String[] args) throws IOException {

        Calendar startDate =  Calendar.getInstance();
        startDate.setTime(new Date());
        startDate.add(Calendar.DAY_OF_YEAR,-15);
        System.out.println(startDate.toString());
        System.out.println(isHoliday(startDate));      
    }

}

