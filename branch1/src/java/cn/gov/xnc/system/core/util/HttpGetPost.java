package cn.gov.xnc.system.core.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class HttpGetPost {
	 /**
     * 向指定URL发送GET方法的请求
     * 
     * @param url
     *            发送请求的URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String uriAPI) {
    	 String message ="";
	    try {    	
		      URL urlGet = new URL(uriAPI);
	        HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();
	        http.setRequestMethod("GET"); // 必须是get方式请求
	        http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        http.setDoOutput(true);
	        http.setDoInput(true);
	        System.setProperty("sun.net.client.defaultConnectTimeout", "30000");// 连接超时30秒 单位：毫秒
	        System.setProperty("sun.net.client.defaultReadTimeout", "30000"); // 读取超时30秒      单位：毫秒
	        http.connect();
	        InputStream is = http.getInputStream();
	        int size = is.available();
	
	        byte[] jsonBytes = new byte[size];
	
	        is.read(jsonBytes);
	
	       message = new String(jsonBytes, "UTF-8");
	    }catch (Exception e) {
	    	message=e.toString();
		}
        return message;
    }

    /**
     * 向指定 URL 发送POST方法的请求
     * 
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String httpUrl, String parameJson) {
    	//发送的请求参数，发送的格式也是Json的
    	StringBuffer buffer = new StringBuffer();
    	   URL url = null;

    	  HttpURLConnection conn = null;
    	   try {
    	    url = new URL(httpUrl);
    	    conn = (HttpURLConnection) url.openConnection();
    	    conn.setDoOutput(true);
    	    conn.setDoInput(true);
    	    conn.setRequestMethod("POST");// post

    	   conn.setUseCaches(false);
    	    conn.connect();
    	    // DataOutputStream out = new
    	    // DataOutputStream(conn.getOutputStream());
    	    PrintWriter outprint = new PrintWriter(new OutputStreamWriter( conn.getOutputStream(), "UTF-8"));
    	    outprint.write(parameJson);
    	    outprint.flush();
    	    outprint.close();
    	    BufferedReader reader = new BufferedReader(new InputStreamReader( conn.getInputStream(), "UTF-8"));
    	    String line = "";
    	    while ((line = reader.readLine()) != null) {
    	     buffer.append(line);
    	    }
    	    reader.close();
    	   } catch (Exception e) {
    	    e.printStackTrace();
    	  } 
    	      
    	   if (conn != null) {
    	    conn.disconnect();
    	   }
  
   return buffer.toString();

    }    
}
