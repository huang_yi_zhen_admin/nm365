package cn.gov.xnc.system.core.util;

import java.io.File;

import org.apache.log4j.Logger;

public class RootUtil {
	private static String rootDir = null;
	private static Logger log = Logger.getLogger(RootUtil.class);

	
	/*
	 * 获取跟目录路径
	 */
	public static String getRootDir() throws Exception {
		if (rootDir == null) {
			
			String classPath = RootUtil.class.getProtectionDomain().getCodeSource()
		     .getLocation().getPath();
						
			String rootPath = "";
			
			rootPath = classPath.substring(1,
					classPath.indexOf("/WEB-INF/classes"));
			
			// windows下
			if ("\\".equals(File.separator)) {				
				rootPath = rootPath.replace("/", "\\");
			}
			// linux下
			if ("/".equals(File.separator)) {				
				rootPath = rootPath.replace("\\", "/");
				rootPath = File.separator+rootPath;
			}
			rootDir = rootPath;

		}
		
		return rootDir;
	}
}
