package cn.gov.xnc.system.core.util;


import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


public class MessageKit {
	private static Logger log = Logger.getLogger(MessageKit.class);

	public static void alertMessage(HttpServletResponse response, String msg, boolean isBack) {
		try {
			PrintWriter out = response.getWriter();
			out.println("<script>");
			out.println("alert(\"" + msg + "\");");
			if (isBack) {
				out.println("history.back();");
			}
			out.println("</script>");
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

	public static void alertMessage(HttpServletResponse response, String msg, String redirectUrl) {
		try {
			PrintWriter out = response.getWriter();
			out.println("<script >");
			if(msg!=null)out.println("alert(\"" + msg + "\");");
			if (!StringUtil.isEmpty(redirectUrl)) {
				out.println("location.href=\"" + redirectUrl + "\";");
			}
			out.println("</script>");
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

	public static void displayMessage(HttpServletResponse response, String msg) {
		try {
			PrintWriter out = response.getWriter();
			out.println(msg);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

}