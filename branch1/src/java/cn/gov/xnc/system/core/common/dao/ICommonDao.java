package cn.gov.xnc.system.core.common.dao;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;


import cn.gov.xnc.system.core.common.model.common.UploadFile;
import cn.gov.xnc.system.core.common.model.json.ComboTree;
import cn.gov.xnc.system.core.common.model.json.ImportFile;

import cn.gov.xnc.system.core.extend.template.Template;
import cn.gov.xnc.system.tag.vo.easyui.ComboTreeModel;
import cn.gov.xnc.system.tag.vo.easyui.TreeGridModel;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface ICommonDao extends IGenericBaseCommonDao{
	
	
	/**
	 * admin账户密码初始化
	 * @param user
	 */
	public void pwdInit(TSUser user,String newPwd);
//	/**
//	 * 检查用户是否存在
//	 * */
//	public TSUser getUserByUserIdAndUserNameExits(TSUser user );
	public String getUserRole(TSUser user);
	/**
	 * 文件上传
	 * @param request
	 */
	public <T> T  uploadFile(UploadFile uploadFile);
	/**
	 * 文件上传或预览
	 * @param uploadFile
	 * @return
	 */
	public HttpServletResponse viewOrDownloadFile(UploadFile uploadFile);

	public Map<Object,Object> getDataSourceMap(Template template);
	/**
	 * 生成XML文件
	 * @param fileName XML全路径
	 */
	public HttpServletResponse createXml(ImportFile importFile);
	/**
	 * 解析XML文件
	 * @param fileName XML全路径
	 */
	public void parserXml(String fileName);
	/**
	 * 根据模型生成JSON
	 * @param all 全部对象
	 * @param in  已拥有的对象
	 * @param comboBox 模型
	 * @return
	 */
	public  List<ComboTree> ComboTree(List all,ComboTreeModel comboTreeModel,List in , boolean recursive);
	
//	public  List<TreeGrid> treegrid(List all,TreeGridModel treeGridModel);
	
	public <T> List<T> findListByJdbc(String sql, Class<T> type,
			Object... param) ;

	public <T> List<T> queryListByJdbc(String sql, Class type);
	

	
}

