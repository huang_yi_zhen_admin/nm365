package cn.gov.xnc.system.core.util;

/**
 * 
 * @author  zero
 *
 */
public enum BrowserType {
	IE11,IE10,IE9,IE8,IE7,IE6,Firefox,Safari,Chrome,Opera,Camino,Gecko
}
