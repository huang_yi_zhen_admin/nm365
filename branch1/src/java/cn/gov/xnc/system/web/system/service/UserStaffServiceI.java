package cn.gov.xnc.system.web.system.service;


import java.util.List;

import cn.gov.xnc.system.core.common.service.CommonService;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
/**
 * 
 * @author  zero
 *
 */
public interface UserStaffServiceI extends CommonService{

	/**
	 * 根据业务员获取采购商用户
	 * @param salesMan
	 * @return
	 */
	public List<TSUser> getBuyerUserBySalesMan(TSUser salesMan);
}
