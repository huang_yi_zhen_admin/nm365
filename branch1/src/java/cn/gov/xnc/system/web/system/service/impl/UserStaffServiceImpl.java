package cn.gov.xnc.system.web.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserClientsEntity;
import cn.gov.xnc.system.web.system.service.UserStaffServiceI;

/**
 * 
 * @author ljz
 *
 */
@Service("userStaffService")

public class UserStaffServiceImpl extends CommonServiceImpl implements UserStaffServiceI {

	/**
	 * 根据业务员获取采购商用户
	 * @param salesMan
	 * @return
	 */
	public List<TSUser> getBuyerUserBySalesMan(TSUser salesMan) {
		// 如果是业务员
		if ("4".equals(salesMan.getType())) {
			// 根据业务员获取相应的客户信息
			CriteriaQuery cq = new CriteriaQuery(UserClientsEntity.class);
			cq.eq("yewuid", salesMan);
			cq.add();
			List<UserClientsEntity> userClientlist = this.getListByCriteriaQuery(cq, false);

			List<TSUser> caiGouUserList = new ArrayList<TSUser>();

			for (UserClientsEntity userClientsEntity : userClientlist) {
				CriteriaQuery cqCaigou = new CriteriaQuery(TSUser.class);
				cqCaigou.eq("tsuserclients", userClientsEntity);
				cqCaigou.add();
				List<TSUser> caiGouUser = this.getListByCriteriaQuery(cqCaigou, false);

				// 添加采购商信息
				if (null != caiGouUser && caiGouUser.size() > 0) {
					caiGouUserList.add(caiGouUser.get(0));
				}

			}

			if (caiGouUserList.size() > 0) {
				return caiGouUserList;
			}
		}
		return null;
	}

}
