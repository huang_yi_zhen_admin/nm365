package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 公司赋值角色
 * @author zero
 * @date 2016-10-09 15:41:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_role_company", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class RoleCompanyEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	
	private TSRole roleid;
	/**公司信息id*/
	private TSCompany companyid;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  roleid
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROLEID")
	public TSRole getRoleid(){
		return this.roleid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  roleid
	 */
	public void setRoleid(TSRole roleid){
		this.roleid = roleid;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司信息id
	 */
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司信息id
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
}
