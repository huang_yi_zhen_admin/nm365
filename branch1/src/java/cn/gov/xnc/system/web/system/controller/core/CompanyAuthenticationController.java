package cn.gov.xnc.system.web.system.controller.core;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyAuthenticationEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;


/**   
 * @Title: Controller
 * @Description: 公司认证信息
 * @author zero
 * @date 2016-09-26 02:51:35
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/companyAuthenticationController")
public class CompanyAuthenticationController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CompanyAuthenticationController.class);


	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 公司认证信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView companyAuthentication(HttpServletRequest request) {
		return new ModelAndView("system/user/companyAuthenticationList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(CompanyAuthenticationEntity companyAuthentication,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		if(  StringUtil.isNotEmpty(user.getCompany().getId()) ){//如果不公司信息 这返回空
			TSCompany tScompany = systemService.getEntity(TSCompany.class, user.getCompany().getId());
			companyAuthentication.setId(tScompany.getCompanyauthentication().getId());
			
			CriteriaQuery cq = new CriteriaQuery(CompanyAuthenticationEntity.class, dataGrid);
			//查询条件组装器
			cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyAuthentication, request.getParameterMap());
			this.systemService.getDataGridReturn(cq, true);
			TagUtil.datagrid(response, dataGrid);
		}
	}

	/**
	 * 删除公司认证信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(CompanyAuthenticationEntity companyAuthentication, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyAuthentication = systemService.getEntity(CompanyAuthenticationEntity.class, companyAuthentication.getId());
		message = "公司认证信息删除成功";
		systemService.delete(companyAuthentication);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加公司认证信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(CompanyAuthenticationEntity companyAuthentication, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(companyAuthentication.getId())) {
			message = "公司认证信息更新成功";
			CompanyAuthenticationEntity t = systemService.get(CompanyAuthenticationEntity.class, companyAuthentication.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(companyAuthentication, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "公司认证信息更新失败";
			}
		} 
		j.setMsg(message);
		return j;
	}

	/**
	 * 公司认证信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(HttpServletRequest req) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		if (StringUtil.isNotEmpty(user.getCompany().getCompanyauthentication().getId())) {
			CompanyAuthenticationEntity companyAuthentication = systemService.getEntity(CompanyAuthenticationEntity.class, user.getCompany().getCompanyauthentication().getId());
			req.setAttribute("companyAuthenticationPage", companyAuthentication);
		}
		return new ModelAndView("system/user/companyAuthentication");
	}
	
	
	
	
	
}
