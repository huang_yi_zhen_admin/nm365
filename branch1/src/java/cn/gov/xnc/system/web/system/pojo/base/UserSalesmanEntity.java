package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 个人账户管理
 * @author zero
 * @date 2016-10-06 15:20:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_user_salesman", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class UserSalesmanEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**可使用金额*/
	private BigDecimal usemoney;
	/**可提现金额*/
	private BigDecimal withdrawalsmoney;
	/**锁定金额*/
	private BigDecimal frozenmoney;
	/**积分*/
	private java.lang.Integer integral;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  可使用金额
	 */
	@Column(name ="USEMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getUsemoney(){
		return this.usemoney;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  可使用金额
	 */
	public void setUsemoney(BigDecimal usemoney){
		this.usemoney = usemoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  可提现金额
	 */
	@Column(name ="WITHDRAWALSMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getWithdrawalsmoney(){
		return this.withdrawalsmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  可提现金额
	 */
	public void setWithdrawalsmoney(BigDecimal withdrawalsmoney){
		this.withdrawalsmoney = withdrawalsmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  锁定金额
	 */
	@Column(name ="FROZENMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getFrozenmoney(){
		return this.frozenmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  锁定金额
	 */
	public void setFrozenmoney(BigDecimal frozenmoney){
		this.frozenmoney = frozenmoney;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  积分
	 */
	@Column(name ="INTEGRAL",nullable=true,precision=10,scale=0)
	public java.lang.Integer getIntegral(){
		return this.integral;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  积分
	 */
	public void setIntegral(java.lang.Integer integral){
		this.integral = integral;
	}
}
