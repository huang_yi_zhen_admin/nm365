package cn.gov.xnc.system.web.system.pojo.base;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;


/**   
 * @Title: Entity
 * @Description: 系统公司信息表
 * @author huangyz
 * @date 2016-03-03 15:34:17
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_company", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class TSCompany implements java.io.Serializable {
	/**ID*/
	private java.lang.String id;
	/**公司名称*/
	private java.lang.String companyName;
	
	/**联系电话*/
	private java.lang.String mobilephone;
	/**联系人*/
	private java.lang.String contactname;
	/**公司介绍*/
	private java.lang.String remarks;
	/**地址信息*/
	private TSTerritory TSTerritory ;// 地址
	/**详细地址*/
	private java.lang.String address;
	/**行业*/
	private CompanyIndustryEntity  industry;
	/**更新时间*/
	private java.util.Date updatedate;
	/**创建时间*/
	private java.util.Date createdate;
	/**公司第三方接口信息*/
	private CompanyThirdpartyEntity companythirdparty;
	/**公司管理付费信息*/
	private CompanyReviewedEntity companyreviewed;
	/**平台信息*/
	private CompanyPlatformEntity companyplatform;
	/**公司认证信息*/
	private CompanyAuthenticationEntity companyauthentication;
	/**客服电话*/
	private java.lang.String servicephone;
	
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地址信息
	 */
	@JsonIgnore    //getList查询转换为列表时处理json转换异常
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TERRITORYID")
	public TSTerritory getTSTerritory() {
		return TSTerritory;
	}

	public void setTSTerritory(TSTerritory tSTerritory) {
		TSTerritory = tSTerritory;
	}
	
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  ID
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  ID
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *
	 *@return: java.lang.String  公司名称
	 */
	@Column(name ="companyName",nullable=false,length=500)
	public java.lang.String getCompanyName() {
		return companyName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司名称
	 */

	public void setCompanyName(java.lang.String companyName) {
		this.companyName = companyName;
	}
	
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  联系电话
	 */
	@Column(name ="MOBILEPHONE",nullable=true,length=50)
	public java.lang.String getMobilephone(){
		return this.mobilephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  联系电话
	 */
	public void setMobilephone(java.lang.String mobilephone){
		this.mobilephone = mobilephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  联系人
	 */
	@Column(name ="CONTACTNAME",nullable=true,length=50)
	public java.lang.String getContactname(){
		return this.contactname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  联系人
	 */
	public void setContactname(java.lang.String contactname){
		this.contactname = contactname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司介绍
	 */
	@Column(name ="REMARKS",nullable=true,length=3000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司介绍
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  详细地址
	 */
	@Column(name ="ADDRESS",nullable=true,length=200)
	public java.lang.String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  详细地址
	 */
	public void setAddress(java.lang.String address){
		this.address = address;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  行业
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "INDUSTRY")
	public CompanyIndustryEntity getIndustry(){
		return this.industry;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  行业
	 */
	public void setIndustry(CompanyIndustryEntity industry){
		this.industry = industry;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司第三方接口信息
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYTHIRDPARTY")
	public CompanyThirdpartyEntity getCompanythirdparty(){
		return this.companythirdparty;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司第三方接口信息
	 */
	public void setCompanythirdparty(CompanyThirdpartyEntity companythirdparty){
		this.companythirdparty = companythirdparty;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司管理付费信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYREVIEWED")
	public CompanyReviewedEntity getCompanyreviewed(){
		return this.companyreviewed;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司管理付费信息
	 */
	public void setCompanyreviewed(CompanyReviewedEntity companyreviewed){
		this.companyreviewed = companyreviewed;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  平台信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYPLATFORM")
	public CompanyPlatformEntity getCompanyplatform(){
		return this.companyplatform;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  平台信息
	 */
	public void setCompanyplatform(CompanyPlatformEntity companyplatform){
		this.companyplatform = companyplatform;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司认证信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYAUTHENTICATION")
	public CompanyAuthenticationEntity getCompanyauthentication(){
		return this.companyauthentication;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司认证信息
	 */
	public void setCompanyauthentication(CompanyAuthenticationEntity companyauthentication){
		this.companyauthentication = companyauthentication;
	}

	/**
	 * 客服电话
	 * @return
	 */
	public java.lang.String getServicephone() {
		return servicephone;
	}

	public void setServicephone(java.lang.String servicephone) {
		this.servicephone = servicephone;
	}
	
	
	
}
