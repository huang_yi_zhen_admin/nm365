package cn.gov.xnc.system.web.system.controller.core;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserTypeEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;
import cn.gov.xnc.system.core.util.MyBeanUtils;



/**   
 * @Title: Controller
 * @Description: 客户类型折扣率
 * @author zero
 * @date 2016-09-26 09:13:13
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/userTypeController")
public class UserTypeController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserTypeController.class);


	@Autowired
	private SystemService systemService;
	private String message;
	@Autowired
	private UserGradePriceServiceI userGradePriceService;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 客户类型折扣率列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView userType(HttpServletRequest request) {
		return new ModelAndView("system/user/userTypeList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(UserTypeEntity userType,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(UserTypeEntity.class, dataGrid);
		if(StringUtil.isNotEmpty(userType.getTypename())){
			cq.like("typename", "%" + userType.getTypename() + "%");
		}
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userType, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除客户类型折扣率
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(UserTypeEntity userType, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		userType = systemService.getEntity(UserTypeEntity.class, userType.getId());
		message = "客户类型折扣率删除成功";
		systemService.delete(userType);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		userGradePriceService.delGradePriceByUserType(userType);
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加客户类型折扣率
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(UserTypeEntity userType, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userType.getId())) {
			message = "客户类型折扣率更新成功";
			UserTypeEntity t = systemService.get(UserTypeEntity.class, userType.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userType, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
				
				userGradePriceService.updateGradePriceByUserType(t);
				
			} catch (Exception e) {
				e.printStackTrace();
				message = "客户类型折扣率更新失败";
			}
		} else {
			message = "客户类型折扣率添加成功";
			systemService.save(userType);
			userGradePriceService.initUserGradePrice(userType);
			
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 客户类型折扣率列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(UserTypeEntity userType, HttpServletRequest req) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		
		
		if (StringUtil.isNotEmpty(userType.getId())) {
			userType = systemService.getEntity(UserTypeEntity.class, userType.getId());
			req.setAttribute("userTypePage", userType);
		}
		return new ModelAndView("system/user/userType");
	}
	
	
	
	/**
	 * 选择业务员列表
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "choiceUserType")
	public String choiceUserType(HttpServletRequest request, HttpServletResponse response) {
		return "system/user/choiceUserType";
		
	}
	/**
	 * 业务员列表
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "choiceUserTypeList")
	public void choiceUserTypeList( UserTypeEntity userType , HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		//只显示对应下属的权限，目前如果是企业管理员，不显示系统管理员，如果是其他权限， 不显示企业管理员、系统管理员	
		CriteriaQuery cq = new CriteriaQuery(UserTypeEntity.class, dataGrid);
			cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userType);
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
}
