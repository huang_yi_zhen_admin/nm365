package cn.gov.xnc.admin.dataStatistics.entity;

import java.math.BigDecimal;

public class StatisPageVO {
	
	String Name1;
	BigDecimal value1 = new BigDecimal(0.00);
	
	String name2;
	BigDecimal value2 = new BigDecimal(0.00);
	
	String name3;
	BigDecimal value3 = new BigDecimal(0.00);
	
	String name4;
	BigDecimal value4 = new BigDecimal(0.00);
	
	String datetime;

	public String getName1() {
		return Name1;
	}

	public void setName1(String name1) {
		Name1 = name1;
	}

	public BigDecimal getValue1() {
		return value1;
	}

	public void setValue1(BigDecimal value1) {
		this.value1 = value1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public BigDecimal getValue2() {
		return value2;
	}

	public void setValue2(BigDecimal value2) {
		this.value2 = value2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public BigDecimal getValue3() {
		return value3;
	}

	public void setValue3(BigDecimal value3) {
		this.value3 = value3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public BigDecimal getValue4() {
		return value4;
	}

	public void setValue4(BigDecimal value4) {
		this.value4 = value4;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

}
