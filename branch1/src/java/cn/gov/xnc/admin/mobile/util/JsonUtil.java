package cn.gov.xnc.admin.mobile.util;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser.Feature;
import org.codehaus.jackson.map.ObjectMapper;

public final class JsonUtil
{
  private static JsonFactory jsonFactory = new JsonFactory();

  private static ObjectMapper mapper = null;

  public static ObjectMapper getMapper()
  {
    return mapper;
  }

  public static JsonFactory getJsonFactory()
  {
    return jsonFactory;
  }

  public static <T> T toBean(String json, Class<T> clazz)
  {
    Object rtv = null;
    try {
      rtv = mapper.readValue(json, clazz);
    } catch (Exception ex) {
      throw new IllegalArgumentException("json字符串转成java bean异常", ex);
    }
    return (T) rtv;
  }

  public static String toJson(Object bean)
  {
    String rtv = null;
    try {
      rtv = mapper.writeValueAsString(bean);
    } catch (Exception ex) {
      throw new IllegalArgumentException("java bean转成json字符串异常", ex);
    }
    return rtv;
  }

  static
  {
    jsonFactory.configure(Feature.ALLOW_SINGLE_QUOTES, true);
    jsonFactory.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
    mapper = new ObjectMapper(jsonFactory);
  }
}
