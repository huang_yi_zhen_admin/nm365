package cn.gov.xnc.admin.message.service;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;



public interface MessageTemplateServiceI {

	
	
	
	public void sendSMSTTMM(String account, String pswd, String mobile, String msgtitle, String msg);
	
	
	public boolean mosSendSmsDelivery_1(TSUser user , String passwordRandCode);
	public boolean mosSendSmsDelivery_2(PayBillsOrderEntity payBillsOrder );
	public boolean mosSendSmsDelivery_3(PayBillsOrderEntity payBillsOrder );
	
	public boolean mosSendSmsDelivery_4(PayBillsOrderEntity payBillsOrder );
	public boolean mosSendSmsDelivery_5(OrderEntity orderEntity );
	public boolean mosSendSmsDelivery_6(OrderEntity orderEntity );
	
	public boolean mosSendSmsDelivery_7(OrderEntity order  );
	public boolean mosSendSmsDelivery_8(OrderEntity order  );
	
	public boolean mosSendSmsDelivery_9(PayBillsOrderEntity payBillsOrder  );
	
	public boolean mosSendSmsCopartner_1(TSUser user , String passwordRandCode);
	public boolean mosSendSmsDelivery_13(String recieverId);
	
}
