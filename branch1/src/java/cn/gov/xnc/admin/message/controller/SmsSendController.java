package cn.gov.xnc.admin.message.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.message.entity.SmsSendEntity;
import cn.gov.xnc.admin.message.service.impl.SmsSendServiceImpl;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DisplayMsgUtil;
import cn.gov.xnc.system.core.util.ParamUtil;

@Scope("prototype")
@Controller
@RequestMapping("/smsSendController")
public class SmsSendController {
	@Resource(name="smsSendService")
	private SmsSendServiceImpl smsSendService;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * 系统消息列表
	 * */
	@RequestMapping(value = "smsSendList")
	public ModelAndView smsSendList(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/message/smsSendList");
	}
	/**
	 * 支付记录列表
	 * */
	@RequestMapping(value = "getSmsSendListPage")
	@ResponseBody
	public void getPayListPage(SmsSendEntity smsSend, HttpServletRequest request, HttpServletResponse response){
		DataGrid dataGrid = new DataGrid();
		String[] ids;
		String field = ParamUtil.getParameter(request, "field", "");
		dataGrid.setPageNum(ParamUtil.getIntParameter(request, "pageNum", 1));
		dataGrid.setShowNum(ParamUtil.getIntParameter(request, "showNum", 10));
		dataGrid.setSort("createDate");
		if(field.indexOf(",")>=0){
			ids = field.split(",");
		}else{
			ids = ParamUtil.getArrayParameter(request, "field");
		}
		dataGrid = smsSendService.getSmsSendListPage(dataGrid, smsSend);
		List<Map<String, Object>> mapList = dataGrid.getResults();
		for(Map<String, Object> m : mapList){
			switch(Integer.parseInt(m.get("type").toString().toString())){
				case 1 : m.put("type", "开通客户帐号");
						break;
				case 2 : m.put("type", "付款通知订单员");
						break;
				case 3 : m.put("type", "付款通知业务员");
						break;
				case 4 : m.put("type", "付款通知客户");
						break;
				case 5 : m.put("type", "发货通知收货人");
						break;
				case 6 : m.put("type", "发货通知客户");
						break;
				case 7 : m.put("type", "退款通知财务");
						break;
				case 8 : m.put("type", "退款通知用户");
						break;
				case 13 : m.put("type", "通知发货商接单发货");
						break;		
				default : m.put("type", "");		
			}
			m.put("createDate", sdf.format(m.get("createDate")));
		}
		dataGrid.setResults(mapList);
		DisplayMsgUtil.printMsg(response, DataConstruction(ids, dataGrid));
	}
	
	private static String DataConstruction(String[] field, DataGrid dataGrid){
		StringBuilder sbd = new StringBuilder();
		StringBuilder data = new StringBuilder();
		StringBuilder rows = new StringBuilder();
		List<Map<String, Object>> list;
		String msg = "获取数据成功";
		Integer success = 1;
		int i = 0;
		
		list = dataGrid.getResults();
		
		rows.append("[");
		if(list!=null && list.size()>0){
			for(Map<String, Object> m : list){
				if(i>0){
					rows.append(",");
				}
				rows.append("[");
				int j = 0;
				for(String s : field){
					if(j>0){
						rows.append(",");
					}
					if(m.get(s) == null || m.get(s).toString().equals("")){
						rows.append("\"\"");
					}else{
						rows.append("\"" + m.get(s) + "\"");
					}
					j++;
				}
				rows.append("]");
				i++;
			}
		}
		rows.append("]");
		data.append("\"page\"");
		data.append(":");
		data.append("\"" + dataGrid.getPageNum() +"\"");
		data.append(",");
		data.append("\"total\"");
		data.append(":");
		data.append("\"" + dataGrid.getTotal() +"\"");
		data.append(",");
		data.append("\"rows\"");
		data.append(":");
		data.append(rows);
		
		sbd.append("{");
		sbd.append("\"msg\"");
		sbd.append(":");
		sbd.append("\"" + msg +"\"");
		sbd.append(",");
		sbd.append("\"success\"");
		sbd.append(":");
		sbd.append(success);
		sbd.append(",");
		sbd.append("\"data\"");
		sbd.append(":");
		sbd.append("{" + data +"}");
		sbd.append("}"); 
		return sbd.toString();
	}
}
