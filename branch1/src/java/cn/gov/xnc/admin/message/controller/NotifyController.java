package cn.gov.xnc.admin.message.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.message.entity.NotifyEntity;
import cn.gov.xnc.admin.message.entity.NotifyTextEntity;
import cn.gov.xnc.admin.message.service.impl.NotifyServiceImpl;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DisplayMsgUtil;
import cn.gov.xnc.system.core.util.ParamUtil;

@Scope("prototype")
@Controller
@RequestMapping("/notifyController")
public class NotifyController extends NotifyConfigCongroller {
	@Resource(name="NotifyService")
	private NotifyServiceImpl notifyService;
	@RequestMapping(value = "stationMsgSend")
	public ModelAndView stationMsgSend(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/notify/stationMsgSend");
	}
	
	/**
	 * 系统消息列表
	 * */
	@RequestMapping(value = "noticeList")
	public ModelAndView noticeList(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/notify/noticeList");
	}
	/**
	 * 站内消息列表
	 * */
	@RequestMapping(value = "messageList")
	public ModelAndView messageList(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/notify/msgList");
	}
	/**
	 * 筛选用户
	 * */
	@RequestMapping(value = "choiseUser")
	public ModelAndView choiseUser(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/notify/choiseUser");
	}
	
	/**
	 * 站短详情
	 * */
	@RequestMapping(value = "showMsg")
	public ModelAndView showMsg(NotifyEntity notify, ModelMap modelMap){
		NotifyTextEntity notifyTextEntity = new NotifyTextEntity();
		notify = notifyService.getNotify(notify);
		notifyTextEntity.setId(notify.getNotify_text_id());
		notifyTextEntity = notifyService.getNotifyText(notifyTextEntity);
		modelMap.put("title", notify.getTitle());
		modelMap.put("content", notifyTextEntity.getContent());
		return new ModelAndView("admin/notify/showMsg");
	}
	
	/**
	 * 消息保存
	 * */
	@RequestMapping(value = "notifySave")
	@ResponseBody
	public AjaxJson notifySave(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		NotifyTextEntity notifyTextEntity = new NotifyTextEntity();
		NotifyEntity notifyEntity = null;
		List<NotifyEntity> notifyEntityList = new ArrayList<NotifyEntity>();
		String[] receiverIdArray = null;
		String notifyid = ParamUtil.getParameter(request, "notifyid", "");
		String receiver_id = ParamUtil.getParameter(request, "receiverId", "");
		String content = ParamUtil.getParameter(request, "content", "");
		String title = ParamUtil.getParameter(request, "title", "");
		if(notifyid.equals("")){
			if(title.length()>64){
				j.setSuccess(false);
				j.setMsg("标题字数过长");
				return j;
			}
		}
		if(content.length()>515){
			j.setSuccess(false);
			j.setMsg("内容字数过长,已超出500字符");
			return j;
		}
		
		if(receiver_id.indexOf(",") > 0){
			receiverIdArray = receiver_id.split(",");
			for(String r : receiverIdArray){
				notifyEntity = new NotifyEntity();
				notifyEntity.setTitle(title);
				notifyEntity.setReceiver_id(r);
				notifyEntity.setNotifytype((byte) NOTIFYMESSAGE);
				notifyEntityList.add(notifyEntity);
			}
		}else{
			notifyEntity = new NotifyEntity();
			notifyEntity.setTitle(title);
			notifyEntity.setReceiver_id(receiver_id);
			notifyEntity.setNotifytype((byte) NOTIFYMESSAGE);
			notifyEntityList.add(notifyEntity);
		}
		
		notifyTextEntity.setContent(content);
		notifyService.notifySave(notifyTextEntity, notifyEntityList);
		j.setMsg("消息发送成功");
		return j;
	}
	
	/**
	 * 消息删除
	 * */
	@RequestMapping(value = "notifyDel")
	@ResponseBody
	public AjaxJson notifyDel(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		String ids = ParamUtil.getParameter(request, "ids", "");
		String[] idsArray = null;
		String cstr = "";
		if(ids.indexOf(",") > 0){
			idsArray = ids.split(",");
			for(String s : idsArray){
				if(!cstr.equals("")){
					cstr = "," + cstr;
				}
				cstr = "'" + s + "'" + cstr;
			}
		}else{
			cstr = "'" + ids +"'";
		}
		notifyService.notifyDelete(cstr);
		j.setMsg("删除成功");
		j.setSuccess(true);
		return j;
	}
	
	/**
	 * 获取消息数据
	 * */
	@RequestMapping(value = "getDataListByType")
	public void getDataListByType(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> where = new HashMap<String, Object>();
		Integer type = ParamUtil.getIntParameter(request, "type", 0);
		String status = ParamUtil.getParameter(request, "status", null);
		String key = ParamUtil.getParameter(request, "key", null);
		Integer pageNum = ParamUtil.getIntParameter(request, "pageNum", 1);
		Integer showNum = ParamUtil.getIntParameter(request, "showNum", 10);
		DataGrid dataGrid = new DataGrid();
		dataGrid.setPageNum(pageNum);
		dataGrid.setShowNum(showNum);
		if(type == NOTIFYMESSAGE){
			where.put("notifytype", NOTIFYMESSAGE);
		} else if(type == NOTIFYNOTICE){
			where.put("notifytype", NOTIFYNOTICE);
		}
		if(status != null){
			where.put("status", Integer.parseInt(status));
		}
		where.put("key", key);
		DisplayMsgUtil.printMsg(response, notifyService.getDataList(dataGrid, where));
	}
	/**
	 * 列出公司内的用户数据
	 * */
	@RequestMapping(value = "getUserDataListByCompany")
	public void getUserDataListByCompany(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> where = new HashMap<String, Object>();
		DataGrid dataGrid = new DataGrid();
		String username = ParamUtil.getParameter(request, "userName", "");
		String realname = ParamUtil.getParameter(request, "realname", "");
		Integer type = ParamUtil.getIntParameter(request, "searchType", 0);
		if(!username.equals("")){
			where.put("username", username);
		}
		if(!realname.equals("")){
			where.put("realname", realname);
		}
		if(type > 0){
			where.put("type", type);
		}
		if(type == 24){
			where.put("type", null);
			where.put("typein", "2,4");
		}

		DisplayMsgUtil.printMsg(response, notifyService.getUserDataListByCompany(dataGrid, where));
	}
	/**
	 * 获取未读消息数
	 * */
	@RequestMapping(value = "getCountUnreadNotify")
	@ResponseBody
	public AjaxJson getCountUnreadNotify(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		Map<String,Object> messageContent = new HashMap<String,Object>();
		Map<String,Object> noticeContent = new HashMap<String,Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("notifytype", NOTIFYMESSAGE);
		messageContent = notifyService.getData(where);
		where.clear();
		where.put("notifytype", NOTIFYNOTICE);
		noticeContent = notifyService.getData(where);
		map.put("messageContent", messageContent != null ? messageContent.get("content") : "");
		map.put("noticeContent", noticeContent != null ? noticeContent.get("content") : "");
		map.put("messageCount", notifyService.getCountUnreadNotify(NOTIFYMESSAGE));
		map.put("noticeCount", notifyService.getCountUnreadNotify(NOTIFYNOTICE));
		j.setMsg("查询数据成功");
		j.setObj(map);
		return j;
	}
	/**
	 * 更新消息状态
	 * */
	@RequestMapping(value = "updateStatusByIds")
	@ResponseBody
	public AjaxJson updateStatusByIds(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		String ids = ParamUtil.getParameter(request, "ids", "");
		String[] idsArray = null;
		String cstr = "";
		if(ids.indexOf(",") > 0){
			idsArray = ids.split(",");
			for(String s : idsArray){
				if(!cstr.equals("")){
					cstr = "," + cstr;
				}
				cstr = "'" + s + "'" + cstr;
			}
		}else{
			cstr = "'" + ids +"'";
		}
		notifyService.updateStatusByIds(cstr);
		j.setMsg("更新状态成功");
		return j;
	}
	/**
	 * 消息回复
	 * */
	@RequestMapping(value = "notifyReply")
	public ModelAndView notifyReply(HttpServletRequest request,  ModelMap modelMap){
		String receiverId = ParamUtil.getParameter(request, "receid", "");
		String notifyid = ParamUtil.getParameter(request, "notifyid", "");
		String title = ParamUtil.getParameter(request, "title", "");
		modelMap.put("receiverId", receiverId);
		modelMap.put("notifyid", notifyid);
		modelMap.put("title", title);
		return new ModelAndView("admin/notify/reply");
	}
	/**
	 * 发送消息给管理员
	 * */
	@RequestMapping(value = "notifyToA", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson notifyBuyersToAdminSave(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		String payBillsOrderId = ParamUtil.getParameter(request, "payBillsOrderId", "");
		String paytype = ParamUtil.getParameter(request, "paytype", "0");
		String other = ParamUtil.getParameter(request, "other", "");
		notifyService.notifyToAdminSave(payBillsOrderId, paytype, other);
		j.setMsg("发送成功");
		return j;
	}
	/**
	 * 发送消息给全作伙伴
	 * */
	@RequestMapping(value = "notifyToP", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson notifyToPartnerrSave(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		StringBuilder sbd = new StringBuilder();
		String[] ids;
		String orderId = ParamUtil.getParameter(request, "orderId", "");
		String cases = ParamUtil.getParameter(request, "cases", "0");
		String array = ParamUtil.getParameter(request, "array", "");
		if(orderId.indexOf(",") >= 0){
			ids = orderId.split(",");
			for(String s : ids){
				if(!sbd.toString().equals("")){
					sbd.append(",");
				}
				sbd.append("'" + s + "'");
			}
			orderId = sbd.toString();
		}else{
			orderId = "'" + orderId + "'";
		}
		notifyService.notifyToPartnerrSave(orderId, cases, array);
		j.setMsg("发送成功");
		return j;
	}
}
