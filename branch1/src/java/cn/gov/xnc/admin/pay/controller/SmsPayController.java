package cn.gov.xnc.admin.pay.controller;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;





import cn.gov.xnc.admin.mobile.util.JsonUtil;
import cn.gov.xnc.admin.mobile.util.ParamUtil;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;
import cn.gov.xnc.admin.pay.config.CommonConfig;
import cn.gov.xnc.admin.pay.entity.PaySmsRecordEntity;
import cn.gov.xnc.admin.pay.service.impl.PaySmsRecordServiceImpl;
import cn.gov.xnc.admin.pay.service.impl.SmsRelateServiceImpl;
import cn.gov.xnc.admin.pay.util.DataHandle;
import cn.gov.xnc.admin.zhifu.alipay.config.AlipayConfig;
import cn.gov.xnc.admin.zhifu.alipay.util.AlipayNotify;
import cn.gov.xnc.admin.zhifu.alipay.util.AlipaySubmit;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DisplayMsgUtil;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyThirdpartyEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Controller
@RequestMapping("/smsPayController")
public class SmsPayController {
	@Autowired
	private SystemService systemService;
	@Autowired
	private PayBillsServiceI payBillsService;
	@Resource(name="smsRelateService")
	private SmsRelateServiceImpl smsRelateService;
	@Resource(name="paySmsRecordService")
	private PaySmsRecordServiceImpl paySmsRecordService;
	private Logger logger = Logger.getLogger(SmsPayController.class);
	private static Calendar c = Calendar.getInstance();
	@RequestMapping(value = "index")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response,  ModelMap modelMap){
		modelMap.put("msgnumberTotal", smsRelateService.getCompanySmsNum());
		return new ModelAndView("admin/pay/smsPayIndex");
	}
	
	@RequestMapping(value = "smsPayRecordList")
	public ModelAndView paymentRecordList(HttpServletRequest request, HttpServletResponse response,  ModelMap modelMap){
		return new ModelAndView("admin/pay/smsPayRecordList");
	}
	
	/**
	 * 支付记录列表
	 * */
	@RequestMapping(value = "getPayListPage")
	@ResponseBody
	public void getPayListPage(PaySmsRecordEntity paySmsRecord,TSCompany tSCompany, HttpServletRequest request, HttpServletResponse response){
		DataGrid dataGrid = new DataGrid();
		String[] ids;
		String field = ParamUtil.getParameter(request, "field", "");
		String tradeStatus = "交易进行中";
		dataGrid.setPageNum(ParamUtil.getIntParameter(request, "pageNum", 1));
		dataGrid.setShowNum(ParamUtil.getIntParameter(request, "showNum", 10));
		dataGrid.setSort("updateDate");
		if(field.indexOf(",")>=0){
			ids = field.split(",");
		}else{
			ids = ParamUtil.getArrayParameter(request, "field");
		}
		dataGrid = paySmsRecordService.getPaymentRecordListPage(dataGrid, paySmsRecord, tSCompany);
		List<Map<String, Object>> mapList = dataGrid.getResults();
		for(Map<String, Object> m : mapList){
			if(m.get("returnParams")!=null && !m.get("returnParams").toString().equals("")){
				if(m.get("returnParams").toString().indexOf("TRADE_SUCCESS")>0 || m.get("returnParams").toString().indexOf("TRADE_FINISHED")>0){
					tradeStatus = "交易成功";
				}else{
					tradeStatus = "交易异常";
				}
			}
			m.put("tradeStatus", tradeStatus);
			/*tradeStatus = m.get("trade_status")+"";
			if(m.get("pay_app").toString().equals(CommonConfig.PayApp.SMS.toString())){
				m.put("pay_app", "短信购买");
			}
			if(tradeStatus.equals("TRADE_FINISHED") || tradeStatus.equals("TRADE_SUCCESS")){
				m.put("trade_status", "交易成功");
			}else{
				m.put("trade_status", "<span style=\\\"color:#F60\\\">交易返回异常</span>");
			}*/
			m.put("des", m.get("trade_amount")+"元-" + m.get("number") + "条短信套餐");
		}
		dataGrid.setResults(mapList);
		DisplayMsgUtil.printMsg(response, DataHandle.DataConstruction(ids, dataGrid));
	}
	/**
	 * 支付给固定商家
	 * */
	@RequestMapping(value = "payFixedBusiness", method = RequestMethod.POST)
	public ModelAndView payDo(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
		AlipayConfig alipayConfig = new AlipayConfig();
		
		TSUser user  = ResourceUtil.getSessionUserName();
		if( user != null && StringUtil.isNotEmpty(user.getId()) ){
			user = systemService.get(TSUser.class, user.getId());
		}
		//CompanyThirdpartyEntity companyThirdparty = this.getCompanyThirdparty();
		//String alipayid = companyThirdparty.getAlipayid();//星农场商户号
		//String alipaykey = companyThirdparty.getAlipaykey();//MD5密钥
		String alipayid = CommonConfig.getAliPidKey().get("xncPid").toString();//星农场商户号
		String alipaykey = CommonConfig.getAliPidKey().get("xncKey").toString();//MD5密钥
		
		String url =  request.getScheme() +"://" + request.getServerName();
	 	String notify_url = url + "/alipayaController/notifyAlipayaSpec"; 
	 	String return_url = url + "/smsPayController/returnUrl";
	 	String result = "";
	 	//logger.error("#########alipayid:"+alipayid);
		//logger.error("#########alipaykey:"+alipaykey);
		//logger.error("#########notify_url:"+notify_url);
		//logger.error("#########return_url:"+return_url);
		//商户订单号，商户网站订单系统中唯一订单号，必填
		//String identifierOr = ParamUtil.getParameter(request, "identifierOr", "");
	 	//String identifierOr = getIdentifierOr();
		//付款金额，必填
		String tradeAmount = ParamUtil.getParameter(request, "tradeAmount", "10");
		//订单名称，必填
		String subject = ParamUtil.getParameter(request, "subject", "");
		//商品数量
		Integer number = ParamUtil.getIntParameter(request, "number", 0);
		
		switch(number){
			case 100 : tradeAmount = "10";
				break;
			case 1000 : tradeAmount = "100";
				break;
			case 2000 : tradeAmount = "200";
				break;
			case 5000 : tradeAmount = "500";
				break;
			default : 	number = 100;
						tradeAmount = "10";	
		}
		
		alipayConfig.setPartner(alipayid);
 		alipayConfig.setSeller_id(alipayid);
 		alipayConfig.setKey(alipaykey);
 		alipayConfig.setNotify_url(notify_url);
	 	alipayConfig.setReturn_url(return_url);
	 	
	 	//支付单基础记录入库，短信购买记录入库
	 	PayBillsEntity payment = new PayBillsEntity();
		payment.setIdentifier(IdWorker.generateSequenceNo());
        payment.setClientid(user);
        payment.setCompany(user.getCompany());
        payment.setType(CommonConfig.PayApp.SMS + "");
        payment.setState("5");
        payment.setPaymentmethod(CommonConfig.PayType.alipay + "");
        payment.setPayName(subject);
        payment.setRemark(subject);
		payment.setAlreadypaidmoney(new BigDecimal(tradeAmount));
		payment  = payBillsService.paymentUser( payment  );
		if (StringUtil.isNotEmpty(payment.getId())) {
			paySmsRecordService.paymentRecordSave(payment.getId(), new BigDecimal(tradeAmount), number, subject, alipayid);
		}
	 	
	 	
	 	//获得初始化的sParaTemp
		Map<String, String> sParaTemp = initAlipayClient(alipayConfig, payment.getId(), subject, tradeAmount, "");
		result = AlipaySubmit.buildRequest(sParaTemp,"get","确认",alipayConfig);
		
		modelMap.put("result", result);
		return new ModelAndView("admin/pay/alipayDo");
		
	}
	
	@RequestMapping(value = "notifyUrl")
	@ResponseBody
	public void notifyUrl(HttpServletRequest request, HttpServletResponse response){
		AlipayConfig alipayConfig = this.initAlipayConfig();
		Map<String,String> params = new HashMap<String,String>();
		Map<String,String[]> requestParams = request.getParameterMap();
		String identifierOr = ParamUtil.getParameter(request, "out_trade_no", "");//商户订单号
		String tradeStatus = ParamUtil.getParameter(request, "trade_status", "");//交易状态
		
		logger.error("#######################");
		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			/*for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}*/
			//乱码解决，这段代码在出现乱码时使用
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			logger.error("#name:"+name+"#valueStr:"+valueStr);
			params.put(name, valueStr);
		}
		logger.error("#######################");
		//计算得出通知验证结果
		boolean verifyResult = AlipayNotify.verifyOriginal(params, alipayConfig);
		if(verifyResult){//验证成功
			systemService.addLog("通过支付状态认证trade_status，进入订单状态验证：", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
			if(tradeStatus.equals("TRADE_FINISHED") || tradeStatus.equals("TRADE_SUCCESS") ){//TRADE_FINISHED    交易完成  TRADE_SUCCESS  交易成功（或支付成功）
				smsRelateService.companyReviewedUpdateById(identifierOr);//支付成功后
			}
			//paySmsRecordService.paymentRecordUpdateByIdentifierOr(identifierOr, tradeNum, tradeStatus, initReturnParams(params));//更新支付记录交易号及交易状态
		}else{//验证失败
			logger.error(">>>>>>>>>>>>notifyAlipaya 支付回调认证失败！");
			systemService.addLog("支付宝支付回调失败", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		}
	}
	
	@RequestMapping(value = "returnUrl")
	public ModelAndView returnUrl(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
		AlipayConfig alipayConfig = this.initAlipayConfig();
		Map<String,String> params = new HashMap<String,String>();
		Map<String,String[]> requestParams = request.getParameterMap();
		//String identifierOr = ParamUtil.getParameter(request, "out_trade_no", "");//商户订单号
		//String tradeNum = ParamUtil.getParameter(request, "trade_no", "");//支付宝交易号
		String tradeStatus = ParamUtil.getParameter(request, "trade_status", "");//交易状态			
		//String tradeAmount = ParamUtil.getParameter(request, "total_fee", "");//付款金额
		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用
			/*try {
				valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}*/
			params.put(name, valueStr);
		}
		
		//计算得出通知验证结果
		boolean verifyResult = AlipayNotify.verifyOriginal(params, alipayConfig);
		if(verifyResult){//验证成功
			if(tradeStatus.equals("TRADE_FINISHED") || tradeStatus.equals("TRADE_SUCCESS")){
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
				//smsRelateService.companyReviewedUpdateById(identifierOr);//支付成功后
				modelMap.put("result", "true");
			}else{
				modelMap.put("result", "false");
			}
			//payService.paymentRecordUpdateByIdentifierOr(identifierOr, tradeNum, tradeStatus, initReturnParams(params));
			//System.out.println("#trade_no:"+identifierOr+"<br/>#out_trade_no:"+tradeNum+"<br/>#total_amount:"+tradeAmount+"#tradeStatus:"+tradeStatus);
		}else{
			modelMap.put("result", "验签失败");
		}
		
		return new ModelAndView("admin/pay/returnUrl");
	}
	
	
	private AlipayConfig initAlipayConfig(){
		AlipayConfig alipayConfig = new AlipayConfig();
		/*TSUser user  = ResourceUtil.getSessionUserName();
		if( user != null && StringUtil.isNotEmpty(user.getId()) ){
			user = systemService.get(TSUser.class, user.getId());
		}
		CompanyThirdpartyEntity  companyThirdparty = user.getCompany().getCompanythirdparty();
		if(companyThirdparty != null){
			alipayConfig.setPartner(companyThirdparty.getAlipayid() != null ? companyThirdparty.getAlipayid() : "");
			alipayConfig.setKey(companyThirdparty.getAlipaykey() != null ? companyThirdparty.getAlipaykey() : "");
		}*/
		String alipayid = CommonConfig.getAliPidKey().get("xncPid").toString();//星农场商户号
		String alipaykey = CommonConfig.getAliPidKey().get("xncKey").toString();//MD5密钥
		logger.error("#alipayid:"+alipayid+"#alipaykey:"+alipaykey);
		alipayConfig.setPartner(alipayid);
		alipayConfig.setKey(alipaykey);
		return alipayConfig;
	}
	
	
	/**
	 * 支付初始化
	 * */
	private static Map<String, String> initAlipayClient(AlipayConfig alipayConfig, String out_trade_no, String subject, String total_fee, String body){
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", AlipayConfig.service);
        sParaTemp.put("partner", alipayConfig.getPartner());
        sParaTemp.put("seller_id", alipayConfig.getSeller_id());
        sParaTemp.put("_input_charset", AlipayConfig.input_charset);
		sParaTemp.put("payment_type", AlipayConfig.payment_type);
		sParaTemp.put("notify_url", alipayConfig.getNotify_url());
		sParaTemp.put("return_url", alipayConfig.getReturn_url());
		sParaTemp.put("anti_phishing_key", AlipayConfig.anti_phishing_key);
		sParaTemp.put("exter_invoke_ip", AlipayConfig.exter_invoke_ip);
		sParaTemp.put("out_trade_no", out_trade_no);
		sParaTemp.put("subject", subject);
		sParaTemp.put("total_fee", total_fee);
		//sParaTemp.put("body", body);
		return sParaTemp;
	}
	
	/**
	 * 返回参数进行封装
	 * */
	private static String initReturnParams(Map<String, String> params){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("buyerId", params.get("buyer_id"));
		map.put("buyerEmail", params.get("buyer_email"));
		map.put("sellerId", params.get("seller_id"));
		map.put("sellerEmail", params.get("seller_email"));
		map.put("notifyId", params.get("notify_id"));
		map.put("notifyTime", params.get("notify_time"));
		return JsonUtil.toJson(map);
	}
	
	private static String getIdentifierOr(){
		String year = c.get(Calendar.YEAR)+"";//年
		String month = c.get(Calendar.MONTH)+1 <=9 ? "0"+(c.get(Calendar.MONTH)+1) : ""+(c.get(Calendar.MONTH)+1);//月
		String day = c.get(Calendar.DATE) <=9 ? "0"+c.get(Calendar.DATE) : ""+c.get(Calendar.DATE);//日
		String hour = c.get(Calendar.HOUR_OF_DAY)+"";//时
		String minute = c.get(Calendar.MINUTE) <=9 ? "0"+c.get(Calendar.MINUTE) : ""+c.get(Calendar.MINUTE);//分 
		String second = c.get(Calendar.SECOND) <=9 ? "0"+c.get(Calendar.SECOND) : ""+c.get(Calendar.SECOND);//秒
		String	millisecond = c.get(Calendar.MILLISECOND)+"";//毫秒
		
		return year + month + day + hour + minute + second + millisecond;
	}	
}
