package cn.gov.xnc.admin.pay.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.pay.entity.PaySmsRecordEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.SqlUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
@Service("paySmsRecordDAO")
public class PaySmsRecordDAO {
	@Resource(name="commonService")
	private CommonService commonService;
	private final String TABLE = "xnc_pay_sms_record";
	private final String FIELD = "id, payBillsId, user_id, company_id ,trade_amount ,number ,returnParams ,createDate, updateDate";
	private PaySmsRecordDAO(){}
	
	/**
	 * 支付查询，单条数据
	 * */
	public PaySmsRecordEntity getPaymentRecordBypayBid(PaySmsRecordEntity paySmsRecord){
		PaySmsRecordEntity PaySmsRecordEntity = commonService.findUniqueByProperty(PaySmsRecordEntity.class, "payBillsId", paySmsRecord.getPayBillsId());
		return PaySmsRecordEntity;
	}
	
	/**
	 * 支付查询，单条数据
	 * */
	public PaySmsRecordEntity getPaymentRecordSingleData(PaySmsRecordEntity paySmsRecord){
		PaySmsRecordEntity PaySmsRecordEntity = commonService.findUniqueByProperty(PaySmsRecordEntity.class, "id", paySmsRecord.getId());
		return PaySmsRecordEntity;
	}
	
	/**
	 * 支付保存
	 * */
	public Serializable paymentRecordSave(PaySmsRecordEntity paySmsRecord){
		TSUser user = ResourceUtil.getSessionUserName();
		paySmsRecord.setCreateDate(DateUtils.gettimestamp());
		paySmsRecord.setCompany_id(user.getCompany().getId());
		paySmsRecord.setUser_id(user.getId());
		Serializable id = commonService.save(paySmsRecord);
		return id;
	}
	/**
	 * 支付记录更新
	 * @return 
	 * */
	public Integer paymentRecordUpdateById(PaySmsRecordEntity paySmsRecord){
		Map<String ,Object> where = new HashMap<String ,Object>();
		Integer result = 0 ;
		where.put("id", paySmsRecord.getId());
		paySmsRecord.setId(null);
		String sql = SqlUtil.makeUpdateSql(TABLE, MyBeanUtils.beanToMapNew(paySmsRecord), where);
		System.out.println("#"+sql);
		result = commonService.updateBySqlString(sql);
		return result;
	}
	/**
	 * 支付记录更新
	 * @return 
	 * */
	public Integer paymentRecordUpdateByPayBillsId(PaySmsRecordEntity paySmsRecord){
		Map<String ,Object> where = new HashMap<String ,Object>();
		Integer result = 0;
		where.put("payBillsId", paySmsRecord.getPayBillsId());
		paySmsRecord.setPayBillsId(null);
		String sql = SqlUtil.makeUpdateSql(TABLE, MyBeanUtils.beanToMapNew(paySmsRecord), where);
		result = commonService.updateBySqlString(sql);
		return result;
	}
	/**
	 * 支付记录列表
	 * */
	public DataGrid getPaymentRecordListPage(DataGrid dataGrid, PaySmsRecordEntity paySmsRecord, TSCompany tSCompany){
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String ,Object> where = null;
		String field = dataGrid.getField() == null ? FIELD : dataGrid.getField();
		StringBuilder sql = new StringBuilder();
		long count = 0;
		sql.append("select field");
		sql.append(" from " + TABLE);
		//sql.append(" from " + TABLE + " INNER JOIN t_s_company on " + TABLE + ".company_id=t_s_company.id ");
		sql.append(" where 1=1");
		sql.append(" and company_id='" + user.getCompany().getId() + "'");
		where = MyBeanUtils.beanToMapNew(paySmsRecord);
		for(String key : where.keySet()){
			if(where.get(key) != null){
				sql.append(" and " + key + "='" + where.get(key).toString().trim() + "'");				
			}
		}
		where = MyBeanUtils.beanToMapNew(tSCompany);
		for(String key : where.keySet()){
			if(where.get(key) != null){
				sql.append(" and " + key + " like '%" + where.get(key).toString().trim() + "%'");				
			}
		}
		count = commonService.getCountForJdbc(sql.toString().replace("field", "count(1)"));
		if(dataGrid.getSort()!=null){
			sql.append(" order by " + dataGrid.getSort() + " desc");
		}
		List<Map<String, Object>> mapList = commonService.findForJdbc(sql.toString().replace("field", field),dataGrid.getPageNum(),dataGrid.getShowNum());
		dataGrid.setTotal((int) count);
		dataGrid.setResults(mapList);
		return dataGrid;
	}
}
