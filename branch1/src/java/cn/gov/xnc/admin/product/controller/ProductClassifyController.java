package cn.gov.xnc.admin.product.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.product.entity.ProductClassifyEntity;
import cn.gov.xnc.admin.product.service.ProductClassifyServiceI;

/**   
 * @Title: Controller
 * @Description: 产品分类
 * @author zero
 * @date 2016-09-28 15:46:49
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/productClassifyController")
public class ProductClassifyController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductClassifyController.class);

	@Autowired
	private ProductClassifyServiceI productClassifyService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 产品分类列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView productClassify(HttpServletRequest request) {
		return new ModelAndView("admin/product/productClassifyList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(value = "datagrid")
	public void datagrid(ProductClassifyEntity productClassify,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ProductClassifyEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, productClassify, request.getParameterMap());
		this.productClassifyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除产品分类
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ProductClassifyEntity productClassify, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		productClassify = systemService.getEntity(ProductClassifyEntity.class, productClassify.getId());
		message = "产品分类删除成功";
		productClassifyService.delete(productClassify);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加产品分类
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ProductClassifyEntity productClassify, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(productClassify.getId())) {
			message = "产品分类更新成功";
			ProductClassifyEntity t = productClassifyService.get(ProductClassifyEntity.class, productClassify.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(productClassify, t);
				productClassifyService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "产品分类更新失败";
			}
		} else {
			Map<String, Object> attributes = new HashMap<String, Object>();
			try {
				if(StringUtil.isNotEmpty(productClassify.getClassifyname())){
			message = "产品分类添加成功";
			productClassifyService.save(productClassify);
			
			attributes.put("id", productClassify.getId());
			attributes.put("classifyname", productClassify.getClassifyname());
			j.setAttributes(attributes);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
				}
			}catch (Exception e) {
				message = "产品分类添加失败";
			}
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 产品分类列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ProductClassifyEntity productClassify, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productClassify.getId())) {
			productClassify = productClassifyService.getEntity(ProductClassifyEntity.class, productClassify.getId());
			req.setAttribute("productClassifyPage", productClassify);
		}else {
			productClassify = new ProductClassifyEntity();
			req.setAttribute("productClassifyPage", productClassify);
		}
		return new ModelAndView("admin/product/productClassify");
	}
	
	
	/**
	 * 产品分类列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "details")
	public ModelAndView details(ProductClassifyEntity productClassify, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productClassify.getId())) {
			productClassify = productClassifyService.getEntity(ProductClassifyEntity.class, productClassify.getId());
			req.setAttribute("productClassifyPage", productClassify);
		}else {
			productClassify = new ProductClassifyEntity();
			req.setAttribute("productClassifyPage", productClassify);
		}
		return new ModelAndView("admin/product/productClassifyDetails");
	}
	
	
	/**
	 * 选择产品分类
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiceProductClassify")
	public ModelAndView choiceProductClassify(HttpServletRequest request) {
		return new ModelAndView("admin/product/choiceProductClassify");
	}
}
