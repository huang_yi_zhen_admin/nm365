package cn.gov.xnc.admin.product.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.product.service.ProductBrandServiceI;

/**   
 * @Title: Controller
 * @Description: 品牌信息
 * @author zero
 * @date 2016-09-28 15:45:26
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/productBrandController")
public class ProductBrandController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductBrandController.class);

	@Autowired
	private ProductBrandServiceI productBrandService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 品牌信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView productBrand(HttpServletRequest request) {
		return new ModelAndView("admin/product/productBrandList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ProductBrandEntity productBrand,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ProductBrandEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, productBrand, request.getParameterMap());
		this.productBrandService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除品牌信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ProductBrandEntity productBrand, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		productBrand = systemService.getEntity(ProductBrandEntity.class, productBrand.getId());
		message = "品牌信息删除成功";
		productBrandService.delete(productBrand);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加品牌信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ProductBrandEntity productBrand, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(productBrand.getId())) {
			message = "品牌信息更新成功";
			ProductBrandEntity t = productBrandService.get(ProductBrandEntity.class, productBrand.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(productBrand, t);
				productBrandService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				j.setSuccess(false);
				message = "品牌信息更新失败";
			}
		} else {
			Map<String, Object> attributes = new HashMap<String, Object>();
			try {
				if(StringUtil.isNotEmpty(productBrand.getBrandname())){
					message = "品牌信息添加成功";
					productBrandService.save(productBrand);
					//j.setObj(productBrand);
					attributes.put("id", productBrand.getId());
					attributes.put("brandname", productBrand.getBrandname());
					j.setAttributes(attributes);
					systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
				}
			}catch (Exception e) {
				j.setSuccess(false);
				message = "品牌信息更新失败";
			}
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 品牌信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ProductBrandEntity productBrand, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productBrand.getId())) {
			productBrand = productBrandService.getEntity(ProductBrandEntity.class, productBrand.getId());
			req.setAttribute("productBrandPage", productBrand);
		}else {
			productBrand = new ProductBrandEntity();
			req.setAttribute("productBrandPage", productBrand);
		}
		return new ModelAndView("admin/product/productBrand");
	}
	
	
	/**
	 * 品牌信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "details")
	public ModelAndView details(ProductBrandEntity productBrand, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productBrand.getId())) {
			productBrand = productBrandService.getEntity(ProductBrandEntity.class, productBrand.getId());
			req.setAttribute("productBrandPage", productBrand);
		}else {
			productBrand = new ProductBrandEntity();
			req.setAttribute("productBrandPage", productBrand);
		}
		return new ModelAndView("admin/product/productBrandDetails");
	}
	
	
	
	
	/**
	 * 选择产品品牌
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiceProductBrand")
	public ModelAndView choiceProductBrand(HttpServletRequest request) {
		return new ModelAndView("admin/product/choiceProductBrand");
	}
}
