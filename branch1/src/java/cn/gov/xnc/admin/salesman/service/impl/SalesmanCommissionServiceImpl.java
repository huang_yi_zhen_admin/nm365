package cn.gov.xnc.admin.salesman.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.salesman.entity.SalesmanCommissionEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanCommissionServiceI;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;

@Service("salesmanCommissionService")
public class SalesmanCommissionServiceImpl extends CommonServiceImpl implements SalesmanCommissionServiceI {
	
	
	/**
	 * 发货后  创建业务提现流水
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "SalesmanCommissionSave")
	@ResponseBody
	public AjaxJson SalesmanCommissionSave(OrderEntity  order  ) {
		
		AjaxJson j = new AjaxJson();
		String message;
		order =  get(OrderEntity.class, order.getId());
		
		//PayBillsOrderEntity payBillsOrder = get(PayBillsOrderEntity.class, order.getBillsorderid().getId());
		UserSalesmanEntity tsuserSalesman  = get(UserSalesmanEntity.class, order.getYewu().getTsuserSalesman().getId());
		
		SalesmanCommissionEntity salesmanCommission = new SalesmanCommissionEntity();
			salesmanCommission.setOrderid(order);
			salesmanCommission.setYwuser(order.getYewu());
			salesmanCommission.setCompany(order.getCompany());
			salesmanCommission.setCreatedate(DateUtils.getDate());
			salesmanCommission.setType("3");
			salesmanCommission.setReceived("1");
			salesmanCommission.setMoney(order.getPriceyw());
			tsuserSalesman.setFrozenmoney(tsuserSalesman.getFrozenmoney().add(order.getPriceyw()));
		message = "业务员提成流水添加成功";
		save(salesmanCommission);
		saveOrUpdate(tsuserSalesman);
		j.setMsg(message);
		
		return j;
		
	}
	
}