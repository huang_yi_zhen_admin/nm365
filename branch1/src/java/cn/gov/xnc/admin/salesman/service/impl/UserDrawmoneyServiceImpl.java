package cn.gov.xnc.admin.salesman.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.salesman.service.UserDrawmoneyServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

@Service("userDrawmoneyService")
@Transactional
public class UserDrawmoneyServiceImpl extends CommonServiceImpl implements UserDrawmoneyServiceI {

	@Override
	public StatisPageVO statisUserDrawmoney(TSCompany company, HttpServletRequest request) {
		String state = request.getParameter("state");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.applicationmoney),0) value2 FROM xnc_user_drawmoney a ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& "1".equals(state)){
			Sql.append(" AND a.state <> '2' ");//待审核单
			
		}else if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
				&& "2".equals(state)){
			
			Sql.append(" AND a.state = '2' ");//已审核单
		}

		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		
		return statisPageVO;
	}
	
}