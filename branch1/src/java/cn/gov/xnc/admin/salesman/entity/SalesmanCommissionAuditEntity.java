package cn.gov.xnc.admin.salesman.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 业务结算流水订单关系
 * @author zero
 * @date 2016-11-27 19:14:52
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_salesman_commission_audit", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class SalesmanCommissionAuditEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**审核*/
	private SalesmanAuditEntity auditid;
	/**业务流水*/
	private SalesmanCommissionEntity salesmanid;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  审核
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUDITID")
	public SalesmanAuditEntity getAuditid(){
		return this.auditid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  审核
	 */
	public void setAuditid(SalesmanAuditEntity auditid){
		this.auditid = auditid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  业务流水
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SALESMANID")
	public SalesmanCommissionEntity getSalesmanid(){
		return this.salesmanid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  业务流水
	 */
	public void setSalesmanid(SalesmanCommissionEntity salesmanid){
		this.salesmanid = salesmanid;
	}
}
