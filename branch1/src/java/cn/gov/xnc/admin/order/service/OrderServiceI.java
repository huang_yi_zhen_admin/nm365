package cn.gov.xnc.admin.order.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.excel.vo.XiadanProductVO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface OrderServiceI {
	
	public enum  State{
		
		UNPAID("待付款", "1"), UNDELIVERED("待发货", "2"), DELIVERED("已发货", "3"), RECEIVED("已签收", "4"), CANCEL("已取消", "5"), CANCELAUDIT("取消待审核", "6"), CANCELREJECT("取消拒绝","7"), REBACK("退单","8");
	    
		private String name ;
	    private String value ;
	     
	    private State( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
		
	
	public AjaxJson orderGeneratorJson(HttpServletRequest request, List<XiadanProductVO> xiadanlist) throws Exception;
	
	public List<OrderEntity> orderGenerator(HttpServletRequest request, List<XiadanProductVO> xiadanlist) throws Exception;

	
	/**
	* 公司产品销售数据分析
	* String companyid 公司ID
	* String startTime 开始时间
	* String endTime   截至时间
	*/
	public List<OrderEntity> salesCPList ( String companyid , String startTime, String endTime );
	
	
	
	/**
	* 公司发货情况分析
	* String companyid 公司ID
	* String startTime 开始时间
	* String endTime   截至时间
	*/
	public Map<String, OrderEntity> salesOPMap ( String companyid  );
	
	/**
	* 月销售统计
	* 
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesTIC( String userCLid, String productId, String startTime, String endTime ,String companyId );

	/**
	* 产品销售统计销售(按月统计量  ，金额)
	* 
	* String companyId 公司ID
	* String userCLid  采购商信息
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesCPBL(String companyId, String userCLid , String startTime, String endTime) ;
	/**
	* 产品销售统计销售(按月统计量  ，金额)
	* 
	* String companyId 公司ID
	* String userCLid  采购商信息
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<Map<String, Object>> salesCPBL_new(String companyId, String userCLid  , String startTime, String endTime);
	/**
	* 渠道销售统计销售情况
	* 
	* String productId 商品id
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesQDXS(String  companyId , String userid, String startTime, String endTime);
	/**
	* 采购商数据统计
	* 
	* String userid 用户id
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesCGXTJ(String  companyId , String userid, String startTime, String endTime);
	
	/**
	* 采购商数据统计,供图表使用
	* 
	* String userid 用户id
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesCGXTJCharts(String  companyId , String userid, String startTime, String endTime);
	
	/**
	* 用户消费区域分析
	* 
	* String userCLid 采购商id
	* String province 省份中文名称
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesYH( String companyId ,String userCLid ,String province, String startTime, String endTime);
	
	
	
	/**
	* 产品发货状态图
	* 
	* String userCLid 采购商id
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	
	public List<OrderEntity> salesdeparture(String companyId ,String userCLid ,String productId, String startTime, String endTime);
	/**
	* 业务情况统计
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesYewu( String yewuid, String productId, String startTime, String endTime ,String companyId);
	
	/**
	 * 退单审核统计
	 * @param company
	 * @param request
	 * @return
	 */
	public StatisPageVO statisCancelOrder(TSCompany company,HttpServletRequest request);
	
	/**
	 * 订单统计
	 * @param company
	 * @param orderState
	 * @return
	 */
	public StatisPageVO statisOrderByState(TSCompany company,String orderState);
	
	/**
	 * 订单统计
	 * @param company
	 * @param request
	 * @return
	 */
	public StatisPageVO statisOrderByRequest(TSCompany company,HttpServletRequest request);
	
	/**
	 * 合作商订单统计
	 * @param company
	 * @param request
	 * @return
	 */
	public StatisPageVO statisCopartnerOrders(TSCompany company,HttpServletRequest request, TSUser receiver);
	
	/**
	 * 合作商订单统计
	 * @param company
	 * @param orderState
	 * @return
	 */
	public StatisPageVO statisCopartnerOrderByState(TSCompany company,String orderState, TSUser receiver);
	
	
	
	/**
	 * 根据订单id获取订单列表
	 * @param orderIds
	 * @return
	 */
	public List<OrderEntity> getOrderListByIds(String orderIds);
	/**
	 * 添加订单明细,修改订单,小单订单属性修改
	 * @param orderIds 
	 * @return
	 */
	public AjaxJson orderSave(OrderEntity order, HttpServletRequest request);
	
}
