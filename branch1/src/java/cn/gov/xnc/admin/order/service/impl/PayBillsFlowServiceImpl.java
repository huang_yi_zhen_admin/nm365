package cn.gov.xnc.admin.order.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.order.service.PayBillsFlowServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("payBillsFlowService")

public class PayBillsFlowServiceImpl extends CommonServiceImpl implements PayBillsFlowServiceI {
	
}