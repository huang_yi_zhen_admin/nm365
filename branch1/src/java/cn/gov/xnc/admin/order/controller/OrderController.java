package cn.gov.xnc.admin.order.controller;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.express.service.ExpressServiceI;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;
import cn.gov.xnc.admin.kuaidi.service.Kuaid100ServiceI;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticsServiceI;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanCommissionServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ExpressSearchUtil;
import cn.gov.xnc.system.core.util.JsonUtil;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;

/**   
 * @Title: Controller
 * @Description: 订单明细
 * @author zero
 * @date 2016-10-02 02:00:47
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderController")
public class OrderController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderController.class);
	@Autowired
	private Kuaid100ServiceI kuaid100ServiceI;
	@Autowired
	private MessageTemplateServiceI messageTemplateService ;
	@Autowired
	private SalesmanCommissionServiceI salesmanCommissionService ;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private FreightDetailsServiceI freightDetailsService;
	@Autowired
	private UserGradePriceServiceI userGradePriceService;
	@Autowired
	private ExpressServiceI expressService;
	@Autowired
	private OrderLogisticsServiceI orderLogisticsService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView order(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
//		CriteriaQuery cq_u = new CriteriaQuery(TSUser.class);
//			cq_u.eq("company", user.getCompany());
//			cq_u.eq("type", "4");
//			cq_u.eq("status", new Short("1"));
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
//		List<TSUser> userlist = systemService.getListByCriteriaQuery(cq_u,false);//获取全部商品列表
		
//		request.setAttribute("departsUserlist", RoletoJson.listToReplaceStr(userlist, "realname", "id"));
		
		if("6".equals(user.getType())){//发货商进入
			request.setAttribute("statisOrder2Send", orderService.statisCopartnerOrderByState(company, "2", user));
			request.setAttribute("statisOrderSendOK", orderService.statisCopartnerOrderByState(company, "3", user));
			request.setAttribute("statisOrderSigned", orderService.statisCopartnerOrderByState(company, "4", user));
			request.setAttribute("statisOrderCancel", orderService.statisCopartnerOrderByState(company, "5", user));
			
			
			return new ModelAndView("admin/order/copartnerOrderList");
			
		}else{//非发货商进入
			
			request.setAttribute("statisOrder2Send", orderService.statisOrderByState(company, "2"));
			request.setAttribute("statisOrderSendOK", orderService.statisOrderByState(company, "3"));
			request.setAttribute("statisOrderSigned", orderService.statisOrderByState(company, "4"));
			request.setAttribute("statisOrderCancel", orderService.statisOrderByState(company, "5"));
			
			return new ModelAndView("admin/order/orderList");
		}
		
	}
	
	
	
	
	
	/**
	 * 特殊状态订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderListState")
	public ModelAndView orderListState(HttpServletRequest request) {
		
		
		String state = request.getParameter("state");
		if(StringUtil.isNotEmpty(state)){
			request.setAttribute("state", state);
		}
		String orderDate = request.getParameter("orderDate"); 
		if(StringUtil.isNotEmpty(orderDate)){
			request.setAttribute("orderDate", orderDate);
		}
		
		if( "2".equals(state) ){
			return new ModelAndView("admin/order/orderListStatefahuo");
		}
		
		return new ModelAndView("admin/order/orderListState");
	}
	
	@RequestMapping(value = "printExpress")
	public String printExpress(OrderEntity order, HttpServletRequest request) {
		
		request.setAttribute("printtype", "single");
		request.setAttribute("orderids", order.getId());
		return "admin/order/printExpress";
	}
	
	@RequestMapping(value = "printAllExpress")
	public String printAllExpress(PayBillsOrderEntity paybillsorder, HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
		cq.eq("company", user.getCompany());
		cq.eq("billsorderid", paybillsorder);
		List<OrderEntity> orderlist = systemService.getListByCriteriaQuery(cq, false);
		StringBuffer orderids = new StringBuffer();
		for( int i = 0; i <  orderlist.size(); i++ ){
			OrderEntity order= orderlist.get(i);
			orderids.append(order.getId());
			if( i != (orderlist.size()-1)){
				orderids.append(",");
			}
		}
		
		
		request.setAttribute("printtype", "all");
		request.setAttribute("orderids", orderids);
		return "admin/order/printExpress";
	}
	
	@RequestMapping(value = "printAllSelectedExpress")
	public String printAllSelectedExpress(String orderids, HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		
		request.setAttribute("printtype", "all");
		request.setAttribute("orderids", orderids);
		return "admin/order/printExpress";
	}
	
	
	

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderEntity order,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		String ordertype = ResourceUtil.getParameter("ordertype");
		if(StringUtil.isNotEmpty(ordertype)){
			order.setState(ordertype);
		}
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class, dataGrid);
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.like("identifieror", "%"+order.getIdentifieror()+"%");
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.like("productname", "%"+order.getProductname()+"%");
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.like("customername", "%"+order.getCustomername()+"%");
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.like("telephone", "%"+order.getTelephone()+"%");
			order.setTelephone(null);
		}
		if( StringUtil.isNotEmpty(order.getAddress())){
			cq.like("address", "%"+order.getAddress()+"%");
			order.setAddress(null);
		}
		if(StringUtil.isNotEmpty(order.getState()) &&  order.getState().indexOf(",") > 0 ){
			String stateS[] = order.getState().split(",");
			cq.in("state", stateS);
			order.setState(null);
		}else if(StringUtil.isNotEmpty(order.getState()) &&  order.getState().indexOf(",") == -1){
			cq.eq("state", order.getState());
			order.setState(null);
		}
		
		//业务员只能看自己负责客户的单子
		if("4".equals(user.getType())){
			cq.createAlias("yewu", "yw");
			cq.add( Restrictions.eq("yw.id", user.getId()) );
			order.setYewu(null);
		}
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		
		
		String orderDate = request.getParameter("orderDate"); 
		
		if(StringUtil.isNotEmpty(orderDate) && "departuredate".equals(orderDate)){
			//发货时间排序
			cq.addOrder("departuredate", SortDirection.desc);
		}else if( StringUtil.isNotEmpty(orderDate) && "paydate".equals(orderDate) ){
			//付款时间排序
			cq.addOrder("paydate", SortDirection.desc);
		}else if( StringUtil.isNotEmpty(orderDate) && "canceldate".equals(orderDate) ){
			//付款时间排序
			cq.addOrder("canceldate", SortDirection.desc);
		}else {
			//下单时间排序
			cq.addOrder("createdate", SortDirection.desc);
		}
		
		
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, order, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

//	/**
//	 * 删除订单明细
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "del")
//	@ResponseBody
//	public AjaxJson del(OrderEntity order, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//			order = systemService.get(OrderEntity.class, order.getId());
//			order.setState("5");
//			
//		message = "订单取消成功！";
//		systemService.saveOrUpdate(order);
//		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
//		
//		j.setMsg(message);
//		return j;
//	}


	
	
	/**
	 * 删除取消订单明细
	 * 
	 * @return
	 */
	@RequestMapping(value  = "cancelorder")
	@ResponseBody
	public AjaxJson cancelorder(OrderEntity order, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
			order = systemService.get(OrderEntity.class, order.getId());

		   //判断订单状态，如果未付款可以直接取消，如果是已付款，则发送取消审核单，由财务处理
			if("1".equals(order.getState())){//未付款
				message = "未付款订单取消成功！";
				order.setState("5");
				systemService.saveOrUpdate(order);
			//更新对应大单情况，金额做修改计算
				 PayBillsOrderEntity billsorderid = order.getBillsorderid();
			 	 BigDecimal moneyb = billsorderid.getObligationsmoney();//购买总结算
			 	 BigDecimal moneyp = billsorderid.getPayablemoney();//购买应付款

			 	   moneyb = moneyb.subtract((order.getPrice().add(order.getFreightpic())));
			 	   moneyp = moneyp.subtract((order.getPrice().add(order.getFreightpic())));
			 	   if( moneyb .compareTo( new BigDecimal("0.00") ) <= 0 ){//如果存在
			 		  billsorderid.setState("5");
			 	   }
				   billsorderid.setObligationsmoney(moneyb);
				   billsorderid.setPayablemoney(moneyp);
				   systemService.saveOrUpdate(billsorderid);
				   j.setSuccess(true);
			} else if("2".equals(order.getState())){//已付款
				message = "已付款订单提交取消申请成功，请等待审核！";
				order.setState("6");
				systemService.saveOrUpdate(order);
				//发送退单财务信息
				messageTemplateService.mosSendSmsDelivery_7(order);
				j.setSuccess(true);
			}else {
				message = "不符合订单取消状态，不能取消订单！";
				j.setSuccess(true);
			}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 添加订单明细,修改订单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderEntity order, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(order.getId())) {
			message = "订单明细更新成功";
			
			
			OrderEntity t = systemService.get(OrderEntity.class, order.getId());
			PayBillsOrderEntity billsorderid = t.getBillsorderid();
			
			//只有大单未支付的情况（包括先货后款）才可以修改订单价格
			if( ("1".equals(t.getState()) || "2".equals(t.getState())) && "1".equals(billsorderid.getState()) ){
				String useraddress = request.getParameter("useraddress");
				if(StringUtil.isNotEmpty(useraddress)){
					order.setAddress(useraddress +"_"+ order.getAddress());
				}
				ProductEntity p = systemService.findUniqueByProperty (ProductEntity.class,"id" ,t.getProductid().getId());
				if( (StringUtil.isNotEmpty(order.getAddress()) && !order.getAddress().equals(t.getAddress()))
						|| order.getNumber().compareTo(t.getNumber()) !=0 ){
					//重新计算商品运费
					/**
					 * 通过比较页面传回的运费和原运费，判断运费是否人工干涉
					 * 1、如果页面传回的运费不等于元订单运费，则说明人工干涉，此时采用页面传回的运费
					 * 2、如果页面传回的运费和元订单运费相同，则说明没有人工干涉，此时重新计算系统运费
					 */
					if( order.getFreightpic().compareTo(t.getFreightpic()) ==0 ){
						BigDecimal freightMoney = new BigDecimal("0.00"); // 运费价格
						try {
							freightMoney = freightDetailsService.getProductFreightByFullAddr(p, order.getAddress(), order.getNumber());
							
						} catch (Exception ex) {
							freightMoney = new BigDecimal("0.00");
						}
						order.setFreightpic(freightMoney);//写入新的运费信息	
					}
				}
				//如果 发生价格变化 ，按输入的标准更新  计算修改后的价格，与原价格差，更新支付单
				BigDecimal picMoneyO = new BigDecimal(t.getPrice().add(t.getFreightpic()).doubleValue()); // 运费价格
				//页面传回订单价格和系统订单价格相同，说明人工并未干预商品价格
				if(order.getPrice().compareTo(t.getPrice()) ==0 && order.getNumber().compareTo(t.getNumber()) != 0){
					//计算用户等级价格
					TSUser client = systemService.findUniqueByProperty(TSUser.class, "id", order.getClientid().getId());
					BigDecimal productprice = userGradePriceService.getProductFinalPrice(p, client);
					order.setPrice( productprice.multiply(new BigDecimal(order.getNumber())));
				}
				//重新计算订单价格
				BigDecimal picMoneyN = new BigDecimal( order.getPrice().add(order.getFreightpic()).doubleValue() );
				
				if( picMoneyO.compareTo(picMoneyN) !=0 ){ //存在价格差
					//读取支付单修改支付价格
					if( picMoneyN.compareTo(picMoneyO) == 1){ //金额价格变大
						//变化金额
						BigDecimal picMoneyB = picMoneyN.subtract(picMoneyO);
						  //payablemoney   应付款 增加 
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().add(picMoneyB)) ;//
						  //obligationsmoney 待付金额 增加
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().add(picMoneyB));//
						
					}else if(  picMoneyN.compareTo(picMoneyO) == -1 ){//金额价格减小
						  //payablemoney   应付款 减少
						BigDecimal picMoneyB = picMoneyO.subtract(picMoneyN);
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().subtract(picMoneyB)) ;//
						  //obligationsmoney 待付金额  减少
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().subtract(picMoneyB));//
					}
					systemService.saveOrUpdate(billsorderid);
				}
			}
			
			
			
			try {
				
				boolean messageSend = false ;
				if(  "2".equals(t.getState()) &&  StringUtil.isNotEmpty(order.getLogisticscompany()) && StringUtil.isNotEmpty(order.getLogisticsnumber() )  ){//发货信息，状态为待发货   同时填写物流公司  物流单号   自动修改为  发货状态
					order.setState("3");
					order.setDeparturedate(DateUtils.getDate());
					
					messageSend = true;
				}
				else{
					
					if(StringUtil.isNotEmpty(t.getLogisticsnumber()) 
							&& ( (null != order.getLogisticsnumber() && StringUtil.isEmpty(order.getLogisticsnumber().trim())) || StringUtil.isEmpty(order.getLogisticsnumber()) ) ){
						throw new Exception("更新失败:不能全部清空物流单号，请修改或者部分删除！");
					}
				}
				
				/*if(messageSend  
						|| (StringUtil.isEmpty(t.getLogisticsnumber()) && StringUtil.isNotEmpty(order.getLogisticsnumber())) 
						|| (StringUtil.isNotEmpty(t.getLogisticsnumber()) && StringUtil.isNotEmpty(order.getLogisticsnumber()) && !t.getLogisticsnumber().equals(order.getLogisticsnumber())) ){
					//如果已经存在的信息则删除全部,重新录入
					List<OrderLogisticsEntity> orderLogisticsListDel = null ;
					try {
						orderLogisticsListDel = t.getOrderLogisticsList();
					} catch (Exception e) {
						
					}
					if( orderLogisticsListDel != null && orderLogisticsListDel.size() > 0){
						t.setOrderLogisticsList(null);
						for( OrderLogisticsEntity delol :  orderLogisticsListDel ){
							
							systemService.delete(delol);
						}
					}
				    String logisticsnumberS[] = order.getLogisticsnumber().trim().split(",");
				    List<OrderLogisticsEntity> orderLogisticsList = new ArrayList<OrderLogisticsEntity>();
					if( logisticsnumberS.length > 0 ){
						for(int i=0 ;i <logisticsnumberS.length ; i++){
							OrderLogisticsEntity ol = new OrderLogisticsEntity();
							ol.setOrderid(t);
							ol.setLogisticscompany(order.getLogisticscompany());
							ol.setLogisticsnumber(logisticsnumberS[i]);
							ol.setDeparturedate(order.getDeparturedate());
						 orderLogisticsList.add(ol);
						}
						if(orderLogisticsList.size() > 0){
							systemService.batchSave(orderLogisticsList);
						}
					}
					
				}*/
				//更新小单
				MyBeanUtils.copyBeanNotNull2Bean(order, t);
				systemService.updateEntitie(t);
				
				//order 是页面传入对象
				expressService.updateManualExpress(order);
				
				if(StringUtil.isNotEmpty(t.getLogisticsnumber()) && StringUtil.isNotEmpty(t.getLogisticscompany())){
					//更新物流报文信息
					String logisticsnumberS[] = t.getLogisticsnumber().trim().split(",");
					String logisticsCompanyS[] = t.getLogisticscompany().trim().split(",");
					if( logisticsnumberS.length > 0 ){
						for(int i=0 ;i <logisticsnumberS.length ; i++){
							orderLogisticsService.saveOrderLogistics(t, logisticsnumberS[i], logisticsCompanyS[i]);
						}
					}
				}
				
				if(messageSend){
					billsorderid.setFreightstatus(payBillsOrderService.getPayBillsFreightStatus(billsorderid));
					systemService.saveOrUpdate(billsorderid);
					//发货通知下单客户
					messageTemplateService.mosSendSmsDelivery_6(t);
					//发货通知收货人
					messageTemplateService.mosSendSmsDelivery_5(t);
					salesmanCommissionService.SalesmanCommissionSave(t);
				}
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				j.setSuccess(false);
				message = "订单明细更新失败:" + e.getMessage();
			}
		} 
		j.setMsg(message);
		systemService.addLog(message, Globals.Log_Leavel_INFO, Globals.Log_Type_UPDATE);
		return j;
	}

	/**
	 * 订单明细列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderEntity order, HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		req.setAttribute("user", user);
		if (StringUtil.isNotEmpty(order.getId())) {
			order = systemService.getEntity(OrderEntity.class, order.getId());

			//存在地址信息的，状态为1的情况  对地址进行拆分
			String useraddress ="";
			if( "1".equals(order.getState()) ){//未付款订单允许修改地址
				String address =  order.getAddress();
				
				if(  StringUtil.isNotEmpty(address)){
					if( address.indexOf("_") > 0 ){
						useraddress = address.substring(0,address.lastIndexOf("_"));
					}
					order.setAddress( order.getAddress().replaceAll(useraddress, "").replace("_", "") );
				}
			}
			 TSTerritory territory = order.getTSTerritory();
			 if(null == territory){
				 territory = new TSTerritory();
			 }
			
			req.setAttribute("useraddress", useraddress);
			req.setAttribute("orderPage", order);
		}
		return new ModelAndView("admin/order/order");
	}
	
	
	/**
	 * 财务订单审核列表跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderAuditList")
	public ModelAndView orderAuditList(OrderEntity order, HttpServletRequest req) {
		return new ModelAndView("admin/order/orderAuditList");
	}
	
	/**
	 * 财务订单审核跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderAuditdDatagrid")
	public void orderAuditdDatagrid(OrderEntity order,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class, dataGrid);
		
		String []states = "5,6,7".split(",");
		if( !"5".equals(order.getState()) && !"6".equals(order.getState()) && !"7".equals(order.getState()) ){
			cq.in("state", states);
			order.setState(null);
		}
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.like("identifieror", "%"+order.getIdentifieror()+"%");
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.like("productname", "%"+order.getProductname()+"%");
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.like("customername", "%"+order.getCustomername()+"%");
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.like("telephone", "%"+order.getTelephone()+"%");
			order.setTelephone(null);
		}
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, order, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);

		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 财务订单审核跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderAudit")
	public ModelAndView orderAudit(OrderEntity order, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(order.getId())) {
			order = systemService.getEntity(OrderEntity.class, order.getId());
			req.setAttribute("orderPage", order);
		}
		return new ModelAndView("admin/order/orderAudit");
	}
	

	/**
	 * 财务订单审核处理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "orderAuditSave")
	@ResponseBody
	public AjaxJson orderAuditSave(OrderEntity order, HttpServletRequest req) {
		
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		if (StringUtil.isNotEmpty(order.getId())) {
			message = "退单审核修改更新成功";
			OrderEntity t = systemService.get(OrderEntity.class, order.getId());
			if( !"6".equals(t.getState()) &&  !"7".equals(t.getState())  ){
				message = "不符合审核状态订单,修改失败！";
				j.setMsg(message);
				return j;
			}
			if( "5".equals(order.getState()) && ( "6".equals(t.getState())  || "7".equals(t.getState()) )){
				PayBillsOrderEntity billsorderid = t.getBillsorderid();
				BigDecimal cancelMoney = new BigDecimal("0.00");
				if(billsorderid.getCancelMoney() != null ){
					cancelMoney = billsorderid.getCancelMoney();
				}
				billsorderid.setCancelMoney( t.getPrice().add(t.getFreightpic()).add( cancelMoney)  );
				systemService.saveOrUpdate(billsorderid);
				//通过状态才发送审核通知
				messageTemplateService.mosSendSmsDelivery_8(t);
			}
			try {
				
				if( "7".equals(order.getState())){
					order.setState("2");
				}
				t.setCanceldate(DateUtils.getDate());
				t.setState(order.getState());
				t.setRemarks(order.getRemarks());
				//MyBeanUtils.copyBeanNotNull2Bean(order, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "退单审核修改失败！";
			}
		} 
		j.setMsg(message);

		return j;
	}
	

	
	
	
//	/**
//	 * 快递物流查询
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "kuaidi")
//	public String kuaidi(HttpServletRequest req) {
//		req.setAttribute("orderid", req.getParameter("id"));
//		return "express/expresslist";
//	}
	
	/**
	 * 快递物流查询
	 * 
	 * @return
	 */
	@RequestMapping(value = "expressinfo")
	@ResponseBody
	public AjaxJson kuaidi(OrderEntity order, HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(order.getId())) {
			try {
				Map<String, Object> attributes = new HashMap<String, Object>();
				order = systemService.getEntity(OrderEntity.class, order.getId());
				String logisticscompany = order.getLogisticscompany();
				String logisticsresp = null;
				String state = null;
				String date = null;
				List<OrderLogisticsEntity> orderlogisticslist = order.getOrderLogisticsList();
				if(null != orderlogisticslist && orderlogisticslist.size() > 0){
					for (OrderLogisticsEntity orderLogisticsEntity : orderlogisticslist) {
						String logisticsnumber = orderLogisticsEntity.getLogisticsnumber();
						state = orderLogisticsEntity.getState();
						logisticsresp = ExpressSearchUtil.getOrderTracesByCompany(logisticscompany, orderLogisticsEntity.getLogisticsnumber());
						
						JSONObject obj = new JSONObject();
						obj = obj.parseObject(logisticsresp);
						
						orderLogisticsEntity.setMessage( logisticsresp );
						orderLogisticsEntity.setState( obj.getString("State"));
						if(obj.getJSONArray("Traces") != null && obj.getJSONArray("Traces").size()>0){
							List<Map<String, Object>> list = JsonUtil.toBean(obj.getJSONArray("Traces")+"", ArrayList.class);
							for(Map<String, Object> m : list){
								if(m.get("AcceptTime") != null){
									date = m.get("AcceptTime")+"";
								}
							}
						}
						orderLogisticsEntity.setLogisticsdate(DateUtils.parseDate(date, "yyyy-MM-dd HH:mm:ss"));
						attributes.put(logisticscompany + logisticsnumber, logisticsresp);
					}
					
					systemService.batchUpdate(orderlogisticslist);
				}else{
					String logisticsnum = order.getLogisticsnumber();
					String logisticscom = order.getLogisticscompany();
					
					if(null != logisticsnum && StringUtil.isNotEmpty(order.getLogisticsnumber())){
						
						String[] arrayLogisticsCode = logisticsnum.split(",");
						String[] arrayLogisticsCom = logisticscom.split(",");
						
						for (int i = 0; i < arrayLogisticsCode.length; i++) {
							logisticsresp = ExpressSearchUtil.getOrderTracesByCompany(logisticscompany, arrayLogisticsCode[i]);
							
							attributes.put(arrayLogisticsCom[i] + arrayLogisticsCode[i], logisticsresp);
						}
					}
				}
				
				req.setAttribute("expressinfo", attributes);
				
				j.setAttributes(attributes);
				
			} catch (Exception e) {
				systemService.addLog("快递查询出错", Globals.Log_Leavel_ERROR, Globals.Log_Type_OTHER);
			}
			
		}
		return j;
	}

	/**
	 * 查看订单详情列表页
	 * 
	 * @return
	 */
	@RequestMapping(value = "orderListSet")
	public ModelAndView orderListSet(HttpServletRequest request) {
		//增加产品列表查询
		String payBills_id = request.getParameter("payBillsId");
		if(StringUtil.isNotEmpty(payBills_id)){
			request.setAttribute("payBills_id", payBills_id);
		}
		return new ModelAndView("admin/order/orderListSet");
	}
	
	
	
	/**
	 * 查看订单详情列表页
	 * 
	 * @return
	 */
	@RequestMapping(value = "details")
	public ModelAndView details(OrderEntity order,HttpServletRequest request) {
		if (StringUtil.isNotEmpty(order.getId())) {
			order = systemService.getEntity(OrderEntity.class, order.getId());

			//存在地址信息的，状态为1的情况  对地址进行拆分
			String useraddress ="";
			if( "1".equals(order.getState()) ){//未付款订单允许修改地址
				String address =  order.getAddress();
				
				if(  StringUtil.isNotEmpty(address)){
					if( address.indexOf("_") > 0 ){
						useraddress = address.substring(0,address.lastIndexOf("_"));
					}
					order.setAddress( order.getAddress().replaceAll(useraddress, "").replace("_", "") );
				}
			}
			 TSTerritory territory = order.getTSTerritory();
			 if(null == territory){
				 territory = new TSTerritory();
			 }
			
			 request.setAttribute("useraddress", useraddress);
			 request.setAttribute("orderPage", order);
		}else{
			request.setAttribute("orderPage", order);
		}
		return new ModelAndView("admin/order/orderDetails");
	}
	
	
	@RequestMapping(value= "statisCancelOrders")
	@ResponseBody
	public AjaxJson statisCancelOrders(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计退单失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = orderService.statisCancelOrder(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计退单成功！");
			
		}
		
		return json;
	}
	
	@RequestMapping(value= "statisOrderDetails")
	@ResponseBody
	public AjaxJson statisOrderDetails(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计订单详情失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = orderService.statisOrderByRequest(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计订单详情成功！");
			
		}
		
		return json;
	}
	
	
	@RequestMapping(value= "statisCopartnerOrders")
	@ResponseBody
	public AjaxJson statisCopartnerOrdersByReq(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计订单详情失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = orderService.statisCopartnerOrders(company, req, user);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计订单详情成功！");
			
		}
		
		return json;
	}
	
	
	/**
	 * 特殊状态订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "copartnerOrdersState")
	public ModelAndView copartnerOrdersState(HttpServletRequest request) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		String state = request.getParameter("state");
		if(StringUtil.isNotEmpty(state)){
			request.setAttribute("state", state);
		}
		String orderDate = request.getParameter("orderDate"); 
		if(StringUtil.isNotEmpty(orderDate)){
			request.setAttribute("orderDate", orderDate);
		}
		
		if( "2".equals(state) ){//在派单中查找需要发货的订单
			return new ModelAndView("admin/order/copartnerOrdersStateSend");
		}
		if(!"6".equals(user.getType())){
			return new ModelAndView("admin/order/adminOrderSendList");
			
		}else{
			return new ModelAndView("admin/order/copartnerReceiveOrders");
		}
		
	}
	
	@RequestMapping(value = "copartnerDatagrid")
	public void copartnerDatagrid(OrderSendEntity orderSend, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		OrderEntity order = orderSend.getOrderid();
		if(null == order){
			order = new OrderEntity();
			orderSend.setOrderid(order);
		}
		String ordertype = ResourceUtil.getParameter("ordertype");
		if(StringUtil.isNotEmpty(ordertype)){
			order.setState(ordertype);
		}
		
		
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class, dataGrid);
		cq.createAlias("orderid", "order");
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.add(Restrictions.like("order.identifieror", "%"+order.getIdentifieror()+"%"));
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.add(Restrictions.like("order.productname", "%"+order.getProductname()+"%"));
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.add(Restrictions.like("order.customername", "%"+order.getCustomername()+"%"));
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.add(Restrictions.like("order.telephone", "%"+order.getTelephone()+"%"));
			order.setTelephone(null);
		}
		if(StringUtil.isNotEmpty(order.getAddress()) ){
			cq.add(Restrictions.like("order.address", "%"+order.getAddress()+"%"));
			order.setAddress(null);
		}
		if( StringUtil.isNotEmpty(order.getSendtype())){
			cq.add(Restrictions.eq("order.sendtype", order.getSendtype()));
			order.setSendtype(null);
		}
		if(StringUtil.isNotEmpty(order.getState()) ){
			String[] orders= order.getState().split(",");
			if( orders.length > 0 ){
				cq.add(Restrictions.in("order.state", orders));
				order.setState(null);
			}
		}
		String status = orderSend.getStatus();
		if(StringUtil.isEmpty(status)){
			cq.in("status", "1,2".split(","));
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		String createdate3 = request.getParameter("createdate3");
		String createdate4 = request.getParameter("createdate4");
		//派单时间
		SimpleDateFormat sdf1 = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.add(Restrictions.between("createtime", DateUtils.str2Date(createdate1,sdf1), DateUtils.str2Date(createdate2,sdf1)));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.add(Restrictions.ge("createtime", DateUtils.str2Date(createdate1,sdf1)));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.add(Restrictions.le("createtime", DateUtils.str2Date(createdate2,sdf1)));
		}
		//下单时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate3)&&StringUtil.isNotEmpty(createdate4)){
			cq.add(Restrictions.between("order.createdate", DateUtils.str2Date(createdate3,sdf), DateUtils.str2Date(createdate4,sdf)));
		}else if( StringUtil.isNotEmpty(createdate3) ){
			cq.add(Restrictions.ge("order.createdate", DateUtils.str2Date(createdate3,sdf)));
		}else if( StringUtil.isNotEmpty(createdate4) ){
			cq.add(Restrictions.le("order.createdate", DateUtils.str2Date(createdate4,sdf)));
		}
		
		
//		String orderDate = request.getParameter("orderDate"); 
//		
//		if(StringUtil.isNotEmpty(orderDate) && "departuredate".equals(orderDate)){
//			//发货时间排序
//			cq.addOrder("order.departuredate", SortDirection.desc);
//		}else if( StringUtil.isNotEmpty(orderDate) && "paydate".equals(orderDate) ){
//			//付款时间排序
//			cq.addOrder("order.paydate", SortDirection.desc);
//		}else if( StringUtil.isNotEmpty(orderDate) && "canceldate".equals(orderDate) ){
//			//付款时间排序
//			cq.addOrder("order.canceldate", SortDirection.desc);
//		}else {
//			//下单时间排序
//			cq.addOrder("order.createdate", SortDirection.desc);
//		}
		cq.addOrder("createtime", SortDirection.desc);
		if("6".equals(user.getType())){
			cq.eq("receiver", user);
		}
		cq.eq("distributecompanyid", user.getCompany());
		cq.add();
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderSend, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 订单明细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "copartnerOrderList")
	public ModelAndView copartnerOrderList(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		
		request.setAttribute("statisOrder2Send", orderService.statisCopartnerOrderByState(company, "2", user));
		request.setAttribute("statisOrderSendOK", orderService.statisCopartnerOrderByState(company, "3", user));
		request.setAttribute("statisOrderSigned", orderService.statisCopartnerOrderByState(company, "4", user));
		request.setAttribute("statisOrderCancel", orderService.statisCopartnerOrderByState(company, "5", user));
		
		
		return new ModelAndView("admin/order/copartnerOrderList");
	}
}
