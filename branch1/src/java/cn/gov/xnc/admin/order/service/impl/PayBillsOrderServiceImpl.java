package cn.gov.xnc.admin.order.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.sql.ordering.antlr.OrderByFragmentRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("payBillsOrderService")

public class PayBillsOrderServiceImpl extends CommonServiceImpl implements PayBillsOrderServiceI {
	
	private Logger logger = Logger.getLogger(PayBillsOrderServiceImpl.class);
	@Autowired
	private SystemService systemService;
	
	
	/**
	 *创建一个充值类型的流水订单 
	 * 
	 **/
	public PayBillsOrderEntity  payBillsOrderUser(  PayBillsOrderEntity payBillsOrder  ){
		
		
		
		
		
		
		return null;
		
	}

	@Override
	public StatisPageVO statisUserPaybills(TSCompany company, TSUser user,HttpServletRequest request) {
		String type = request.getParameter("type");
		String ordertype = request.getParameter("ordertype");//支付单类型
		String identifier = request.getParameter("identifier");
		String clientid = request.getParameter("clientid.id");
//		String state = request.getParameter("state");
		String paymentmethod = request.getParameter("paymentmethod");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(a.alreadyPaidMoney), 0) value1, IFNULL(SUM(a.payableMoney), 0) value2, IFNULL(SUM(a.obligationsMoney), 0) value3, count(1) value4 FROM xnc_pay_bills_order a ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		
		if(StringUtil.isNotEmpty(type) && StringUtil.isNotEmpty(type.trim())){
			Sql.append(" AND a.type = '" + type + "'");
		}
		if(StringUtil.isNotEmpty(ordertype) 
				&& ordertype.indexOf(",") > -1){//应付
			
			Sql.append(" AND a.state in (" + ordertype + ") ");
			
		}else if(StringUtil.isNotEmpty(ordertype) 
				&& ordertype.indexOf(",") == -1){
			
			Sql.append(" AND a.state ='"+ ordertype + "'");
		}
		
		if(StringUtil.isNotEmpty(identifier) && StringUtil.isNotEmpty(identifier.trim())){
			Sql.append(" AND a.identifier like '%" + identifier.trim() + "%'");
		}
//		if(StringUtil.isNotEmpty(state) && StringUtil.isNotEmpty(state.trim())){
//			Sql.append("AND a.state = '" + state.trim() + "'");
//		}
		if(null != user && StringUtil.isNotEmpty(user.getId())){
			Sql.append("AND a.clientId = '" + user.getId() + "'");
		}
		if(StringUtil.isNotEmpty(clientid) && StringUtil.isNotEmpty(clientid.trim())){
			Sql.append("AND a.clientId = '" + clientid.trim() + "'");
		}
		if(StringUtil.isNotEmpty(paymentmethod) && StringUtil.isNotEmpty(paymentmethod.trim())){
			Sql.append("AND a.PaymentMethod = '" + paymentmethod.trim() + "'");
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append("AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append("AND a.createDate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append("AND a.createDate <= '" + createdate2 + "'" );
		}

		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		
		return statisPageVO;
	}
	
	
	@Override
	public List<PayBillsOrderEntity> getBillsOrderListByIds(String billsIds) {
		List<PayBillsOrderEntity> billsorderlist = new ArrayList<PayBillsOrderEntity>();
		
		if(StringUtil.isNotEmpty(billsIds)){
			String[] billsIdList = billsIds.split(",");
			
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
			cq.setCurPage(1);
			cq.setPageSize(30);
			cq.in("id", billsIdList);
			cq.add();
			
			billsorderlist = getListByCriteriaQuery(cq, false);
		}
		
		
		return billsorderlist;
	}
	
	/**
	 * 先货后款审核
	 * @param payBillsOrder
	 * @param request
	 * @return
	 */
	@Override
	public AjaxJson sendFirstPaymentAudit(PayBillsOrderEntity payBillsOrder, HttpServletRequest request) {
		
		boolean success = true;
		int orderFailSize = 0;
		String letitgo = request.getParameter("letItGo");
		
		String msg = "先货后款通过审核";
		if("0".equals(letitgo)){
			msg = "取消先货后款";
		}
		
		AjaxJson json = new AjaxJson();
		//只有客户选择了先货后款的订单，才可以使用先货后款审核功能
		if(null != payBillsOrder && StringUtil.isNotEmpty(payBillsOrder.getId()) && "6".equals(payBillsOrder.getPaymentmethod())){
			
			try {
				List<OrderEntity> orderlist = payBillsOrder.getOrderS();
				if(null != orderlist && orderlist.size() > 0){
					
					for (OrderEntity orderEntity : orderlist) {
						
						String state = orderEntity.getState();
						if("1".equals(letitgo) && "1".equals( state )){//只有未付款状态的单子可以先货后款
							orderEntity.setState("2");
							
						}else if("0".equals(letitgo) && "2".equals(state)){//只有待发货状态的单子才可以取消先货后款
							orderEntity.setState("1");
							
						}else {
							msg = "不符合先货后款状态，" + msg + "失败！订单号：" + orderEntity.getId();
							systemService.addLog(msg, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
							orderFailSize++;
							break;
						}
					}
					//更新订单状态
					if(orderFailSize == 0){
						batchUpdate(orderlist);
						msg += "成功！";
						success = true;
						
					}else{
						success = false;
						msg += "失败！抱歉~您处理的单子有部分不符合先货后款状态，请查看详细！";
					}
					
					//取消订单的先货后款状态
					if("0".equals(letitgo) && success){
						PayBillsOrderEntity payBillsOrderEntity = findUniqueByProperty(PayBillsOrderEntity.class,"id",payBillsOrder.getId());
						payBillsOrderEntity.setPaymentmethod(null);//设置下单支付方式为 取消先货后款
						systemService.updateEntitie(payBillsOrderEntity);
					}
				}
			} catch (Exception e) {
				msg= "先货后款通过审核失败，请联系系统管理人员";
				success = false;
				logger.error("------------" + msg + e);
			}
			
		}else{
			msg= "先货后款通过审核失败~订单信息获取失败【或者】不是现货后款订单！";
			success = false;
			systemService.addLog(msg, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			logger.error("------------" + msg);
		}
		json.setMsg(msg);
		json.setSuccess(success);
		return json;
	}
	
	/**
	 * 获取大单物流状态
	 */
	@Override
	public String getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder) {
		String freightStatus = "1"; //未发货_1,已发货_2,部分发货_3
		Integer totalOrder = 1;
		Integer hasFreightOrderNum = 0;
		if(null != paybillsOrder){
			PayBillsOrderEntity payBills = findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
			List<OrderEntity> orderList = payBills.getOrderS();
			totalOrder = orderList.size();
			String isSend = null;
			for (OrderEntity orderEntity : orderList) {
				isSend = orderEntity.getState();//  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7
				if("3".equals(isSend) || "4".equals(isSend)){
					hasFreightOrderNum++;
				}
			}
			
		}
		if(totalOrder > 0){
			
			if( totalOrder == hasFreightOrderNum ){
				
				freightStatus = "2";
			}
			else if( hasFreightOrderNum > 0 && totalOrder > hasFreightOrderNum ){
				
				freightStatus = "3";
			}
		}
		
		return freightStatus;
	}

	@Override
	public AjaxJson cancelPayBillsOrder(PayBillsOrderEntity paybillsOrder, HttpServletRequest request) throws Exception {
		AjaxJson json = new AjaxJson();
		
		if(StringUtil.isEmpty(paybillsOrder.getId())){
			json.setSuccess(false);
			json.setMsg( "系统没有找到您要取消的订单！" );
			return json;
		}
		
		PayBillsOrderEntity payBillsOrderEntity = findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
		
		if(PayBillsOrderServiceI.State.UNPAID.getValue().equals( payBillsOrderEntity.getState() )){
			
			List<OrderEntity> orderlist = payBillsOrderEntity.getOrderS();
			for (OrderEntity orderEntity : orderlist) {
				
				if( !OrderServiceI.State.DELIVERED.getValue().equals( orderEntity.getState() ) 
						|| !OrderServiceI.State.RECEIVED.getValue().equals( orderEntity.getState() )
						|| !OrderServiceI.State.CANCELREJECT.getValue().equals( orderEntity.getState() )){
					
					orderEntity.setState( OrderServiceI.State.CANCEL.getValue());
					
				}else{
					json.setSuccess(false);
					json.setMsg( "取消订单失败，您有部分订单已经发货或者签收，请联系供货商进行处理或者申请退单！" );
					break;
				}
			}
			
			if(json.isSuccess()){
				
				batchSave(orderlist);
				
				payBillsOrderEntity.setState( PayBillsOrderServiceI.State.CANCEL.getValue() );
				
				updateEntitie( payBillsOrderEntity );
			}
		}
		else if(PayBillsOrderServiceI.State.CANCEL.getValue().equals( payBillsOrderEntity.getState() )){
			json.setSuccess(false);
			json.setMsg( "不要闹，您的订单已经取消！" );
			
		}else{
			
			json.setSuccess(false);
			json.setMsg( "取消订单失败，只有未支付订单才能取消，其它情况请申请退单！" );
		}
		return json;
	}
}