package cn.gov.xnc.admin.order.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticsServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.StringUtil;

@Service("orderLogisticsService")

public class OrderLogisticsServiceImpl extends CommonServiceImpl implements OrderLogisticsServiceI {

	@Override
	public void saveOrderLogistics(OrderEntity order, String logisticsCode, String logisticsCompany){
		if(null != order && StringUtil.isNotEmpty(logisticsCode) && StringUtil.isNotEmpty(logisticsCompany)){
			
			CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class);
			cq.eq("orderid.id", order.getId());
			cq.eq("logisticsnumber", logisticsCode);
			
			List<OrderLogisticsEntity> orderLogisticsList = getListByCriteriaQuery(cq, false);
			
			if(null != orderLogisticsList && orderLogisticsList.size() > 0){
				OrderLogisticsEntity orderLogistics = orderLogisticsList.get(0);
				orderLogistics.setLogisticsdate(DateUtils.getDate());
				
				updateEntitie(orderLogistics);
			}else{
				
				OrderLogisticsEntity ol = new OrderLogisticsEntity();
				ol.setOrderid(order);
				ol.setLogisticscompany(logisticsCompany);
				ol.setLogisticsnumber(logisticsCode);
				ol.setLogisticsdate(DateUtils.getDate());
				
				save( ol );
			}
		}
		
	}
	
}