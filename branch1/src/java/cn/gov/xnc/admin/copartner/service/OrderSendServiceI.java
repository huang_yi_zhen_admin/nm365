package cn.gov.xnc.admin.copartner.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderSendServiceI extends CommonService{

	public String sendPaybillsOrder(List<PayBillsOrderEntity> billsOrderList, HttpServletRequest req);
	
	public String sendOrders(List<OrderEntity> orderList, HttpServletRequest req);
	
	public OrderSendEntity getSendOrder(OrderEntity order, HttpServletRequest req);
}
