package cn.gov.xnc.admin.copartner.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 派单记录表
 * @author zero
 * @date 2017-01-12 15:13:23
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_order_send", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class OrderSendEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**订单号*/
	private OrderEntity orderid;
	/**受理人、订单处理人*/
	private TSUser receiver;
	/**订单派发人*/
	private TSUser distributer;
	/**派发人单位*/
	private TSCompany distributecompanyid;
	/**1 派单 2 处理 3 取消*/
	private java.lang.String status;
	/**创建时间*/
	private java.util.Date createtime;
	/**修改时间*/
	private java.util.Date updatetime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单号
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDERID")
	public OrderEntity getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单号
	 */
	public void setOrderid(OrderEntity orderid){
		this.orderid = orderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  受理人、订单处理人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RECEIVER")
	public TSUser getReceiver(){
		return this.receiver;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  受理人、订单处理人
	 */
	public void setReceiver(TSUser receiver){
		this.receiver = receiver;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单派发人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DISTRIBUTER")
	public TSUser getDistributer(){
		return this.distributer;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单派发人
	 */
	public void setDistributer(TSUser distributer){
		this.distributer = distributer;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  派发人单位
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DISTRIBUTECOMPANYID")
	public TSCompany getDistributecompanyid(){
		return this.distributecompanyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  派发人单位
	 */
	public void setDistributecompanyid(TSCompany distributecompanyid){
		this.distributecompanyid = distributecompanyid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  1 派单 2 处理 3 取消
	 */
	@Column(name ="STATUS",nullable=false,length=2)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  1 派单 2 处理 3 取消
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="UPDATETIME",nullable=true)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
}
