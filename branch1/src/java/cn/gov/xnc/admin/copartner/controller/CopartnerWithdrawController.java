package cn.gov.xnc.admin.copartner.controller;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.entity.CopartnerWithdrawEntity;
import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerAccountEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerOrderServiceI;
import cn.gov.xnc.admin.copartner.service.CopartnerWithdrawServiceI;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import freemarker.template.utility.DateUtil;
import sun.font.CreatedFontTracker;

/**   
 * @Title: Controller
 * @Description: 合作伙伴相关订单
 * @author zero
 * @date 2017-01-12 15:12:08
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/copartnerWithdrawController")
public class CopartnerWithdrawController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CopartnerWithdrawController.class);

	@Autowired
	private CopartnerWithdrawServiceI copartnerWithdrawOrderService;
	@Autowired
	private CopartnerOrderServiceI copartnerOrderService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * 结算业务流水
	 * @return
	 */
	@RequestMapping(value = "copartnerDrawOrderLog")
	public ModelAndView copartnerDrawOrderLog(CopartnerWithdrawEntity copartnerWithdraw, HttpServletRequest request) {
		String drawid = copartnerWithdraw.getId();
		request.setAttribute("drawid", drawid);
		return new ModelAndView("admin/copartner/copartnerDrawOrderLog");
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "drawOrderLogDatagrid")
	public void drawOrderLogDatagrid(CopartnerWithdrawEntity copartnerWithdraw,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user =  ResourceUtil.getSessionUserName();
		String userType  = user.getType();
		
		//CopartnerWithdrawEntity copartnerWithdrawdb = systemService.findUniqueByProperty(CopartnerWithdrawEntity.class, "id", copartnerWithdraw.getId());
		
		CopartnerOrderEntity copartnerOrder = new CopartnerOrderEntity();
		CriteriaQuery cq = new CriteriaQuery(CopartnerOrderEntity.class, dataGrid);
		if("6".equals(userType)){//如果是第三方发货商非平台方注册的供应商
			cq.eq("usercopartnerid", user);
		}
		cq.eq("withdraworderid.id", copartnerWithdraw.getId());
		cq.addOrder("createtime", SortDirection.desc);
		cq.add();
		
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, copartnerOrder, request.getParameterMap());
		copartnerWithdrawOrderService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
		
	}
	
	/**
	 * 合作伙伴相关订单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView copartnerWithdrawOrder(HttpServletRequest request) {
		TSUser user =  ResourceUtil.getSessionUserName();
		String status = ResourceUtil.getParameter("status");
		request.setAttribute("status", status);
		if("6".equals(user.getType())){
			return new ModelAndView("admin/copartner/copartnerWithdrawList");
		}
		return new ModelAndView("admin/copartner/copartnerWithdrawAuditList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(CopartnerWithdrawEntity copartnerWithdrawOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user =  ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(CopartnerWithdrawEntity.class, dataGrid);
		if("6".equals(user.getType())){
			cq.eq("usercopartnerid", user);
		}else{
			cq.eq("companyid", user.getCompany());
		}
		cq.eq("status", copartnerWithdrawOrder.getStatus());
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, copartnerWithdrawOrder, request.getParameterMap());
		this.copartnerWithdrawOrderService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除合作伙伴相关订单
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(CopartnerWithdrawEntity copartnerWithdrawOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		copartnerWithdrawOrder = systemService.getEntity(CopartnerWithdrawEntity.class, copartnerWithdrawOrder.getId());
		message = "合作伙伴相关订单删除成功";
		copartnerWithdrawOrderService.delete(copartnerWithdrawOrder);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 申请结算存条
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(CopartnerWithdrawEntity copartnerWithdrawOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(copartnerWithdrawOrder.getId())) {
			copartnerWithdrawOrder.setCompanyid(null);
			copartnerWithdrawOrder.setUsercopartnerid(null);
			copartnerWithdrawOrder.setStatus("2");
			message = "申请结算成功，请等待站方审核";
			CopartnerWithdrawEntity t = copartnerWithdrawOrderService.get(CopartnerWithdrawEntity.class, copartnerWithdrawOrder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(copartnerWithdrawOrder, t);
				copartnerWithdrawOrderService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "申请结算失败";
				j.setSuccess(false);
			}
		} else {
			BigDecimal withdrawMoney = copartnerWithdrawOrder.getWaittodrawmoney();
			if(withdrawMoney.compareTo(new BigDecimal(0.00)) > 0 ){
				//只能是合作方提供生成申请单
				TSUser user = ResourceUtil.getSessionUserName();
				message = "申请结算成功，请等待站方审核";
				String id = IdWorker.generateSequenceNo();
				copartnerWithdrawOrder.setId( id );
				copartnerWithdrawOrder.setUsercopartnerid(user);
				copartnerWithdrawOrder.setCompanyid(user.getCompany());
				copartnerWithdrawOrder.setStatus("2");
				copartnerWithdrawOrderService.save(copartnerWithdrawOrder);
				
				try{
					copartnerOrderService.updateCopartnerOrder2withdraw(user, id);
					
					//结算的时候冻结账户提取的款项
					CriteriaQuery cq = new CriteriaQuery(UserCopartnerAccountEntity.class);
					cq.eq("usercopartnerid", user);
					cq.add();
					List<UserCopartnerAccountEntity> userCopartnerAccountList = systemService.getListByCriteriaQuery(cq, false);
					if(null != userCopartnerAccountList && userCopartnerAccountList.size() > 0){
						
						UserCopartnerAccountEntity userCopartnerAccountdb = userCopartnerAccountList.get(0);
						BigDecimal useablemoney = userCopartnerAccountdb.getUsablemoney();
						BigDecimal frozenmoney = userCopartnerAccountdb.getFrozenmoney();
						userCopartnerAccountdb.setFrozenmoney(frozenmoney.add(useablemoney));
						userCopartnerAccountdb.setUsablemoney(new BigDecimal(0.00));
						
						systemService.batchUpdate(userCopartnerAccountList);
						
					}
					
					
				}catch(Exception e){
					logger.error(">>>>>>>> updateCopartnerOrderDraw : " + e);
					j.setSuccess(false);
				}
			}else{
				message = "申请结算失败,没有可提现金额";
				j.setSuccess(false);
			}
			
			
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 申请结算存条
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "saveAudit")
	@ResponseBody
	public AjaxJson saveAudit(CopartnerWithdrawEntity copartnerWithdrawOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		if (StringUtil.isNotEmpty(copartnerWithdrawOrder.getId())) {
			copartnerWithdrawOrder.setCompanyid(null);
			copartnerWithdrawOrder.setUsercopartnerid(null);
			copartnerWithdrawOrder.setStatus("3");//状态由审核进入了结
			copartnerWithdrawOrder.setUpdatetime(DateUtils.getDate());
			message = "结算审核通过";
			CopartnerWithdrawEntity t = copartnerWithdrawOrderService.get(CopartnerWithdrawEntity.class, copartnerWithdrawOrder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(copartnerWithdrawOrder, t);
				copartnerWithdrawOrderService.saveOrUpdate(t);
				
				CriteriaQuery cq = new CriteriaQuery(UserCopartnerAccountEntity.class);
				cq.eq("usercopartnerid", t.getUsercopartnerid());
				cq.add();
				List<UserCopartnerAccountEntity> userCopartnerAccountList = systemService.getListByCriteriaQuery(cq, false);
				if(null != userCopartnerAccountList && userCopartnerAccountList.size() > 0){
					
					UserCopartnerAccountEntity  userCopartnerAccount = userCopartnerAccountList.get(0);
					BigDecimal total = userCopartnerAccount.getTotalmoney();
//					BigDecimal wanttoWithdraw = copartnerWithdrawOrder.getWaittodrawmoney();
					BigDecimal actualWithdraw = copartnerWithdrawOrder.getActualdrawmoney();
					BigDecimal frozenMoney = userCopartnerAccount.getFrozenmoney();
					userCopartnerAccount.setTotalmoney(total.subtract(actualWithdraw));
					userCopartnerAccount.setFrozenmoney(frozenMoney.subtract(actualWithdraw));
					userCopartnerAccount.setUsablemoney(new BigDecimal(0.00));
					
					systemService.updateEntitie(userCopartnerAccount);
				}				
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "结算审核失败";
			}
		} else {
			message = "结算审核通过";
			copartnerWithdrawOrderService.save(copartnerWithdrawOrder);
			copartnerWithdrawOrder.setCompanyid(user.getCompany());
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 申请结算表单
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(CopartnerWithdrawEntity copartnerWithdrawOrder, HttpServletRequest req) {
		TSUser userCopartner = ResourceUtil.getSessionUserName();
		CopartnerWithdrawEntity copartnerWithdraw = null;
		if (StringUtil.isNotEmpty(copartnerWithdrawOrder.getId())) {
			copartnerWithdraw = copartnerWithdrawOrderService.getEntity(CopartnerWithdrawEntity.class, copartnerWithdrawOrder.getId());
			
		}
		if(null == copartnerWithdraw){
			copartnerWithdraw = new CopartnerWithdrawEntity();
			copartnerWithdraw.setWaittodrawmoney(copartnerWithdrawOrder.getWaittodrawmoney());
			copartnerWithdraw.setUsercopartnerid(userCopartner);
			copartnerWithdraw.setCompanyid(userCopartner.getCompany());
		}
		req.setAttribute("copartnerWithdrawPage", copartnerWithdraw);
//		if("6".equals(userCopartner.getType())){
//			return new ModelAndView("admin/copartner/copartnerWithdraw");
//		}
		return new ModelAndView("admin/copartner/copartnerWithdrawAudit");
	}
	
	
	/**
	 * 申请结算表单
	 * @return
	 */
	@RequestMapping(value = "adddraworder")
	public ModelAndView addWithdrawOrder(UserCopartnerAccountEntity userCopartnerAccount, HttpServletRequest req) {
		TSUser userCopartner = ResourceUtil.getSessionUserName();
		UserCopartnerAccountEntity userCopartnerAccountdb = systemService.findUniqueByProperty(UserCopartnerAccountEntity.class, "id", userCopartnerAccount.getId());
		UserCopartnerEntity copartner = systemService.findUniqueByProperty(UserCopartnerEntity.class, "id", userCopartner.getTsuserCopartner().getId());
		//生成结算申请单
		CopartnerWithdrawEntity copartnerWithdraw = new CopartnerWithdrawEntity();
		copartnerWithdraw.setWaittodrawmoney(userCopartnerAccountdb.getUsablemoney());
		copartnerWithdraw.setUsercopartnerid(userCopartner);
		copartnerWithdraw.setCompanyid(userCopartner.getCompany());
		copartnerWithdraw.setCopartneraccount(copartner.getPartneraccount());
		copartnerWithdraw.setCopartneraccountname(copartner.getPartneraccountname());
		
		req.setAttribute("copartnerWithdrawPage", copartnerWithdraw);
		return new ModelAndView("admin/copartner/copartnerWithdrawOrder");
	}
	
	/**
	 * 显示凭据
	 * @param userCopartnerAccount
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "evidence")
	public ModelAndView evidenceShow(HttpServletRequest req) {
		String imgUrl = req.getParameter("url");
		
		req.setAttribute("evidenceUrl", imgUrl);
		return new ModelAndView("admin/copartner/evidence");
	}
	
	@RequestMapping(value= "statisWithdrawOrder")
	@ResponseBody
	public AjaxJson statisWithdrawOrders(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计发货结算审核失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = copartnerWithdrawOrderService.statisWithdrawOrder(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计退单成功！");
			
		}
		
		return json;
	}
}
