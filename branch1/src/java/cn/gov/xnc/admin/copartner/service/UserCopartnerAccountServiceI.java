package cn.gov.xnc.admin.copartner.service;

import java.util.List;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerAccountEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface UserCopartnerAccountServiceI extends CommonService{

	/**
	 * 结算计算器，发货商每次发货系统都会把发货单成本价进行汇总计算，放入发货方的帐号中待结算使用
	 * @param copartnerOrderList
	 * @param company
	 * @throws Exception
	 */
	public void withdrawCaculater(List<CopartnerOrderEntity> copartnerOrderList, TSUser userCopartner) throws Exception;
	
	/**
	 * @Description: 根据用户创建提现账户信息
	 */
	public void buildUserCopartnerAccount(TSUser userCopartner) throws Exception;
	
	/**
	 * @Description: 获取所有合作用户的帐号
	 * @return
	 * @throws Exception
	 */
	public List<UserCopartnerAccountEntity> getAllUserCopartnerAccount() throws Exception;
	
	/**
	 * @Description: 根据合作伙伴用户和时间，获取合作伙伴上个月订单的提现总额
	 * @param userCopartner
	 * @param endDateTime
	 * @return
	 */
	public Long getLastMonthDrawByUser(TSUser userCopartner, String endDateTime);
	
	/**
	 * @Description: 对每个账户进行月结核算，提供给自动任务调用，避免自动任务调用dao层，无法获取session问题
	 * @throws Exception
	 */
	public void jobMonthWithDraw() throws Exception;
	
}
