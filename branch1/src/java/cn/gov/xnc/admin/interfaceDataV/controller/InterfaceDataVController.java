package cn.gov.xnc.admin.interfaceDataV.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gov.xnc.admin.interfaceDataV.service.impl.InterfaceDataVServiceImpl;
import cn.gov.xnc.system.core.util.ParamUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Controller
public class InterfaceDataVController {
	private static Calendar c = Calendar.getInstance();
	@Resource(name="interfaceDataVService")
	private InterfaceDataVServiceImpl interfaceDataVService;
	
	/**
	 * 查询订单状态的订单量：1待付、2已付、3未付、4,5已取消
	 * */
	@RequestMapping("/interfaceDataVController/getPayBillsOrderByState")
	@ResponseBody
	public Object getPayBillsOrderByState(HttpServletRequest request){
		Integer m1 = ParamUtil.getIntParameter(request, "begin", 1);
		Integer m2 = ParamUtil.getIntParameter(request, "end", 12);
		Integer s = ParamUtil.getIntParameter(request, "s", 1);
		if(m1<1 || m1>12){
			m1 = 1;
		}
		if(m2<1 || m2>12){
			m2 = 1;
		}
		return interfaceDataVService.getPayBillsOrderByState(m1, m2, s);
	}
	/**
	 * 显示商品订单概况，查询特定状态下订单量的总合：2待发货、3已发货、4已签收、5,6已取消
	 * */
	@RequestMapping("/interfaceDataVController/getOrderCount")
	@ResponseBody
	public Object getOrderCount(HttpServletRequest request){
		Integer m1 = ParamUtil.getIntParameter(request, "begin", 1);
		Integer m2 = ParamUtil.getIntParameter(request, "end", 12);
		Integer cases = ParamUtil.getIntParameter(request, "state", 0);
		if(m1<1 || m1>12){
			m1 = 1;
		}
		if(m2<1 || m2>12){
			m2 = 1;
		}
		return interfaceDataVService.getOrderCount(m1, m2, cases);
	}
	
	/**
	* 获取派单相关数据
	*/
	@RequestMapping("/interfaceDataVController/getCopartnerList")
	@ResponseBody
	public Object getCopartnerList(HttpServletRequest request){
		Integer m1 = ParamUtil.getIntParameter(request, "begin", 1);
		Integer m2 = ParamUtil.getIntParameter(request, "end", 12);
		Integer showNum = ParamUtil.getIntParameter(request, "showNum", 20);
		if(showNum<0 || showNum>20){
			showNum = 10;
		}
		if(m1<1 || m1>12){
			m1 = 1;
		}
		if(m2<1 || m2>12){
			m2 = 1;
		}
		return interfaceDataVService.getCopartnerList(m1, m2, showNum);
	}
	/**
	 * 热销省份统计
	 * */
	@RequestMapping("/interfaceDataVController/getAreaSalesCount")
	@ResponseBody
	public Object getAreaSalesCount(HttpServletRequest request){
		Integer m1 = ParamUtil.getIntParameter(request, "begin", 1);
		Integer m2 = ParamUtil.getIntParameter(request, "end", 12);
		Integer showNum = ParamUtil.getIntParameter(request, "showNum", 5);
		if(m1<1 || m1>12){
			m1 = 1;
		}
		if(m2<1 || m2>12){
			m2 = 1;
		}
		return interfaceDataVService.getAreaSalesCount(m1, m2, showNum);
	}
	
	
	/**
	 * 产品销售统计
	 * @return
	 */
	@RequestMapping("/interfaceDataVController/getSalesCount")
	@ResponseBody
	public Object getSalesCount(HttpServletRequest request){
		Integer m1 = ParamUtil.getIntParameter(request, "begin", 1);
		Integer m2 = ParamUtil.getIntParameter(request, "end", 12);
		Integer showNum = ParamUtil.getIntParameter(request, "showNum", 20);
		if(showNum<0 || showNum>20){
			showNum = 10;
		}
		if(m1<1 || m1>12){
			m1 = 1;
		}
		if(m2<1 || m2>12){
			m2 = 1;
		}
		return interfaceDataVService.getSalesCount(m1, m2, showNum);
	}
	
	/**
	* 获取仓库相关数据
	*/
	@RequestMapping("/interfaceDataVController/getStockList")
	@ResponseBody
	public Object getStockList(HttpServletRequest request) {
		Integer showNum = ParamUtil.getIntParameter(request, "showNum", 10);
		if(showNum<0 || showNum>50){
			showNum = 10;
		}
		
		return interfaceDataVService.getStockList(showNum);
	}
	
	
	@RequestMapping("/interfaceDataVController/getCompany")
	@ResponseBody
	public Object getCompany(){
		List<Map<String, Object>> maplist = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String, Object>();
		//TSUser user = ResourceUtil.getSessionUserName();
		//map.put("value",  c.get(Calendar.YEAR)+"年 "+user.getCompany().getCompanyName());
		map.put("value",  c.get(Calendar.YEAR)+"年");
		maplist.add(map);
		return maplist;
	}
	/**
	 * 采购商数据统计
	 * @return
	 * */
	@RequestMapping("/interfaceDataVController/getQdxs")
	@ResponseBody
	public Object getqdxs(){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("showNum", 10);
		return interfaceDataVService.qdxs(map);
	}
	/**
	 * 地区销售情况
	 * @return
	 * */
	@RequestMapping("/interfaceDataVController/getYh")
	@ResponseBody
	public Object getyh(){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("showNum", 10);
		return interfaceDataVService.yh(map);
	}
	/**
	 * 发货区域统计
	 * @return
	 * */
	@RequestMapping("/interfaceDataVController/getInvoice")
	@ResponseBody
	public Object getinvoice(){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("showNum", 10);
		return interfaceDataVService.invoice(map);
	}
	/**
	 *  款项明细
	 * @return
	 */
	@RequestMapping("/interfaceDataVController/getPaymentDetails")
	@ResponseBody
	public Object getPaymentDetails(HttpServletRequest request){
		Integer m1 = ParamUtil.getIntParameter(request, "begin", 0);
		Integer m2 = ParamUtil.getIntParameter(request, "end", 0);
		String state = ParamUtil.getParameter(request, "state", "");
		if(m1<1 || m1>12){
			m1 = 0;
		}
		if(m2<1 || m2>12){
			m2 = 0;
		}
		if(state.equals("2")){
			
		}else if(state.equals("1,2,3")){
			
		}else if(state.equals("1,3")){
			
		}else{
			state = "2";
		}
		return interfaceDataVService.getPaymentDetails(state, m1, m2);
	}
	/**
	 *  款项明细
	 * @return
	 */
	@RequestMapping("/interfaceDataVController/getPreDeposit")
	@ResponseBody
	public Object getPreDeposit(){
		return interfaceDataVService.getPreDeposit();
	}
}
