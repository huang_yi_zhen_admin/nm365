package cn.gov.xnc.admin.kuaidi.service.impl;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.zip.GZIPInputStream;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.kuaidi.entity.Kuaid100Entity;
import cn.gov.xnc.admin.kuaidi.service.Kuaid100ServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.JSONHelper;

import cn.gov.xnc.system.core.util.StringUtil;



@Service("kuaidi100Service")

public class Kuaidi100ServiceImpl extends CommonServiceImpl implements Kuaid100ServiceI {
		
	
	

	/**
	 * 快递单号查询
	 * @param logisticscompany 快递公司
	 * @param logisticsnumber  快递单号，多单号使用,隔开
	 * @return
	 * @throws Exception
	 */
	public AjaxJson getkuaidi100(  String logisticscompany    ,String logisticsnumberS ){
		AjaxJson j = new AjaxJson();
		//
		String type ="";
		
		
		Map<String, Object> attributes = new HashMap<String, Object>();
		
		if( StringUtil.isNotEmpty(logisticsnumberS) && StringUtil.isNotEmpty(logisticscompany)){
			Kuaid100Entity kuaid100 = new Kuaid100Entity();
			String logisticsnumber[] = logisticsnumberS.trim().split(",");
			
			type = (String) kuaid100.kuaidi100ToKeyMap().get(logisticscompany.trim().substring(0,2));
			
			for( int i = 0 ; i <logisticsnumber.length ;i++ ){
				if(StringUtil.isNotEmpty(logisticsnumber[i].trim() )&& StringUtil.isNotEmpty(type)){
					String postid =logisticsnumber[i].trim();
					String url = "http://www.kuaidi100.com/query?type="+type+"&postid="+postid+"&id=1&valicode=&temp=0.3015635129995644";
			        try {
			            HttpURLConnection.setFollowRedirects(true);    
			            HttpURLConnection http = (HttpURLConnection) (new URL(url).openConnection());    
			            http.setDoOutput(true);
			            http.setDoOutput(true);
			            http.setInstanceFollowRedirects(true);    
			            http.setRequestMethod("GET");    
			            http.setRequestProperty("Connection", "keep-alive");    
			            http.setRequestProperty("X-Requested-With", "XMLHttpRequest");    
			            http.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.92 Safari/537.1 LBBROWSER");    
			            http.setRequestProperty("Accept", "*/*");    
			            http.setRequestProperty("Referer", "http://www.kuaidi100.com/");    
			            http.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8");    
			            http.setRequestProperty("Accept-Charset", "GBK,utf-8;q=0.7,*;q=0.3");    
			            http.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");    
			                
			            
			            String contentEncoding = http.getContentEncoding();     
			            InputStream  in = null;    
			            if("gzip".equalsIgnoreCase(contentEncoding)){    
			                in = new GZIPInputStream(http.getInputStream());     
			            }else{    
			                in = http.getInputStream();    
			            }    
			            ByteArrayOutputStream baos = new ByteArrayOutputStream();    
			            int data = -1;    
			            while((data = in.read())!=-1){    
			                baos.write(data);    
			            }    
			            String resp = baos.toString("utf8");    
			            
			            ObjectMapper objectMapper = new ObjectMapper();  
			            objectMapper.readValue(resp, Map.class);
			            
			            attributes.put(logisticscompany+postid, resp);
			            System.out.println(resp);    
			        } catch (MalformedURLException e) {
			        	attributes.put(logisticscompany+postid, "查询异常！");   
			        } catch (IOException e) {    
			        	attributes.put(logisticscompany+postid, "查询异常！");      
			        }    
				}
				
			}
		}
		
		j.setAttributes(attributes);

		return j;
		
		
		
		
	}
	
	
	
//	/**
//	 * 快递单号查询
//	 * @param logisticscompany 快递公司
//	 * @param logisticsnumber  快递单号，多单号使用,隔开
//	 * @return
//	 * @throws Exception
//	 */
//	public List getNkuaidi100(  String logisticscompany    ,String logisticsnumberS ){
//		
//		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
//		//
//		String type ="";
//		
//		
//		Map<String, Object> attributes = new HashMap<String, Object>();
//		
//		if( StringUtil.isNotEmpty(logisticsnumberS) && StringUtil.isNotEmpty(logisticscompany)){
//			Kuaid100Entity kuaid100 = new Kuaid100Entity();
//			String logisticsnumber[] = logisticsnumberS.trim().split(",");
//			
//			type = kuaid100.kuaidi100ToKeyMap().get(logisticscompany.trim().substring(0,2));
//			
//			for( int i = 0 ; i <logisticsnumber.length ;i++ ){
//				Map<String, Object> map = new HashMap<String, Object>();
//				
//				if(StringUtil.isNotEmpty(logisticsnumber[i].trim() )&& StringUtil.isNotEmpty(type)){
//					String postid =logisticsnumber[i].trim();
//					String url = "http://www.kuaidi100.com/query?type="+type+"&postid="+postid+"&id=1&valicode=&temp=0.3015635129995644";
//			        try {
//			            HttpURLConnection.setFollowRedirects(true);    
//			            HttpURLConnection http = (HttpURLConnection) (new URL(url).openConnection());    
//			            http.setDoOutput(true);
//			            http.setDoOutput(true);
//			            http.setInstanceFollowRedirects(true);    
//			            http.setRequestMethod("GET");    
//			            http.setRequestProperty("Connection", "keep-alive");    
//			            http.setRequestProperty("X-Requested-With", "XMLHttpRequest");    
//			            http.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.92 Safari/537.1 LBBROWSER");    
//			            http.setRequestProperty("Accept", "*/*");    
//			            http.setRequestProperty("Referer", "http://www.kuaidi100.com/");    
//			            http.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8");    
//			            http.setRequestProperty("Accept-Charset", "GBK,utf-8;q=0.7,*;q=0.3");    
//			            http.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");    
//			                
//			            
//			            String contentEncoding = http.getContentEncoding();     
//			            InputStream  in = null;    
//			            if("gzip".equalsIgnoreCase(contentEncoding)){    
//			                in = new GZIPInputStream(http.getInputStream());     
//			            }else{    
//			                in = http.getInputStream();    
//			            }    
//			            ByteArrayOutputStream baos = new ByteArrayOutputStream();    
//			            int data = -1;    
//			            while((data = in.read())!=-1){    
//			                baos.write(data);    
//			            }    
//			            String resp = baos.toString("utf8");    
//			            
//			            ObjectMapper objectMapper = new ObjectMapper();  
//			            map = objectMapper.readValue(resp, Map.class);
//			            map.put("name", logisticscompany+postid);
//			            list.add(map);
//			            
//			        } catch (MalformedURLException e) {
//			        	attributes.put(logisticscompany+postid, "查询异常！");   
//			        } catch (IOException e) {    
//			        	attributes.put(logisticscompany+postid, "查询异常！");      
//			        }    
//				}
//				
//			}
//		}
//
//		return list;
//		
//	}

	/**
	 * 快递单号查询
	 * @param 订单id orderId
	 * @param 物流信息  是否马上更新
	 * @return
	 * @throws Exception
	 */
	@Override
	public void  upkuaidi(  String orderId    ,  Map kuaidi ,boolean up ){
		
		if (StringUtil.isNotEmpty(orderId)) {
			OrderEntity order = getEntity(OrderEntity.class, orderId);
			List<OrderLogisticsEntity> orderLogisticsList = null ;
			try {
				orderLogisticsList = order.getOrderLogisticsList();
				order.setOrderLogisticsList(null);
			} catch (Exception e) {
				
			}
			//如果存在多订单号
			List<OrderLogisticsEntity> addList = new ArrayList<OrderLogisticsEntity>() ;
			if( orderLogisticsList != null && orderLogisticsList.size() > 0){
				for ( OrderLogisticsEntity orls :  orderLogisticsList  ) {
					//OrderLogisticsEntity orls = get(OrderLogisticsEntity.class, o.getId());
					if( !"3".equals(orls.getState()) && !"4".equals(orls.getState()) ){
						String resp = (String) kuaidi.get(orls.getLogisticscompany()+orls.getLogisticsnumber()+"");
						//获取对应物流信息
						resp = resp.replaceAll(":null", ":\"\"");
						System.out.println(resp);
						
						Kuaid100Entity kuaid100 = (Kuaid100Entity) JSONHelper.json2Object(resp, Kuaid100Entity.class);
						
						if( kuaid100 == null){
							orls.setState("5");
						}else if( "ok".equals(kuaid100.getMessage()) ){
							orls.setMessage(kuaid100.getMessage());
							orls.setIscheck(kuaid100.getIscheck());
							orls.setCom(kuaid100.getCom());
							orls.setState(kuaid100.getState());
							orls.setStatus(kuaid100.getStatus());
							//orls.setCondition(kuaid100.getCondition());
						}
						orls.setLogisticsdate(DateUtils.getDate());
						
						//saveOrUpdate(orls);
						
						addList.add(orls);
					}
				}
			}
			
			if( addList != null && addList.size() > 0){
				batchUpdate(addList);
			}
	}
  }
	
}