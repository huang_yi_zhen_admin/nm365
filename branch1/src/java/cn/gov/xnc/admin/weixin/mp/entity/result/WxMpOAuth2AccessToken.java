package cn.gov.xnc.admin.weixin.mp.entity.result;

import cn.gov.xnc.admin.weixin.mp.utils.josn.WxMpGsonBuilder;

public class WxMpOAuth2AccessToken {
	
	
	  /**网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同*/
	  private String accessToken;
	  /**access_token接口调用凭证超时时间，单位（秒）*/
	  private int expiresIn = -1;
	  /**用户刷新access_token*/
	  private String refreshToken;
	  /**用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID*/
	  private String openId;
	  /**用户授权的作用域，使用逗号（,）分隔*/
	  private String scope;
	  /**只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段*/
	  private String unionId;
	

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the expiresIn
	 */
	public int getExpiresIn() {
		return expiresIn;
	}

	/**
	 * @param expiresIn the expiresIn to set
	 */
	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	/**
	 * @return the refreshToken
	 */
	public String getRefreshToken() {
		return refreshToken;
	}

	/**
	 * @param refreshToken the refreshToken to set
	 */
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	/**
	 * @return the openId
	 */
	public String getOpenId() {
		return openId;
	}

	/**
	 * @param openId the openId to set
	 */
	public void setOpenId(String openId) {
		this.openId = openId;
	}

	/**
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * @param scope the scope to set
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * @return the unionId
	 */
	public String getUnionId() {
		return unionId;
	}

	/**
	 * @param unionId the unionId to set
	 */
	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

	public static WxMpOAuth2AccessToken fromJson(String json) {
		    return WxMpGsonBuilder.create().fromJson(json, WxMpOAuth2AccessToken.class);
	}

	@Override
	public String toString() {
		    return "WxMpOAuth2AccessToken{" +
		    		"accessToken='" + this.accessToken + '\'' +
		    		", expiresTime=" + this.expiresIn +
		    		", refreshToken='" + this.refreshToken + '\'' +
		    		", openId='" + this.openId + '\'' +
		    		", scope='" + this.scope + '\'' +
		    		", unionId='" + this.unionId + '\'' +
		    		'}';
		  }
	
}
