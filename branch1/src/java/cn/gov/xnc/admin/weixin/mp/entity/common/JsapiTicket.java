package cn.gov.xnc.admin.weixin.mp.entity.common;


import cn.gov.xnc.admin.weixin.mp.utils.josn.WxMpGsonBuilder;

public class JsapiTicket {
	
	private String  errcode;
	private String errmsg;
	private String ticket;
	private int  expiresIn;
	/**
	 * @return the errcode
	 */
	public String getErrcode() {
		return errcode;
	}
	/**
	 * @param errcode the errcode to set
	 */
	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}
	/**
	 * @return the errmsg
	 */
	public String getErrmsg() {
		return errmsg;
	}
	/**
	 * @param errmsg the errmsg to set
	 */
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}
	/**
	 * @param ticket the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	/**
	 * @return the expiresIn
	 */
	public int getExpiresIn() {
		return expiresIn;
	}
	/**
	 * @param expiresIn the expiresIn to set
	 */
	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}
	
	
	public static JsapiTicket fromJson(String json) {
	    return WxMpGsonBuilder.create().fromJson(json, JsapiTicket.class);
}
	
}
