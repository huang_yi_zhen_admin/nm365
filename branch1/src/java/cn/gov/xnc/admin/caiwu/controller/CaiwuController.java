package cn.gov.xnc.admin.caiwu.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.salesman.controller.SalesmanAuditController;
import cn.gov.xnc.admin.salesman.service.SalesmanAuditServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.web.system.service.SystemService;


/**   
 * @Title: Controller
 * @Description: 业务结算流水
 * @author zero
 * @date 2016-11-27 19:14:04
 * @version V1.0
 */
@Scope("prototype")
@Controller
@RequestMapping("/caiwuController")

public class CaiwuController extends BaseController {
	
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SalesmanAuditController.class);

	@Autowired
	private SalesmanAuditServiceI salesmanAuditService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 业务结算流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "caiwufundList")
	public ModelAndView caiwufundList(HttpServletRequest request) {
		return new ModelAndView("admin/caiwu/caiwufundList");
	}

}
