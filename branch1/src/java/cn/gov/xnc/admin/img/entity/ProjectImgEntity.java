package cn.gov.xnc.admin.img.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 图片存储基础信息
 * @author huangyz
 * @date 2016-03-14 23:29:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_sa_ProjectImg", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ProjectImgEntity implements java.io.Serializable {
	/**记录ID*/
	private java.lang.String id;
	/**表ID*/
	private java.lang.String tableid;
	/**类型*/
	private java.lang.String type;
	/**原图地址*/
	private java.lang.String imgurl;
	/**中图地址*/
	private java.lang.String midimgurl;
	/**小图地址*/
	private java.lang.String smallimgurl;
	/**小图地址*/
	private java.lang.String showpicurl;
	/**图片标示*/
	private java.lang.String imgid;
	/**状态 1 显示 2关闭*/
	private java.lang.Integer status;
	/**createdate*/
	private java.util.Date createdate;
	/**updatedate*/
	private java.util.Date updatedate;
	/**createuser*/
	private java.lang.String createuser;
	/**updateuser*/
	private java.lang.String updateuser;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  记录ID
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  记录ID
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  表ID
	 */
	@Column(name ="TABLEID",nullable=true,length=32)
	public java.lang.String getTableid(){
		return this.tableid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  表ID
	 */
	public void setTableid(java.lang.String tableid){
		this.tableid = tableid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  类型
	 */
	@Column(name ="TYPE",nullable=true,length=2)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  类型
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  原图地址
	 */
	@Column(name ="IMGURL",nullable=true,length=500)
	public java.lang.String getImgurl(){
		return this.imgurl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  原图地址
	 */
	public void setImgurl(java.lang.String imgurl){
		this.imgurl = imgurl;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  小图地址
	 */
	@Column(name ="SHOWPICURL",nullable=true,length=500)
	public java.lang.String getShowpicurl(){
		return this.showpicurl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  小图地址
	 */
	public void setShowpicurl(java.lang.String showpicurl){
		this.showpicurl = showpicurl;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  imgid
	 */
	@Column(name ="IMGID",nullable=true,length=2)
	public java.lang.String getImgid(){
		return this.imgid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  imgid
	 */
	public void setImgid(java.lang.String imgid){
		this.imgid = imgid;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  状态 1 显示 2关闭
	 */
	@Column(name ="STATUS",nullable=true,precision=10,scale=0)
	public java.lang.Integer getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  状态 1 显示 2关闭
	 */
	public void setStatus(java.lang.Integer status){
		this.status = status;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  createdate
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  createdate
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  updatedate
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  updatedate
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  createuser
	 */
	@Column(name ="CREATEUSER",nullable=true,length=32)
	public java.lang.String getCreateuser(){
		return this.createuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  createuser
	 */
	public void setCreateuser(java.lang.String createuser){
		this.createuser = createuser;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  updateuser
	 */
	@Column(name ="UPDATEUSER",nullable=true,length=32)
	public java.lang.String getUpdateuser(){
		return this.updateuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  updateuser
	 */
	public void setUpdateuser(java.lang.String updateuser){
		this.updateuser = updateuser;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  小图地址
	 */
	@Column(name ="SMALLIMGURL",nullable=true,length=500)
	public java.lang.String getSmallimgurl() {
		return smallimgurl;
	}

	public void setSmallimgurl(java.lang.String smallimgurl) {
		this.smallimgurl = smallimgurl;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  中图地址
	 */
	@Column(name ="MIDIMGURL",nullable=true,length=500)
	public java.lang.String getMidimgurl() {
		return midimgurl;
	}

	public void setMidimgurl(java.lang.String midimgurl) {
		this.midimgurl = midimgurl;
	}

}
