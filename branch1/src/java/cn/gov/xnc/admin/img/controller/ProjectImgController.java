package cn.gov.xnc.admin.img.controller;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;



import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.common.UploadFile;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;

import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.Client;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.img.entity.ProjectImgEntity;
import cn.gov.xnc.admin.img.service.ProjectImgServiceI;

/**   
 * @Title: Controller
 * @Description: 图片存储基础信息
 * @author huangyz
 * @date 2016-03-14 23:29:54
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/projectImgController")
public class ProjectImgController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProjectImgController.class);

	@Autowired
	private ProjectImgServiceI projectImgService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 图片存储基础信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView projectImg(HttpServletRequest request) {
		return new ModelAndView("admin/img/projectImgList");
	}
	
	
	/**
	 * 上传图片 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "ImgUpload")
	public ModelAndView ImgUpload(HttpServletRequest request) {
		return new ModelAndView("admin/img/ImgUpload");
	}
	
	
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ProjectImgEntity projectImg,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ProjectImgEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, projectImg, request.getParameterMap());
		this.projectImgService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除图片存储基础信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ProjectImgEntity projectImg, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		projectImg = systemService.getEntity(ProjectImgEntity.class, projectImg.getId());
		message = "图片存储基础信息删除成功";
		projectImgService.delete(projectImg);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加图片存储基础信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ProjectImgEntity projectImg, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(projectImg.getId())) {
			message = "图片存储基础信息更新成功";
			ProjectImgEntity t = projectImgService.get(ProjectImgEntity.class, projectImg.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(projectImg, t);
				projectImgService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "图片存储基础信息更新失败";
			}
		} else {
			message = "图片存储基础信息添加成功";
			projectImgService.save(projectImg);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 图片存储基础信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ProjectImgEntity projectImg, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(projectImg.getId())) {
			projectImg = projectImgService.getEntity(ProjectImgEntity.class, projectImg.getId());
			req.setAttribute("projectImgPage", projectImg);
		}
		return new ModelAndView("common/projectImg");
	}
	
	
	//图片跳转上次页面公共页
	
	
	/**
	 * 图片存储基础信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdateJsp")
	public ModelAndView addorupdateJsp(ProjectImgEntity projectImg, HttpServletRequest req) {
		
		return new ModelAndView("admin/img/addorupdateJsp");
	}
	
	
	
	/**
	 * 上传图标
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "saveOrUpdateImg", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson saveOrUpdateImg(HttpServletRequest request, HttpServletResponse response ) throws Exception {
		
		//获取用户id创建公司对应存储信息
		TSUser user = null;
		try {
			user = ResourceUtil.getSessionUserName();
			if(user == null ){//第二种获取方式
				user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
				user = systemService.getEntity(TSUser.class, user.getId());
			}
		} catch (Exception e) {
			
		}
		
		
		
		AjaxJson j = new AjaxJson();
		ProjectImgEntity projectImg = new ProjectImgEntity();
//		Short iconType = oConvertUtils.getShort(request.getParameter("iconType"));
//		String iconName = oConvertUtils.getString(request.getParameter("iconName"));
		String id = request.getParameter("id");
		String imgId = request.getParameter("imgid");//图片标示
		if(null == imgId){
			imgId = "web";
		}
		String  tableid = request.getParameter("tableid");//表ID
		String  type = request.getParameter("type");//图片类型
		
		String sessionId = request.getParameter("sessionId");
		if(StringUtil.isNotEmpty(sessionId)){
			Client client = ClientManager.getInstance().getClient(sessionId);
			if(client!=null){
				projectImg.setCreateuser(client.getUser().getUserName());
				projectImg.setUpdateuser(client.getUser().getUserName());
				projectImg.setUpdatedate(getDate());				
			}
		}
		
		projectImg.setId(id);
		projectImg.setImgid(imgId);
		projectImg.setTableid(tableid);
		projectImg.setType(type);
		projectImg.setStatus(1);
		//uploadFile.setBasePath("images/accordion");
		UploadFile uploadFile = new UploadFile(request, projectImg);
		
		uploadFile.setCusPath("project_img/"+user.getCompany().getId()+"/images/"+imgId);
		uploadFile.setExtend("extend");//
		uploadFile.setTitleField("iconclas");//
		uploadFile.setRealPath("iconPath");//
		uploadFile.setObject(projectImg);
		uploadFile.setByteField("iconContent");//
		uploadFile.setRename(true);
		
		systemService.uploadFile(uploadFile);//上传同时写入数据库
		
		String basePath = request.getScheme()+"://"+request.getServerName()+"/"; 
		
		// 图标的css样式
		//String css = "." + icon.getIconClas() + "{background:url('../images/" + icon.getIconClas() + "." + icon.getExtend() + "') no-repeat}";
		//write(request, css);
		message = "上传成功";
		j.setObj(projectImg);
		j.setMsg(message);
		//System.out.println(j.getJsonStr());
		
		return j;
	}
	
	private Date getDate(){
		 SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Date d = null;
		try {
			d = sf.parse(sf.format(Calendar.getInstance().getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		 return d;
	}
	
}
