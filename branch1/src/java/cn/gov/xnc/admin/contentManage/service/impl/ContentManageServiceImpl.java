package cn.gov.xnc.admin.contentManage.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.contentManage.dao.ContentManageDAO;
import cn.gov.xnc.admin.contentManage.entity.ArticleEntity;
import cn.gov.xnc.admin.contentManage.entity.SectionEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;


@Service("contentManageService")
public class ContentManageServiceImpl {
	
	@Resource(name="contentManageDAO")
	private ContentManageDAO contentManageDAO;
	//private static ContentManageDAO contentManageDAO = ContentManageDAO.getInstance();
	
	/**
	 * 栏目查询，单条数据
	 * */
	public SectionEntity getSectionSingleData(SectionEntity section){
		return contentManageDAO.getSectionSingleData(section);
	}
	
	/**
	 * 栏目列表
	 * @return 
	 * */
	public DataGrid getSectionListPage(DataGrid dataGrid, SectionEntity section){
		dataGrid = contentManageDAO.getSectionListPage(dataGrid, section);
		return dataGrid;
	}
	/**
	 * 栏目列表所有
	 * @return 
	 * */
	public List<SectionEntity> getSectionList(DataGrid dataGrid, SectionEntity section){
		List<SectionEntity> list = contentManageDAO.getSectionList(dataGrid, section);
		return list;
	}
	/**
	 * 栏目数据更新
	 * @return 
	 * */
	public Integer sectionUpdateById(SectionEntity section){
		return contentManageDAO.sectionUpdateById(section);
	}
	/**
	 * 栏目保存
	 * @return 
	 * */
	public Serializable sectionSave(SectionEntity section){
		return contentManageDAO.sectionSave(section);
	}
	/**
	 * 文章插入
	 * @return 
	 * */
	public Serializable articleSave(ArticleEntity article){
		return contentManageDAO.articleSave(article);
	}
	/**
	 * 文章更新
	 * @return 
	 * */
	public Integer articleUpdateById(ArticleEntity article){
		return contentManageDAO.articleUpdate(article);
	}
	/**
	 * 单条文章数据
	 * @return 
	 * */
	public ArticleEntity getArticleData(ArticleEntity article){
		return contentManageDAO.getArticleData(article);
	}
	/**
	 * 文章列表
	 * @return 
	 * */
	public DataGrid getArticleListPage(DataGrid dataGrid, ArticleEntity article){
		dataGrid = contentManageDAO.getArticleListPage(dataGrid, article);
		return dataGrid;
	}
	/**
	 * 文章屏蔽
	 * @return 
	 * */
	public void articleDelById(String[] ids){
		ArticleEntity article = new ArticleEntity();
		article.setStatus((byte) 0);
		for(String id : ids){
			article.setId(id);
			contentManageDAO.articleUpdate(article);
		}
	}
	
}
