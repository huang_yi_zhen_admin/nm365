package cn.gov.xnc.admin.express.controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.express.impl.DBL_SendExpress;
import cn.gov.xnc.system.core.express.impl.EMS_SendExpress;
import cn.gov.xnc.system.core.express.impl.FAST_SendExpress;
import cn.gov.xnc.system.core.express.impl.HTKY_SendExpress;
import cn.gov.xnc.system.core.express.impl.QFKD_SendExpress;
import cn.gov.xnc.system.core.express.impl.SF_SendExpress;
import cn.gov.xnc.system.core.express.impl.STO_SendExpress;
import cn.gov.xnc.system.core.express.impl.UC_RecvExpress;
import cn.gov.xnc.system.core.express.impl.UC_SendExpress;
import cn.gov.xnc.system.core.express.impl.YD_SendExpress;
import cn.gov.xnc.system.core.express.impl.YTO_SendExpress;
import cn.gov.xnc.system.core.express.impl.ZJS_SendExpress;
import cn.gov.xnc.system.core.express.impl.ZTO_SendExpress;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.express.entity.CompanyExpressEntity;
import cn.gov.xnc.admin.express.service.CompanyExpressServiceI;

/**   
 * @Title: Controller
 * @Description: 发货地址
 * @author zero
 * @date 2017-03-14 18:04:40
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/companyExpressController")
public class CompanyExpressController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CompanyExpressController.class);

	@Autowired
	private CompanyExpressServiceI companyExpressService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}



	
	/**
	 * 客户列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "companyexpresslist")
	public String companyexpresslist( TSUser user, HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(user.getId())) {
			
			user = systemService.getEntity(TSUser.class, user.getId());
		}
		
		return "admin/express/companyExpressList";
	}
	
	/**
	 * 客户列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdateExpress")
	public String addorupdateExpress( CompanyExpressEntity companyExpressEntity, HttpServletRequest req) {
		
		TSUser sessionUser = ResourceUtil.getSessionUserName();//当前登录用户
		
		if (StringUtil.isNotEmpty(companyExpressEntity.getId())) {
			
			companyExpressEntity = systemService.getEntity(CompanyExpressEntity.class, companyExpressEntity.getId());
			
			TSTerritory top1Territory = companyExpressEntity.getTerritory();
			TSTerritory top2Territory = null;
			TSTerritory top3Territory = null;
			
			if(null != top1Territory && StringUtil.isNotEmpty(top1Territory.getId())){
				top2Territory = top1Territory.getTSTerritory();
				req.setAttribute("province", top1Territory.getId());
			}
			if(null != top2Territory && StringUtil.isNotEmpty(top2Territory.getId())){
				top3Territory = top2Territory.getTSTerritory();
				req.setAttribute("province", top2Territory.getId());
				req.setAttribute("city", top1Territory.getId());
			}
			if(null != top3Territory && StringUtil.isNotEmpty(top3Territory.getId())){
				req.setAttribute("province", top3Territory.getId());
				req.setAttribute("city", top2Territory.getId());
				req.setAttribute("area", top1Territory.getId());
			}
			
			req.setAttribute("exptypename", ExpressUtil.getExpTypeName(companyExpressEntity.getShippercode(), companyExpressEntity.getExptype()));
		}
		
		req.setAttribute("companyexpress", companyExpressEntity);
		return "admin/express/companyExpress";
	}
	
	/**
	 * 保存更新发货地址信息
	 * 
	 * @param user
	 * @param req
	 * @return
	 */

	@RequestMapping(value = "saveCompanyExpress")
	@ResponseBody
	public AjaxJson saveCompanyExpress(HttpServletRequest req, CompanyExpressEntity companyExpressEntity) {
		AjaxJson j = new AjaxJson();
		TSUser sessionUser = ResourceUtil.getSessionUserName();//当前登录用户
		
		if( "6".equals(sessionUser.getType()) ){//如果是发货商则设置发货商id
			companyExpressEntity.setShipperuserid(sessionUser.getId());
		}else{
			companyExpressEntity.setShipperuserid(null);
		}
		
		if( StringUtil.isEmpty(companyExpressEntity.getId())){//新增数据
			companyExpressEntity.setCompany(sessionUser.getCompany());
			companyExpressEntity.setId(IdWorker.generateSequenceNo());
			systemService.save(companyExpressEntity);
			//message = "客户"+sessionUser.getId()+"新增发货地址成功！";
			message = "新增";
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}else{//修改数据
			companyExpressEntity.setCompany(sessionUser.getCompany());
			systemService.updateEntitie(companyExpressEntity);
			//message = "客户"+sessionUser.getId()+"更新发货地址成功！";
			message = "更新";
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		return j;
	}
	
	/**
	 * 用户选择角色跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiseShipper")
	public String choiseShipper() {
		return "admin/express/choiseShipper";
	}
	
	@RequestMapping(value = "choiseShipperuser")
	public String choiseShipperuser() {
		return "admin/express/choiseShipperuser";
	}
	
	/**
	 * 用户选择角色跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiseExptype")
	public String choiseExptype(CompanyExpressEntity companyExpress, HttpServletRequest req) {
		req.setAttribute("companyExpress", companyExpress);
		return "admin/express/choiseExptype";
	}
	
	/**
	 * 用户选择快递类型跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "getExpTypeList")
	public void getExpTypeList(CompanyExpressEntity companyExpress, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		String shippercode = companyExpress.getShippercode();
		if( ExpressCompany.DBL.equals(shippercode)){
			dataGrid = DBL_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.EMS.equals(shippercode)){
			dataGrid = EMS_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.FAST.equals(shippercode)){
			dataGrid = FAST_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.HTKY.equals(shippercode)){
			dataGrid = HTKY_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.QFKD.equals(shippercode)){
			dataGrid = QFKD_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.SF.equals(shippercode)){
			dataGrid = SF_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.STO.equals(shippercode)){
			dataGrid = STO_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.UC.equals(shippercode)){
			dataGrid = UC_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if(ExpressCompany.YD.equals(shippercode)){
			dataGrid = YD_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.YTO.equals(shippercode)){
			dataGrid = YTO_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.ZJS.equals(shippercode)){
			dataGrid = ZJS_SendExpress.getSupportedExpressExpType(dataGrid);
		}else if( ExpressCompany.ZTO.equals(shippercode) ){
			dataGrid = ZTO_SendExpress.getSupportedExpressExpType(dataGrid);
		}
		TagUtil.datagrid(response, dataGrid);
	}
	
	
	
	/**
	 * 用户选择快递公司显示列表
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "shipperlist")
	public void shipperlist(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		dataGrid = ExpressCompany.getSupportedExpressDataGrid(dataGrid);
		TagUtil.datagrid(response, dataGrid);
	}
	
	
	@RequestMapping(value = "shipperuserlist")
	public void shipperuserlist(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		//获取当前登录用户
		TSUser user = ResourceUtil.getSessionUserName();//当前登录用户
		//获取公司
		TSUser u = systemService.getEntity(TSUser.class, user.getId());
		TSCompany company = u.getCompany();
		
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
		cq.eq("company", company);
		cq.eq("type", "6");
		
		TSUser tmp = new TSUser();
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tmp);
		cq.addOrder("createDate", SortDirection.desc);
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 获取用户的可用的发货地址列表
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "getMyExpressAddress")
	public void getMyExpressAddress(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {

		CriteriaQuery cq = new CriteriaQuery(CompanyExpressEntity.class, dataGrid);
		//获取当前登录用户
		TSUser user = ResourceUtil.getSessionUserName();//当前登录用户
		//获取公司
		user = systemService.getEntity(TSUser.class, user.getId());
		TSCompany company = user.getCompany();
		//根据公司获取companyexpress表信息
		cq.eq("company", company);
		if( "6".equals(user.getType()) ){//如果是发货商只给他展示属于他的发货地址
			cq.eq("shipperuserid", user.getId());
		}else{
			cq.isNull("shipperuserid");
		}
		CompanyExpressEntity companyExpress = new CompanyExpressEntity();
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyExpress, request.getParameterMap());
		this.companyExpressService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	
	
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(CompanyExpressEntity companyExpress,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(CompanyExpressEntity.class, dataGrid);
		//查询条件组装器
		TSUser user = ResourceUtil.getSessionUserName();//当前登录用户
		
		
		user = systemService.getEntity(TSUser.class, user.getId());
		TSCompany company = user.getCompany();
		//System.out.println("companyid:"+company.getId());
		cq.eq("company", company);
		if( "6".equals(user.getType()) ){//如果是发货商只给他展示属于他的发货地址
			cq.eq("shipperuserid", user.getId());
		}else{
			cq.isNull("shipperuserid");
		}
		
		if( StringUtil.isNotEmpty(companyExpress.getAddress()) ){
			cq.like("address", "%"+companyExpress.getAddress()+"%");
			companyExpress.setAddress(null);
		}
		if(StringUtil.isNotEmpty(companyExpress.getExpressname())){
			cq.like("expressname", "%"+companyExpress.getExpressname()+"%");
			companyExpress.setExpressname(null);
		}
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyExpress, request.getParameterMap());
		this.companyExpressService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

//	/**
//	 * 删除发货地址
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "shipperlist")
//	@ResponseBody
//	public AjaxJson shipperlist( HttpServletRequest request) {
//		AjaxJson	j = new AjaxJson();
//		
//		j.setSuccess(true);
//		j.setMsg("success");
//		
//		
//		return j;
//	}

	/**
	 * 删除发货地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "delexpress")
	@ResponseBody
	public AjaxJson del(CompanyExpressEntity companyExpress, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyExpress = systemService.getEntity(CompanyExpressEntity.class, companyExpress.getId());
		message = "发货地址删除成功";
		companyExpressService.delete(companyExpress);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加发货地址
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(CompanyExpressEntity companyExpress, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(companyExpress.getId())) {
			message = "发货地址更新成功";
			CompanyExpressEntity t = companyExpressService.get(CompanyExpressEntity.class, companyExpress.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(companyExpress, t);
				companyExpressService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "发货地址更新失败";
			}
		} else {
			message = "发货地址添加成功";
			companyExpressService.save(companyExpress);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 发货地址列表页面跳转
	 * 
	 * @return
	 */
	/*@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(CompanyExpressEntity companyExpress, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(companyExpress.getId())) {
			companyExpress = companyExpressService.getEntity(CompanyExpressEntity.class, companyExpress.getId());
			req.setAttribute("companyExpressPage", companyExpress);
		}
		return new ModelAndView("admin/express/companyExpress");
	}*/
}
