package cn.gov.xnc.admin.excel.vo;


import cn.gov.xnc.system.excel.annotation.Excel;


public class OrderExcel {
		
	
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	/**订单编号*/
	@Excel(exportName="订单编号",orderNum="1")
	private java.lang.String identifieror;
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="2")
	private java.lang.String productname;
	/**数量*/
	@Excel(exportName="数量",orderNum="3")
	private java.lang.String number;
	/**收货人*/
	@Excel(exportName="收货人",orderNum="4")
	private java.lang.String customername;
	/**电话*/
	@Excel(exportName="电话",orderNum="5")
	private java.lang.String telephone;
	/**省份*/
	@Excel(exportName="省份",orderNum="6")
	private java.lang.String province;
	/**收货地址*/
	@Excel(exportName="收货地址",orderNum="7")
	private java.lang.String address;
	
	/**发货时间*/
	@Excel(exportName="发货时间",orderNum="8")
	private java.lang.String departuredate;
	/**下单时间*/
	@Excel(exportName="下单时间",orderNum="8")
	private java.lang.String orderdatetime;
	
	/**物流公司*/
	@Excel(exportName="物流公司",orderNum="9")
	private java.lang.String logisticscompany;
	/**物流单号*/
	@Excel(exportName="物流单号",orderNum="10")
	private java.lang.String logisticsnumber;
	/**备注*/
	@Excel(exportName="备注",orderNum="11")
	private java.lang.String remarks;
	
	/**订货价格*/
	@Excel(exportName="订货价格",orderNum="12")
	private java.lang.String price;
	
	/**商品运费*/
	@Excel(exportName="商品运费",orderNum="13")
	private java.lang.String freightpic;
	
	/**发货状态  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7 */
	@Excel(exportName="状态",orderNum="14")
	private java.lang.String state;
	
	/**客户名称*/
	@Excel(exportName="客户名称",orderNum="15")
	private java.lang.String  clientid;
	
	/**业务员*/
	@Excel(exportName="业务员",orderNum="16")
	private java.lang.String  yewu;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的表格id
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}
	
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的表格id
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}
	
	
	/**
	 * @return the identifieror
	 */
	public java.lang.String getIdentifieror() {
		return identifieror;
	}

	/**
	 * @param identifieror the identifieror to set
	 */
	public void setIdentifieror(java.lang.String identifieror) {
		this.identifieror = identifieror;
	}

	/**
	 * @return the productname
	 */
	public java.lang.String getProductname() {
		return productname;
	}

	/**
	 * @param productname the productname to set
	 */
	public void setProductname(java.lang.String productname) {
		this.productname = productname;
	}

	/**
	 * @return the number
	 */
	public java.lang.String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(java.lang.String number) {
		this.number = number;
	}

	/**
	 * @return the customername
	 */
	public java.lang.String getCustomername() {
		return customername;
	}

	/**
	 * @param customername the customername to set
	 */
	public void setCustomername(java.lang.String customername) {
		this.customername = customername;
	}

	/**
	 * @return the telephone
	 */
	public java.lang.String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(java.lang.String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the province
	 */
	public java.lang.String getProvince() {
		return province;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(java.lang.String province) {
		this.province = province;
	}

	/**
	 * @return the address
	 */
	public java.lang.String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(java.lang.String address) {
		this.address = address;
	}

	/**
	 * @return the departuredate
	 */
	public java.lang.String getDeparturedate() {
		return departuredate;
	}

	/**
	 * @param departuredate the departuredate to set
	 */
	public void setDeparturedate(java.lang.String departuredate) {
		this.departuredate = departuredate;
	}

	/**
	 * @return the logisticscompany
	 */
	public java.lang.String getLogisticscompany() {
		return logisticscompany;
	}

	/**
	 * @param logisticscompany the logisticscompany to set
	 */
	public void setLogisticscompany(java.lang.String logisticscompany) {
		this.logisticscompany = logisticscompany;
	}

	/**
	 * @return the logisticsnumber
	 */
	public java.lang.String getLogisticsnumber() {
		return logisticsnumber;
	}

	/**
	 * @param logisticsnumber the logisticsnumber to set
	 */
	public void setLogisticsnumber(java.lang.String logisticsnumber) {
		this.logisticsnumber = logisticsnumber;
	}

	/**
	 * @return the remarks
	 */
	public java.lang.String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(java.lang.String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the price
	 */
	public java.lang.String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(java.lang.String price) {
		this.price = price;
	}

	/**
	 * @return the state
	 */
	public java.lang.String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(java.lang.String state) {
		this.state = state;
	}

	/**
	 * @return the clientid
	 */
	public java.lang.String getClientid() {
		return clientid;
	}

	/**
	 * @param clientid the clientid to set
	 */
	public void setClientid(java.lang.String clientid) {
		this.clientid = clientid;
	}

	/**
	 * @return the yewu
	 */
	public java.lang.String getYewu() {
		return yewu;
	}

	/**
	 * @param yewu the yewu to set
	 */
	public void setYewu(java.lang.String yewu) {
		this.yewu = yewu;
	}

	public java.lang.String getOrderdatetime() {
		return orderdatetime;
	}

	public void setOrderdatetime(java.lang.String orderdatetime) {
		this.orderdatetime = orderdatetime;
	}

	public java.lang.String getFreightpic() {
		return freightpic;
	}

	public void setFreightpic(java.lang.String freightpic) {
		this.freightpic = freightpic;
	}
	
}
