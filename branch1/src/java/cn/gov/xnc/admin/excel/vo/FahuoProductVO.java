package cn.gov.xnc.admin.excel.vo;

import java.math.BigDecimal;

import cn.gov.xnc.system.excel.annotation.Excel;


/**   
 * @Title: Entity
 * @Description: 导出产品订单
 * @author zero
 * @date 2016-04-13 20:22:17
 * @version V1.0   
 *
 */

public class FahuoProductVO  {
	
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	/**下单日期*/
	@Excel(exportName="下单日期",orderNum="1")
	private java.lang.String createdate;
	/**产品名称*/
	@Excel(exportName="下单产品",orderNum="2")
	private java.lang.String productname;
	/**件数*/
	@Excel(exportName="件数",orderNum="3")
	private java.lang.Integer number;
	/**规格*/
	@Excel(exportName="规格",orderNum="4")
	private java.lang.String specifications;
	/**客户姓名*/
	@Excel(exportName="客户姓名",orderNum="5")
	private java.lang.String customername;
	/**电话*/
	@Excel(exportName="电话",orderNum="6")
	private java.lang.String telephone;
	/**收货地址*/
	@Excel(exportName="收货地址",orderNum="7")
	private java.lang.String address;
	
	/**物流公司*/
	@Excel(exportName="物流公司",orderNum="8")
	private java.lang.String logisticscompany;
	/**快递单号*/
	@Excel(exportName="快递单号",orderNum="9")
	private java.lang.String logisticsnumber;
	
	/**备注*/
	@Excel(exportName="备注",orderNum="10")
	private java.lang.String remarks;
	
//	/**价格*/
//	@Excel(exportName="价格",orderNum="11")
//	private BigDecimal price;
	
	/**快递运费*/
	@Excel(exportName="快递运费",orderNum="11")
	private BigDecimal freightpic;

	/**商品ID*/
	@Excel(exportName="订单ID",orderNum="12")
	private java.lang.String orderId;
	
	
	
	/**公司id*/
	private  java.lang.String  companyId ;;

	/**结算单价*/
	private BigDecimal unitPrice;
	
	/**发货日期*/
	private java.util.Date departuredate;
	
	
	
//	TSUser TSUser = new TSUser();
//	
//	/**
//	 *方法: 取得java.lang.String
//	 *@return: java.lang.String  农场ID、用户ID
//	 */
//	public TSUser getTSUser() {
//		return TSUser;
//	}
//	/**
//	 *方法: 设置java.lang.String
//	 *@param: java.lang.String  农场ID、用户ID
//	 */
//	public void setTSUser(TSUser tSUser) {
//		TSUser = tSUser;
//	}
	
	
//	/**
//	 *方法: 取得java.lang.String
//	 *@return: java.lang.String  id
//	 */
//
//	public java.lang.String getId(){
//		return this.id;
//	}
//
//	/**
//	 *方法: 设置java.lang.String
//	 *@param: java.lang.String  id
//	 */
//	public void setId(java.lang.String id){
//		this.id = id;
//	}
	
	
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品名称
	 */

	public java.lang.String getProductname(){
		return this.productname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品名称
	 */
	public void setProductname(java.lang.String productname){
		this.productname = productname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  件数
	 */

	public java.lang.Integer getNumber(){
		return this.number;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  件数
	 */
	public void setNumber(java.lang.Integer number){
		this.number = number;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  规格
	 */

	public java.lang.String getSpecifications(){
		return this.specifications;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  规格
	 */
	public void setSpecifications(java.lang.String specifications){
		this.specifications = specifications;
	}
	

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户姓名
	 */

	public java.lang.String getCustomername(){
		return this.customername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户姓名
	 */
	public void setCustomername(java.lang.String customername){
		this.customername = customername;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电话
	 */

	public java.lang.String getTelephone(){
		return this.telephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电话
	 */
	public void setTelephone(java.lang.String telephone){
		this.telephone = telephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收货地址
	 */

	public java.lang.String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收货地址
	 */
	public void setAddress(java.lang.String address){
		this.address = address;
	}
	
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  发货日期
	 */

	public java.util.Date getDeparturedate(){
		return this.departuredate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  发货日期
	 */
	public void setDeparturedate(java.util.Date departuredate){
		this.departuredate = departuredate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  下单日期
	 */

	public java.lang.String getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  下单日期
	 */
	public void setCreatedate(java.lang.String createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流发货公司
	 */

	public java.lang.String getLogisticscompany(){
		return this.logisticscompany;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  物流发货公司
	 */
	public void setLogisticscompany(java.lang.String logisticscompany){
		this.logisticscompany = logisticscompany;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递单号
	 */

	public java.lang.String getLogisticsnumber(){
		return this.logisticsnumber;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递单号
	 */
	public void setLogisticsnumber(java.lang.String logisticsnumber){
		this.logisticsnumber = logisticsnumber;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */

	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  快递运费
	 */

	public BigDecimal getFreightpic(){
		return this.freightpic;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  快递运费
	 */
	public void setFreightpic(BigDecimal freightpic){
		this.freightpic = freightpic;
	}
//	/**
//	 *方法: 取得BigDecimal
//	 *@return: BigDecimal  结算价
//	 */
//	public BigDecimal getPrice(){
//		return this.price;
//	}
//
//	/**
//	 *方法: 设置BigDecimal
//	 *@param: BigDecimal  结算价
//	 */
//	public void setPrice(BigDecimal price){
//		this.price = price;
//	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的表格id
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}
	
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的表格id
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}
	
	/**
	 *方法: 获取BigDecimal
	 *@param: BigDecimal  结算单价格
	 */
	
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	
	
	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  结算单价格
	 */
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	
	
	
	


	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单id
	 */
	public java.lang.String getOrderId() {
		return orderId;
	}
	
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单id
	 */
	public void setOrderId(java.lang.String orderId) {
		this.orderId = orderId;
	}


	public java.lang.String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(java.lang.String companyId) {
		this.companyId = companyId;
	}
	
	
	
}
