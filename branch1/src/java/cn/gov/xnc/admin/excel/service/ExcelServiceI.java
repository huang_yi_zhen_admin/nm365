package cn.gov.xnc.admin.excel.service;

import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface ExcelServiceI extends CommonService{

	public String getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder);
}
