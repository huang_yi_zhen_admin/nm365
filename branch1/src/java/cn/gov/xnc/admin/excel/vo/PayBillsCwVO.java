package cn.gov.xnc.admin.excel.vo;

import java.math.BigDecimal;


import cn.gov.xnc.system.excel.annotation.Excel;




/**   
 * @Title: Entity
 * @Description: 财务导出资金流水
 * @author zero
 * @date 2016-04-13 19:54:05
 * @version V1.0   
 *
 */

public class PayBillsCwVO {
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String id;
	/**时间*/
	@Excel(exportName="下单日期",orderNum="1")
	private java.lang.String createdate;
	
	@Excel(exportName="订单编号",orderNum="2")
	private java.lang.String identifier;
	
	@Excel(exportName="渠道",orderNum="3")
	private java.lang.String companyName;
	
	@Excel(exportName="业务员",orderNum="4")
	private java.lang.String createUser;
	
	/**类型   待支付_1,已支付_2,未知_0,销售款_3,奖励_4,退款_5,取消_6*/
	@Excel(exportName="类型",orderNum="5")
	private java.lang.String type;
	
	/**支付方式  支付宝_0,微信_1,银联_2,其他_3 */
	@Excel(exportName="支付方式",orderNum="6")
	private java.lang.String paymentmethod;
	
	/**到款状态   未经账_0,已到账_1,异常_2,冻结_3*/
	@Excel(exportName="到款状态",orderNum="7")
	private java.lang.String received;
	
	/**金额*/
	@Excel(exportName="金额",orderNum="8")
	private java.lang.String money;
	/**
	 * @return the id
	 */
	public java.lang.String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}
	/**
	 * @return the 下单日期
	 */
	public java.lang.String getCreatedate() {
		return createdate;
	}
	/**
	 * @param 下单日期 the 下单日期 to set
	 */
	public void setCreatedate(java.lang.String createdate) {
		this.createdate = createdate;
	}
	
	
	
	/**
	 * @return 订单编号
	 */
	public java.lang.String getIdentifier() {
		return identifier;
	}
	/**
	 * @param 订单编号 to set
	 */
	public void setIdentifier(java.lang.String identifier) {
		this.identifier = identifier;
	}
	/**
	 * @return the 渠道
	 */
	public java.lang.String getCompanyName() {
		return companyName;
	}
	/**
	 * @param 渠道 the 渠道 to set
	 */
	public void setCompanyName(java.lang.String companyName) {
		this.companyName = companyName;
	}
	/**
	 * @return the 业务员
	 */
	public java.lang.String getCreateUser() {
		return createUser;
	}
	/**
	 * @param 业务员 the 业务员 to set
	 */
	public void setCreateUser(java.lang.String createUser) {
		this.createUser = createUser;
	}
	/**
	 * @return 类型   待支付_1,已支付_2,未知_0,销售款_3,奖励_4,退款_5,取消_6
	 */
	public java.lang.String getType() {
		return type;
	}
	/**
	 * @param 类型   待支付_1,已支付_2,未知_0,销售款_3,奖励_4,退款_5,取消_6 to set
	 */
	public void setType(java.lang.String type) {
		this.type = type;
	}
	/**
	 * @return 支付方式  支付宝_0,微信_1,银联_2,其他_3
	 */
	public java.lang.String getPaymentmethod() {
		return paymentmethod;
	}
	/**
	 * @param 支付方式  支付宝_0,微信_1,银联_2,其他_3 to set
	 */
	public void setPaymentmethod(java.lang.String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	/**
	 * @return 未经账_0,已到账_1,异常_2,冻结_3
	 */
	public java.lang.String getReceived() {
		return received;
	}
	/**
	 * @param 未经账_0,已到账_1,异常_2,冻结_3 to set
	 */
	public void setReceived(java.lang.String received) {
		this.received = received;
	}
	/**
	 * @return 金额
	 */
	public java.lang.String getMoney() {
		return money;
	}
	/**
	 * @param 金额 to set
	 */
	public void setMoney(java.lang.String money) {
		this.money = money;
	}
	
	
	

	
}
