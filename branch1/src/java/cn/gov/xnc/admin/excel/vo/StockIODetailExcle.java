package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class StockIODetailExcle {

	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="1")
	private java.lang.String productid_name;
	/**品牌*/
	@Excel(exportName="品牌",orderNum="2")
	private java.lang.String productid_brandid_brandname;
	/**规格*/
	@Excel(exportName="规格",orderNum="3")
	private java.lang.String productid_specifications;
	
	/**单位*/
	@Excel(exportName="单位",orderNum="4")
	private java.lang.String productid_unit;
	
	/**数量*/
	@Excel(exportName="数量",orderNum="5")
	private java.lang.String iostocknum;
	
	/**单价*/
	@Excel(exportName="单价",orderNum="6")
	private java.lang.String unitprice;
	
	/**数量*/
	@Excel(exportName="总价",orderNum="7")
	private java.lang.String totalprice;

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the productid_name
	 */
	public java.lang.String getProductid_name() {
		return productid_name;
	}

	/**
	 * @param productid_name the productid_name to set
	 */
	public void setProductid_name(java.lang.String productid_name) {
		this.productid_name = productid_name;
	}

	/**
	 * @return the productid_brandid_brandname
	 */
	public java.lang.String getProductid_brandid_brandname() {
		return productid_brandid_brandname;
	}

	/**
	 * @param productid_brandid_brandname the productid_brandid_brandname to set
	 */
	public void setProductid_brandid_brandname(java.lang.String productid_brandid_brandname) {
		this.productid_brandid_brandname = productid_brandid_brandname;
	}

	/**
	 * @return the productid_specifications
	 */
	public java.lang.String getProductid_specifications() {
		return productid_specifications;
	}

	/**
	 * @param productid_specifications the productid_specifications to set
	 */
	public void setProductid_specifications(java.lang.String productid_specifications) {
		this.productid_specifications = productid_specifications;
	}

	/**
	 * @return the productid_unit
	 */
	public java.lang.String getProductid_unit() {
		return productid_unit;
	}

	/**
	 * @param productid_unit the productid_unit to set
	 */
	public void setProductid_unit(java.lang.String productid_unit) {
		this.productid_unit = productid_unit;
	}

	/**
	 * @return the iostocknum
	 */
	public java.lang.String getIostocknum() {
		return iostocknum;
	}

	/**
	 * @param iostocknum the iostocknum to set
	 */
	public void setIostocknum(java.lang.String iostocknum) {
		this.iostocknum = iostocknum;
	}

	/**
	 * @return the unitprice
	 */
	public java.lang.String getUnitprice() {
		return unitprice;
	}

	/**
	 * @param unitprice the unitprice to set
	 */
	public void setUnitprice(java.lang.String unitprice) {
		this.unitprice = unitprice;
	}

	/**
	 * @return the totalprice
	 */
	public java.lang.String getTotalprice() {
		return totalprice;
	}

	/**
	 * @param totalprice the totalprice to set
	 */
	public void setTotalprice(java.lang.String totalprice) {
		this.totalprice = totalprice;
	}
	
	
	

}
