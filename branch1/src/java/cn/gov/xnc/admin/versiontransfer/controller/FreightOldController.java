package cn.gov.xnc.admin.versiontransfer.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.admin.versiontransfer.enity.FreightOldEntity;
import cn.gov.xnc.admin.versiontransfer.service.FreightOldServiceI;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 运费模板
 * @author zero
 * @date 2016-09-28 16:17:32
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/freightOldController")
public class FreightOldController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(FreightOldController.class);

	@Autowired
	private FreightOldServiceI freightOldService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 运费模板列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView freight(HttpServletRequest request) {
		return new ModelAndView("admin/freight/freightList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(FreightOldEntity freight,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(FreightOldEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, freight, request.getParameterMap());
		this.freightOldService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除运费模板
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(FreightOldEntity freight, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		freight = systemService.getEntity(FreightOldEntity.class, freight.getId());
		message = "运费模板删除成功";
		freightOldService.delete(freight);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加运费模板
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(FreightOldEntity freight, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(freight.getId())) {
			message = "运费模板更新成功";
			FreightOldEntity t = freightOldService.get(FreightOldEntity.class, freight.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(freight, t);
				freightOldService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "运费模板更新失败";
			}
		} else {
			message = "运费模板添加成功";
			freightOldService.save(freight);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 运费模板列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(FreightOldEntity freight, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(freight.getId())) {
			freight = freightOldService.getEntity(FreightOldEntity.class, freight.getId());
			req.setAttribute("freightPage", freight);
		}
		return new ModelAndView("admin/freight/freight");
	}
	
	
	/**
	 * 选择运费模板
	 * 
	 * @return
	 */
	@RequestMapping(params = "choiceFreight")
	@ResponseBody
	public String choiceFreight(HttpServletRequest request) {
		
		
		
		return "success";
	}
	
	
	/**
	 * 选择运费模板
	 * 
	 * @return
	 */
	@RequestMapping(value = "transferFreight")
	public ModelAndView transferFreight(HttpServletRequest request) {
		CriteriaQuery cq = new CriteriaQuery(FreightOldEntity.class);
		cq.eq("state", "1");
		cq.add();
		List<FreightOldEntity> freightOldList =  systemService.getListByCriteriaQuery(cq, false);
		if(null != freightOldList && freightOldList.size() > 0){
			for (FreightOldEntity freightOldEntity : freightOldList) {
				
				TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", freightOldEntity.getCompany().getId());
				FreightEntity freight = new FreightEntity();
				//创建基础数据
				freight.setId(IdWorker.generateSequenceNo());
				freight.setFreightname(freightOldEntity.getName() + "-" + freightOldEntity.getFreightname());
				freight.setName(freightOldEntity.getName() + "-" + freightOldEntity.getFreightname());
				freight.setFreightunit(1);
				freight.setFreightpic(freightOldEntity.getHainansheng());
				freight.setFreightmoreunit(1);
				freight.setFreightmorepic(freightOldEntity.getHainansheng());
				freight.setCompany(company);
				freight.setCreatedate(DateUtils.getDate());
				freight.setState("1");
				freight.setType("1");
				
				//创建运费地区数据
				List<FreightDetailsEntity> freightDetailList = freightOldService.buildFreightDetail(freight, freightOldEntity);
				if(freightDetailList.size() > 0){
					freight.setFreightDetailsList(freightDetailList);
				}else{
					freight.setFreightDetailsList(null);
				}
				
				systemService.save(freight);
			}
		}
		
			return  new ModelAndView("login/login");
	}
	
}
