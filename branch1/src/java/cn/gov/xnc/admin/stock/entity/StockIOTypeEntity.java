package cn.gov.xnc.admin.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

/**   
 * @Title: Entity
 * @Description: 进销存出入库类型：I 入库 O 出库
 * @author zero
 * @date 2017-01-23 15:02:12
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_io_type", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockIOTypeEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**公司编码*/
	private TSCompany companyid;
	/**进销存编码 I 入库 O 出库*/
	private java.lang.String typecode;
	/**进销存类型名称*/
	private java.lang.String typename;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司编码
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  进销存编码 I 入库 O 出库
	 */
	@Column(name ="TYPECODE",nullable=false,length=2)
	public java.lang.String getTypecode(){
		return this.typecode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  进销存编码 I 入库 O 出库
	 */
	public void setTypecode(java.lang.String typecode){
		this.typecode = typecode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  进销存类型名称
	 */
	@Column(name ="TYPENAME",nullable=false,length=200)
	public java.lang.String getTypename(){
		return this.typename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  进销存类型名称
	 */
	public void setTypename(java.lang.String typename){
		this.typename = typename;
	}
}
