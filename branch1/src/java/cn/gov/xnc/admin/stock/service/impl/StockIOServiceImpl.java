package cn.gov.xnc.admin.stock.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.admin.stock.service.StockIOServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("stockIOService")
@Transactional
public class StockIOServiceImpl extends CommonServiceImpl implements StockIOServiceI {
	
	private static final Logger logger = Logger.getLogger(StockProductServiceImpl.class);

	@Override
	public StockEntity buildStockOrder() {
		return null;
	}
	
	public AjaxJson save(StockIOEntity companyStockOrder, HttpServletRequest request,StockIODetailServiceI stockIODetailService,StockProductServiceI stockProductService,SystemService systemService){
		AjaxJson j = new AjaxJson();
		String message;
		TSUser user = ResourceUtil.getSessionUserName();
		String iotype = request.getParameter("type");
		if (StringUtil.isNotEmpty(companyStockOrder.getId())) {
			message = "出入库表单，记录出入库总体信息更新成功";
			StockIOEntity t = get(StockIOEntity.class, companyStockOrder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(companyStockOrder, t);
				saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				j.setSuccess(false);
				message = "出入库表单，记录出入库总体信息更新失败" + e;
			}
		} else {
			
			try {
				
				TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
				TSUser operator = systemService.findUniqueByProperty(TSUser.class, "id", companyStockOrder.getOperator().getId());
				StockEntity stock = systemService.findUniqueByProperty(StockEntity.class, "id", companyStockOrder.getStockid().getId());
				StockIOTypeEntity stockiotype = systemService.findUniqueByProperty(StockIOTypeEntity.class, "id", companyStockOrder.getStocktypeid().getId());
				//创建基础数据
				StockIOEntity stockIOEntity = new StockIOEntity();
				stockIOEntity.setIdentifier(IdWorker.generateSequenceNo());
				stockIOEntity.setCompanyid(company);
				stockIOEntity.setCreatetime(DateUtils.getDate());
				stockIOEntity.setOperator(operator);
				stockIOEntity.setRemark(companyStockOrder.getRemark());
				stockIOEntity.setStockid(stock);
				stockIOEntity.setStocktypeid(stockiotype);
				stockIOEntity.setStockIODetailList(null);
				
				save(stockIOEntity);
				
				//创建出入库详细数据
				List<StockIODetailEntity> stockIODetailList = stockIODetailService.buildStockInDetail(stockIOEntity, request);
				if(stockIODetailList.size() > 0){
					stockIOEntity.setStockIODetailList(stockIODetailList);
					updateEntitie(stockIOEntity);
					
					//更新或添加库存商品
					for (StockIODetailEntity stockIODetail : stockIODetailList) {
						try {
							
							ProductEntity product = systemService.findUniqueByProperty(ProductEntity.class, "id", stockIODetail.getProductid().getId());
							stockProductService.saveOrUpdateStockProduct(product, iotype, stock, stockIODetail.getIostocknum(), stockIODetail.getUnitprice(), stockIODetail.getTotalprice());
							
						} catch (Exception e) {
							systemService.addLog("更新或添加库存商品失败" + e, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
						}
						
					}
					
					message = "出入库表单添加成功";
				}else{
					j.setSuccess(false);
					message = "出入库表单添加失败，您没有操作任何商品！";
				}
				
			} catch (Exception e) {
				message = "出入库表单添加失败" + e;
				j.setSuccess(false);
				logger.error(message);
			}
			
		}
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		j.setMsg(message);
		return j;
	}
}