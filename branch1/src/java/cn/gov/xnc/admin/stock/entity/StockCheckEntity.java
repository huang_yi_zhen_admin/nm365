package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 进销存盘点总单
 * @author zero
 * @date 2017-01-23 15:39:09
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_check", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockCheckEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**盘点名称*/
	private java.lang.String name;
	/**盘点仓库*/
	private StockEntity stockid;
	/**经办人*/
	private TSUser operator;
	/**盘点时间*/
	private java.util.Date createtime;
	/**普通编码*/
	private java.lang.String identifier;
	/**所属公司*/
	private TSCompany companyid;
	
	
	
	/**出库产品*/
	private List<StockCheckDetailEntity> stockCheckDetailList;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  盘点名称
	 */
	@Column(name ="NAME",nullable=false,length=200)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  盘点名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  盘点仓库
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKID")
	public StockEntity getStockid(){
		return this.stockid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  盘点仓库
	 */
	public void setStockid(StockEntity stockid){
		this.stockid = stockid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  经办人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OPERATOR")
	public TSUser getOperator(){
		return this.operator;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  经办人
	 */
	public void setOperator(TSUser operator){
		this.operator = operator;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  盘点时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  盘点时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}

	/**出库产品*/
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "checkorderid")
	public List<StockCheckDetailEntity> getstockCheckDetailList() {
		return stockCheckDetailList;
	}

	public void setstockCheckDetailList(List<StockCheckDetailEntity> stockCheckDetailList) {
		this.stockCheckDetailList = stockCheckDetailList;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  出库单普通编号
	 */
	@Column(name ="IDENTIFIER",nullable=true,length=32)
	public java.lang.String getIdentifier(){
		return this.identifier;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单编号
	 */
	public void setIdentifier(java.lang.String identifier){
		this.identifier = identifier;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司编码
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
}
