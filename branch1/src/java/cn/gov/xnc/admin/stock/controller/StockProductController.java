package cn.gov.xnc.admin.stock.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;

/**   
 * @Title: Controller
 * @Description: 库存相关联商品表
 * @author zero
 * @date 2017-03-22 00:52:46
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockProductController")
public class StockProductController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockProductController.class);

	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 盘点仓库商品选择
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "stockCheckSelectProduct")
	public ModelAndView stockCheckSelectProduct(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		String stockid = request.getParameter("stockid");
		request.setAttribute("stockid", stockid);
		
		//获取公司商品品牌列表
		CriteriaQuery cb = new CriteriaQuery(ProductBrandEntity.class);
		cb.eq("company", user.getCompany());
		cb.add();
		List<ProductBrandEntity> brandlist = systemService.getListByCriteriaQuery(cb, false);
		request.setAttribute("brandlist", brandlist);
				
		return new ModelAndView("admin/stock/stockCheckSelectProduct");
	}
	
	/**
	 * 库存相关联商品表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockProduct(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.add();
		
		List<StockEntity> stocklist = systemService.getListByCriteriaQuery(cq, false);
		request.setAttribute("stocklist", stocklist);
		
		return new ModelAndView("admin/stock/stockProductInfoList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockProductEntity stockProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();

		CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class, dataGrid);
		cq.eq("companyid", user.getCompany());
		
		if( null != stockProduct.getProductid() && StringUtil.isNotEmpty( stockProduct.getProductid().getName()) ){
			cq.createAlias("productid", "product");
			cq.add(Restrictions.like("product.name", "%"+stockProduct.getProductid().getName()+"%"));
			stockProduct.getProductid().setName(null);
		}
		
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockProduct, request.getParameterMap());
		this.stockProductService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除库存相关联商品表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockProductEntity stockProduct, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockProduct = systemService.getEntity(StockProductEntity.class, stockProduct.getId());
		message = "库存相关联商品表删除成功";
		stockProductService.delete(stockProduct);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加库存相关联商品表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockProductEntity stockProduct, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(stockProduct.getId())) {
			message = "库存相关联商品表更新成功";
			StockProductEntity t = stockProductService.get(StockProductEntity.class, stockProduct.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockProduct, t);
				stockProductService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "库存相关联商品表更新失败";
			}
		} else {
			message = "库存相关联商品表添加成功";
			stockProductService.save(stockProduct);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 库存相关联商品表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockProductEntity stockProduct, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockProduct.getId())) {
			stockProduct = stockProductService.getEntity(StockProductEntity.class, stockProduct.getId());
			req.setAttribute("stockProductPage", stockProduct);
		}
		return new ModelAndView("admin/stock/stockProduct");
	}
}
