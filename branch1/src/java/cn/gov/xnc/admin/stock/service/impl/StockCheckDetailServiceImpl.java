package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.service.StockCheckDetailServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;

@Service("stockCheckDetailService")
@Transactional
public class StockCheckDetailServiceImpl extends CommonServiceImpl implements StockCheckDetailServiceI {
	
	private Logger logger = Logger.getLogger(StockCheckDetailServiceImpl.class);
	
	@Override
	public List<StockCheckDetailEntity> buildStockCheckDetail(StockCheckEntity stockCheckEntity, HttpServletRequest req) throws Exception {
		List<StockCheckDetailEntity> stockCheckDetailList = new ArrayList<StockCheckDetailEntity>();
		String expLen = ResourceUtil.getParameter("Len");
		if(StringUtil.isNotEmpty(expLen)){
			
			int stockIODetailsNum = Integer.parseInt(expLen);
			for (int i = 0; i < stockIODetailsNum; i++) {
				String stockproductid = req.getParameter("id-" + i);
				String productName = req.getParameter("productid_name-" + i);
				if(StringUtil.isNotEmpty(stockproductid) && StringUtil.isNotEmpty(productName)){
					
					StockProductEntity stockProductEntity = findUniqueByProperty(StockProductEntity.class, "id", stockproductid);
					StockCheckDetailEntity stockCheckDetail = new StockCheckDetailEntity();
					stockCheckDetail.setAdjuststocknum(new BigDecimal(req.getParameter("adjuststocknum-" + i)));
					stockCheckDetail.setCheckorderid(stockCheckEntity);
					stockCheckDetail.setCheckstocknum(new BigDecimal(req.getParameter("checkstocknum-" + i)));
					stockCheckDetail.setCurrentstocknum(new BigDecimal(req.getParameter("stocknum-" + i)));
					stockCheckDetail.setRemark(req.getParameter("remark-" + i));
					stockCheckDetail.setStockproductid(stockProductEntity);
					stockCheckDetailList.add(stockCheckDetail);
				}
				
			}
		}
		
		if(stockCheckDetailList.size() > 0){
			
			batchSave(stockCheckDetailList);
		}
		return stockCheckDetailList;
	}
	
}