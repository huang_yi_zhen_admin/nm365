package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.product.entity.ProductEntity;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 出库详细单
 * @author zero
 * @date 2017-03-22 14:40:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_io_detail", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockIODetailEntity implements java.io.Serializable {
	/**主键*/
	private java.lang.String id;
	/**identifier*/
	private java.lang.String identifier;
	/**出入库商品编码*/
	private ProductEntity productid;
	/**出、入库数量*/
	private BigDecimal iostocknum;
	/**进货单价*/
	private BigDecimal unitprice;
	/**总成本*/
	private BigDecimal totalprice;
	/**备注*/
	private java.lang.String remark;
	/**出入库单编码*/
	private StockIOEntity stockorderid;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得ProductEntity
	 *@return: ProductEntity  出入库商品编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  出入库商品编码
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  出、入库数量
	 */
	@Column(name ="IOSTOCKNUM",nullable=false,precision=10,scale=2)
	public BigDecimal getIostocknum(){
		return this.iostocknum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  出、入库数量
	 */
	public void setIostocknum(BigDecimal iostocknum){
		this.iostocknum = iostocknum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  进货单价
	 */
	@Column(name ="UNITPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getUnitprice(){
		return this.unitprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  进货单价
	 */
	public void setUnitprice(BigDecimal unitprice){
		this.unitprice = unitprice;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  总成本
	 */
	@Column(name ="TOTALPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getTotalprice(){
		return this.totalprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  总成本
	 */
	public void setTotalprice(BigDecimal totalprice){
		this.totalprice = totalprice;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=4000)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  出入库总单编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKORDERID")
	public StockIOEntity getStockorderid(){
		return this.stockorderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  出入库总单编码
	 */
	public void setStockorderid(StockIOEntity stockorderid){
		this.stockorderid = stockorderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  出库详单普通编号
	 */
	@Column(name ="IDENTIFIER",nullable=true,length=32)
	public java.lang.String getIdentifier(){
		return this.identifier;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  出库详单普通编号
	 */
	public void setIdentifier(java.lang.String identifier){
		this.identifier = identifier;
	}
}
