package cn.gov.xnc.admin.stock.controller;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.service.StockServiceI;

/**   
 * @Title: Controller
 * @Description: 进销存仓库管理，记录公司的仓库名称和地址等
 * @author zero
 * @date 2017-01-23 15:00:40
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockController")
public class StockController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockController.class);

	@Autowired
	private StockServiceI stockService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 进销存仓库管理，记录公司的仓库名称和地址等列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView companyStock(HttpServletRequest request) {
		
		return new ModelAndView("admin/stock/stockList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockEntity companyStock,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class, dataGrid);
		cq.eq("companyid", user.getCompany());
		if(StringUtil.isNotEmpty(companyStock.getStockname())){
			cq.like("stockname", "%" + companyStock.getStockname() + "%");
		}
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyStock, request.getParameterMap());
		this.stockService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 进销存仓库管理，记录公司的仓库名称和地址等列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockProductList")
	public ModelAndView stockProductList(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockProductList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "stockProductDatagrid")
	public void stockProductDatagrid(StockEntity companyStock,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyStock, request.getParameterMap());
		this.stockService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除进销存仓库管理，记录公司的仓库名称和地址等
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockEntity companyStock, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			companyStock = systemService.getEntity(StockEntity.class, companyStock.getId());
			message = "您的仓库【" + companyStock.getStockname() + "】已经删除!";
			stockService.delete(companyStock);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			j.setSuccess(true);
		} catch (Exception e) {
			j.setSuccess(false);
			message = e.getMessage();
		}
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加进销存仓库管理，记录公司的仓库名称和地址等
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockEntity companyStock, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
//		String stockareaid = ResourceUtil.getParameter("stockareaid");
		TSUser user = ResourceUtil.getSessionUserName();
		TSUser dutyperson = companyStock.getDutyperson();
		String dutypersonid = companyStock.getDutyperson().getId();
		if(null != dutypersonid){
			dutyperson = systemService.findUniqueByProperty(TSUser.class, "id", companyStock.getDutyperson().getId());
		}
		if (StringUtil.isNotEmpty(companyStock.getId())) {
			message = "进销存仓库管理更新成功";
			StockEntity t = stockService.get(StockEntity.class, companyStock.getId());
			try {
				//companyStock.setStockaddress(stockareaid + "_" + companyStock.getStockaddress());
				companyStock.setCompanyid(user.getCompany());
				if(null != dutyperson){
					companyStock.setDutyperson(dutyperson);
					if(StringUtil.isEmpty(companyStock.getContactphone())){
						companyStock.setContactphone(dutyperson.getMobilephone());
					}
				}
				
				MyBeanUtils.copyBeanNotNull2Bean(companyStock, t);
				stockService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "进销存仓库管理更新失败";
			}
		} else {
			message = "进销存仓库管理，记录公司的仓库名称和地址等添加成功";
			companyStock.setId(IdWorker.generateSequenceNo());
//			companyStock.setStockaddress(stockareaid + "_" + companyStock.getStockaddress());
			companyStock.setCompanyid(user.getCompany());
			companyStock.setCreatetime(new Date());
			if(null != dutyperson){
				companyStock.setDutyperson(dutyperson);
				if(StringUtil.isEmpty(companyStock.getContactphone())){
					companyStock.setContactphone(dutyperson.getMobilephone());
				}
			}
			stockService.save(companyStock);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 进销存仓库管理，记录公司的仓库名称和地址等列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockEntity companyStock, HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(TSUser.class);
		cq.eq("company", user.getCompany());
		cq.in("type", new String[]{"1","2","4"});
		cq.add();
		
		List<TSUser> staffList = systemService.getListByCriteriaQuery(cq, false);
		req.setAttribute("staffList", staffList);
		
		if (StringUtil.isNotEmpty(companyStock.getId())) {
			companyStock = stockService.getEntity(StockEntity.class, companyStock.getId());
			TSTerritory top1Territory = companyStock.getTerritoryid();
			TSTerritory top2Territory = null;
			TSTerritory top3Territory = null;
			
			if(null != top1Territory && StringUtil.isNotEmpty(top1Territory.getId())){
				top2Territory = top1Territory.getTSTerritory();
				req.setAttribute("provinceid", top1Territory.getId());
			}
			if(null != top2Territory && StringUtil.isNotEmpty(top2Territory.getId())){
				top3Territory = top2Territory.getTSTerritory();
				req.setAttribute("provinceid", top2Territory.getId());
				req.setAttribute("cityid", top1Territory.getId());
			}
			if(null != top3Territory && StringUtil.isNotEmpty(top3Territory.getId())){
				req.setAttribute("provinceid", top3Territory.getId());
				req.setAttribute("cityid", top2Territory.getId());
				req.setAttribute("regionid", top1Territory.getId());
			}
			
		}
		
		req.setAttribute("stockPage", companyStock);
		return new ModelAndView("admin/stock/stock");
	}
}
