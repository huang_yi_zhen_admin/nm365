package cn.gov.xnc.admin.stock.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.service.StockServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("stockService")
@Transactional
public class StockServiceImpl extends CommonServiceImpl implements StockServiceI {
	
}