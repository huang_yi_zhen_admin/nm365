package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.StringUtil;

@Service("stockProductService")
@Transactional
public class StockProductServiceImpl extends CommonServiceImpl implements StockProductServiceI {

	public boolean saveOrUpdateStockProduct(ProductEntity stockproduct, String type, StockEntity stock, BigDecimal changNum, BigDecimal baseunitprice, BigDecimal detailtotalprice) throws Exception {
		
		boolean rlt = true;
		if(null != stockproduct && StringUtil.isNotEmpty(stockproduct.getId()) && changNum.compareTo(new BigDecimal(0.00)) >= 0){
			
			CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class);
			cq.eq("productid.id", stockproduct.getId());
			cq.eq("stockid", stock);
			cq.add();
			
			StockProductEntity stockProductEntity = null;
			List<StockProductEntity> stockProductList = getListByCriteriaQuery(cq, false);
			if(null != stockProductList && stockProductList.size() > 0){
				
				stockProductEntity = stockProductList.get(0);
				String status = "1";
				
				BigDecimal subnum = changNum;
				if("I".equals(type)){
					subnum = stockProductEntity.getStocknum().add(changNum);
				}else if( "O".equals(type) ){
					subnum = stockProductEntity.getStocknum().subtract(changNum);
				}
				if(subnum.compareTo(new BigDecimal(0.00)) > 0 && subnum.compareTo(stockProductEntity.getAlertnum()) <= 0){
					status = "2";
				}
				if(subnum.compareTo(new BigDecimal(0.00)) > 0 && subnum.compareTo(stockProductEntity.getAlertnum()) > 0){
					status = "1";
				}
				if(subnum.compareTo(new BigDecimal(0.00)) <= 0){
					status = "3";
				}
				
				MathContext mc = new MathContext(4, RoundingMode.HALF_DOWN);
				BigDecimal totalprice = stockProductEntity.getTotalprice();
				if( null == totalprice ){
					totalprice = new BigDecimal(0.00);
				}
				BigDecimal averageprice = stockProductEntity.getAverageprice();
				if( null == averageprice ){
					averageprice = totalprice.divide(subnum,mc);
				}
				//计算库存总价和均价
				/*BigDecimal inTotalNum = new BigDecimal(0);
				BigDecimal inTotalPrice = new BigDecimal(0);
				BigDecimal outTotalNum = new BigDecimal(0);*/
				
				if(!"CK".equals(type)){
					//计算库存总价和均价
					if("I".equals(type)){
						totalprice = totalprice.add(detailtotalprice);//原来的库存总金额加上后面的库存总金额
						averageprice = totalprice.divide(subnum,mc);//库存均价为库存总金额除以数量
					}else if( "O".equals(type) ){
						if( subnum.compareTo(new BigDecimal(0.00)) == 0 ){
							totalprice = new BigDecimal(0.00);
							averageprice = new BigDecimal(0.00);
						}else{
							totalprice = totalprice.subtract( averageprice.multiply(changNum,mc));
							averageprice = totalprice.divide(subnum,mc);//库存均价为库存总金额除以数量
						}
					}
					/*CriteriaQuery cde = new CriteriaQuery(StockIODetailEntity.class);
					cde.eq("productid", stockproduct);
					cde.createAlias("stockorderid", "stockio");
					cde.add(Restrictions.eq("stockio.stockid", stock));
					
					List<StockIODetailEntity> detaillist = getListByCriteriaQuery(cde, false);
						
					for( int i = 0; i < detaillist.size(); i++ ){
						StockIODetailEntity detail = detaillist.get(i);
						if( "I".equals(detail.getStockorderid().getStocktypeid().getTypecode()) ){
							inTotalNum = inTotalNum.add(detail.getIostocknum());
							inTotalPrice = inTotalPrice.add(detail.getTotalprice());
						}else if( "O".equals(detail.getStockorderid().getStocktypeid().getTypecode())){
							outTotalNum = outTotalNum.add(detail.getIostocknum());
						}
					}
					
					MathContext mc = new MathContext(4, RoundingMode.HALF_DOWN);
					averageprice = inTotalPrice.divide(inTotalNum,mc);
					totalprice = inTotalPrice.subtract(averageprice.multiply(outTotalNum));
					if( subnum.compareTo(inTotalNum.subtract(outTotalNum)) != 0 ){
						subnum = inTotalNum.subtract(outTotalNum);
					}*/
				}else{
					averageprice = stockProductEntity.getAverageprice();
					totalprice = subnum.multiply(averageprice);
					if(totalprice.compareTo(new BigDecimal(0)) <=0){
						averageprice = new BigDecimal(0);
					}
				}
				
//				stockProductEntity.setAlertnum(new BigDecimal(0.00));
				stockProductEntity.setCompanyid(stockproduct.getCompany());
				stockProductEntity.setProductid(stockproduct);
				stockProductEntity.setStatus(status);
				stockProductEntity.setStocknum(subnum);
				stockProductEntity.setAverageprice(averageprice);
				stockProductEntity.setTotalprice(totalprice);
				
				updateEntitie(stockProductEntity);
			}else{
				stockProductEntity = new StockProductEntity();
				
				if(null != stock && StringUtil.isNotEmpty(stock.getId())){
					stockProductEntity.setAlertnum(new BigDecimal(0.00));
					stockProductEntity.setCompanyid(stockproduct.getCompany());
					stockProductEntity.setProductid(stockproduct);
					stockProductEntity.setStockid(stock);
					stockProductEntity.setStocknum(changNum);
					
					String status = "1";
					
					BigDecimal subnum = changNum;
					if("I".equals(type)){
						subnum = stockProductEntity.getStocknum().add(changNum);
					}else if( "O".equals(type) ){
						subnum = stockProductEntity.getStocknum().subtract(changNum);
					}
					if(subnum.compareTo(new BigDecimal(0.00)) > 0 && subnum.compareTo(stockProductEntity.getAlertnum()) <= 0){
						status = "2";
					}
					if(subnum.compareTo(new BigDecimal(0.00)) > 0 && subnum.compareTo(stockProductEntity.getAlertnum()) > 0){
						status = "1";
					}
					if(subnum.compareTo(new BigDecimal(0.00)) <= 0){
						status = "3";
					}
					
					MathContext mc = new MathContext(4, RoundingMode.HALF_DOWN);
					BigDecimal totalprice = stockProductEntity.getTotalprice();
					if( null == totalprice ){
						totalprice = new BigDecimal(0.00);
					}
					BigDecimal averageprice = stockProductEntity.getAverageprice();
					if( null == averageprice ){
						averageprice = totalprice.divide(subnum,mc);
					}
					//计算库存总价和均价
					/*BigDecimal inTotalNum = new BigDecimal(0);
					BigDecimal inTotalPrice = new BigDecimal(0);
					BigDecimal outTotalNum = new BigDecimal(0);*/
					
					if(!"CK".equals(type)){
						//计算库存总价和均价
						if("I".equals(type)){
							totalprice = totalprice.add(detailtotalprice);//原来的库存总金额加上后面的库存总金额
							averageprice = totalprice.divide(subnum,mc);//库存均价为库存总金额除以数量
						}else if( "O".equals(type) ){
							if( subnum.compareTo(new BigDecimal(0.00)) == 0 ){
								totalprice = new BigDecimal(0.00);
								averageprice = new BigDecimal(0.00);
							}else{
								totalprice = totalprice.subtract( averageprice.multiply(changNum,mc));
								averageprice = totalprice.divide(subnum,mc);//库存均价为库存总金额除以数量
							}
						}
						/*CriteriaQuery cde = new CriteriaQuery(StockIODetailEntity.class);
						cde.eq("productid", stockproduct);
						cde.createAlias("stockorderid", "stockio");
						cde.add(Restrictions.eq("stockio.stockid", stock));
						
						List<StockIODetailEntity> detaillist = getListByCriteriaQuery(cde, false);
							
						for( int i = 0; i < detaillist.size(); i++ ){
							StockIODetailEntity detail = detaillist.get(i);
							if( "I".equals(detail.getStockorderid().getStocktypeid().getTypecode()) ){
								inTotalNum = inTotalNum.add(detail.getIostocknum());
								inTotalPrice = inTotalPrice.add(detail.getTotalprice());
							}else if( "O".equals(detail.getStockorderid().getStocktypeid().getTypecode())){
								outTotalNum = outTotalNum.add(detail.getIostocknum());
							}
						}
						
						MathContext mc = new MathContext(4, RoundingMode.HALF_DOWN);
						averageprice = inTotalPrice.divide(inTotalNum,mc);
						totalprice = inTotalPrice.subtract(averageprice.multiply(outTotalNum));
						if( subnum.compareTo(inTotalNum.subtract(outTotalNum)) != 0 ){
							subnum = inTotalNum.subtract(outTotalNum);
						}*/
					}else{
						averageprice = stockProductEntity.getAverageprice();
						totalprice = subnum.multiply(averageprice);
						if(totalprice.compareTo(new BigDecimal(0)) <=0){
							averageprice = new BigDecimal(0);
						}
					}
					
					stockProductEntity.setStatus(status);
					stockProductEntity.setAverageprice(averageprice);
					stockProductEntity.setTotalprice(totalprice);
					
					save(stockProductEntity);
					
				}else{
					rlt = false;
					throw new Exception("新添库存商品仓库不能为空！");
				}
			}
		}
		
		return rlt;
	}
	
}