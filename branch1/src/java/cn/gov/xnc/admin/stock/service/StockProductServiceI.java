package cn.gov.xnc.admin.stock.service;

import java.math.BigDecimal;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockProductServiceI extends CommonService{

	/**
	 * 添加或更新库存商品信息
	 * @param stockproduct 必填
	 * @param stock 新增库存必填
	 * @param changNum 必填
	 * @return
	 * @throws Exception
	 */
	public boolean saveOrUpdateStockProduct(ProductEntity stockproduct,String type, StockEntity stock, BigDecimal changNum, BigDecimal baseunitprice, BigDecimal detailtotalprice)throws Exception;
}
