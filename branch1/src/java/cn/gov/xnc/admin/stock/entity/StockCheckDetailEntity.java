package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 盘点库存产品详细
 * @author zero
 * @date 2017-01-23 15:09:54
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_check_detail", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockCheckDetailEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**盘点的仓库产品*/
	private StockProductEntity stockproductid;
	/**当前库存*/
	private BigDecimal currentstocknum;
	/**盘点库存*/
	private BigDecimal checkstocknum;
	/**需要调整库存数量差*/
	private BigDecimal adjuststocknum;
	/**备注*/
	private java.lang.String remark;
	/**盘点单*/
	private StockCheckEntity checkorderid;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  盘点的仓库产品
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKPRODUCTID")
	public StockProductEntity getStockproductid(){
		return this.stockproductid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  盘点的仓库产品
	 */
	public void setStockproductid(StockProductEntity stockproductid){
		this.stockproductid = stockproductid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  当前库存
	 */
	@Column(name ="CURRENTSTOCKNUM",nullable=false,precision=10,scale=0)
	public BigDecimal getCurrentstocknum(){
		return this.currentstocknum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  当前库存
	 */
	public void setCurrentstocknum(BigDecimal currentstocknum){
		this.currentstocknum = currentstocknum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  盘点库存
	 */
	@Column(name ="CHECKSTOCKNUM",nullable=false,precision=10,scale=0)
	public BigDecimal getCheckstocknum(){
		return this.checkstocknum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  盘点库存
	 */
	public void setCheckstocknum(BigDecimal checkstocknum){
		this.checkstocknum = checkstocknum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  需要调整库存数量差
	 */
	@Column(name ="ADJUSTSTOCKNUM",nullable=false,precision=10,scale=0)
	public BigDecimal getAdjuststocknum(){
		return this.adjuststocknum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  需要调整库存数量差
	 */
	public void setAdjuststocknum(BigDecimal adjuststocknum){
		this.adjuststocknum = adjuststocknum;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=4000)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  入库单编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHECKORDERID")
	public StockCheckEntity getCheckorderid() {
		return checkorderid;
	}

	public void setCheckorderid(StockCheckEntity checkorderid) {
		this.checkorderid = checkorderid;
	}
}
