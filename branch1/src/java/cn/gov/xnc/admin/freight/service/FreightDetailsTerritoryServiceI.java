package cn.gov.xnc.admin.freight.service;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightDetailsTerritoryEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface FreightDetailsTerritoryServiceI extends CommonService{
	
	/**
	 * 构建运费区域数据，记录区域信息
	 * @param freightDetail
	 * @param req
	 * @return
	 */
	public FreightDetailsTerritoryEntity buildFreightTerritory(final FreightDetailsEntity freightDetail, String district, HttpServletRequest req);

}
