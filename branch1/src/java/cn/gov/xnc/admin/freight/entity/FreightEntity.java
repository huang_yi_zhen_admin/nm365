package cn.gov.xnc.admin.freight.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;



/**   
 * @Title: Entity
 * @Description: 运费基础信息
 * @author zero
 * @date 2017-02-03 10:10:38
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_freight", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class FreightEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**模板名称*/
	private java.lang.String name;
	/**快递公司*/
	private java.lang.String freightname;
	/**计量类型 1件*/
	private java.lang.String type;
	/**状态 1 有效 2 失效*/
	private java.lang.String state;
	/**首重量*/
	private int freightunit;
	/**首重价格*/
	private BigDecimal freightpic;
	/**续重价格*/
	private BigDecimal freightmorepic;
	/**续重量*/
	private int freightmoreunit;
	/**公司信息*/
	private TSCompany company;
	/**创建时间*/
	private java.util.Date createdate;
	/**更新时间*/
	private java.util.Date updatedate;
	
	
	/**运费信息*/
	private List<FreightDetailsEntity> freightDetailsList = new ArrayList<FreightDetailsEntity>();
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  模板名称
	 */
	@Column(name ="NAME",nullable=false,length=500)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  模板名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递公司
	 */
	@Column(name ="FREIGHTNAME",nullable=true,length=500)
	public java.lang.String getFreightname(){
		return this.freightname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递公司
	 */
	public void setFreightname(java.lang.String freightname){
		this.freightname = freightname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  计量类型 1件
	 */
	@Column(name ="TYPE",nullable=false,length=2)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  计量类型 1件
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态
	 */
	@Column(name ="STATE",nullable=false,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  首总量
	 */
	@Column(name ="FREIGHTUNIT",nullable=true,length=2)
	public int getFreightunit(){
		return this.freightunit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  首总量
	 */
	public void setFreightunit(int freightunit){
		this.freightunit = freightunit;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  首重价格
	 */
	@Column(name ="FREIGHTPIC",nullable=true,precision=10,scale=2)
	public BigDecimal getFreightpic(){
		return this.freightpic;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  首重价格
	 */
	public void setFreightpic(BigDecimal freightpic){
		this.freightpic = freightpic;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  续重价格
	 */
	@Column(name ="FREIGHTMOREPIC",nullable=true,precision=10,scale=2)
	public BigDecimal getFreightmorepic(){
		return this.freightmorepic;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  续重价格
	 */
	public void setFreightmorepic(BigDecimal freightmorepic){
		this.freightmorepic = freightmorepic;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  续重单位
	 */
	@Column(name ="FREIGHTMOREUNIT",nullable=true,length=2)
	public int getFreightmoreunit(){
		return this.freightmoreunit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  续重单位
	 */
	public void setFreightmoreunit(int freightmoreunit){
		this.freightmoreunit = freightmoreunit;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany(){
		return this.company;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company){
		this.company = company;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}
	
	/**运费信息*/
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "freightid")
	public List<FreightDetailsEntity> getFreightDetailsList() {
		return freightDetailsList;
	}

	public void setFreightDetailsList(List<FreightDetailsEntity> freightDetailsList) {
		this.freightDetailsList = freightDetailsList;
	}
	
	
}
