package cn.gov.xnc.admin.freight.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;
import cn.gov.xnc.admin.freight.service.FreightServiceI;

/**   
 * @Title: Controller
 * @Description: 运费基础信息
 * @author zero
 * @date 2017-02-03 10:10:38
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/freightController")
public class FreightController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(FreightController.class);

	@Autowired
	private FreightServiceI freightService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private FreightDetailsServiceI freightDetailsService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 运费基础信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView freight(HttpServletRequest request) {
		return new ModelAndView("admin/freight/freightList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(FreightEntity freight,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(FreightEntity.class, dataGrid);
		if(StringUtil.isNotEmpty(freight.getName())){
			cq.like("name", "%"+freight.getName()+"%");
			freight.setName(null);
		}
		cq.eq("state", "1");
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, freight, request.getParameterMap());
		this.freightService.getDataGridReturn(cq, true);
		TagUtil.datagridFreight(response, dataGrid);
	}

	/**
	 * 删除运费基础信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(FreightEntity freight, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		freight = systemService.getEntity(FreightEntity.class, freight.getId());
		freight.setState("2");
		freightService.updateEntitie( freight );
		
		message = "运费基础信息删除成功";
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加运费基础信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(FreightEntity freight, HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(freight.getId())) {
			message = "运费基础信息更新成功";
			FreightEntity t = freightService.get(FreightEntity.class, freight.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(freight, t);
				freightService.saveOrUpdate(t);
				
				freightDetailsService.batchUpdatefreightDetails(t, request);
				
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "运费基础信息更新失败";
				j.setSuccess(false);
			}
		} else {
			try {
				message = "运费基础信息添加成功";
				TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
				//创建基础数据
				freight.setId(IdWorker.generateSequenceNo());
				freight.setCompany(company);
				freight.setCreatedate(DateUtils.getDate());
				freight.setState("1");
				freight.setType("1");
				
				//创建运费地区数据
				List<FreightDetailsEntity> freightDetailList = freightDetailsService.buildFreightDetail(freight, request);
				if(freightDetailList.size() > 0){
					freight.setFreightDetailsList(freightDetailList);
				}else{
					freight.setFreightDetailsList(null);
				}
				String expLen = ResourceUtil.getParameter("expLen");
				int freightDetailsNum = Integer.parseInt(expLen);
				int warnNum = freightDetailsNum - freightDetailList.size();
				if(warnNum > 0){
					message = message +"，但有" + warnNum + "条区域模板数据没有填写完善，请检查！";
				}
				
				freightService.save(freight);
				
			} catch (Exception e) {
				message = "运费基础信息添加失败！";
				j.setSuccess(false);
				message = "运费基础信息添加失败:" + e;
			}
			
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 运费基础信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(FreightEntity freight, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(freight.getId())) {
			freight = freightService.getEntity(FreightEntity.class, freight.getId());
		}
		req.setAttribute("freightPage", freight);
		return new ModelAndView("admin/freight/freight");
	}
	
	@RequestMapping(value = "choiceFreight")
	public ModelAndView choiceFreight(FreightEntity freight, HttpServletRequest req) {
		return new ModelAndView("admin/product/productfreight");
	}
	
	@RequestMapping(value = "freightDatagrid")
	public void freightDatagrid(FreightEntity freight,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(FreightEntity.class, dataGrid);
		cq.eq("state", "1");
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, freight, request.getParameterMap());
		this.freightService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 运费基础信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "getFreightDetailJson")
	@ResponseBody
	public AjaxJson getFreightDetailJson(FreightEntity freight, HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		j.setSuccess(false);
		
		if (StringUtil.isNotEmpty(freight.getId())) {
			try {
				freight = freightService.findUniqueByProperty(FreightEntity.class, "id", freight.getId());
				j.setObj(freightDetailsService.freightDetail2jsonStr(freight.getFreightDetailsList(), req));
				j.setSuccess(true);
				j.setMsg("获取区域运费成功！");
			} catch (Exception e) {
				j.setSuccess(false);
			}
		}else{
			j.setMsg("获取区域运费失败，缺少运费id");
		}
		
		return j;
	}
}
