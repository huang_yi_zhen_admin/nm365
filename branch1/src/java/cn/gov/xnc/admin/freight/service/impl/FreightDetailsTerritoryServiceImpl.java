package cn.gov.xnc.admin.freight.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightDetailsTerritoryEntity;
import cn.gov.xnc.admin.freight.service.FreightDetailsTerritoryServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("freightDetailsTerritoryService")
@Transactional
public class FreightDetailsTerritoryServiceImpl extends CommonServiceImpl implements FreightDetailsTerritoryServiceI {

	@Autowired
	SystemService systemService;
	
	@Override
	public FreightDetailsTerritoryEntity buildFreightTerritory(final FreightDetailsEntity freightDetail, String district, HttpServletRequest req) {
//		TSTerritory tSTerritory = systemService.findUniqueByProperty(TSTerritory.class, "id", district.split(":")[0]);
		
		FreightDetailsTerritoryEntity  freightDetailsTerritory = new FreightDetailsTerritoryEntity();
		freightDetailsTerritory.setId(IdWorker.generateSequenceNo());
		freightDetailsTerritory.setFreighdetailsid(freightDetail);
		freightDetailsTerritory.setTerritoryid(district.split(":")[0]);
		freightDetailsTerritory.setTerritoryname(district.split(":")[1]);
		
		return freightDetailsTerritory;
	}
	
}