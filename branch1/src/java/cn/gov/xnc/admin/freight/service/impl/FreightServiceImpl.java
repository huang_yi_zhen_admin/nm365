package cn.gov.xnc.admin.freight.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.freight.service.FreightServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("freightService")
@Transactional
public class FreightServiceImpl extends CommonServiceImpl implements FreightServiceI {
	
}