update xnc_product set unitbase = unit

ALTER TABLE xnc_pay_bills_order
ADD baseCost decimal(12,2) default 0.00 COMMENT "退单总成本";

ALTER TABLE xnc_product
ADD clientFilter varchar(4000) default NULL COMMENT "产品对特定客户可见,填写可以看到商品的采购商ID,多个ID用英文半角逗号隔开";

ALTER TABLE xnc_product
ADD topshow varchar(2) default 'N' NOT NULL COMMENT "商品是否推荐至首页 : Y 是 N 否";

ALTER TABLE xnc_product
ADD saleunit varchar(2) default '1' NOT NULL COMMENT "销售单位 : 1 基础单位 2 辅助单位 3 多单位";

ALTER TABLE xnc_product
ADD priceA varchar(2) default NULL COMMENT "辅助单位价格";


ALTER TABLE xnc_advertising_position
ADD adtype varchar(2) default '1' NOT NULL COMMENT "广告类型 : 1 轮播图 2 顶部横栏 3 底部横栏";

ALTER TABLE xnc_advertising_position
ADD status varchar(2) default 'U' NOT NULL COMMENT "广告状态 : U 使用 D 删除";


ALTER TABLE xnc_user_basket
ADD unitid varchar(32) NOT NULL COMMENT "采购单位ID";

ALTER TABLE xnc_user_basket
ADD specid varchar(32) NOT NULL COMMENT "采购规格ID";

ALTER TABLE xnc_user_basket
modify amount decimal(12,2);


ALTER TABLE xnc_order
ADD specid varchar(32) COMMENT "采购多规格单位";

ALTER TABLE xnc_order
ADD specname varchar(255) COMMENT "采购多规格单位名称";



#存储过程-同步购物车商品id
BEGIN
	DECLARE v_pid VARCHAR(32);
	DEclare v_bid varchar(32);
	DEclare v_uid varchar(32);
	DECLARE v_count INT DEFAULT 1;

	BEGIN
		DECLARE cur1 CURSOR FOR

			select t.id, t.productid from xnc_user_basket t ;

		DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_count = 0;
		
		OPEN cur1;

		loop_lable: LOOP

			FETCH cur1 INTO v_bid,v_pid;
			
			IF v_count=0 THEN   
				LEAVE loop_lable;   
			END IF;

select p.unitbase into v_uid from xnc_product p where p.id = v_pid;
		

			IF v_uid is not null THEN
				 
				 update xnc_user_basket set unitid = v_uid where id = v_bid;
				 COMMIT;
				 set v_uid = NULL;
			END IF;

		END LOOP loop_lable;


		CLOSE cur1;
		
	END;
END

#创建多规格表
CREATE TABLE `xnc_product_spec_price` (
`id`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '多规格编码' ,
`productid`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商品编码' ,
`specName`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '多规格名称' ,
`unitid`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '多规格组合单位' ,
`orderPrice`  decimal(12,2) NOT NULL DEFAULT 0.00 COMMENT '采购价格' ,
`normalPrice`  decimal(12,2) NULL DEFAULT 0.00 COMMENT '市场价格' ,
`costprice`  decimal(12,2) NULL DEFAULT 0.00 ,
`stockNum`  decimal(12,2) NULL DEFAULT NULL COMMENT '虚拟库存数量' ,
`companyid`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属公司' ,
`status`  varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'U' COMMENT '状态：U 正在使用 D 删除' ,
`updateTime`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`),
INDEX `idx_pid` (`productid`) USING BTREE ,
INDEX `idx_pid_uid` (`productid`, `unitid`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
COMMENT='商品价格信息对应表，兼顾多规格和多单位'
ROW_FORMAT=DYNAMIC
;

#旧商品数据生成规格价格信息，规格id采用默认采用商品id
BEGIN
	DECLARE v_pid VARCHAR(32);
DEclare v_pname varchar(1000);
	DEclare v_bid varchar(32);
	DEclare v_orderprice DECIMAL(12,2);
	DEclare v_normalprice DECIMAL(12,2);
	DEclare v_uname varchar(1000);
  DEclare v_company varchar(32);
	DECLARE v_count INT DEFAULT 1;
DECLARE v_tag INT DEFAULT 0;

	BEGIN
		DECLARE cur1 CURSOR FOR

			select t.id,t.`name`, t.unitbase, t.Price, IFNULL(t.priceS,0.00), t.company from xnc_product t where t.Price is not null and t.unitbase is not null;

		DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_count = 0;
		
		OPEN cur1;

		loop_lable: LOOP

			FETCH cur1 INTO v_pid,v_pname,v_bid,v_orderprice,v_normalprice,v_company;
			
			IF v_count=0 THEN   
				LEAVE loop_lable;   
			END IF;

			select count(1) into v_tag from xnc_product_spec_price tt where tt.id = v_pid;
			
			if v_tag = 0 THEN
				select u.`name` into v_uname from xnc_product_unit u where u.id = v_bid;
				 
				INSERT INTO xnc_product_spec_price (

				id,

				productid,

				specName,

				unitid,

				orderPrice,

				normalPrice,

				companyid

			) 

			VALUES

				(

					v_pid,

					v_pid,

					CONCAT(v_pname,'-',v_uname),

					v_bid,

					v_orderprice,

					v_normalprice,

					v_company

				) ;

				COMMIT;
			end if;
			

		END LOOP loop_lable;


		CLOSE cur1;
		
	END;
END



#同步购物车内原有商品，使其满足规格功能
update xnc_user_basket set specid = productid;