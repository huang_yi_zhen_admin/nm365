import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as mutations from './mutations'
Vue.use(Vuex)

const store = new Vuex.Store({
    actions,
    mutations,

    state: {
        webSite: [],
        clientList: { //客户信息列表
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'client',
            data: []
        },
        orderList: { //大单信息列表
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'order',
            data: []
        },
        goodsList: { //大单信息列表
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'goods',
            data: []
        },
        salersAudit_0: { //业务审核-全部
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        salersAudit_1: { //业务审核-待审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        salersAudit_2: { //业务审核-已审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        moneyAudit_0: { //提现审核-全部
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        moneyAudit_1: { //提现审核-待审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        moneyAudit_2: { //提现审核-已审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        "payAudit_1,2,3": { //支付审核-全部
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        payAudit_2: { //支付审核-待审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        payAudit_1: { // 支付审核-已审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        "orderAudit_1,2,3": { //发货结算审核-全部
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        orderAudit_2: { //发货结算审核-待审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        orderAudit_1: { // 发货结算审核-已审核
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        "cashList_2": { //资金流水-已付款
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        'cashList_1,2,3': { //发货结算审核-应付款
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        'cashList_1,3': { //发货结算审核-待付款
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        'payBillList': { //收支明细-预存款
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cash',
            data: []
        },
        proviceData: [],
        searchData: {
            goodsList: {},
            orderList: {},
            clientList: {}
        },
        power: { //权限
            'waitCheck': 1, //是否在等待校验接口，校验之后，设置为0
            'clientStat': '8af4a3da58210ee5015821117f030008',
            'saleStat': '8af4a3da58210ee501582110a8360004',
            'goodsStat': '8af4a3da58210ee50158211120d20006',
            'salerStat': '4028802c5779096b01577915ff9d0012',
            'orderList': '402881e75797ce56015797d465320004',
            'goodsList': '8af4a3da58aa50a10158b34945f00001',
            'clientList': '402881e757673e3f01576740023e0004',
            'cashStat': '402881e757741c66015775640997000b',
            'auditStat': '402881e757741c6601577567b90b0019',
            'myCountStat': '4028802f58146bd5015814935fa3001f',
            'myInfoStat': '402881e75762c77a015762ccd62e000b'
        }


    },
    getters: {

        webSite(state) {
            console.log(state.webSite)
            return state.webSite;
        },
        userPower(state) {
            return state.power;
        }
        /*listDate(state, { listName }) {
            console.log(listName)
            return state[listName].data;
        }*/

    }
})

export default store