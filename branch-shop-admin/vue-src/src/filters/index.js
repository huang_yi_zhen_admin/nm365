export function idStatus(id) {
    let map = { 1: "启用", 2: "冻结", 3: "过期", 4: "待审核" };
    return map[id] || id;
}
export function payType(id) {
    let map = { 1: "现款现货", 2: "预付", 3: "先货后款" };
    return map[id] || id;
}
export function payState(id) {
    let map = { 1: "待付", 2: "已付", 3: "未付清", 4: "已取消", 5: "其他" };
    return map[id] || id
}
export function salerPayAuditState(id) {
    let map = { 1: "申请", 2: "已通过", 3: "未付清", 4: "已取消" };
    return map[id] || id;
}
export function payMathod(id) {
    let map = { 1: "支付宝", 2: "微信支付", 3: "线下转账", 4: "预存款支付", 5: "银联支付", 6: "先货后款" }
    return map[id] || id
}
export function expStatus(id) {
    let map = { 1: "未发货", 2: "已发货", 3: "部分发货" };
    return map[id] || id;
}
export function orderType(id) {
    let map = { 1: "销售", 2: "充值" };
    return map[id] || id;
}
export function cashType(id) {
    let map = { 1: "支付", 2: "充值" };
    return map[id] || id;
}
export function orderSendType(id) {
    let map = { "N": "否", "Y": "是" };
    return map[id] || id;
}
export function goodsOrderState(id) {
    let map = { 1: "待付款", 2: "待发货", 3: "已发货", 4: "已签收", 5: "已取消", 6: "待审核", 7: "未通过" };
    return map[id] || id;
}
export function payAuditState(id) {
    let map = { 1: "审核通过", 2: "待审核", 3: "审核未通过", 4: "系统确认", 5: "支付失败" };
    return map[id] || id;
}
export function withdrawOrderState(id) {
    let map = { 1: "取消结算", 2: "待结算", 7: "通过结算" };
    return map[id] || id
}
export function shareprice(id) {
    let map = { 1: "是", 0: "否" };
    return map[id] || id
}
Date.prototype.ZK_format = function() {
    var arg = arguments;
    if (arg.length == 1 && typeof arg[0] == 'string') {
        var str = arg[0];
        var reg = /(yyyy|yy|mm|m|dd|d|hh|h|MM|M|ss|s|w|W)/gi;
        var date = this;
        var d = {
            yyyy: date.getFullYear(),
            yy: date.getFullYear().toString().match(/\d{2}$/),
            mm: (date.getMonth() + 1) < 10 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1),
            m: (date.getMonth() + 1),
            dd: date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate(),
            d: date.getDate(),
            hh: date.getHours() < 10 ? ('0' + date.getHours()) : date.getHours(),
            h: date.getHours(),
            MM: date.getMinutes() < 10 ? ('0' + date.getMinutes()) : date.getMinutes(),
            M: date.getMinutes(),
            ss: date.getSeconds() < 10 ? ('0' + date.getSeconds()) : date.getSeconds(),
            s: date.getSeconds(),
            w: date.getDay() + 1,
            W: ['日', '一', '二', '三', '四', '五', '六'][date.getDay()]
        };
        str = str.replace(reg, function() {
            return d[arguments[1]];
        });
        return str;
    }
};
export function time(str) {
    if (!str) {
        return "";
    }
    if (isNaN(str) && str) {

        return String(str).substr(0, 19);
    } else {
        let date = new Date();
        date.setTime(str);
        return date.ZK_format("yyyy-mm-dd hh:MM:ss");
    }
}
export function imgPath(src) {
    return 'http://www.g.share.static.nongmao365.com/' + src;
}