import Vue from 'vue'
import Router from 'vue-router'
import OrderView from '@/views/orderView'
import DetailView from '@/views/detailView'
import HomeView from '@/views/homeView'

Vue.use(Router)

const scrollBehavior = (to, from, savedPosition) => {
    if (savedPosition) {
        // savedPosition is only available for popstate navigations.
        return savedPosition
    } else {
        const position = {}
            // new navigation.
            // scroll to anchor by returning the selector
        if (to.hash) {
            position.selector = to.hash
        }
        // check if any matched route config has meta that requires scrolling to top
        if (to.matched.some(m => m.meta.scrollToTop)) {
            // cords will be used if no selector is provided,
            // or if the selector didn't match any element.
            position.x = 0
            position.y = 0
        }
        // if the returned position is falsy or an empty object,
        // will retain current scroll position.)

        return position
    }
}

export default new Router({
    mode: 'history',
    //saveScrollPosition: true,
    scrollBehavior,
    routes: [{
            path: '/m/home',
            name: 'home',
            component: HomeView
        }, {
            path: '/m/order',
            name: 'order',
            component: OrderView,
            children: [{
                    path: 'orderlist',
                    name: "orderList",
                    component: resolve => require(['../views/orders/orderList.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'goodslist',
                    name: "goodsList",
                    component: resolve => require(['../views/orders/goodsList.vue'], resolve),
                    meta: { scrollToTop: true }
                }
            ]
        }, {
            path: '/m/detail',
            name: 'detail',
            component: DetailView,
            children: [{
                    path: 'orderDetail',
                    title: "订单详情",
                    name: "orderDetail",
                    component: resolve => require(['../views/details/order.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'goodsDetail',
                    title: "商品订单详情",
                    name: "goodsDetail",
                    component: resolve => require(['../views/details/goods.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'expDetail',
                    title: "物流详情",
                    name: "expDetail",
                    component: resolve => require(['../views/details/express.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'clientDetail',
                    title: "客户详情",
                    name: "clientDetail",
                    component: resolve => require(['../views/clients/clientDetail.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'salersAuditDetail',
                    title: "业务审核详情",
                    name: "salersAuditDetail",
                    component: resolve => require(['../views/audit/salersAuditDetail.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'payAuditDetail',
                    title: "收支审核详情",
                    name: "payAuditDetail",
                    component: resolve => require(['../views/audit/payAuditDetail.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'moneyAuditDetail',
                    title: "提现审核详情",
                    name: "moneyAuditDetail",
                    component: resolve => require(['../views/audit/moneyAuditDetail.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'orderAuditDetail',
                    title: "结算审核详情",
                    name: "orderAuditDetail",
                    component: resolve => require(['../views/audit/orderAuditDetail.vue'], resolve),
                    meta: { scrollToTop: true }
                }
            ]
        }, {
            path: '/m/client',
            name: 'client',
            component: resolve => require(['../views/clientView.vue'], resolve)
        },
        {
            path: '/m/cash',
            name: 'cash',
            component: resolve => require(['../views/cashView.vue'], resolve)
        },
        {
            path: '/m/cashList',
            name: 'cashList',
            component: resolve => require(['../views/cash/cashList.vue'], resolve)
        },
        {
            path: '/m/payBillList',
            name: 'payBillList',
            component: resolve => require(['../views/cash/payBillList.vue'], resolve)
        },
        {
            path: '/m/audit',
            name: 'audit',
            component: resolve => require(['../views/audit/auditList.vue'], resolve),
            children: [{
                    path: 'salers',
                    title: "业务审核",
                    name: "salersAudit",
                    component: resolve => require(['../views/audit/salersAudit.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'pay',
                    title: "收支审核",
                    name: "payAudit",
                    component: resolve => require(['../views/audit/payAudit.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'money',
                    title: "提现审核",
                    name: "moneyAudit",
                    component: resolve => require(['../views/audit/moneyAudit.vue'], resolve),
                    meta: { scrollToTop: true }
                },
                {
                    path: 'order',
                    title: "结算审核",
                    name: "orderAudit",
                    component: resolve => require(['../views/audit/orderAudit.vue'], resolve),
                    meta: { scrollToTop: true }
                }
            ]

        },
        {
            path: '/m/my',
            name: 'myView',
            component: resolve => require(['../views/myView.vue'], resolve),
            meta: { scrollToTop: true }

        },
        {
            path: '/m/my/resetPwd',
            name: 'resetPwd',
            component: resolve => require(['../views/my/resetPwd.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m/my/companyInfo',
            name: 'companyInfo',
            component: resolve => require(['../views/my/companyInfo.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m/my/websiteInfo',
            name: 'websiteInfo',
            component: resolve => require(['../views/my/websiteInfo.vue'], resolve),
            meta: { scrollToTop: true }
        },
        {
            path: '/m',
            redirect: '/m/home'
        }
    ]
})