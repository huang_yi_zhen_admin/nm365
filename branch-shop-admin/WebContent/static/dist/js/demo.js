/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */
(function ($, AdminLTE) {

  "use strict";

  /**
   * List of all the available skins
   *
   * @type Array
   */
  var my_skins = [
    "skin-blue",
    "skin-black",
    "skin-red",
    "skin-yellow",
    "skin-purple",
    "skin-green",
    "skin-blue-light",
    "skin-black-light",
    "skin-red-light",
    "skin-yellow-light",
    "skin-purple-light",
    "skin-green-light"
  ];

  //Create the new tab
  var tab_pane = $("<div />", {
    "id": "control-sidebar-theme-demo-options-tab",
    "class": "tab-pane active"
  });

  //Create the tab button
  var tab_button = $("<li />", {"class": "active"})
      .html("<a href='#control-sidebar-theme-demo-options-tab' data-toggle='tab'>"
      + "<i class='fa fa-wrench'></i>"
      + "</a>");

  //Add the tab button to the right sidebar tabs
  $("[href='#control-sidebar-home-tab']")
      .parent()
      .before(tab_button);

  //Create the menu
  var demo_settings = $("<div />");

  //Layout options
  demo_settings.append(
      "<h4 class='control-sidebar-heading'>"
      + "Layout Options"
      + "</h4>"
        //Fixed layout
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-layout='fixed' class='pull-right'/> "
      + "Fixed layout"
      + "</label>"
      + "<p>Activate the fixed layout. You can't use fixed and boxed layouts together</p>"
      + "</div>"
        //Boxed layout
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-layout='layout-boxed'class='pull-right'/> "
      + "Boxed Layout"
      + "</label>"
      + "<p>Activate the boxed layout</p>"
      + "</div>"
        //Sidebar Toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-layout='sidebar-collapse' class='pull-right'/> "
      + "Toggle Sidebar"
      + "</label>"
      + "<p>Toggle the left sidebar's state (open or collapse)</p>"
      + "</div>"
        //Sidebar mini expand on hover toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-enable='expandOnHover' class='pull-right'/> "
      + "Sidebar Expand on Hover"
      + "</label>"
      + "<p>Let the sidebar mini expand on hover</p>"
      + "</div>"
        //Control Sidebar Toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-controlsidebar='control-sidebar-open' class='pull-right'/> "
      + "Toggle Right Sidebar Slide"
      + "</label>"
      + "<p>Toggle between slide over content and push content effects</p>"
      + "</div>"
        //Control Sidebar Skin Toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-sidebarskin='toggle' class='pull-right'/> "
      + "Toggle Right Sidebar Skin"
      + "</label>"
      + "<p>Toggle between dark and light skins for the right sidebar</p>"
      + "</div>"
  );
  var skins_list = $("<ul />", {"class": 'list-unstyled clearfix'});

  //Dark sidebar skins
  var skin_blue =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-blue' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px; background: #367fa9;'></span><span class='bg-light-blue' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Blue</p>");
  skins_list.append(skin_blue);
  var skin_black =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-black' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div style='box-shadow: 0 0 2px rgba(0,0,0,0.1)' class='clearfix'><span style='display:block; width: 20%; float: left; height: 7px; background: #fefefe;'></span><span style='display:block; width: 80%; float: left; height: 7px; background: #fefefe;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Black</p>");
  skins_list.append(skin_black);
  var skin_purple =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-purple' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-purple-active'></span><span class='bg-purple' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Purple</p>");
  skins_list.append(skin_purple);
  var skin_green =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-green' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-green-active'></span><span class='bg-green' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Green</p>");
  skins_list.append(skin_green);
  var skin_red =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-red' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-red-active'></span><span class='bg-red' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Red</p>");
  skins_list.append(skin_red);
  var skin_yellow =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-yellow' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-yellow-active'></span><span class='bg-yellow' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Yellow</p>");
  skins_list.append(skin_yellow);

  //Light sidebar skins
  var skin_blue_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-blue-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px; background: #367fa9;'></span><span class='bg-light-blue' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Blue Light</p>");
  skins_list.append(skin_blue_light);
  var skin_black_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-black-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div style='box-shadow: 0 0 2px rgba(0,0,0,0.1)' class='clearfix'><span style='display:block; width: 20%; float: left; height: 7px; background: #fefefe;'></span><span style='display:block; width: 80%; float: left; height: 7px; background: #fefefe;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Black Light</p>");
  skins_list.append(skin_black_light);
  var skin_purple_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-purple-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-purple-active'></span><span class='bg-purple' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Purple Light</p>");
  skins_list.append(skin_purple_light);
  var skin_green_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-green-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-green-active'></span><span class='bg-green' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Green Light</p>");
  skins_list.append(skin_green_light);
  var skin_red_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-red-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-red-active'></span><span class='bg-red' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Red Light</p>");
  skins_list.append(skin_red_light);
  var skin_yellow_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-yellow-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-yellow-active'></span><span class='bg-yellow' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px;'>Yellow Light</p>");
  skins_list.append(skin_yellow_light);

  demo_settings.append("<h4 class='control-sidebar-heading'>Skins</h4>");
  demo_settings.append(skins_list);

  tab_pane.append(demo_settings);
  $("#control-sidebar-home-tab").after(tab_pane);

  setup();

  /**
   * Toggles layout classes
   *
   * @param String cls the layout class to toggle
   * @returns void
   */
  function change_layout(cls) {
    $("body").toggleClass(cls);
    AdminLTE.layout.fixSidebar();
    //Fix the problem with right sidebar and layout boxed
    if (cls == "layout-boxed")
      AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
    if ($('body').hasClass('fixed') && cls == 'fixed') {
      AdminLTE.pushMenu.expandOnHover();
      AdminLTE.layout.activate();
    }
    AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
    AdminLTE.controlSidebar._fix($(".control-sidebar"));
  }

  /**
   * Replaces the old skin with the new skin
   * @param String cls the new skin class
   * @returns Boolean false to prevent link's default action
   */
  function change_skin(cls) {
    $.each(my_skins, function (i) {
      $("body").removeClass(my_skins[i]);
    });

    $("body").addClass(cls);
    store('skin', cls);
    return false;
  }

  /**
   * Store a new settings in the browser
   *
   * @param String name Name of the setting
   * @param String val Value of the setting
   * @returns void
   */
  function store(name, val) {
    if (typeof (Storage) !== "undefined") {
      localStorage.setItem(name, val);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
  }

  /**
   * Get a prestored setting
   *
   * @param String name Name of of the setting
   * @returns String The value of the setting | null
   */
  function get(name) {
    if (typeof (Storage) !== "undefined") {
      return localStorage.getItem(name);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
  }

  /**
   * Retrieve default settings and apply them to the template
   *
   * @returns void
   */
  function setup() {
    var tmp = get('skin');
    if (tmp && $.inArray(tmp, my_skins))
      change_skin(tmp);

    //Add the change skin listener
    $("[data-skin]").on('click', function (e) {
      e.preventDefault();
      change_skin($(this).data('skin'));
    });

    //Add the layout manager
    $("[data-layout]").on('click', function () {
      change_layout($(this).data('layout'));
    });

    $("[data-controlsidebar]").on('click', function () {
      change_layout($(this).data('controlsidebar'));
      var slide = !AdminLTE.options.controlSidebarOptions.slide;
      AdminLTE.options.controlSidebarOptions.slide = slide;
      if (!slide)
        $('.control-sidebar').removeClass('control-sidebar-open');
    });

    $("[data-sidebarskin='toggle']").on('click', function () {
      var sidebar = $(".control-sidebar");
      if (sidebar.hasClass("control-sidebar-dark")) {
        sidebar.removeClass("control-sidebar-dark")
        sidebar.addClass("control-sidebar-light")
      } else {
        sidebar.removeClass("control-sidebar-light")
        sidebar.addClass("control-sidebar-dark")
      }
    });

    $("[data-enable='expandOnHover']").on('click', function () {
      $(this).attr('disabled', true);
      AdminLTE.pushMenu.expandOnHover();
      if (!$('body').hasClass('sidebar-collapse'))
        $("[data-layout='sidebar-collapse']").click();
    });

    // Reset options
    if ($('body').hasClass('fixed')) {
      $("[data-layout='fixed']").attr('checked', 'checked');
    }
    if ($('body').hasClass('layout-boxed')) {
      $("[data-layout='layout-boxed']").attr('checked', 'checked');
    }
    if ($('body').hasClass('sidebar-collapse')) {
      $("[data-layout='sidebar-collapse']").attr('checked', 'checked');
    }

  }
})(jQuery, $.AdminLTE);
String.prototype.ZK_toJson = function (lvl_1, lvl_2, fn) {
  fn = $.isFunction(fn)? fn : (function (v) {
      return v;
    });
  lvl_1 = lvl_1 ||"&";
  lvl_2 = lvl_2 ||"=";
  var newar = this.split(lvl_1);
  var ar = [];
  for (var i = 0, l = newar.length; i < l; i++) {
    if (newar[i].trim() != '');
    ar.push(newar[i]);
  }
  var r = {};
  $.each(ar, function (i,n) {
    var a = n.split(lvl_2);
    if (a[1] != undefined)
      r[a[0]] = fn(a[1]);
  });
  return r;
};

function getHrefParam(str){
    var url = str || location.href,pos = url.indexOf('?'),pos2=url.indexOf('#');
    if(pos2!=-1){
      url= url.substring(0,pos2);
    }
    return url.substring(pos+1).ZK_toJson('&', '=', function (v) {
      try {
        return decodeURIComponent(v);
      } catch (e) {}
    
    });
}
String.prototype.ZK_trim = function (r) {
  r = r || /(^\s+)|(\s+$)/g;
  return this.replace(r, "");
};

Date.prototype.ZK_format = function () {
  var arg = arguments;
  if (arg.length == 1 && typeof arg[0] == 'string') {
    var str = arg[0];
    var reg = /(yyyy|yy|mm|m|dd|d|hh|h|MM|M|ss|s|w|W)/gi;
    var date = this;
    var d = {
      yyyy : date.getFullYear(),
      yy : date.getFullYear().toString().match(/\d{2}$/),
      mm : (date.getMonth() + 1) < 10 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1),
      m : (date.getMonth() + 1),
      dd : date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate(),
      d : date.getDate(),
      hh : date.getHours() < 10 ? ('0' + date.getHours()) : date.getHours(),
      h : date.getHours(),
      MM : date.getMinutes() < 10 ? ('0' + date.getMinutes()) : date.getMinutes(),
      M : date.getMinutes(),
      ss : date.getSeconds() < 10 ? ('0' + date.getSeconds()) : date.getSeconds(),
      s : date.getSeconds(),
      w:date.getDay()+1,
      W:['日','一','二','三','四','五','六'][date.getDay()]
    };
    str = str.replace(reg, function () {
        return d[arguments[1]];
      });
    return str;
  }
};

/** 
* @memberOf String
* @description String扩展方法：ZK_format 字符串格式化
* @param {Object} 用来格式化字符串的json格式的数据
* @return {String} 格式化之后的字符串
* @see pics <a href="../../demo/prototype.html#format" target="_blank">使用demo</a >. 
*/
String.prototype.ZK_format = function () {
  var a = arguments;
  var data = (a.length == 1 && typeof(a[0]) == 'object') ? a[0] : a;
  return this.replace(/\{([\d\w_]+)\}/g, function (m, n) {
    return data[n] != undefined ? data[n].toString() : m;
  });
};
//通用方法
var Comm=(function($){
  var pub={};

  //iframe pop
  pub.popIfm=function(conf){
    var Conf={
      title:"标题",
      src:"/pub/tabel.html",
      data:{},//链接的参数
      body:"",
      height:"600",
      width:"900",
      okCbk:function(){
        
      },
      okBtnTitle:"确定",//空则取消按钮
      noBtnTitle:"关闭",//空则取消按钮
      showCbk:function(){

      },
      renderCbk:function(){
      },
      noCbk:function(){
      },
      closeCbk:function(s){
      }
    },
    _this=this,
    gName="gridSystemModal",
    nid=0;

    try{
      nid = top.Comm.popIfm.prototype.nid++;
    }catch(e){
      nid=0;

    }
    console.log("nid:"+nid);
    //this.prototype.nid=0;

    $.extend(Conf, conf);

    var test = $("#"+gName+nid);
    if(test.size()){
        nid = parseInt(test.attr("n"),10)+1;
    }


    var popTpl=[
        '<div class="modal fade" tabindex="-1" role="dialog" n="'+nid+'" id="'+gName+nid+'" aria-labelledby="gridSystemModalLabel">',
        '<div class="modal-dialog modal-lg box" role="document" style="width:'+Conf.width+'px;height:'+Conf.height+'px;">',
          '<div class="modal-content">',
            '<div class="modal-header">',
              '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
              '<h4 class="modal-title" id="gridSystemModalLabel">'+Conf.title+'</h4>',
            '</div>',
            '<div class="modal-body " style="width:100%;height:'+(Conf.height-40)+'px;overflow:hidden;">',
            
             Conf.src?'<div class="overlay"><i class="fa a-spinner fa-spin"></i></div><iframe style="display:none;" id="pop_ifm_'+nid+'" width="100%" height="100%" frameborder="0"></iframe>':Conf.body,
            '</div>',
            '<div class="modal-footer">',
              Conf.noBtnTitle?'<button type="button" class="btn btn-default" data-dismiss="modal">'+Conf.noBtnTitle+'</button>' :"",
              Conf.okBtnTitle?'<button type="button" class="btn btn-primary">'+Conf.okBtnTitle+'</button>':"",
            '</div>',
          '</div',
        '</div>',
      '</div>'
    ].join("");

    $("body").append(popTpl);

    var wrap = $("#"+gName+nid),
        loadingDiv = wrap.find(".overlay"),
        ifr = wrap.find("iframe"),
        that=this,
        okBtn = wrap.find(".btn-primary"),
        noBtn=wrap.find(".btn-default");

    if(ifr.size()){
      ifr.load(function(){
        console.log("load2");
        loadingDiv.hide();
        ifr.show();
        Conf.renderCbk();
        ifr.get(0).contentWindow.Pop=that;
      });
      ifr.attr({src:Conf.src+(!$.isEmptyObject(Conf.data) ? "?formArgs="+encodeURIComponent($.param(Conf.data)):"")})
      
    }

    function show(){
      wrap.modal("show");
      Conf.showCbk()
    }
    function hide(s){
      wrap.modal("hide");
      wrap.next().remove();
      wrap.remove();

    }
    function setBusy(t){
      if(t){
        loadingDiv.show();
        okBtn.attr({"disabled":"true"});
      }else{
        loadingDiv.hide();
        okBtn.removeAttr('disabled');
      }
    }
    okBtn.bind("click",function(){
      var subIfm=ifr.size() && ifr.get(0).contentWindow;
      //获取子窗口提供的提交方法
      if(subIfm && subIfm.Sub && subIfm.Sub.submit){
          setBusy(true);
          subIfm.Sub.submit(function(type,ret,data){
              if(type){//返回成功
                hide();
                Conf.okCbk(true,ret,data);
              }else{//返回失败
                Conf.okCbk(false);
              }
              setBusy(false);
          },_this);
       }else{
        hide()
       }

    })
    wrap.on("hidden.bs.modal",function(){
     // hide();
      Conf.closeCbk();
      wrap.remove();
    })

    show();
    this.show=show;
    this.hide=hide;
    this.setBusy=setBusy;
  }
  pub.popModal=function(conf){
    var Conf={
          title:"标题",
          text:"",
          width:"400",
          type:"",//primary,info,warning,success,danger,几种风格
          okCbk:function(){
          },
          okBtnTitle:"确定",//空则取消按钮
          noBtnTitle:"关闭",//空则取消按钮
          showCbk:function(){
          },
          renderCbk:function(){
          },
          noCbk:function(){
          },
          closeCbk:function(s){
          }
        };
    $.extend(Conf, conf);
    var 
      name='pop_modal_',
      nid=0,
      _this=this;

    try{
      nid = top.Comm.popModal.prototype.nid++;
    }catch(e){
      nid=0;
    }

    var nameId = name+nid,
      popTpl=['<div class="modal" id="'+nameId+'">',
              '<div class="modal-dialog modal-{modalType}" style="width:{width}px">',
                '<div class="modal-content">',
                  '<div class="modal-header">',
                    '<button aria-label="Close" data-dismiss="modal" class="close" type="button">',
                      '<span aria-hidden="true">×</span></button>',
                    '<h4 class="modal-title">{title}</h4>',
                  '</div>',
                  '<div class="modal-body">',
                    '<p>{text}</p>',
                  '</div>',
                  '<div class="modal-footer">',
                    Conf.noBtnTitle?'<button data-dismiss="modal" class="btn btn-{nobtnType} pull-left btn-no" type="button">{noBtnTitle}</button>':"",
                    Conf.okBtnTitle?'<button class="btn btn-{okbtnType} btn-ok" type="button">{okBtnTitle}</button>':"",
                  '</div>',
                '</div>',
              '</div>',
            '</div>'].join("");

    $("body").append(popTpl.ZK_format({
      title:Conf.title,
      text:Conf.text,
      okBtnTitle:Conf.okBtnTitle,
      noBtnTitle:Conf.noBtnTitle,
      width:Conf.width||auto,
      modalType:Conf.type,
      okbtnType:Conf.type ? "outline":"primary",
      nobtnType:Conf.type ? "outline":"default"
    }));

    var wrap = $("#"+nameId),
        okBtn = wrap.find(".btn-ok"),
        noBtn=wrap.find(".btn-no");

    function show(){
      wrap.modal("show");
      Conf.showCbk()
    }
    function hide(){
      wrap.modal("hide");
      wrap.next().remove();
      wrap.remove();
    }
    wrap.on("hidden.bs.modal",function(){
      Conf.closeCbk();
      wrap.remove();
    })
    okBtn.click(function(event) {
      Conf.okCbk();
      hide();
    });
    noBtn.click(function(event) {
      Conf.noCbk();
      hide();
    });
    show();
    Conf.renderCbk();

    
  }
   
  //获取查询参数
  pub.FormData=function(conf){
    var Conf={
        btn:$("#search-btn"),
        cancelBtn:$("#cancel-btn"),
        form:$("#search-form"),
        method:"GET",
        isFormSender:false,
        isEnterTrigger:true,
        dateBeginName:'dateBegin',
        dateEndName:'dateEnd',
        splitCh:",",//多个值得分割，比如多选框里的值
        dealData:function(data){//处理数据,返回true，则提交，返回false，则取消提交
          return true;
        },
        cbk:function(data){//搜索请求的返回结果

        }
    };
    $.extend(Conf, conf);

    function getData(data){
        data = data||{};
        var 
            doms = Conf.form.find("input[type=text][name],input[type=password],input[type=hidden][name],textarea,select[name],input[type=radio]:checked,input[type=checkbox]:checked,input.calendar-sel");
            
      doms.each(function(i,el){
          var name,val;
          el = $(el);
          name = el.attr("name")||"",
          ext = el.attr("ext")||"",
          val=el.val();

         
          //日期单独处理
          if(el.hasClass('calendar-sel') && val){
            var reg=/\d{4}-\d{2}-\d{2}/g,
                dd = val.match(reg);
                ext = (name=="" || name=="rq") ? "":name;
                if(dd.length==2){
                  data[Conf.dateBeginName+ext] = dd[0];
                  data[Conf.dateEndName+ext] = dd[1];
                }else if(dd.length==1){
                  data[Conf.dateBeginName+ext] = dd[0];
                }
          }else if(name){
            name = ext ? name+"-"+ext:name;
            if(typeof(data[name])=="undefined"){
              data[name]=val
            }else{
              data[name]=data[name]+Conf.splitCh+val;
            }
          } 

          if(el.prop("tagName")=="SELECT"){//选择框id和值
            name = el.attr("vname");
            if(name){data[name] = el.find("option:selected").text();}
          }
      })
      return Conf.dealData(data);
    }
    function resetData(data){
      data = data ||{};
      var doms = Conf.form.find("input[type=text][name],input[type=password],input[type=hidden][name],textarea,select[name],input[type=radio]:checked,input[type=checkbox]:checked,input.calendar-sel");
      doms.each(function(i,el){
        var name,val;
          el = $(el);
          name = el.attr("name")||"",
          ext = el.attr("ext")||"",
          val = "";

          name = ext?name+"-"+ext : name;

          if(typeof(data[name])!="undefined"){
            val = data[name];
          }
          $(el).val(val);
      });

    }
    function sendData(cbk,pop){
      var data={},
          isSend = getData(data),
          url=Conf.form.attr("action");
          
        if(isSend && url){
          if(Conf.isFormSender){
             Conf.form.submit();
             return true;
          }else{
            Conf.btn && Conf.btn.attr({"disabled":true})

              $.ajax({
                type:Conf.method,
                url:url,
                data:data,
                dataType:"json",
                success:function(ret){
                   pop&&pop.setBusy(false);
                   Conf.btn && Conf.btn.removeAttr('disabled');
                   Conf.cbk(ret,data);
                   cbk&&cbk(ret,data);
                }
              })
            }
          }else{
            pop&&pop.setBusy(false);
        }
    }
    Conf.btn && Conf.btn.click(function(){
        sendData();
        return false;
    });
    Conf.cancelBtn && Conf.cancelBtn.click(function(){
      resetData();
      sendData();
      return false;
    })
    this.getData = function(dd){
      dd = dd||{} ;
      getData(dd);

      return dd;
    };
    if(Conf.isEnterTrigger){
      Conf.form.bind("keypress",function(evt){
        if(evt.keyCode==13){
          sendData();
          return false;
        }
      })
    }
    this.sendData = sendData;
  }
  var popId=100;
  pub.showMsg=function(txt,type,cbk,time,el,isUnique){
    var delay=time||2000,
      id="msg_"+popId++,
      type=type||'warning',
      pop;
    if(isUnique){
      $(".pop-msg").hide();
    }
    $("body").append('<div id="'+id+'" class="pop-msg alert alert-'+type+' alert-dismissable">\
                        <button aria-hidden="true" data-dismiss="pop-msg" class="close" type="button">×</button>\
                          <i class="icon fa fa-'+(type=="success" ? "check":type)+'"></i>'+txt+'\
                      </div>');
    pop=$("#"+id);
    if(el){
      var p= parseInt(el.offset().top,10)-pop.height();
      pop.css({"position":"absolute","top":p+"px",'margin-top':'0px'});
    }
    pop.find(".close").click(function(){
      pop.remove();
    })

    setTimeout(function(){
      pop.remove();
      cbk&&cbk();
    },delay);
  }

  pub.upLoadImg=function(conf){
      var Conf={
        'inputCls':'pic-src-ipt',
        'url':'/projectImgController/saveOrUpdateImg?type=1',
        'exts':'jpg,jpeg,png,gif',
        'imgHost':'http://www.g.share.static.nongmao365.com/',
        'wrapSelector':'.file-open',
        'prevSelector':'.pre-img',
        'addSelector':'.add-img',
        'charSplit':'|',
        'fileCls':'file-ipt',
        'prevTpl':['<div class="col-xs-1 pre-img">',
                        '<i class="fa fa-times del-img text-red" title="删除图片"></i>',
                        '<img src="{src2}" data-src="{src}" title="点击查看原图">',
                    '</div>'].join(""),
        'cbk':function(){}
      };
      $.extend(Conf,conf);
      
      
      var wrapDiv = $(Conf.wrapSelector),
          fileTpl = '<input type="file" accept="image/*" \
                  style="position:absolute;top:0px;left:0px;width:100%;height:100%;opacity:0" \
                  id="img_file_{i}" class="'+Conf.fileCls+'" name="file">\
                  <div class="overlay" style="display:none;"><i class="fa a-spinner fa-spin"></i></div>';

            //初始化
      function init(wrap){
        var iptEl = wrap.find("."+Conf.inputCls),
            addBtn = wrap.find(Conf.addSelector),
            urls=(iptEl.val()||"").split(Conf.charSplit),
            src=""
        for(var i=0,l=urls.length;i<l;i++){
            src=urls[i];
            if(src.length){
              add2ipt(true,src,iptEl,addBtn);
              $(Conf.prevTpl.ZK_format({src:src,src2:Conf.imgHost+src+"?_t="+Math.random()})).insertBefore(addBtn);
          }
        }
      }

      //增加上传文件file
      wrapDiv.each(function(i,el){
        el = $(el);
        var addDiv = el.find(Conf.addSelector),
            h=addDiv.height(),
            w=addDiv.width();
        addDiv.css({"position":'relative',"overflow":"hidden"})
          .append(fileTpl.ZK_format({width:w,height:h,i:i}));
        init(el);
      })

      //在隐藏ipt中，增加图片地址
      function add2ipt(isAdd,src,ipt,addEl){
        var val = ipt.val()||"",
            pics = val.split(Conf.charSplit)||[],
            num = ipt.attr("maxNum")||1;

            for(var i=0,l=pics.length;i<l;i++){
              if(pics[i]==src || pics[i]==""){
                  pics.splice(i,1);
              }
            }

            if(isAdd){
              pics.push(src);
            }

            ipt.val(pics.join(Conf.charSplit));

            if(pics.length>=num){
              addEl.hide();
            }else{
              addEl.show();
            }
      }

      //检查文件后缀
      function checkImgSuffix(fileName){
        fileName= fileName.toLowerCase();
        var picSuffix =Conf.exts;
        var fileSuffix = fileName.substr(fileName.lastIndexOf(".")+1, fileName.length);

        return picSuffix.indexOf(fileSuffix)==-1? false :true;
      } 

      //图片删除
      $("body").delegate(".del-img","click",function(){
        var el = $(this),
            wrap =el.closest(Conf.wrapSelector),
            prev = el.closest(Conf.prevSelector),
            img = prev.find("img"),
            ipt = wrap.find("."+Conf.inputCls);

          if(confirm("确定删除这张图片吗？")){
            prev.remove();
            add2ipt(false,img.data("src"),ipt,wrap.find(Conf.addSelector));
          }
      });

      //图片预览
      $("body").delegate(Conf.prevSelector+" img", 'click', function(event) {
        var src = this.src;
        top.Comm.popIfm({
          src:null,
          title:"图片预览",
          width:"600",
          height:"400",
          body:'<div style="text-align:center;padding:20px"><img style="max-width:100%;max-height:100%;" src="'+src+'" /></div>'
        })
     });



      


      //图片上传
      $("."+Conf.fileCls).unbind('change').bind("change",function(){
          var el = $(this),
              wrap =el.closest(Conf.wrapSelector),
              add = wrap.find(Conf.addSelector),
              load = wrap.find(".overlay"),
              iptEl = wrap.find("."+Conf.inputCls),
              filename = el.val();

        if(!checkImgSuffix(filename)){
          Comm.showMsg("图片的格式仅为："+Conf.exts);
          return false;
        }
          el.hide();
          load.show();

        function addPic(src){
            add2ipt(true,src,iptEl,add);
            $(Conf.prevTpl.ZK_format({src:src,src2:Conf.imgHost+src+"?_t="+Math.random()})).insertBefore(add);

        }

        var file = el.get(0).files[0],
          formdata = new FormData(),
          xhr = new XMLHttpRequest();
            formdata.append("avatarFile", file);
            xhr.open("POST",Conf.url, true);
            xhr.send(formdata);
            xhr.onload = function(e) {
                if (this.status == 200) {
                   var data = $.parseJSON(this.responseText);
                    if( data.success){
                      var src = data.obj.imgurl;
                      Comm.showMsg(data.msg||'上传成功','success'); 
                      addPic(src);

                      Conf.cbk && Conf.cbk(src,el);

                  } else {
                    Comm.showMsg(data.msg||"上传失败");
                    Conf.cbk && Conf.cbk();
                  }
                } else {
                  Comm.showMsg(data.msg||"上传失败");
                  Conf.cbk && Conf.cbk();
                }
                el.show();
                load.hide();
            };
      })
    }

    pub.upLoadFile=function(conf){
    var Conf={
        'inputCls':'file-src-ipt',
        'url':'/excelController/importOrder',
        'exts':'xls,xlsx,csv',
        'wrapSelector':'.file-open',
        'prevSelector':'.pre-img',
        'addSelector':'.add-img',
        'charSplit':'|',
        'fileCls':'file-ipt',
        'prevTpl':['<div class="col-xs-1 pre-img">',
                        '<i class="fa fa-times del-img text-red" title="删除文件"></i>',
                        '<span>{src}</span>',
                    '</div>'].join(""),
        'cbk':function(){}
      };
      $.extend(Conf,conf);
    var wrapDiv = $(Conf.wrapSelector),
          fileTpl = '<input type="file" accept="*" \
                  style="position:absolute;top:0px;left:0px;width:100%;height:100%;opacity:0" \
                  id="file_{i}" class="'+Conf.fileCls+'" name="file">\
                  <div class="overlay" style="display:none;"><i class="fa a-spinner fa-spin"></i></div>';
     //增加上传文件file
      wrapDiv.each(function(i,el){
        el = $(el);
        var addDiv = el.find(Conf.addSelector),
            h=addDiv.height(),
            w=addDiv.width();
        addDiv.css({"position":'relative',"overflow":"hidden"})
          .append(fileTpl.ZK_format({width:w,height:h,i:i}));
      })

      //在隐藏ipt中，增加文件地址
      function add2ipt(isAdd,src,ipt,addEl){
        var val = ipt.val()||"",
            pics = val.split(Conf.charSplit)||[],
            num = ipt.attr("maxNum")||1;

            for(var i=0,l=pics.length;i<l;i++){
              if(pics[i]==src || pics[i]==""){
                  pics.splice(i,1);
              }
            }

            if(isAdd){
              pics.push(src);
            }

            ipt.val(pics.join(Conf.charSplit));

            if(pics.length>num){
              addEl.hide();
            }else{
              addEl.show();
            }
      }

      //检查文件后缀
      function checkImgSuffix(fileName){
        fileName= fileName.toLowerCase();
        var picSuffix =Conf.exts;
        var fileSuffix = fileName.substr(fileName.lastIndexOf(".")+1, fileName.length);

        return picSuffix.indexOf(fileSuffix)==-1? false :true;
      } 

      //文件删除
      $("body").delegate(".del-img","click",function(){
        var el = $(this),
            wrap =el.closest(Conf.wrapSelector),
            prev = el.closest(Conf.prevSelector),
            img = prev.find("img"),
            ipt = wrap.find("."+Conf.inputCls);

          if(confirm("确定删除该文件吗？")){
            prev.remove();
            add2ipt(false,img.data("src"),ipt,wrap.find(Conf.addSelector));
          }
      });

      //文件下载/预览
      $("body").delegate(Conf.prevSelector+" img", 'click', function(event) {
        var src = this.src;
        
     });
   
   function progress(load,per){
     var progress = load.find(".progress-bar"),
        em = load.find("small");
      if(progress.size()==0){
        load.html('<div class="clearfix"><small class="pull-right">'+per+'</small></div>\
              <div class="progress xs">\
                <div class="progress-bar progress-bar-green" style="width: '+per+'"></div>\
              </div>');
      }else{
        em.html(per);
        progress.css({"width":per});
      }
      
   }


      //文件上传
      $("."+Conf.fileCls).unbind('change').bind("change",function(){
          var el = $(this),
              wrap =el.closest(Conf.wrapSelector),
              add = wrap.find(Conf.addSelector),
              load = wrap.find(".overlay"),
              iptEl = wrap.find("."+Conf.inputCls),
              filename = el.val();

        if(!checkImgSuffix(filename)){
          Comm.showMsg("文件的格式仅为："+Conf.exts);
          return false;
        }
          el.hide();
          load.show();

        function addPic(src){
            add2ipt(true,src,iptEl,add);
            var arr = filename.split("\\");
            $(Conf.prevTpl.ZK_format({src:arr[arr.length-1]})).insertBefore(add);
            el.show();
            load.hide();
        }

        var file = el.get(0).files[0],
          formdata = new FormData(),
          xhr = new XMLHttpRequest();
            formdata.append("avatarFile", file);
            xhr.open("POST",Conf.url, true);
            xhr.send(formdata);
            xhr.onload = function(e) {
                el.show();
                load.hide();
                if (this.status == 200) {
                  try{
                     var data = $.parseJSON(this.responseText);
                    if( data.success){
                        var src = data.attributes.fileid||"";
                        addPic(src);
                        Comm.showMsg(data.msg||'上传成功','success'); 
                        progress(load,"100%");
                        Conf.cbk && Conf.cbk(src,el);
                      } else {
                        Comm.showMsg(data.msg||"上传失败");
                        Conf.cbk && Conf.cbk(0);
                      }
                  }catch(e){
                    Comm.showMsg("上传失败");
                    Conf.cbk && Conf.cbk(0);
                  }


                } else {
                  Comm.showMsg(data.msg||"上传失败");
                  Conf.cbk && Conf.cbk(0);
                }
                
            };
    xhr.upload.onprogress=function(evt){
      if (evt.lengthComputable) {
        var percentComplete = Math.round(evt.loaded * 100 / evt.total)+"%";
        progress(load,percentComplete);
      }
    }
    /*var p=0;
    var timer=setInterval(function(){
      progress(load,(p++)+"%");
      if(p>=100){
        clearInterval(timer);
        addPic();
      }
    },1000);*/


      })
    
    
  }

    pub.ZKInterface={
      'Page':'http://www.g.share.static.nongmao365.com/static/plugins/pager/ZK.page.js'
    };
    pub.ZKLoader=function(name,cbk){
      if(typeof(ZK)!="undefined" && typeof(ZK[name])!="undefined"){
        cbk(ZK[name]);
      }else{
        $.getScript(pub.ZKInterface[name],function(){
          cbk(ZK[name]);
        })
      }
    }
    pub.initTable=function(conf){
      var Conf={
          wrap:$("#table_div"),
          tableCls:"",
          pageWrap:$("#page_div"),
          dataUrl:null,//接口
          method:"GET",
          cache:false,//翻页时是否缓存当前页的内容，默认为false 不缓存，用于刷新内容
          rowsArg:'showNum',//一页几行的参数名
          pageArg:'pageNum',//页码的参数名
          fieldsArg:'field',//获取字段参数列表
          showNum:12,//一页几行
          pageNum:0,//获取第几页
          isTable:true,//默认用table，false 为自定义结构
          liTpl:'<li></li>',//当使用isTable=false 时，用这里的模板
          emptyTpl:'<div class="data-empty"><i class="fa fa-fw fa-file-o"></i>暂无数据~~</div>',
          trTpl:'<tr role="row" class="{odd}">{tds}</tr>',
          thTpl:'<th {keys} style="{thStyle}">{d}</th>',
          tdTpl:'<td {keys} colspan="{colspan}" style="{tdStyle}">{d}</td>',
          loadingTpl:'<div class="overlay"><i class="fa a-spinner fa-spin"></i></div>',
          tableTpl:'<table  class="table  table-striped dataTable {tableCls}" role="grid">{ths}{trs}</table>',

          dealData:function(data){//对数据的处理

          },
          renderCbk:function(el){//回执之后的回调

          },
          controls:['detail','edit','del']//操作的选项
      },
      controls={
        'detail':'<button type="button" class="btn btn-default detail-btn" r="{r}" {keys}><i class="fa fa-file-text-o"></i>详情</button>',
        'edit':'<button type="button" class="btn btn-default edit-btn" r="{r}" {keys}><i class="fa  fa-edit"></i>编辑</button>',
        'del':'<button type="button" class="btn btn-default del-btn" r="{r}" {keys}><i class="fa fa-times"></i>删除</button>',
        'radio':['<div class="radio">',
                    '<label>',
                      '<input type="radio" {ch} value="0" r="{r}" {keys} name="optionsRadios">',
                      '选择',
                    '</label>',
                  '</div>'].join(""),
        'checkbox':['<div class="checkbox">',
                    '<label>',
                      '<input type="checkbox" {ch} r="{r}" value="0" {keys} name="optionsRadios">',
                      '选择',
                    '</label>',
                  '</div>'].join("")

      }
    
    $.extend(Conf, conf);
    function initLoading(){
      if(!Conf.wrap.find(".overlay").size()){
        Conf.wrap.append(Conf.loadingTpl);
      }

    }
    function format(val,o){
      var map={
        'dateFormat':function(s,f){
          var d = new Date();
          d.setTime(s);
            return d.ZK_format(f);
        },
        'maxLen':function(s,n){
          var ext="...";
          if($.isPlainObject(n)){
            
            ext=n.fix;
            n=n.num;
          }
          if(s.length>n){
            return s.substr(s,n-1)+ext;
          }
          return s;

        },
        'valMap':function(s,m){
            return typeof(m[s])!="undefined" ? m[s] : s;
        },
        'defaultVal':function(s,v){
            return s && s!="" && s!="NULL" && s!="null" ? s: v;
        },
        'func':function(s){
          return o['func'](s);
        }

      }
      for(var key in o){
        if(typeof(map[key])!="undefined"){
          val=map[key](val,o[key])
        }
      }
      return val;
    }

    function getControls(cts){
      cts = cts || Conf.controls;
      var ct=[],
          tpl=[
            '<div class=" ctr-div">',
                '<span class="ctr-btn"><i class="fa fa-fw fa-cog"></i><i class="fa fa-fw fa-angle-down"></i></span>',
                '<div class="box ctr-float">{ct}</div>',
            '</div>'
          ].join("");
      if(cts){
        for(var j=0,jl=cts.length;j<jl;j++){
          var a = cts[j];
          if(typeof(controls[a])!="undefined"){
            ct.push(controls[a]);
          }else{
            ct.push('<button type="button" class="btn btn-default btn-'+j+'" {keys}>'+a+'</button>')
          }
        }
        if(cts.length==1){
          return ct.join("");
        }else{
          return tpl.ZK_format({ct:ct.join("")})

        }
      }else{
        return false;
      }
    }

    //处理选择事件
    var selData={};
    function dealSelClick(){
        var heads=Conf.fields,keys=[],ch="|||";

       for(var i=0,l=heads.length;i<l;i++){
        if(heads[i]['isKey']){
          keys.push(heads[i]['id']);
        }
      }

        Conf.wrap.delegate('input[type=radio],input[type=checkbox]', 'click', function(event) {
          var me = $(this),
              type =me.attr("type").toLowerCase();
          if(type=='radio'){
            selData={};
          }
          var key=[],o={},d;
          for(var i=0,l=keys.length;i<l;i++){
            d= me.data(keys[i]);
            key.push(d);
            o[keys[i]]=d;
          }
          //设置全局的属性
          selData[key.join(ch)]=this.checked ? o :false;
        })
    }
    function getSelData(){
      var data=Conf.data,ret=[],ch="|||";
      
      for(var i=0,l=data.length;i<l;i++){
        var b=false;
        for(var key in selData){
          b=true;
          if(selData[key]){
            var o = selData[key];
            for(var k in o){
              if(o[k] != data[i][k]){
                  b=false;
                  break;
              }
            }
            if(b){//匹配
              break;
            }
          }else{
            b=false;
          }
          
        }

        if(b){
          ret.push(data[i]);
          data[i]['ch']="checked";
        }else{
          data[i]['ch']="";
        }
      }
  
      return ret;
    }
    if(Conf.controls && Conf.controls.join(",").match("radio|checkbox")){
      dealSelClick();
    }
    function getTpl(heads){
      var arr=[],keys=[],ths=[],colspanTd=[],ct=getControls(),colNum=0,hides={};
      for(var i=0,l=heads.length;i<l;i++){
        if(heads[i]['isKey']){
          keys.push('data-'+heads[i]['id']+'="{'+heads[i]['id']+'}"')
        }
        if(!heads[i]['isHide']){
          colNum++;
          //arr.push('<td {keys}>{'+heads[i]['id']+'}</td>');
          if(heads[i]['isColspan']){//通栏的操作
            var ctt= heads[i]['controls'] ? getControls(heads[i]['controls']):false;
            colspanTd.push(Conf.tdTpl.ZK_format({"d":"{"+heads[i]['id']+"}"+(ctt ? '<div style="float:right">'+ctt+'</div>' :""),"tdStyle":heads[i]['tdStyle']||""}));
          }else{
            arr.push(Conf.tdTpl.ZK_format({"d":"{"+heads[i]['id']+"}","tdStyle":heads[i]['tdStyle']||""}));
            ths.push(Conf.thTpl.ZK_format({"d":heads[i]['name'],"thStyle":heads[i]['thStyle']||""}));
          }
          
        }else{
          hides[heads[i]['id']]=1;
        }
      }
      //行的 操作
      if(ct && ct.length){
        colNum++;
        ths.push(Conf.thTpl.ZK_format({'d':"操作","thStyle":"width:90px"}));
        arr.push(Conf.tdTpl.ZK_format({"d":ct,"tdStyle":"width:80px;text-align:center;"}));
      }
      for(var j=0,jl=colspanTd.length;j<jl;j++){
        colspanTd[j] = colspanTd[j].ZK_format({'keys':keys.join(" ")})
      }
      return {
              'td':arr.join("").ZK_format({'keys':keys.join(" ")}),
              'th':ths.join("").ZK_format({'keys':keys.join(" ")}),
              'keys':keys.join(" "),
              'hides':hides,
              'colNum':colNum,
              'colspanTdArr':colspanTd
            };
    }

    function testFormat(tpl,o,keys){

      var isOk=false,
          str= tpl.replace(/\{([\d\w_]+)\}/g, function (m, n) {
            if(typeof(o[n]) != "undefined"){
              if(!keys[n] && n!="r"){
                isOk=true;
              }
             // console.log(o[n])
              return o[n];
            }else{
              return m;     
            }
          });

      return isOk ? str : false;
    }
  
    function makeTable(data){
          var tr=[],
          tdObj = getTpl(data.heads),
          td="",
          ct="",
          colArr=tdObj['colspanTdArr'],
          colNum=tdObj['colNum'],
          tdTpl=tdObj['td'],
          trTpl=Conf.trTpl,
          th=tdObj['th'],
          hides = tdObj['hides'],
          tdData = data.rows||[];
 
      th=trTpl.ZK_format({no:-1,odd:"odd",tds:th});

      getSelData();
      var odd="",str;
      for(var i=0,l=tdData.length;i<l;i++){ 
          tdData[i]['r'] =i;
          odd = i%2?"odd":"even";
          
          for(var j=0,k=colArr.length;j<k;j++){
            td=colArr[j].ZK_format({"colspan":colNum});
            td = testFormat(td,tdData[i],hides);
            if(tdData[i]['showColspan']!==false && td){
              tr.push(trTpl.ZK_format({no:i,odd:odd,tds:td}));
              if(Conf.showHead===false){
                tr.push(th);
              }
            }

          }

          td =testFormat(tdTpl,tdData[i],hides);
          if(td){
            tr.push(trTpl.ZK_format({no:i,odd:odd,tds:td}));
          }
      }

      if(Conf.showHead===false){
        th="";
      }
      Conf.wrap.html(Conf.tableTpl.ZK_format({ths:th,trs:tr.join(""),'tableCls':Conf.tableCls}));
      if(l==0){
        Conf.wrap.append(Conf.emptyTpl);
      }
      Conf.renderCbk();
    }
    function copyO(o){
      var oo={};
      for(var key in o){
        oo[key] = o[key];
      }
      return oo;
    }
    function copyArr(arr){
      var a=[];
      for(var i=0,l=arr.length;i<l;i++){
        a.push( $.isPlainObject(arr[i]) ? copyO(arr[i]):arr[i]);        
      }
      return a;
    }
    function getLi(data){
      var liTpl= Conf.liTpl,
          tdObj = getTpl(Conf.fields),
          arr=[],
          ct=getControls(),
          rows = data.rows;

      for(var i=0,l=rows.length;i<l;i++){
        arr.push(liTpl.ZK_format({'controls':ct}).ZK_format({keys:tdObj.keys}).ZK_format(rows[i]));
        
      }
      return arr.join("");
    }
    function makeLi(data){      
      Conf.wrap.html(getLi(data));
    }
    function getField(data){//获取 表的标题，格式化返回值{id:'x','name':'name','data':'data'}  
      var rows = data.rows,arr=[],heads=Conf.fields||data.heads,arr1;
      if(!data.rows){

        arr = data.items;
        var aa=[]
        for(var i=0,l=arr.length;i<l;i++){
          aa.push(arr[i]);
          if(arr[i]['rows']){
            for(var j=0,jl=arr[i]['rows'].length;j<jl;j++){
              aa.push(arr[i]['rows'][j])
            }
          }
        }
        arr=aa;

      }else{
        
        for(var i=0,l=rows.length;i<l;i++){
            var o={};
            for(var j=0,jl=rows[i].length,hl=heads.length;j<jl&&j<hl;j++){
              o[heads[j]['id']] =format(rows[i][j],heads[j]);
            }
            format(o,heads[j]);
            arr.push(o);
        }
                 
      }
      data.rows=arr;

      data.heads = copyArr(heads);
      Conf.heads = data.heads;
       //缓存数据
      if(typeof(Conf.data)=="undefined" || !Conf.cache){
        Conf.data=[];
      }
      Conf.data = Conf.data.concat(arr);
      Conf.rows =data.rows;
      
    }
    function getFieldArgs(fields){
      var arr=[];
      for(var i=0,l=fields.length;i<l;i++ ){
        arr.push(fields[i]['id']);
      }
      return arr.join(",");
    }
    var dataMap={};
    function getPage(p){
      initLoading();
      var data={};
          
      data[Conf.rowsArg] = Conf.showNum;
      data[Conf.pageArg] = p||Conf.pageNum;
      data[Conf.fieldsArg] = getFieldArgs(Conf.fields);

      var temp = data[Conf.fieldsArg] + Conf.showNum+ data[Conf.pageArg];


      if(Conf.cache && typeof(dataMap[temp])!="undefined"){
            var ret =dataMap[temp];
            console.log(ret);
            Conf.isTable ? makeTable(ret.data) : makeLi(ret.data);
            magePages(ret.data.total,ret.data.page);
      }else{
        $.ajax({
          method:Conf.method,
          url:Conf.dataUrl,
          data:data,
          dataType:"json",
          success:function(ret){
             if(ret && ret.success==1){
              getField(ret.data);

              Conf.dealData(ret.data);
              if(Conf.cache){
                dataMap[temp]=ret;
              }

              Conf.isTable ? makeTable(ret.data) : makeLi(ret.data);
              magePages(ret.data.total,ret.data.page);
                           }
          }
        })
      }
    }

    function magePages(total,curPage){
      pub.ZKLoader("Page",function(mod){
        new mod({
          el:Conf.pageWrap,
          page:curPage,
          total:total,
          showNum:Conf.showNum,
          pageTagNum:4,
          changeList:function(p){
            getPage(p);
          }
        })
      })
    }
    initLoading();
    getPage(Conf.pageNum);

    //对外函数，获取选择的数据
    this.getSelData=function(){
      return getSelData();
    }

    function focusItem(rowEl,cbk){
      var cls="active-row",
          delay = 1000;
      rowEl.addClass(cls);

      setTimeout(function(){
        cbk && cbk();
        rowEl.removeClass(cls);
        },delay)
    }
    function makeNewData(o){
      var ret={rows:[o]},
            tdObj = getTpl(Conf.heads),
            tdTpl=tdObj['td'];

        Conf.dealData(ret);
        var tpl = "";
        if(Conf.isTable){
          tpl= tdTpl.ZK_format(ret.rows[0]);
        }else{
          tpl = getLi(ret);
        }
        return tpl;
    }

    //向表格中添加一条数据
    this.insertItem=function(data){
      var tpl = makeNewData(data),
          tr;
      if(Conf.isTable){
        tpl = Conf.trTpl.ZK_format({tds:tpl});
        tr = Conf.wrap.find("tr.even:first");

      }else{
        tr=Conf.wrap.find("li:first");
      }
      var el = $(tpl);
      el.insertBefore(tr);
      focusItem(el);

    }
    //删除表格中的一条数据
    this.delItem=function(rowEl){
      focusItem(rowEl,function(){
        rowEl.fadeOut('fast', function() {
          rowEl.remove();
          Comm.showMsg("删除成功",'success',function(){});
        });
      })
    }

    //更新表格中的一条数据
    this.updateItem=function(rowEl,data){
      focusItem(rowEl,function(){
        var tpl= makeNewData(data);
            rowEl.html(tpl);
        Comm.showMsg("编辑成功",'success',function(){});

      });
      
    }

  }
  //区域选择
  pub.ZoneSel=function(conf){
    var Conf={
      pUrl:'/territoryController/cascadeArea',
      cUrl:'/territoryController/cascadeArea?id={id}',
      rUrl:'/territoryController/cascadeArea?id={id}',
      pName:'provinceId',
      pValName:'provinceName',
      cName:'cityId',
      cValName:"cityName",
      rName:'regionId',
      rValName:'regionName',
      selSelector:"zone_sel",
      initVal:{'p':'省','c':'市','r':'区'},
      selCbk:function(data){},
      initData:false
    };

    $.extend(Conf, conf);

    var div = $(Conf.selSelector),
      pSel = $("select[name="+Conf.pName+"]"),
      cSel = $("select[name="+Conf.cName+"]"),
      rSel = $("select[name="+Conf.rName+"]"),
      initOk= false,
      dataMap={};

 

    function getData(t,id,cbk){
      var url = Conf[t+"Url"].ZK_format({id:id||0}),
        v = t+"_"+id;

      if(typeof(dataMap[v])!="undefined"){
        cbk&&cbk(dataMap[v]);
      }else{
        $.getJSON(url,function(data){
          if(data){
            var arr = [{'code':0,'name':Conf['initVal'][t]}];
            for(var i=0,l=data.length;i<l;i++){
              var o = data[i],oo={};
              for(var key in o){
                if(key=="id"){
                  oo['code'] = o[key];
                }else if(key.match(/Name$/)){
                  oo['name'] = o[key];
                }
              }
              arr.push(oo);
            }
            dataMap[v] = arr;
            cbk&&cbk(arr);
          }
        })
      }
    }

    function setData(el,data){
      var arr=[],
        tpl='<option value="{code}">{name}</option>';
      for(var i=0,l=data.length;i<l;i++){
        arr.push(tpl.ZK_format(data[i]));
      }
      el.html(arr.join(""));
    }

    function clean(t,el){
      setData(el,[{'code':0,'name':Conf['initVal'][t]}]);
    }
    var that=this;
    function change(type,id,el,cbk){
      if(id==0){
        if(type=='c'){
          clean('c',cSel);
        }
        if(type=='r' ||type=="c"){
          clean('r',rSel);
        }
        Conf.selCbk(that.getSelData());
      }else{
        getData(type,id,function(data){
          setData(el,data);
          cbk&&cbk();
          Conf.selCbk(that.getSelData());

        })
      }
    }

    //获取省得数据
    if(!Conf.initData){
      getData('p',0,function(data){
        setData(pSel,data);
        initOk=true;
      });
    }
    

    //选则省
    $("select[name="+Conf.pName+"]").change(function(event) {
      change('c',this.value,cSel);
    });

    //选择市
    $("select[name="+Conf.cName+"]").change(function(){
      change('r',this.value,rSel);
    })

    //选择区
    $("select[name="+Conf.rName+"]").change(function(){
      Conf.selCbk(that.getSelData());
    })

    
    this.init=function(json){//{“provinceId”:0,"cityId":0,"regionId":0}
      json  = json||{"provinceId":0,"cityId":0,"regionId":0};

      getData('p',0,function(data){
        setData(pSel,data);
        //设置省
        pSel.val(json['provinceId']);
        change('c',json['provinceId'],cSel,function(){
          //设置市
          cSel.val(json['cityId']);
          change('r',json['cityId'],rSel,function(){
            //设置区
            rSel.val(json['regionId']);

          });

        });
      })
    }
    this.getSelData=function(){
      var data={};
      data[Conf.pName]=pSel.val();
      data[Conf.pValName]=pSel.find("option:selected").text();

      data[Conf.cName]=cSel.val();
      data[Conf.cValName]=cSel.find("option:selected").text();

      data[Conf.rName]=rSel.val();
      data[Conf.rValName]=rSel.find("option:selected").text();

      return data;

    }
  }

  pub.floatDiv=function(cbk,conf){
      var divSelector=conf && conf.divSelector||"float-div",
          btnSelector=conf && conf.btnSelector||"float-btn",
          contSelector=conf && conf.contSelector||"float-cont",
          downCls =conf && conf.downCls||'fa-angle-down',
          upCls=conf && conf.upCls||"fa-angle-up";
     function show(){

     }
     function hide(){

     }

     $("body").delegate('.'+divSelector, "mouseenter", function(event) {
        var me = $(this),
            btn = me.find("."+btnSelector),
            div = me.find("."+contSelector);
        div.show();

        cbk&&cbk(btn.data("key"),btn.data("type"),div);
        btn.find("."+downCls).removeClass(downCls).addClass(upCls);
     }).delegate('.'+divSelector,"mouseleave", function(event) {
         var me = $(this),
            btn = me.find("."+btnSelector),
            div = me.find("."+contSelector);

        div.hide();
        btn.find("."+upCls).removeClass(upCls).addClass(downCls);


     });
  }
  pub.showSelPop=function(conf){
      var Conf={
        title:"",
        type:"",
        links:[],
        delay:5,
        goBack:{'link':'',"name":"上个"}
      };
      $.extend(Conf, conf);

      var id="sel-pop",
        popTpl=[
          '<div class="modal fade" tabindex="-1" role="dialog" id="'+id+'" aria-labelledby="gridSystemModalLabel">',
            '<div class="modal-dialog modal-lg box" role="document" style="width:400px;">',
              '<div class="modal-content">',
                '<div class="modal-header">',
                  '<h4 class="modal-title" id="gridSystemModalLabel"><i class="icon fa fa-check text-green"></i>'+Conf.title+'<span class="text-red">成功</span>，请选择下列操作:</h4>',
                '</div>',
                '<div class="modal-body " style="width:100%;height:auto;overflow:auto;">',
                  (function(){
                    var link=[],arr=Conf.links;
                    for(var i=0,l=arr.length;i<l;i++){
                      link.push('<p><a href="'+arr[i]['link']+'" target="main_ifm">'+arr[i]['name']+'</a></p>');
                    }
                    return link.join("");
                  })(),
                  '<p class="text-right"><span class="text-yellow" id="pop_delay_span">'+Conf.delay+'</span>秒钟后，将为您返回<a class="text-light-blue" href="'+Conf.goBack['link']+'" target="main_ifm">'+Conf.goBack['name']+'</a></p>',
                '</div>',
              '</div',
            '</div>',
          '</div>'
        ].join("");

    $("body").append(popTpl);
    var wrap= $("#"+id),
        span = $("#pop_delay_span");
    wrap.modal("show");

    var timer=setInterval(function(){
      if(Conf.delay--==0){
        clearInterval(timer);
        $("#main_ifm").attr({src:Conf.goBack.link});
         wrap.modal("hide");

      }else{
        span.html(Conf.delay);
      }
    },1000);

      
  }
  pub.goPage=function(link){
    $("#main_ifm",top.document).attr({src:link});
  }
  pub.groupErrMsg=function(ipt,msg){
  var wrap = ipt.closest('.form-group'),
      cls="has-error",
      tpl='<label for="inputError" class="control-label err-msg"><i class="fa fa-times-circle-o"></i>'+msg+'</label>';
    
    wrap.removeClass(cls);
    wrap.find(".err-msg").remove();

    if(msg){
      wrap.addClass(cls);
      $(tpl).insertBefore(ipt);
      ipt.focus();
    }
  
};
pub.sendCode=function(conf){
  var Conf={
    phoneEl:$("#userNameClient"),
    phoneName:"phonenum",
    codeEl:$("#randCode"),
    codeBtn:$("#btnSendCode"),
    checkPhoneApi:"",
    sendCodeApi:"/randCodeMo",
    waitSeconds:60
  };

  $.extend(Conf, conf);

  function check(cbk){
    var pho = Conf.phoneEl.val()||"";

    //检查手机号码是否输入
    if(pho=="" || !pho.match(/^1\d{10}$/)){
      pub.groupErrMsg(Conf.phoneEl,"请您先输入正确的手机号码。");
      cbk(false);
      return;
    }

    //检查手机号码是否已被注册
    if(Conf.checkPhoneApi){
        $.ajax({
          type: "POST", //用POST方式传输
          dataType: "text", //数据格式:JSON
          url: Conf.checkPhoneApi, //目标地址
          data: (Conf.phoneName||Conf.phoneEl.attr("name"))+"=" + pho,
          error: function (XMLHttpRequest, textStatus, errorThrown) { 
              cbk(false);
              pub.groupErrMsg(Conf.phoneEl,"网络繁忙请稍候再试。");

              return false;
          },
          success: function (msg){
            var json = eval('(' + msg + ')');
            if(json.status == 'y'){
              cbk(pho);
            }else{
              cbk(false);
              pub.groupErrMsg(Conf.phoneEl,json&&json.msg||"您的手机号已经注册过了");
            }
          }
                 
        });
    }else{
      cbk(pho);
    }
  }
  function activeBtn(b){
    if(b){
      Conf.codeBtn.removeAttr('disabled').html("发送验证码")
    }else{
        Conf.codeBtn.attr({
          disabled: true
        }).html("发送中");
    }
  }
  function setWait(){
      var i=Conf.waitSeconds;
      Conf.codeBtn.html("剩余:"+i+"秒");
      var timer= setInterval(function(){
        if(--i <0){
          clearInterval(timer);
          activeBtn(true)
        }else{
          Conf.codeBtn.html("剩余:"+i+"秒");
        }
      },1000);
  }
  Conf.codeBtn.click(function(){
    check(function(ret){
        if(ret){//验证ok，发送请求
          var str = Conf.codeBtn.text();
          activeBtn(false);

          $.ajax({
              type: "POST", //用POST方式传输
              dataType: "text", //数据格式:JSON
              url: Conf.sendCodeApi, //目标地址
              data:  (Conf.phoneName||Conf.phoneEl.attr("name"))+"="+ret,
              error: function (XMLHttpRequest, textStatus, errorThrown) { },
              success: function (json){ 
                    setWait();
              }
          });


        }
    });
  });


  
}
  pub.selAll=function(conf){
    var Conf={
        mainCheckBoxSelector:"#sel_all",
        subCheckBoxSelector:"input[name=sub-sel]",
        selCbk:function(){}
    }
    $.extend(Conf, conf);
    
    function set(b){
      $(Conf.mainCheckBoxSelector).prop({"checked":b});
      $(Conf.subCheckBoxSelector).prop({"checked":b});
    }

    $(Conf.mainCheckBoxSelector).click(function(){
      set(this.checked);
      Conf.selCbk(true,this);

    })
    $("body").delegate(Conf.subCheckBoxSelector, 'click', function(event) {
     if(!this.checked){
        $(Conf.mainCheckBoxSelector).prop({"checked":false});
     }
     Conf.selCbk(false,this);
   });
    this.get=function(){
      var v=[];
      $(Conf.subCheckBoxSelector+":checked").each(function(){
        v.push(this.value);
      });
      return v;
    }

  }
    //数量选择事件
  pub.buyNum=function(el,cbk){
      var f=function(n,numIpt){
        var p = numIpt.attr("price"),priceEl=numIpt.closest('.buy-num .price');

        if(isNaN(n)){
          numIpt.val("1");
          n=1;
        }else{
          numIpt.val(n);
          
        }
        cbk&&cbk(n,numIpt);

        if(n*p){
          priceEl.html("共：￥"+n*p);
        }else{
          priceEl.html("");
        }
        
      },     
      num;

    function getVal(numIpt){
      num=parseInt(numIpt.val()||0,10)||0;
      num = isNaN(num) ? 0:num;
      return num;
    }

    
    //加一
    el.delegate('.inc', 'click', function(event) {
      var ipt = $(this).closest('.buy-num').find(".num_ipt");

      num=getVal(ipt)+1;
      f(num,ipt);
    });
    //减一
    el.delegate('.dec', 'click', function(event) {
      
      var ipt = $(this).closest('.buy-num').find(".num_ipt");
      num=getVal(ipt)-1;
      f(num,ipt);
    });
    //设置
    el.delegate('.num_ipt', 'keyup blur', function(event) {
      var ipt= $(this),
          num=ipt.val();
      if(num!=""){
        f(num,ipt);
      }
    });
    
  }
  //数量选择
  pub.buyNumSel=function(conf){
    var Conf={
      wrap:null,
      price:0,
      num:0,
      cbk:function(){}
    };
    $.extend(Conf,conf);
    
    var tpl=[
      '<p class="f-cf buy-num"><span class="dec"><i class="minus"></i></span> ',
        '<input type="tel" class="num_ipt" value="{num}" p="{price}" maxlength="4"> ',
        '<span class="inc"><i class="minus"></i><i class="plus"></i></span> ',
        '<font class="gray"><em class="text-yellow price"></em>&nbsp;&nbsp;</font>',
        '</p>'
    ].join("");

    if(Conf.wrap){
      Conf.wrap.html(tpl.ZK_format(Conf));
      pub.buyNum(Conf.wrap);
    }else{
      return tpl.ZK_format(Conf);
    }
  }

  //添加表格形式的表单
  pub.addTableList=function(conf){
    var Conf={
    tableEl:$(".exp-table"),
    sumNumIpt:$("#expLenIpt"),
    delBtn:true,
    fields:[]
  }
  $.extend(Conf, conf);

  function makeTh(){
    var th=[],
        arr=Conf.fields;
      for(var i=0,l=arr.length;i<l;i++){
        th.push('<th style="'+(arr[i]['thStyle']||"")+'">'+arr[i]['name']+'</th>');
      }
      alert(Conf.Btn);
      if(Conf.delBtn){
    	  th.push('<th>操作</th>'); 
      }
      Conf.tableEl.html('<tr>'+th.join("")+'</tr>');
  }
  makeTh();

  function addTr(o){
    var td=[],
        arr=Conf.fields,
        tpl='<td class="{id}" style="{tdStyle}"><input name="{id}" type="{type}" style="{iptStyle}" {attr} value="{val2}"></td>';

    for(var i=0,l=arr.length;i<l;i++){
        arr[i]['type']=arr[i]['type']||"text";
        arr[i]['val2'] =o[arr[i]['id']] ||  arr[i]['val'] ||"";
        arr[i]['attr'] = arr[i]['attr'] ||"";
        td.push(tpl.ZK_format(arr[i]));

      }
    if(Conf.delBtn){
    	td.push('<td><button class="btn btn-default del-btn" type="button"><i class="fa fa-fw fa-remove text-red"></i>删除</button></td>');
    }
    var trEl = $('<tr>'+td.join("")+'</tr>');
    Conf.tableEl.append(trEl);
    refresh();
    Conf.addCbk(trEl);
  }
  

  //刷新行的ext
  function refresh(){
    var num=0;
    Conf.tableEl.find("tr").each(function(i,el){
      if(i!=0){
        $(el).find("input").attr({'ext':i-1});
        num++;
      }
    })
    Conf.sumNumIpt.val(num);
  }

  //删除事件
  Conf.tableEl.delegate('.del-btn', 'click', function(event) {
    if(confirm("您是否要删除该行数据？")){
      var tr = $(this).closest('tr');
      tr.remove();
      refresh();
    }
  });
  



  var index=0;
  this.addTr=function(val){

    if($.isPlainObject(val)){//设置内容
        addTr(val);
    }else{
        val = $.isNumeric(val) && val>0 ? val :1;
        for(var i=0;i<val;i++){
          addTr({});
        }
    }
  
  
  }
}

  return pub;
    
  
  
})(jQuery);

function navControl(){


    var hash= location.hash.substr(1),
      lis = $(".treeview-menu li"),
      trees = $(".treeview"),
      ifm =$("#main_ifm");

  function click(el){
     var p=el.closest('li'),
         cls='active',
         name = el.attr("name");

      lis.removeClass(cls);
      trees.removeClass(cls);
      p.addClass(cls);
      p.closest('.treeview').addClass(cls);

      if(name){
        location.hash=name;
      }
      ifm.attr({src:el.attr("href")});
      $(document).scrollTop(0);
      return false;
  }

  ifm.height($(".sidebar").height());
  $(".sidebar-menu ").delegate('.treeview-menu a', 'click', function(event) {
      click($(this));
      return false;
  });

  if(hash){
    var initEl =$(".sidebar-menu a[name="+hash+"]");
    if(initEl.size()){
      click(initEl);
    }
  }

  //导航消息
  $(".navbar-nav .dropdown").hover(function(){
    $(this).addClass('open');
  },function(){
    $(this).removeClass('open');
  }).unbind('click').click(function(){
    var me = $(this).find(".dropdown-toggle"),
        href = me.attr("href"),
        ifm = $('#main_ifm');
        if(href!="#"){
          ifm.attr({'src':href})
        }
  });



}
try{
  if(typeof(top.Comm.popIfm.prototype.nid)=="undefined"){
    top.Comm.popIfm.prototype.nid=1;
  }else{
    top.Comm.popIfm.prototype.nid++;
  }

  if(typeof(top.Comm.popModal.prototype.nid)=="undefined"){
    top.Comm.popModal.prototype.nid=1;
  }else{
    top.Comm.popModal.prototype.nid++;
  }
}catch(e){}



$("body").ready(function($) {
  if($("#main_ifm").size()){
    navControl();
  }
  //操作浮层事件
    Comm.floatDiv(null,{
        divSelector:'ctr-div',
        btnSelector:"ctr-btn",
        contSelector:"ctr-float"
    })
  //若页面链接中带有formArgs参数
  var args=getHrefParam(),
      form = $("form");
  if(args && typeof(args['formArgs'])!="undefined"){
    var formArgs = args['formArgs'].ZK_toJson();
    for(var key in formArgs){
      form.append('<input type="hidden" value="'+decodeURIComponent(formArgs[key])+'" name="'+key+'"/>');
    }
  }


});
navControl();
