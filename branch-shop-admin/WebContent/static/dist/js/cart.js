
  Comm.Cart=function(conf){
     var Conf={
      addApi:null,
      delApi:null,
      getApi:null,
      updateApi:null,
      split:'|'
     },
     cartList=[],
     isInit=false;
     $.extend(Conf, conf);

     function loop(gid,cbk){
        for(var i=0,l=cartList.length;i<l;i++){
          if(cartList[i].gid==gid){
            cbk(cartList[i],i);
            return true;
          }
        }
        cbk(false);
        return false;
     }
     function api(url,data,cbk){
      $.getJSON(url,data,function(ret){
          if(ret && ret.success){
            cbk(true,ret);
          }else{
            cbk(false);
          }

        })
     }
    function change(cid,num,cbk){
       api(Conf.updateApi,
          {
           'cartId':cid,
           'num':num
          },
          function(type,ret){
            cbk(type);
          }
        );
    }

    function getCart(cbk){
        api(Conf.getApi,{},function(type,ret){
          cartList=ret.data;
          isInit=true;
          if(cbk){
            cbk()
          }
        });
    }



    var update=function(gid,num){//现有列表中，加,减
      loop(gid,function(o,i){
        if(o){
          o['num']+=num;
          if(o['num'] <=0){
            Comm.showMsg("宝贝不能再减少了。");
            o['num'] =1;
          }
          return o;
        }
        return false;
      })
    }
    var add=function(gid,obj){//添加到购物车
      loop(gid,function(o,i){
        if(!o){//添加
          cartList.push(obj);
        }else{//修改
          o['num']=parseInt(obj['num'],10)+parseInt(o['num'],10);
        }
      })

      return cartList;
    }
    var del=function(gid){//删除
      loop(gid,function(o,i){
        if(o){
          cartList.splice(i,1);
          return true;
        }else{
          return false;
        }
      })
    }
    var set =function(gid,num){//设置
      var oo=null;
      loop(gid,function(o){
        if(o){
          if(num<=0){
            Comm.showMsg("数量超出范围");
            num=1;
          }
          o['num']=num;
          oo=o;
        }
      })
      return oo;
    }
    var sum=function(){//求和
      var num = 0,selNum=0,selSum=0,sum = 0;
        for(var i=0,l=cartList.length;i<l;i++){
          if(cartList[i]['checked']){
            selNum+=parseInt(cartList[i]['num'],10);
            selSum+=cartList[i]['price']*cartList[i]['num'];
          }
          num+=parseInt(cartList[i]['num'],10);
          sum+=cartList[i]['price']*cartList[i]['num'];
        }
        sum = Math.round(sum*100)/100

        return {'num':num,'sum':sum,"selNum":selNum,"selSum":selSum};
    }

    function getList(cbk){
      if(isInit){
        return cbk(cartList);
      }else{
        getCart(function(){
          cbk(cartList);
        });//获取购物车的初始数量
      }
    }
    

    this.add=function(gid,num){
      function fun(){
        api(Conf.addApi,
          {
           'goodsId':gid,
           'num':num
          },
          function(type,data){
            if(type && data.data.length==1){
              var ret=data.data[0];
              //add(ret.title,ret.gid,ret.price,num,ret.max,ret.img,ret.guige,ret.pinpai);
              ret.num=num;
              ret.checked=true;
              add(gid,ret);
              Comm.showMsg('添加购物车成功~',"success");
              initCartList();
              cartBtnAction();
            }else{
              Comm.showMsg('添加购物车失败~');
            }
             console.log(cartList);
          }
        );

      }
      if(isInit){
          fun();
      }else{
        getCart(fun);
      }
      
    }
    this.update=function(gid,num){
      var o = update(gid,num);

      if(o){
        change(o['cid'],o['num'],function(type){
          if(!type){
              Comm.showMsg('更新商品数量失败');
              update(gid,num*-1);//再更新回原数量
            }
           console.log(cartList);
        });
        
      }else{
        Comm.showMsg('更新商品数量失败');
      }
      
    }
    this.del=function(gid,cid,cbk){
      api(Conf.delApi,
          {
           'goodsId':gid,
           'cartId':cid
          },
          function(type,ret){
            if(type){
              //Comm.showMsg('删除商品成功',"success");
              del(gid);
              cbk(true);
            }else{
              Comm.showMsg('删除商品失败');
              cbk(false);
            }
          }
        );
    }
    this.set=function(gid,num){
      var num0= 1;
      loop(gid,function(o,i){
        if(o){
          num0=o['num'];//原始数量
        }
      })
      var o=set(gid,num);
      if(o){
        change(o['cid'],o['num'],function(type){
          if(!type){
              Comm.showMsg('更新商品数量失败');
              set(gid,num0);//再更新回原数量
            }
        });
        
      }else{
        Comm.showMsg('更新商品数量失败');
      }
    }
    this.sum=function(){
      return sum();
    }
    this.getList=function(cbk){
      this.getList(cbk);      
    }

    //购物车的事件操作
    var liTpl=[          
          '<li>',
            '<div class="checkbox">',
              '<label>',
                '<input type="checkbox" {sel} name="sub-sel" value="{gid}">',
              '</label>',
            '</div>',
            '<div class="img">',
              '<img src="{img}" alt="">  ',          
            '</div>',
            '<div class="info">',
             '<h3 title="{title}">{title}</h3>',
              '<p>{pinpai} {guige}</p>',
              '<div class="price-num">',
                '<em class="text-red">￥{price}</em>',
                '<div class="num-sel"><p class="f-cf buy-num"><span class="dec"><i class="fa fa-fw fa-minus"></i></span> <input type="tel" maxlength="4" p="{price}" data-gid="{gid}" value="{num}" class="num_ipt"> <span class="inc"><i class="fa fa-fw fa-plus"></i></span> <font class="gray"><em class="text-yellow price"></em>&nbsp;&nbsp;</font></p></div>',
              '</div>',
            '</div>',
            '<div class="del" data-cid="{cid}" data-gid="{gid}">',
              '<i class="fa fa-fw fa-trash"></i>',
            '</div>',
          '</li>'].join(""),
      cartTpl=[
        '<div class="cart-wrap">',
      '<div class="cart-btn">',
        '<div class="cart-icon">',
          '<i class="fa fa-fw fa-cart-plus animated"></i>',
          
        '</div>',
        '购 物 车<br/>',
        '<em class="upper"></em>',
      '</div>',
      '<div class="cart-list">',
        '<div class="cart-head">',
          '<div class="checkbox">',
              '<label>',
                '<input type="checkbox" checked id="sel_all">全选',
              '</label>',
            '</div>',
        '</div>',
        '<ul id="cart_ul">',
        '加载中...',
        '</ul>',
        '<div class="ctrl">',
          '<div class="sum" id="sum_info"></div>',
          '<button id="simple_buy_btn" class="btn bg-maroon btn-flat margin" > <i class="fa fa-paypal"></i>单地址采购</button>',
          '<div class="btn-group">',
              '<button class="btn bg-orange btn-flat margin dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-file-excel-o"></i>多地址采购</button>',
              '<ul class="dropdown-menu" role="menu">',
                '<li><a id="goods_out_btn" href="#"><i class="fa fa-cloud-download"></i>导出表格</a></li>',
                '<li><a id="goods_in_btn" href="#"><i class="fa fa-cloud-upload"></i>导入表格</a></li>',
              '</ul>',
          '</div>',
        '</div>',
      '</div>',
    '</div>',

    ].join("");

    $("body").append(cartTpl);
    var cartWrap = $(".cart-wrap")
        listWrap = $("#cart_ul"),
        numDiv = $(".cart-btn .upper"),
        cartBtn = $(".cart-btn"),
        sumDiv = $("#sum_info");

    function cal(){
      var ss = sum();
      if(ss['num']){
        sumDiv.html("合计："+ss['selNum']+"件商品，"+ss["selSum"]+"元");
        numDiv.html(ss['num']);

      }else{
        sumDiv.html("");
        numDiv.html("");
      }
      setEmpty();
    }
    function reset(){
      var h=100+35,hh=cartWrap.height();
      listWrap.height(hh-h);
    }
    reset();
    $(window).resize(function(event) {
      reset();    });
    function setEmpty(){
      if(cartList.length==0){
        listWrap.html('<li class="info text-center">您的购物车是空的，快去挑选商品吧。</li>');
      }
    }
    function initCartList(){
      var arr= cartList,
          html=[];
      for(var i=0,l=arr.length;i<l;i++){
        arr[i]['checked']=true;
        if(typeof(arr[i]['checked'])=="undefined"){
          arr[i]['checked']=true;
        }
        arr[i]['sel'] = arr[i]['checked'] ? "checked" :"";
        
        html.push(liTpl.ZK_format(arr[i]));

      }
      if(html.length){
        listWrap.html(html.join(""));
      }

      cal();
    }
    var that = this;
    //初始化购物车
    getCart(initCartList);

    //数量事件
    Comm.buyNum(cartWrap,function(n,numIpt){
      var gid=numIpt.data("gid");
      if(n<1){
        Comm.showMsg("亲，不能再少了。");
        n=1;
        numIpt.val(n);
      }
      that.set(gid,n);
      cal();
    });

    //删除事件
    cartWrap.delegate('.del', 'click', function(event) {

      var me =$(this),
          gid = me.data("gid")
          cid = me.data("cid");
      if(confirm("确定删除该商品吗？")){
        that.del(gid,cid,function(ret){
          if(ret){
            var li =me.closest('li')
            li.fadeOut('slow', function() {
              li.remove();
              

            });
            cal();
          }
        });
      }
    });

    //全选事件
    Comm.selAll({
      mainCheckBoxSelector:"#sel_all",
      subCheckBoxSelector:"input[name=sub-sel]",
      selCbk:function(){
        $('input[name=sub-sel]').each(function(i,el){
          loop(el.value,function(o){
            if(o){
              o['checked'] = el.checked;
            }
          })
        });
        cal();

      }
    });

    //购物车按钮
    var show=false,w=300;
    cartBtn.click(function(){
      if(!show){
        show=true;
        cartWrap.animate({"right":"0px"});
      }else{
        show=false;
        cartWrap.animate({"right":-1*w+"px"})
      }
    });
    function cartBtnAction(){
      var cls = 'rubberBand',
          time = 1000,
          icon= cartBtn.find("i");
      icon.addClass(cls);
      setTimeout(function(){
        icon.removeClass(cls);
      },time);
    }

    //获取采购商品
    this.getBuyArgs=function(){
      var arr=[];
      for(var i=0,l=cartList.length;i<l;i++){
        if(cartList[i]['checked']){
          arr.push(cartList[i]['gid']+":"+cartList[i]['num'])
        }
      }
      return arr.join("|");
    }

    //单地址购买事件
    $("#simple_buy_btn").click(function(){
      var args=that.getBuyArgs();
      if(args==""){
        Comm.showMsg("请您先选择商品~");
      }
      Conf.addrBuy(args);
    })
    //多地址购买，导出
    $("#goods_out_btn").click(function(){
      var args=that.getBuyArgs();
      if(args==""){
        Comm.showMsg("请您先选择商品~");
      }
      Conf.xlsDown(args);
    })
    //多地址购买，导入
    $("#goods_in_btn").click(function(){
      Conf.xlsUp();
    });


  }
