
 if(typeof(ZK)=="undefined"){
    ZK={};
}

ZK.Page=function(config){
	this.init(config);
};
(function(packager){
	jQuery.extend(packager.prototype,{
		setConfig:function(config){
			var index=new Date().getTime();
			//id
			this.id="ZK_view_page_"+index;
			this.defArgs={
				preTxt:'上一页',
				nextTxt:'下一页',
				totalTxt:'共',
				recordsTxt:'条记录',
				isShowCount:true,
				isShowExtra:true,
				//切换列表函数
				changeList:null,
				//当前页
				page:1,
				//数据显示量
				showNum:10,
				//绑定的元素
				el:null,
				//分页标签显示量
				pageTagNum:5,
				total:0
			};
			this.args=jQuery.extend({},this.defArgs,config);
			//读取缓存,优先缓存数据，解决重绘时无法得到页数值 ,暂时屏蔽该功能
//			if(typeof (this.args["el"].data("page"))!="undefined"){
//				this.args.page=+this.args["el"].data("page");
//			}
			//页数总量
			this.pageSize=Math.ceil(this.args.total/this.args.showNum);
		},
		createHtml:function(){
			var temp='<font class="num">{0}'+this.args.total+'{1}</font>';
			this.args["el"].empty();

			if (this.args.total>0) {
				var ah=["<ul class='pagination' id='"+this.id+"'>"],z,j;
				var tagTotalSize=Math.ceil(this.pageSize/this.args.pageTagNum);
				//判断tag是否能整除
				var isClear=(this.pageSize%this.args.pageTagNum)==0?true:false;
				var curTagSize=Math.ceil(this.args.page/this.args.pageTagNum),tagStartNum=(curTagSize*this.args.pageTagNum)-this.args.pageTagNum+1;
				if (this.args.isShowCount) {
					temp=temp.ZK_format(this.args.totalTxt,this.args.recordsTxt);
				}
				ah.push('<li class="paginate_button previous '+(this.args.page>1 ? "":"disabled")+'"><a href="#" class="previous">'+this.args.preTxt+'</a></li>');
								//page 小于pagesize
				if (curTagSize>=tagTotalSize) {
					if (this.pageSize<this.args.pageTagNum) {
						for (var i=0,slen=this.pageSize; i<slen; i++) {
							ah.push('<li class="paginate_button '+((tagStartNum+i)==this.args.page? "active":"")+'"><a href="#" >'+(tagStartNum+i)+'</a></li>');
						}
						ah.push('<li class="paginate_button next '+(this.args.page==this.pageSize?"disabled":"")+'"><a href="#">'+this.args.nextTxt+'</a></li>');
					}else{
						if (!isClear) {
							tagStartNum=this.pageSize-this.args.pageTagNum+1;
						}
						for (var i=0,slen=slen; i<this.args.pageTagNum; i++) {
							ah.push('<li class="paginate_button '+((tagStartNum+i)==this.args.page? "active":"")+'"><a href="#" >'+(tagStartNum+i)+'</a></li>');
						}
						ah.push('<li class="paginate_button next '+(this.args.page==this.pageSize?"disabled":"")+'"><a href="#" class="next">'+this.args.nextTxt+'</a></li>');
					}
				}else if(curTagSize==1){
					if (this.pageSize<this.args.pageTagNum) {
						for (var i=0,slen=this.pageSize; i<slen; i++) {
							ah.push('<li class="paginate_button '+((tagStartNum+i)==this.args.page? "active":"")+'"><a href="#" >'+(tagStartNum+i)+'</a></li>');
						}
					}else {
						for (var i=0,slen=this.args.pageTagNum; i<slen; i++) {
							ah.push('<li class="paginate_button '+((tagStartNum+i)==this.args.page? "active":"")+'"><a href="#" >'+(tagStartNum+i)+'</a></li>');
						}
					}
					if(!(tagTotalSize-curTagSize == 1 && this.pageSize%this.args.pageTagNum == 1)){
						ah.push('<li class="paginate_button disabled"><a class="addOne" href="#">...</a></li>');	
					}
					ah.push('<li class="paginate_button"><a href="#">'+this.pageSize+'</a></li>');
					ah.push('<li class="paginate_button '+(this.args.page==this.pageSize?"disabled":"")+'"><a href="#" class="next">'+this.args.nextTxt+'</a></li>');
				}else{
						for (var i=0,slen=this.args.pageTagNum; i<slen; i++) {
							ah.push('<li class="paginate_button '+((tagStartNum+i)==this.args.page? "active":"")+'"><a href="#" >'+(tagStartNum+i)+'</a></li>');
						}
						if(!(tagTotalSize-curTagSize == 1 && this.pageSize%this.args.pageTagNum == 1)){
							ah.push('<li class="paginate_button disabled"><a class="addOne" href="#">...</a></li>');	
						}
						ah.push('<li class="paginate_button"><a href="#">'+this.pageSize+'</a></li>');
						ah.push('<li class="paginate_button '+(this.args.page==this.pageSize?"disabled":"")+'"><a href="#" style="" class="next">'+this.args.nextTxt+'</a></li>');
				}
				ah.push('</ul>');
				 if (this.args.isShowExtra) {
					ah.push('<span class="page-info">'+temp+'，<span class="num"><input type="text" class="page" placeholder="'+this.args.page+'"/>/'+this.pageSize+'页');
					ah.push('<a href="javascript:void(0)" class="btn btn-default">GO</a></span>');
				 }
				
				this.args["el"].append(ah.join(''));
				this.args["el"].data("page",this.args.page);


			}
		},
		bindEvent:function(){
			var _this=this,page=1,timer;
			jQuery('a',this.args["el"]).bind('click',function(event){
				event.preventDefault(); 
				var curTagSize=Math.ceil(_this.args.page/_this.args.pageTagNum),tagStartNum=(curTagSize*_this.args.pageTagNum)-_this.args.pageTagNum+1;
				if (!jQuery(this).hasClass("on")&&!jQuery(this).hasClass('gopage')) {
					if (jQuery(this).hasClass("previous")) {
						page=+(_this.args["el"].data("page"))-1;
					}else if (jQuery(this).hasClass("next")) {
						page=+(_this.args["el"].data("page"))+1;
					}else if (jQuery(this).hasClass('addOne')) {
						page=(curTagSize*_this.args.pageTagNum)+1;
					}else {
						page=+jQuery(this).text();
					}
					_this.args.changeList(page);
					_this.args["el"].data("page",page);
				}
				if (jQuery(this).hasClass('gopage')) {
					clearTimeout(timer);
					var pageNo=jQuery.trim(jQuery(':input',_this.args["el"]).val());
					if (isNaN(pageNo)) {
						pageNo=1;
					}
					if (pageNo<=0) {
						pageNo=1;
					}
					if (pageNo>=_this.pageSize) {
						pageNo=_this.pageSize;
					}
					_this.args.changeList(pageNo);
					_this.args["el"].data("page",pageNo);
				}
			});
			jQuery(':input',this.args["el"]).keyup(function(e) {
				clearTimeout(timer);
				var page=+jQuery(this).val(),keycode=e.keyCode;
				if (isNaN(page)) {
					page=1;
				}
				if (page<=0) {
					page=1;
				}
				if (page>=_this.pageSize) {
					page=_this.pageSize;
				}
				timer=setTimeout(function(){
					_this.args.changeList(page);
					_this.args["el"].data("page",page);
				},2000);
			});
		},
		init:function(config){
			this.setConfig(config);
			this.createHtml();
			this.bindEvent();
		}
	});
})(ZK.Page);