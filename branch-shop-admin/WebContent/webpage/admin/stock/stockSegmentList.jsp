<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="stockSegmentList" title="出库详细单" actionUrl="stockSegmentController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="仓库分区名称或者是货柜名称" field="name" ></z:dgCol>
   <z:dgCol title="上级仓储区域或者货柜编码" field="preid" ></z:dgCol>
   <z:dgCol title="下级仓储区域或者货柜编码" field="nextid" ></z:dgCol>
   <z:dgCol title="所属仓库编码（仓库最高级别节点）" field="stockid" ></z:dgCol>
   <z:dgCol title="备注" field="remarks" ></z:dgCol>
   <z:dgCol title="updateTime" field="updateTime" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="stockSegmentController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="stockSegmentController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="stockSegmentController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="stockSegmentController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>