<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>出入库商品记录表</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="stockProductController.do?save">
		<input id="id" name="id" type="hidden" value="${stockProductPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">库存关联商品唯一编码:</label>
		      <input class="inputxt" id="productid" name="productid" 
					   value="${stockProductPage.productid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">所属公司:</label>
		      <input class="inputxt" id="companyid" name="companyid" 
					   value="${stockProductPage.companyid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">商品所在仓库:</label>
		      <input class="inputxt" id="stockid" name="stockid" 
					   value="${stockProductPage.stockid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">库存数量:</label>
		      <input class="inputxt" id="stocknum" name="stocknum" 
					   value="${stockProductPage.stocknum}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">库存预警值:</label>
		      <input class="inputxt" id="alertnum" name="alertnum" 
					   value="${stockProductPage.alertnum}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">库存状态 1 充足 2 缺货 3 补货:</label>
		      <input class="inputxt" id="status" name="status" 
					   value="${stockProductPage.status}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>