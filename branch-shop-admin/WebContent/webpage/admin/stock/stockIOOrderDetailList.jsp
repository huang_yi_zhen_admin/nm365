<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="stockIOOrderDetailList" title="出库详细单" actionUrl="stockIOOrderDetailController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="出入库商品编码" field="productid" ></z:dgCol>
   <z:dgCol title="出、入库数量" field="iostocknum" ></z:dgCol>
   <z:dgCol title="进货单价" field="unitprice" ></z:dgCol>
   <z:dgCol title="总成本" field="totalprice" ></z:dgCol>
   <z:dgCol title="备注" field="remark" ></z:dgCol>
   <z:dgCol title="出入库单编码" field="stockorderid" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="stockIOOrderDetailController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="stockIOOrderDetailController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="stockIOOrderDetailController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="stockIOOrderDetailController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>