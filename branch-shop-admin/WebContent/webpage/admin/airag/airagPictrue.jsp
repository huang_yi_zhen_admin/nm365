<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>大气候图片数据</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="airagPictrueController.do?save">
		<input id="id" name="id" type="hidden" value="${airagPictruePage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">aeid:</label>
		      <input class="inputxt" id="aeid" name="aeid" 
					   value="${airagPictruePage.aeid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">collectTime:</label>
		      <input class="inputxt" id="collectTime" name="collectTime" 
					   value="${airagPictruePage.collectTime}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">图片压缩率:</label>
		      <input class="inputxt" id="ratio" name="ratio" ignore="ignore"
					   value="${airagPictruePage.ratio}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">图片1:</label>
		      <input class="inputxt" id="pic1" name="pic1" ignore="ignore"
					   value="${airagPictruePage.pic1}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">图片2:</label>
		      <input class="inputxt" id="pic2" name="pic2" ignore="ignore"
					   value="${airagPictruePage.pic2}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">图片3:</label>
		      <input class="inputxt" id="pic3" name="pic3" ignore="ignore"
					   value="${airagPictruePage.pic3}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">图片4:</label>
		      <input class="inputxt" id="pic4" name="pic4" ignore="ignore"
					   value="${airagPictruePage.pic4}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>