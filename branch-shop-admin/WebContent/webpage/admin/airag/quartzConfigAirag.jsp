<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>大气候数据自动抓取配置</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="quartzConfigAiragController.do?save">
		<input id="id" name="id" type="hidden" value="${quartzConfigAiragPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">农眼id:</label>
		      <input class="inputxt" id="aeid" name="aeid" 
					   value="${quartzConfigAiragPage.aeid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">接口名称:</label>
		      <input class="inputxt" id="interfaceName" name="interfaceName" 
					   value="${quartzConfigAiragPage.interfaceName}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">接口地址:</label>
		      <input class="inputxt" id="interfaceUrl" name="interfaceUrl" 
					   value="${quartzConfigAiragPage.interfaceUrl}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">上次调用时间:</label>
		      <input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="lastCallTime" name="lastCallTime" ignore="ignore"
					     value="<fmt:formatDate value='${quartzConfigAiragPage.lastCallTime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">失败尝试次数:</label>
		      <input class="inputxt" id="failTryTimes" name="failTryTimes" ignore="ignore"
					   value="${quartzConfigAiragPage.failTryTimes}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">获取数据条数:</label>
		      <input class="inputxt" id="fetchsize" name="fetchsize" ignore="ignore"
					   value="${quartzConfigAiragPage.fetchsize}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">用户id，调接口时用:</label>
		      <input class="inputxt" id="userId" name="userId" ignore="ignore"
					   value="${quartzConfigAiragPage.userId}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">客户id，获取token时候用:</label>
		      <input class="inputxt" id="clientId" name="clientId" ignore="ignore"
					   value="${quartzConfigAiragPage.clientId}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">客户密码，获取token时候用:</label>
		      <input class="inputxt" id="clientSecret" name="clientSecret" ignore="ignore"
					   value="${quartzConfigAiragPage.clientSecret}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">授权方式，获取token时候用:</label>
		      <input class="inputxt" id="grantType" name="grantType" ignore="ignore"
					   value="${quartzConfigAiragPage.grantType}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">用户名称，获取token时候用:</label>
		      <input class="inputxt" id="username" name="username" ignore="ignore"
					   value="${quartzConfigAiragPage.username}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">用户密码，获取token时候用:</label>
		      <input class="inputxt" id="password" name="password" ignore="ignore"
					   value="${quartzConfigAiragPage.password}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>