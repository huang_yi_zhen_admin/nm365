<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="airagPictrueList" title="大气候图片数据" actionUrl="airagPictrueController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="aeid" field="aeid" ></z:dgCol>
   <z:dgCol title="collectTime" field="collectTime" ></z:dgCol>
   <z:dgCol title="图片压缩率" field="ratio" ></z:dgCol>
   <z:dgCol title="图片1" field="pic1" ></z:dgCol>
   <z:dgCol title="图片2" field="pic2" ></z:dgCol>
   <z:dgCol title="图片3" field="pic3" ></z:dgCol>
   <z:dgCol title="图片4" field="pic4" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="airagPictrueController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="airagPictrueController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="airagPictrueController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="airagPictrueController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>