<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="companyExpressList" title="发货地址" actionUrl="companyExpressController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="company表id" field="companyid" ></z:dgCol>
   <z:dgCol title="省份，不能漏省字" field="province" ></z:dgCol>
   <z:dgCol title="城市，不能漏市字" field="city" ></z:dgCol>
   <z:dgCol title="不能漏区字" field="area" ></z:dgCol>
   <z:dgCol title="详细地址" field="address" ></z:dgCol>
   <z:dgCol title="快递商编码" field="shippercode" ></z:dgCol>
   <z:dgCol title="快递类型" field="exptype" ></z:dgCol>
   <z:dgCol title="快递网点标识_由快递网点提供" field="sendsite" ></z:dgCol>
   <z:dgCol title="电子面单账号 由快递网点提供 " field="customername" ></z:dgCol>
   <z:dgCol title="电子面单密码 由快递网点提供 " field="customerpwd" ></z:dgCol>
   <z:dgCol title="月结编码_由快递网点提供 " field="monthcode" ></z:dgCol>
   <z:dgCol title="支付类型1-现付，2-到付，3-月结，4-第三方支付" field="paytype" ></z:dgCol>
   <z:dgCol title="是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0" field="isnotice" ></z:dgCol>
   <z:dgCol title="发件人公司" field="company" ></z:dgCol>
   <z:dgCol title="发件人姓名" field="name" ></z:dgCol>
   <z:dgCol title="发件人电话" field="tel" ></z:dgCol>
   <z:dgCol title="发件人手机" field="mobile" ></z:dgCol>
   <z:dgCol title="发货点邮政编码" field="postcode" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="companyExpressController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="companyExpressController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="companyExpressController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="companyExpressController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>