import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as mutations from './mutations'
Vue.use(Vuex)

const store = new Vuex.Store({
    actions,
    mutations,

    state: {
        webSite: [],
        buyGoodsId: 0,
        goodsList: { //商品列表
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'goods',
            data: []
        },
        goodsSearch: { //商品搜索列表
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'goodsSearch',
            data: []
        },
        cartList: {
            "pageNo": 1,
            "totalPage": 0,
            "pageSize": 10,
            "page": 'cart',
            data: []
        },
        proviceData: [],
        searchData: {
            goodsSearch: {},
            goodsList: {},
            orderList: {},
            clientList: {}
        },
        buyData: {

        },
        power: { //权限
            'waitCheck': 1, //是否在等待校验接口，校验之后，设置为0
            'clientStat': '8af4a3da58210ee5015821117f030008',
            'saleStat': '8af4a3da58210ee501582110a8360004',
            'goodsStat': '8af4a3da58210ee50158211120d20006',
            'salerStat': '4028802c5779096b01577915ff9d0012',
            'orderList': '402881e75797ce56015797d465320004',
            'goodsList': '8af4a3da58aa50a10158b34945f00001',
            'clientList': '402881e757673e3f01576740023e0004',
            'cashStat': '402881e757741c66015775640997000b',
            'auditStat': '402881e757741c6601577567b90b0019',
            'myCountStat': '4028802f58146bd5015814935fa3001f',
            'myInfoStat': '402881e75762c77a015762ccd62e000b'
        }


    },
    getters: {

        webSite(state) {
            console.log(state.webSite)
            return state.webSite;
        },
        userPower(state) {
            return state.power;
        }
        /*listDate(state, { listName }) {
            console.log(listName)
            return state[listName].data;
        }*/

    }
})

export default store