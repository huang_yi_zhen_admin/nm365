import axios from 'axios'
const $ajax = axios
    //const baseUrl = 'http://admin.my.nongmao365.com/'
const baseUrl = "";
const isLocalApi = process.env.NODE_ENV == "development" ? true : false;
const localApi = isLocalApi ? '/api' : "";
const qs = require('qs');
//格式化参数
function json2Str(json) {
    let arr = [];
    for (let key in json) {
        arr.push(key + '=' + encodeURIComponent(json[key]));
    }
    return arr.join("&");
}
//将数组的返回值，格式化为json格式
function formatRet(files, rows) {
    let args = files.split(','),
        arr = [];
    for (let i = 0, l = rows.length; i < l; i++) {
        let o = {};
        for (let j = 0, k = rows[i].length; j < k; j++) {
            o[args[j]] = rows[i][j];
        }

        arr.push(o);
    }
    return arr;
}

export function fetch(url, data = {}, method = 'GET') {
    if (method == "GET") {
        url += url.indexOf("?") == -1 ? "?" : "";
        url += json2Str(data);
    }

    return new Promise((resolve, reject) => {
        $ajax({
            method: method,
            url: baseUrl + url,
            data: qs.stringify(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(ret => {
            if (ret.status == 200) {
                if (data['field'] && ret.data.data && ret.data.data.rows) { //格式化返回值
                    ret.data.data.rows = formatRet(data['field'], ret.data.data.rows);
                } else if (ret.data['obj']) { //格式化json格式的数据
                    if (typeof(ret.data['obg']) == 'array') { //列表的
                        ret.data.data = {
                            rows: ret.data['obj']
                        }
                    } else { //详情的
                        ret.data = ret.data['obj']
                    }
                }
                resolve(ret.data);
            } else {
                reject(ret)
            }
        }).catch(error => {
            reject()
        })
    })

}
export function fetchProvice() {
    let url = '/territoryController/cascadeArea'
    return fetch(url);
}
export function fetchZoneById(id) {

    return fetch('/territoryController/cascadeArea', { id });
}
export function getSaleMan() {
    let url = localApi + '/userController/datagridSaleman'
    return fetch(url, { 'field': 'id,realname', 'pageNo': 1, 'showNum': 10000 });
}
export function getCopartner() {
    let url = localApi + '/userController/datagridCopartner'
    return fetch(url, { 'field': 'id,username,status,tsuserCopartner_partnercompany,tsuserCopartner_partnercontactman,tsuserCopartner_partnercontactphone,tsuserCopartner_withdrawtype,tsuserCopartner_partneraccountdes,tsuserCopartner_partneraccountname,tsuserCopartner_partneraccount', 'pageNo': 1, 'showNum': 10000 });
}
export function getLev() {
    let url = localApi + '/userController/datagridUserType'
    return fetch(url, { 'field': 'id,typename', 'pageNo': 1, 'showNum': 10000 });
}
export function getDetailById({ id, type }) {
    let map = {
        'goodsInfo': localApi + '/productController/productDetailInfo', // 商品详情
        'userAddrInfo': localApi + '/userController/getClientsAddressMap', //获取用户地址信息
        'websiteInfo': localApi + '/mobileController/companyPlatformDetail', //网站信息
        'companyInfo': localApi + '/mobileController/companyInfoDetail' //企业信息
    }
    let url = map[type];
    return fetch(url, { 'id': id });
}
export function getListByType(type, data) {
    let map = {
            'goodsList': localApi + '/shoppingController/pShoppingDatagrid', //商品列表
            'goodsSearch': localApi + '/shoppingController/pShoppingDatagrid', //商品列表
            'cartList': localApi + '/userBasketController/datagrid',
            'powerList': localApi + '/loginController/getUserFunctionIdList' //权限列表
        },
        url = map[type];
    return fetch(url, data)
}

export function getStatDataByType(type, data) {
    let map = {
            'cashStat': localApi + '/mobileController/getMoneyStatis', // 资金明细统计
            'auditStat': localApi + '/mobileController/getAuditFundStatistics', //资金审核统计
            'clientStat': localApi + '/dataStatisticsController/getqdxs', //采购商数据统计
            'saleStat': localApi + '/dataStatisticsController/getic', //销售统计
            'goodsStat': localApi + '/dataStatisticsController/getcpbl', //产品统计
            'salerStat': localApi + '/dataStatisticsController/getyewu', //业务员
            'myStat': localApi + '/mobileController/personalCenter' //个人统计
        },
        url = map[type];
    return fetch(url, data)
}

export function getListById(type, data) {
    let map = {
            'goodsList': localApi + '/mobileController/getOrderListByPayBillsId'
        },
        url = map[type];
    return fetch(url, data);
}
export function submitData(type, data) {
    let map = {
            'addCart': localApi + '/userBasketController/addCart', //添加到购物车
            'addOrder': localApi + '/shoppingController/mOrderSubmit' //下单接口
        },
        url = map[type];
    return fetch(url, data, "POST");
}