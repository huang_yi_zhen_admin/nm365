export default {

    methods: {
        data() {
            return {

            }
        },
        jump(to) {
            if (this.$router) {
                this.$router.push(to)
            }
        },
        extend(obj1 = {}, obj2 = {}) {
            for (let k in obj2) {
                obj1[k] = obj2[k];
            };
            return obj1;
        },
        getScrollTop(doc) {
            var _doc = doc || document;
            return Math.max(_doc.documentElement.getScrollTop || 0, _doc.body.scrollTop || 0, window.screenY || 0);
        },
        setScrollTop(st) {
            window.scrollTo(0, st);
        },
        showPop(mainViewShow, popView, popCommponet) {
            this.popInfo.mainViewShow = mainViewShow;
            this.popInfo.popView = popCommponet;
            this.scrollTop = this.getScrollTop();
            this.popInfo.mainViewShow = false;
        },
        closePop() {
            this.popInfo.popView = "";
            this.popInfo.mainViewShow = true;
            var that = this;
            setTimeout(function() {
                that.setScrollTop(that.scrollTop);
            }, 0);

        },
        getScreenSize() {
            return {
                width: window.screen.width,
                height: window.screen.width
            }
        },
        getDomPosition(dom) {
            let left = dom.offsetLeft,
                top = dom.offsetTop,
                current = dom.offsetParent;

            while (current !== null) {
                left += current.offsetLeft;
                top += current.offsetTop;
            }
            return { top: top, left: left };

        },
        getDomSize(dom) {
            var style = null;
            if (window.getComputedStyle) {
                style = window.getComputedStyle(dom, null); // 非IE
            } else {
                style = dom.currentStyle; // IE
            }
            return {
                height: parseFloat(style.height, 10) + parseFloat(style.paddingTop, 10) + parseFloat(style.paddingBottom, 10) + parseFloat(style.borderTopWidth, 10) + parseFloat(style.borderBottomWidth, 10),
                width: parseFloat(style.width, 10) + parseFloat(style.paddingLeft, 10) + parseFloat(style.paddingRight, 10) + parseFloat(style.borderLeftWidth, 10) + parseFloat(style.borderRightWidth, 10)
            }
            return style;
        },
        getFormData(dom) {
            dom = dom || document;

            let items = dom.getElementsByClassName("item"),
                data = {};
            for (let i = 0, l = items.length; i < l; i++) {
                let ret = items[i];
                data[ret.name] = (ret.value);
                if (ret.tagName.toLocaleUpperCase() == "SELECT") {
                    let vName = ret.getAttribute("vname");
                    if (vName) {
                        data[vName] = (this.getTxtFromSelect(ret));
                    }
                }
            }
            return data;
        },
        getSearchInfo(dom) {
            dom = dom || document;

            let items = dom.getElementsByClassName("row"),
                data = [];

            for (let i = 0, l = items.length; i < l; i++) {
                let row = items[i],
                    label = row.getElementsByTagName("label"),
                    item = row.getElementsByClassName("item"),
                    txt = [];
                if (label) {
                    for (let j = 0, k = item.length; j < k; j++) {
                        let ret = item[j],
                            temp = ret.value;
                        if (temp && temp != "") {
                            if (ret.tagName.toLocaleUpperCase() == "SELECT") {
                                temp = this.getTxtFromSelect(ret);
                            }
                            txt.push(temp);
                        }
                    }
                    if (label && txt.length) {
                        data.push({
                            key: label[0].innerHTML,
                            value: txt.join(";")
                        })
                    }
                }

            }
            return data;
        },
        getTxtFromSelect(sel) {
            let el = sel.options[sel.selectedIndex];
            return el ? el.innerHTML : "";
        },
        bindEvent: function(div, evtType, cbk) {
            function fun(e) {
                try {
                    Object.defineProperty(e, 'currentTarget', { // 重写currentTarget对象 与jq相同
                        value: div,
                        writable: true,
                        enumerable: true,
                        configurable: true
                    })
                } catch (e) {
                    // ios 7下对 e.currentTarget 用defineProperty会报错。
                    // 报“TypeError：Attempting to configurable attribute of unconfigurable property”错误
                    // 在catch里重写
                    console.error(e.message)
                    e.currentTarget = div
                }
                e.preventDefault();
                cbk && cbk();
                //console.log(evtType)
            }
            try {
                div.removeEventListener(evtType, fun)
            } catch (e) {

            }
            div.addEventListener(evtType, fun, false);
        },

        initLoadingMore(id, cbk) {
            let div = document.getElementById(id);
            this.bindEvent(div, 'touchend', cbk);
            // this.bindEvent(div, 'touchmove', cbk);
        },
        getListByType(searchArg, that = this, cbk) {
            that.isLoading = true;
            let data = {};
            if (that.field) {
                data.field = that.field || "";
            }

            that.extend(data, searchArg || that.searchArg);
            that.$store.dispatch("FETCH_STATE_LIST_DATA", {
                data: data,
                listName: that.listName
            }).then(ret => {
                that.isLoading = false;
                cbk && cbk(ret);
            });
        },
        //获取购物车的数量
        getCartNum() {
            let num = 0;
            this.$store.dispatch("FETCH_STATE_LIST_DATA", {
                data: {},
                listName: 'cartList'
            }).then(ret => {
                ret.data.forEach(item => {
                    num += Number(item.num);
                })
                this.cartNum = num;
            });
        },
        //添加到购物车中
        addCartApi(pid, uid, num) {

            if (!uid || !pid) {
                this.showMsgMix({ type: 'info', txt: "请先选择要添加到购物车的商品" });
                return;
            }
            num = num || 1;

            this.$store.dispatch("SUBMIT_DATA", { type: 'addCart', data: { pid, uid, num } }).then(ret => {
                if (ret.success) {
                    this.cartNum += Number(num);
                    this.showMsgMix({ type: 'suc', txt: "添加购物车成功" });
                } else {
                    this.showMsgMix({ type: 'info', txt: ret && ret.msg || "添加购物车失败" });
                }
            })
        },

        getDetailById: function(cbk) {
            this.$store.dispatch("FETCH_DETAIL_DATA", { type: this.detailType, id: this.id }).then(ret => {
                this.data = ret;
                cbk && cbk(ret);

            })
        },
        showMsgMix: function(conf) {
            this.isPopMsg = true;
            this.popType = conf.type || 'info';
            this.popTxt = conf.txt || "";
            let that = this;
            setTimeout(function() {
                that.isPopMsg = false;
                conf.cbk && conf.cbk();
            }, 3000);
        }
    }
}