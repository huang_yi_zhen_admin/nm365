package cn.gov.xnc.system.web.system.controller.core;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSComponent;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.ComponentService;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

/**
 * 服务组件管理
 */
@Scope("prototype")
@Controller
@RequestMapping("/componentController")
public class ComponentController extends BaseController {
	private static final Logger logger = Logger.getLogger(ComponentController.class);
	@Autowired
	private SystemService systemService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private ComponentService componentService;
	
	@RequestMapping(value = "list")
	public ModelAndView componentList(HttpServletRequest request) {
		return new ModelAndView("admin/component/list");
	}
	
	/**
	 * AJAX 加载服务组件
	 * @param component
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagrid")
	public void datagrid(TSComponent component,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		Map<String, Object> params = componentService.getParams(component);
		params.put("startTime", startTime);
		params.put("endTime", endTime);
		params.put("isdel", "N");
		params.put("dataGrid", dataGrid);
		componentService.findListPageByParams(params);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 表单界面/保存/更新
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "form")
	public ModelAndView form(TSComponent component,HttpServletRequest request, HttpServletResponse response){
		TSComponent com = new TSComponent();
		if(StringUtil.isNotEmpty(component.getId())){
			com = systemService.getEntity(TSComponent.class, component.getId());
		}
		List<TSDictionary> serviceType = componentService.checkServiceType();
		List<TSDictionary> seviceOFF = componentService.checkServiceSwitch();
		request.setAttribute("serviceType", serviceType);
		request.setAttribute("seviceOFF", seviceOFF);
		request.setAttribute("component", com);
		return new ModelAndView("admin/component/form");
	}
	
	/**
	 * 提交
	 * @param component
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "submit")
	@ResponseBody
	public AjaxJson submit(TSComponent component, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		try {
			if(StringUtil.isNotEmpty(component.getId())){
				String desc = component.getComponentDesc();
				String name = component.getComponentName();
				TSDictionary type = component.getComponentType();
				TSDictionary value = component.getComponentValue();
				component = systemService.getEntity(TSComponent.class, component.getId());
				component.setComponentType(type);
				component.setComponentDesc(desc);
				component.setComponentName(name);
				component.setComponentValue(value);
				component.setUpdateTime(DateUtils.getDate());
				systemService.updateEntitie(component);
			}else{
				component.setIsdel("N");
				component.setCreateTime(DateUtils.getDate());
				component.setUpdateTime(DateUtils.getDate());
				systemService.save(component);
			}
			result.setSuccess(true);
			result.setMsg("操作完成");
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("保存服务组件出错：" + e);
			systemService.addLog("保存服务组件出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return result;
	}
	
	/**
	 * 删除信息
	 * @param component
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson delete(TSComponent component, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		try {
			if(StringUtil.isNotEmpty(component.getId())){
				component = systemService.getEntity(TSComponent.class, component.getId());
				component.setIsdel("Y");
				systemService.updateEntitie(component);
				result.setSuccess(true);
				result.setMsg("操作完成");
			}else{
				result.setSuccess(false);
				result.setMsg("操作失败");
			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("更新服务组件出错：" + e);
			systemService.addLog("更新服务组件出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return result;
	}
}
