package cn.gov.xnc.system.web.system.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.Client;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.TSUserAccessTokenEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSUserAccesslogEntity;

public interface UserAccessServiceI extends CommonService{
	
	//订单状态枚举
	public enum  Status{
		VALID("有效", "1"), INVALID("无效", "2");
	    
		private String name ;
	    private String value ;
	     
	    private Status( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
	
	/**
	 * 生成访问令牌
	 * @return
	 * @throws Exception
	 */
	public String generateAccessToken() throws Exception;
	
	/**
	 * 获取请求访问令牌
	 * @return
	 * @throws Exception
	 */
	public String getRequestAccessToken(HttpServletRequest req) throws Exception;
	
	/**
	 * 获取系统给用户颁发的访问令牌
	 * @return
	 * @throws Exception
	 */
	public TSUserAccessTokenEntity getUserAccessTokenInfo(String token) throws Exception;
	
	/**
	 * 获取系统给用户颁发的访问令牌
	 * @return
	 * @throws Exception
	 */
	public List<TSUserAccessTokenEntity> getUserActiveTokenByUserId(String userid) throws Exception;
	
	/**
	 * 保存访问记录
	 * @throws Exception
	 */
	public TSUserAccesslogEntity saveAccessLog(HttpServletRequest req) throws Exception;
	
	/**
	 * 保存更新访问令牌
	 * @throws Exception
	 */
	public void activeAccessToken(String token, TSUserAccesslogEntity lastAccesslog) throws Exception;
	
	/**
	 * 把新的访问令牌写到请求返回头部和Cookie中
	 * @param req
	 * @param resp
	 * @throws Exception
	 */
	public void writeAccessToken2client(String token, HttpServletRequest req, HttpServletResponse resp) throws Exception;
	
	/**
	 * 访问权限认证
	 * @throws Exception
	 */
	public boolean authenUserAccess(HttpServletRequest req, HttpServletResponse resp) throws Exception;
	
	
	/**
	 * 判断令牌是否有效
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public boolean isTokenValid(TSUserAccessTokenEntity tokeninfo) throws Exception;
	
	/**
	 * 根据访问令牌重新建立会话数据
	 * @param lastToken
	 * @return
	 * @throws Exception
	 */
	public Client renewClient(TSUserAccessTokenEntity tokeninfo, HttpSession sesseion, HttpServletRequest req, HttpServletResponse resp) throws Exception;
	
	
	/**
	 * 初始化会话数据
	 * @param tokeninfo
	 * @param sesseion
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	public Client initClient(TSUser user, HttpSession session, HttpServletRequest req, HttpServletResponse resp) throws Exception;
	
	
	/**
	 * 清除会话数据
	 * @param user
	 * @param session
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	public void removeClient(HttpServletRequest req) throws Exception;
	
	
}
