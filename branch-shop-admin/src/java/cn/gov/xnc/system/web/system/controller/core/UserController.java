package cn.gov.xnc.system.web.system.controller.core;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.UserCopartnerEntity;
import cn.gov.xnc.admin.copartner.service.UserCopartnerAccountServiceI;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.message.service.impl.MessageTemplateServiceImpl;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.ListtoMenu;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.PasswordUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.SetListSort;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.core.util.oConvertUtils;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.Client;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserClientsEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserStaffEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserTypeEntity;
import cn.gov.xnc.system.web.system.service.CompanyService;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserService;
import cn.gov.xnc.system.web.system.servlet.RandCodeImageServlet;



/**
 * @ClassName: UserController
 * @Description: TODO(用户管理处理类)
 * @author zero
 */
@Scope("prototype")
@Controller
@RequestMapping("/userController")
public class UserController {

	private static final Logger logger = Logger.getLogger(UserController.class);

	private SystemService systemService;
	private String message = null;
	
	@Autowired
	private UserService userService ;
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private MessageTemplateServiceI messageTemplateService;
	@Autowired
	private UserCopartnerAccountServiceI userCopartnerAccountService;
	
	@Autowired
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
	
	
	
	
	/**
	 * 员工用户列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "userStafflist")
	public String userStafflist(HttpServletRequest request) {
			
		return "system/user/userStaffList";
	}
	
	/**
	 * easyuiAJAX 员工用户用户列表请求数据 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridStaff")
	public void datagridStaff(TSUser user, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
			String type[] ="1,2,4".split(",");
			cq.in("type", type);
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, user);
			cq.addOrder("createDate", SortDirection.desc);
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	

	/**
	 * 员工列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdateStaff")
	public String addorupdateStaff( TSUser user, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(user.getId())) {
			user = systemService.getEntity(TSUser.class, user.getId());
			List<TSRoleUser> ru = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());//更新用户权限
			String roleUserId ="";
			String roleUserName ="";
			try {
				if( ru != null && ru.size() > 0){
					for(TSRoleUser tSRoleUser : ru){
						roleUserName +=tSRoleUser.getTSRole().getRoleName()+",";
						roleUserId +=tSRoleUser.getTSRole().getId()+",";
					}
					//过滤最后一个,
					roleUserId = roleUserId.substring(0, roleUserId.lastIndexOf(",")) ; 
					roleUserName = roleUserName.substring(0, roleUserName.lastIndexOf(",")) ; 
				}
			} catch (Exception e) {
				
			}
			
			req.setAttribute("roleUserId", roleUserId);
			req.setAttribute("roleUserName", roleUserName);
			req.setAttribute("user", user);
		}else {
			user = new TSUser();
			 UserStaffEntity tsuserstaff = new UserStaffEntity();
			 user.setTsuserstaff(tsuserstaff);
			req.setAttribute("roleUserId", "");
			req.setAttribute("roleUserName", "");
			req.setAttribute("user", user);
			
		}
		return "system/user/userStaff";
	}
	
	/**
	 * 员工信息录入更新
	 * 
	 * @param user
	 * @param req
	 * @return
	 */

	@RequestMapping(value = "saveUserStaff")
	@ResponseBody
	public AjaxJson saveUserStaff(HttpServletRequest req, TSUser user) {
		 AjaxJson j = new AjaxJson();
		// 得到用户的角色
		String roleid = oConvertUtils.getString(req.getParameter("roleid"));
		if (StringUtil.isNotEmpty(user.getId())) {//更新用户信息
			TSUser u = systemService.getEntity(TSUser.class, user.getId());
			message = "用户: " + u.getUserName() + "更新成功！";
			try {
				//个人账户禁止更新
				user.setTsuserSalesman(u.getTsuserSalesman());
				//是否为业务员角色设置
				if( StringUtil.isNotEmpty(roleid) ){
					TSRole  tsRole = systemService.getEntity(TSRole.class, roleid);
					if(tsRole != null && "yewuyuan".equals(tsRole.getRoleCode())){
						user.setType("4");
					}
				}else{
					user.setType("2");
				}
				MyBeanUtils.copyBeanNotNull2Bean(user , u);
				
				UserStaffEntity ts = user.getTsuserstaff(); 
				UserStaffEntity t = u.getTsuserstaff(); 
				ts.setId(t.getId());
				
				MyBeanUtils.copyBeanNotNull2Bean(ts , t);
				systemService.saveOrUpdate(t);//更新用户附属信息
				systemService.saveOrUpdate(u);
				List<TSRoleUser> ru = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());//更新用户权限
				systemService.deleteAllEntitie(ru);
				if (StringUtil.isNotEmpty(roleid)) {
					saveRoleUser(user, roleid);
				}
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "用户: " + user.getUserName() + "更新失败！";
				j.setSuccess(false);
			}
		} else {//创建用户
			TSUser operater = ResourceUtil.getSessionUserName();
			CriteriaQuery cq = new CriteriaQuery(TSUser.class);
			cq.eq("userName",user.getUserName());
			cq.eq("company.id", operater.getCompany().getId());
			cq.add();
			
			List<TSUser> userlist = systemService.getListByCriteriaQuery(cq, false);
			if(null != userlist && userlist.size() > 0){
				message = "用户: " + user.getUserName() + "已经存在";
				j.setSuccess(false);
			}
			else 
			{
				message = "用户: " + user.getUserName() + "添加成功";
				user.setPassword(PasswordUtil.md5(user.getPassword()));
				//是否为业务员角色设置
				if( StringUtil.isNotEmpty(roleid) ){
					TSRole  tsRole = systemService.getEntity(TSRole.class, roleid);
					if(tsRole != null && "yewuyuan".equals(tsRole.getRoleCode())){
						user.setType("4");
					}
				}else{
					user.setType("2");
				}	
				//获取用户的权限级别
				UserStaffEntity ts = user.getTsuserstaff(); 
				Serializable id = systemService.save(ts);
					ts.setId((String) id);
				//个人账号信息	
				UserSalesmanEntity se = new UserSalesmanEntity(); 
					Serializable seid = systemService.save(se);
						se.setId((String) seid);
				user.setTsuserSalesman(se);		
				user.setTsuserstaff(ts);
				systemService.save(user);
				if (StringUtil.isNotEmpty(roleid)) {
					saveRoleUser(user, roleid);
				}
				systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			}
		}
		j.setMsg(message);

		return j;
	}
	
	
	/**
	 * 账号删除
	 * 
	 * @param user
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(TSUser user, HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		
		if( StringUtil.isNotEmpty(user.getId()) ){
			
			user= systemService.getEntity(TSUser.class,user.getId());
			if("1".equals(user.getType())){
				message = "超级管理员创始账号不可删除";	

			}else{
			//user = systemService.getEntity(TSUser.class, user.getId());
			List<TSRoleUser> roleUser = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());
				message = "账号：" + user.getUserName() + "删除成功";
				if (roleUser.size()>0) {
					// 删除用户时先删除用户和角色关系表
					delRoleUser(user);
					
					systemService.delete(user);
					systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
				} else {
					systemService.delete(user);
					
				}
		  }
		}
		j.setMsg(message);
		return j;
	}
	
	
	/**
	 * 客户自主申请注册-申请
	 * 
	 * @param user
	 * @param req
	 * @return
	 */

	@RequestMapping(value = "saveClients")
	@ResponseBody
	public AjaxJson saveClients(HttpServletRequest request, TSUser user) {
		//验证公司下是否有该客户账号
		
		AjaxJson j = new AjaxJson();
		
		String url =  request.getScheme() +"://" + request.getServerName();
		String userNameClient =request.getParameter("userNameClient");
		user.setUserName(userNameClient);
		HttpSession session = ContextHolderUtils.getSession();
		String randCode = request.getParameter("randCode");
	    if (!randCode.equalsIgnoreCase(String.valueOf(session.getAttribute("randCodeMos")))) {//短信验证码randCodeMos
            j.setMsg("验证码错误！");				            
            j.setSuccess(false);
            return j;
	    }
	    
	    if (!(userNameClient).equalsIgnoreCase(String.valueOf(session.getAttribute("randCodeMosPhonenum")))) {//接收短信验证码randCodeMos 手机号
            j.setMsg("注册号码非接收验证码手机号！");				            
            j.setSuccess(false);
            return j;
	    }
		
		
		
		TSCompany company = companyService.companyByUrl(url);
		if ( company != null && StringUtil.isNotEmpty(company.getId()) ){
			//TSUser users = systemService.findUniqueByProperty(TSUser.class, "userName",user.getUserName());
			if (userService.checkuseradd(company.getId(), user.getUserName())) {
				message = "账号: " + user.getUserName() + "已经存在";
				j.setSuccess(false);
			} else {
				j.setSuccess(true);
				message = "账号: " + user.getUserName() + "申请成功,请耐心等待站方审核！";
				
				
				
				
				user.setCompany(company);
				
				user.setRealname(user.getRealname());
				user.setMobilephone(user.getUserName());
				String orgpwd = user.getPassword();
				user.setPassword(PasswordUtil.md5( orgpwd ));
				user.setType("3");
				user.setStatus( new Short("4"));
				//创建对应客户补充数据
				UserClientsEntity cs = new UserClientsEntity(); 
				Serializable id = systemService.save(cs);//创建用户客户扩展信息
					cs.setId((String) id);
				UserSalesmanEntity se = new UserSalesmanEntity();  //创建个人账户信息
					Serializable seid = systemService.save(se);
						se.setId((String) seid);
				user.setTsuserSalesman(se);
				user.setTsuserclients(cs);
				systemService.save(user);//创建个人用户
				//获取用户角色
				CriteriaQuery cq = new CriteriaQuery(TSRole.class);
					cq.eq("roleCode", "caigouyuan");//采购商默认为客户的角色
					cq.eq("company", user.getCompany());
				TSRole  tsRole = (TSRole) systemService.getObjectByCriteriaQuery(cq, false);
				if (StringUtil.isNotEmpty(tsRole.getId())) {
					saveRoleUser(user, tsRole.getId());
				}
				//发送注册开通短信 
				messageTemplateService.mosSendSmsDelivery_1(user, orgpwd );
				systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);

			}
			
		}
		j.setMsg(message);
		return j;
	}
	


	
	/**
	 * 客户列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "userClientsList")
	public String userClientsList(HttpServletRequest request) {	
		return "system/user/userClientsList";
	}
	
	/**
	 * easyuiAJAX 客户用户列表请求数据 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridClients")
	public void datagridClients(TSUser user,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
		
			cq.eq("type", "3");
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, user);
			cq.addOrder("createDate", SortDirection.desc);
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 客户列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdateClients")
	public String addorupdateClients( TSUser user, HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(user.getId())) {
			
			user = systemService.getEntity(TSUser.class, user.getId());
			
			if( StringUtil.isEmpty (user.getTsuserclients().getId().trim())){
				UserClientsEntity tsc = new UserClientsEntity() ;
				user.setTsuserclients(tsc);
			}
			
			//获取客户省会信息
			UserClientsEntity client = user.getTsuserclients();
			if(null != client && StringUtil.isNotEmpty(client.getId())){
				TSTerritory top1Territory = client.getTSTerritory();
				TSTerritory top2Territory = null;
				TSTerritory top3Territory = null;
				
				if(null != top1Territory && StringUtil.isNotEmpty(top1Territory.getId())){
					top2Territory = top1Territory.getTSTerritory();
					req.setAttribute("provinceid", top1Territory.getId());
				}
				if(null != top2Territory && StringUtil.isNotEmpty(top2Territory.getId())){
					top3Territory = top2Territory.getTSTerritory();
					req.setAttribute("provinceid", top2Territory.getId());
					req.setAttribute("cityid", top1Territory.getId());
				}
				if(null != top3Territory && StringUtil.isNotEmpty(top3Territory.getId())){
					req.setAttribute("provinceid", top3Territory.getId());
					req.setAttribute("cityid", top2Territory.getId());
					req.setAttribute("regionid", top1Territory.getId());
				}
				
			}
			
			try {
				//业务员空处理
				if( null == user.getTsuserclients().getYewuid() 
						|| (null != user.getTsuserclients().getYewuid() 
								&& StringUtil.isEmpty ( user.getTsuserclients().getYewuid().getId().trim() )) ){
					
					TSUser yewuUser = new TSUser();
					UserClientsEntity userClient =user.getTsuserclients();
					userClient.setYewuid(yewuUser);
					user.setTsuserclients(userClient);
				}
				
				//客户等级空处理
				UserTypeEntity userType = new UserTypeEntity();
				if( null !=user.getTsuserclients().getUsertpyrid()){
					try{
						userType = systemService.findUniqueByProperty(UserTypeEntity.class, "id", user.getTsuserclients().getUsertpyrid().getId());
					}catch(Exception e){
					}
					
					if(null == userType){
						userType = new UserTypeEntity();
					}
				}
				
				UserClientsEntity tsc =user.getTsuserclients();
				tsc.setUsertpyrid(userType);
				user.setTsuserclients(tsc);
				
			} catch (Exception e) {
				UserClientsEntity tsc =user.getTsuserclients();
				TSUser yewuUser = new TSUser();
				tsc.setYewuid(yewuUser);
				user.setTsuserclients(tsc);
				
				UserTypeEntity userType = new UserTypeEntity();
				tsc.setUsertpyrid(userType);
				user.setTsuserclients(tsc);
				
			}
		}
		
		req.setAttribute("user", user);
		return "system/user/userClients";
	}
	
	
	
	/**
	 * 客户信息录入更新
	 * 
	 * @param user
	 * @param req
	 * @return
	 */

	@RequestMapping(value = "saveUserClients")
	@ResponseBody
	public AjaxJson saveUserClients(HttpServletRequest req, TSUser user) {
		
		TSUser sessionUser = ResourceUtil.getSessionUserName();
		
		
		 AjaxJson j = new AjaxJson();
		// 得到用户的角色
		 //String roleid = oConvertUtils.getString(req.getParameter("roleid"));
		if (StringUtil.isNotEmpty(user.getId())) {//更新用户信息
			TSUser u = systemService.getEntity(TSUser.class, user.getId());
			message = "客户: " + u.getUserName() + "更新成功！";
			try {
				user.setType("3");
				UserClientsEntity cs = user.getTsuserclients(); 
				UserClientsEntity c = u.getTsuserclients(); 
				
				cs.setId(c.getId());
				MyBeanUtils.copyBeanNotNull2Bean(cs , c);
				systemService.saveOrUpdate(c);//更新用户附属信息
			
				//个人账号信息禁止更新
				user.setTsuserSalesman(u.getTsuserSalesman());
				user.setTsuserclients(c);
				MyBeanUtils.copyBeanNotNull2Bean(user , u);
				
				systemService.saveOrUpdate(u);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "客户: " + user.getUserName() + "更新失败！";
			}
		} else {//创建用户
			
			if (userService.checkuseradd(sessionUser.getCompany().getId(), user.getUserName())) {
				message = "客户: " + user.getUserName() + "已经存在";
			} else {
				message = "客户: " + user.getUserName() + "添加成功";
				
				//生成随机的密码 2位字母加4位数字
				RandCodeImageServlet RandCodeImageServlet = new RandCodeImageServlet();     
				String passwordRandCode  =  RandCodeImageServlet.passwordRandCode();
				user.setPassword(PasswordUtil.md5(passwordRandCode));
				user.setType("3");
				//创建对应的业务员信息
				UserClientsEntity cs = user.getTsuserclients(); 
				Serializable id = systemService.save(cs);//创建用户客户扩展信息
					cs.setId((String) id);
				UserSalesmanEntity se = new UserSalesmanEntity();  //创建个人账户信息
					Serializable seid = systemService.save(se);
						se.setId((String) seid);
				user.setTsuserSalesman(se);
				user.setTsuserclients(cs);
				systemService.save(user);//创建个人用户
				//获取用户角色
				CriteriaQuery cq = new CriteriaQuery(TSRole.class);
					cq.eq("roleCode", "caigouyuan");//采购商默认为客户的角色
					cq.eq("company", user.getCompany());
				TSRole  tsRole = (TSRole) systemService.getObjectByCriteriaQuery(cq, false);
				if (StringUtil.isNotEmpty(tsRole.getId())) {
					saveRoleUser(user, tsRole.getId());
				}
				//发送注册开通短信 
				messageTemplateService.mosSendSmsDelivery_1(user ,passwordRandCode );
				
				systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			}
		}
		j.setMsg(message);

		return j;
	}
	
	
	
	
	
	/**
	 * 选择业务员列表
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "choiceYewuUser")
	public String choiceYewuUser(HttpServletRequest request, HttpServletResponse response) {
		return "system/user/choiceYewuUser";
		
	}
	/**
	 * 业务员列表
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "choiceYewuUserList")
	public void choiceYewuUserList( TSUser  tSUser , HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		//只显示对应下属的权限，目前如果是企业管理员，不显示系统管理员，如果是其他权限， 不显示企业管理员、系统管理员	
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
			cq.eq("type", "4");
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSUser);
			cq.addOrder("createDate", SortDirection.desc);
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	
	/**
	 * 选择客户等级
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "choiceUserType")
	public String choiceUserType(HttpServletRequest request, HttpServletResponse response) {
		return "system/user/choiceUserType";
		
	}
	
	
	
	/**
	 * 用户选择角色跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "roles")
	public String roles() {
		return "system/user/users";
	}
	
	/**
	 * 用户选择角色显示列表
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridRole")
	public void datagridRole(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSRole.class, dataGrid);
		TSUser user = ResourceUtil.getSessionUserName();
		cq.eq("company", user.getCompany());
		cq.add();
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 用户选择员工跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiseStaff")
	public String salechoiseStaffman() {
		return "system/user/choiseStaff";
	}
	
	/**
	 * 用户选择业务员显示列表
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridSaleman")
	public void datagridSaleman(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
		TSUser user = ResourceUtil.getSessionUserName();
		cq.eq("company", user.getCompany());
		cq.eq("type", "4");
		cq.add();
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 用户选择角色跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiseSaleMan")
	public String saleman() {
		return "system/user/choiseSaleMan";
	}
	
	/**
	 * 用户选择业务员显示列表
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridUserType")
	public void datagridUserType(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(UserTypeEntity.class, dataGrid);
		TSUser user = ResourceUtil.getSessionUserName();
		cq.eq("company", user.getCompany());
		cq.add();
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 用户选择角色跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiseUserType")
	public String choiseUserType() {
		return "system/user/choiseUserType";
	}
	
	
	/**
	 * 菜单列表
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "menu")
	public void menu(HttpServletRequest request, HttpServletResponse response) {
		SetListSort sort = new SetListSort();
		TSUser u = ResourceUtil.getSessionUserName();
		// 登陆者的权限
		Set<TSFunction> loginActionlist = new HashSet<TSFunction>();// 已有权限菜单
		List<TSRoleUser> rUsers = systemService.findByProperty(TSRoleUser.class, "TSUser.id", u.getId());
		for (TSRoleUser ru : rUsers) {
			TSRole role = ru.getTSRole();
			List<TSRoleFunction> roleFunctionList = systemService.findByProperty(TSRoleFunction.class, "TSRole.id", role.getId());
			if (roleFunctionList.size() > 0) {
				for (TSRoleFunction roleFunction : roleFunctionList) {
					TSFunction function = (TSFunction) roleFunction.getTSFunction();
					loginActionlist.add(function);
				}
			}
		}
		List<TSFunction> bigActionlist = new ArrayList<TSFunction>();// 一级权限菜单
		List<TSFunction> smailActionlist = new ArrayList<TSFunction>();// 二级权限菜单
		if (loginActionlist.size() > 0) {
			for (TSFunction function : loginActionlist) {
				if (function.getFunctionLevel() == 0) {
					bigActionlist.add(function);
				} else if (function.getFunctionLevel() == 1) {
					smailActionlist.add(function);
				}
			}
		}
		// 菜单栏排序
		Collections.sort(bigActionlist, sort);
		Collections.sort(smailActionlist, sort);
		String logString = ListtoMenu.getMenu(bigActionlist, smailActionlist);
		// request.setAttribute("loginMenu",logString);
		try {
			response.getWriter().write(logString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 同步删除用户角色关联表
	 * 
	 */
	public void delRoleUser(TSUser user) {
		// 同步删除用户角色关联表
		List<TSRoleUser> roleUserList = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());
		if (roleUserList.size() >= 1) {
			for (TSRoleUser tRoleUser : roleUserList) {
				systemService.delete(tRoleUser);
			}
		}
	}
	/**
	 * 创建用户角色关联表
	 * 
	 */
	protected void saveRoleUser(TSUser user, String roleidstr) {
		String[] roleids = roleidstr.split(",");
		for (int i = 0; i < roleids.length; i++) {
			TSRoleUser rUser = new TSRoleUser();
			TSRole role = systemService.getEntity(TSRole.class, roleids[i]);
			rUser.setTSRole(role);
			rUser.setTSUser(user);
			systemService.save(rUser);
		}
	}
	
	
	
	/**
	 * 个人账户页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "clientAccountList")
	public String clientAccountList(HttpServletRequest request) {	
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(TSUser.class);
		cq.eq("company", user.getCompany());
		cq.eq("type", "3");
		cq.add();
		
		List<TSUser> buyerUserList = systemService.getListByCriteriaQuery(cq, false);
		
		request.setAttribute("buyerUserList", buyerUserList);
		
		return "admin/clients/clientAccountList";
	}
	
	/**
	 * 个人账户显示列表
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "clientAccountDatagrid")
	public void userSalesmanAdmin(TSUser user, HttpServletResponse response, DataGrid dataGrid) {
		TSUser userEntity = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
			if(null != user && StringUtil.isNotEmpty(user.getId())){
				cq.eq("id", user.getId());
			}
			cq.eq("company", userEntity.getCompany());
			cq.eq("type", "3");
			cq.add();
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	

	
	/**
	 * 个人账户页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "userSalesmanList")
	public String userSalesmanList(HttpServletRequest request) {	
		return "system/user/userSalesmanList";
	}
	
	/**
	 * 个人账户显示列表
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridUserSalesman")
	public void datagridUserSalesman(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
		cq.eq("id", user.getId());
		cq.eq("company", user.getCompany());
		cq.add();
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	
	
	/**
	 * 修改密码
	 * 
	 * @return
	 */
	@RequestMapping(value = "changepassword")
	public String changepassword(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		request.setAttribute("user", user);
		return "login/changepassword";
	}

	/**
	 * 修改密码
	 * 
	 * @return
	 */
	@RequestMapping(value = "savenewpwd")
	@ResponseBody
	public AjaxJson savenewpwd(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		
		String password = oConvertUtils.getString(request.getParameter("password"));
		String newpassword = oConvertUtils.getString(request.getParameter("newpassword"));
		String pString  = PasswordUtil.md5(password);
		
		if (!pString.equals(user.getPassword())) {
			j.setMsg("原密码不正确");
			j.setSuccess(false);
		} else {
			try {
				//user.setPassword(PasswordUtil.encrypt(user.getUserName(), newpassword, PasswordUtil.getStaticSalt()));
				user.setPassword(PasswordUtil.md5(newpassword));
				systemService.updateEntitie(user);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			j.setMsg("修改成功");

		}
		return j;
	}
	
	
	/**
	 * 用户信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "userinfo")
	public String userinfo(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		request.setAttribute("user", user);
		return "system/user/userinfo";
	}
	
	
	/**
	 *验证修改密码验证码 
	 */
	
	@RequestMapping(value = "randCodepw")
	@ResponseBody
	public AjaxJson randCodepw(HttpServletRequest req) {
		
		HttpSession session = ContextHolderUtils.getSession();
		AjaxJson j = new AjaxJson();
		
        String randCode = req.getParameter("randCode");
        if (StringUtils.isEmpty(randCode) ) {
            j.setMsg("请输入验证码");
            j.setSuccess(false);
        } else if ( !randCode.equalsIgnoreCase(String.valueOf(session.getAttribute("randCode")))) {
            j.setMsg("验证码错误！");
            j.setSuccess(false);
        } 

		return j;
	}
	
	
	
	/**
	 *修改个人密码  
	 */
	
	@RequestMapping(value = "savenewpwdforuser")
	@ResponseBody
	public AjaxJson savenewpwdforuser(HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		String id = oConvertUtils.getString(req.getParameter("id"));
		String password = oConvertUtils.getString(req.getParameter("password"));
		if (StringUtil.isNotEmpty(id)) {
			TSUser users = systemService.getEntity(TSUser.class,id);
			cn.gov.xnc.system.core.util.LogUtil.info(users.getUserName());
			users.setPassword(PasswordUtil.md5(password));
			systemService.updateEntitie(users);	
			message = "用户: " + users.getUserName() + "密码修改成功";
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} 
		
		j.setMsg(message);

		return j;
	}

	
	/**
	 * 
	 * 修改用户密码
	 * @author Chj
	 */
	
	@RequestMapping(value = "changepasswordforuser")
	public ModelAndView changepasswordforuser(TSUser user, HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(user.getId())) {
			user = systemService.getEntity(TSUser.class, user.getId());
			req.setAttribute("user", user);
			//idandname(req, user);
			cn.gov.xnc.system.core.util.LogUtil.info(user.getPassword()+"-----"+user.getUserName());
		}
		return new ModelAndView("system/user/adminchangepwd");
		
	}
	
	
	
	
	/**
	 *重置个人密码
	 */
	
	@RequestMapping(value = "resetPassword")
	@ResponseBody
	public AjaxJson resetPassword(HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		String id = oConvertUtils.getString(req.getParameter("id"));
		
		if (StringUtil.isNotEmpty(id)) {
			
			try {
				
				TSUser user = systemService.getEntity(TSUser.class,id);
	    		//重置用户密码
	    		//生成随机的密码 2位字母加4位数字
				RandCodeImageServlet RandCodeImageServlet = new RandCodeImageServlet();     
				String passwordRandCode  =  RandCodeImageServlet.passwordRandCode();
				user.setPassword(PasswordUtil.md5(passwordRandCode));
				String msg ="尊敬的用户你好，你的登陆密码已经重置为："+passwordRandCode+"，请勿泄露密码信息登陆后请及时修改你的登陆密码！";
				MessageTemplateServiceI messageTemplateServiceI = new MessageTemplateServiceImpl();
				System.out.println(msg);
				if( "3".equals(user.getType())){
					//客户的话直接使用账号为作为手机号发送验证码
					  messageTemplateServiceI.sendSMSTTMM("", "", user.getUserName(), "天天农贸", msg);
				}else {
					if(StringUtils.isNotEmpty(user.getMobilephone())){
					  messageTemplateServiceI.sendSMSTTMM("", "", user.getMobilephone(), "天天农贸", msg);
					}else{
						j.setMsg("账号信息未设置手机号，请联系站方管理员！");
						return j;
					}
				}
				systemService.saveOrUpdate(user);//更新用户密码信息
				message = "用户: " + user.getUserName() + "密码重置成功";
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				message = "密码重置失败，系统异常";
			}
		}
		j.setMsg(message);

		return j;
	}
	
	
	//===================================================
	
	
	
	
	
	
//	/**
//	 * 获取用户的所在区域
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "getUserAddress")
//	@ResponseBody
//	public AjaxJson getUserAddress(HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		
//		TSUser user =  ResourceUtil.getSessionUserName();
//		String address = "";
//		
//		if(user!=null){
//			//address = user.getUserAddress();
//			//address = address+"@"+user.getTSTerritory().getId();
//		}
//		j.setMsg(address);		
//		return j;
//	}

	


	
	



	



//	
//

//	
//	public void idandname(HttpServletRequest req, TSUser user) {
//		
//		
//		List<TSRoleUser> roleUsers = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());
//		String roleId = "";
//		String roleName = "";
//		if (roleUsers.size() > 0) {
//			for (TSRoleUser tRoleUser : roleUsers) {
//				roleId += tRoleUser.getTSRole().getId() + ",";
//				roleName += tRoleUser.getTSRole().getRoleName() + ",";
//			}
//		}
//		req.setAttribute("id", roleId);
//		req.setAttribute("roleName", roleName);
//	}
//	
//	
//	
//	


	
	
	
//	/**
//	 * 锁定账户
//	
//	 * 
//	 * @author Chj
//	 */
//	@RequestMapping(params = "lock")
//	@ResponseBody
//	public AjaxJson lock(String id, HttpServletRequest req) {
//		AjaxJson j = new AjaxJson();
//		
//		TSUser user = systemService.getEntity(TSUser.class, id);
//		if("admin".equals(user.getUserName())){
//			message = "超级管理员[admin]不可锁定";
//			j.setMsg(message);
//			return j;
//		}
//		if(user.getStatus()!=2 ){
//			user.setStatus((short)2);
//			systemService.updateEntitie(user);
//			message = "用户：" + user.getUserName() + "锁定成功";
//			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
//		} else {
//			message = "锁定账户失败";
//		}
//
//		j.setMsg(message);
//		return j;
//	}
//
//	/**
//	 * 解锁用户
//	
//	 * 
//	 * @author Chj
//	 */
//	@RequestMapping(params = "unlock")
//	@ResponseBody
//	public AjaxJson unlock(String id, HttpServletRequest req) {
//		AjaxJson j = new AjaxJson();
//		
//		TSUser user = systemService.getEntity(TSUser.class, id);
//		if("admin".equals(user.getUserName())){
//			message = "超级管理员[admin]不可锁定";
//			j.setMsg(message);
//			return j;
//		}
//		if(user.getStatus()==2){
//			user.setStatus((short) 1);
//			systemService.updateEntitie(user);
//			message = "用户：" + user.getUserName() + "解锁成功";
//			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);		
//		} else {
//			message = "解锁账户失败";
//		}
//
//		j.setMsg(message);
//		return j;
//	}
//
//	/**
//	 * 得到角色列表
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "role")
//	@ResponseBody
//	public List<ComboBox> role(HttpServletResponse response, HttpServletRequest request, ComboBox comboBox) {
//		String id = request.getParameter("id");
//		List<ComboBox> comboBoxs = new ArrayList<ComboBox>();
//		
//		List<TSRole> roles = new ArrayList<TSRole>();
//		
//		if(StringUtils.isNotEmpty(id)){		
//			if (StringUtil.isNotEmpty(id)) {
//				List<TSRoleUser> roleUser = systemService.findByProperty(TSRoleUser.class, "TSUser.id", id);
//				if (roleUser.size() > 0) {
//					for (TSRoleUser ru : roleUser) {
//						roles.add(ru.getTSRole());
//					}
//				}
//			}
//		}
//		
//		List<TSRole> roleList = systemService.getList(TSRole.class);
//		comboBoxs = TagUtil.getComboBox(roleList, roles, comboBox);
//		return comboBoxs;
//	}
//
//	





	
	

	
//	/**
//	 * 检查用户名
//	 * 
//	 * @param ids
//	 * @return
//	 */
//	@RequestMapping(params = "checkUser")
//	@ResponseBody
//	public ValidForm checkUser(HttpServletRequest request) {
//		ValidForm v = new ValidForm();
//		String userName=oConvertUtils.getString(request.getParameter("param"));
//		String code=oConvertUtils.getString(request.getParameter("code"));
//		List<TSUser> roles=systemService.findByProperty(TSUser.class,"userName",userName);
//		if(roles.size()>0&&!code.equals(userName))
//		{
//			v.setInfo("用户名已存在");
//			v.setStatus("n");
//		}
//		return v;
//	}
//
//
//
//
//
//	
//
//	
//	
//
//	/**
//	 * 用户选择角色跳转页面
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "choiceUsers")
//	public String choiceUsers() {
//		return "system/user/choiceUsers";
//	}
	
//	/**
//	 * 用户显示列表
//	 * 
//	 * @param request
//	 * @param response
//	 * @param dataGrid
//	 */
//	@RequestMapping(params = "choiceUsersList")
//	public void choiceUsersList(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
//		//只显示对应下属的权限，目前如果是企业管理员，不显示系统管理员，如果是其他权限， 不显示企业管理员、系统管理员
//		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
//		//获取用户的权限级别
//		TSUser currentUser = null;
//		try {
//			currentUser = ResourceUtil.getSessionUserName();
//		} catch (RuntimeException e) {
//			logger.warn("当前session为空,无法获取用户");
//			return ;
//		}
//		
//		if( currentUser != null && "2".equals(currentUser.getType())){//企业用户
//			cq.eq("company.id", currentUser.getCompany().getId());         
//		}
//		this.systemService.getDataGridReturn(cq, true);
//		TagUtil.datagrid(response, dataGrid);
//	}
	

	
	
	
	
	
	
//	/**
//	 * easyuiAJAX请求数据： 
//	 * 
//	 * @param request
//	 * @param response
//	 * @param dataGrid
//	 * @param user
//	 */
//	@RequestMapping(params = "addorupdate")
//	public ModelAndView addorupdate(TSUser user, HttpServletRequest req) {
//		
//		TSUser tsUser =  ResourceUtil.getSessionUserName();
//		req.setAttribute("tsUser", tsUser);
//		if (StringUtil.isNotEmpty(user.getId())) {
//			user = systemService.getEntity(TSUser.class, user.getId());
//			req.setAttribute("user", user);
//			idandname(req, user);
//		}		
//		return new ModelAndView("system/user/user");
//
//	}
	
	
	



//	/**
//	 * 根据部门和角色选择用户跳转页面
//	 */
//	@RequestMapping(params = "choose")
//	public String choose(HttpServletRequest request) {
//		List<TSRole> roles = systemService.loadAll(TSRole.class);
//		request.setAttribute("roleList", roles);
//		return "system/membership/checkuser";
//	}
//
//	/**
//	 * 部门和角色选择用户的panel跳转页面
//	 * 
//	 * @param request
//	 * @return
//	 */
//	@RequestMapping(params = "chooseUser")
//	public String chooseUser(HttpServletRequest request) {
//		String departid = request.getParameter("departid");
//		String roleid = request.getParameter("roleid");
//		request.setAttribute("roleid", roleid);
//		request.setAttribute("departid", departid);
//		return "system/membership/userlist";
//	}
//
//	/**
//	 * 部门和角色选择用户的用户显示列表
//	 * 
//	 * @param request
//	 * @param response
//	 * @param dataGrid
//	 */
//	@RequestMapping(params = "datagridUser")
//	public void datagridUser(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
//		String departid = request.getParameter("departid");
//		String roleid = request.getParameter("roleid");
//		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
//		if (departid.length() > 0) {
//			cq.eq("TDepart.departid", oConvertUtils.getInt(departid, 0));
//			cq.add();
//		}
//		String userid = "";
//		if (roleid.length() > 0) {
//			List<TSRoleUser> roleUsers = systemService.findByProperty(TSRoleUser.class, "TRole.roleid", oConvertUtils.getInt(roleid, 0));
//			if (roleUsers.size() > 0) {
//				for (TSRoleUser tRoleUser : roleUsers) {
//					userid += tRoleUser.getTSUser().getId() + ",";
//				}
//			}
//			cq.in("userid", oConvertUtils.getInts(userid.split(",")));
//			cq.add();
//		}
//		this.systemService.getDataGridReturn(cq, true);
//		TagUtil.datagrid(response, dataGrid);
//	}
//
//	/**
//	 * 根据部门和角色选择用户跳转页面
//	 */
//	@RequestMapping(params = "roleDepart")
//	public String roleDepart(HttpServletRequest request) {
//		List<TSRole> roles = systemService.loadAll(TSRole.class);
//		request.setAttribute("roleList", roles);
//		return "system/membership/roledepart";
//	}



//	/**
//	 * 部门和角色选择用户的用户显示列表
//	 * 
//	 * @param request
//	 * @param response
//	 * @param dataGrid
//	 */
//	@RequestMapping(params = "datagridDepart")
//	public void datagridDepart(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
//		CriteriaQuery cq = new CriteriaQuery(TSTerritory.class, dataGrid);
//		systemService.getDataGridReturn(cq, true);
//		TagUtil.datagrid(response, dataGrid);
//	}

	
	@RequestMapping(value= "isLogin")
	@ResponseBody
	public AjaxJson isUserLogin(HttpServletRequest req){
		AjaxJson json = new AjaxJson();
		HttpSession session = ContextHolderUtils.getSession();
		Client client = ClientManager.getInstance().getClient(session.getId());
		
		
		if(client == null){
			client = ClientManager.getInstance().getClient(req.getParameter("sessionId")); 
		}
		
		if(client != null && client.getUser()!=null  && client.getUser().getCompany() != null){
			json.setSuccess(true);
		}else{
			json.setSuccess(false);;
		}
		
		return json;
	}
	
	/**
	 * 采购商预存款统计
	 * @param req
	 * @return
	 */
	@RequestMapping(value= "statisUserClientAcount")
	@ResponseBody
	public AjaxJson statisUserClientAcount(HttpServletRequest req){
		//------初始化返回信息
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("采购商预存款统计失败");
		
		//------查询统计条件
		TSUser user = ResourceUtil.getSessionUserName();
		TSUser buyerUser = null;
		String buyerUserId = ResourceUtil.getParameter("clientid.id");//查询具体的采购商
		if(StringUtil.isNotEmpty(buyerUserId) && StringUtil.isNotEmpty(buyerUserId.trim())){
			buyerUser = systemService.findUniqueByProperty(TSUser.class, "id", buyerUserId);
		}
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			json.setSuccess(true);
			
			StatisPageVO statispage = userService.staticsUserClientAccountMoney(company, buyerUser,req);
			json.setObj(statispage);
			json.setMsg("采购商预存款统计成功");
		}
		
		return json;
	}
	
	/**
	 * 发货商信息编辑页面跳转
	 * @return
	 */
	@RequestMapping(value = "userCopartner")
	public String editCopartner( TSUser user, HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(user.getId())) {
			
			user = systemService.getEntity(TSUser.class, user.getId());
			if( null == user.getTsuserclients() || StringUtil.isEmpty (user.getTsuserclients().getId().trim())){
				UserClientsEntity tsc = new UserClientsEntity() ;
				user.setTsuserclients(tsc);
			}

			List<TSRoleUser> ru = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());//更新用户权限
			String roleUserId ="";
			String roleUserName ="";
			try {
				if( ru != null && ru.size() > 0){
					for(TSRoleUser tSRoleUser : ru){
						roleUserName +=tSRoleUser.getTSRole().getRoleName()+",";
						roleUserId +=tSRoleUser.getTSRole().getId()+",";
					}
					//过滤最后一个,
					roleUserId = roleUserId.substring(0, roleUserId.lastIndexOf(",")) ; 
					roleUserName = roleUserName.substring(0, roleUserName.lastIndexOf(",")) ; 
				}
			} catch (Exception e) {
				
			}
			
			req.setAttribute("roleUserId", roleUserId);
			req.setAttribute("roleUserName", roleUserName);
			req.setAttribute("user", user);
		}
		return "system/user/userCopartner";
	}
	

	/**
	 * 客户自主申请注册-申请
	 * 
	 * @param user
	 * @param req
	 * @return
	 */

	@RequestMapping(value = "saveCopartner")
	@ResponseBody
	public AjaxJson saveCopartner(HttpServletRequest request, TSUser user) {
		TSUser sessionUser = ResourceUtil.getSessionUserName();
		
		
		 AjaxJson j = new AjaxJson();
		 user.setType("6");
		 user.setMobilephone(user.getTsuserCopartner().getPartnercontactphone());
		// 得到用户的角色
		String roleid = oConvertUtils.getString(request.getParameter("roleid"));
		if (StringUtil.isNotEmpty(user.getId())) {//更新用户信息
			TSUser userEntity = systemService.getEntity(TSUser.class, user.getId());
			message = "发货商: " + userEntity.getUserName() + "更新成功！";
			try {
				UserCopartnerEntity copartnerPage = user.getTsuserCopartner(); 
				UserCopartnerEntity copartnerDB = userEntity.getTsuserCopartner(); 
				
				//个人账号信息禁止更新
				user.setTsuserSalesman(userEntity.getTsuserSalesman());
				user.setTsuserCopartner(copartnerDB);
				MyBeanUtils.copyBeanNotNull2Bean(user , userEntity);
				
				systemService.saveOrUpdate(userEntity);

				copartnerPage.setId(copartnerDB.getId());
					MyBeanUtils.copyBeanNotNull2Bean(copartnerPage , copartnerDB);
				systemService.saveOrUpdate(copartnerDB);//更新用户附属信息
				List<TSRoleUser> ru = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());//更新用户权限
				systemService.deleteAllEntitie(ru);
				if (StringUtil.isNotEmpty(roleid)) {
					saveRoleUser(user, roleid);
				}
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "发货商: " + user.getUserName() + "更新失败！";
			}
		} else {//创建用户
			
			if (userService.checkuseradd(sessionUser.getCompany().getId(), user.getUserName())) {
				message = "发货商: " + user.getUserName() + "已经存在";
			} else {
				message = "发货商: " + user.getUserName() + "添加成功";
				
				//生成随机的密码 2位字母加4位数字
				RandCodeImageServlet RandCodeImageServlet = new RandCodeImageServlet();     
				String passwordRandCode  =  RandCodeImageServlet.passwordRandCode();
				user.setPassword(PasswordUtil.md5(passwordRandCode));
				//创建对应的业务员信息
				UserCopartnerEntity copartnerPage = user.getTsuserCopartner(); 
				Serializable id = systemService.save(copartnerPage);//创建用户客户扩展信息
				copartnerPage.setId((String) id);
				UserSalesmanEntity se = new UserSalesmanEntity();  //创建个人账户信息
					Serializable seid = systemService.save(se);
						se.setId((String) seid);
				user.setTsuserSalesman(se);
				user.setTsuserCopartner(copartnerPage);
				systemService.save(user);//创建个人用户
				//获取用户角色
				if (StringUtil.isNotEmpty(roleid)) {
					saveRoleUser(user, roleid);
				}
				
				TSRoleUser rUser = new TSRoleUser();
				TSRole role = systemService.getEntity(TSRole.class, roleid);
				if(null != role){
					rUser.setTSRole(role);
					rUser.setTSUser(user);
					systemService.save(rUser);
				}
				
				
				try{
					userCopartnerAccountService.buildUserCopartnerAccount(user);
					
				}catch (Exception e) {
					logger.error(">>>>>>>>>>>>>buildUserCopartnerAccount : " + e);
				}
				
				//发送注册开通短信 
				messageTemplateService.mosSendSmsCopartner_1(user ,passwordRandCode );
				
				systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			}
		}
		j.setMsg(message);

		return j;
	}
	
	/**
	 * 员工用户列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "userCopartnerList")
	public String userCopartnerList(HttpServletRequest request) {
			
		return "system/user/userCopartnerList";
	}
	
	/**
	 * easyuiAJAX 员工用户用户列表请求数据 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridCopartner")
	public void datagridCopartner(TSUser user,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
			cq.eq("type", "6");
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, user);
			cq.addOrder("createDate", SortDirection.desc);
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	@RequestMapping(value = "getCompanyClients")
	public String getCompanyClients(){
		return "system/user/choiseClients";
	}
	
	/**
	 * easyuiAJAX 员工用户用户列表请求数据 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "datagridCompanyClients")
	public void datagridCompanyClients(TSUser user,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
			cq.eq("type", "3");
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, user);
			cq.addOrder("createDate", SortDirection.desc);
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
}