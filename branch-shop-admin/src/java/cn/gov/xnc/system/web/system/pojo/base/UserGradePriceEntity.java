package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 个人账户管理
 * @author zero
 * @date 2016-11-20 22:21:31
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_product_grade", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class UserGradePriceEntity implements java.io.Serializable {
	/**等级价格唯一序列号*/
	private java.lang.String id;
	/**产品唯一序列号*/
	private java.lang.String productid;
	/**用户等级唯一序列号*/
	private java.lang.String usertypeid;
	/**用户等级对应的等级价格*/
	private BigDecimal gradeprice;
	/**实际手动设置的价格*/
	private BigDecimal manualprice;
	
	/**创建时间*/
	private java.util.Date createdate;
	/**更新时间*/
	private java.util.Date updatedate;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  等级价格唯一序列号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  等级价格唯一序列号
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品唯一序列号
	 */
	@Column(name ="PRODUCTID",nullable=false,length=32)
	public java.lang.String getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品唯一序列号
	 */
	public void setProductid(java.lang.String productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户等级唯一序列号
	 */
	@Column(name ="USERTYPEID",nullable=false,length=32)
	public java.lang.String getUsertypeid(){
		return this.usertypeid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户等级唯一序列号
	 */
	public void setUsertypeid(java.lang.String usertypeid){
		this.usertypeid = usertypeid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  用户等级对应的等级价格
	 */
	@Column(name ="GRADEPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getGradeprice(){
		return this.gradeprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  用户等级对应的等级价格
	 */
	public void setGradeprice(BigDecimal gradeprice){
		this.gradeprice = gradeprice;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  实际手动设置的价格
	 */
	@Column(name ="MANUALPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getManualprice(){
		return this.manualprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  实际手动设置的价格
	 */
	public void setManualprice(BigDecimal manualprice){
		this.manualprice = manualprice;
	}
	
	
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}

}
