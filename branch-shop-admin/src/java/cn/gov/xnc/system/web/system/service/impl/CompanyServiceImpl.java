package cn.gov.xnc.system.web.system.service.impl;



import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.service.CompanyService;


/**
 * 
 * @author  zero
 *
 */
@Service("companyService")

public class CompanyServiceImpl extends CommonServiceImpl implements CompanyService {
	
	public TSCompany companyByUrl(String url) {

		TSCompany company = null;
		try {
			String finalUrl = url;
			if(finalUrl.indexOf("http://admin") > -1){
				finalUrl = url.replaceAll("http://admin.", "http://");
			}//换成公司管理域名，保证可以查找到公司信息
			
			CompanyPlatformEntity companyplatform = new CompanyPlatformEntity();
			companyplatform.setWebsite(finalUrl);
			CriteriaQuery cq = new CriteriaQuery(TSCompany.class);
			cq.createAlias("companyplatform", "p");
			cq.add(Restrictions.eq("p.website", finalUrl));
			cq.add();
			company = (TSCompany) getObjectByCriteriaQuery(cq, true);// 公司是否已经存在角色

		} catch (Exception e) {

		}

		return company;
	}

	public TSCompany getCompanyByDomain(HttpServletRequest request) {

		String url = request.getScheme() + "://" + request.getServerName();
		TSCompany company = null;
		try {

			String finalUrl = url;
			if(finalUrl.indexOf("http://admin") > -1){
				finalUrl = url.replaceAll("http://admin.", "http://");
			}//换成公司管理域名，保证可以查找到公司信息
			
			CompanyPlatformEntity companyplatform = new CompanyPlatformEntity();
			companyplatform.setWebsite(finalUrl);
			CriteriaQuery cq = new CriteriaQuery(TSCompany.class);
			cq.createAlias("companyplatform", "p");
			cq.add(Restrictions.eq("p.website", finalUrl));
			cq.add();
			company = (TSCompany) getObjectByCriteriaQuery(cq, true);// 公司是否已经存在角色

		} catch (Exception e) {
			return null;
		}

		return company;
	}
	

}
