package cn.gov.xnc.system.web.system.controller.core;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyReviewedEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;



/**   
 * @Title: Controller
 * @Description: 公司资费信息
 * @author zero
 * @date 2016-09-26 03:00:15
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/companyReviewedController")
public class CompanyReviewedController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CompanyReviewedController.class);


	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 公司资费信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView companyReviewed(HttpServletRequest request) {
		return new ModelAndView("system/user/companyReviewedList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(CompanyReviewedEntity companyReviewed,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		if(  StringUtil.isNotEmpty(user.getCompany().getCompanyreviewed().getId()) ){//如果不公司信息 这返回空
			companyReviewed.setId(user.getCompany().getCompanyreviewed().getId());
			CriteriaQuery cq = new CriteriaQuery(CompanyReviewedEntity.class, dataGrid);
			//查询条件组装器
			cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, companyReviewed, request.getParameterMap());
			this.systemService.getDataGridReturn(cq, true);
			TagUtil.datagrid(response, dataGrid);
		}
	}

	/**
	 * 删除公司资费信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(CompanyReviewedEntity companyReviewed, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyReviewed = systemService.getEntity(CompanyReviewedEntity.class, companyReviewed.getId());
		message = "公司资费信息删除成功";
		systemService.delete(companyReviewed);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加公司资费信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(CompanyReviewedEntity companyReviewed, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(companyReviewed.getId())) {
			message = "公司资费信息更新成功";
			CompanyReviewedEntity t = systemService.get(CompanyReviewedEntity.class, companyReviewed.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(companyReviewed, t);
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "公司资费信息更新失败";
			}
		} else {
			message = "公司资费信息添加成功";
			systemService.save(companyReviewed);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 公司资费信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(CompanyReviewedEntity companyReviewed, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(companyReviewed.getId())) {
			companyReviewed = systemService.getEntity(CompanyReviewedEntity.class, companyReviewed.getId());
			req.setAttribute("companyReviewedPage", companyReviewed);
		}
		return new ModelAndView("system/user/companyReviewed");
	}
}
