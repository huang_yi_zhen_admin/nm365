package cn.gov.xnc.system.web.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserRoleService;

@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private SystemService systemService;
	
	private Logger logger = Logger.getLogger(UserRoleServiceImpl.class);
	
	@Override
	public TSRole getRoleByUserRole(TSUser user) {
		// 根据用户获取用户绑定的权限
		CriteriaQuery ru = new CriteriaQuery(TSRoleUser.class);
		ru.eq("TSUser.id", user.getId());
		ru.add();
		List<TSRoleUser> roleUserList = systemService.getListByCriteriaQuery(ru, false);
		// 获取权限
		List<TSRole> roleList = new ArrayList<TSRole>();
		for (TSRoleUser tsRoleUser : roleUserList) {
			TSRole role = systemService.getEntity(TSRole.class, tsRoleUser.getTSRole().getId());
			roleList.add(role);
		}
		TSRole role = null;
		if (roleList.size() > 0) {
			role = roleList.get(0);
		}
		return role;
	}

}
