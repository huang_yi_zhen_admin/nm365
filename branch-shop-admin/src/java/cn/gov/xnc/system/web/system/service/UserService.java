package cn.gov.xnc.system.web.system.service;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
/**
 * 
 * @author  zero
 *
 */
public interface UserService extends CommonService{

	//public TSUser checkUserExits(TSUser user  );
	public String getUserRole(TSUser user);
	
	public void pwdInit(TSUser user, String newPwd);
	/**
	 * 判断这个角色是不是还有用户使用
	 */
	public int getUsersOfThisRole(String id);

	/**
	 *账号是否存在 
	 */
	public  boolean checkuseradd( String companyid , String  userName );
	
	/**
	 * 登陆用户检查
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public TSUser checkUserExits(TSUser user ,TSCompany company  ) throws Exception;
	
	
	/**
	 * 获取业务员的对应信息
	 * 
	 */
	public List<TSUser> findListUserByYW(String companyid);
	
	/**
	 * 统计所有采购客户的预存款项
	 * @param company
	 * @param user
	 * @return
	 */
	public StatisPageVO staticsUserClientAccountMoney(TSCompany company, TSUser user, HttpServletRequest request);
	
	/**
	 * 根据条件获取用户信息
	 * @param params
	 * @return
	 */
	public  List<TSUser> findListByParams(Map<String, Object> params);
	
}
