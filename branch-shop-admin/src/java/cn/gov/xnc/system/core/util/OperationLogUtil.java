package cn.gov.xnc.system.core.util;

import java.util.HashMap;
import java.util.Map;

public class OperationLogUtil {
	
	//-------------库存管理-----------------
	/**商品入库操作*/
	public final static int STOCK_IN = 100001;//商品入库操作
	/**商品出库操作*/
	public final static int STOCK_OUT = 100002;//商品出库操作
	/**新增仓库*/
	public final static int STOCK_ADD = 100004;//新增仓库
	/**编辑仓库*/
	public final static int STOCK_UPDATE = 100005;//编辑仓库
	/**删除仓库*/
	public final static int STOCK_DEL = 100006;//删除仓库
	/**新增仓库区域*/
	public final static int STOCK_SEGMENT_ADD= 100007;//新增仓库区域
	/**编辑仓库区域*/
	public final static int STOCK_SEGMENT_UPDATE = 100008;//编辑仓库区域
	/**删除仓库区域及子区域*/
	public final static int STOCK_SEGMENT_DEL = 100009;//删除仓库区域及子区域
	/**入库类型新增*/
	public final static int STOCK_TYPE_ADD = 100010;//入库类型新增
	/**入库类型变更*/
	public final static int STOCK_TYPE_UPDATE = 100011;//入库类型变更
	/**入库类型删除*/
	public final static int STOCK_TYPE_DEL = 100012;//入库类型删除
	/**直接盘点操作*/
	public final static int STOCK_CHECK_COMMIT = 100013;//直接盘点操作
	/**保存盘点单*/
	public final static int STOCK_CHECK_SAVE = 100014;//保存盘点单
	/**合并盘点*/
	public final static int STOCK_CHECK_MULTICOMMIT = 100015;//合并盘点
	/**删除盘点单*/
	public final static int STOCK_CHECK_DEL = 100016;//合并盘点
	/**新增供应商*/
	public final static int STOCK_SUPPLIER_ADD= 100017;//新增供应商
	/**更新供应商信息*/
	public final static int STOCK_SUPPLIER_UPDATE=100018;//更新供应商信息
	/**删除供应商信息*/
	public final static int STOCK_SUPPLIER_DEL = 100019;//删除供应商信息
	/**调整库存成本价格*/
	public final static int STOCK_PRODUCT_SETPRICE = 100020;//调整库存成本价格
	/**锁定仓库*/
	public final static int STOCK_LOCK = 100021;//锁定仓库
	/**解锁仓库*/
	public final static int STOCK_UNLOCK = 100022;//解锁仓库
	
	
	//-----------------------系统设置 用户管理 ------------------
	/**更新权限*/
	public final static int SYSTEM_AUTHORITY_UPDATE = 200001;
	/**新增角色*/
	public final static int SYSTEM_ROLE_ADD = 200002;
	/**更新角色*/
	public final static int SYSTEM_ROLE_UPDATE = 200003;
	/**删除角色*/
	public final static int SYSTEM_ROLE_DEL = 200004;
	/**短信模板更新*/
	public final static int SYSTEM_SMS_UPDATE = 200005;
	/**广告位添加*/
	public final static int SYSTEM_AD_ADD = 200006;
	/**广告位更新*/
	public final static int SYSTEM_AD_UPDATE = 200007;
	/**广告位删除*/
	public final static int SYSTEM_AD_DEL = 200008;
	/**支付设置*/
	public final static int SYSTEM_PAYSETTING = 200009;
	/**发货地址管理-新增发货地址*/
	public final static int SYSTEM_EXPRESS_ADD = 200010;
	/**发货地址管理-修改发货地址*/
	public final static int SYSTEM_EXPRESS_UPDATE = 200011;
	/**发货地址管理-删除发货地址*/
	public final static int SYSTEM_EXPRESS_DEL = 200012;
	/**企业信息修改*/
	public final static int SYSTEM_COMPANYINFO_UPDATE = 200013;
	/**企业平台信息修改*/
	public final static int SYSTEM_COMPANYPLATFORM_UPDATE = 200014;
	/**发货商编辑*/
	public final static int SYSTEM_COPARTNER_UPDATE = 200015;
	/**发货商新增*/
	public final static int SYSTEM_COPARTNER_ADD = 200016;
	/**客户等级新增*/
	public final static int SYSTEM_USERTYPE_ADD = 200017;
	/**客户等级编辑*/
	public final static int SYSTEM_USERTYPE_UPDATE = 200018;
	/**客户等级删除*/
	public final static int SYSTEM_USERTYPE_DEL = 200019;
	/**企业员工编辑*/
	public final static int SYSTEM_USERSTAFF_UPDATE = 200020;
	/**企业员工新增*/
	public final static int SYSTEM_USERSTAFF_ADD = 200021;
	/**客户资料更新*/
	public final static int SYSTEM_USERCLIENTS_UPDATE = 200022;
	/**客户新增*/
	public final static int SYSTEM_USERCLIENTS_ADD = 200023;
	/**重置密码*/
	public final static int SYSTEM_RESETPASSWORD = 200024;
	/**用户协议价保存*/
	public final static int SYSTEM_USERPRICE_SAVE = 200025;
	/**用户协议价删除*/
	public final static int SYSTEM_USERPRICE_DEL = 200026;
	/**客户扩展信息添加*/
	public final static int SYSTEM_USERCLIENTEXTEND_ADD = 200027;
	/**客户扩展信息更新*/
	public final static int SYSTEM_USERCLIENTEXTEND_UPDATE = 200028;
	/**客户扩展信息删除*/
	public final static int SYSTEM_USERCLIENTEXTEND_DEL = 200029;
	/**公司银行帐号信息添加*/
	public final static int SYSTEM_COMPANYBANK_ADD = 200030;
	/**公司银行帐号信息删除*/
	public final static int SYSTEM_COMPANYBANK_DEL = 200031;
	/**公司银行帐号信息更新*/
	public final static int SYSTEM_COMPANYBANK_UPDATE = 200032;
	
	//------------------------- 商品 ---------------------
	/**商品品牌添加*/
	public final static int PRODUCT_BRAND_ADD = 300001;
	/**商品品牌更新*/
	public final static int PRODUCT_BRAND_UPDATE = 300002;
	/**商品品牌删除*/
	public final static int PRODUCT_BRAND_DEL = 300003;
	/**商品分类新增*/
	public final static int PRODUCT_CLASSIFY_ADD = 300004;
	/**商品分类更新*/
	public final static int PRODUCT_CLASSIFY_UPDATE = 300005;
	/**商品分类删除*/
	public final static int PRODUCT_CLASSIFY_DEL = 300006;
	/**商品新增*/
	public final static int PRODUCT_ADD = 300007;
	/**商品更新*/
	public final static int PRODUCT_UPDATE = 300008;
	/**商品删除*/
	public final static int PRODUCT_DEL = 300009;
	
	//-----------------------  资金相关    ----------------
	/**业绩结算新增*/
	public final static int BUSSINESS_SETTLEMENT_ADD = 400001;
	/**业绩结算变更*/
	public final static int BUSSINESS_SETTLEMENT_UPDATE = 400002;
	/**业绩结算删除*/
	public final static int BUSSINESS_SETTLEMENT_DEL = 400003;
	/**业绩结算审核*/
	public final static int BUSSINESS_SETTLEMENT_AUDIT = 400004;
	/**业绩提现申请*/
	public final static int USERDRAWMONEY_ADD = 400005;
	/**业绩提现更新*/
	public final static int USERDRAWMONEY_UPDATE = 400006;
	/**业绩提现审核*/
	public final static int USERDRAWMONEY_AUDIT = 400007;
	
	/**支付审核*/
	public final static int PAYBILLS_AUDIT = 40008;
	/**删除支付审核*/
	public final static int PAYBILLS_AUDIT_DEL = 40009;
	/**账户余额支付*/
	public final static int PAYMENT_USERMONEY = 40010;
	/**线下支付*/
	public final static int PAYMENT_OFFLINE = 40011;
	
	
	//-------------  订单相关 -------------------
	/**退单*/
	public final static int PAYBILLSORDER_CANCLE= 50001;
	/**打印销售单*/
	public final static int PAYBILLSORDER_PRINTPAYBILLS = 50002;
	/**订单确认*/
	public final static int PAYBILLSORDER_COMFIRM = 50003;
	/**对已经确认的订单更新配送信息*/
	public final static int PAYBILLSORDER_UPDATE = 50004;
	/**对订单进行发货*/
	public final static int PAYBILLSORDER_DELIVERY = 50005;
	/**对订单进行签收*/
	public final static int PAYBILLSORDER_RECEIVED = 50006;
	/**退小单审核*/
	public final static int ORDER_CANCLE= 50007;
	
	//-------------  核销记录相关 -------------------
	/**核销记录操作*/
	public final static int VERIFY_ORDER_LOG_COMMIT = 60001;
	
	
	
	public OperationLogUtil() {
		// TODO Auto-generated constructor stub
		idmap = new HashMap<Integer, String>();
		
		//库存管理
		idmap.put(OperationLogUtil.STOCK_IN, "商品入库操作");
		idmap.put(OperationLogUtil.STOCK_OUT, "商品出库操作");
		idmap.put(OperationLogUtil.STOCK_CHECK_COMMIT, "直接盘点");
		idmap.put(OperationLogUtil.STOCK_CHECK_SAVE, "保存盘点单");
		idmap.put(OperationLogUtil.STOCK_CHECK_MULTICOMMIT, "合并盘点");
		idmap.put(OperationLogUtil.STOCK_CHECK_DEL, "删除盘点单");
		idmap.put(OperationLogUtil.STOCK_ADD, "新增仓库");
		idmap.put(OperationLogUtil.STOCK_UPDATE, "编辑仓库");
		idmap.put(OperationLogUtil.STOCK_DEL, "删除仓库");
		idmap.put(OperationLogUtil.STOCK_SEGMENT_ADD, "新增仓库区域");
		idmap.put(OperationLogUtil.STOCK_SEGMENT_UPDATE, "编辑仓库区域");
		idmap.put(OperationLogUtil.STOCK_SEGMENT_DEL, "删除仓库区域及子区域");
		idmap.put(OperationLogUtil.STOCK_TYPE_ADD, "入库类型新增");
		idmap.put(OperationLogUtil.STOCK_TYPE_UPDATE, "入库类型变更");
		idmap.put(OperationLogUtil.STOCK_TYPE_DEL, "入库类型删除");
		idmap.put(OperationLogUtil.STOCK_SUPPLIER_ADD, "新增供应商");
		idmap.put(OperationLogUtil.STOCK_SUPPLIER_UPDATE, "更新供应商信息");
		idmap.put(OperationLogUtil.STOCK_SUPPLIER_DEL, "删除供应商信息");
		idmap.put(OperationLogUtil.STOCK_PRODUCT_SETPRICE, "调成库存成本单价");
		idmap.put(OperationLogUtil.STOCK_LOCK, "锁定仓库");
		idmap.put(OperationLogUtil.STOCK_UNLOCK, "解锁仓库");
		
		//系统设置
		idmap.put(OperationLogUtil.SYSTEM_AUTHORITY_UPDATE, "更新用户权限");
		idmap.put(OperationLogUtil.SYSTEM_ROLE_ADD, "增加用户角色");
		idmap.put(OperationLogUtil.SYSTEM_ROLE_UPDATE, "更新用户角色");
		idmap.put(OperationLogUtil.SYSTEM_ROLE_DEL, "删除用户角色");
		idmap.put(OperationLogUtil.SYSTEM_SMS_UPDATE, "短信模板更新");
		idmap.put(OperationLogUtil.SYSTEM_AD_ADD, "广告位添加");
		idmap.put(OperationLogUtil.SYSTEM_AD_UPDATE, "广告位更新");
		idmap.put(OperationLogUtil.SYSTEM_AD_DEL, "广告位删除");
		idmap.put(OperationLogUtil.SYSTEM_PAYSETTING, "支付设置");
		idmap.put(OperationLogUtil.SYSTEM_EXPRESS_ADD, "发货地址管理-新增发货地址");
		idmap.put(OperationLogUtil.SYSTEM_EXPRESS_UPDATE, "发货地址管理-修改发货地址");
		idmap.put(OperationLogUtil.SYSTEM_EXPRESS_DEL, "发货地址管理-删除发货地址");
		idmap.put(OperationLogUtil.SYSTEM_COMPANYINFO_UPDATE, "企业信息修改");
		idmap.put(OperationLogUtil.SYSTEM_COMPANYPLATFORM_UPDATE, "企业平台信息修改");
		idmap.put(OperationLogUtil.SYSTEM_COPARTNER_UPDATE, "发货商编辑");
		idmap.put(OperationLogUtil.SYSTEM_COPARTNER_ADD, "发货商新增");
		idmap.put(OperationLogUtil.SYSTEM_USERTYPE_ADD, "客户等级新增");
		idmap.put(OperationLogUtil.SYSTEM_USERTYPE_UPDATE, "客户等级编辑");
		idmap.put(OperationLogUtil.SYSTEM_USERTYPE_DEL, "客户等级删除");
		idmap.put(OperationLogUtil.SYSTEM_USERSTAFF_UPDATE, "企业员工编辑");
		idmap.put(OperationLogUtil.SYSTEM_USERSTAFF_ADD, "企业员工新增");
		idmap.put(OperationLogUtil.SYSTEM_USERCLIENTS_UPDATE, "客户资料更新");
		idmap.put(OperationLogUtil.SYSTEM_USERCLIENTS_ADD, "客户新增");
		idmap.put(OperationLogUtil.SYSTEM_RESETPASSWORD, "重置密码");
		idmap.put(OperationLogUtil.SYSTEM_USERPRICE_SAVE, "用户协议价保存");
		idmap.put(OperationLogUtil.SYSTEM_USERPRICE_DEL, "用户协议价删除");
		idmap.put(OperationLogUtil.SYSTEM_USERCLIENTEXTEND_ADD, "客户扩展信息添加");
		idmap.put(OperationLogUtil.SYSTEM_USERCLIENTEXTEND_UPDATE, "客户扩展信息更新");
		idmap.put(OperationLogUtil.SYSTEM_USERCLIENTEXTEND_DEL, "客户扩展信息删除");
		idmap.put(OperationLogUtil.SYSTEM_COMPANYBANK_ADD, "公司银行帐号信息添加");
		idmap.put(OperationLogUtil.SYSTEM_COMPANYBANK_DEL, "公司银行帐号信息删除");
		idmap.put(OperationLogUtil.SYSTEM_COMPANYBANK_UPDATE, "公司银行帐号信息更新");
		
		//商品
		idmap.put(OperationLogUtil.PRODUCT_BRAND_ADD, "商品品牌添加");
		idmap.put(OperationLogUtil.PRODUCT_BRAND_UPDATE, "商品品牌更新");
		idmap.put(OperationLogUtil.PRODUCT_BRAND_DEL, "商品品牌删除");
		idmap.put(OperationLogUtil.PRODUCT_CLASSIFY_ADD, "商品品牌添加");
		idmap.put(OperationLogUtil.PRODUCT_CLASSIFY_UPDATE, "商品分类更新");
		idmap.put(OperationLogUtil.PRODUCT_CLASSIFY_DEL, "商品分类删除");
		idmap.put(OperationLogUtil.PRODUCT_ADD, "商品添加");
		idmap.put(OperationLogUtil.PRODUCT_UPDATE, "商品更新");
		idmap.put(OperationLogUtil.PRODUCT_DEL, "商品删除");
		
		//资金相关
		idmap.put(OperationLogUtil.BUSSINESS_SETTLEMENT_ADD, "业绩结算新增");
		idmap.put(OperationLogUtil.BUSSINESS_SETTLEMENT_UPDATE, "业绩结算变更");
		idmap.put(OperationLogUtil.BUSSINESS_SETTLEMENT_DEL, "业绩结算删除");
		idmap.put(OperationLogUtil.BUSSINESS_SETTLEMENT_AUDIT, "业绩结算审核");
		idmap.put(OperationLogUtil.USERDRAWMONEY_ADD, "业绩提现申请");
		idmap.put(OperationLogUtil.USERDRAWMONEY_UPDATE, "业绩提现更新");
		idmap.put(OperationLogUtil.USERDRAWMONEY_AUDIT, "业绩提现审核");
		idmap.put(OperationLogUtil.PAYBILLS_AUDIT, "支付审核");
		idmap.put(OperationLogUtil.PAYBILLS_AUDIT_DEL, "删除支付审核");
		idmap.put(OperationLogUtil.PAYMENT_USERMONEY, "账户余额支付");
		idmap.put(OperationLogUtil.PAYMENT_OFFLINE, "线下支付");
		
		//订单相关
		idmap.put(OperationLogUtil.PAYBILLSORDER_CANCLE, "订单退单");
		idmap.put(OperationLogUtil.PAYBILLSORDER_PRINTPAYBILLS, "打印销售单");
		idmap.put(OperationLogUtil.PAYBILLSORDER_COMFIRM, "确认订单");
		idmap.put(OperationLogUtil.PAYBILLSORDER_UPDATE, "更新配送信息");
		idmap.put(OperationLogUtil.PAYBILLSORDER_DELIVERY, "订单发货");
		idmap.put(OperationLogUtil.PAYBILLSORDER_RECEIVED, "订单签收");
		idmap.put(OperationLogUtil.ORDER_CANCLE, "商品退单审核");
		

	}
	
	public static OperationLogUtil getInsatnce(){
		if( null == instance ){
			instance = new OperationLogUtil();
		}
		
		return instance;
	}
	
	public static String getOperationName(int operationcode){
		return OperationLogUtil.getInsatnce().idmap.get(operationcode);
	}
	
	private static OperationLogUtil instance = null;
	
	private Map<Integer, String> idmap = null;
	
	
	
	
}
