package cn.gov.xnc.system.core.util;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



/**
 * 按照日期格式创建对应的订单流水id
 * @author boyan
 * @Date 2011-4-27
 * 
 */
public class IdWorker {
	
	
	
	
	private static final FieldPosition HELPER_POSITION = new FieldPosition(0); 
	private final static Format dateFormat = new SimpleDateFormat("yyyyMMddHHmmssS"); 
	private final static NumberFormat numberFormat = new DecimalFormat("0000"); 
	private static int seq = 0; 
	private static final int MAX = 9999;
	
	public static String TABLE_COMPANY_STOCK = "xnc_company_stock";
	public static String TABLE_ORDER_REBACK = "xnc_order_reback";
	public static String TABLE_ORDER_REMARK_DETAIL = "xnc_order_reback_detail";
	public static String XNC_COMPANY_STOCK_TRANSFER = "xnc_company_stock_transfer";
	public static String XNC_COMPANY_STOCK_TRANSFER_DETAIL = "xnc_company_stock_transfer_detail";
	


	/**
	 * 随即生成指定位数的含数字验证码字符串
	 * 
	 * @author Peltason
	 * @date 2007-5-9
	 * @param bit
	 *            指定生成验证码位数
	 * @return String
	 */
    /** 
     * 时间格式生成序列 
     * @return String 
     */ 
    public static synchronized String generateSequenceNo() { 
 
       // Calendar rightNow = Calendar.getInstance(); 
 
        StringBuffer sb = new StringBuffer(); 
 
        dateFormat.format(DateUtils.getDate().getTime(), sb, HELPER_POSITION); 
        numberFormat.format(seq, sb, HELPER_POSITION); 
        
        if (seq == MAX) {
            seq = 0; 
        } else { 
            seq++; 
        } 
        
       
 
        return sb.toString(); 
    } 
	
    /** 
     * 时间格式生成序列 
     * @return String 
     */ 
    public static synchronized String generateSequenceNoDate( Date date) { 
 
        //Calendar rightNow = Calendar.getInstance(); 
 
        StringBuffer sb = new StringBuffer(); 
 
        dateFormat.format(date.getTime(), sb, HELPER_POSITION); 
 
        numberFormat.format(seq, sb, HELPER_POSITION); 
 
        if (seq == MAX) { 
            seq = 0; 
        } else { 
            seq++; 
        } 
        
        return sb.toString(); 
    }
	    /**
		 * @param args
		 */
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			IdWorker IdWorker  = new IdWorker();
			for(int i = 0 ; i <10 ; i++){
				System.out.println(IdWorker.generateSequenceNo());
			}
			
		}

	
	
}
