/**
 * 
 */
package cn.gov.xnc.system.core.express;

import com.alibaba.fastjson.JSONObject;

/**
 * @author Administrator
 *
 */
public class Order {

	/*
	 * 必填！订单编号
	 */
	private String OrderCode;//必填！订单编号
	
	/*
	 * 快递公司编码
	 */
	private String ShipperCode;//快递公司编码
	
	/*
	 * 快递单号
	 */
	private String LogisticCode;//快递单号
	
	/*
	 * 大头笔
	 */
	private String MarkDestination;//大头笔
	
	/*
	 * 始发地区域编码
	 */
	private String OriginCode;//始发地区域编码
	
	/*
	 * 始发地/始发网点
	 */
	private String OriginName;//始发地/始发网点
	
	/*
	 * 目的地区域编码
	 */
	private String DestinatioCode;//目的地区域编码
	
	/*
	 * 目的地/到达网点
	 */
	private String DestinatioName;//目的地/到达网点
	
	/*
	 * 分拣编码
	 */
	private String SortingCode;//分拣编码
	
	/*
	 * 集包编码
	 */
	private String PackageCode;//集包编码
	
	
	public String toJsonString(){
		JSONObject obj = new JSONObject();
		if( null != OrderCode ){
			obj.put("OrderCode", OrderCode);
		}
		if( null != ShipperCode ){
			obj.put("ShipperCode", ShipperCode);
		}
		if( null != LogisticCode ){
			obj.put("LogisticCode", LogisticCode);
		}
		if( null != MarkDestination ){
			obj.put("MarkDestination", MarkDestination);
		}
		if( null != OriginName ){
			obj.put("OriginName", OriginName);
		}
		if( null != DestinatioCode ){
			obj.put("DestinatioCode", DestinatioCode);
		}
		if( null != DestinatioName ){
			obj.put("DestinatioName", DestinatioName);
		}
		if( null != OriginCode ){
			obj.put("OriginCode", OriginCode);
		}
		if( null != SortingCode ){
			obj.put("SortingCode", SortingCode);
		}
		if( null != PackageCode ){
			obj.put("PackageCode", PackageCode);
		}
		
		
		return obj.toJSONString();
	}
	

	/**
	 * 必填！订单编号
	 * @return the orderCode
	 */
	public String getOrderCode() {
		return OrderCode;
	}

	/**
	 * 必填！订单编号
	 * @param orderCode the orderCode to set
	 */
	public void setOrderCode(String orderCode) {
		OrderCode = orderCode;
	}

	/**
	 * 快递公司编码
	 * @return the shipperCode
	 */
	public String getShipperCode() {
		return ShipperCode;
	}

	/**
	 * 快递公司编码
	 * @param shipperCode the shipperCode to set
	 */
	public void setShipperCode(String shipperCode) {
		ShipperCode = shipperCode;
	}

	/**
	 * 快递单号
	 * @return the logisticCode
	 */
	public String getLogisticCode() {
		return LogisticCode;
	}

	/**
	 * 快递单号
	 * @param logisticCode the logisticCode to set
	 */
	public void setLogisticCode(String logisticCode) {
		LogisticCode = logisticCode;
	}

	/**
	 * 大头笔
	 * @return the markDestination
	 */
	public String getMarkDestination() {
		return MarkDestination;
	}

	/**
	 * 大头笔
	 * @param markDestination the markDestination to set
	 */
	public void setMarkDestination(String markDestination) {
		MarkDestination = markDestination;
	}

	/**
	 * 始发地区域编码
	 * @return the originCode
	 */
	public String getOriginCode() {
		return OriginCode;
	}

	/**
	 * 始发地区域编码
	 * @param originCode the originCode to set
	 */
	public void setOriginCode(String originCode) {
		OriginCode = originCode;
	}

	/**
	 * 始发地/始发网点
	 * @return the originName
	 */
	public String getOriginName() {
		return OriginName;
	}

	/**
	 * 始发地/始发网点
	 * @param originName the originName to set
	 */
	public void setOriginName(String originName) {
		OriginName = originName;
	}

	/**
	 * 目的地区域编码
	 * @return the destinatioCode
	 */
	public String getDestinatioCode() {
		return DestinatioCode;
	}

	/**
	 * 目的地区域编码
	 * @param destinatioCode the destinatioCode to set
	 */
	public void setDestinatioCode(String destinatioCode) {
		DestinatioCode = destinatioCode;
	}

	/**
	 * 目的地/到达网点
	 * @return the destinatioName
	 */
	public String getDestinatioName() {
		return DestinatioName;
	}

	/**
	 * 目的地/到达网点
	 * @param destinatioName the destinatioName to set
	 */
	public void setDestinatioName(String destinatioName) {
		DestinatioName = destinatioName;
	}

	/**
	 * 分拣编码
	 * @return the sortingCode
	 */
	public String getSortingCode() {
		return SortingCode;
	}

	/**
	 * 分拣编码
	 * @param sortingCode the sortingCode to set
	 */
	public void setSortingCode(String sortingCode) {
		SortingCode = sortingCode;
	}

	/**
	 * 集包编码
	 * @return the packageCode
	 */
	public String getPackageCode() {
		return PackageCode;
	}

	/**
	 * 集包编码
	 * @param packageCode the packageCode to set
	 */
	public void setPackageCode(String packageCode) {
		PackageCode = packageCode;
	}

}
