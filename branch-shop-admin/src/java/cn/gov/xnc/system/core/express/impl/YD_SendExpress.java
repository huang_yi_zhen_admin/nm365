/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * @author fyy
 *
 */
public class YD_SendExpress extends BaseSendExpress {
	
	//快递类型  标准快递
	public final static int YD_ExpType_1 = 1;//快递类型自己联系

	/**
	 * 
	 */
	public YD_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.YD);
	}
	
	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_1 = new HashMap<String,String>();
		exptype_1.put("exptype", "1");
		exptype_1.put("exptypename", "标准快递");
		results.add(exptype_1);
		
		

		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(1);
		data.setPageNum(1);
		data.setShowNum(1);
		data.setResults(results);	
		
		return data;
	}
	
	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		if( StringUtil.isEmpty(getCustomerName()) ){
			errMsg.append("客户ID");
			return false;
		}
		if(StringUtil.isEmpty(getCustomerPwd())){
			errMsg.append("接口联调密码");
			return false;
		}
		
		return super.checkdata(errMsg);
	}


}
