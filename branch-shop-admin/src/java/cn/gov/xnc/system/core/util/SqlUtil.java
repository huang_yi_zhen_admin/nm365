package cn.gov.xnc.system.core.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtils;

public final class SqlUtil
{
  private static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

  private static ThreadLocal threadlocal = new ThreadLocal();

  public static DateFormat getDateFormat()
  {
    return (DateFormat)threadlocal.get();
  }

  public static void setDateFormat(String format)
  {
    DATE_FORMAT = format;
  }

  public static String makeSelectAllSql(String tableName, Map<String, Object> where)
  {
    int ws = where != null ? where.size() : 0;
    StringBuilder sql = new StringBuilder(64 + ws * 32);
    sql.append("\n select * from ").append(tableName);
    sql.append("\n where ");
    int index = 0;
    for (String key : where.keySet()) {
      Object v = where.get(key);
      sql.append(key).append("=").append(sqlValue(v));
      index++;
      if (index < ws) {
        sql.append("\n   and ");
      }
    }
    return sql.toString();
  }

  
  public static String makeInsertSql(String tableName, Map<String, Object> columns)
  {
    return getInsertSql(tableName, columns);
  }

 
  public static String makeUpdateSql(String tableName, Map<String, Object> set, Map<String, Object> where)
  {
    if (set == null) {
      throw new IllegalArgumentException("update的字段集合不能为null");
    }
    int ss = set.size();
    int ws = where != null ? where.size() : 0;
    StringBuilder sql = new StringBuilder(64 + ss * 32 + ws * 32);

    sql.append("\n update ").append(tableName).append("\n set ");
    int index = 0;
    for (String key : set.keySet()) {
      Object v = set.get(key);
      sql.append("\t").append(key).append("=").append(sqlValue(v));
      index++;
      if (index < ss) {
        sql.append(",\n");
      }
    }

    if (ws == 0) {
      return sql.toString();
    }

    sql.append("\n where ");
    index = 0;
    for (String key : where.keySet()) {
      Object v = where.get(key);
      sql.append(key).append("=").append(sqlValue(v));
      index++;
      if (index < ws) {
        sql.append("\n   and ");
      }
    }
    return sql.toString();
  }

  
  public static String makeDeleteSql(String tableName, Map<String, Object> where)
  {
    int ws = where != null ? where.size() : 0;
    StringBuilder sql = new StringBuilder(64 + ws * 32);
    sql.append("\n delete from ").append(tableName);
    if (ws == 0) {
      return sql.toString();
    }
    sql.append("\n where ");
    int index = 0;
    for (String key : where.keySet()) {
      Object v = where.get(key);
      sql.append(key).append("=").append(sqlValue(v));
      index++;
      if (index < ws) {
        sql.append("\n   and ");
      }
    }
    return sql.toString();
  }

  
  public static String makeDynamicSql(String dynamicSql, Object params)
  {
    Map b = null;
    if ((params instanceof Map))
      b = (Map)params;
    else {
      try {
        b = BeanUtils.describe(params);
      } catch (Exception ex) {
        throw new IllegalArgumentException("BeanUtils.describe(bean)异常", ex);
      }
    }
    return makeDynamicSql(dynamicSql, b);
  }

  
  public static String makeDynamicSql(String dynamicSql, Map<String, Object> params)
  {
    int ps = dynamicSql.length();
    StringBuilder sql = new StringBuilder(128 + ps * 2);
    StringBuilder item = new StringBuilder(128);

    boolean isDynamicStart = false;
    char c = '\000';
    for (int i = 0; i < ps; i++) {
      c = dynamicSql.charAt(i);
      if (isDynamicStart) {
        if ('}' == c) {
          isDynamicStart = false;
          String ii = item.toString();
          item.setLength(0);
          sql.append(makeDynamicItem(ii, params));
        } else {
          item.append(c);
        }
      }
      else if ('{' == c)
        isDynamicStart = true;
      else {
        sql.append(c);
      }
    }

    if (item.length() > 0) {
      String ii = item.toString();
      sql.append(makeDynamicItem(ii, params));
    }
    return sql.toString();
  }

  
  private static String makeDynamicItem(String dynamicItem, Map<String, Object> params)
  {
    int ps = dynamicItem.length();
    StringBuilder sqlItem = new StringBuilder(64 + ps * 2);
    StringBuilder param = new StringBuilder(64);

    boolean isParamStart = false;
    char c = '\000';
    char flag = '\000';
    for (int i = 0; i < ps; i++) {
      c = dynamicItem.charAt(i);
      if (isParamStart) {
        if ((' ' == c) || ('\n' == c) || ('\t' == c) || ('\r' == c)) {
          isParamStart = false;
          String p = param.toString();
          param.setLength(0);
          Object v = params.get(p);
          if ('$' == flag) {
            if (v == null) {
              return "";
            }
            sqlItem.append(sqlValue(v));
          } else if ('#' == flag) {
            sqlItem.append(sqlValue(v));
          } else if ('&' == flag) {
            if (v == null) {
              return "";
            }
            sqlItem.append(v);
          }
        } else {
          param.append(c);
        }
      }
      else if (('$' == c) || ('#' == c) || ('&' == c) || ('?' == c) || ('@' == c)) {
        flag = c;
        isParamStart = true;
      } else {
        sqlItem.append(c);
      }
    }

    if (param.length() > 0) {
      String p = param.toString();
      Object v = params.get(p);
      if ('$' == flag) {
        if (v == null) {
          return "";
        }
        sqlItem.append(sqlValue(v));
      } else if ('#' == flag) {
        sqlItem.append(sqlValue(v));
      }
    }
    return sqlItem.toString();
  }

  private static String sqlValue(String value)
  {
    if (value == null) {
      return "''";
    }
    String v = value.trim();
    int vs = v.length();
    StringBuilder sb = new StringBuilder(2 + vs * 2);
    char c = '\000';
    sb.append('\'');
    for (int i = 0; i < vs; i++) {
      c = v.charAt(i);

      if ('\'' == c) {
        sb.append('\'');
        sb.append('\'');
      } else if ('\\' == c) {
        sb.append('\\');
        sb.append('\\');
      } else {
        sb.append(c);
      }
    }
    sb.append('\'');
    return sb.toString();
  }

  private static String sqlValue(Date value)
  {
    return "'" + value + "'";
  }

  public static Date parseObject(String value) {
    try {
      return (Date)getDateFormat().parseObject(value);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String sqlValue(Date value, SimpleDateFormat simpleDateFormat)
  {
    return "'" + simpleDateFormat.format(value) + "'";
  }

  private static String sqlValue(Timestamp value)
  {
    return "'" + value + "'";
  }

  private static <T> String sqlValuePrimitive(T value)
  {
    return value.toString();
  }

  private static <T> String sqlValueArray(T[] value)
  {
    if (value == null) {
      return "''";
    }
    StringBuilder sql = new StringBuilder(64 + value.length * 32);
    for (int i = 0; i < value.length; i++) {
      sql.append(sqlValue(value[i]));
      if (i < value.length - 1) {
        sql.append(",");
      }
    }
    return sql.toString();
  }

  public static String sqlValue(Object value)
  {
    if (value == null)
      return "''";
    if ((value instanceof String))
      return sqlValue((String)value);
    if ((value instanceof Date))
      return sqlValue((Date)value);
    if ((value instanceof Timestamp))
      return sqlValue((Timestamp)value);
    if (((value instanceof Integer)) || ((value instanceof Long)) || ((value instanceof Short)) || ((value instanceof Float)) || ((value instanceof Double)))
    {
      return sqlValuePrimitive(value);
    }if ((value instanceof List))
      return sqlValueArray(((List)value).toArray());
    if (value.getClass().isArray())
    {
      Class ct = value.getClass().getComponentType();
      if (ct == String.class)
        return sqlValueArray((String[]) value);
      if (ct == Integer.TYPE)
        return sqlValueArray(boxedPrimitiveArray((int[])value));
      if (ct == Long.TYPE)
        return sqlValueArray(boxedPrimitiveArray((long[])value));
      if (ct == Short.TYPE)
        return sqlValueArray(boxedPrimitiveArray((short[])value));
      if (ct == Float.TYPE)
        return sqlValueArray(boxedPrimitiveArray((float[])value));
      if (ct == Double.TYPE) {
        return sqlValueArray(boxedPrimitiveArray((double[])value));
      }

      return sqlValueArray((Object[])value);
    }
    return "'" + value.toString() + "'";
  }

  private static Integer[] boxedPrimitiveArray(int[] array)
  {
    Integer[] result = new Integer[array.length];
    for (int i = 0; i < array.length; i++)
      result[i] = Integer.valueOf(array[i]);
    return result;
  }

  private static Short[] boxedPrimitiveArray(short[] array)
  {
    Short[] result = new Short[array.length];
    for (int i = 0; i < array.length; i++)
      result[i] = Short.valueOf(array[i]);
    return result;
  }

  private static Long[] boxedPrimitiveArray(long[] array)
  {
    Long[] result = new Long[array.length];
    for (int i = 0; i < array.length; i++)
      result[i] = Long.valueOf(array[i]);
    return result;
  }

  private static Float[] boxedPrimitiveArray(float[] array)
  {
    Float[] result = new Float[array.length];
    for (int i = 0; i < array.length; i++)
      result[i] = Float.valueOf(array[i]);
    return result;
  }

  private static Double[] boxedPrimitiveArray(double[] array)
  {
    Double[] result = new Double[array.length];
    for (int i = 0; i < array.length; i++)
      result[i] = Double.valueOf(array[i]);
    return result;
  }

  public static String getSql(String prepareSql, Object[] params)
  {
    if (params != null) {
      int length = prepareSql.length();
      StringBuilder result = new StringBuilder(2 + length * 2);
      int paramIndex = 0;
      for (int i = 0; i < length; i++) {
        char c = prepareSql.charAt(i);
        if (c == '?') {
          result.append(sqlValue(params[paramIndex]));
          paramIndex++;
        } else {
          result.append(c);
        }
      }
      return result.toString();
    }
    return prepareSql;
  }

  public static String getSqlByList(String prepareSql, List<Object> params)
  {
    if (params != null) {
      int length = prepareSql.length();
      StringBuilder result = new StringBuilder(2 + length * 2);
      int paramIndex = 0;
      for (int i = 0; i < length; i++) {
        char c = prepareSql.charAt(i);
        if (c == '?') {
          result.append(sqlValue(params.get(paramIndex)));
          paramIndex++;
        } else {
          result.append(c);
        }
      }
      return result.toString();
    }
    return prepareSql;
  }

  public static String getInsertSql(String tableName, Map<String, Object> columns)
  {
    int columnSize = columns.size();
    StringBuilder sql = new StringBuilder(64 + columnSize * 32);
    sql.append("\n insert into ").append(tableName);
    sql.append(" ( ");
    int index = 0;
    for (String item : columns.keySet()) {
      sql.append(item);
      index++;
      if (index != columnSize) {
        sql.append(",");
      }
    }
    sql.append(" )\n");
    sql.append(" values ( ");
    index = 0;
    for (String item : columns.keySet()) {
      Object value = columns.get(item);
      sql.append(sqlValue(value));
      index++;
      if (index != columnSize) {
        sql.append(",");
      }
    }
    sql.append(" )");
    return sql.toString();
  }
}