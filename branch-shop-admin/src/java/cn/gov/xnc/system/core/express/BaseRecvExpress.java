/**
 * 
 */
package cn.gov.xnc.system.core.express;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


import cn.gov.xnc.admin.express.entity.ExpressEntity;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.service.SystemService;

/**
 * @author Administrator
 * 快递鸟返回数据
 */
public class BaseRecvExpress {
	

	//@Autowired
	//private ExpressServiceI expressService ;
	//@Autowired
	//private SystemService systemService;
	
	public BaseSendExpress basesend;
	
	public BaseRecvExpress(){
		
	}

	/*
	 * 必填！电商用户ID
	 */
	private String EBusinessID;//必填！电商用户ID
	
	/*
	 * 订单信息
	 */
	private Order order;//订单信息
	
	/*
	 * 必填！成功与否
	 */
	private boolean Success;//必填！成功与否
	
	/*
	 * 必填！错误编码
	 */
	private String ResultCode;//必填！错误编码
	
	/*
	 * 失败原因
	 */
	private String Reason;//失败原因
	
	/*
	 * 必填！唯一标识
	 */
	private String UniquerRequestNumber;//必填！唯一标识
	
	/*
	 * 面单打印模板
	 */
	private String PrintTemplate;//面单打印模板
	
	/*
	 * 订单预计到货时间yyyy-mm-dd
	 */
	private String EstimatedDeliveryTime;//订单预计到货时间yyyy-mm-dd
	
	/*
	 * 用户自定义回调信息
	 */
	private String Callback;//用户自定义回调信息
	
	/*
	 * 子单数量
	 */
	private int SubCount;//子单数量
	
	/*
	 * 子单号
	 */
	private String SubOrders;//子单号
	
	/*
	 * 子单模板
	 */
	private String SubPrintTemplates;//子单模板
	
	/*
	 * 收件人安全电话
	 */
	private String ReceiverSafePhone;//收件人安全电话
	
	/*
	 * 寄件人安全电话
	 */
	private String SenderSafePhone;//寄件人安全电话
	
	/*
	 * 拨号页面网址（转换成二维码可扫描拨号）
	 */
	private String DialPage;//拨号页面网址（转换成二维码可扫描拨号）
	
	/*
	 * 接收的json字符串
	 */
	private String jsonstr;//接收的json字符串
	
	/**
	 * @return the basesend
	 */
	public BaseSendExpress getBasesend() {
		return basesend;
	}

	/**
	 * @param basesend the basesend to set
	 */
	public void setBasesend(BaseSendExpress basesend) {
		this.basesend = basesend;
	}

	/*
	 * 获取电子面单
	 */
	public void doExpress(SystemService systemService,BaseSendExpress send){
		
		basesend = send;
		
		StringBuffer errMsg = new StringBuffer();
		if( !send.checkdata(errMsg) ){
			setResultCode("INNERERROR");
			setReason(errMsg.toString());
			return ;
		}
		
		Map<String, String> params = null;
		try {
			params = send.packData(errMsg);
		} catch (Exception e) {
			e.printStackTrace();
			setResultCode("INNERERROR");
			setReason(errMsg.toString());
			return ;
		}
		
		//将发送数据写入数据库
		ExpressEntity express = sendDataToExpressEntity(send,systemService);
		boolean isSuccess = saveExpress(systemService,express);
		
		String result=ExpressUtil.sendPost(ExpressUtil.ReqURL, params);
		parseJson(result);
		
		updateExpressEntityWithRecv(express);
		
		//接收数据后保存
		isSuccess = updateExpress(systemService,express);
		
		return ;
	}
	
	public void updateExpressEntityWithRecv(ExpressEntity entity){
		if( null != order ){
			entity.setOrder(order.toJsonString());
			entity.setLogisticcode(order.getLogisticCode());
		}
		

		if( true == Success ){
			entity.setSuccess(1);
		}else{
			entity.setSuccess(0);
		}
		entity.setResultcode(ResultCode);
		entity.setReason(Reason);
		entity.setUniquerrequestnumber(UniquerRequestNumber);
		entity.setPrinttemplate(PrintTemplate);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟
		if( null!= EstimatedDeliveryTime){
			try {
				entity.setEstimateddeliverytime(sdf.parse(EstimatedDeliveryTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		entity.setSubcount(""+SubCount);
		entity.setSuborders(SubOrders);
		entity.setSubprinttemplates(SubPrintTemplates);
		entity.setReceiversafephone(ReceiverSafePhone);
		entity.setSendersafephone(SenderSafePhone);
		entity.setDialpage(DialPage);
		entity.setRecvdata(jsonstr);
		
	}
	
	public boolean updateExpress(SystemService systemService, ExpressEntity entity){
		try {
			
			systemService.updateEntitie(entity);
			systemService.addLog("电子面单请求入库成功", Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean saveExpress(SystemService systemService, ExpressEntity entity){
		try {
			
			systemService.save(entity);
			systemService.addLog("电子面单请求入库成功", Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public ExpressEntity sendDataToExpressEntity(BaseSendExpress send,SystemService systemService){
		if( send == null ){
			return null;
		}
		
		ExpressEntity entity = new ExpressEntity();
		
		entity.setId(IdWorker.generateSequenceNo());
		entity.setCallback(send.getRequestData().getCallBack());
		entity.setMemberid(send.getRequestData().getMemberID());
		entity.setCustomername(send.getRequestData().getCustomerName());
		entity.setCustomerpwd(send.getRequestData().getCustomerPwd());
		entity.setSendsite(send.getRequestData().getSendSite());
		entity.setShippercode(send.getRequestData().getShipperCode());
		entity.setLogisticcode(send.getRequestData().getLogisticCode());
		entity.setThrordercode(send.getRequestData().getThrOrderCode());
		entity.setOrdercode(send.getRequestData().getOrderCode());
		entity.setMonthcode(send.getRequestData().getMonthCode());
		entity.setPaytype(send.getRequestData().getPayType());
		entity.setExptype(send.getRequestData().getExpType());
		entity.setIsnotice(send.getRequestData().getIsNotice());
		entity.setCost(new BigDecimal(send.getRequestData().getCost()));
		entity.setOthercost(new BigDecimal(send.getRequestData().getOtherCost()));
		entity.setReceivercompany(send.getRequestData().getReceiver().getCompany());
		entity.setReceivername(send.getRequestData().getReceiver().getName());
		entity.setReceivertel(send.getRequestData().getReceiver().getTel());
		entity.setReceivermobile(send.getRequestData().getReceiver().getMobile());
		entity.setReceiverpostcode(send.getRequestData().getReceiver().getPostCode());
		entity.setReceiverprovincename(send.getRequestData().getReceiver().getProvinceName());
		entity.setReceivercityname(send.getRequestData().getReceiver().getCityName());
		entity.setReceiverexpareaname(send.getRequestData().getReceiver().getExpAreaName());
		entity.setReceiveraddress(send.getRequestData().getReceiver().getAddress());
		entity.setSendercompany(send.getRequestData().getSender().getCompany());
		entity.setSendername(send.getRequestData().getSender().getName());
		entity.setSendertel(send.getRequestData().getSender().getTel());
		entity.setSendermobile(send.getRequestData().getSender().getMobile());
		entity.setSenderpostcode(send.getRequestData().getSender().getPostCode());
		entity.setSenderprovincename(send.getRequestData().getSender().getProvinceName());
		entity.setSendercityname(send.getRequestData().getSender().getCityName());
		entity.setSenderexpareaname(send.getRequestData().getSender().getExpAreaName());
		entity.setSenderaddress(send.getRequestData().getSender().getAddress());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟  
		try {
			if( !StringUtil.isEmpty(send.getRequestData().getStartDate()) && !StringUtil.isEmpty(send.getRequestData().getEndDate()))
			{
				entity.setStartdate(sdf.parse(send.getRequestData().getStartDate()));
				entity.setEnddate(sdf.parse(send.getRequestData().getEndDate()));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		entity.setWeight(new BigDecimal(send.getRequestData().getWeight()));
		entity.setQuantity(send.getRequestData().getQuantity());
		entity.setVolume(new BigDecimal(send.getRequestData().getVolume()));
		entity.setRemark(send.getRequestData().getRemark());
		
		if( null != send.getRequestData().getAddService() ){
			entity.setAddservice(send.getRequestData().getAddService().toJsonString());
		}
		
		JSONArray commodityListObj = new JSONArray();
		commodityListObj.addAll(send.getRequestData().getCommodity());
		entity.setCommodity(commodityListObj.toJSONString());
		
		entity.setIsreturnprinttemplate(Integer.parseInt(send.getRequestData().getIsReturnPrintTemplate()));
		entity.setSenddata(send.getRequestData().toJsonStr());
		
		OrderEntity order = systemService.findUniqueByProperty(OrderEntity.class, "id", send.getOrderid());
		entity.setOrderid(order);
		
		return entity;
	}
	
	/**
	 * 返回是否成功
	 * @return isSucccess
	 */
	public void parseJson(String json){

		JSONObject obj = new JSONObject();
		jsonstr = json;
		obj = obj.parseObject(jsonstr);
		
		EBusinessID = obj.getString("EBusinessID");
		
		JSONObject orderObj = obj.getJSONObject("Order");
		if( null != orderObj ){
			order = new Order();
			String OrderCode = orderObj.getString("OrderCode");
			if( null != OrderCode ){
				order.setOrderCode(OrderCode);
			}
			String ShipperCode = orderObj.getString("ShipperCode");
			if( null != ShipperCode ){
				order.setShipperCode(ShipperCode);
			}
			String LogisticCode = orderObj.getString("LogisticCode");
			if( null != LogisticCode ){
				order.setLogisticCode(LogisticCode);
			}
			String MarkDestination = orderObj.getString("MarkDestination");
			if( null != MarkDestination ){
				order.setMarkDestination(MarkDestination);
			}
			String OriginCode = orderObj.getString("OriginCode");
			if( null != OriginCode ){
				order.setOriginCode(OriginCode);
			}
			String OriginName = orderObj.getString("OriginName");
			if( null != OriginName ){
				order.setOriginName(OriginName);
			}
			String DestinatioCode = orderObj.getString("DestinatioCode");
			if( null != DestinatioCode ){
				order.setDestinatioCode(DestinatioCode);
			}
			String DestinatioName = orderObj.getString("DestinatioName");
			if( null != DestinatioName ){
				order.setDestinatioName(DestinatioName);
			}
			String SortingCode = orderObj.getString("SortingCode");
			if( null != SortingCode ){
				order.setSortingCode(SortingCode);
			}
			String PackageCode = orderObj.getString("PackageCode");
			if( null != PackageCode ){
				order.setPackageCode(PackageCode);
			}
		}
		
		Success = obj.getBoolean("Success");
		ResultCode = obj.getString("ResultCode");
		Reason = obj.getString("Reason");
		UniquerRequestNumber = obj.getString("UniquerRequestNumber");
		PrintTemplate = obj.getString("PrintTemplate");
		EstimatedDeliveryTime = obj.getString("EstimatedDeliveryTime");
		Callback = obj.getString("Callback");
		SubCount = obj.getIntValue("SubCount");
		SubOrders = obj.getString("SubOrders");
		SubPrintTemplates = obj.getString("SubPrintTemplates");
		ReceiverSafePhone = obj.getString("ReceiverSafePhone");
		SenderSafePhone = obj.getString("SenderSafePhone");
		DialPage = obj.getString("DialPage");
		
	}
	
	/**
	 * 必填！电商用户ID
	 * @return the eBusinessID
	 */
	public String getEBusinessID() {
		return EBusinessID;
	}

	/**
	 * 必填！电商用户ID
	 * @param eBusinessID the eBusinessID to set
	 */
	public void setEBusinessID(String eBusinessID) {
		EBusinessID = eBusinessID;
	}

	/**
	 * 成功与否
	 * @return the success
	 */
	public boolean isSuccess() {
		return Success;
	}

	/**
	 * 成功与否
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		Success = success;
	}

	/**
	 * @return the resultCode
	 */
	public String getResultCode() {
		return ResultCode;
	}

	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(String resultCode) {
		ResultCode = resultCode;
	}

	/**
	 * 订单信息
	 * @return the order
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * 订单信息
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		this.order = order;
	}

	/**
	 * 失败原因
	 * @return the reason
	 */
	public String getReason() {
		return Reason;
	}

	/**
	 * 失败原因
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		Reason = reason;
	}

	/**
	 * 必填！唯一标识
	 * @return the uniquerRequestNumber
	 */
	public String getUniquerRequestNumber() {
		return UniquerRequestNumber;
	}

	/**
	 * 必填！唯一标识
	 * @param uniquerRequestNumber the uniquerRequestNumber to set
	 */
	public void setUniquerRequestNumber(String uniquerRequestNumber) {
		UniquerRequestNumber = uniquerRequestNumber;
	}

	/**
	 * 面单打印模板
	 * @return the printTemplate
	 */
	public String getPrintTemplate() {
		return PrintTemplate;
	}

	/**
	 * 面单打印模板
	 * @param printTemplate the printTemplate to set
	 */
	public void setPrintTemplate(String printTemplate) {
		PrintTemplate = printTemplate;
	}

	/**
	 * 订单预计到货时间yyyy-mm-dd
	 * @return the estimatedDeliveryTime
	 */
	public String getEstimatedDeliveryTime() {
		return EstimatedDeliveryTime;
	}

	/**
	 * 订单预计到货时间yyyy-mm-dd
	 * @param estimatedDeliveryTime the estimatedDeliveryTime to set
	 */
	public void setEstimatedDeliveryTime(String estimatedDeliveryTime) {
		EstimatedDeliveryTime = estimatedDeliveryTime;
	}

	/**
	 * 用户自定义回调信息
	 * @return the callback
	 */
	public String getCallback() {
		return Callback;
	}

	/**
	 * 用户自定义回调信息
	 * @param callback the callback to set
	 */
	public void setCallback(String callback) {
		Callback = callback;
	}

	/**
	 * 子单数量
	 * @return the subCount
	 */
	public int getSubCount() {
		return SubCount;
	}

	/**
	 * 子单数量
	 * @param subCount the subCount to set
	 */
	public void setSubCount(int subCount) {
		SubCount = subCount;
	}

	/**
	 * 子单号
	 * @return the subOrders
	 */
	public String getSubOrders() {
		return SubOrders;
	}

	/**
	 * 子单号
	 * @param subOrders the subOrders to set
	 */
	public void setSubOrders(String subOrders) {
		SubOrders = subOrders;
	}

	/**
	 * 子单模板
	 * @return the subPrintTemplates
	 */
	public String getSubPrintTemplates() {
		return SubPrintTemplates;
	}

	/**
	 * 子单模板
	 * @param subPrintTemplates the subPrintTemplates to set
	 */
	public void setSubPrintTemplates(String subPrintTemplates) {
		SubPrintTemplates = subPrintTemplates;
	}

	/**
	 * 收件人安全电话
	 * @return the receiverSafePhone
	 */
	public String getReceiverSafePhone() {
		return ReceiverSafePhone;
	}

	/**
	 * 收件人安全电话
	 * @param receiverSafePhone the receiverSafePhone to set
	 */
	public void setReceiverSafePhone(String receiverSafePhone) {
		ReceiverSafePhone = receiverSafePhone;
	}

	/**
	 * 寄件人安全电话
	 * @return the senderSafePhone
	 */
	public String getSenderSafePhone() {
		return SenderSafePhone;
	}

	/**
	 * 寄件人安全电话
	 * @param senderSafePhone the senderSafePhone to set
	 */
	public void setSenderSafePhone(String senderSafePhone) {
		SenderSafePhone = senderSafePhone;
	}

	/**
	 * 拨号页面网址（转换成二维码可扫描拨号）
	 * @return the dialPage
	 */
	public String getDialPage() {
		return DialPage;
	}

	/**
	 * 拨号页面网址（转换成二维码可扫描拨号）
	 * @param dialPage the dialPage to set
	 */
	public void setDialPage(String dialPage) {
		DialPage = dialPage;
	}

	/**
	 * 接收的json字符串
	 * @return the jsonstr
	 */
	public String getJsonstr() {
		return jsonstr;
	}

	/**
	 * 接收的json字符串
	 * @param jsonstr the jsonstr to set
	 */
	public void setJsonstr(String jsonstr) {
		this.jsonstr = jsonstr;
	}
	
	
}
