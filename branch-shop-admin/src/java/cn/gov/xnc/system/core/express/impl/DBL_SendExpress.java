/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * @author fyy
 *
 */
public class DBL_SendExpress extends BaseSendExpress {
	
	//快递类型  标准快递
	public final static int DBL_ExpType_1 = 1;//快递类型 标准快递
	//快递类型  360特惠件
	public final static int DBL_ExpType_2 = 2;//快递类型 360特惠件
	//快递类型  电商尊享
	public final static int DBL_ExpType_3 = 3;//快递类型 电商尊享
	//快递类型  特准快件
	public final static int DBL_ExpType_4 = 4;//快递类型 特准快件
	
	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_1 = new HashMap<String,String>();
		exptype_1.put("exptype", "1");
		exptype_1.put("exptypename", "标准快递");
		results.add(exptype_1);
		
		HashMap<String,String> exptype_2 = new HashMap<String,String>();
		exptype_2.put("exptype", "2");
		exptype_2.put("exptypename", "360特惠件");
		results.add(exptype_2);
		
		HashMap<String,String> exptype_3 = new HashMap<String,String>();
		exptype_3.put("exptype", "3");
		exptype_3.put("exptypename", "电商尊享");
		results.add(exptype_3);
		
		HashMap<String,String> exptype_4 = new HashMap<String,String>();
		exptype_4.put("exptype", "4");
		exptype_4.put("exptypename", "特准快件");
		results.add(exptype_4);

		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(4);
		data.setPageNum(1);
		data.setShowNum(4);
		data.setResults(results);	
		
		return data;
	}

	/**
	 * 
	 */
	public DBL_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.DBL);
	}
	
	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		if( StringUtil.isEmpty(getCustomerName()) ){
			errMsg.append("请输入客户编码");
			return false;
		}
		
	
		return super.checkdata(errMsg);
	}

}
