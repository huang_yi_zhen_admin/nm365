package cn.gov.xnc.system.core.util;

import java.util.List;

/**
 * 非空验证工具
 * @author Leiante
 *
 */
public class ListUtil {

	/**
	 * List 是否为空
	 * @param list
	 * @return
	 */
	public static boolean isNotEmpty(List list){
		if(list != null && !list.isEmpty()){
			return true;
		}
		return false;
	}
}
