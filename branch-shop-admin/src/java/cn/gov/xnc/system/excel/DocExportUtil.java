package cn.gov.xnc.system.excel;

import java.io.IOException;
import java.util.List;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TextAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
//import cn.gov.zsyz.admin.majorProject.entity.MajorProjectEntity;
/**
 * 
* @Description: 导出doc工具 
* @author lijingzan@staff.tianya.cn  
* @date 2015-5-29 上午10:50:42
 */
public class DocExportUtil {

//	public XWPFDocument exportDoc(List<MajorProjectEntity> entity) throws IOException {
//
//		XWPFDocument doc = new XWPFDocument();
//		XWPFParagraph p1 = doc.createParagraph();
//		p1.setAlignment(ParagraphAlignment.LEFT);
//		p1.setVerticalAlignment(TextAlignment.TOP);
//		int i = 1;
//		
//		for (MajorProjectEntity m : entity) {
//
//			XWPFRun r1 = p1.createRun();
//			r1.setFontFamily("宋体");
//			r1.setFontSize(16);
//			r1.setBold(true);
//			r1.setText(m.getProjectName());
//			r1.addBreak();
//
//			XWPFRun r2 = p1.createRun();
//			r2.setBold(true);
//			r2.setFontSize(16);
//			r2.setFontFamily("宋体");
//			r2.setText("1.项目背景:");
//
//			r2 = p1.createRun();
//			r2.setFontFamily("宋体");
//			r2.setFontSize(16);
//			r2.setText(m.getProjectBackground() + "");
//			r2.addBreak();
//
//			XWPFRun r3 = p1.createRun();
//			r3.setBold(true);
//			r3.setFontSize(16);
//			r3.setFontFamily("宋体");
//			r3.setText("2.建设地点:");
//
//			r3 = p1.createRun();
//			r3.setFontFamily("宋体");
//			r3.setFontSize(16);
//			r3.setText(m.getUseraddressStreet() + "");
//			r3.addBreak();
//
//			XWPFRun r4 = p1.createRun();
//			r4.setBold(true);
//			r4.setFontSize(16);
//			r4.setFontFamily("宋体");
//			r4.setText("3.建设内容:");
//
//			r4 = p1.createRun();
//			r4.setFontFamily("宋体");
//			r4.setFontSize(16);
//			r4.setText(m.getProjectSubstance() + "");
//			r4.addBreak();
//
//			XWPFRun r5 = p1.createRun();
//			r5.setBold(true);
//			r5.setFontSize(16);
//			r5.setFontFamily("宋体");
//			r5.setText("4.投资规模:");
//
//			r5 = p1.createRun();
//			r5.setFontFamily("宋体");
//			r5.setFontSize(16);
//			r5.setText("投资了" + m.getProjectScale() + "亿");
//			r5.addBreak();
//
//			XWPFRun r6 = p1.createRun();
//			r6.setBold(true);
//			r6.setFontSize(16);
//			r6.setFontFamily("宋体");
//			r6.setText("5.联系方式:");
//
//			r6 = p1.createRun();
//			r6.setFontFamily("宋体");
//			r6.setFontSize(16);
//			r6.setText(m.getContactInformation() + "");
//			r6.addBreak();
//
//			r6 = p1.createRun();
//			r6.addBreak();
//			r6.addBreak();
//			i++;
//		}
//
//		return doc;
//	/*	FileOutputStream out = new FileOutputStream(
//				"D:\\eclipsehelloworspace\\zsyz_total\\src\\test\\cn\\gov\\zsyz\\generatecode\\8.doc");
//		doc.write(out);
//		out.close();*/
//	}

}
