package cn.gov.xnc.admin.ylb.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 玉绿宝真扶贫
 * @author zero
 * @date 2018-01-04 16:01:32
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_ylb_sales", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class YlbSalesEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**编号*/
	private java.lang.String identifier;
	/**销售网点*/
	private java.lang.String salesoutlets;
	/**销售员*/
	private java.lang.String salesstaff;
	/**销售时间*/
	private java.lang.String salestime;
	/**客户名称*/
	private java.lang.String customername;
	/**telephone*/
	private java.lang.String telephone;
	/**收货人*/
	private java.lang.String deliveryname;
	/**地址id*/
	private TSTerritory TSTerritory ;// 地址
	/**送货地址*/
	private java.lang.String deliveryaddress;
	/**收货电话*/
	private java.lang.String deliverytelephone;
	/**核销方式*/
	private java.lang.String mode;
	/**订单状态*/
	private java.lang.String state;
	/**创建时间*/
	private java.util.Date createdate;
	/**创建人*/
	private java.lang.String createuser;
	/**更新时间*/
	private java.util.Date updatedate;
	/**更新人*/
	private java.lang.String updateuser;
	/**公司信息*/
	private TSCompany company;
	
	/**省份*/
	private java.lang.String province;
	/**城市*/
	private java.lang.String city;
	/**地区*/
	private java.lang.String area;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  编号
	 */
	@Column(name ="IDENTIFIER",nullable=true,length=32)
	public java.lang.String getIdentifier(){
		return this.identifier;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  编号
	 */
	public void setIdentifier(java.lang.String identifier){
		this.identifier = identifier;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  销售网点
	 */
	@Column(name ="SALESOUTLETS",nullable=true,length=200)
	public java.lang.String getSalesoutlets(){
		return this.salesoutlets;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  销售网点
	 */
	public void setSalesoutlets(java.lang.String salesoutlets){
		this.salesoutlets = salesoutlets;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  销售员
	 */
	@Column(name ="SALESSTAFF",nullable=true,length=100)
	public java.lang.String getSalesstaff(){
		return this.salesstaff;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  销售员
	 */
	public void setSalesstaff(java.lang.String salesstaff){
		this.salesstaff = salesstaff;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  销售时间
	 */
	@Column(name ="SALESTIME",nullable=true)
	public String getSalestime(){

		return this.salestime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  销售时间
	 */
	public void setSalestime(String salestime){
		this.salestime = salestime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户名称
	 */
	@Column(name ="CUSTOMERNAME",nullable=true,length=100)
	public java.lang.String getCustomername(){
		return this.customername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户名称
	 */
	public void setCustomername(java.lang.String customername){
		this.customername = customername;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  telephone
	 */
	@Column(name ="TELEPHONE",nullable=true,length=100)
	public java.lang.String getTelephone(){
		return this.telephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  telephone
	 */
	public void setTelephone(java.lang.String telephone){
		this.telephone = telephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收货人
	 */
	@Column(name ="DELIVERYNAME",nullable=true,length=100)
	public java.lang.String getDeliveryname(){
		return this.deliveryname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收货人
	 */
	public void setDeliveryname(java.lang.String deliveryname){
		this.deliveryname = deliveryname;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地址信息
	 */
	@JsonIgnore    //getList查询转换为列表时处理json转换异常
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "territoryid")
	public TSTerritory getTSTerritory() {
		return TSTerritory;
	}

	public void setTSTerritory(TSTerritory tSTerritory) {
		TSTerritory = tSTerritory;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  送货地址
	 */
	@Column(name ="DELIVERYADDRESS",nullable=true,length=400)
	public java.lang.String getDeliveryaddress(){
		return this.deliveryaddress;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  送货地址
	 */
	public void setDeliveryaddress(java.lang.String deliveryaddress){
		this.deliveryaddress = deliveryaddress;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收货电话
	 */
	@Column(name ="DELIVERYTELEPHONE",nullable=true,length=255)
	public java.lang.String getDeliverytelephone(){
		return this.deliverytelephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收货电话
	 */
	public void setDeliverytelephone(java.lang.String deliverytelephone){
		this.deliverytelephone = deliverytelephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  核销方式
	 */
	@Column(name ="MODE",nullable=true,length=10)
	public java.lang.String getMode(){
		return this.mode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  核销方式
	 */
	public void setMode(java.lang.String mode){
		this.mode = mode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单状态
	 */
	@Column(name ="STATE",nullable=true,length=10)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单状态
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建人
	 */
	@Column(name ="CREATEUSER",nullable=true,length=100)
	public java.lang.String getCreateuser(){
		return this.createuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建人
	 */
	public void setCreateuser(java.lang.String createuser){
		this.createuser = createuser;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  更新人
	 */
	@Column(name ="UPDATEUSER",nullable=true,length=100)
	public java.lang.String getUpdateuser(){
		return this.updateuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  更新人
	 */
	public void setUpdateuser(java.lang.String updateuser){
		this.updateuser = updateuser;
	}
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  省份
	 */
	@Column(name ="PROVINCE",nullable=true,length=100)
	public java.lang.String getProvince(){
		return this.province;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  省份
	 */
	public void setProvince(java.lang.String province){
		this.province = province;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  城市
	 */
	@Column(name ="CITY",nullable=true,length=200)
	public java.lang.String getCity(){
		return this.city;
	}
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  城市
	 */
	public void setCity(java.lang.String city){
		this.city = city;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地区
	 */
	@Column(name ="AREA",nullable=true,length=300)
	public java.lang.String getArea(){
		return this.area;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  地区
	 */
	public void setArea(java.lang.String area){
		this.area = area;
	}
}
