package cn.gov.xnc.admin.express.service;

import java.util.List;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface ExpressServiceI extends CommonService{
	
	/**
	 * 设置/更新订单物流号
	 * @param shippercode
	 * @param logisticcode
	 * @param ordercode
	 * @param printtype
	 * @return
	 * @throws Exception
	 */
	public AjaxJson setOrderLogistcs(String shippercode, String logisticcode, String ordercode,String printtype) throws Exception;
	
	/**
	 * 更新订单修改页面中的物流单号信息
	 * @param order
	 * @throws Exception
	 */
	public void updateManualExpress(OrderEntity order) throws Exception;
	
	public List<String> getArrayOrderLogistcs(OrderEntity order) throws Exception;
	
	/**
	 * 保存新增的物流单号（人工新增）
	 * @param orderid
	 * @param logisticsCode
	 * @param logisticsCompany
	 * @throws Exception
	 */
	public void saveManualOrderLogistcs(String orderid, String logisticsCode, String logisticsCompany) throws Exception;
	
	/**
	 * 致物流单号无效
	 * @param orderid
	 * @param logisticsCode
	 * @throws Exception
	 */
	public void inValidExpressLogistcs(String orderid, String logisticsCode) throws Exception;
	
	/**
	 * 启用物流单号作为订单的可用面单号
	 * @param orderid
	 * @param logisticsCode
	 * @throws Exception
	 */
	public void validExpressLogistcs(String orderid, String logisticsCode) throws Exception;
	
	/**
	 * 物流订单号是否存在其它订单中使用
	 * @param orderid
	 * @param logisticsCode
	 * @return
	 * @throws Exception
	 */
	public boolean hasDuplicateLogistcs(String orderid, String logisticsCode) throws Exception;
	

}
