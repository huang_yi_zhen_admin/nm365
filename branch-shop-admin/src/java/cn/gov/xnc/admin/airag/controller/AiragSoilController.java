package cn.gov.xnc.admin.airag.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.airag.entity.AiragSoilEntity;
import cn.gov.xnc.admin.airag.service.AiragSoilServiceI;

/**   
 * @Title: Controller
 * @Description: 大气候土壤数据
 * @author zero
 * @date 2017-06-12 16:37:56
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/airagSoilController")
public class AiragSoilController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AiragSoilController.class);

	@Autowired
	private AiragSoilServiceI airagSoilService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 大气候土壤数据列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView airagSoil(HttpServletRequest request) {
		return new ModelAndView("admin/airag/airagSoilList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(AiragSoilEntity airagSoil,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(AiragSoilEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, airagSoil, request.getParameterMap());
		this.airagSoilService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除大气候土壤数据
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(AiragSoilEntity airagSoil, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		airagSoil = systemService.getEntity(AiragSoilEntity.class, airagSoil.getId());
		message = "大气候土壤数据删除成功";
		airagSoilService.delete(airagSoil);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加大气候土壤数据
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(AiragSoilEntity airagSoil, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(airagSoil.getId())) {
			message = "大气候土壤数据更新成功";
			AiragSoilEntity t = airagSoilService.get(AiragSoilEntity.class, airagSoil.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(airagSoil, t);
				airagSoilService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "大气候土壤数据更新失败";
			}
		} else {
			message = "大气候土壤数据添加成功";
			airagSoilService.save(airagSoil);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 大气候土壤数据列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(AiragSoilEntity airagSoil, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(airagSoil.getId())) {
			airagSoil = airagSoilService.getEntity(AiragSoilEntity.class, airagSoil.getId());
			req.setAttribute("airagSoilPage", airagSoil);
		}
		return new ModelAndView("admin/airag/airagSoil");
	}
}
