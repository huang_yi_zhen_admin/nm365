package cn.gov.xnc.admin.airag.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.airag.entity.AiragPictrueEntity;
import cn.gov.xnc.admin.airag.service.AiragPictrueServiceI;

/**   
 * @Title: Controller
 * @Description: 大气候图片数据
 * @author zero
 * @date 2017-06-12 16:39:32
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/airagPictrueController")
public class AiragPictrueController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AiragPictrueController.class);

	@Autowired
	private AiragPictrueServiceI airagPictrueService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 大气候图片数据列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView airagPictrue(HttpServletRequest request) {
		return new ModelAndView("admin/airag/airagPictrueList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(AiragPictrueEntity airagPictrue,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(AiragPictrueEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, airagPictrue, request.getParameterMap());
		this.airagPictrueService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除大气候图片数据
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(AiragPictrueEntity airagPictrue, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		airagPictrue = systemService.getEntity(AiragPictrueEntity.class, airagPictrue.getId());
		message = "大气候图片数据删除成功";
		airagPictrueService.delete(airagPictrue);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加大气候图片数据
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(AiragPictrueEntity airagPictrue, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(airagPictrue.getId())) {
			message = "大气候图片数据更新成功";
			AiragPictrueEntity t = airagPictrueService.get(AiragPictrueEntity.class, airagPictrue.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(airagPictrue, t);
				airagPictrueService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "大气候图片数据更新失败";
			}
		} else {
			message = "大气候图片数据添加成功";
			airagPictrueService.save(airagPictrue);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 大气候图片数据列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(AiragPictrueEntity airagPictrue, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(airagPictrue.getId())) {
			airagPictrue = airagPictrueService.getEntity(AiragPictrueEntity.class, airagPictrue.getId());
			req.setAttribute("airagPictruePage", airagPictrue);
		}
		return new ModelAndView("admin/airag/airagPictrue");
	}
}
