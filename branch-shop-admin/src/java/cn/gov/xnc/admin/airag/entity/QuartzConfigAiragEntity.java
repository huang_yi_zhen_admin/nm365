package cn.gov.xnc.admin.airag.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 大气候数据自动抓取配置
 * @author zero
 * @date 2017-06-12 16:34:06
 * @version V1.0   
 *
 */
@Entity
@Table(name = "quartz_config_airag", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class QuartzConfigAiragEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**农眼id*/
	private java.lang.String aeid;
	/**接口名称*/
	private java.lang.String interfaceName;
	/**接口地址*/
	private java.lang.String interfaceUrl;
	/**上次调用时间*/
	private java.util.Date lastCallTime;
	/**失败尝试次数*/
	private java.lang.Integer failTryTimes;
	/**获取数据条数*/
	private java.lang.Integer fetchsize;
	/**用户id，调接口时用*/
	private java.lang.String userId;
	/**客户id，获取token时候用*/
	private java.lang.String clientId;
	/**客户密码，获取token时候用*/
	private java.lang.String clientSecret;
	/**授权方式，获取token时候用*/
	private java.lang.String grantType;
	/**用户名称，获取token时候用*/
	private java.lang.String username;
	/**用户密码，获取token时候用*/
	private java.lang.String password;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  农眼id
	 */
	@Column(name ="AEID",nullable=false,length=32)
	public java.lang.String getAeid(){
		return this.aeid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  农眼id
	 */
	public void setAeid(java.lang.String aeid){
		this.aeid = aeid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  接口名称
	 */
	@Column(name ="INTERFACE_NAME",nullable=false,length=255)
	public java.lang.String getInterfaceName(){
		return this.interfaceName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  接口名称
	 */
	public void setInterfaceName(java.lang.String interfaceName){
		this.interfaceName = interfaceName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  接口地址
	 */
	@Column(name ="INTERFACE_URL",nullable=false,length=1024)
	public java.lang.String getInterfaceUrl(){
		return this.interfaceUrl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  接口地址
	 */
	public void setInterfaceUrl(java.lang.String interfaceUrl){
		this.interfaceUrl = interfaceUrl;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  上次调用时间
	 */
	@Column(name ="LAST_CALL_TIME",nullable=true)
	public java.util.Date getLastCallTime(){
		return this.lastCallTime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  上次调用时间
	 */
	public void setLastCallTime(java.util.Date lastCallTime){
		this.lastCallTime = lastCallTime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  失败尝试次数
	 */
	@Column(name ="FAIL_TRY_TIMES",nullable=true,precision=10,scale=0)
	public java.lang.Integer getFailTryTimes(){
		return this.failTryTimes;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  失败尝试次数
	 */
	public void setFailTryTimes(java.lang.Integer failTryTimes){
		this.failTryTimes = failTryTimes;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  获取数据条数
	 */
	@Column(name ="FETCHSIZE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getFetchsize(){
		return this.fetchsize;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  获取数据条数
	 */
	public void setFetchsize(java.lang.Integer fetchsize){
		this.fetchsize = fetchsize;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户id，调接口时用
	 */
	@Column(name ="USER_ID",nullable=true,length=32)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户id，调接口时用
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户id，获取token时候用
	 */
	@Column(name ="CLIENT_ID",nullable=true,length=32)
	public java.lang.String getClientId(){
		return this.clientId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户id，获取token时候用
	 */
	public void setClientId(java.lang.String clientId){
		this.clientId = clientId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户密码，获取token时候用
	 */
	@Column(name ="CLIENT_SECRET",nullable=true,length=32)
	public java.lang.String getClientSecret(){
		return this.clientSecret;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户密码，获取token时候用
	 */
	public void setClientSecret(java.lang.String clientSecret){
		this.clientSecret = clientSecret;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  授权方式，获取token时候用
	 */
	@Column(name ="GRANT_TYPE",nullable=true,length=32)
	public java.lang.String getGrantType(){
		return this.grantType;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  授权方式，获取token时候用
	 */
	public void setGrantType(java.lang.String grantType){
		this.grantType = grantType;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户名称，获取token时候用
	 */
	@Column(name ="USERNAME",nullable=true,length=32)
	public java.lang.String getUsername(){
		return this.username;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户名称，获取token时候用
	 */
	public void setUsername(java.lang.String username){
		this.username = username;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户密码，获取token时候用
	 */
	@Column(name ="PASSWORD",nullable=true,length=32)
	public java.lang.String getPassword(){
		return this.password;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户密码，获取token时候用
	 */
	public void setPassword(java.lang.String password){
		this.password = password;
	}
}
