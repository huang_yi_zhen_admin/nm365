package cn.gov.xnc.admin.advertising.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.advertising.service.AdvertisingPositionServiceI;
import cn.gov.xnc.admin.advertising.service.AdvertisingType;
import cn.gov.xnc.admin.basicSetting.vo.UtilTypeVO;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("advertisingPositionService")

public class AdvertisingPositionServiceImpl extends CommonServiceImpl implements AdvertisingPositionServiceI {
	
	private static final List<UtilTypeVO> adPositionTypeList = new ArrayList<UtilTypeVO>();
	
	static{
		for (AdvertisingType ad : AdvertisingType.values()) {
			
			UtilTypeVO type = new UtilTypeVO();
			type.setId(ad.getValue());
			type.setName(ad.getName());
			
			adPositionTypeList.add(type);
		}
	}

	@Override
	public List<UtilTypeVO> getAdvertisingPositionType() {

		return adPositionTypeList;
	}
	
}