package cn.gov.xnc.admin.advertising.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.advertising.entity.AdvertisingPositionEntity;
import cn.gov.xnc.admin.advertising.service.AdvertisingPositionServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;

/**   
 * @Title: Controller
 * @Description: 广告位管理
 * @author zero
 * @date 2016-10-01 02:26:45
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/advertisingPositionController")
public class AdvertisingPositionController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AdvertisingPositionController.class);

	@Autowired
	private AdvertisingPositionServiceI advertisingPositionService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 广告位管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value  = "list")
	public ModelAndView advertisingPosition(AdvertisingPositionEntity advertisingPosition,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		
		CriteriaQuery cq = new CriteriaQuery(AdvertisingPositionEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, advertisingPosition, request.getParameterMap());
		this.advertisingPositionService.getDataGridReturn(cq, false);
		//TagUtil.datagrid(response, dataGrid);
		
		List<AdvertisingPositionEntity> advertisingPositionList = dataGrid.getResults();
		
		request.setAttribute("advertisingPositionList", advertisingPositionList);
		return new ModelAndView("admin/advertising/advertisingPositionList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(AdvertisingPositionEntity advertisingPosition,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(AdvertisingPositionEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, advertisingPosition, request.getParameterMap());
		this.advertisingPositionService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除广告位管理
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(AdvertisingPositionEntity advertisingPosition, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		advertisingPosition = systemService.getEntity(AdvertisingPositionEntity.class, advertisingPosition.getId());
		message = "广告位管理删除成功";
		advertisingPositionService.delete(advertisingPosition);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加广告位管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(AdvertisingPositionEntity advertisingPosition, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(advertisingPosition.getId())) {
			message = "广告位管理更新成功";
			AdvertisingPositionEntity t = advertisingPositionService.get(AdvertisingPositionEntity.class, advertisingPosition.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(advertisingPosition, t);
				advertisingPositionService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "广告位管理更新失败";
			}
		} else {
			message = "广告位管理添加成功";
			advertisingPositionService.save(advertisingPosition);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 广告位管理列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(AdvertisingPositionEntity advertisingPosition, HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(advertisingPosition.getId())) {
			advertisingPosition = advertisingPositionService.getEntity(AdvertisingPositionEntity.class, advertisingPosition.getId());
			try {
				if (null == advertisingPosition.getProduct() || StringUtil.isEmpty(advertisingPosition.getProduct().getId())) {
					advertisingPosition.setProduct(new ProductEntity());
				}
			} catch (Exception e) {
				advertisingPosition.setProduct(new ProductEntity());
			}
			req.setAttribute("advertisingPositionPage", advertisingPosition);
		}else {
			advertisingPosition = new AdvertisingPositionEntity();
			ProductEntity product = new ProductEntity();
			advertisingPosition.setProduct(product);		
			req.setAttribute("advertisingPositionPage", advertisingPosition);
		}
		
		req.setAttribute("adTypeList", advertisingPositionService.getAdvertisingPositionType());
		
		return new ModelAndView("admin/advertising/advertisingPosition");
	}
}
