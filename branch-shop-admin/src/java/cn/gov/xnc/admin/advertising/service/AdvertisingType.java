package cn.gov.xnc.admin.advertising.service;

public enum AdvertisingType {

		ROLL_BANNER("轮播图", "1"), 
		TOP_COLUMN_BANNER("顶部横栏", "2"), 
		BOTTOM_COLUMN_BANNER("底部横栏","3");

		private String name;
		private String value;

		private AdvertisingType(String name, String value) {
			this.name = name;
			this.value = value;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String index) {
			this.value = index;
		}

}
