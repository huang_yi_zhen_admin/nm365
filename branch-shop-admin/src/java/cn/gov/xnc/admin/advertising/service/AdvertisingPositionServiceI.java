package cn.gov.xnc.admin.advertising.service;

import java.util.List;

import cn.gov.xnc.admin.basicSetting.vo.UtilTypeVO;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface AdvertisingPositionServiceI extends CommonService{

	public List<UtilTypeVO> getAdvertisingPositionType();
}
