package cn.gov.xnc.admin.kuaidi.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ExpressSearchUtil;
import cn.gov.xnc.system.core.util.JsonUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

public class KuaidiDAO {
	@Autowired
	private static KuaidiDAO instance = null;
	private String field = "oLogistics.orderId, oLogistics.logisticscompany, oLogistics.logisticsnumber, oLogistics.state, o.identifierOr, o.clientId, o.yewu, o.productName, o.Number, o.CustomerName, o.Telephone, o.address, o.DepartureDate, dictionary.dictionaryName";
	/**
	 * 返回实例
	 * */
	public static KuaidiDAO getInstance(){
		if(instance == null){
			synchronized(KuaidiDAO.class){
				if(instance == null){
					instance = new KuaidiDAO();
				}
			}
		}
		return instance;
	}
	/**
	 * 物流查询
	 * */
	public List<Map<String,Object>> getLogisticsInquiriesList(Map<String, Object> map, DataGrid dataGrid, SystemService systemService){
		TSUser user = ResourceUtil.getSessionUserName();
		StringBuilder sql = new StringBuilder();
		List<Map<String, Object>> mapList = null;
		List<Map<String, Object>> list = null;
		
 		long count = 0;
		String clientidRealname = "";
		String yewuRealname = "";
		Integer startNum = (dataGrid.getPageNum()-1) * dataGrid.getShowNum();
		Integer yewui = 0;
		Integer clienti = 0;
		sql.append("select");
		sql.append(" field");
		sql.append(" from");
		sql.append(" xnc_order_Logistics as oLogistics");
		sql.append(" inner join");
		sql.append(" xnc_order as o");
		sql.append(" on oLogistics.orderId=o.id");
		sql.append(" inner join");
		sql.append(" t_s_company company");
		sql.append(" on o.company=company.id");
		sql.append(" inner join");
		sql.append(" t_s_dictionary dictionary");
		sql.append(" on o.freightState=dictionary.id");
		sql.append(" where");
		/*sql.append(" o.state=3");*/
		/*sql.append(" and");*/
		sql.append(" company.id='" + user.getCompany().getId() + "'");
		if(map.get("identifierOr") != null && !map.get("identifierOr").equals("")){
			//sql.append(" and o.identifierOr='" + map.get("identifierOr") + "'");
			sql.append(" and o.identifierOr like '%" + map.get("identifierOr") + "%'");
		}
		if(map.get("CustomerName") != null && !map.get("CustomerName").equals("")){
			sql.append(" and o.CustomerName like '%" + map.get("CustomerName") + "%'");
		}
		if(map.get("Telephone") != null && !map.get("Telephone").equals("")){
			//sql.append(" and o.Telephone='" + map.get("Telephone") + "'");
			sql.append(" and o.Telephone like '%" + map.get("Telephone") + "%'");
		}
		if(map.get("productName") != null && !map.get("productName").equals("")){
			//sql.append(" and o.productName='" + map.get("productName") + "'");
			sql.append(" and o.productName like '%" + map.get("productName") + "%'");
		}
		if(map.get("address") != null && !map.get("address").equals("")){
			sql.append(" and o.address like '%" + map.get("address") + "%'");
		}
		if(map.get("logisticsCompany") != null && !map.get("logisticsCompany").equals("")){
			//sql.append(" and oLogistics.logisticsCompany='" + map.get("logisticsCompany") + "'");
			sql.append(" and oLogistics.logisticsCompany like '%" + map.get("logisticsCompany") + "%'");
		}
		if(map.get("logisticsNumber") != null && !map.get("logisticsNumber").equals("")){
			//sql.append(" and oLogistics.logisticsNumber='" + map.get("logisticsNumber") + "'");
			sql.append(" and oLogistics.logisticsNumber like '%" + map.get("logisticsNumber") + "%'");
		}
		if(map.get("state") != null && !map.get("state").equals("")){
			sql.append(" and oLogistics.state=" + map.get("state") + "");
		}
		if(map.get("freightState") != null && !map.get("freightState").equals("")){
			sql.append(" and dictionary.dictionaryValue=" + map.get("freightState") + "");
		}
		if(map.get("clientId") != null && !map.get("clientId").equals("")){
			/*map = systemService.findOneForJdbc("select id from t_s_user where realname='"+map.get("clientId")+"'", null);
			if(map != null){
				sql.append(" and o.clientId='" + map.get("id") + "'");
			}*/
			list = systemService.findForJdbc("select id from t_s_user where realname='"+map.get("clientId")+"'");
			if(list != null && list.size()>0){
				sql.append(" and o.clientId in ");
				sql.append("(");
				for(Map<String,Object> yewuMap:list){
					if(clienti>0){
						sql.append(",");
					}
					sql.append("'"+yewuMap.get("id")+"'");
					clienti++;
				}
				sql.append(")");
			}
		}
		if(map.get("yewu") != null && !map.get("yewu").equals("")){
			list = systemService.findForJdbc("select id from t_s_user where realname='"+map.get("yewu")+"'");
			if(list != null && list.size()>0){
				sql.append(" and o.yewu in ");
				sql.append("(");
				for(Map<String,Object> yewuMap:list){
					if(yewui>0){
						sql.append(",");
					}
					sql.append("'"+yewuMap.get("id")+"'");
					yewui++;
				}
				sql.append(")");
			}
		}
		if(map.get("eqDepartureDate") != null && !map.get("eqDepartureDate").equals("")){
			sql.append(" and o.DepartureDate='" + map.get("eqDepartureDate") + "'");
		}
		if(map.get("gtDepartureDate") != null && !map.get("gtDepartureDate").equals("")){
			sql.append(" and o.DepartureDate>='" + map.get("gtDepartureDate") + "'");
		}
		if(map.get("ltDepartureDate") != null && !map.get("ltDepartureDate").equals("")){
			sql.append(" and o.DepartureDate<='" + map.get("ltDepartureDate") + "'");
		}
		count = systemService.getCountForJdbc(sql.toString().replace("field", "count(1)"));
		sql.append(" order by");
		sql.append(" oLogistics.logisticsdate desc");
		sql.append(" limit " + startNum + " , " + dataGrid.getShowNum());
		System.out.println("#"+sql.toString().replace("field", field));
		mapList = systemService.findForJdbc(sql.toString().replace("field", field));
		map.clear();
		for(Map<String, Object> m : mapList){
			
			clientidRealname = "";
			yewuRealname = "";
			if(m.get("clientId") != null){
				map = systemService.findOneForJdbc("select realname from t_s_user where id='"+m.get("clientId")+"'", null);
				if(map != null){
					clientidRealname = map.get("realname")+"";
				}
			}
			if(m.get("yewu") != null){
				map = systemService.findOneForJdbc("select realname from t_s_user where id='"+m.get("yewu")+"'", null);
				if(map != null){
					yewuRealname = map.get("realname")+"";
				}
			}
			m.put("clientId", clientidRealname);
			m.put("yewu", yewuRealname);
		}
		dataGrid.setTotal(Integer.parseInt(count+""));
		return mapList;
	}
	/**
	 * 物流信息
	 **/
	public List<Map<String, Object>> getLogistics(String orderId, String logisticscompany, String logisticsnumber, SystemService systemService) throws Exception{
		List<Map<String, Object>> mapList = null;
		Map<String, Object> map = null;
		String date = null;
		String logisticsresp = "";
		JSONObject jSONObject = new JSONObject();
		String sql = "select message,state from xnc_order_logistics where orderId='"+orderId+"'";
		System.out.println("#####sql:"+sql);
		map = systemService.findOneForJdbc(sql, null);
		if(map != null){
			if(map.get("state").toString().equals("3")){
				System.out.println("#############################3");
				logisticsresp = map.get("message")+"";
				jSONObject = jSONObject.parseObject(logisticsresp);
				mapList = JsonUtil.toBean(jSONObject.getJSONArray("Traces")+"", ArrayList.class);
			}else{
				
				logisticsresp = ExpressSearchUtil.getOrderTracesByCompany(logisticscompany, logisticsnumber);
				jSONObject = jSONObject.parseObject(logisticsresp);
				if(jSONObject.getJSONArray("Traces") != null && jSONObject.getJSONArray("Traces").size()>0){
					mapList = JsonUtil.toBean(jSONObject.getJSONArray("Traces")+"", ArrayList.class);
					for(Map<String, Object> m : mapList){
						if(m.get("AcceptTime") != null){
							date = m.get("AcceptTime")+"";
						}
					}
				}
				sql = "update xnc_order_logistics set message='"+logisticsresp+"',state='"+jSONObject.getString("State")+"',logisticsdate="+date+" where orderId='"+orderId+"'";
				System.out.println(sql);
				systemService.updateBySqlString(sql);
			}
		}
		System.out.println("logisticsresp:"+logisticsresp);
		return mapList;
	}
}
