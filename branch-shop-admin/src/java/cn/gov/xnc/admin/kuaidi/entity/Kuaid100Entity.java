package cn.gov.xnc.admin.kuaidi.entity;

import java.util.HashMap;
import java.util.Map;

public class Kuaid100Entity {
	
	
	private java.lang.String message;
	private java.lang.String nu;
	private java.lang.String ischeck;
	private java.lang.String condition;
	private java.lang.String com;
	private java.lang.String status;
	private java.lang.String state;
	private Object data;
	private java.lang.String location;
	
	
    /**
	 * @return the message
	 */
	public java.lang.String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(java.lang.String message) {
		this.message = message;
	}



	/**
	 * @return the nu
	 */
	public java.lang.String getNu() {
		return nu;
	}



	/**
	 * @param nu the nu to set
	 */
	public void setNu(java.lang.String nu) {
		this.nu = nu;
	}



	/**
	 * @return the ischeck
	 */
	public java.lang.String getIscheck() {
		return ischeck;
	}



	/**
	 * @param ischeck the ischeck to set
	 */
	public void setIscheck(java.lang.String ischeck) {
		this.ischeck = ischeck;
	}



	/**
	 * @return the condition
	 */
	public java.lang.String getCondition() {
		return condition;
	}



	/**
	 * @param condition the condition to set
	 */
	public void setCondition(java.lang.String condition) {
		this.condition = condition;
	}



	/**
	 * @return the com
	 */
	public java.lang.String getCom() {
		return com;
	}



	/**
	 * @param com the com to set
	 */
	public void setCom(java.lang.String com) {
		this.com = com;
	}



	/**
	 * @return the status
	 */
	public java.lang.String getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(java.lang.String status) {
		this.status = status;
	}



	/**
	 * @return the state
	 */
	public java.lang.String getState() {
		return state;
	}



	/**
	 * @param state the state to set
	 */
	public void setState(java.lang.String state) {
		this.state = state;
	}



	



	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/** 
     *  
     *  
     * 快递100的查询代码封装
     * @param <T> 
     * @param map   
     * @param class1 
     * @return 
     */  
    public  Map<String, String> kuaidi100ToKeyMap () {
    	Map<String, String> map = new HashMap<String, String>();
    	//A
    	//B
    	//C
    	//D
    	map.put("德邦物流", "debangwuliu");
    	//E
    	map.put("EMS", "ems");
    	map.put("E邮宝", "ems");
    	//F
    	//G
    	map.put("国通", "guotongkuaidi");
    	//H   	
    	map.put("汇通", "huitongkuaidi");
    	//S
    	map.put("申通", "shentong");
    	map.put("顺丰", "shunfeng");
    	//T
    	map.put("天天", "tiantian");
    	//Y
    	map.put("圆通", "yuantong");
    	map.put("韵达", "yunda");
    	map.put("邮政", "youzhengguonei");
    	//Z
    	map.put("中通", "zhongtong");
    	map.put("宅急", "zhaijisong");
    	
    	
    	
    	
		return map;  

    }
	
	
}
