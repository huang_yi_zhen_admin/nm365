package cn.gov.xnc.admin.message.dao;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.message.entity.SmsSendEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
@Service("smsSendDAO")
public class SmsSendDAO {
	@Resource(name="commonService")
	private CommonService commonService;
	private static final Logger logger = Logger.getLogger(SmsSendDAO.class);
	private final String TABLE = "xnc_sms_send";
	private final String FIELD = "id, type, content, company_id ,send_mobile ,receive_mobile , number, createDate";
	private SmsSendDAO(){}
	
	/**
	 * 查询，单条数据
	 * */
	public SmsSendEntity getSmsSendById(SmsSendEntity smsSend){
		SmsSendEntity smsSendEntity = commonService.findUniqueByProperty(SmsSendEntity.class, "id", smsSend.getId());
		return smsSendEntity;
	}
	
	/**
	 * 保存
	 * */
	public Serializable smsSendSave(SmsSendEntity smsSend){
		Serializable id = "";
		try{
			smsSend.setCreateDate(DateUtils.gettimestamp());
			id = commonService.save(smsSend);
		}catch(Exception e){
			logger.error("保存异常：" + e);
		}
		return id;
	}
	
	/**
	 * 更新
	 * */
	public Integer smsSendUpdateType(SmsSendEntity smsSend){
		Integer result = 0 ;
		String sql = "update " + TABLE + " set type='" + smsSend.getType() + "' where company_id='" + smsSend.getCompany_id() + "' and type='-1'";
		System.out.println("#"+sql);
		result = commonService.updateBySqlString(sql);
		return result;
	}
	
	/**
	 * 查询，单条数据
	 * */
	public SmsSendEntity getSmsSendData(SmsSendEntity smsSend){
		StringBuilder sql = new StringBuilder();
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String ,Object> where = null;
		where = MyBeanUtils.beanToMapNew(smsSend);
		sql.append("select " + FIELD);
		sql.append(" from " + TABLE);
		sql.append(" where 1=1");
		for(String key : where.keySet()){
			if(where.get(key) != null){
				sql.append(" and " + key + "='" + where.get(key).toString().trim() + "'");				
			}
		}
		sql.append(" order by createDate desc limit 1");
		System.out.println("#:"+sql);
		map = commonService.findOneForJdbc(sql.toString(), null);
		smsSend = new SmsSendEntity();
		if(map != null){
			smsSend.setId(map.get("id") == null? null : map.get("id")+"");
			smsSend.setCompany_id(map.get("company_id") == null? null : map.get("company_id")+"");
			smsSend.setContent(map.get("content") == null? null : map.get("content")+"");
			smsSend.setNumber(map.get("number") == null ? null : Integer.parseInt(map.get("number")+""));
			smsSend.setReceive_mobile(map.get("receive_mobile") == null ? null : map.get("receive_mobile") + "");
			smsSend.setType(map.get("type") == null ? null : map.get("type") + "");
		}
		return smsSend;
	}
	
	
	/**
	 * 列表
	 * */
	public DataGrid getSmsSendListPage(DataGrid dataGrid, SmsSendEntity smsSend){
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String ,Object> where = null;
		String field = dataGrid.getField() == null ? FIELD : dataGrid.getField();
		StringBuilder sql = new StringBuilder();
		long count = 0;
		sql.append("select field");
		sql.append(" from " + TABLE);
		sql.append(" where 1=1");
		sql.append(" and company_id='" + user.getCompany().getId() + "'");
		where = MyBeanUtils.beanToMapNew(smsSend);
		for(String key : where.keySet()){
			if(where.get(key) != null){
				sql.append(" and " + key + "='" + where.get(key).toString().trim() + "'");				
			}
		}
		count = commonService.getCountForJdbc(sql.toString().replace("field", "count(1)"));
		if(dataGrid.getSort()!=null){
			sql.append(" order by " + dataGrid.getSort() + " desc");
		}
		System.out.println("#######:"+sql.toString().replace("field", field));
		List<Map<String, Object>> mapList = commonService.findForJdbc(sql.toString().replace("field", field),dataGrid.getPageNum(),dataGrid.getShowNum());
		dataGrid.setTotal((int) count);
		dataGrid.setResults(mapList);
		return dataGrid;
	}
}
