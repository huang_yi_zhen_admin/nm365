package cn.gov.xnc.admin.message.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.esms.MessageData;
import com.esms.PostMsg;
import com.esms.common.entity.Account;
import com.esms.common.entity.GsmsResponse;
import com.esms.common.entity.MTPack;

import cn.gov.xnc.admin.message.entity.MessageTemplateEntity;
import cn.gov.xnc.admin.message.entity.NotifyEntity;
import cn.gov.xnc.admin.message.entity.NotifyTextEntity;
import cn.gov.xnc.admin.message.entity.SmsSendEntity;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyReviewedEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.admin.message.service.impl.SmsSendServiceImpl;
import cn.gov.xnc.admin.message.service.impl.NotifyServiceImpl;


@Service("messageTemplateService")

public class MessageTemplateServiceImpl extends CommonServiceImpl implements MessageTemplateServiceI {
	private static Logger logger = Logger.getLogger(MessageTemplateServiceImpl.class);
	
	private final String mosaccount = "ttnm@hnxnc";
	//发货模板id
	private final String mospswd = "1H8BHWp6";

	@Resource(name="smsSendService")
	private SmsSendServiceImpl smsSendService;
	@Resource(name="NotifyService")
	private NotifyServiceImpl notifyService;

	/**
	 * account 短信账号
	 * pswd  短信发送密码
	 * mobile 接收手机号
	 * msgtitle  短信标题
	 * msg   内容
	 * 
	 */
	public void sendSMSTTMM(String account, String pswd, String mobile, String msgtitle, String msg){
		
		//设置短信发送的账号和密码
		Account ac = new Account(mosaccount, mospswd);
		PostMsg pm = new PostMsg();
		//配置网关，需要跟短信看第三方获取
//		pm.getCmHost().setHost(ResourceUtil.getCmHost(), Integer.parseInt(ResourceUtil.getCmHostPort()));//设置网关的IP和port，用于发送信息
//		pm.getWsHost().setHost(ResourceUtil.getWsHost(), Integer.parseInt(ResourceUtil.getWsHostPort()));//设置网关的 IP和port，用于获取账号信息、上行、状态报告等等

		pm.getCmHost().setHost("211.147.239.62",9080);//设置网关的IP和port，用于发送信息
		pm.getWsHost().setHost("211.147.239.62",9070);//设置网关的 IP和port，用于获取账号信息、上行、状态报告等等
		
		
		try {
			doSendSms(pm, ac, mobile, msgtitle, msg);
		} catch (Exception e) {
			
		}
	}
	
	
	/**
	 * @param String mosaccount 短信账号
	 * @param String mospswd  短信发送密码
	 * @param String mobile 接收手机号
	 * @param String msgtitle  短信标题
	 * @param String msg   内容
	 * 
	 */
	private void sendSMS(String account, String pswd, String mobile, String msgtitle, String msg) throws Exception {
		
		//设置短信发送的账号和密码
		Account ac = new Account(mosaccount, mospswd);
		PostMsg pm = new PostMsg();
		//配置网关，需要跟短信看第三方获取
		pm.getCmHost().setHost("211.147.239.62",9080);//设置网关的IP和port，用于发送信息
		pm.getWsHost().setHost("211.147.239.62",9070);//设置网关的 IP和port，用于获取账号信息、上行、状态报告等等
		
		
		doSendSms(pm, ac, mobile, msgtitle, msg);
	}
	
	/**
	 * 短信下发范例
	 * @param pm
	 * @param ac
	 */
	private static void doSendSms(PostMsg pm, Account ac, String toUser, String msgtitle, String content) throws Exception{
		MTPack pack = new MTPack();
		pack.setBatchID(UUID.randomUUID());
		pack.setBatchName(msgtitle);
		pack.setMsgType(MTPack.MsgType.SMS);
		pack.setSendType(MTPack.SendType.MASS);
		pack.setBizType(0);
		pack.setDistinctFlag(false);
		ArrayList<MessageData> msgs = new ArrayList<MessageData>();
		
//		/** 单发，一号码一内容 */
		msgs.add(new MessageData(toUser, content));
		pack.setMsgs(msgs);
		
		/** 群发，多号码一内容 */
//		String content = "短信群发测试";
//		msgs.add(new MessageData(toUser, content));
//		msgs.add(new MessageData("13430258222", content));
//		msgs.add(new MessageData("13430258333", content));
//		pack.setMsgs(msgs);
		
//		/** 组发，多号码多内容 */
//		pack.setSendType(SendType.GROUP);
//		msgs.add(new MessageData("13430258111", "短信组发测试111"));
//		msgs.add(new MessageData("13430258222", "短信组发测试222"));
//		msgs.add(new MessageData("13430258333", "短信组发测试333"));
//		pack.setMsgs(msgs);
		/** 设置模板编号(静态模板将以模板内容发送; 动态模板需要发送变量值，JSON格式：[{"key":"变量名1","value":"变量值1"},{"key":"变量名2","value":"变量值2"}]) */
		//pack.setTemplateNo("test");
		GsmsResponse resp = pm.post(ac, pack);
		//systemService.addLog(resp.getMessage(), Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		logger.error(resp.getMessage());
		System.out.println("短信发送："+resp.getMessage());
		
		
	}
	
	/**
	 * 开通客户短信通知
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_1(TSUser u ,String passwordRandCode) {
		try {
			if( u != null && StringUtil.isNotEmpty(u.getId())){
				TSUser user = getEntity(TSUser.class, u.getId());
				TSCompany company = getEntity(TSCompany.class, user.getCompany().getId() );
				if( "3".equals(user.getType()) ){
					MessageTemplateEntity messageTemplate = messageTemplate("1",user.getCompany());
					if( messageTemplate != null &&  company.getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", company.getCompanyName());
							   smscontent  =smscontent.replaceAll("【账号名】", user.getUserName());
							   smscontent  =smscontent.replaceAll("【密码】", passwordRandCode);
							   smscontent  =smscontent.replaceAll("【登录地址】", company.getCompanyplatform().getWebsite());
							   smscontent  =smscontent.replaceAll("【供货方联系电话】", company.getServicephone());
							   sendSMS("", "", user.getUserName(), "", smscontent);
							   if(user.getUserName()!=null && !user.getUserName().equals("")){
								 //添加短信发送记录
								   smsSendService.smsSendSave(messageTemplate.getType(), smscontent.replace(passwordRandCode, ""), "", user.getUserName(), company.getId());
							   }
						addSmsnumber(messageTemplate, company.getCompanyreviewed());
					}
				}
				this.checkMsgnumberTotal(company.getCompanyreviewed().getMsgnumberTotal(), company);
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	
	
	
	/**
	 * 
	 * 付款待发货通知订单员
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_2(PayBillsOrderEntity payBillsOrder ) {
		try {
			if( payBillsOrder != null && StringUtil.isNotEmpty(payBillsOrder.getId())){
				payBillsOrder = getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
				
				TSCompany company = getEntity(TSCompany.class, payBillsOrder.getCompany().getId() );
				
				if( "2".equals(payBillsOrder.getState())  ){
					MessageTemplateEntity messageTemplate = messageTemplate("2",company);
					if( messageTemplate != null &&  company.getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", company.getCompanyName());
							   smscontent  =smscontent.replaceAll("【采购方名称】", payBillsOrder.getClientid().getRealname());
							   smscontent  =smscontent.replaceAll("【订单号】", payBillsOrder.getIdentifier());
							   //存在指定发送则先获取发送数据
							   if( messageTemplate != null && StringUtil.isNotEmpty(messageTemplate.getDespatcher()) ){
								   addSmsMobile(smscontent, messageTemplate , company);
							   }else{//存在关联业务员则发给对应的业务员
								   //对于关联业务员
								   TSUser  yewu = payBillsOrder.getYewu();
								   addSmsUser(yewu,smscontent,messageTemplate,company) ;
							   }
							   
					}
				}
				this.checkMsgnumberTotal(company.getCompanyreviewed().getMsgnumberTotal(), company);
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 * 付款待发货通知业务员
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_3(PayBillsOrderEntity payBillsOrder ) {
		try {
			if( payBillsOrder != null && StringUtil.isNotEmpty(payBillsOrder.getId())){
				payBillsOrder = getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
				
				TSCompany company = getEntity(TSCompany.class, payBillsOrder.getCompany().getId() );
				
				if( "2".equals(payBillsOrder.getState()) ){
					MessageTemplateEntity messageTemplate = messageTemplate("3",payBillsOrder.getCompany());
					if( messageTemplate != null &&  company.getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", company.getCompanyName());
							   smscontent  =smscontent.replaceAll("【业务员名称】", payBillsOrder.getYewu().getRealname());
							   smscontent  =smscontent.replaceAll("【采购客户名称】", payBillsOrder.getClientid().getRealname());
							   smscontent  =smscontent.replaceAll("【订单号】", payBillsOrder.getIdentifier());
							   //存在指定发送则先获取发送数据
							   if( messageTemplate != null && StringUtil.isNotEmpty(messageTemplate.getDespatcher()) ){
								   addSmsMobile(smscontent, messageTemplate ,company  );
							   }else{
								   try {
									   if(null != payBillsOrder.getYewu() && StringUtil.isNotEmpty(payBillsOrder.getYewu().getId()) ){
										   sendSMS("", "", payBillsOrder.getYewu().getMobilephone(), "", smscontent);
										   if(payBillsOrder.getYewu().getMobilephone()!=null && !payBillsOrder.getYewu().getMobilephone().equals("")){
											 //添加短信发送记录
											   smsSendService.smsSendSave(messageTemplate.getType(), smscontent, "", payBillsOrder.getYewu().getMobilephone(), company.getId());
										   }
											addSmsnumber(messageTemplate, company.getCompanyreviewed());
											
										   
									   }
									   /*else{
										   addSmsRolecode("yewuyuan", smscontent, messageTemplate ,company) ;
									   }*/
									   
									} catch (Exception e) {
										logger.error(e);
									}
							   }
							   
					}
				}
				this.checkMsgnumberTotal(company.getCompanyreviewed().getMsgnumberTotal(), company);
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	
	
	
	/**
	 * 
	 * 现货后款通过通知客户
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_4(PayBillsOrderEntity payBillsOrder ) {
		try {
			if( payBillsOrder != null && StringUtil.isNotEmpty(payBillsOrder.getId())){
				payBillsOrder = getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
				
				if( "2".equals(payBillsOrder.getState())  ){
					MessageTemplateEntity messageTemplate = messageTemplate("4",payBillsOrder.getCompany());
					if( messageTemplate != null &&  payBillsOrder.getCompany().getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", payBillsOrder.getCompany().getCompanyName());
							   smscontent  =smscontent.replaceAll("【客户名称】", payBillsOrder.getClientid().getRealname());
							   smscontent  =smscontent.replaceAll("【订单号】", payBillsOrder.getIdentifier());							   
							   sendSMS("", "", payBillsOrder.getClientid().getMobilephone(), "", smscontent);
							   if(payBillsOrder.getClientid().getMobilephone()!=null && !payBillsOrder.getClientid().getMobilephone().equals("")){
								 //添加短信发送记录
								   smsSendService.smsSendSave(messageTemplate.getType(), smscontent, "", payBillsOrder.getClientid().getMobilephone(), payBillsOrder.getCompany().getId());
							   }
							 
							   addSmsnumber(messageTemplate, payBillsOrder.getCompany().getCompanyreviewed());
								   
					}
				}
				this.checkMsgnumberTotal(payBillsOrder.getCompany().getCompanyreviewed().getMsgnumberTotal(), payBillsOrder.getCompany());
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	
	
	
	
	/**
	 * 
	 * 发货通知收货人
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_5(OrderEntity orderEntity ) {
		try {
			if( orderEntity != null && StringUtil.isNotEmpty(orderEntity.getId())){
				orderEntity = getEntity(OrderEntity.class, orderEntity.getId());
				
				if( "3".equals(orderEntity.getState())  ){
					MessageTemplateEntity messageTemplate = messageTemplate("5",orderEntity.getCompany());
					if( messageTemplate != null &&  orderEntity.getCompany().getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", orderEntity.getCompany().getCompanyName());
							   smscontent  =smscontent.replaceAll("【快递公司】", orderEntity.getLogisticscompany());
							   smscontent  =smscontent.replaceAll("【快递单号】", orderEntity.getLogisticsnumber());
							sendSMS("", "", orderEntity.getTelephone(), "", smscontent);
							 if(orderEntity.getTelephone()!=null && !orderEntity.getTelephone().equals("")){
								//添加短信发送记录
								smsSendService.smsSendSave(messageTemplate.getType(), smscontent, "", orderEntity.getTelephone(), orderEntity.getCompany().getId());
							 }
							
							addSmsnumber(messageTemplate, orderEntity.getCompany().getCompanyreviewed());
							
					}
				}
				this.checkMsgnumberTotal(orderEntity.getCompany().getCompanyreviewed().getMsgnumberTotal(), orderEntity.getCompany());
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 * 发货通知下单客户
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_6(OrderEntity orderEntity ) {
		try {
			if( orderEntity != null && StringUtil.isNotEmpty(orderEntity.getId())){
				orderEntity = getEntity(OrderEntity.class, orderEntity.getId());
				
				if( "3".equals(orderEntity.getState())  ){
					MessageTemplateEntity messageTemplate = messageTemplate("6",orderEntity.getCompany());
					if( messageTemplate != null &&  orderEntity.getCompany().getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", orderEntity.getCompany().getCompanyName());
							   smscontent  =smscontent.replaceAll("【快递公司】", orderEntity.getLogisticscompany());
							   smscontent  =smscontent.replaceAll("【订单号】", orderEntity.getLogisticsnumber());
							sendSMS("", "", orderEntity.getBillsorderid().getClientid().getMobilephone(), "", smscontent);
							if(orderEntity.getBillsorderid().getClientid().getMobilephone()!=null && !orderEntity.getBillsorderid().getClientid().getMobilephone().equals("")){
								//添加短信发送记录
								smsSendService.smsSendSave(messageTemplate.getType(), smscontent, "", orderEntity.getBillsorderid().getClientid().getMobilephone(), orderEntity.getCompany().getId());
							}
							
							addSmsnumber(messageTemplate, orderEntity.getCompany().getCompanyreviewed());
							
							 //存在指定发送则先获取发送数据
							
					}
				}
				this.checkMsgnumberTotal(orderEntity.getCompany().getCompanyreviewed().getMsgnumberTotal(), orderEntity.getCompany());
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	
	
	/**
	 * 
	 * 退单申请通知财务处理
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_7(OrderEntity order ) {
		try {
			if( order != null && StringUtil.isNotEmpty(order.getId())){
				OrderEntity o = getEntity(OrderEntity.class, order.getId());
				TSCompany company = getEntity(TSCompany.class, o.getCompany().getId() );
				
				if( "6".equals(o.getState())  ){
					MessageTemplateEntity messageTemplate = messageTemplate("7", o.getCompany());
					if( messageTemplate != null &&  company.getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", company.getCompanyName());
							   smscontent  =smscontent.replaceAll("【采购方名称】", o.getBillsorderid().getClientid().getRealname());
							   smscontent  =smscontent.replaceAll("【订单号】", o.getIdentifieror());
							   //存在指定发送则先获取发送数据
							   if( messageTemplate != null && StringUtil.isNotEmpty(messageTemplate.getDespatcher()) ){
								   addSmsMobile(smscontent, messageTemplate ,company  );
							   }else{
								   addSmsRolecode("caiwuyuan", smscontent, messageTemplate ,company) ;
							   }
						
					}
				}
				this.checkMsgnumberTotal(company.getCompanyreviewed().getMsgnumberTotal(), company);
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * 退款通知客户
	 * 
	 * */
	
	public boolean mosSendSmsDelivery_8( OrderEntity order  ) {
		try {
			if( order != null && StringUtil.isNotEmpty(order.getId())){
				OrderEntity o = getEntity(OrderEntity.class, order.getId());
				TSCompany company = getEntity(TSCompany.class, o.getCompany().getId() );
				
				if( "6".equals(o.getState())  ){
					MessageTemplateEntity messageTemplate = messageTemplate("8", o.getCompany());
					if( messageTemplate != null &&  company.getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", company.getCompanyName());
							   smscontent  =smscontent.replaceAll("【采购方名称】", o.getBillsorderid().getClientid().getRealname());
							   smscontent  =smscontent.replaceAll("【订单号】", o.getIdentifieror());
							   
							   
							   //存在指定发送则先获取发送数据
							   if( messageTemplate != null && StringUtil.isNotEmpty(messageTemplate.getDespatcher()) ){
								   addSmsMobile(smscontent, messageTemplate ,company  );
							   }else{
								   addSmsRolecode("caiwuyuan", smscontent, messageTemplate ,company) ;
							   }
								   
					}
				}
				this.checkMsgnumberTotal(company.getCompanyreviewed().getMsgnumberTotal(), company);
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	
	
	/**
	 * 
	 * 获取模板结构
	 * @param tmpid  消息类型
	 * @param tmpid  消息类型
	 * @return
	 * @throws Exception
	 * 
	 */
	private void   addSmsnumber   ( MessageTemplateEntity messageTemplate , CompanyReviewedEntity companyReviewed ) throws Exception{
		
		 	messageTemplate.setSmsnumber(messageTemplate.getSmsnumber()+1);
		 	companyReviewed.setMsgnumber(companyReviewed.getMsgnumber()+1);
		 	companyReviewed.setMsgnumberTotal(companyReviewed.getMsgnumberTotal() -1);
		 	saveOrUpdate(messageTemplate);
		 	saveOrUpdate(companyReviewed);
	}
	
	/**
	 * 
	 *指定号码发送短信 
	 * 
	 */
	private void   addSmsMobile   (  String smscontent , MessageTemplateEntity messageTemplate  , TSCompany company) {
		
		String mobile[] = messageTemplate.getDespatcher().split(",");
		   for(int i = 0 ; i < mobile.length ; i++){
			   if( StringUtil.isNotEmpty(mobile[i]) ){
				   try {
					sendSMS("", "", mobile[i], "", smscontent);
					//添加短信发送记录
					smsSendService.smsSendSave(messageTemplate.getType(), smscontent, "", mobile[i], company.getId());
					addSmsnumber(messageTemplate, company.getCompanyreviewed());
					
				} catch (Exception e) {
					
				}
				   
			   }
		   }
	}

	
	/**
	 *指定角色
	 * 
	 */
	private void   addSmsRolecode(String rolecode ,String smscontent , MessageTemplateEntity messageTemplate ,TSCompany  company ) {
		

		CriteriaQuery cqRole = new CriteriaQuery(TSRole.class);
		cqRole.eq("roleCode", rolecode);
		cqRole.eq("company", company );
		TSRole tsRole  = (TSRole) getObjectByCriteriaQuery(cqRole, false);
			
		CriteriaQuery cq = new CriteriaQuery(TSRoleUser.class);
		cq.eq("TSRole", tsRole);
		cq.eq("company", company );

		//获取消息模板
		List<TSRoleUser>  tsRoleUserList  =  getListByCriteriaQuery(cq, false);
		   for(int i = 0 ; i < tsRoleUserList.size() ; i++){
			   TSUser user = getEntity(TSUser.class, tsRoleUserList.get(i).getTSUser().getId());
			   
			   
			   if( StringUtil.isNotEmpty(user.getId()) ){
				   try {
					sendSMS("", "", user.getMobilephone(), "", smscontent);
					if(user.getMobilephone()!=null && !user.getMobilephone().equals("")){
						//添加短信发送记录
						smsSendService.smsSendSave(messageTemplate.getType(), smscontent, "", user.getMobilephone(), company.getId());
					}
					addSmsnumber(messageTemplate, company.getCompanyreviewed());
					
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				  
			   }
		   }
	}
	
	
	
	/**
	 *指定角色
	 * 
	 */
	private void   addSmsUser   ( TSUser  user , String smscontent , MessageTemplateEntity messageTemplate ,TSCompany  company ) {
		
			   if( StringUtil.isNotEmpty(user.getId()) ){
				   
				   try {
					user = getEntity(TSUser.class, user.getId());
					sendSMS("", "", user.getMobilephone(), "", smscontent);
					//添加短信发送记录
					if(user.getMobilephone()!=null && !user.getMobilephone().equals("")){
						smsSendService.smsSendSave(messageTemplate.getType(), smscontent, "", user.getMobilephone(), company.getId());
					}
					addSmsnumber(messageTemplate, company.getCompanyreviewed());
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				  
			   }
		   
	}
	
	
	
	/**
	 * 获取模板结构
	 * @param tmpid  消息类型
	 * @param tmpid  消息类型
	 * @return
	 * @throws Exception
	 */
	private MessageTemplateEntity messageTemplate(String tmpid , TSCompany tScompany) throws Exception{
		
		CriteriaQuery cq = new CriteriaQuery(MessageTemplateEntity.class);
		cq.eq("type", tmpid);
		cq.eq("state", 1);
		cq.eq("company", tScompany);
		//获取消息模板
		MessageTemplateEntity messageTemplate  = null;
		try{
			
			messageTemplate  = (MessageTemplateEntity) getObjectByCriteriaQuery(cq, false);
			
			if(  StringUtil.isEmpty( messageTemplate.getContent().trim() ) ){
				return null;
			}
		}catch (Exception e) {
			
		}
		
		
		return messageTemplate;
	}

	/**
	 * 线下转账完成通知财务
	 * */
	
	public boolean mosSendSmsDelivery_9(PayBillsOrderEntity payBillsOrder ) {
		try {
			if( payBillsOrder != null && StringUtil.isNotEmpty(payBillsOrder.getId())){
				payBillsOrder = getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
				
				TSCompany company = getEntity(TSCompany.class, payBillsOrder.getCompany().getId() );
				
				if( "2".equals(payBillsOrder.getState()) ){
					MessageTemplateEntity messageTemplate = messageTemplate("3",payBillsOrder.getCompany());
					if( messageTemplate != null &&  company.getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", company.getCompanyName());
							   smscontent  =smscontent.replaceAll("【采购客户名称】", payBillsOrder.getClientid().getRealname());
							   smscontent  =smscontent.replaceAll("【订单号】", payBillsOrder.getIdentifier());
							   //存在指定发送则先获取发送数据
							   if( messageTemplate != null && StringUtil.isNotEmpty(messageTemplate.getDespatcher()) ){
								   addSmsMobile(smscontent, messageTemplate ,company  );
							   }else{
								   addSmsRolecode("yewuyuan", smscontent, messageTemplate ,company) ;
							   }
							   
					}
				}
				this.checkMsgnumberTotal(company.getCompanyreviewed().getMsgnumberTotal(), company);
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * 开通客户短信通知
	 * 
	 * */
	
	public boolean mosSendSmsCopartner_1(TSUser u ,String passwordRandCode) {
		try {
			if( u != null && StringUtil.isNotEmpty(u.getId())){
				TSUser user = getEntity(TSUser.class, u.getId());
				TSCompany company = getEntity(TSCompany.class, user.getCompany().getId() );
				
				if( "6".equals(user.getType()) ){
					MessageTemplateEntity messageTemplate = messageTemplate("10",user.getCompany());
					if( messageTemplate != null &&  company.getCompanyreviewed().getMsgnumberTotal() > 0 ){
						String smscontent  = messageTemplate.getContent();
							   smscontent  =smscontent.replaceAll("【企业前缀】", company.getCompanyName());
							   smscontent  =smscontent.replaceAll("【账号名】", user.getUserName());
							   smscontent  =smscontent.replaceAll("【密码】", passwordRandCode);
							   smscontent  =smscontent.replaceAll("【登录地址】", company.getCompanyplatform().getWebsite().replace("http://", "http://admin."));
							   smscontent  =smscontent.replaceAll("【供货方联系电话】", company.getServicephone());
							   sendSMS("", "", user.getMobilephone(), "", smscontent);
							   if(user.getMobilephone()!=null && !user.getMobilephone().equals("")){
								   //添加短信发送记录
								   smsSendService.smsSendSave(messageTemplate.getType(), smscontent.replace(passwordRandCode, ""), "", user.getMobilephone(), company.getId());
							   }
							   addSmsnumber(messageTemplate, company.getCompanyreviewed());
							   
					}
				}
				this.checkMsgnumberTotal(company.getCompanyreviewed().getMsgnumberTotal(), company);
			}
		} catch (Exception e) {
			
			if(logger.isDebugEnabled()){
				logger.info(e);
			}
			logger.error(e);
			
			return false;
		}
		
		return true;
	}
	/**
	 * 验证剩余短信条数，看是否需要通知客户充值
	 * MsgnumberTotal：短信剩余条数
	 * */
	private void checkMsgnumberTotal(Integer msgnumberTotal, TSCompany company){
		NotifyTextEntity notifyTextEntity = new NotifyTextEntity();;
		NotifyEntity notifyEntity = new NotifyEntity();
		SmsSendEntity smsSend = new SmsSendEntity();
		//String smscontent = "您好，你的剩余短信条数已剩余" + msgnumberTotal + "条，为了您能够正常使用，请及时购买短信，谢谢";
		String smscontent = "您好，您的短信条数已少于20条，为了您能够正常使用，请及时购买短信，谢谢";
		notifyTextEntity.setContent(smscontent);
		notifyEntity.setTitle("短信购买");
		notifyEntity.setSender_id("星农场集团");
		Map<String,Object> map = findOneForJdbc("SELECT id from t_s_user where company='" + company.getId() + "' and type=2 order by createDate asc LIMIT 1",null);
		notifyEntity.setReceiver_id(map.get("id")+"");
		if(msgnumberTotal<20){
			try {
				smsSend.setType("-1");
				smsSend.setCompany_id(company.getId());
				smsSend = smsSendService.getSmsSendData(smsSend);
				if(smsSend == null || smsSend.getId() == null){
					sendSMS("", "", company.getMobilephone(), "", smscontent);
					if(company.getMobilephone()!=null && !company.getMobilephone().equals("")){
						//添加短信发送记录
						smsSendService.smsSendSave("-1", smscontent, "", company.getMobilephone(), company.getId());
						notifyService.notifySystemSave(notifyEntity, notifyTextEntity);
						CompanyReviewedEntity companyReviewed = company.getCompanyreviewed();
						companyReviewed.setMsgnumber(companyReviewed.getMsgnumber()+1);
						companyReviewed.setMsgnumberTotal(companyReviewed.getMsgnumberTotal()-1);
						saveOrUpdate(companyReviewed);
					}
					
				}
				
			} catch (Exception e) {
				logger.error("checkMsgnumberTotal发送短信异常#" + company.getMobilephone() + "#"+e);
			}
		}else{
			smsSend.setType("0");
			smsSend.setCompany_id(company.getId());
			smsSendService.smsSendUpdateType(smsSend);
		}
	}
	
}