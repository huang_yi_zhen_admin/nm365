package cn.gov.xnc.admin.dataStatistics.service;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.kuaidi.dao.KuaidiDAO;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.ExpressSearchUtil;
import cn.gov.xnc.system.core.util.ParamUtil;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("dataStatisticsService")
public class DataStatisticsService {
	private static KuaidiDAO kuaidiDAO = KuaidiDAO.getInstance();
	private static final Logger logger = Logger.getLogger(DataStatisticsService.class);
	/**
	 * 返回物流查询信息列表
	 * */
	public String getLogisticsInquiriesList(HttpServletRequest request, SystemService systemService){
		Map<String, Object> map = new HashMap<String,Object>();
		DataGrid dataGrid = new DataGrid();
		StringBuilder sbd = new StringBuilder();
		StringBuilder data = new StringBuilder();
		StringBuilder rows = new StringBuilder();
		List<Map<String, Object>> list = null;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Integer pageNum = ParamUtil.getIntParameter(request, "pageNum", 1);
		Integer showNum = ParamUtil.getIntParameter(request, "showNum", 10);
		String date = "";
		Integer searchType  = ParamUtil.getIntParameter(request, "searchType", 0);
		String key = "";
		String departureDate = ParamUtil.getParameter(request, "departureDate", "");
		String[] departureDateArry;
		key = ParamUtil.getParameter(request, "key", "");
		switch(searchType){
			case 1 : map.put("identifierOr", key);break;
			case 2 : map.put("CustomerName", key);break;
			case 3 : map.put("Telephone", key);break;
			case 4 : map.put("productName", key);break;
			case 5 : map.put("address", key);break;
			case 6 : map.put("logisticsCompany", key);break;
			case 7 : map.put("logisticsNumber", key);break;
			case 8 : map.put("freightState", key);break;
			case 9 : map.put("clientId", key);break;
			case 10 : map.put("yewu", key);break;
			case 11 : 
				if(!departureDate.equals("")){
					departureDateArry = departureDate.split("-");
					map.put("gtDepartureDate", departureDateArry[0].replaceAll("/", "-").trim());
					map.put("ltDepartureDate", departureDateArry[1].replaceAll("/", "-").trim());
				}
				
				break;
		}
		
		String msg = "获取数据成功";
		Integer success = 1;
		String state = "";
		int i=0;
		dataGrid.setPageNum(pageNum);
		dataGrid.setShowNum(showNum);
		
		list = kuaidiDAO.getLogisticsInquiriesList(map, dataGrid, systemService);
		rows.append("[");
		if(list!=null && list.size()>0){
			for(Map<String, Object> m : list){
				state = "暂无物流信息";
				date = "";
				if(i>0){
					rows.append(",");
				}
				if(m.get("state") != null){
					state = m.get("state")+"";
				}
				if(m.get("DepartureDate") != null){
					date = sdf.format(m.get("DepartureDate"));
				}
				rows.append("[");
				rows.append("\"" + m.get("orderId") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("identifierOr") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("productName") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("Number") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("CustomerName") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("Telephone") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("address") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("logisticscompany") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("logisticsnumber") + "\"");
				rows.append(",");
//				rows.append("\"" + state + "\"");
//				rows.append(",");
				rows.append("\"" + m.get("dictionaryName") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("clientId") + "\"");
				rows.append(",");
				rows.append("\"" + m.get("yewu") + "\"");
				rows.append(",");
				rows.append("\"" + date + "\"");
				rows.append("]");
				i++;
			}
			
		}else{
			msg = "暂无数据";
		}
		rows.append("]");
		data.append("\"page\"");
		data.append(":");
		data.append("\"" + dataGrid.getPageNum() +"\"");
		data.append(",");
		data.append("\"total\"");
		data.append(":");
		data.append("\"" + dataGrid.getTotal() +"\"");
		data.append(",");
		data.append("\"rows\"");
		data.append(":");
		data.append(rows);
		
		sbd.append("{");
		sbd.append("\"msg\"");
		sbd.append(":");
		sbd.append("\"" + msg +"\"");
		sbd.append(",");
		sbd.append("\"success\"");
		sbd.append(":");
		sbd.append(success);
		sbd.append(",");
		sbd.append("\"data\"");
		sbd.append(":");
		sbd.append("{" + data +"}");
		sbd.append("}"); 
		return sbd.toString();
	}
	/**
	 * 返回物流查询信
	 * @throws Exception 
	 * */
	public List<Map<String, Object>> getLogistics(String orderId, String logisticscompany, String logisticsnumber, SystemService systemService) throws Exception{
		return kuaidiDAO.getLogistics(orderId, logisticscompany, logisticsnumber, systemService);
	}
}
