package cn.gov.xnc.admin.zhifu.alipay.controller;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;








import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsFlowEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;
import cn.gov.xnc.admin.pay.config.CommonConfig;
import cn.gov.xnc.admin.pay.service.impl.PaySmsRecordServiceImpl;
import cn.gov.xnc.admin.product.controller.ProductController;
import cn.gov.xnc.admin.zhifu.alipay.config.AlipayConfig;
import cn.gov.xnc.admin.zhifu.alipay.util.AlipayNotify;


import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyThirdpartyEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;




/**   
 * @Title: Controller
 * @Description: 支付宝支付基础
 * @author zero
 * @date 2016-04-13 20:22:17
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/alipayaController")
public class AlipayaController extends BaseController {
	
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductController.class);

	@Autowired
	private SystemService systemService;
	
	@Autowired
	private PayBillsServiceI payBillsService;
	
	@Autowired
	private PayBillsOrderServiceI billsOrderService;
	
	@Resource(name="paySmsRecordService")
	private PaySmsRecordServiceImpl paySmsRecordService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@RequestMapping(value = "r2alipay")
	public ModelAndView r2alipay(HttpServletRequest request) {
		request.setAttribute("paybillsids", request.getParameter("id"));
		request.setAttribute("type", request.getParameter("type"));
		request.setAttribute("paymoney", request.getParameter("paymoney"));
		return new ModelAndView("admin/alipay/r2alipay");
	}
	/**
	 * 支付总入口跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "alipayapi")
	public ModelAndView alipaya(HttpServletRequest request) {
		
		Map<String, String> sParaTemp = new HashMap<String, String>();
		AlipayConfig alipayConfig = new AlipayConfig();
		 boolean alipay =true ;
		
		try{
			String idS = request.getParameter("id");
			String type = request.getParameter("type");
			
			 TSUser user  = ResourceUtil.getSessionUserName();
			 if( user != null && StringUtil.isNotEmpty(user.getId()) ){
				 user = systemService.get(TSUser.class, user.getId());
			 }
			 try {
				 //获取访问域名
				 
				 CompanyThirdpartyEntity  companyThirdparty = user.getCompany().getCompanythirdparty();
				 	if(  alipay && companyThirdparty != null && StringUtil.isNotEmpty(companyThirdparty.getAlipayid()) ){
				 		alipayConfig.setPartner(companyThirdparty.getAlipayid());
				 		alipayConfig.setSeller_id(companyThirdparty.getAlipayid());
				 	}else{
				 		alipay = false ;
				 	}
				 	
				 	if( alipay && companyThirdparty != null && StringUtil.isNotEmpty(companyThirdparty.getAlipaykey()) ){
				 		alipayConfig.setKey(companyThirdparty.getAlipaykey());
				 	}else{
				 		alipay = false ;
				 	}
				 	String url =  request.getScheme() +"://" + request.getServerName();
				 	String  notify_url =url+"/alipayaController/notifyAlipaya"; 
				 	String  return_url =url+"/webpage/admin/alipay/return_url.jsp";
				 	alipayConfig.setNotify_url(notify_url);
				 	alipayConfig.setReturn_url(return_url);
				 	
				 	alipayConfig.setAlipay(alipay);
			} catch (Exception e) {
				alipay = false ;
			}

			if( alipay ){

		        String out_trade_no="";//商户订单号，商户网站订单系统中唯一订单号，必填   payBills支付表id
		        String subject =user.getCompany().getCompanyName()+"支付单";//订单名称，必填
		        String total_fee ="";//付款金额，必填
		       
		        
		        PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
			        payment.setIdentifier(IdWorker.generateSequenceNo());
			        payment.setClientid(user);
			        payment.setCompany(user.getCompany());
			        payment.setType(type);
			        payment.setState("5");
			        payment.setPaymentmethod("1");
		        
			        if( "1".equals(type)){//购买
			        	//下单自动出库
			        	AjaxJson ajaxJson = new AjaxJson();
			        	ajaxJson = billsOrderService.payBillsOrderStockOut(payment, request);
			        	if(!ajaxJson.isSuccess()){
			        		return new ModelAndView("errorpage/500");
			        	}
				    	payment  = payBillsService.payment(idS, user, payment);
				    }else if ( "2".equals(type) ){//充值

				    	String paymoney = request.getParameter("paymoney");
				    	BigDecimal moneyPage = new BigDecimal(paymoney);
				    	payment.setPayName("充值");
						payment.setAlreadypaidmoney(moneyPage);
				    	payment  = payBillsService.paymentUser( payment  );
				    }      
			        
		        
				if (StringUtil.isNotEmpty(payment.getId())) {
					message = "账户资金流水更新成功";
					try {
						out_trade_no = new String(payment.getId().getBytes("UTF-8"),"UTF-8");
						//subject = new String(request.getParameter("WIDsubject").getBytes("UTF-8"),"UTF-8");
						//total_fee = new String(request.getParameter("WIDtotal_fee").getBytes("UTF-8"),"UTF-8");
						//body = new String(request.getParameter("WIDbody").getBytes("UTF-8"),"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					 total_fee = payment.getAlreadypaidmoney().toString();
					 String body =payment.getPayName();//商品描述，可空
					 
					 if( "4028802f58149a320158149cdd570004".equals( payment.getCompany().getId()) ){
						 total_fee ="0.01";
					 }
					 
					//total_fee ="0.01";
						//把请求参数打包成数组
							sParaTemp.put("service", AlipayConfig.service);
					        sParaTemp.put("partner", alipayConfig.getPartner());
					        sParaTemp.put("seller_id", alipayConfig.getSeller_id());
					        sParaTemp.put("_input_charset", AlipayConfig.input_charset);
							sParaTemp.put("payment_type", AlipayConfig.payment_type);
							sParaTemp.put("notify_url", alipayConfig.getNotify_url());
							sParaTemp.put("return_url", alipayConfig.getReturn_url());
							sParaTemp.put("anti_phishing_key", AlipayConfig.anti_phishing_key);
							sParaTemp.put("exter_invoke_ip", AlipayConfig.exter_invoke_ip);
							sParaTemp.put("out_trade_no", out_trade_no);
							sParaTemp.put("subject", subject);
							sParaTemp.put("total_fee", total_fee);
							sParaTemp.put("body", body);	
			  }
			} 
			
			
			
			
			
			
			
		}catch (Exception e) {
			
		}
		
		request.setAttribute("sParaTemp", sParaTemp);
		request.setAttribute("alipayConfig", alipayConfig);
		return new ModelAndView("admin/alipay/alipayapi");
	}
	

	
	/**
	 * 支付跳转页面
	 * 
	 * @return
	 */
	@RequestMapping("/notifyAlipaya")
	public void notifyAlipaya(HttpServletRequest request) {
		
		//获取支付宝POST过来反馈信息
		systemService.addLog("支付宝支付回调开始", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}
		
		//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
		//商户订单号
		String out_trade_no ="";
		//支付宝交易号
		String trade_no = "";
		//交易状态
		String trade_status = "";
		try {
			out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
			trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
			trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
		} catch (Exception e) {
			
		}

		systemService.addLog("获取支付宝POST过来反馈信息，交易状态trade_status="+trade_status+",商户订单号=out_trade_no="+out_trade_no+"支付宝交易号trade_no=" + trade_no, Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		PayBillsEntity payment = systemService.get(PayBillsEntity.class, out_trade_no);
		PayBillsEntity billsParam = new PayBillsEntity();
		billsParam.setId(payment.getId());
		
		AlipayConfig alipayConfig = payBillsService.getAlipayConfig(payment);
		
		systemService.addLog("进入支付回调认证AlipayNotify.verify", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		if(AlipayNotify.verify(params )){//验证成功
			/*
			 * TRADE_FINISHED 即时到账普通版。   普通版不支持支付完成后的退款操作，即用户充值完成后，该交易就算是完成了，这笔交易就不能再做任何操作了。
    		 * TRADE_SUCCESS  即时到账高级版。   这个版本在用户充值完成后，卖家可以执行退款操作进行退款，即该交易还没有彻底完成，卖家还可以修改这笔交易。
			 * */
			//请在这里加上商户的业务逻辑程序代码
			systemService.addLog("通过支付回调认证AlipayNotify.verify，进入支付状态认证：", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
			//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
			if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS") ){//TRADE_FINISHED    交易完成  TRADE_SUCCESS  交易成功（或支付成功）
				//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
					//如果有做过处理，不执行商户的业务程序
				systemService.addLog("通过支付状态认证trade_status，进入订单状态验证：", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
				if( payment != null &&  alipayConfig  != null  ){  //对应的订单信息
					
					systemService.addLog("通过订单状态验证，开始修改订单状态："+payment.getState()+ ", alipayConfig.getSeller_id:" + alipayConfig.getSeller_id() + ",params.seller_id:" + params.get("seller_id"), Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
					if( "5".equals(payment.getState()) && ( params.get("seller_id")).equals( alipayConfig.getSeller_id() )){
						billsParam.setState("4");
						billsParam.setVoucher(trade_no);
						payBillsService.saveState4OnlinePay(null, billsParam);
						systemService.addLog("订单状态支付状态已经修改", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
					}
				}
			}
			systemService.addLog("支付宝支付回调结束", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		}else{//验证失败
			logger.error(">>>>>>>>>>>>notifyAlipaya 支付回调认证失败！");
			systemService.addLog("支付宝支付回调失败", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		}
		
	}
	
	/**
	 * 支付跳转页面,为特别商户
	 * 
	 * @return
	 */
	@RequestMapping("/notifyAlipayaSpec")
	public void notifyAlipayaSpec(HttpServletRequest request) {
		
		//获取支付宝POST过来反馈信息
		systemService.addLog("支付宝支付回调开始", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		Map<String,String> params = new HashMap<String,String>();
		Map<?, ?> requestParams = request.getParameterMap();
		for (Iterator<?> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			params.put(name, valueStr);
		}
		
		//商户订单号
		String out_trade_no ="";
		//支付宝交易号
		String trade_no = "";
		//交易状态
		String trade_status = "";
		//收款账户
		String seller_id = params.get("seller_id");
		try {
			out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
			trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
			trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
		} catch (Exception e) {
			
		}

		systemService.addLog("获取支付宝POST过来反馈信息，交易状态trade_status="+trade_status+",商户订单号=out_trade_no="+out_trade_no+"支付宝交易号trade_no=" + trade_no, Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		PayBillsEntity payment = systemService.get(PayBillsEntity.class, out_trade_no);
		
		AlipayConfig alipayConfig = payBillsService.getAlipayConfig(payment);
		alipayConfig.setPartner(CommonConfig.getAliPidKey().get("xncPid").toString());
		alipayConfig.setSeller_id(CommonConfig.getAliPidKey().get("xncPid").toString());
		alipayConfig.setKey(CommonConfig.getAliPidKey().get("xncKey").toString());
		systemService.addLog("进入支付回调认证AlipayNotify.verify", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		if(AlipayNotify.verifyOriginal(params ,alipayConfig)){//验证成功
			systemService.addLog("通过支付回调认证AlipayNotify.verify，进入支付状态认证：", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
			if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS") ){//TRADE_FINISHED    交易完成  TRADE_SUCCESS  交易成功（或支付成功）
				systemService.addLog("通过支付状态认证trade_status，进入订单状态验证：", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
				if( payment != null &&  alipayConfig  != null  ){  //对应的订单信息
					if( "5".equals(payment.getState()) && ( seller_id).equals( alipayConfig.getSeller_id() )){
						systemService.addLog("通过订单状态验证，开始修改订单状态：", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
						payment.setState("4");
						payment.setVoucher(trade_no);
						payBillsService.saveState(null, payment);
						paySmsRecordService.paymentRecordUpdate(params, payment);
					}
				}
			}
			systemService.addLog("支付宝支付回调结束", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		}else{//验证失败
			logger.error(">>>>>>>>>>>>notifyAlipayaSpec 支付回调认证失败！");
			systemService.addLog("支付宝支付回调失败", Globals.Log_Leavel_INFO, Globals.Log_Type_OTHER);
		}
		
	}

	
	
}
