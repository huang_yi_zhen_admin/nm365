package cn.gov.xnc.admin.operation.controller;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.operation.entity.OperationLogEntity;
import cn.gov.xnc.admin.operation.service.OperationLogServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 用户操作记录
 * @author zero
 * @date 2017-05-05 14:45:41
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/operationLogController")
public class OperationLogController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OperationLogController.class);

	@Autowired
	private OperationLogServiceI operationLogService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 用户操作记录列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView operationLog(HttpServletRequest request) {
		return new ModelAndView("admin/operation/operationLogList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OperationLogEntity operationLog,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(OperationLogEntity.class, dataGrid);
		
		if( null!= operationLog.getOperatorid() && StringUtil.isNotEmpty(operationLog.getOperatorid().getRealname())){
			cq.createAlias("operatorid", "user") ;
	    	cq.add( Restrictions.like("user.realname", "%"+operationLog.getOperatorid().getRealname()+"%" ) ) ;
			operationLog.getOperatorid().setRealname(null);;
		}
		
		cq.eq("company", user.getCompany());
		
		
		String dateBegin = request.getParameter("dateBegin");
		String dateEnd = request.getParameter("dateEnd");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss"); 
		if(StringUtil.isNotEmpty(dateBegin)&&StringUtil.isNotEmpty(dateEnd)){
			cq.between("updatetime", DateUtils.str2Date(dateBegin,sdf), DateUtils.str2Date(dateEnd,sdf));
		}else if( StringUtil.isNotEmpty(dateBegin) ){
			cq.ge("updatetime", DateUtils.str2Date(dateBegin,sdf));
		}else if( StringUtil.isNotEmpty(dateEnd) ){
			cq.le("updatetime", DateUtils.str2Date(dateEnd,sdf));
		}
		cq.addOrder("updatetime", SortDirection.desc);
		cq.add();
		
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, operationLog, request.getParameterMap());
		this.operationLogService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除用户操作记录
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(OperationLogEntity operationLog, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		operationLog = systemService.getEntity(OperationLogEntity.class, operationLog.getId());
		message = "用户操作记录删除成功";
		operationLogService.delete(operationLog);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加用户操作记录
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(OperationLogEntity operationLog, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(operationLog.getId())) {
			message = "用户操作记录更新成功";
			OperationLogEntity t = operationLogService.get(OperationLogEntity.class, operationLog.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(operationLog, t);
				operationLogService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "用户操作记录更新失败";
			}
		} else {
			message = "用户操作记录添加成功";
			operationLogService.save(operationLog);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 用户操作记录列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(OperationLogEntity operationLog, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(operationLog.getId())) {
			operationLog = operationLogService.getEntity(OperationLogEntity.class, operationLog.getId());
			req.setAttribute("operationLogPage", operationLog);
		}
		return new ModelAndView("admin/operation/operationLog");
	}
}
