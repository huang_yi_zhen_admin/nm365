package cn.gov.xnc.admin.excel.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerOrderServiceI;
import cn.gov.xnc.admin.copartner.service.OrderSendServiceI;
import cn.gov.xnc.admin.copartner.service.UserCopartnerAccountServiceI;
import cn.gov.xnc.admin.excel.service.ExcelServiceI;
import cn.gov.xnc.admin.excel.vo.CPBLExcel;
import cn.gov.xnc.admin.excel.vo.CopartnerOrderExcel;
import cn.gov.xnc.admin.excel.vo.CopartnerOrderSend;
import cn.gov.xnc.admin.excel.vo.FahuoProductVO;
import cn.gov.xnc.admin.excel.vo.FreightVO;
import cn.gov.xnc.admin.excel.vo.ICExcel;
import cn.gov.xnc.admin.excel.vo.InvoiceExcel;
import cn.gov.xnc.admin.excel.vo.OrderExcel;
import cn.gov.xnc.admin.excel.vo.PayBillOrderExcel;
import cn.gov.xnc.admin.excel.vo.PayBillsExcel;
import cn.gov.xnc.admin.excel.vo.PayBillsOrderCashExcel;
import cn.gov.xnc.admin.excel.vo.ProductFreightVO;
import cn.gov.xnc.admin.excel.vo.QDXSExcel;
import cn.gov.xnc.admin.excel.vo.StockCheckDetailExcel;
import cn.gov.xnc.admin.excel.vo.StockIODetailExcle;
import cn.gov.xnc.admin.excel.vo.StockIODetailVOExcel;
import cn.gov.xnc.admin.excel.vo.StockProductDetailExcel;
import cn.gov.xnc.admin.excel.vo.StockProductExcel;
import cn.gov.xnc.admin.excel.vo.StockSupplierExcel;
import cn.gov.xnc.admin.excel.vo.XiadanProductVO;
import cn.gov.xnc.admin.excel.vo.YHExcel;
import cn.gov.xnc.admin.excel.vo.YeWuExcel;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.entity.StockSupplierEntity;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.util.BrowserUtils;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ExceptionUtil;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.excel.ExcelExportUtil;
import cn.gov.xnc.system.excel.ExcelImportUtil;
import cn.gov.xnc.system.excel.entity.ImportParams;
import cn.gov.xnc.system.excel.entity.TemplateExportParams;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserStaffServiceI;





/**   
 * @Title: Controller
 * @Description: 导入导出excel控制类
 * @author ljz
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/excelController")
public class ExcelController extends BaseController {
	
	
	private static final Logger log = Logger.getLogger(ExcelController.class);

	@Autowired
	private SystemService systemService;
	@Autowired
	private UserStaffServiceI userStaffService;
	@Autowired
	private  OrderServiceI orderService;
	@Autowired
	private OrderSendServiceI orderSendService;
	@Autowired
	private CopartnerOrderServiceI copartnerOrderService;
	@Autowired
	private UserCopartnerAccountServiceI userCopartnerAccountService;
	@Autowired
	private  ExcelServiceI excelService;
	@Autowired
	private MessageTemplateServiceI messageTemplateService ;
	@Autowired
	private FreightDetailsServiceI freightDetailsService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;

	/**
	 * 根据条件返回response
	 * @param request
	 * @param response
	 * @return
	 */
	public void getResponse(HttpServletRequest request,HttpServletResponse response,String fileName){
		try{
			response.setContentType("application/vnd.ms-excel");
			// 根据浏览器进行转码，使其支持中文文件名
			if (BrowserUtils.isIE(request)) {
				response.setHeader("content-disposition","attachment;filename="+ java.net.URLEncoder.encode(fileName,"UTF-8") + ".xls");
			} else {
				String newtitle = new String(fileName.getBytes("UTF-8"),"ISO8859-1");
				response.setHeader("content-disposition","attachment;filename=" + newtitle + ".xls");
			}
		}catch(Exception e){
			log.error("导出excel出错,出错信息为:"+ExceptionUtil.getExceptionMessage(e));
		}
	}
	
	public void getDocResponse(HttpServletRequest request,HttpServletResponse response,String fileName){
		try{
			response.setContentType("application/msword");
			// 根据浏览器进行转码，使其支持中文文件名
			if (BrowserUtils.isIE(request)) {
				response.setHeader(
						"content-disposition",
						"attachment;filename="
								+ java.net.URLEncoder.encode(fileName,
										"UTF-8") + ".doc");
			} else {
				String newtitle = new String(fileName.getBytes("UTF-8"),
						"ISO8859-1");
				response.setHeader("content-disposition",
						"attachment;filename=" + newtitle + ".doc");
			}
		}catch(Exception e){
			log.error("导出excel出错,出错信息为:"+ExceptionUtil.getExceptionMessage(e));
		}
	}
	
	public void excel(String methodName,HttpServletResponse response,TemplateExportParams templateExportParams,Class<?> pojoClass, ArrayList<Collection<?>> dataSet, Map<String, Object> map){
		OutputStream fOut = null;
		try{
				// 产生工作簿对象
				Workbook workbook  = ExcelExportUtil.exportExcelByArray(templateExportParams,pojoClass, dataSet,map);
				
				fOut = response.getOutputStream();
				workbook.write(fOut);
		}catch (Exception e) {
			log.error("方法["+methodName+"],导出excel出错,出错信息为:"+ExceptionUtil.getExceptionMessage(e));
		} finally {
			try {
				fOut.flush();
				fOut.close();
			} catch (IOException e) {

			}
		}
	}
	
	/**
	 * 
	 * @param <T>
	 * @param j
	 * @param params
	 * @param pojoClass
	 * @param request
	 * @param response
	 * @param isRetrueList  等于true则直接返回list  否则入库操作
	 * @return
	 */
	public <T> List<T> importExcel(AjaxJson j,ImportParams params,Class<?> pojoClass,HttpServletRequest request, HttpServletResponse response,boolean isRetrueList){
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		String fileName = "";
		StringBuffer fileSuccess = new StringBuffer();
		StringBuffer fileFail = new StringBuffer();
		List<T> list = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			fileName = file.getName();
			try {
				
				if(isRetrueList){
					list = (List<T>) ExcelImportUtil.importExcelByIs(file.getInputStream(),pojoClass,params);
					break;
				}else{
					this.systemService.batchSave(list);
				}
				
				if(fileSuccess.length() == 0){
					fileSuccess.append(fileName);
				}else{
					fileSuccess.append(",");
					fileSuccess.append(fileName);
				}
				
			} catch (Exception e) {
				if(fileFail.length() == 0){
					fileFail.append(fileName);
				}else{
					fileFail.append(",");
					fileFail.append(fileName);
				}
				log.error(ExceptionUtil.getExceptionMessage(e));
			}finally{
				
				try {
					if(file.getInputStream()!=null){
						file.getInputStream().close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		String allfile = null;
		if(fileSuccess.length() > 0 && fileFail.length() > 0){
			j.setMsg("您的文件：" + fileSuccess.toString() + "已经导入成功，其中：" + fileFail.toString() + "导入失败！");
			allfile = fileSuccess.toString() + "," + fileFail.toString();
			
		}else if(fileSuccess.length() > 0 && fileFail.length() == 0){
			j.setMsg("您的文件：" + fileSuccess.toString() + "已经导入成功!");
			allfile = fileSuccess.toString();
			
		}else if(fileSuccess.length() == 0 && fileFail.length() > 0){
			j.setMsg("您的文件：" + fileSuccess.toString() + "已经导入失败!");
			allfile = fileFail.toString();
			
		}else if(fileSuccess.length() == 0 && fileFail.length() == 0){
			j.setMsg("请上传您需要导入的文件~");
		}
		//记录所有导入的文件
		Map fileAttr = new HashMap<String, String>();
		fileAttr.put("fileid",allfile);
		j.setAttributes(fileAttr);
		return list;
	}
	
	/**
	 * 导出出入库明细
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputIODetailExcel")
	public void  outputIODetailExcel(StockIODetailEntity stockIOOrderDetail, String ordername, String orderdirection, HttpServletRequest request, HttpServletResponse response){
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class );
		if( null!= stockIOOrderDetail.getProductid() ){
			stockIOOrderDetail.getProductid().setIsDelete(null);
			stockIOOrderDetail.getProductid().setNotsale(null);
			if( StringUtil.isNotEmpty(stockIOOrderDetail.getProductid().getName()) ){
				cq.createAlias("productid", "product");
				cq.add(Restrictions.like("product.name", "%"+stockIOOrderDetail.getProductid().getName()+"%"));
				stockIOOrderDetail.getProductid().setName(null);
			}
		}

		if( null != stockIOOrderDetail.getSupplierid() && StringUtil.isNotEmpty(stockIOOrderDetail.getSupplierid().getName())){
			cq.createAlias("supplierid", "supplier");
			cq.add(Restrictions.like("supplier.name", "%"+stockIOOrderDetail.getSupplierid().getName()+"%"));
			stockIOOrderDetail.getSupplierid().setName(null);
		}else{
			stockIOOrderDetail.setSupplierid(null);
		}
		
		if( null!=stockIOOrderDetail.getStockorderid() ){
			if( null!=stockIOOrderDetail.getStockorderid().getOperator() && StringUtil.isNotEmpty(stockIOOrderDetail.getStockorderid().getOperator().getRealname()) ){
				cq.createAlias("stockorderid.operator", "oper");
				cq.add(Restrictions.like("oper.realname", "%"+stockIOOrderDetail.getStockorderid().getOperator().getRealname()+"%"));
				stockIOOrderDetail.getStockorderid().getOperator().setRealname(null);
			}
			if( null != stockIOOrderDetail.getStockorderid().getStocktypeid() && StringUtil.isNotEmpty(stockIOOrderDetail.getStockorderid().getStocktypeid().getDictionaryType() )){
				cq.createAlias("stockorderid.stocktypeid", "stocktype");
				cq.add(Restrictions.eq("stocktype.typecode",stockIOOrderDetail.getStockorderid().getStocktypeid().getDictionaryType()));
				stockIOOrderDetail.getStockorderid().getStocktypeid().setDictionaryType(null);;
			}
			
			if( null != stockIOOrderDetail.getStockorderid().getStockid() && StringUtil.isNotEmpty( stockIOOrderDetail.getStockorderid().getStockid().getId())){
				cq.createAlias("stockorderid.stockid", "stock");
				cq.add(Restrictions.eq("stock.id", stockIOOrderDetail.getStockorderid().getStockid().getId()));
			}
			
			stockIOOrderDetail.getStockorderid().setCompanyid(user.getCompany());
			cq.createAlias("stockorderid", "stockorder");
			cq.createAlias("stockorder.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
			
			if( StringUtil.isNotEmpty(stockIOOrderDetail.getStockorderid().getId()) ){
				cq.add(Restrictions.eq("stockorder.id", stockIOOrderDetail.getStockorderid().getId()));
			}
		}else{
			stockIOOrderDetail.setStockorderid(new StockIOEntity());
			stockIOOrderDetail.getStockorderid().setCompanyid(user.getCompany());
			cq.createAlias("stockorderid", "stockorder");
			cq.createAlias("stockorder.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
		}
		
		//时间
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("updatetime", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("updatetime", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("updatetime", DateUtils.str2Date(createdate2,sdf));
		}
		if( StringUtil.isNotEmpty(ordername) ){
			if( "asc".equals(orderdirection) ){
				cq.addOrder(ordername, SortDirection.asc);
			}else{
				cq.addOrder(ordername, SortDirection.desc);
			}
		}else{
			cq.addOrder("updatetime", SortDirection.desc);
		}
		
		List<StockIODetailEntity> list = systemService.getListByCriteriaQuery(cq, false);
		List<StockIODetailVOExcel> excelList = new ArrayList<StockIODetailVOExcel>();
		
		if( null!=list && list.size() > 0 ){
			
			for(StockIODetailEntity detail : list ){
				StockIODetailVOExcel excel = new StockIODetailVOExcel();
				try{
					excel.setStockorderid_stockid_stockname( detail.getStockorderid().getStockid().getStockname());
				}catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setStockorderid_operator_realname(detail.getStockorderid().getOperator().getRealname());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setProductid_name(detail.getProductid().getName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setProductid_code(detail.getProductid().getCode());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					if(StockIOType.IN.equals(detail.getStockorderid().getStocktypeid().getDictionaryType())){
						excel.setStockorderid_stocktypeid_type("入库");
					}else if(StockIOType.OUT.equals(detail.getStockorderid().getStocktypeid().getDictionaryType()) ){
						excel.setStockorderid_stocktypeid_type("出库");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setStockorderid_stocktypeid_typecode(detail.getStockorderid().getStocktypeid().getDictionaryName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setIostocknum(detail.getIostocknum().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setUnitid_name(detail.getUnitid().getName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setFreightprice(detail.getFreightprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setUnitprice(detail.getUnitprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setTotalprice(detail.getTotalprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setSupplierid_name(detail.getSupplierid().getName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setUpdatetime(DateUtils.date3Str(detail.getUpdatetime(), "yyyy-MM-dd HH:mm:ss"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
//					excel.setRightnowstockbasenum(detail.getRightnowstockbasenum().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
//					excel.setAveragebaseunitprice(detail.getAveragebaseunitprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
//					excel.setRightnowtotalprice(detail.getRightnowstockbasenum().multiply(detail.getUnitprice()).toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				excelList.add(excel);
			}
			
			
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(excelList);

		String fileName  = "出入库明细单数据"+DateUtils.getDate("yyyyMMddHHmmss");
		getResponse(request,response,fileName);
		excel("outputIODetailExcel",response,new TemplateExportParams("export/template/output/StockIODetailVOExcel.xls"),StockIODetailVOExcel.class,dataList,map);
		
		
		
	}
	
	/**
	 * 导出库存状态
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputStockProductInfo")
	public void  outputStockProductInfo( StockProductEntity stockProduct ,HttpServletRequest request,HttpServletResponse response) {
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
								
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
		
		StringBuffer productidlist = null;
		if( StringUtil.isNotEmpty(stockProduct.getStatus())){
			CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class);
			cq.eq("companyid", user.getCompany() );
			cq.eq("status", stockProduct.getStatus() );
			List<StockProductEntity> splist = systemService.getListByCriteriaQuery(cq,false);
			if( null!=splist && splist.size() > 0 ){
				productidlist = new StringBuffer();
				for( StockProductEntity sp : splist ){
					if( productidlist.length() > 0 ){
						productidlist.append(",");
					}
					productidlist.append("'").append(sp.getProductid().getId()).append("'");
				}
			}
		}
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("select CONCAT(a.productid,'-',a.stockid,'-',a.stock_product_price_id) as id, SUM(a.stocknum) as stocknum, SUM(a.totalprice) as totalprice, (SUM(a.totalprice)/SUM(a.stocknum)) as averageprice ");
		sql.append(" from  xnc_company_stock_product a INNER JOIN xnc_product b INNER JOIN xnc_product_classify c where a.productid=b.id  and c.id=b.classifyid ");
		sql.append(" and a.companyid='").append(user.getCompany().getId()).append("' ");
		
		if( null != stockProduct.getProductid() && StringUtil.isNotEmpty( stockProduct.getProductid().getName()) ){
			sql.append(" and b.name like '%").append(stockProduct.getProductid().getName()).append("%' ");
		}
		
		if( null != stockProduct.getProductid() && null != stockProduct.getProductid().getClassifyid() && StringUtil.isNotEmpty( stockProduct.getProductid().getClassifyid().getId()) ){
			sql.append(" and c.id='").append(stockProduct.getProductid().getClassifyid().getId()).append("' ");
		}
		
		if( null != stockProduct.getStockid() && StringUtil.isNotEmpty(stockProduct.getStockid().getId())){
			sql.append(" and a.stockid='").append(stockProduct.getStockid().getId()).append("' ");
		}
		
		if( null != productidlist && productidlist.length() > 0 ){
			sql.append(" and b.id in (").append(productidlist).append(") ");
		}
		
		//过滤不显示库存为0且一个月内没有补充库存的商品
		int expireday = 30;
		sql.append(" and (a.stocknum > 0 or ( a.stocknum = 0 and datediff(curdate(), date(a.update_time)) < ").append(expireday).append(")) ");
		
		sql.append(" GROUP BY a.productid,a.stockid ORDER BY a.update_time DESC; ");
		
		List<StockProductEntity> list = stockProductService.queryListByJdbc(sql.toString(),StockProductEntity.class);
		if( null != list ){
			for( int i = 0 ; i < list.size(); i++ ){
				StockProductEntity stockproduct = list.get(i);
				String id = stockproduct.getId();
				if( id != null){
					String[] idlist = id.split("-");
					String productid = idlist[0];
					String stockid = idlist[1];
					String stockproductprice = idlist[2];
					
					ProductEntity product = stockProductService.getEntity(ProductEntity.class, productid);
					stockproduct.setProductid(product);
					StockEntity stock = stockProductService.getEntity(StockEntity.class, stockid);
					stockproduct.setStockid(stock);
					if( null == stockproduct.getAverageprice() ){
						stockproduct.setAverageprice(new BigDecimal(0.00));
					}
					
					StockProductPriceEntity stockProductPrice = stockProductService.getEntity(StockProductPriceEntity.class, stockproductprice);
					stockproduct.setStockProductPrice(stockProductPrice);
				}
			}
		}
		
		List<StockProductExcel> addlist = new ArrayList<StockProductExcel>();
		if( null != list && list.size() > 0){
			for( StockProductEntity sp : list ){
				StockProductExcel excel = new StockProductExcel();
				try{
					excel.setProductid_code(sp.getProductid().getCode());
				}catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setProductid_name(sp.getProductid().getName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setStockid_stockname(sp.getStockid().getStockname());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setProductid_brandid_brandname(sp.getProductid().getBrandid().getBrandname());
				} catch (Exception e) {
					// TODO: handle exception
				}
				StringBuffer unitinfo = new StringBuffer();
//				try {
//					if( null != sp.getProductid().getUnitassist() && null != sp.getProductid().getPrice() ){
//						unitinfo.append(1).append(sp.getProductid().getUnitassist().getName()).append("=").append(sp.getProductid().getPrice()).append(sp.getProductid().getUnitbase().getName());
//					}else{
//						unitinfo.append(sp.getProductid().getUnitbase().getName());
//					}
//				
//					excel.setUnitinfo(unitinfo.toString());
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
				
//				StringBuffer stocknuminfo = new StringBuffer();
//				try{
//					if( null != sp.getProductid().getUnitassist() && null != sp.getProductid().getPrice() ){
//						BigDecimal assistnum = sp.getStocknum().divide(sp.getProductid().getPrice(), 4, BigDecimal.ROUND_HALF_UP);
//						stocknuminfo.append(sp.getStocknum()).append(sp.getProductid().getUnitbase().getName()).append("(合计").append(assistnum).append(sp.getProductid().getUnitassist().getName()).append(")");
//					}else{
//						stocknuminfo.append(sp.getStocknum()).append(sp.getProductid().getUnitbase().getName());
//					}
//					excel.setStocknuminfo(stocknuminfo.toString());
//				}catch (Exception e) {
//					// TODO: handle exception
//				}
				
				try {
					excel.setTotalprice(sp.getTotalprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setAverageprice(sp.getAverageprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
//				try{
//					excel.setBaseunit(sp.getProductid().getUnitbase().getName());
//				}catch (Exception e) {
//					// TODO: handle exception
//				}
				
				try {
					excel.setBaseunit_num(sp.getStocknum().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setProductid_specifications(sp.getProductid().getSpecifications());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				addlist.add(excel);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "库存状态";
		getResponse(request,response,fileName);
		excel("outputStockProductInfo",response,new TemplateExportParams("export/template/output/stockProductInfoExcel.xls"),StockProductExcel.class,dataList,map);
		
		
		
	}
	
	/**
	 * 导出资金明细
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputCheckDetailExcel")
	public void  outputCheckDetailExcel( StockCheckDetailEntity stockCheckProduct ,HttpServletRequest request,HttpServletResponse response) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		String checkorderid = request.getParameter("orderid");
		CriteriaQuery cq = new CriteriaQuery(StockCheckDetailEntity.class);
		if( StringUtil.isNotEmpty(checkorderid)){
			cq.eq("checkorderid.id", checkorderid);
		}
		
		if( null!= stockCheckProduct.getStockproductid() ){
			if( null != stockCheckProduct.getStockproductid().getProductid() && StringUtil.isNotEmpty(stockCheckProduct.getStockproductid().getProductid().getName())){
				cq.createAlias("stockproductid", "stockproduct");
				cq.createAlias("stockproduct.productid", "product");
				cq.add(Restrictions.like("product.name", "%"+stockCheckProduct.getStockproductid().getProductid().getName()+"%"));
				stockCheckProduct.getStockproductid().getProductid().setName(null);
			}
			
			if( null != stockCheckProduct.getStockproductid().getProductid() ){
				stockCheckProduct.getStockproductid().getProductid().setIsDelete(null);
				stockCheckProduct.getStockproductid().getProductid().setNotsale(null);
			}
			
			
		}
		
		if( null!=stockCheckProduct.getCheckorderid() ){
			if( null!=stockCheckProduct.getCheckorderid().getOperator() && StringUtil.isNotEmpty(stockCheckProduct.getCheckorderid().getOperator().getRealname()) ){
				cq.createAlias("checkorderid.operator", "oper");
				cq.add(Restrictions.like("oper.realname", "%"+stockCheckProduct.getCheckorderid().getOperator().getRealname()+"%"));
				stockCheckProduct.getCheckorderid().getOperator().setRealname(null);
			}
			
			if( null != stockCheckProduct.getCheckorderid().getStockid() && StringUtil.isNotEmpty( stockCheckProduct.getCheckorderid().getStockid().getId())){
				cq.createAlias("checkorderid.stockid", "stock");
				cq.add(Restrictions.eq("stock.id", stockCheckProduct.getCheckorderid().getStockid().getId()));
			}
			
			stockCheckProduct.getCheckorderid().setCompanyid(user.getCompany());
			cq.createAlias("checkorderid.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
			if( StringUtil.isEmpty(stockCheckProduct.getCheckorderid().getId()) ){
				cq.createAlias("checkorderid", "check");
				cq.add(Restrictions.eq("check.status", "2"));
				//cq.eq("checkorderid.status", "2");
			}
		}else{
			stockCheckProduct.setCheckorderid(new StockCheckEntity());
			stockCheckProduct.getCheckorderid().setCompanyid(user.getCompany());
			cq.createAlias("checkorderid.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
			if( StringUtil.isEmpty(stockCheckProduct.getCheckorderid().getId()) ){
				cq.createAlias("checkorderid", "check");
				cq.add(Restrictions.eq("check.status", "2"));
				//cq.eq("checkorderid.status", "2");
			}
		}
		
		//时间
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("updatetime", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("updatetime", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("updatetime", DateUtils.str2Date(createdate2,sdf));
		}
		cq.addOrder("updatetime", SortDirection.asc);
		
		List<StockCheckDetailEntity> list = systemService.getListByCriteriaQuery(cq, false);
		List<StockCheckDetailExcel> excelList = new ArrayList<StockCheckDetailExcel>();
		if( null != list && list.size() > 0 ){
			for( StockCheckDetailEntity check : list ){
				StockCheckDetailExcel excel = new StockCheckDetailExcel();
				try{
					excel.setCheckorderid_name(check.getCheckorderid().getName());
				}catch (Exception e) {
				}
				try{
					excel.setCheckorderid_stockid_stockname(check.getCheckorderid().getStockid().getStockname());
				}catch (Exception e) {
				}
				try{
					excel.setCheckorderid_operator_realname(check.getCheckorderid().getOperator().getRealname());
				}catch (Exception e) {
				}
				try {
					excel.setStockproductid_productid_name(check.getStockproductid().getProductid().getName());
				} catch (Exception e) {
				}
				try {
					excel.setCheckstocknum(check.getCheckstocknum().toPlainString());
				} catch (Exception e) {
				}
				try {
					excel.setCurrentstocknum(check.getCurrentstocknum().toPlainString());
				} catch (Exception e) {
				}
				try {
					BigDecimal zero = new BigDecimal(0.00);
					excel.setAdjuststocknum(check.getAdjuststocknum().toPlainString());
				} catch (Exception e) {
				}
				try{
					excel.setUnitid_name(check.getUnitid().getName());
				}catch (Exception e) {
				}
				try {
					excel.setAverageunitprice(check.getAvgPrice().toPlainString());
				} catch (Exception e) {
				}
				try {
					BigDecimal zero = new BigDecimal(0.00);
					if( check.getCheckstocknum().compareTo(check.getCurrentstocknum()) >= 0 ){
						excel.setTotalprice((check.getAdjuststocknum().multiply(check.getAvgPrice())).toPlainString());
					}else{
						excel.setTotalprice((check.getAdjuststocknum().multiply(check.getAvgPrice())).toPlainString());
					}
				} catch (Exception e) {
				}
				try {
					excel.setUpdatetime(DateUtils.date3Str(check.getUpdatetime(), "yyyy-MM-dd HH:mm:ss"));
				} catch (Exception e) {
				}
				
				excelList.add(excel);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(excelList);

		String fileName  = "盘点明细单数据"+DateUtils.getDate("yyyyMMddHHmmss");
		getResponse(request,response,fileName);
		excel("outputCheckDetailExcel",response,new TemplateExportParams("export/template/output/CheckDetailExcel.xls"),StockCheckDetailExcel.class,dataList,map);
		
		
	}

	/**
	 * 导出库存状态明细
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputStockProductDetailInfo")
	public void  outputStockProductDetailInfo( StockProductEntity stockProduct ,HttpServletRequest request,HttpServletResponse response) {
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
								
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
		
		CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class);
		cq.eq("companyid", user.getCompany());
		
		if( null != stockProduct.getProductid()){
			stockProduct.getProductid().setIsDelete(null);
			stockProduct.getProductid().setNotsale(null);
		}
		
		if( null != stockProduct.getProductid() ){
			cq.createAlias("productid", "product");
			if( StringUtil.isNotEmpty( stockProduct.getProductid().getName()) ){
				cq.add(Restrictions.like("product.name", "%"+stockProduct.getProductid().getName()+"%"));
				stockProduct.getProductid().setName(null);
			}
			
			if( null != stockProduct.getProductid().getClassifyid() && StringUtil.isNotEmpty(stockProduct.getProductid().getClassifyid().getId())){
				cq.createAlias("product.classifyid", "classify");
				cq.add(Restrictions.eq("classify.id", stockProduct.getProductid().getClassifyid().getId()));
			}
		}
		
		if( StringUtil.isNotEmpty(stockProduct.getStatus())){
			cq.eq("status", stockProduct.getStatus());
		}
		
		if(  null != stockProduct.getStockid() && StringUtil.isNotEmpty(stockProduct.getStockid().getId())){
			cq.eq("stockid", stockProduct.getStockid());
		}
		
		cq.add();
		
		List<StockProductEntity> list = stockProductService.getListByCriteriaQuery(cq, false);
		
		List<StockProductDetailExcel> addlist = new ArrayList<StockProductDetailExcel>();
		if( null!=list && list.size()>0){
			for( StockProductEntity sp :list ){
				StockProductDetailExcel excel = new StockProductDetailExcel();
				try{
					excel.setProductid_code(sp.getProductid().getCode());
				}catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setProductid_name(sp.getProductid().getName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setStockid_stockname(sp.getStockid().getStockname());
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					excel.setProductid_brandid_brandname(sp.getProductid().getBrandid().getBrandname());
				} catch (Exception e) {
					// TODO: handle exception
				}
//				StringBuffer unitinfo = new StringBuffer();
//				try {
//					if( null != sp.getProductid().getUnitassist() && null != sp.getProductid().getPrice() ){
//						unitinfo.append(1).append(sp.getProductid().getUnitassist().getName()).append("=").append(sp.getProductid().getPrice()).append(sp.getProductid().getUnitbase().getName());
//					}else{
//						unitinfo.append(sp.getProductid().getUnitbase().getName());
//					}
//				
//					excel.setUnitinfo(unitinfo.toString());
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
				
				StringBuffer stocknuminfo = new StringBuffer();
				try{
//					if( null != sp.getProductid().getUnitassist() && null != sp.getProductid().getPrice() ){
//						BigDecimal assistnum = sp.getStocknum().divide(sp.getProductid().getPrice(), 4, BigDecimal.ROUND_HALF_UP);
//						
//						
//						stocknuminfo.append(sp.getStocknum()).append(sp.getProductid().getUnitbase().getName()).append("(合计").append(assistnum).append(sp.getProductid().getUnitassist().getName()).append(")");
//					}else{
//						stocknuminfo.append(sp.getStocknum()).append(sp.getProductid().getUnitbase().getName());
//					}
//					excel.setStocknuminfo(stocknuminfo.toString());
				}catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setTotalprice(sp.getTotalprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setAverageprice(sp.getAverageprice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setStocksegment(sp.getStockSegment().getFullpathname());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					if( null != sp.getAlertUnit() ){
						String alertinfo = sp.getAlertnum() + sp.getAlertUnit().getName();
						excel.setAlertinfo(alertinfo);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try{
					if( null != sp.getAlertUpperUnit() ){
						String alertupperinfo = sp.getAlertUpperNum() + sp.getAlertUpperUnit().getName();
						excel.setAlertupperinfo(alertupperinfo);
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					if( "1".equals(sp.getStatus()) ){
						excel.setStatus("充足");
					}else if( "2".equals(sp.getStatus()) ){
						excel.setStatus("缺货");
					}else if( "3".equals(sp.getStatus()) ){
						excel.setStatus("补货");
					}else if( "4".equals(sp.getStatus()) ){
						excel.setStatus("库存过高");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setProductid_specifications(sp.getProductid().getSpecifications());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
//				try {
//					excel.setBaseunit(sp.getProductid().getUnitbase().getName());
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
				
				try {
					excel.setBaseunit_num(sp.getStocknum().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setAll_totalprice(sp.getStockProductPrice().getTotalPrice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setAll_averageprice(sp.getStockProductPrice().getAveragePrice().toPlainString());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				addlist.add(excel);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "库存状态明细";
		getResponse(request,response,fileName);
		excel("outputStockProductDetailInfo",response,new TemplateExportParams("export/template/output/stockProductDetailInfoExcel.xls"),StockProductDetailExcel.class,dataList,map);
		
		
		
	}
	
	/**
	 * 导入产品、物流模板信息
	 *
	 * @return
	 */
	@RequestMapping(value = "uploadProductFreightExcel")
	public ModelAndView uploadProductFreightExcel(HttpServletRequest req) {
		
		return new ModelAndView("admin/product/uploadProductFreightExcel");
		
	}
	
	/**
	 * 导入产品、物流模板信息
	 *
	 * @return
	 */

	@RequestMapping(value = "uploadProductFreight")
	@ResponseBody
	public AjaxJson impProductFreight(HttpServletRequest request, HttpServletResponse response) {
		//当前用户信息
		TSUser user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
			   user = systemService.getEntity(TSUser.class, user.getId());
		AjaxJson j = new AjaxJson();
		String message = "导入成功！";
		if( ( user.getCompany() != null && StringUtil.isNotEmpty(user.getCompany().getId()) ) &&  ("1".equals(user.getType()) ||  "2".equals(user.getType())) ){

			ImportParams params = new ImportParams();
				/**
				 * 名称不能为空
				 */
				params.setBeginRows(3);
				params.setNeedSave(false);
				params.setSaveUrl("upload/excelUpload/impProductFreight");
				params.setSheetNum(1);
			final List<ProductFreightVO> list = importExcel(j,params,ProductFreightVO.class,request,response,true);
				params.setSheetNum(2);
			final List<FreightVO> listF = importExcel(j,params,FreightVO.class,request,response,true);
			
			//运费模板数据转换为map
			Map<String, FreightVO> mapF = new HashMap<String, FreightVO>();
			if( listF != null && listF.size() > 0 ){
				for( FreightVO f : listF){
					mapF.put(f.getCode().toString(), f);
				}
			}
			
			List<ProductEntity> addlProductList = new ArrayList<ProductEntity>();//创建的产品信息
			
			List<ProductEntity> upProductList = new ArrayList<ProductEntity>();//更新产品系统
			List<FreightEntity> upFreightList = new ArrayList<FreightEntity>();//更新的物流模板信息
			

			for(ProductFreightVO p:list){
					
					boolean add = true ;
					if(StringUtils.isEmpty(p.getName())){
						//产品名称为空
						add = false ;
					}else if(StringUtils.isEmpty(p.getSpecifications())){
						//产品规格为空
						add = false ;
					}else if(StringUtils.isEmpty(p.getCode())){
						//产品编号为空
						add = false ;
					}
					
					if(add){
						//是否存在对应的产品
						CriteriaQuery cq = new CriteriaQuery(ProductEntity.class);
							cq.eq("code", p.getCode());//产品编码作为唯一编码
							cq.eq("company", user.getCompany());
						List<ProductEntity> productlist = systemService.getListByCriteriaQuery(cq,false);
						
						FreightVO  f =(FreightVO) mapF.get(p.getCode().toString());
						
						if( productlist != null && productlist.size() > 0){//更新对应的产品信息、模板信息
							for( int index = 0 ; index <productlist.size() ; index++ ){//是否更新产品价格
								ProductEntity up  =productlist.get(index);
								
								//直接更新价格信息
								up.setSpecifications(replaceBlank(p.getSpecifications()));
									up.setName(replaceBlank(p.getName()));
									up.setPrice(p.getPrice());
									up.setPricec(p.getPricec());
									up.setPrices(p.getPrices());
									up.setPriceyw(p.getPriceyw());
									up.setUpdatedate(DateUtils.getDate());

								upProductList.add(up);
								
//								FreightEntity  freightUp = null;
//								try {
//									freightUp = systemService.getEntity(FreightEntity.class,up.getFreightid().getId());
//								} catch (Exception e) {
//									
//								}		
//										
//								if(f != null && freightUp != null  ){//更新物流模板数据
//									try {
//										MyBeanUtils.copyBeanNotNull2Bean(f , freightUp);
//										freightUp.setName(up.getName()+up.getSpecifications()+f.getFreightName());
//											upFreightList.add(freightUp);
//									} catch (Exception e) {
//										
//									}
//								}else if( f != null ){//创建新的物流模板数据
//									FreightEntity  freightsave =  new FreightEntity();//创建对应的模板
//									try {
//										MyBeanUtils.copyBeanNotNull2Bean(f , freightsave);
//											freightsave.setName(p.getName()+p.getSpecifications()+f.getFreightName());
//											freightsave.setFreightname(f.getFreightName());
//											freightsave.setState("1");
//											freightsave.setType("1");
//
//										Serializable id = systemService.save(freightsave);
//											freightsave.setId((String) id);
//										up.setFreightid(freightsave);
//									} catch (Exception e) {
//										
//									}
//								}	
							}
						}else {//创建对应的产品信息、模板信息

							//FreightEntity  freightsave =  new FreightEntity();//创建对应的模板
							ProductEntity  Productsave = new ProductEntity();//产品信息
//							try {
//								MyBeanUtils.copyBeanNotNull2Bean(f , freightsave);
//								freightsave.setName(p.getName()+p.getSpecifications()+f.getFreightName());
//								freightsave.setFreightname(f.getFreightName());
//								freightsave.setState("1");
//								freightsave.setType("1");
//								freightsave.setCompany(user.getCompany());
//							Serializable id = systemService.save(freightsave);//先写入模板信息
//								freightsave.setId((String) id);
//								} catch (Exception e) {
//								
//							}
							try{	
							//创建产品信息	
								MyBeanUtils.copyBeanNotNull2Bean( p , Productsave);
//								if(freightsave != null && StringUtils.isNotEmpty( freightsave.getId() ) ){
//									Productsave.setFreightid(freightsave);
//								}
								Productsave.setUpdatedate(DateUtils.getDate());
								Productsave.setCompany(user.getCompany());
							addlProductList.add(Productsave);
								
							} catch (Exception e) {
								
							}
						}
					}
				}
			systemService.batchSave(addlProductList);
			systemService.batchUpdate(upProductList);
			systemService.batchUpdate(upFreightList);

			//写入日记记录
			
			message +="创建"+addlProductList.size()+"件！更新"+upProductList.size()+"件！物流信息"+upFreightList.size();
		}else {
			message ="更新失败，更新账户不匹配！";
		}
		j.setMsg(message);
			return j;

	}
	
	
	/**
	 * 导出购货单数据
	 *
	 * @return
	 */
	@RequestMapping(value = "shoppingproductOut" )
	
	public ModelAndView shoppingproductOut(HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
			String id = req.getParameter("id");
		if( StringUtil.isNotEmpty(id)){
			ProductEntity t = systemService.get(ProductEntity.class, id);
			req.setAttribute("productPage", t);
		}	
		//采购商信息
		List<TSUser> buyerUserList = new ArrayList<TSUser>();
		if(null != userStaffService.getBuyerUserBySalesMan(user)){
			buyerUserList = userStaffService.getBuyerUserBySalesMan(user);
		}
		req.setAttribute("userClientList", buyerUserList);
		req.setAttribute("user", user);
		
		return new ModelAndView("shopping/shoppingproductOut");	
	}
	

	/**
	 * 导出购货单数据
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "productOutExcel" )
	public void  productOutExcel(HttpServletRequest request,HttpServletResponse response) {
			
			String idS = request.getParameter("id");
			
			String numS = request.getParameter("num");
			
			String buyerid = request.getParameter("buyerUserId");//代客下单对应的采购商
		TSUser user =  ResourceUtil.getSessionUserName();	
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
				user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
			    
		}
			user = systemService.getEntity(TSUser.class, user.getId()); 
		List<XiadanProductVO> addlist = new ArrayList<XiadanProductVO>();	
			
		if( StringUtil.isNotEmpty(idS) ){
			String[] idL = idS.split(",");
			String[] numL = numS.split(",");
			
			for(int i =0 ;i < idL.length ; i ++ ){
				String id  =idL[i];
				int num = 0;
				try {
					num = Integer.parseInt(numL[i]);
				} catch (Exception e) {
					
				}
				
				ProductEntity t = new ProductEntity();
					
			if( StringUtil.isNotEmpty(id) && num > 0 ){
				
				
				t = systemService.get(ProductEntity.class, id);
				for(int k = 0 ; k < num ; k++){
					if(  user.getCompany().getId() .equals(t.getCompany().getId())){
						
						XiadanProductVO xiadanProduct = new XiadanProductVO();
						xiadanProduct.setCreatedate(DateUtils.getDate("yyyy/MM/dd"));
						xiadanProduct.setProductname(t.getName());
						xiadanProduct.setNumber(1);
						xiadanProduct.setSpecifications(t.getSpecifications());
						xiadanProduct.setPrice(t.getPrice());
						xiadanProduct.setCode(t.getCode());
						
						if(StringUtil.isNotEmpty(buyerid)){
							xiadanProduct.setUserBuyerId(buyerid);
						}
						
						addlist.add(xiadanProduct);
					}
				}
			}	
		  }	
		}
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		dataList.add(addlist);
		
		String fileName  = DateUtils.getDate("yyyyMMdd")+"采购单";
		getResponse(request,response,fileName);
		excel("productOutExcel",response,new TemplateExportParams("export/template/output/XiadanProductVo.xls"),XiadanProductVO.class,dataList,map);
		
	}

	
	
	/**
	 * 导入订单
	 *
	 * @return
	 */
	@RequestMapping(value = "importOrder")
	public ModelAndView importOrder(HttpServletRequest req) {
		
		return new ModelAndView("admin/order/importOrder");
		
	}
	/**
	 * 导入订单数据处理
	 *
	 * @return
	 * 
	 */
	@RequestMapping(value = "importOrderExcel", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importOrderExcel(HttpServletRequest request, HttpServletResponse response) {
		//final String importMonth = request.getParameter("month");
		
		AjaxJson j = new AjaxJson();
		String message = "导入多单成功！请到支付管理进行支付！";
		
		ImportParams params = new ImportParams();
			/**
			 * 名称不能为空
			 */		
			params.setBeginRows(2);
			params.setNeedSave(false);
			TSUser u = ResourceUtil.getSessionUserName();
			//u = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
			   u = systemService.getEntity(TSUser.class, u.getId());
		final List<XiadanProductVO> list = importExcel(j,params,XiadanProductVO.class,request,response,true);
		
		try {
			j = orderService.orderGeneratorJson(request, list);
		} catch (Exception e) {
			message = "导入多单失败！请到核对订单字段再次提交";
		}
		j.setMsg(message);
//		List<OrderEntity> addlist = new ArrayList<OrderEntity>();//对应的订单数据
//		
//		
//		 Map<String, Integer > orderNumber = new HashMap<String, Integer>();
//		
//		
//		
//			for(XiadanProductVO e:list){
//				boolean add = true ;				
//				if(StringUtils.isEmpty(e.getAddress())){
//					//收货地址为空
//					add = false ;
//				}else 
//				if(StringUtils.isEmpty(e.getCustomername())){
//					//收货姓名为空
//					add = false ;
//				}else 
//				if(StringUtils.isEmpty(e.getTelephone())){
//					//收货联系方式
//					add = false ;
//				}else 
//				if(StringUtils.isEmpty(e.getProductname())){
//					//产品名称为空
//					add = false ;
//				}else 
//				if(e.getNumber() < 1){
//					//产品件数不匹配
//					add = false ;
//				}else 
//				if(StringUtils.isEmpty(e.getCode())){
//						//产品信息不存在
//						add = false ;
//					}
//				if(add){
//					e.setAddress(replaceBlank(e.getAddress()));//过滤地址自带换行符
//					e.setCustomername(replaceBlank(e.getCustomername()));
//					e.setTelephone(replaceBlank(e.getTelephone()));
//					ProductEntity p = null;
//					CriteriaQuery cq = new CriteriaQuery(ProductEntity.class);
//						cq.eq("code", e.getCode());
//						cq.eq("company", u.getCompany());
//						cq.add();
//					List<ProductEntity> productlist = systemService.getListByCriteriaQuery(cq,false);
//					if( productlist != null && productlist.size() > 0){
//						p = productlist.get(0);
//					}
//					
//					//产品名称
//					if (  (p != null && StringUtil.isNotEmpty(p.getId())) ) {
//							e.setSpecifications(p.getSpecifications());
//							//e.setPrice(p.getPrice());//销售价格需要重新计算
//							e.setCreatedate(null);
//					//创建对应的订单数据
//					OrderEntity o = new OrderEntity();
//					String province ="";//对于省份信息
//						try {
//							
//							MyBeanUtils.copyBeanNotNull2Bean(e , o);
//							BigDecimal freightMoney = new BigDecimal("0.00"); //运费价格
//							try{
//								freightMoney =p.getFreightid().FreighToMap().get(o.getAddress().substring(0, 2));
//								province = p.getFreightid().territoryToMap().get(o.getAddress().substring(0, 2)); 
//								if( freightMoney == null){
//									freightMoney = new BigDecimal("0.00");
//								}
//							}catch (Exception ex) {
//								freightMoney = new BigDecimal("0.00");
//							}
//							
//							//统计同类产品的数量，方便计算阶梯价格
//							if( orderNumber.containsKey(p.getId()) ){
//								int t = orderNumber.get(p.getId()) +o.getNumber() ;
//								orderNumber.put(p.getId(), t);
//							}else {
//								int t = o.getNumber() ;
//								orderNumber.put(p.getId(), t);
//							}
//							
//							
//						    freightMoney=freightMoney.multiply(new BigDecimal(e.getNumber()));
//						    o.setProductid(p);
//							o.setFreightpic(freightMoney);
//							o.setState("1");
//							o.setPlatform(u.getRealname());
//							o.setCreatedate(DateUtils.getDate());
//							o.setProvince(province);
//							o.setCompany(u.getCompany());
//								addlist.add(o);
//						} catch (Exception e1) {
//							
//						}		
//						}
//					}
//
//			}
//			if(addlist != null && addlist.size() > 0 ){
//				BigDecimal money = new BigDecimal("0.00"); //订单结算总价
//				PayBillsOrderEntity payBillsOrder = new PayBillsOrderEntity();//创建对应支付信息
//				//根据产品属性  计算产品阶梯价格
//				
//				for(int k = 0 ; k <addlist.size() ; k++ ){
//					OrderEntity o = addlist.get(k);
//					ProductEntity product =	o.getProductid();
//					BigDecimal productpice =product.getPrice().multiply(new BigDecimal(o.getNumber()));//产品预订价，单价X件数
//					BigDecimal productpiceYw =product.getPriceyw().multiply(new BigDecimal(o.getNumber()));//产品预订价，单价X件数
//					
//					if( orderNumber.containsKey(product.getId()) && product.getProductLadderList() != null && product.getProductLadderList().size() > 0 ){//存在量价体系
//							int number =  orderNumber.get(product.getId());
//							
//							for(ProductLadderEntity productLadder : product.getProductLadderList() ){
//								if( number >= productLadder.getLadderminorde() ){
//									productpice =productLadder.getPrice().multiply(new BigDecimal(o.getNumber()));//产品预订价，单价X件数
//									productpiceYw =productLadder.getPriceyw().multiply(new BigDecimal(o.getNumber()));//产品预订价，单价X件数
//								}
//							}
//						}
//					o.setPrice(productpice);
//					o.setPriceyw(productpiceYw);
//					money =money.add(o.getFreightpic());
//					money = money.add(productpice);	
//					addlist.set(k,o);//修改对应的元素
//				}
//				
//				
//				payBillsOrder.setIdentifier(IdWorker.generateSequenceNo());
//				payBillsOrder.setCompany(u.getCompany());
//				payBillsOrder.setClientid(u);
//				payBillsOrder.setYewu(u.getTsuserclients().getYewuid());
//				payBillsOrder.setType("1");//销售类型流水
//				payBillsOrder.setState("1");//待支付
//				payBillsOrder.setFreightstatus("1");//未发货
//				payBillsOrder.setPayablemoney(money);
//				payBillsOrder.setAlreadypaidmoney(new BigDecimal("0.00"));
//				payBillsOrder.setObligationsmoney(money);
//					message =DateUtils.getDate("yyyy-MM-dd HH:mm:ss")+"导入成功,导入"+list.size()+"条订单信息,成功"+addlist.size()+"条！";
//						Serializable id = systemService.save(payBillsOrder);
//						payBillsOrder.setId(id.toString());
//					
//					
//				for(int k = 0 ; k <addlist.size() ; k++ ){
//						OrderEntity o = addlist.get(k);
//							o.setBillsorderid(payBillsOrder);
//							o.setIdentifieror(payBillsOrder.getIdentifier()+"_"+(k+1));
//						addlist.set(k,o);//修改对应的元素
//				}
//				systemService.batchSave(addlist);//订单数据写入数据库
//			}
//			

		return j;
	}
	
	
	/**
	 * 导出发货数据处理
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputOrderlogisticsExcel")
	public void  outputOrderlogisticsExcel(HttpServletRequest request,HttpServletResponse response) {

		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();	
		List<OrderEntity> list = new ArrayList<OrderEntity>();
		if("6".equals(user.getType())){//如果是第三方发货方
			String identifieid = request.getParameter("order.identifieror");
			String productname = request.getParameter("order.productname");
			String customername = request.getParameter("order.customername");
			String telephone = request.getParameter("order.telephone");
			String address = request.getParameter("order.address");
			String sendtype = request.getParameter("order.sendtype");
			String status = request.getParameter("ordertype");
			if(StringUtils.isEmpty(status)){
				status = request.getParameter("order.state");
			}
			
			CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class);
			cq.createAlias("orderid", "order");
			
			cq.eq("status", "1");
			cq.eq("receiver", user);
			//查询条件组装器
			if(StringUtil.isNotEmpty(identifieid) ){
				cq.add(Restrictions.like("order.identifieror", "%"+ identifieid +"%"));
			}
			if(StringUtil.isNotEmpty(productname) ){
				cq.add(Restrictions.like("order.productname", "%"+ productname +"%"));
			}
			if(StringUtil.isNotEmpty(customername) ){
				cq.add(Restrictions.like("order.customername", "%"+ customername +"%"));
			}
			if(StringUtil.isNotEmpty(telephone) ){
				cq.add(Restrictions.like("order.telephone", "%"+ telephone +"%"));
			}
			if(StringUtil.isNotEmpty(address) ){
				cq.add(Restrictions.like("order.address", "%"+ address +"%"));
			}
			if( StringUtil.isNotEmpty(sendtype)){
				cq.add(Restrictions.eq("order.sendtype", sendtype));
			}
			if(StringUtil.isNotEmpty(status) ){
				String[] orders= status.split(",");
				if( orders.length > 0 ){
					cq.add(Restrictions.in("order.state", orders));
				}
			}
			cq.in("status", "1,2".split(","));
			
			String createdate1 = request.getParameter("createdate1");
			String createdate2 = request.getParameter("createdate2");
			String createdate3 = request.getParameter("createdate3");
			String createdate4 = request.getParameter("createdate4");
			//派单时间
			SimpleDateFormat sdf1 = new SimpleDateFormat ("yyyy-MM-dd"); 
			if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
				cq.add(Restrictions.between("createtime", DateUtils.str2Date(createdate1,sdf1), DateUtils.str2Date(createdate2,sdf1)));
			}else if( StringUtil.isNotEmpty(createdate1) ){
				cq.add(Restrictions.ge("createtime", DateUtils.str2Date(createdate1,sdf1)));
			}else if( StringUtil.isNotEmpty(createdate2) ){
				cq.add(Restrictions.le("createtime", DateUtils.str2Date(createdate2,sdf1)));
			}
			//下单时间
			SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
			if(StringUtil.isNotEmpty(createdate3)&&StringUtil.isNotEmpty(createdate4)){
				cq.add(Restrictions.between("order.createdate", DateUtils.str2Date(createdate3,sdf), DateUtils.str2Date(createdate4,sdf)));
			}else if( StringUtil.isNotEmpty(createdate3) ){
				cq.add(Restrictions.ge("order.createdate", DateUtils.str2Date(createdate3,sdf)));
			}else if( StringUtil.isNotEmpty(createdate4) ){
				cq.add(Restrictions.le("order.createdate", DateUtils.str2Date(createdate4,sdf)));
			}
			cq.add();
			
			List<OrderSendEntity> orderSendList = systemService.getListByCriteriaQuery(cq,false);
			
			if(null != orderSendList && orderSendList.size() > 0){
				for (OrderSendEntity orderSendEntity : orderSendList) {
					
					OrderEntity order = orderSendEntity.getOrderid();
					if("2".equals(order.getState())){
						list.add(orderSendEntity.getOrderid());
					}
					
				}
			}
			
		}else{
			CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
			cq.createAlias("freightState", "freightState");
			cq.eq("freightState.dictionaryValue", "2");
			cq.eq("company", user.getCompany());
			list = systemService.getListByCriteriaQuery(cq,false);
		}
		
		List<FahuoProductVO> addlist = new ArrayList<FahuoProductVO>();
		if( list != null && list.size() > 0 ){
			for(OrderEntity o : list){
				FahuoProductVO p = new FahuoProductVO();
				try {
					MyBeanUtils.copyBeanNotNull2Bean(o , p);
					
					p.setSpecifications(o.getProductid().getSpecifications());
					p.setLogisticscompany("");
					p.setLogisticsnumber("");
					p.setCreatedate(DateUtils.date3Str(o.getCreatedate(), "yyyy-MM-dd HH:mm:ss" ));
					p.setOrderId(o.getId());
					
					try {
						p.setFreightpic(freightDetailsService.getProductFreightByFullAddr(o.getProductid(), o.getAddress(), o.getNumber().intValue()));
					} catch (Exception e) {
						log.error("导出发货数据运费获取失败，详单号：" +o.getId() );
					}
					
					
					addlist.add(p);
				} catch (Exception e1) {
					log.error(e1);
				}
			}
		}
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		dataList.add(addlist);

		String fileName  = DateUtils.getDate("yyyyMMdd")+"发货数据";
		getResponse(request,response,fileName);
		excel("outputOrderlogisticsExcel",response,new TemplateExportParams("export/template/output/FahuoProductVo.xls"),FahuoProductVO.class,dataList,map);

	}
	
	
	/**
	 * 导入订单物流数据
	 *
	 * @return
	 */
	@RequestMapping(value = "importDeliverXls")
	public ModelAndView importOrderXls(HttpServletRequest req) {
		
		return new ModelAndView("admin/fahuo/importDeliverXls");
		
	}
	
	/**
	 * 导入订单物流数据处理
	 *
	 * @return
	 */

	@RequestMapping(value = "importOrder", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importOrder(HttpServletRequest request, HttpServletResponse response) {
		
		AjaxJson result = new AjaxJson();
		String message = "导入成功！";
		
		ImportParams params = new ImportParams();
			/**
			 * 名称不能为空
			 */
			params.setBeginRows(2);
			params.setNeedSave(false);
			params.setSaveUrl("upload/excelUpload/wuliu");
		TSUser u = ResourceUtil.getSessionUserName();	
		
		final List<FahuoProductVO> list = importExcel(result,params,FahuoProductVO.class,request,response,true);
		//添加对应的订单数据
		List<OrderEntity> addlist = new ArrayList<OrderEntity>();
		//物理订单
		List<OrderLogisticsEntity> orderLogisticsList = new ArrayList<OrderLogisticsEntity>();
		//涉及的大单
		List<PayBillsOrderEntity> paybillslist = new ArrayList<PayBillsOrderEntity>();
		//涉及第三方的发货记录
		List<CopartnerOrderEntity> copartnerOrderList = new ArrayList<CopartnerOrderEntity>();
		//涉及第三方的发货记录
		List<OrderSendEntity> orderSendList = new ArrayList<OrderSendEntity>();
		
		/**结算总额*/
		BigDecimal money = new BigDecimal("0.00");
		
		//定义发送对于订单的公司map
		
			for(FahuoProductVO p:list){
				
				boolean add = true ;
				if(StringUtils.isEmpty(p.getOrderId())){
					//id为空
					add = false ;
				}
				if(StringUtils.isEmpty(p.getLogisticsnumber())){
					//快递单号
					add = false ;
				}
				if(StringUtils.isEmpty(p.getLogisticscompany())){
					//快递公司
					add = false ;
				}
				
				if(add){//只更新对应的物流数据，根据唯一id找到对应的数据
					OrderEntity t = systemService.getEntity(OrderEntity.class , p.getOrderId() );
					if(t.getFreightState() == null){
						result.setSuccess(false);
						result.setMsg("订单号：" + t.getIdentifieror() + "信息有误，请联系管理员处理");
						return result;
					}
					
					if( t != null && t.getFreightState().getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_DELIVER.getValue())){
						t.setLogisticscompany(replaceBlank (p.getLogisticscompany()));
						t.setLogisticsnumber( replaceBlank (p.getLogisticsnumber().replaceAll("，", ",")));
						t.setDeparturedate(DateUtils.getDate());
						TSDictionary freightState = dictionaryService.checkDictionaryItem("freightStates", "3");
						t.setFreightState(freightState);
//						t.setState("3");
						//t.setBillsorderid(null);
						//创建物流订单号信息列表
					    String logisticsnumberS[] = p.getLogisticsnumber().trim().split(",");
					    
						if( logisticsnumberS.length > 0 ){
							OrderEntity ot = new OrderEntity();
								ot.setId(t.getId());
							for(int i=0 ;i <logisticsnumberS.length ; i++){
								OrderLogisticsEntity ol = new OrderLogisticsEntity();
								//ol.setOrderid(ot);
								ol.setOrderid(t);
								ol.setLogisticscompany(p.getLogisticscompany());
								ol.setLogisticsnumber(logisticsnumberS[i]);
								ol.setDeparturedate(DateUtils.getDate());
							 orderLogisticsList.add(ol);
							}
							/*if(orderLogisticsList.size() > 0){
								systemService.batchSave(orderLogisticsList);
							}*/
						}
						addlist.add(t);
						//发货通知下单客户
						messageTemplateService.mosSendSmsDelivery_6(t);
						//发货通知收货人
						messageTemplateService.mosSendSmsDelivery_5(t);
					}
					//计算对应的提现金额	
				}
			}
			if(addlist != null && addlist.size() > 0 ){
//				CashEntity c = systemService.findUniqueByProperty(CashEntity.class ,"TSUser.id", tSUser.getId() );
//					c.setMoney(c.getMoney().add(money));
//				systemService.saveOrUpdate(c);	
				//创建一个获取基地获得价格
//				PayBillsEntity payBills = new PayBillsEntity();//创建对应支付信息				
//					payBills.settSUser(u);
//					payBills.setCompany(u.getCompany());
//					payBills.setCreatedate(DateUtils.getDate());
//					payBills.setCreateuser(u.getId());
//					payBills.setIdentifier(IdWorker.generateSequenceNo());
//					payBills.setType("3");
//					payBills.setReceived("3");
//					payBills.setMoney(money);
					message =DateUtils.getDate("yyyy-MM-dd HH:mm:ss")+"导入成功,导入"+list.size()+"条发货信息,成功"+addlist.size()+"条！";
					
//					payBills.setRemark(message+"结算总价："+money);
			
				//同时写入个人账户 冻结款	
//				PersonalAccountEntity t = systemService.findUniqueByProperty (PersonalAccountEntity.class,"company" ,u.getCompany());
//					BigDecimal moneyp = new BigDecimal("0.00"); 
//						moneyp = t.getFrozenmoney().add (money);//结算价格
//					t.setFrozenmoney(moneyp);
				//暂未添加事务处理	
//				systemService.save(payBills);	
//				systemService.saveOrUpdate(t);//更新个人冻结金额
				systemService.batchUpdate(addlist);//更新数据根据id,只更新物流数据
				if(orderLogisticsList.size() > 0){
					systemService.batchSave(orderLogisticsList);
				}
				
				
				//更新大订单发货状态
				try {
					for(OrderEntity o :  addlist){
						
						PayBillsOrderEntity payBills = systemService.findUniqueByProperty(PayBillsOrderEntity.class, "id", o.getBillsorderid().getId());
						payBills.setFreightState(payBillsOrderService.getPayBillsFreightStatus(payBills));
						if(!paybillslist.contains(payBills)){
							paybillslist.add(payBills);
						}
					}
					if(paybillslist.size() > 0){
						systemService.batchUpdate(paybillslist);
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				
				
				//发送支付成功微信服务号信息
				try{
					//BigDecimal moneyp = new BigDecimal("0.00");//基地结算
//					String touser=payBills.getCompany().getWeixin();
//					String toparty="@all";
//					String totag="@all";
//					int agentid=0;
//					String set1= ""+payBills.getMoney();
//					String set0=payBills.getId();
//					messageTemplateService.sendMessageTozhifu(touser, toparty, totag, agentid, set1, set0);
//					
				}catch (Exception e) {
					
				}
				
				//保存发货商相关订单 
				try{
					for(OrderEntity o :  addlist){
						OrderEntity orderdb = systemService.findUniqueByProperty(OrderEntity.class, "id", o.getId());
						CopartnerOrderEntity copartnerOrder = copartnerOrderService.buildCopartnerOrder(orderdb);
						if(null != copartnerOrder){
							copartnerOrderList.add(copartnerOrder);
						}
						
						OrderSendEntity orderSend = orderSendService.getSendOrder(orderdb, request);
						if(null != orderSend){
							orderSend.setStatus("2");//订单被处理更新处理状态
							orderSendList.add(orderSend);
						}
					}
					if(copartnerOrderList.size() > 0){
						systemService.batchSave(copartnerOrderList);
					}
					
					if(orderSendList.size() > 0){
						systemService.batchUpdate(orderSendList);
					}
					//汇总成本价到发货商账户
					userCopartnerAccountService.withdrawCaculater(copartnerOrderList, u);
				}catch (Exception e) {
					log.error(">>>>>>>>>>>update copartner order error : " + e);
				}
				
				
				//发送发货信息
				try{
					//BigDecimal moneyp = new BigDecimal("0.00");//基地结算
//					String touser=payBills.getCompany().getWeixin();
//					String toparty="@all";
//					String totag="@all";
//					int agentid=0;
//					String set1= ""+payBills.getMoney();
//					String set0=payBills.getId();
//					messageTemplateService.sendMessageTozhifu(touser, toparty, totag, agentid, set1, set0);
					
				}catch (Exception e) {
					
				}
			}else{
				message = "抱歉~您的订单不符合发货订单标准，请核查之后再上传！";
				result.setSuccess(false);
			}
			//写入日记记录
			result.setMsg(message);
			return result;

	}
	
	
	/**
	 * 下载产品模板
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "exportProductXls")
	public void  exportProductXls(HttpServletRequest request,HttpServletResponse response) {

		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String fileName  = "产品模板";
		getResponse(request,response,fileName);
		excel("exportProductXlsExcel",response,new TemplateExportParams("export/template/output/produc.xls",2,false),FahuoProductVO.class,dataList,map);

	}
	
	
	
	/**
	 * 导出入库单详情
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputstockIODetailInfos")
	public void  outputstockIODetailInfos( StockIODetailEntity stockIODetailEntity, HttpServletRequest request,HttpServletResponse response) {
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
				
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();	    
		}
		
		String stockorderid = stockIODetailEntity.getStockorderid().getId();
		StockIOEntity stockIOEntity = systemService.getEntity(StockIOEntity.class, stockorderid);
		if( stockIOEntity == null ){
			return;
		}
		
		
		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class);
		cq.eq("stockorderid", stockIODetailEntity.getStockorderid());
		
		List<StockIODetailEntity> list = systemService.getListByCriteriaQuery(cq,false);
		List<StockIODetailExcle> addlist = new ArrayList<StockIODetailExcle>();
		if( list != null && list.size() > 0 ){
			for(StockIODetailEntity s : list){
				StockIODetailExcle excle = new StockIODetailExcle();
				if( null!=s.getProductid()){
					try {
						excle.setProductid_name(s.getProductid().getName());
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				if( null!=s.getProductid() && null!=s.getProductid().getBrandid() ){
					try {
						excle.setProductid_brandid_brandname(s.getProductid().getBrandid().getBrandname());
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				if( null!=s.getProductid()){
					try {
						excle.setProductid_specifications(s.getProductid().getSpecifications());
					}catch (Exception e) {
						
					}
				}
				if( null!=s.getProductid()){
					try {
						excle.setProductid_unit(s.getProductid().getUnitBase().getCode());
					}catch (Exception e) {
						
					}
				}
				excle.setIostocknum(""+s.getIostocknum());
				excle.setUnitprice(""+s.getUnitprice());
				excle.setTotalprice(""+s.getTotalprice());
				addlist.add(excle);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		/**
		 * 出库/入库类型
		 */
		String type = stockIOEntity.getStocktypeid().getDictionaryType();
		String fileName  = stockIOEntity.getIdentifier();
		String title = "";
		String timeType = "";
		if(StockIOType.OUT.equals( type )){
			fileName  += stockIOEntity.getIdentifier()+"出库单详情";
			title =  user.getCompany().getCompanyName()  +  "出库单详情查询表";
			map.put("timeType", "出库时间");
			timeType = "出库时间";
		}else if(StockIOType.IN.equals(type)){
			fileName += stockIOEntity.getIdentifier()+"入库单详情";
			title = user.getCompany().getCompanyName() + "入库单详情查询表";
			timeType = "入库时间";
		}
		
		map.put("add", title);
		map.put("timeType", timeType);
		map.put("stockid", stockIOEntity.getIdentifier() + " ");
		map.put("stockname", stockIOEntity.getStockid().getStockname());
		map.put("operater", stockIOEntity.getOperator().getRealname());
		map.put("stocktype", stockIOEntity.getStocktypeid().getDictionaryName());
		map.put("intime", DateUtils.date2Str(stockIOEntity.getCreatetime(), DateUtils.datetimeFormat));
		map.put("remarks", stockIOEntity.getRemark());
		
		dataList.add(addlist);
		
		getResponse(request,response,fileName);
		excel("outputOrderExcel",response,new TemplateExportParams("export/template/output/StockIODetailExcle.xls"),StockIODetailExcle.class,dataList,map);
		
	}
	
	
	/**
	 * 导出发货区域统计
	 * @param request
	 * @param response
	 * 
	 * */
	@RequestMapping(value = "outputInvoiceExcel")
	public void  outputInvoiceExcel( HttpServletRequest request,HttpServletResponse response) {
		String productid =request.getParameter("productid");
		String userCLid =request.getParameter("userCLid");
		
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		String during = startTime + " - " +endTime;
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
								
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();	    
		}
		
		String companyId = user.getCompany().getId();
		String address = "";
		//存在地址信息
		if(  StringUtil.isNotEmpty(user.getCompany().getAddress())){
			address = user.getCompany().getAddress().trim().substring(0,2);
		}
		
		
		List<OrderEntity> salesOrderList  = orderService.salesdeparture( companyId , userCLid , productid,  startTime, endTime );
		List<InvoiceExcel> addlist = new ArrayList<InvoiceExcel>();
		if( salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o:salesOrderList ){
				InvoiceExcel excel = new InvoiceExcel();
				
				
				if( StringUtil.isNotEmpty(o.getProvince()) ){
					o.setProvince(o.getProvince().substring(0,2));
					if("黑龙".equals(o.getProvince())){
						o.setProvince("黑龙江");
					}else if ( "内蒙".equals(o.getProvince()) ){
						o.setProvince("内蒙古");
					}
				}else {
					o.setProvince("自提");
				}
				excel.setProvince(o.getProvince());
				
				excel.setFreightpic(o.getFreightpic().toString());
				excel.setNumber(o.getNumber().toString());
				excel.setPrice(o.getPrice().toString());
				addlist.add(excel);
			}
			
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		map.put("date", during);
		
		dataList.add(addlist);

		String fileName  = "导出发货区域统计查询数据(" + during + ")";
		getResponse(request,response,fileName);
		excel("outputInvoiceExcel",response,new TemplateExportParams("export/template/output/InvoiceExcel.xls"),InvoiceExcel.class,dataList,map);
		
		
	}
	
	/**
	 * 导出业务员佣金统计
	 * @param request
	 * @param response
	 * 
	 * */
	@RequestMapping(value = "outputYeWuExcel")
	public void  outputYeWuExcel( HttpServletRequest request,HttpServletResponse response) {
		String productid = request.getParameter("productid");
		String userYWid =request.getParameter("userYWid");
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		String during = startTime + " - " +endTime;
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
						
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();	    
		}	
		String companyId = user.getCompany().getId();
		List<OrderEntity> salesOrderList  = orderService.salesYewu( userYWid , productid,  startTime, endTime ,companyId  );
		List<YeWuExcel> addlist = new ArrayList<YeWuExcel>();
		if( salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o:salesOrderList ){
				YeWuExcel excel = new YeWuExcel();
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				if( null != o.getCreatedate() ){
					String createdate = formatter.format(o.getCreatedate());
					excel.setCreatedate(createdate);
				}
				
				excel.setPriceyw(o.getPriceyw().toString());
				excel.setPrice(o.getPrice().toString());
				addlist.add(excel);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		map.put("date", during);
		
		dataList.add(addlist);

		String fileName  = "导出业务员佣金统计查询数据(" + during + ")";
		getResponse(request,response,fileName);
		excel("outputYeWuExcel",response,new TemplateExportParams("export/template/output/YeWuExcel.xls"),YeWuExcel.class,dataList,map);
		
		
	}
	
	/**
	 * 导出地区销售情况
	 * @param request
	 * @param response
	 * 
	 * */
	@RequestMapping(value = "outputYHExcel")
	public void  outputYHExcel( HttpServletRequest request,HttpServletResponse response) {

		String province = request.getParameter("province");// 省份信息
		
		String userCLid =request.getParameter("userCLid");
		
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		String during = startTime + " - " +endTime;
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
																
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
		String companyId = user.getCompany().getId();

		List<OrderEntity> salesOrderList  = orderService.salesYH( companyId , userCLid , province,  startTime, endTime );
		List<YHExcel> addlist = new ArrayList<YHExcel>();
		if( salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o:salesOrderList ){
				YHExcel excel = new YHExcel();
				
				
				if( StringUtil.isNotEmpty(o.getProvince()) ){
					o.setProvince(o.getProvince().substring(0,2));
					if("黑龙".equals(o.getProvince())){
						o.setProvince("黑龙江");
					}else if ( "内蒙".equals(o.getProvince()) ){
						o.setProvince("内蒙古");
					}
				}else {
					o.setProvince("自提");
				}
				excel.setProvince(o.getProvince());
				
				excel.setFreightpic(o.getFreightpic().toString());
				excel.setNumber(o.getNumber().toString());
				excel.setPrice(o.getPrice().toString());
				addlist.add(excel);
			}
		}
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		map.put("date", during);
		
		dataList.add(addlist);

		String fileName  = "导出地区销售情况查询数据(" + during + ")";
		getResponse(request,response,fileName);
		excel("outputYHExcel",response,new TemplateExportParams("export/template/output/YHExcel.xls"),YHExcel.class,dataList,map);
		
		
	}
	
	/**
	 * 导出产品销售统计
	 * @param request
	 * @param response
	 * 
	 * */
	@RequestMapping(value = "outputCPBLExcel")
	public void  outputCPBLExcel( HttpServletRequest request,HttpServletResponse response) {
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		String during = startTime + " - " +endTime;
		String number = request.getParameter("number") == null ? "" : request.getParameter("number");
		String price = request.getParameter("price") == null ? "" : request.getParameter("price");
		String grossProfit = request.getParameter("grossProfit") == null ? "" : request.getParameter("grossProfit");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		String userCLid =request.getParameter("userCLid");
		
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
														
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}	
		String companyId = user.getCompany().getId();
		List<OrderEntity> salesOrderList  =orderService.salesCPBL( companyId , userCLid ,  startTime, endTime );
		
		List<CPBLExcel> addlist = new ArrayList<CPBLExcel>();
		if( salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o:salesOrderList ){
				CPBLExcel excel = new CPBLExcel();
				
				
				excel.setProductname(o.getProductname());
				
				excel.setFreightpic(o.getFreightpic().toString());
				excel.setNumber(o.getNumber().toString());
				excel.setPrice(o.getPrice().toString());
				addlist.add(excel);
			}
		}
		
		//增加一行汇总
		if(!number.equals("") && !price.equals("") && !grossProfit.equals("")){
			for(int i=0;i<3;i++){
				CPBLExcel excel = new CPBLExcel();
				if(i==2){
					excel.setPrice("总销量：" + number + " 件 | 总销售额 ：" + price + "  元 | 总毛利润： " + grossProfit + " 元");
				}else{
					excel.setPrice("");
				}
				
				addlist.add(excel);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		map.put("date", during);
		
		dataList.add(addlist);

		String fileName  = "导出产品销售统计查询数据(" + during + ")";
		getResponse(request,response,fileName);
		excel("outputCPBLExcel",response,new TemplateExportParams("export/template/output/CPBLExcel.xls"),CPBLExcel.class,dataList,map);
		
		
	}
	
	/**
	 * 导出销售数据统计
	 * @param request
	 * @param response
	 * 
	 * */
	@RequestMapping(value = "outputICExcel")
	public void  outputICExcel( HttpServletRequest request,HttpServletResponse response) {
		String productid = request.getParameter("productid");
		String userCLid =request.getParameter("userCLid");
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		String during = startTime + " - " +endTime;
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
												
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}	
		String companyId = user.getCompany().getId();
		List<OrderEntity> salesOrderList  = orderService.salesTIC( userCLid , productid,  startTime, endTime ,companyId  );
		
		List<ICExcel> addlist = new ArrayList<ICExcel>();
		
		if( salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o:salesOrderList ){
				ICExcel excel = new ICExcel();
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				if( null != o.getCreatedate() ){
					String createdate = formatter.format(o.getCreatedate());
					excel.setCreatedate(createdate);
				}
				
				excel.setFreightpic(o.getFreightpic().toString());
				excel.setNumber(o.getNumber().toString());
				excel.setPrice(o.getPrice().toString());
				addlist.add(excel);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		map.put("date", during);
		
		dataList.add(addlist);

		String fileName  = "导出销售数据统计查询数据(" + during + ")";
		getResponse(request,response,fileName);
		excel("outputICExcel",response,new TemplateExportParams("export/template/output/ICExcel.xls"),ICExcel.class,dataList,map);
		
		
		
	}
	
	/**
	 * 导出采购商数据统计
	 * @param request
	 * @param response
	 * 
	 * */
	@RequestMapping(value = "outputQDXSExcel")
	public void  outputQDXSExcel( HttpServletRequest request,HttpServletResponse response) {
		//String productid =request.getParameter("productid");
		String userid = request.getParameter("userid");
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		String during = startTime + " - " +endTime;
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
										
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
		String companyId = user.getCompany().getId();
		
		List<OrderEntity> salesOrderList  = orderService.salesQDXS( companyId ,userid ,  startTime, endTime );

		if( salesOrderList != null && salesOrderList.size() > 0){
			for( OrderEntity o:salesOrderList ){
				TSUser t = new TSUser();
				if( StringUtil.isNotEmpty(o.getId())){
					t = systemService.findUniqueByProperty (TSUser.class,"id" ,o.getId());
			    }
				o.setClientid(t);
			}
		}
		
		List<QDXSExcel> addlist = new ArrayList<QDXSExcel>();
		
		if( salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o:salesOrderList ){
				QDXSExcel excel = new QDXSExcel();
				/*if( null != o.getClientid() ){
					try{
						excel.setClientid_realname(o.getClientid().getRealname());
					}catch (Exception e) {
						// TODO: handle exception
					}
				}*/
				excel.setClientid_realname(o.getProductname());
				excel.setFreightpic(o.getFreightpic().toString());
				excel.setNumber(o.getNumber().toString());
				excel.setPrice(o.getPrice().toString());
				addlist.add(excel);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		map.put("date", during);
		
		dataList.add(addlist);

		String fileName  = "导出采购商数据统计查询数据(" + during + ")";
		getResponse(request,response,fileName);
		excel("outputQDXSExcel",response,new TemplateExportParams("export/template/output/QDXSExcel.xls"),QDXSExcel.class,dataList,map);
		
	}
	
	/**
	 * 导出资金明细
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputPayBillsExcel")
	public void  outputPayBillsExcel( PayBillsEntity payBillsEntity ,HttpServletRequest request,HttpServletResponse response) {
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
								
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
						
		String ordertype = ResourceUtil.getParameter("ordertype");
		CriteriaQuery cq = new CriteriaQuery(PayBillsEntity.class);
		cq.eq("company", user.getCompany());
		
		if(StringUtil.isNotEmpty(payBillsEntity.getIdentifier()) ){
			cq.like("identifier", "%"+payBillsEntity.getIdentifier()+"%");
			payBillsEntity.setIdentifier(null);
		}
		
		if( StringUtil.isNotEmpty(payBillsEntity.getPaymentmethod()) ){
			cq.eq("paymentmethod", payBillsEntity.getPaymentmethod());
		}
		
		if( StringUtil.isNotEmpty(payBillsEntity.getType()) ){
			cq.eq("type", payBillsEntity.getType());
		}
		
		if( StringUtil.isNotEmpty(payBillsEntity.getState()) ){
			cq.eq("state", payBillsEntity.getState());
		}
		
		if(!"1".equals(user.getType()) && !"2".equals(user.getType())){
			cq.eq("clientid", user);
		}
		//时间
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		cq.addOrder("createdate", SortDirection.desc);
				
		List<PayBillsEntity> paylist = systemService.getListByCriteriaQuery(cq, false);
		List<PayBillsExcel> addlist = new ArrayList<PayBillsExcel>();
		
		if( paylist!=null && paylist.size()>0 ){
			for( PayBillsEntity p : paylist){
				PayBillsExcel excle = new PayBillsExcel();
				excle.setIdentifier(p.getIdentifier());
				if( null != p.getClientid() ){
					try{
						excle.setClientid_realname(p.getClientid().getRealname());
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				
				String type = "";
				if( "1".equals(p.getType()) ){
					type = "销售";
				}else if( "2".equals(p.getType()) ){
					type = "充值";
				}
				excle.setType(type);

				String state = "";
				if( "1".equals(p.getState()) ){
					state = "审核通过";
				}else if( "2".equals(p.getState()) ){
					state = "待审核";
				}else if( "3".equals(p.getState()) ){
					state = "审核未通过";
				}else if( "4".equals(p.getState()) ){
					state = "系统确认";
				}else if( "5".equals(p.getState()) ){
					state = "支付失败";
				}
				excle.setState(state);
				
				
				
				String paymentmethod= "";
				if( "1".equals(p.getPaymentmethod()) ){
					paymentmethod = "支付宝";
				}else if( "2".equals(p.getPaymentmethod()) ){
					paymentmethod = "微信支付";
				}else if( "3".equals(p.getPaymentmethod()) ){
					paymentmethod = "线下转账";
				}else if( "4".equals(p.getPaymentmethod()) ){
					paymentmethod = "预存款支付";
				}else if( "5".equals(p.getPaymentmethod()) ){
					paymentmethod = "银联支付";
				}else if( "6".equals(p.getPaymentmethod()) ){
					paymentmethod = "先货后款";
				}
				excle.setPaymentmethod(paymentmethod);
				
			
				
				if( null != p.getCompany() ){
					try {
						excle.setCompany_companyName(p.getCompany().getCompanyName());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				excle.setPayablemoney(p.getPayablemoney().toString() + "元");
				excle.setAlreadypaidmoney(p.getAlreadypaidmoney().toString() + "元");
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				if( null != p.getCreatedate() ){
					String createdate = formatter.format(p.getCreatedate());
					excle.setCreatedate(createdate);
				}
				
				addlist.add(excle);
			}
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "资金明细预存款查询数据";
		getResponse(request,response,fileName);
		excel("outputPayBillsExcel",response,new TemplateExportParams("export/template/output/PayBillsExcel.xls"),PayBillsExcel.class,dataList,map);
		
				
	}
	
	/**
	 * 导出资金明细
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputPayBillsOrderCashExcel")
	public void  outputPayBillsOrderCashExcel( PayBillsOrderEntity payBillsOrderEntity ,HttpServletRequest request,HttpServletResponse response) {
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
						
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
				
		String ordertype = ResourceUtil.getParameter("ordertype");
		CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
		cq.eq("company", user.getCompany());
		
		//查询条件组装器
		if(StringUtil.isNotEmpty(ordertype) && ordertype.indexOf(",") > -1){
			String stateS[] = ordertype.split(",");
			cq.in("state", stateS);
			payBillsOrderEntity.setState(null);
					
		}else if(StringUtil.isNotEmpty(ordertype) && ordertype.indexOf(",") == -1){
			cq.eq("state", ordertype);
			payBillsOrderEntity.setState(null);
					
		}
				
		if(StringUtil.isNotEmpty(payBillsOrderEntity.getIdentifier()) ){
			cq.like("identifier", "%"+payBillsOrderEntity.getIdentifier()+"%");
			payBillsOrderEntity.setIdentifier(null);
		}
		
		if( StringUtil.isNotEmpty(payBillsOrderEntity.getPaymentmethod()) ){
			cq.eq("paymentmethod", payBillsOrderEntity.getPaymentmethod());
		}
		
		if( StringUtil.isNotEmpty(payBillsOrderEntity.getType()) ){
			cq.eq("type", payBillsOrderEntity.getType());
		}
				
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		//查询条件组装器
		if(!"1".equals(user.getType()) && !"2".equals(user.getType())){
			cq.eq("clientid", user);
		}
		cq.addOrder("updatedate", SortDirection.desc);
		
		List<PayBillsOrderEntity> paylist = systemService.getListByCriteriaQuery(cq, false);
		List<PayBillsOrderCashExcel> addlist = new ArrayList<PayBillsOrderCashExcel>();
		
		if( paylist!=null && paylist.size()>0 ){
			for( PayBillsOrderEntity p : paylist){
				PayBillsOrderCashExcel excle = new PayBillsOrderCashExcel();
				excle.setIdentifieror(p.getIdentifier());
				if( null != p.getClientid() ){
					try{
						excle.setClientid_realname(p.getClientid().getRealname());
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				if( null !=p.getYewu() ){
					try {
						excle.setYewu_realname(p.getYewu().getRealname());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				String type = "";
				if( "1".equals(p.getType()) ){
					type = "待付";
				}else if( "2".equals(p.getType()) ){
					type = "已付";
				}
				excle.setType(type);

				String state = "";
				if( "1".equals(p.getState()) ){
					state = "待付";
				}else if( "2".equals(p.getState()) ){
					state = "已付";
				}else if( "3".equals(p.getState()) ){
					state = "未付清";
				}else if( "4".equals(p.getState()) ){
					state = "已取消";
				}else if( "5".equals(p.getState()) ){
					state = "已取消";
				}
				excle.setState(state);
				
				String freightstatus= "";
				if(OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(p.getFreightState().getDictionaryValue())){
					freightstatus = "待发货";
				}else if(OrderServiceI.FreightState.ALREADY_DELIVER.getValue().equals(p.getFreightState().getDictionaryValue())){
					freightstatus = "已发货";
				}else if(OrderServiceI.FreightState.PART_DELIVER.getValue().equals(p.getFreightState().getDictionaryValue())){
					freightstatus = "部分发货";
				}
				
				excle.setFreightstatus(freightstatus);
				
				String paymentmethod= "";
				if( "1".equals(p.getPaymentmethod()) ){
					paymentmethod = "支付宝";
				}else if( "2".equals(p.getPaymentmethod()) ){
					paymentmethod = "微信支付";
				}else if( "3".equals(p.getPaymentmethod()) ){
					paymentmethod = "线下转账";
				}else if( "4".equals(p.getPaymentmethod()) ){
					paymentmethod = "预存款支付";
				}else if( "5".equals(p.getPaymentmethod()) ){
					paymentmethod = "银联支付";
				}else if( "6".equals(p.getPaymentmethod()) ){
					paymentmethod = "先货后款";
				}
				excle.setPaymentmethod(paymentmethod);
				
				
				String isSaleManOrder = "";
				if( "0".equals(p.getIsSaleManOrder()) ){
					isSaleManOrder = "否";
				}else if( "1".equals(p.getIsSaleManOrder()) ){
					isSaleManOrder = "是";
				}
				excle.setIsSaleManOrder(isSaleManOrder);
				
				if( null != p.getAudituser() ){
					try {
						excle.setAudituser(p.getAudituser().getRealname());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				if( null != p.getCompany() ){
					try {
						excle.setCompany_companyName(p.getCompany().getCompanyName());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				excle.setPayablemoney(p.getPayablemoney().toString());
				excle.setAlreadypaidmoney(p.getAlreadypaidmoney().toString());
				excle.setObligationsmoney(p.getObligationsmoney().toString());
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				if( null != p.getCreatedate() ){
					String createdate = formatter.format(p.getCreatedate());
					excle.setCreatedate(createdate);
				}
				if( null != p.getAuditdate() ){
					String auditDate = formatter.format(p.getAuditdate());
					excle.setAuditDate(auditDate);
				}
				addlist.add(excle);
			}
		}
		
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "资金明细查询数据";
		getResponse(request,response,fileName);
		excel("outputPayBillsOrderCashExcel",response,new TemplateExportParams("export/template/output/PayBillsOrderCashExcel.xls"),PayBillsOrderCashExcel.class,dataList,map);
		
	}
	
	
	/**
	 * 导出查询处理
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputPayBillsOrderExcel")
	public void  outputPayBillsOrderExcel( PayBillsOrderEntity payBillsOrderEntity ,HttpServletRequest request,HttpServletResponse response) {
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
				
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
		
		CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
		cq.eq("company", user.getCompany());
		
		//查询条件组装器
		if(StringUtil.isNotEmpty(payBillsOrderEntity.getState()) && "10".equals(payBillsOrderEntity.getState()) ){
			String stateS[] = "1,3".split(",");
			cq.in("state", stateS);
			payBillsOrderEntity.setState(null);
			
		}else if(StringUtil.isNotEmpty(payBillsOrderEntity.getState()) && payBillsOrderEntity.getState().indexOf(",") > 0 ){
			String stateS[] = payBillsOrderEntity.getState().split(",");
			cq.in("state", stateS);
			payBillsOrderEntity.setState(null);
		}else if(StringUtil.isNotEmpty(payBillsOrderEntity.getState())){
			cq.eq("state", payBillsOrderEntity.getState());
			payBillsOrderEntity.setState(null);
		}
		
		if(StringUtil.isNotEmpty(payBillsOrderEntity.getIdentifier()) ){
			cq.like("identifier", "%"+payBillsOrderEntity.getIdentifier()+"%");
			payBillsOrderEntity.setIdentifier(null);
		}
		
		if( null !=payBillsOrderEntity.getClientid() &&  StringUtil.isNotEmpty(payBillsOrderEntity.getClientid().getId() )){
			cq.eq("clientid", payBillsOrderEntity.getClientid());
		}
		
		if( (null != payBillsOrderEntity.getYewu() && StringUtil.isNotEmpty(payBillsOrderEntity.getYewu().getRealname())) || "4".equals(user.getType()) ){
			cq.createAlias("yewu", "u");
			if(null != payBillsOrderEntity.getYewu() && StringUtil.isNotEmpty(payBillsOrderEntity.getYewu().getRealname()) ){
				cq.add( Restrictions.like("u.realname", "%"+payBillsOrderEntity.getYewu().getRealname()+"%" ) ) ;
				payBillsOrderEntity.setYewu(null);
			}
			if("4".equals(user.getType())){
				cq.add( Restrictions.eq("u.id", user.getId()) ) ;
			}
			
		}
		
		if( StringUtil.isNotEmpty(payBillsOrderEntity.getPaymentmethod()) ){
			cq.eq("paymentmethod", payBillsOrderEntity.getPaymentmethod());
		}
		
		if( payBillsOrderEntity.getFreightState() != null){
			cq.eq("freightState", payBillsOrderEntity.getFreightState());
		}

		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		cq.addOrder("updatedate", SortDirection.desc);
		
		List<PayBillsOrderEntity> paylist = systemService.getListByCriteriaQuery(cq, false);
		List<PayBillOrderExcel> addlist = new ArrayList<PayBillOrderExcel>();
		
		if( paylist!=null && paylist.size()>0 ){
			for( PayBillsOrderEntity p : paylist){
				PayBillOrderExcel excle = new PayBillOrderExcel();
				excle.setIdentifieror(p.getIdentifier());
				if( null != p.getClientid() ){
					try{
						excle.setClientid_realname(p.getClientid().getRealname());
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				if( null !=p.getYewu() ){
					try {
						excle.setYewu_realname(p.getYewu().getRealname());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				String type = "";
				if( "1".equals(p.getType()) ){
					type = "待付";
				}else if( "2".equals(p.getType()) ){
					type = "已付";
				}
				excle.setType(type);

				String state = "";
				if( "1".equals(p.getState()) ){
					state = "待付";
				}else if( "2".equals(p.getState()) ){
					state = "已付";
				}else if( "3".equals(p.getState()) ){
					state = "未付清";
				}else if( "4".equals(p.getState()) ){
					state = "已取消";
				}else if( "5".equals(p.getState()) ){
					state = "已取消";
				}
				excle.setState(state);
				
				
				
				String freightstatus= "";
				if(OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(p.getFreightState().getDictionaryValue())){
					freightstatus = "待发货";
				}else if(OrderServiceI.FreightState.ALREADY_DELIVER.getValue().equals(p.getFreightState().getDictionaryValue())){
					freightstatus = "已发货";
				}else if(OrderServiceI.FreightState.PART_DELIVER.getValue().equals(p.getFreightState().getDictionaryValue())){
					freightstatus = "部分发货";
				}
				excle.setFreightstatus(freightstatus);
				
				String paymentmethod= "";
				if( "1".equals(p.getPaymentmethod()) ){
					paymentmethod = "支付宝";
				}else if( "2".equals(p.getPaymentmethod()) ){
					paymentmethod = "微信支付";
				}else if( "3".equals(p.getPaymentmethod()) ){
					paymentmethod = "线下转账";
				}else if( "4".equals(p.getPaymentmethod()) ){
					paymentmethod = "预存款支付";
				}else if( "5".equals(p.getPaymentmethod()) ){
					paymentmethod = "银联支付";
				}else if( "6".equals(p.getPaymentmethod()) ){
					paymentmethod = "先货后款";
				}
				excle.setPaymentmethod(paymentmethod);
				
				
				String isSaleManOrder = "";
				if( "0".equals(p.getIsSaleManOrder()) ){
					isSaleManOrder = "否";
				}else if( "1".equals(p.getIsSaleManOrder()) ){
					isSaleManOrder = "是";
				}
				excle.setIsSaleManOrder(isSaleManOrder);
				
				if( null != p.getAudituser() ){
					try {
						excle.setAudituser(p.getAudituser().getRealname());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				excle.setPayablemoney(p.getPayablemoney().toString());
				excle.setAlreadypaidmoney(p.getAlreadypaidmoney().toString());
				excle.setObligationsmoney(p.getObligationsmoney().toString());
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				if( null != p.getCreatedate() ){
					String createdate = formatter.format(p.getCreatedate());
					excle.setCreatedate(createdate);
				}
				if( null != p.getAuditdate() ){
					String auditDate = formatter.format(p.getAuditdate());
					excle.setAuditDate(auditDate);
				}
				addlist.add(excle);
			}
		}
		
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "订单管理查询数据";
		getResponse(request,response,fileName);
		excel("outputOrderExcel",response,new TemplateExportParams("export/template/output/PayBillsOrderExcel.xls"),PayBillOrderExcel.class,dataList,map);
		
		
	}
	
	/**
	 * 导出发货订单
	 */
	@RequestMapping(value = "outputCopartnerOrderExcel")
	public void outputCopartnerOrderExcel(OrderSendEntity orderSend, HttpServletRequest request, HttpServletResponse response){
		TSUser user = ResourceUtil.getSessionUserName();
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		    
		}
		
		OrderEntity order = orderSend.getOrderid();
		if(null == order){
			order = new OrderEntity();
			orderSend.setOrderid(order);
		}
		String ordertype = ResourceUtil.getParameter("ordertype");
		if(StringUtil.isNotEmpty(ordertype)){
			order.setState(ordertype);
		}
		
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class);
		cq.createAlias("orderid", "order");
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.add(Restrictions.like("order.identifieror", "%"+order.getIdentifieror()+"%"));
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.add(Restrictions.like("order.productname", "%"+order.getProductname()+"%"));
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.add(Restrictions.like("order.customername", "%"+order.getCustomername()+"%"));
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.add(Restrictions.like("order.telephone", "%"+order.getTelephone()+"%"));
			order.setTelephone(null);
		}
		if(StringUtil.isNotEmpty(order.getAddress()) ){
			cq.add(Restrictions.like("order.address", "%"+order.getAddress()+"%"));
			order.setAddress(null);
		}
		if( StringUtil.isNotEmpty(order.getSendtype())){
			cq.add(Restrictions.eq("order.sendtype", order.getSendtype()));
			order.setSendtype(null);
		}
		if(StringUtil.isNotEmpty(order.getState()) ){
			String[] orders= order.getState().split(",");
			if( orders.length > 0 ){
				cq.add(Restrictions.in("order.state", orders));
				order.setState(null);
			}
		}
		String status = orderSend.getStatus();
		if(StringUtil.isEmpty(status)){
			cq.in("status", "1,2".split(","));
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		String createdate3 = request.getParameter("createdate3");
		String createdate4 = request.getParameter("createdate4");
		//派单时间
		SimpleDateFormat sdf1 = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.add(Restrictions.between("createtime", DateUtils.str2Date(createdate1,sdf1), DateUtils.str2Date(createdate2,sdf1)));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.add(Restrictions.ge("createtime", DateUtils.str2Date(createdate1,sdf1)));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.add(Restrictions.le("createtime", DateUtils.str2Date(createdate2,sdf1)));
		}
		//下单时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate3)&&StringUtil.isNotEmpty(createdate4)){
			cq.add(Restrictions.between("order.createdate", DateUtils.str2Date(createdate3,sdf), DateUtils.str2Date(createdate4,sdf)));
		}else if( StringUtil.isNotEmpty(createdate3) ){
			cq.add(Restrictions.ge("order.createdate", DateUtils.str2Date(createdate3,sdf)));
		}else if( StringUtil.isNotEmpty(createdate4) ){
			cq.add(Restrictions.le("order.createdate", DateUtils.str2Date(createdate4,sdf)));
		}
		
		cq.addOrder("createtime", SortDirection.desc);
		if("6".equals(user.getType())){
			cq.eq("receiver", user);
		}
		cq.eq("distributecompanyid", user.getCompany());
		cq.add();
		
		List<OrderSendEntity> list = systemService.getListByCriteriaQuery(cq,false);
		List<CopartnerOrderExcel> addlist = new ArrayList<CopartnerOrderExcel>();
		if( list != null && list.size() > 0 ){
			for(OrderSendEntity os : list){
				OrderEntity o = os.getOrderid();
				CopartnerOrderExcel p = new CopartnerOrderExcel();
				try {
					MyBeanUtils.copyBeanNotNull2Bean(o , p);
					
					try {
						p.setDeparturedate(DateUtils.date3Str(o.getDeparturedate(), "yyyy-MM-dd HH:mm:ss" ));
					} catch (Exception e) {	
					}
					try {
						p.setOrderdatetime(DateUtils.date3Str(o.getCreatedate(), "yyyy-MM-dd HH:mm:ss" ));
					} catch (Exception e) {	
					}
					try {
						p.setClientid(o.getClientid().getRealname());
					} catch (Exception e) {
						
					}
					try {
						p.setYewu(o.getYewu().getRealname());
					} catch (Exception e) {
						
					}
					
					if( "1".equals(o.getState()) ){
						p.setState("待付款");
					}else if( "2".equals(o.getState()) ){
						p.setState("待发货");
					}else if( "3".equals(o.getState()) ){
						p.setState("已发货");
					}else if( "4".equals(o.getState()) ){
						p.setState("已签收");
					}else if( "5".equals(o.getState()) ){
						p.setState("已取消");
					}else if( "6".equals(o.getState()) ){
						p.setState("待审核");
					}else if( "7".equals(o.getState()) ){
						p.setState("未通过");
					}
					
					try {
						p.setYewu(o.getYewu().getRealname());
					} catch (Exception e) {
						
					}
					
					
					addlist.add(p);
				} catch (Exception e1) {
				}
			}
		}
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getUserName());
		
		dataList.add(addlist);

		String fileName  = "发货查询数据";
		getResponse(request,response,fileName);
		excel("outputCopartnerOrderExcel",response,new TemplateExportParams("export/template/output/CopartnerOrderExcel.xls"),CopartnerOrderExcel.class,dataList,map);
	}
	/**
	 * 导出查询处理
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputOrderExcel")
	public void  outputOrderExcel( OrderEntity order ,HttpServletRequest request,HttpServletResponse response) {

		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
		
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		    
		}
		
		//物流状态
		String freightStateValue = ResourceUtil.getParameter("freightStateValue");
		
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
		cq.eq("company", user.getCompany());
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.like("identifieror", "%"+order.getIdentifieror()+"%");
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.like("productname", "%"+order.getProductname()+"%");
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.like("customername", "%"+order.getCustomername()+"%");
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.like("telephone", "%"+order.getTelephone()+"%");
			order.setTelephone(null);
		}
		
		if(StringUtil.isNotEmpty(freightStateValue)){
			cq.createAlias("freightState", "freightState");
			cq.eq("freightState.dictionaryValue", freightStateValue);
			order.setFreightState(null);
		}
		
		/*if(StringUtil.isNotEmpty(order.getState()) &&  order.getState().indexOf(",") > 0 ){
			String stateS[] = order.getState().split(",");
			cq.in("state", stateS);
			order.setState(null);
		}*/
		//业务员只能看自己负责客户的单子
		if("4".equals(user.getType())){
			cq.createAlias("yewu", "yw");
			cq.add( Restrictions.eq("yw.id", user.getId()) );
			order.setYewu(null);
		}
			
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
			
		cq.addOrder("createdate", SortDirection.desc);	
		cq.add();
		
		List<OrderEntity> list = systemService.getListByCriteriaQuery(cq,false);
		List<OrderExcel> addlist = new ArrayList<OrderExcel>();
		if( list != null && list.size() > 0 ){
			for(OrderEntity o : list){
				OrderExcel p = new OrderExcel();
				try {
					MyBeanUtils.copyBeanNotNull2Bean(o , p);
					
					try {
						p.setDeparturedate(DateUtils.date3Str(o.getDeparturedate(), "yyyy-MM-dd HH:mm:ss" ));
					} catch (Exception e) {	
					}
					try {
						p.setOrderdatetime(DateUtils.date3Str(o.getCreatedate(), "yyyy-MM-dd HH:mm:ss" ));
					} catch (Exception e) {	
					}
					try {
						p.setClientid(o.getClientid().getRealname());
					} catch (Exception e) {
						
					}
					try {
						p.setYewu(o.getYewu().getRealname());
					} catch (Exception e) {
						
					}
					
					if(o.getFreightState() == null){
						continue;
					}
					
					if(o.getFreightState().getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_PACK.getValue())){
						//待打包
						p.setFreightState("待打包");
					}else if(o.getFreightState().getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_DELIVER.getValue())){
						//待发货
						p.setFreightState("待发货");
					}else if(o.getFreightState().getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_DELIVER.getValue())){
						//已发货
						p.setFreightState("已发货");
					}else if(o.getFreightState().getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_SIGN.getValue())){
						//已签收
						p.setFreightState("已签收");
					}else if(o.getFreightState().getDictionaryValue().equals(OrderServiceI.FreightState.PART_DELIVER.getValue())){
						//部分发货
						p.setFreightState("部分发货");
					}
					
					if( "1".equals(o.getState()) ){
						p.setState("待付款");
					}else if( "2".equals(o.getState()) ){
						p.setState("无");
					}else if( "3".equals(o.getState()) ){
						p.setState("无");
					}else if( "4".equals(o.getState()) ){
						p.setState("无");
					}else if( "5".equals(o.getState()) ){
						p.setState("已取消");
					}else if( "6".equals(o.getState()) ){
						p.setState("待审核");
					}else if( "7".equals(o.getState()) ){
						p.setState("未通过");
					}
					
					try {
						p.setYewu(o.getYewu().getRealname());
					} catch (Exception e) {
						
					}
					
					addlist.add(p);
				} catch (Exception e1) {
				}
			}
		}
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "查询数据";
		getResponse(request,response,fileName);
		excel("outputPayBillsOrderExcel",response,new TemplateExportParams("export/template/output/OrderExcel.xls"),OrderExcel.class,dataList,map);
		
	}
	
	/**
	 * 导出发货单历史
	 * @param request
	 * @param response
	 */
	/**
	 * @param orderSend
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputCopartnerOrderHistory")
	public void  outputCopartnerOrderHistory( OrderSendEntity orderSend ,HttpServletRequest request,HttpServletResponse response) {

		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
		
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		    
		}
		
		OrderEntity order = orderSend.getOrderid();
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class);
		cq.createAlias("orderid", "order");
		//查询条件组装器
		if(null != order && StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.add(Restrictions.like("order.identifieror", "%"+order.getIdentifieror()+"%"));
			order.setIdentifieror(null);
		}
		if(null != order && StringUtil.isNotEmpty(order.getProductname()) ){
			cq.add(Restrictions.like("order.productname", "%"+order.getProductname()+"%"));
			order.setProductname(null);
		}
		if(null != order && StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.add(Restrictions.like("order.customername", "%"+order.getCustomername()+"%"));
			order.setCustomername(null);
		}
		if(null != order && StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.add(Restrictions.like("order.telephone", "%"+order.getTelephone()+"%"));
			order.setTelephone(null);
		}
		if(null != order && StringUtil.isNotEmpty(order.getAddress()) ){
			cq.add(Restrictions.like("order.address", "%"+order.getAddress()+"%"));
			order.setAddress(null);
		}
		if(null != order &&  StringUtil.isNotEmpty(order.getSendtype())){
			cq.add(Restrictions.eq("order.sendtype", order.getSendtype()));
			order.setSendtype(null);
		}
		if(null != order && StringUtil.isNotEmpty(order.getState()) ){
			String[] orders= order.getState().split(",");
			if( orders.length > 0 ){
				cq.add(Restrictions.in("order.state", orders));
				order.setState(null);
			}
		}
		
		if(null != orderSend.getReceiver() && StringUtil.isNotEmpty(orderSend.getReceiver().getId())){
			cq.eq("receiver.id", orderSend.getReceiver().getId());
		}
		cq.eq("distributecompanyid.id", user.getCompany().getId());
		
		cq.in("status", "1,2".split(","));
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//派单时间
		SimpleDateFormat sdf1 = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.add(Restrictions.between("createtime", DateUtils.str2Date(createdate1,sdf1), DateUtils.str2Date(createdate2,sdf1)));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.add(Restrictions.ge("createtime", DateUtils.str2Date(createdate1,sdf1)));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.add(Restrictions.le("createtime", DateUtils.str2Date(createdate2,sdf1)));
		}
		cq.addOrder("createtime", SortDirection.desc);
		
		cq.add();
		
		List<OrderSendEntity> list = systemService.getListByCriteriaQuery(cq,false);
		List<CopartnerOrderSend> addlist = new ArrayList<CopartnerOrderSend>();
		if( list != null && list.size() > 0 ){
			for(OrderSendEntity os : list){
				OrderEntity o = os.getOrderid();
				CopartnerOrderSend p = new CopartnerOrderSend();
				try {
					MyBeanUtils.copyBeanNotNull2Bean(o , p);
					
					try {
						p.setDeparturedate(DateUtils.date3Str(o.getDeparturedate(), "yyyy-MM-dd HH:mm:ss" ));
					} catch (Exception e) {	
					}
					try {
						p.setOrderdatetime(DateUtils.date3Str(o.getCreatedate(), "yyyy-MM-dd HH:mm:ss" ));
					} catch (Exception e) {	
					}
					try {
						p.setClientid(o.getClientid().getRealname());
					} catch (Exception e) {
						
					}
					try {
						p.setYewu(o.getYewu().getRealname());
					} catch (Exception e) {
						
					}
					
					if( "1".equals(o.getState()) ){
						p.setState("待付款");
					}else if( "2".equals(o.getState()) ){
						p.setState("待发货");
					}else if( "3".equals(o.getState()) ){
						p.setState("已发货");
					}else if( "4".equals(o.getState()) ){
						p.setState("已签收");
					}else if( "5".equals(o.getState()) ){
						p.setState("已取消");
					}else if( "6".equals(o.getState()) ){
						p.setState("待审核");
					}else if( "7".equals(o.getState()) ){
						p.setState("未通过");
					}
					
					try {
						p.setYewu(o.getYewu().getRealname());
					} catch (Exception e) {
						
					}
					
					if("Y".equals(o.getOrdersend())){
						try {//处理发货商
							p.setCopartner(os.getReceiver().getTsuserCopartner().getPartnercompany());
						} catch (Exception e) {
							
						}
					}
					addlist.add(p);
				} catch (Exception e1) {
				}
			}
		}
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "派单历史导出数据-" + DateUtils.getDate("yyyyMMddHHmmss");
		getResponse(request,response,fileName);
		excel("outputCopartnerOrderHistory",response,new TemplateExportParams("export/template/output/CopartnerOrderSend.xls"),CopartnerOrderSend.class,dataList,map);
		
	}
	
	/**
	 * 导出库存供应商
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "outputStockIOSupplier")
	public void  outputStockIOSupplier( StockSupplierEntity stockSupplier ,HttpServletRequest request,HttpServletResponse response) {
		//当前用户信息
		TSUser user =  ResourceUtil.getSessionUserName();		
								
		if( user == null || !StringUtil.isNotEmpty(user.getId())){
			user = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
		}
		
		CriteriaQuery cq = new CriteriaQuery(StockSupplierEntity.class);
		cq.eq("company", user.getCompany());
		cq.eq("status", 0);
		
		if( StringUtil.isNotEmpty(stockSupplier.getName()) ){
			cq.like("name", "%"+stockSupplier.getName()+"%");
		}
		if( StringUtil.isNotEmpty(stockSupplier.getCode()) ){
			cq.eq("code", stockSupplier.getCode());
		}
		
		cq.add();
		List<StockSupplierEntity> list = systemService.getListByCriteriaQuery(cq, false);
		
		List<StockSupplierExcel> addlist = new ArrayList<StockSupplierExcel>();
		if( null!=list && list.size()>0){
			for( StockSupplierEntity ss : list){
				StockSupplierExcel excel = new StockSupplierExcel();
				
				try {
					excel.setCode(ss.getCode());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setName(ss.getName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setTel(ss.getTel());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					excel.setAddress(ss.getAddress());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				addlist.add(excel);
			}
			
		}
		
		/**
		 * 拼接数据传入方法
		 */
		ArrayList<Collection<?>> dataList=new ArrayList<Collection<?>>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("add", user.getCompany().getCompanyName());
		
		dataList.add(addlist);

		String fileName  = "库存供应商";
		getResponse(request,response,fileName);
		excel("outputStockIOSupplier",response,new TemplateExportParams("export/template/output/stockSupplierExcel.xls"),StockSupplierExcel.class,dataList,map);
		
	}
	
	 public static String replaceBlank(String str) {
	        String dest = "";
	        if (str!=null) {
	            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
	            Matcher m = p.matcher(str);
	            dest = m.replaceAll("");
	        }
	        return dest;
	    }
	
	
}
