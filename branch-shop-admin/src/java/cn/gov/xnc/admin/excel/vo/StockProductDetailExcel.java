package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class StockProductDetailExcel {

	public StockProductDetailExcel() {
		// TODO Auto-generated constructor stub
	}
		
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
		
	/**商品编码*/
	@Excel(exportName="商品编码",orderNum="1")
	private java.lang.String productid_code;
		
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="2")
	private java.lang.String productid_name;
		
	/**品牌*/
	@Excel(exportName="品牌",orderNum="3")
	private java.lang.String productid_brandid_brandname;
	
	/**规格*/
	@Excel(exportName="规格",orderNum="4")
	private java.lang.String productid_specifications;
		
	/**仓库*/
	@Excel(exportName="仓库",orderNum="5")
	private java.lang.String stockid_stockname;
	
	/**仓储位置*/
	@Excel(exportName="仓储位置",orderNum="6")
	private java.lang.String stocksegment;
		
	/**单位说明*/
	@Excel(exportName="单位说明",orderNum="7")
	private java.lang.String unitinfo;
		
	/**当前库存*/
	@Excel(exportName="当前库存",orderNum="8")
	private java.lang.String stocknuminfo;
	
	/**预警值下限*/
	@Excel(exportName="预警值下限",orderNum="9")
	private java.lang.String alertinfo;
	
	/**预警值上限*/
	@Excel(exportName="预警值上限",orderNum="10")
	private java.lang.String alertupperinfo;
	
	/**库存状态*/
	@Excel(exportName="库存状态",orderNum="11")
	private java.lang.String status;
		
	/**基础单位*/
//	@Excel(exportName="基础单位",orderNum="12")
//	private java.lang.String baseunit;
	
	/**数量*/
	@Excel(exportName="数量",orderNum="13")
	private java.lang.String baseunit_num;
		
	/**库存区域均价*/
	@Excel(exportName="库存区域均价",orderNum="14")
	private java.lang.String averageprice;
	
	/**库存区域总价*/
	@Excel(exportName="库存区域总价",orderNum="15")
	private java.lang.String totalprice;
	
	/**库存均价*/
	@Excel(exportName="库存均价",orderNum="16")
	private java.lang.String all_averageprice;
	
	/**库存总价*/
	@Excel(exportName="库存总价",orderNum="17")
	private java.lang.String all_totalprice;

	/**
	 * @return the productid_specifications
	 */
	public java.lang.String getProductid_specifications() {
		return productid_specifications;
	}

	/**
	 * @param productid_specifications the productid_specifications to set
	 */
	public void setProductid_specifications(java.lang.String productid_specifications) {
		this.productid_specifications = productid_specifications;
	}

	/**
	 * @return the baseunit_num
	 */
	public java.lang.String getBaseunit_num() {
		return baseunit_num;
	}

	/**
	 * @param baseunit_num the baseunit_num to set
	 */
	public void setBaseunit_num(java.lang.String baseunit_num) {
		this.baseunit_num = baseunit_num;
	}

	/**
	 * @return the all_averageprice
	 */
	public java.lang.String getAll_averageprice() {
		return all_averageprice;
	}

	/**
	 * @param all_averageprice the all_averageprice to set
	 */
	public void setAll_averageprice(java.lang.String all_averageprice) {
		this.all_averageprice = all_averageprice;
	}

	/**
	 * @return the all_totalprice
	 */
	public java.lang.String getAll_totalprice() {
		return all_totalprice;
	}

	/**
	 * @param all_totalprice the all_totalprice to set
	 */
	public void setAll_totalprice(java.lang.String all_totalprice) {
		this.all_totalprice = all_totalprice;
	}

	/**
	 * @return the status
	 */
	public java.lang.String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(java.lang.String status) {
		this.status = status;
	}

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the productid_code
	 */
	public java.lang.String getProductid_code() {
		return productid_code;
	}

	/**
	 * @param productid_code the productid_code to set
	 */
	public void setProductid_code(java.lang.String productid_code) {
		this.productid_code = productid_code;
	}

	/**
	 * @return the productid_name
	 */
	public java.lang.String getProductid_name() {
		return productid_name;
	}

	/**
	 * @param productid_name the productid_name to set
	 */
	public void setProductid_name(java.lang.String productid_name) {
		this.productid_name = productid_name;
	}

	/**
	 * @return the stockid_stockname
	 */
	public java.lang.String getStockid_stockname() {
		return stockid_stockname;
	}

	/**
	 * @param stockid_stockname the stockid_stockname to set
	 */
	public void setStockid_stockname(java.lang.String stockid_stockname) {
		this.stockid_stockname = stockid_stockname;
	}
	
	/**
	 * @return the productid_brandid_brandname
	 */
	public java.lang.String getProductid_brandid_brandname() {
		return productid_brandid_brandname;
	}
	
	/**
	 * @param productid_brandid_brandname the productid_brandid_brandname to set
	 */
	public void setProductid_brandid_brandname(java.lang.String productid_brandid_brandname) {
		this.productid_brandid_brandname = productid_brandid_brandname;
	}

	/**
	 * @return the unitinfo
	 */
	public java.lang.String getUnitinfo() {
		return unitinfo;
	}

	/**
	 * @param unitinfo the unitinfo to set
	 */
	public void setUnitinfo(java.lang.String unitinfo) {
		this.unitinfo = unitinfo;
	}

	/**
	 * @return the stocknuminfo
	 */
	public java.lang.String getStocknuminfo() {
		return stocknuminfo;
	}

	/**
	 * @param stocknuminfo the stocknuminfo to set
	 */
	public void setStocknuminfo(java.lang.String stocknuminfo) {
		this.stocknuminfo = stocknuminfo;
	}

	/**
	 * @return the totalprice
	 */
	public java.lang.String getTotalprice() {
		return totalprice;
	}

	/**
	 * @param totalprice the totalprice to set
	 */
	public void setTotalprice(java.lang.String totalprice) {
		this.totalprice = totalprice;
	}

	/**
	 * @return the averageprice
	 */
	public java.lang.String getAverageprice() {
		return averageprice;
	}

	/**
	 * @param averageprice the averageprice to set
	 */
	public void setAverageprice(java.lang.String averageprice) {
		this.averageprice = averageprice;
	}

	/**
	 * @return the stocksegment
	 */
	public java.lang.String getStocksegment() {
		return stocksegment;
	}

	/**
	 * @param stocksegment the stocksegment to set
	 */
	public void setStocksegment(java.lang.String stocksegment) {
		this.stocksegment = stocksegment;
	}

	/**
	 * @return the alertinfo
	 */
	public java.lang.String getAlertinfo() {
		return alertinfo;
	}

	/**
	 * @param alertinfo the alertinfo to set
	 */
	public void setAlertinfo(java.lang.String alertinfo) {
		this.alertinfo = alertinfo;
	}

	/**
	 * @return the alertupperinfo
	 */
	public java.lang.String getAlertupperinfo() {
		return alertupperinfo;
	}

	/**
	 * @param alertupperinfo the alertupperinfo to set
	 */
	public void setAlertupperinfo(java.lang.String alertupperinfo) {
		this.alertupperinfo = alertupperinfo;
	}
		



}
