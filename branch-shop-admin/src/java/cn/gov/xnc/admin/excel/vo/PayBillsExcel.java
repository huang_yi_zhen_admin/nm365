package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class PayBillsExcel {

	public PayBillsExcel() {
		// TODO Auto-generated constructor stub
	}
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**订单编号*/
	@Excel(exportName="订单编号",orderNum="1")
	private java.lang.String identifier;
	
	/**客户*/
	@Excel(exportName="客户",orderNum="2")
	private java.lang.String clientid_realname;
	
	/**公司*/
	@Excel(exportName="公司",orderNum="3")
	private java.lang.String company_companyName;
	
	/**收支类型*/
	@Excel(exportName="收支类型",orderNum="4")
	private java.lang.String type;
	
	/**状态*/
	@Excel(exportName="状态",orderNum="5")
	private java.lang.String state;
	
	/**支付方式*/
	@Excel(exportName="支付方式",orderNum="6")
	private java.lang.String paymentmethod;
	
	/**应付款*/
	@Excel(exportName="支付方式",orderNum="7")
	private java.lang.String payablemoney;
	
	/**已付款*/
	@Excel(exportName="已付款",orderNum="8")
	private java.lang.String alreadypaidmoney;
	
	/**创建时间*/
	@Excel(exportName="创建时间",orderNum="9")
	private java.lang.String createdate;

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the identifier
	 */
	public java.lang.String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(java.lang.String identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the clientid_realname
	 */
	public java.lang.String getClientid_realname() {
		return clientid_realname;
	}

	/**
	 * @param clientid_realname the clientid_realname to set
	 */
	public void setClientid_realname(java.lang.String clientid_realname) {
		this.clientid_realname = clientid_realname;
	}

	/**
	 * @return the company_companyName
	 */
	public java.lang.String getCompany_companyName() {
		return company_companyName;
	}

	/**
	 * @param company_companyName the company_companyName to set
	 */
	public void setCompany_companyName(java.lang.String company_companyName) {
		this.company_companyName = company_companyName;
	}

	/**
	 * @return the type
	 */
	public java.lang.String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(java.lang.String type) {
		this.type = type;
	}

	/**
	 * @return the state
	 */
	public java.lang.String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(java.lang.String state) {
		this.state = state;
	}

	/**
	 * @return the paymentmethod
	 */
	public java.lang.String getPaymentmethod() {
		return paymentmethod;
	}

	/**
	 * @param paymentmethod the paymentmethod to set
	 */
	public void setPaymentmethod(java.lang.String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}

	/**
	 * @return the payablemoney
	 */
	public java.lang.String getPayablemoney() {
		return payablemoney;
	}

	/**
	 * @param payablemoney the payablemoney to set
	 */
	public void setPayablemoney(java.lang.String payablemoney) {
		this.payablemoney = payablemoney;
	}

	/**
	 * @return the alreadypaidmoney
	 */
	public java.lang.String getAlreadypaidmoney() {
		return alreadypaidmoney;
	}

	/**
	 * @param alreadypaidmoney the alreadypaidmoney to set
	 */
	public void setAlreadypaidmoney(java.lang.String alreadypaidmoney) {
		this.alreadypaidmoney = alreadypaidmoney;
	}

	

	/**
	 * @return the createdate
	 */
	public java.lang.String getCreatedate() {
		return createdate;
	}

	/**
	 * @param createdate the createdate to set
	 */
	public void setCreatedate(java.lang.String createdate) {
		this.createdate = createdate;
	}
	
	

}
