package cn.gov.xnc.admin.excel.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.excel.service.ExcelServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("excelService")
public class ExcelServiceImpl extends CommonServiceImpl implements ExcelServiceI {

	@Override
	public String getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder) {
		String freightStatus = "1"; //未发货_1,已发货_2,部分发货_3
		Integer totalOrder = 1;
		Integer hasFreightOrderNum = 0;
		if(null != paybillsOrder){
			PayBillsOrderEntity payBills = findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
			List<OrderEntity> orderList = payBills.getOrderS();
			totalOrder = orderList.size();
			String isSend = null;
			for (OrderEntity orderEntity : orderList) {
				isSend = orderEntity.getState();//  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7
				if("3".equals(isSend) || "4".equals(isSend)){
					hasFreightOrderNum++;
				}
			}
			
		}
		if(totalOrder > 0){
			
			if( totalOrder == hasFreightOrderNum ){
				
				freightStatus = "2";
			}
			else if( hasFreightOrderNum > 0 && totalOrder > hasFreightOrderNum ){
				
				freightStatus = "3";
			}
		}
		
		return freightStatus;
	}


}
