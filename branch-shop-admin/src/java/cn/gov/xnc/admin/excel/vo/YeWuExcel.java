package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class YeWuExcel {

	public YeWuExcel() {
		// TODO Auto-generated constructor stub
	}
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**销售*/
	@Excel(exportName="销售",orderNum="1")
	private java.lang.String price;
	
	/**奖励*/
	@Excel(exportName="奖励",orderNum="2")
	private java.lang.String priceyw;
	
	/**日期*/
	@Excel(exportName="日期",orderNum="3")
	private java.lang.String createdate;

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the price
	 */
	public java.lang.String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(java.lang.String price) {
		this.price = price;
	}

	/**
	 * @return the priceyw
	 */
	public java.lang.String getPriceyw() {
		return priceyw;
	}

	/**
	 * @param priceyw the priceyw to set
	 */
	public void setPriceyw(java.lang.String priceyw) {
		this.priceyw = priceyw;
	}

	/**
	 * @return the createdate
	 */
	public java.lang.String getCreatedate() {
		return createdate;
	}

	/**
	 * @param createdate the createdate to set
	 */
	public void setCreatedate(java.lang.String createdate) {
		this.createdate = createdate;
	}
	
	

}
