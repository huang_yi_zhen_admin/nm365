package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonArray;

import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockSegmentServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("stockSegmentService")
@Transactional(rollbackForClassName={"Exception"})
public class StockSegmentServiceImpl extends CommonServiceImpl implements StockSegmentServiceI {
	
	public AjaxJson save(StockSegmentEntity stockSegment, HttpServletRequest request,SystemService systemService,StockSegmentServiceI stockSegmentService) {
		AjaxJson j = new AjaxJson();
		String message = "";
		if (StringUtil.isNotEmpty(stockSegment.getId())) {//变更
			
			StockSegmentEntity t = stockSegmentService.get(StockSegmentEntity.class, stockSegment.getId());
			stockSegment.setStockid(t.getStockid());
			stockSegment.setPreid(t.getPreid());//避免被页面传进来的空值所影响覆盖
			String fullpath = getFullPathName(stockSegment, systemService);
			stockSegment.setFullpathname(fullpath);
			try {
				String loginfo = "编辑库存区域，编辑前名称："+ t.getName() +",备注：" + t.getRemarks() + " 编辑后名称：" + stockSegment.getName() + ",备注：" +stockSegment.getRemarks() ;
				MyBeanUtils.copyBeanNotNull2Bean(stockSegment, t);
				stockSegmentService.saveOrUpdate(t);
				message = "仓库区域更新成功("+ stockSegmentEntity2Json(t) + ")";
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
				
				opLog(request, OperationLogUtil.STOCK_SEGMENT_UPDATE, stockSegment, loginfo );
			} catch (Exception e) {
				e.printStackTrace();
				message = "出库详细单更新失败";
				j.setSuccess(false);
			}
		} else {
			
			//获取出父节点
			StockSegmentEntity pret = stockSegmentService.get(StockSegmentEntity.class, stockSegment.getPreid() );
			if( null == pret){
				message = "无效父节点区域，请重新操作";
				j.setMsg(message);
				j.setSuccess(false);
				return j;
			}
			stockSegment.setStockid(pret.getStockid());
			String fullpath = getFullPathName(stockSegment, systemService);
			stockSegment.setFullpathname(fullpath);
			message = "仓库区域添加成功(" + stockSegmentEntity2Json(stockSegment) + ")";
			stockSegment.setId(IdWorker.generateSequenceNo());
			stockSegmentService.save(stockSegment);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			
			String loginfo = "新增库存区域名称："+ stockSegment.getName() +",备注：" + stockSegment.getRemarks() ;
			opLog(request, OperationLogUtil.STOCK_SEGMENT_ADD, stockSegment, loginfo );
		}
		j.setMsg(message);
		return j;
	}
	
	public String getFullPathName(StockSegmentEntity segment,SystemService systemService){
		CriteriaQuery css = new CriteriaQuery(StockSegmentEntity.class);
		css.eq("stockid", segment.getStockid());
		List<StockSegmentEntity> seglist = systemService.getListByCriteriaQuery(css,false);
		Map<String, StockSegmentEntity> segmap = new HashMap<String, StockSegmentEntity>();
		for(StockSegmentEntity s: seglist){
			segmap.put(s.getId(), s);
		}
		
		List<StockSegmentEntity> segfulllink = new ArrayList<StockSegmentEntity>();
		StockSegmentEntity tmp = segment;
		segfulllink.add(tmp);
		while(StringUtil.isNotEmpty(tmp.getPreid())){
			tmp = segmap.get(tmp.getPreid());
			if( null == tmp ){
				break;
			}
			segfulllink.add(tmp);
		}
		StringBuffer buf = new StringBuffer();
		for( int i = segfulllink.size()-1; i >=0 ; i--  ){
			StockSegmentEntity s = segfulllink.get(i);
			buf.append(s.getName());
			if( i != 0 ){
				buf.append("->");
			}
		}
		return buf.toString();
	}
	
	public AjaxJson getAllStockInfo( HttpServletRequest request, SystemService systemService) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		//1、获取仓库信息
		CriteriaQuery cse = new CriteriaQuery(StockEntity.class);
		cse.eq("companyid", user.getCompany());
		
		String stockid = request.getParameter("stockid");
		if(StringUtil.isNotEmpty("stockid")){
			cse.eq("id", stockid);
		}
		
		String stockname = request.getParameter("stockname");
		if( StringUtil.isNotEmpty(stockname) ){
			cse.like("stockname", "%" + stockname + "%");
		}
		
		cse.notEq("status", 1);//已删除的不显示
		
		List<StockEntity> list = systemService.getListByCriteriaQuery(cse,false);
		Map<String, StockEntity> allstockmap = new HashMap<String, StockEntity>();
		for(StockEntity s:list){
			allstockmap.put(s.getId(), s);
		}
		
		
		List<Map<String, List<StockSegmentEntity>>> allstockseginfolist = new ArrayList<Map<String, List<StockSegmentEntity>>>();
		
		
		//2、遍历仓库id去获取全部区域信息,存到list<map>里面
		if( list != null && list.size()>0 ){
			
			
			
			for( int i = 0; i < list.size(); i++){
				StockEntity stock = list.get(i);
				
				
				Map<String, List<StockSegmentEntity>> stockseginfo = new HashMap<String, List<StockSegmentEntity>>();//用于存储父节点id和子节点之间的关系
				
				CriteriaQuery css = new CriteriaQuery(StockSegmentEntity.class);
				css.eq("stockid", stock);
				css.notEq("status", 1);
				List<StockSegmentEntity> stockSegmentList = systemService.getListByCriteriaQuery(css,false);//所有的节点信息
				
				for( int k = 0; k < stockSegmentList.size(); k++ ){
					StockSegmentEntity stockSegmentEntity = stockSegmentList.get(k);

					
					if( StringUtil.isEmpty(stockSegmentEntity.getPreid())){
						List<StockSegmentEntity> childlist = stockseginfo.get("root");//根节点的上级存放在root的key里面，便于下面递归处理
						if( null == childlist || childlist.size() == 0 ){//还没有
							childlist = new ArrayList<StockSegmentEntity>();
							childlist.add(stockSegmentEntity);
							stockseginfo.put("root", childlist);
						}else{
							childlist.add(stockSegmentEntity);
						}
					}
					else {//存在父节点id，表示当前节点是子节点，加入到父节点的队列里面去
						List<StockSegmentEntity> childlist = stockseginfo.get(stockSegmentEntity.getPreid());//获取父节点下是否挂有子节点
						if( null == childlist || childlist.size() == 0 ){//还没有
							childlist = new ArrayList<StockSegmentEntity>();
							childlist.add(stockSegmentEntity);
							stockseginfo.put(stockSegmentEntity.getPreid(), childlist);
						}else{
							childlist.add(stockSegmentEntity);
						}
					}
				}
				
				allstockseginfolist.add(stockseginfo);
				
			}
		}

		//3、组装成json
		JSONArray array = StockSegment2Json(allstockseginfolist,allstockmap);
		j.setObj(array);
		j.setSuccess(true);
		
		return j;
	}
	
	public AjaxJson del(StockSegmentEntity stockSegment, HttpServletRequest request,SystemService systemService,StockSegmentServiceI stockSegmentService) {
		AjaxJson j = new AjaxJson();
		String message = "";
		stockSegment = systemService.getEntity(StockSegmentEntity.class, stockSegment.getId());
		
		TSUser user = ResourceUtil.getSessionUserName();

		CriteriaQuery cse = new CriteriaQuery(StockEntity.class);
		cse.eq("companyid", user.getCompany());
		cse.eq("id", stockSegment.getStockid().getId());
		List<StockEntity> list = systemService.getListByCriteriaQuery(cse,false);
		if( null == list || list.size() <=0 ){
			j.setMsg("该仓库区域不属于该公司，不能删除！");
			j.setSuccess(false);
			return j;
		}
		
		
		//0、如果该节点是根节点，不允许删除，需要在删除仓库接口删除
		/*if( StringUtil.isEmpty(stockSegment.getPreid())){
			j.setMsg("根节点不允许删除！");
			j.setSuccess(false);
			return j;
		}*/
		

		//1、如果该节点是叶子节点
		CriteriaQuery css = new CriteriaQuery(StockSegmentEntity.class);
		css.eq("stockid", stockSegment.getStockid());
		css.eq("preid", stockSegment.getId());
		css.notEq("status", 1);
		List<StockSegmentEntity> seglist = systemService.getListByCriteriaQuery(css,false);
		if( null == seglist || seglist.size() <= 0 ){//叶子节点
			//2、如果该叶子节点还有库存商品，不允许删除
			CriteriaQuery csp = new CriteriaQuery(StockProductEntity.class);
			csp.eq("companyid", user.getCompany());
			csp.eq("stockSegment", stockSegment);
			csp.notEq("stocknum", new BigDecimal(0));
			
			List<StockProductEntity> stockproductlist = systemService.getListByCriteriaQuery(csp,false);
			if( null != stockproductlist && stockproductlist.size() > 0 ){
				j.setMsg("该库存区域库存商品不为0，不能删除");
				j.setSuccess(false);
				return j;
			}
			
			//删除叶子区域
			//stockSegmentService.delete(stockSegment);
			stockSegment.setStatus(1);//标记删除
			stockSegmentService.updateEntitie(stockSegment);
			message = "仓库区域删除成功("+ stockSegmentEntity2Json(stockSegment) + ")";
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			
			String loginfo = "删除仓库区域："+stockSegment.getName()+" ,路径" + stockSegment.getFullpathname();
			opLog(request, OperationLogUtil.STOCK_SEGMENT_DEL, stockSegment, loginfo );
			
			if( StringUtil.isEmpty(stockSegment.getPreid())){//删除整个仓库根节点的情况，仓库表一并删除
				StockEntity companyStock = systemService.getEntity(StockEntity.class, stockSegment.getStockid().getId());
				message = "您的仓库【" + companyStock.getStockname() + "】已经删除!";
				//systemService.delete(companyStock);
				companyStock.setStatus(1);//标记删除
				systemService.updateEntitie(companyStock);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
				
				loginfo = "删除仓库："+companyStock.getStockname();
				opLog(request, OperationLogUtil.STOCK_DEL, companyStock, loginfo );
			}
			
		}else{//如果该节点不是叶子节点，取出该节点和它的所有子节点id，并查询这些区域是否都还有库存商品，如果有则不允许删除
			List<StockSegmentEntity> sonlist = getAllSonsSegments(stockSegment,systemService);
			String idstr = segmentListId2SqlStr(sonlist);
			CriteriaQuery csp = new CriteriaQuery(StockProductEntity.class);
			csp.eq("companyid", user.getCompany());
			csp.in("stockSegment", sonlist.toArray());
			csp.notEq("stocknum", new BigDecimal(0));
			List<StockProductEntity> stockproductlist = systemService.getListByCriteriaQuery(csp,false);
			if( null != stockproductlist && stockproductlist.size() > 0 ){
				j.setMsg("该库存区域及子区域库存商品不为0，不能删除");
				j.setSuccess(false);
				return j;
			}
			
			//stockSegmentService.deleteAllEntitie(sonlist);
			for(StockSegmentEntity s: sonlist){
				s.setStatus(1);//标记删除
			}
			systemService.batchUpdate(sonlist);
			message = "仓库区域删除成功("+ stockSegmentEntityList2Json(sonlist) + ")";
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			
			String loginfo = "删除仓库区域："+stockSegment.getName()+" ,路径" + stockSegment.getFullpathname();
			opLog(request, OperationLogUtil.STOCK_SEGMENT_DEL, stockSegment, loginfo );
			
			if( StringUtil.isEmpty(stockSegment.getPreid())){//删除整个仓库根节点的情况，仓库表一并删除
				StockEntity companyStock = systemService.getEntity(StockEntity.class, stockSegment.getStockid().getId());
				message = "您的仓库【" + companyStock.getStockname() + "】已经删除!";
				//systemService.delete(companyStock);
				companyStock.setStatus(1);//标记删除
				systemService.updateEntitie(companyStock);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
				
				loginfo = "删除仓库："+companyStock.getStockname();
				opLog(request, OperationLogUtil.STOCK_DEL, companyStock, loginfo );
			}
		}
		
		
		
		j.setObj(stockSegment.getId());
		
		
		
		j.setMsg(message);
		return j;
	}
	
	public JSONArray StockSegment2Json(List<Map<String, List<StockSegmentEntity>>> allstockseginfolist,Map<String, StockEntity> allstockmap ){
		
		JSONArray array = new JSONArray();
		if( null != allstockseginfolist && allstockseginfolist.size()> 0 ){
			for( int i = 0; i < allstockseginfolist.size(); i++ ){
				List<StockSegmentEntity> rootlist = allstockseginfolist.get(i).get("root");//获取根节点，根节点一个仓库只有一个，该list只有1个对象
				if(rootlist != null){
					JSONObject tree = Segments2JsonObj(allstockseginfolist.get(i),rootlist.get(0),allstockmap);
					array.add(tree);
				}
			}
		}
		
		return array;
	}
	
	
	public JSONObject Segments2JsonObj(Map<String, List<StockSegmentEntity>> segmeninfo,StockSegmentEntity stockSegmentEntity,Map<String, StockEntity> allstockmap){
		JSONObject tree = new JSONObject();
		tree.put("id", stockSegmentEntity.getId() );
		
		tree.put("stockid", stockSegmentEntity.getStockid().getId());
		/*Map<String,String> info = new HashMap<String,String>();
		info.put("remarks", stockSegmentEntity.getRemarks());
		info.put("update_time", DateUtils.date3Str(stockSegmentEntity.getUpdateTime(), "yyyy-MM-dd HH:mm:ss"));
		tree.put("info", info);*/
		if( StringUtil.isEmpty(stockSegmentEntity.getPreid()) ){//父节点
			StockEntity stock = allstockmap.get(stockSegmentEntity.getStockid().getId() );
			if( null != stock ){
				StringBuffer info = new StringBuffer();
				
				TSUser user = stock.getDutyperson();
				try{
					if(user != null){
						info.append("责任人：").append(user.getRealname()).append(" ");
						info.append("电话：").append(user.getMobilephone());
					}else{
						info.append("责任人：").append("无").append(" ");
						info.append("电话：").append("无");
					}
				}catch(Exception e){
					info.append("责任人：").append("无").append(" ");
					info.append("电话：").append("无");
				}
				tree.put("info", info.toString());
				tree.put("name", stock.getStockname());
			}
		}else{//子节点
			tree.put("info", stockSegmentEntity.getRemarks());
			tree.put("name", stockSegmentEntity.getName());
		}
		
		List<StockSegmentEntity> subs = segmeninfo.get(stockSegmentEntity.getId());
		if( null != subs && subs.size() > 0 ){
			JSONArray array = new JSONArray();
			for( int i = 0; i < subs.size(); i++ ){
				JSONObject sub = Segments2JsonObj( segmeninfo, subs.get(i), allstockmap );
				array.add(sub);
			}
			tree.put("subs", array);
		}
		
		
		return tree;
	}
	
	public List<StockSegmentEntity> getAllSonsSegments(StockSegmentEntity stockSegment,SystemService systemService){
		List<StockSegmentEntity> result = new ArrayList<StockSegmentEntity>();
		
		CriteriaQuery css = new CriteriaQuery(StockSegmentEntity.class);
		css.eq("stockid", stockSegment.getStockid());
		css.notEq("status", 1);
		List<StockSegmentEntity> stockSegmentList = systemService.getListByCriteriaQuery(css,false);//所有的节点信息
		
		Map<String, List<StockSegmentEntity>> stockseginfo = new HashMap<String, List<StockSegmentEntity>>();//用于存储父节点id和子节点之间的关系
		
		for( int k = 0; k < stockSegmentList.size(); k++ ){
			StockSegmentEntity stockSegmentEntity = stockSegmentList.get(k);

			
			if( StringUtil.isEmpty(stockSegmentEntity.getPreid())){
				List<StockSegmentEntity> childlist = stockseginfo.get("root");//根节点的上级存放在root的key里面，便于下面递归处理
				if( null == childlist || childlist.size() == 0 ){//还没有
					childlist = new ArrayList<StockSegmentEntity>();
					childlist.add(stockSegmentEntity);
					stockseginfo.put("root", childlist);
				}else{
					childlist.add(stockSegmentEntity);
				}
			}
			else {//存在父节点id，表示当前节点是子节点，加入到父节点的队列里面去
				List<StockSegmentEntity> childlist = stockseginfo.get(stockSegmentEntity.getPreid());//获取父节点下是否挂有子节点
				if( null == childlist || childlist.size() == 0 ){//还没有
					childlist = new ArrayList<StockSegmentEntity>();
					childlist.add(stockSegmentEntity);
					stockseginfo.put(stockSegmentEntity.getPreid(), childlist);
				}else{
					childlist.add(stockSegmentEntity);
				}
			}
		}
		
		result = getSonSegment(stockseginfo, stockSegment);
		result.add(stockSegment);//把自己也加进去
		
		return result;
	}
	
	public List<StockSegmentEntity> getSonSegment(Map<String, List<StockSegmentEntity>> stockseginfo, StockSegmentEntity stockSegment){
		List<StockSegmentEntity> list  = null;
		
		List<StockSegmentEntity> sons = stockseginfo.get(stockSegment.getId());
		if( null != sons && sons.size() > 0 ){
			list = new ArrayList<StockSegmentEntity>();
			list.addAll(sons);
			
			for( int i = 0; i < sons.size(); i++ ){
				StockSegmentEntity seg = sons.get(i);
				List<StockSegmentEntity> tmplist = getSonSegment(stockseginfo, seg);
				if( null != tmplist && tmplist.size() > 0){
					list.addAll(tmplist);
				}
			}
		}
		
		
		return list;
	}
	
	public String segmentListId2SqlStr(List<StockSegmentEntity> list){
		StringBuffer buf = new StringBuffer();
		for( int i = 0; i < list.size(); i++ ){
			StockSegmentEntity seg = list.get(i);
			buf.append(seg.getId());
			buf.append(",");
		}
		
		return buf.toString();
	}
	
	private String stockSegmentEntity2Json(StockSegmentEntity stockSegment){
		JSONObject json = new JSONObject();
		
		json.put("id", stockSegment.getId());
		json.put("name", stockSegment.getName());
		json.put("preid", stockSegment.getPreid());
		json.put("remarks", stockSegment.getRemarks());
		json.put("stockid", stockSegment.getStockid().getId());
		
		return json.toJSONString();
	}
	
	private String stockSegmentEntityList2Json(List<StockSegmentEntity> list){
		JsonArray array = new JsonArray();
		for( int i = 0; i < list.size(); i++ ){
			array.add( stockSegmentEntity2Json(list.get(i)) );
		}
		return array.toString();
	}
	
}