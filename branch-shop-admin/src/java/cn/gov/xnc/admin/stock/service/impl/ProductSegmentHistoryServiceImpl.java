package cn.gov.xnc.admin.stock.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.entity.ProductSegmentHistoryEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.ProductSegmentHistoryServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("productSegmentHistoryService")
@Transactional
public class ProductSegmentHistoryServiceImpl extends CommonServiceImpl implements ProductSegmentHistoryServiceI {
	

	public boolean save( TSUser user, ProductSegmentHistoryEntity productSegmentHistory, String errMsg ){
		
		//根据Segmentid查历史表，如果已经有记录则直接返回
				CriteriaQuery ch = new CriteriaQuery(ProductSegmentHistoryEntity.class);
				ch.eq("stockid", productSegmentHistory.getStockid());
				ch.eq("segmentid", productSegmentHistory.getSegmentid());
				ch.eq("productid", productSegmentHistory.getProductid());
				
				List<ProductSegmentHistoryEntity> tmplist = getListByCriteriaQuery(ch, false);
				if( null != tmplist && tmplist.size() > 0 ){
					productSegmentHistory = tmplist.get(0);
					
					return true;
				}
				
				//1、根据传进来的segmentid获取出segment对象和stock对象，获取不到则返回错误
				//
				StockSegmentEntity segment = getEntity(StockSegmentEntity.class, productSegmentHistory.getSegmentid().getId());
				if( null == segment ){
					errMsg = "您的输入参数有误，请稍后重试";
					return false;
				}
				StockEntity stock = getEntity(StockEntity.class, segment.getStockid().getId());
				if( null == stock ){
					errMsg = "您的输入参数有误，请稍后重试";
					return false;
				}
				
				//2、根据segment对象拼接出他的完成名称
				CriteriaQuery css = new CriteriaQuery(StockSegmentEntity.class);
				css.eq("stockid", segment.getStockid());
				List<StockSegmentEntity> seglist = getListByCriteriaQuery(css,false);
				Map<String, StockSegmentEntity> segmap = new HashMap<String, StockSegmentEntity>();
				for(StockSegmentEntity s: seglist){
					segmap.put(s.getId(), s);
				}
				
				List<StockSegmentEntity> segfulllink = new ArrayList<StockSegmentEntity>();
				StockSegmentEntity tmp = segment;
				segfulllink.add(tmp);
				while(StringUtil.isNotEmpty(tmp.getPreid())){
					tmp = segmap.get(tmp.getPreid());
					if( null == tmp ){
						break;
					}
					segfulllink.add(tmp);
				}
				StringBuffer buf = new StringBuffer();
				for( int i = segfulllink.size()-1; i >=0 ; i--  ){
					StockSegmentEntity s = segfulllink.get(i);
					buf.append(s.getName());
					if( i != 0 ){
						buf.append("->");
					}
				}
				productSegmentHistory.setSegmentfullname(buf.toString());
				
				//3、保存进数据库
				productSegmentHistory.setCompany(user.getCompany());
				
				save(productSegmentHistory);
				
				return true;
	}
}