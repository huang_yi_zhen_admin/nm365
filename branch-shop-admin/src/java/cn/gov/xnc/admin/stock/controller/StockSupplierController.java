package cn.gov.xnc.admin.stock.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.stock.entity.StockSupplierEntity;
import cn.gov.xnc.admin.stock.service.StockSupplierServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 库存商品供应商信息
 * @author zero
 * @date 2017-05-18 14:26:52
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockSupplierController")
public class StockSupplierController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockSupplierController.class);

	@Autowired
	private StockSupplierServiceI stockSupplierService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 库存商品供应商信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockSupplier(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockIOSupplierList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockSupplierEntity stockSupplier,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockSupplierEntity.class, dataGrid);
		cq.eq("company", user.getCompany());
		cq.eq("status", 0);
		//查询条件组装器
		
		if(StringUtil.isNotEmpty(stockSupplier.getName()) ){
			cq.like("name", "%"+stockSupplier.getName()+"%");
			stockSupplier.setName(null);
		}
		
		if(StringUtil.isNotEmpty(stockSupplier.getCode()) ){
			cq.like("code", "%"+stockSupplier.getCode()+"%");
			stockSupplier.setCode(null);
		}
		
		
		HqlGenerateUtil.installHql(cq, stockSupplier, request.getParameterMap());
		this.stockSupplierService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除库存商品供应商信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockSupplierEntity stockSupplier, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockSupplier = systemService.getEntity(StockSupplierEntity.class, stockSupplier.getId());
		message = "供应商信息删除成功";
		stockSupplier.setStatus(1);
		stockSupplierService.updateEntitie(stockSupplier);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		systemService.opLog(request, OperationLogUtil.STOCK_SUPPLIER_DEL, stockSupplier, stockSupplier.getName()+" " + stockSupplier.getCode() + " " + message);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加库存商品供应商信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockSupplierEntity stockSupplier, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(stockSupplier.getId())) {
			message = "供应商信息更新成功";
			StockSupplierEntity t = stockSupplierService.get(StockSupplierEntity.class, stockSupplier.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockSupplier, t);
				stockSupplierService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
				
				systemService.opLog(request, OperationLogUtil.STOCK_SUPPLIER_UPDATE, stockSupplier, stockSupplier.getName()+" " + stockSupplier.getCode() + " " + message);

			} catch (Exception e) {
				e.printStackTrace();
				message = "库存商品供应商信息更新失败";
			}
		} else {
			message = "库存商品供应商信息添加成功";
			stockSupplierService.save(stockSupplier);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			
			systemService.opLog(request, OperationLogUtil.STOCK_SUPPLIER_ADD, stockSupplier, stockSupplier.getName()+" " + stockSupplier.getCode() + " " + message);

		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 库存商品供应商信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockSupplierEntity stockSupplier, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockSupplier.getId())) {
			stockSupplier = stockSupplierService.getEntity(StockSupplierEntity.class, stockSupplier.getId());
			req.setAttribute("stockSupplierPage", stockSupplier);
		}
		return new ModelAndView("admin/stock/stockIOSupplier");
	}
}
