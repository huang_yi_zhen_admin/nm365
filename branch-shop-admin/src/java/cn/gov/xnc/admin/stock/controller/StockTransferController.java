package cn.gov.xnc.admin.stock.controller;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockTransferEntity;
import cn.gov.xnc.admin.stock.service.StockTransferServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 调拨单信息
 * @author zero
 * @date 2018-01-09 09:14:56
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockTransferController")
public class StockTransferController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockTransferController.class);

	@Autowired
	private StockTransferServiceI stockTransferService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 调拨单信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockTransfer(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		request.setAttribute("fromStocklist", stockList);
		request.setAttribute("toStocklist", stockList);
		
		CriteriaQuery cquser = new CriteriaQuery(TSUser.class);
		cquser.eq("company", user.getCompany());
		cquser.in("type", "1,2".split(","));
		cquser.add();
		List<TSUser> operatorlist = systemService.getListByCriteriaQuery(cquser,false);
		request.setAttribute("operatorlist", operatorlist);
		return new ModelAndView("admin/stock/stockTransferList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockTransferEntity stockTransfer,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockTransferEntity.class, dataGrid);
		if( null!=stockTransfer.getCreator() && StringUtil.isNotEmpty(stockTransfer.getCreator().getRealname()) ){
			cq.createAlias("creator", "oper");
			cq.add(Restrictions.like("oper.realname", "%"+stockTransfer.getCreator().getRealname()+"%"));
			stockTransfer.getCreator().setRealname(null);
		}
		if( null != stockTransfer.getFromstockid() && StringUtil.isNotEmpty(stockTransfer.getFromstockid().getId() )){
			cq.createAlias("fromstockid", "fromstock");
			cq.add(Restrictions.eq("fromstock.id",stockTransfer.getFromstockid().getId()));
			stockTransfer.getFromstockid().setId(null);;
		}
		if( null != stockTransfer.getTostockid() && StringUtil.isNotEmpty(stockTransfer.getTostockid().getId() )){
			cq.createAlias("tostockid", "tostock");
			cq.add(Restrictions.eq("tostock.id",stockTransfer.getTostockid().getId()));
			stockTransfer.getTostockid().setId(null);;
		}
		
		stockTransfer.setCompanyid(user.getCompany());
		cq.createAlias("companyid", "com");
		cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createtime", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat), DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createtime", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createtime", DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}
		
		cq.addOrder("createtime", SortDirection.desc);
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockTransfer, request.getParameterMap());
		this.stockTransferService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除调拨单信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockTransferEntity stockTransfer, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockTransfer = systemService.getEntity(StockTransferEntity.class, stockTransfer.getId());
		message = "调拨单信息删除成功";
		stockTransferService.delete(stockTransfer);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加调拨单信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockTransferEntity stockTransfer, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			message = "调拨单信息添加成功";
			j = stockTransferService.saveStockTransfer(stockTransfer, request);
		} catch (Exception e) {
			message = "调拨单信息添加失败";
			j.setSuccess(false);
			logger.error(e);
		}
		
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		j.setMsg(message);
		return j;
	}

	/**
	 * 调拨单信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockTransferEntity stockTransfer, HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		req.setAttribute("fromStocklist", stockList);
		req.setAttribute("toStocklist", stockList);
		
		if (StringUtil.isNotEmpty(stockTransfer.getId())) {
			stockTransfer = stockTransferService.getEntity(StockTransferEntity.class, stockTransfer.getId());
			req.setAttribute("stockTransferPage", stockTransfer);
		}
		return new ModelAndView("admin/stock/stockTransfer");
	}
}
