package cn.gov.xnc.admin.stock.service;

import cn.gov.xnc.admin.stock.entity.ProductSegmentHistoryEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface ProductSegmentHistoryServiceI extends CommonService{
	
	public boolean save( TSUser user, ProductSegmentHistoryEntity productSegmentHistory, String errMsg );

}
