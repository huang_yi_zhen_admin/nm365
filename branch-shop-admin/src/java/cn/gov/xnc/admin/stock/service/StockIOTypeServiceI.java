package cn.gov.xnc.admin.stock.service;

import java.util.List;
import java.util.Map;

import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockIOTypeServiceI extends CommonService{

	public Map<String, Object> getParams(StockIOTypeEntity stockIOTypeEntity);
	
	public List<StockIOTypeEntity> findListByParams(Map<String, Object> params);
	
	/**
	 * 检查出入库类型
	 * @param params
	 * @return
	 */
	public StockIOTypeEntity checkStockIOType(Map<String, Object> params);
}
