package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.admin.stock.service.StockServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("stockProductService")
@Transactional
public class StockProductServiceImpl extends CommonServiceImpl implements StockProductServiceI {
	
	@Autowired
	private StockProductPriceServiceI stockProductPriceService;
	@Autowired
	private StockServiceI stockService;

	@Override
	public List<StockProductEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class);
		ProductEntity productEntity = (ProductEntity) params.get("productEntity");
		if(productEntity != null){
			cq.eq("productid", productEntity);
		}
		StockEntity stockEntity = (StockEntity) params.get("stockEntity");
		if(stockEntity != null){
			cq.eq("stockid", stockEntity);
		}
		StockSegmentEntity segmentEntity = (StockSegmentEntity) params.get("segmentEntity");
		if(segmentEntity != null){
			cq.eq("stockSegment", segmentEntity);
		}
		TSCompany companyEntity = (TSCompany) params.get("companyEntity");
		if(companyEntity != null){
			cq.eq("companyid", companyEntity);
		}
		String status = (String) params.get("status");
		if(status != null){
			cq.eq("status", status);
		}
		cq.add();
		
		List<StockProductEntity> list = getListByCriteriaQuery(cq, false);
		return list;
	}

	/**
	 * 封装条件的参数
	 * @param productEntity
	 * @param stockEntity
	 * @param stockSegmentEntity
	 * @return
	 */
	public Map<String, Object> getParams(String ioType,ProductEntity productEntity,
			StockEntity stockEntity, 
			StockSegmentEntity stockSegmentEntity,
			StockProductPriceEntity productPriceEntity,
			StockProductEntity stockProductEntity,
			StockIODetailEntity stockIODetailEntity) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		if(ioType != null){
			params.put("ioType", ioType);
		}
		if(productEntity != null){
			params.put("productEntity", productEntity);
		}
		if(stockEntity != null){
			params.put("stockEntity", stockEntity);
		}
		if(stockSegmentEntity != null){
			params.put("segmentEntity", stockSegmentEntity);
		}
		if(user != null){
			params.put("companyEntity", user.getCompany());
		}
		if(productPriceEntity != null){
			params.put("stockProductPriceEntity", productPriceEntity);
		}
		if(stockIODetailEntity != null){
			params.put("stockIODetailEntity", stockIODetailEntity);
		}
		if(stockProductEntity != null){
			params.put("stockProductEntity", stockProductEntity);
			if(stockProductEntity.getStocknum() != null){
				params.put("stockNum", stockProductEntity.getStocknum());
			}
			if(stockProductEntity.getTotalprice() != null){
				params.put("totalPrice", stockProductEntity.getTotalprice());
			}
			if(stockProductEntity.getAverageprice() != null){
				params.put("avgPrice", stockProductEntity.getAverageprice());
			}
			if(stockProductEntity.getAlertnum() != null){
				params.put("alertNum", stockProductEntity.getAlertnum());
			}
		}
		return params;
	}
	
	public boolean updateStockProductEntity(Map<String, Object> params){
		StockSegmentEntity segmentEntity = (StockSegmentEntity) params.get("segmentEntity");
		if(segmentEntity == null){
			return false;
		}
		TSCompany companyEntity = (TSCompany) params.get("companyEntity");
		if(companyEntity == null){
			return false;
		}
		ProductEntity productEntity = (ProductEntity) params.get("productEntity");
		if(productEntity == null){
			return false;
		}
		StockProductEntity stockProductEntity = (StockProductEntity) params.get("stockProductEntity");
		if(stockProductEntity == null){
			return false;
		}
		
		//当前库存商品数量
		BigDecimal cur_num =  ConvertTool.toBigDecimal(params.get("stockNum"));
		//当前库存商品总价
		BigDecimal cur_totalprice = ConvertTool.toBigDecimal(params.get("totalPrice"));
		//当前库存商品均价
		BigDecimal cur_averageprice = ConvertTool.toBigDecimal(params.get("avgPrice"));

		//出/入库详情-数量总价
		BigDecimal detail_num = ConvertTool.toBigDecimal(params.get("op_stockNum"));
		//出/入库详情-总价
		BigDecimal detail_totalprice = ConvertTool.toBigDecimal(params.get("op_totalPrice"));
		//出/入库详情-均价
		BigDecimal detail_averageprice = ConvertTool.toBigDecimal(params.get("op_avgPrice"));
		
		String ioType = (String) params.get("ioType");
		if(ioType == null || ioType.isEmpty()){
			return false;
		}
		
		if(StockIOType.IN.equals(ioType)){
			//计算最新库存总价、均价、数量
			//最新库存商品数量 =  当前库存商品数量 + 出入库商品数量
			cur_num = cur_num.add(detail_num);
			//最新库存商品总价 = 当前库存商品总价 + 出入库商品总价
			cur_totalprice = cur_totalprice.add(detail_totalprice);
			//最新库存商品均价 = 最新库存商品总价 ÷ 最新库存商品数量
			cur_averageprice = cur_totalprice.divide(cur_num, 4, BigDecimal.ROUND_HALF_UP);
		}else if(StockIOType.OUT.equals(ioType)){
			//计算最新库存总价、均价、数量
			//最新库存商品数量 =  当前库存商品数量 - 出入库商品数量
			cur_num = cur_num.subtract(detail_num);
			//最新库存商品总价 = 当前库存商品总价 - 出入库商品总价
			cur_totalprice = cur_totalprice.subtract(detail_totalprice);
			//最新库存商品均价 = 最新库存商品总价 ÷ 最新库存商品数量
			if(cur_num.compareTo(new BigDecimal(0.00)) > 0 && cur_totalprice.compareTo(new BigDecimal(0.00)) > 0){
				cur_averageprice =  cur_totalprice.divide(cur_num, 4, BigDecimal.ROUND_HALF_UP);
			}else{
				cur_averageprice = new BigDecimal(0.00);
			}
			
		}else{
			return false;
		}
		
		//当前库存商品-预警值
		BigDecimal alertnum  = ConvertTool.toBigDecimal(params.get("alertNum"));
		/**库存状态 1 充足 2 缺货 3 补货*/
		String status = "1";
		if(cur_num.compareTo(new BigDecimal(0.00)) > 0 && cur_num.compareTo(alertnum) > 0){
			status = "1";
		}
		if(cur_num.compareTo(new BigDecimal(0.00)) > 0 && cur_num.compareTo(alertnum) <= 0){
			status = "2";
		}
		if(cur_num.compareTo(new BigDecimal(0.00)) <= 0){
			status = "3";
		}
		
		stockProductEntity.setStockSegment(segmentEntity);
		stockProductEntity.setCompanyid(companyEntity);
		stockProductEntity.setProductid(productEntity);
		stockProductEntity.setStatus(status);
		stockProductEntity.setStocknum(cur_num);
		stockProductEntity.setAverageprice(cur_averageprice);
		stockProductEntity.setTotalprice(cur_totalprice);
		updateEntitie(stockProductEntity);
		return true;
	}
	
	public boolean saveStockProductEntity(Map<String, Object> params){
		StockEntity stockEntity = (StockEntity) params.get("stockEntity");
		if(stockEntity == null){
			return false;
		}
		StockSegmentEntity segmentEntity = (StockSegmentEntity) params.get("segmentEntity");
		if(segmentEntity == null){
			return false;
		}
		TSCompany company = (TSCompany) params.get("companyEntity");
		if(company == null){
			return false;
		}
		ProductEntity product = (ProductEntity) params.get("productEntity");
		if(product == null){
			return false;
		}
		
		//添加库存商品状况信息
		StockProductEntity stockProductEntity = new StockProductEntity();
		stockProductEntity.setCompanyid(company);
		stockProductEntity.setProductid(product);
		stockProductEntity.setStockid(stockEntity);
		stockProductEntity.setAlertnum(new BigDecimal(0.00));
		stockProductEntity.setStockSegment(segmentEntity);
		
		//出/入库详情-数量总价
		BigDecimal detail_num = ConvertTool.toBigDecimal(params.get("op_stockNum"));
		//出/入库详情-总价
		BigDecimal detail_totalprice = ConvertTool.toBigDecimal(params.get("op_totalPrice"));
		//出/入库详情-均价
		BigDecimal detail_averageprice = ConvertTool.toBigDecimal(params.get("op_avgPrice"));
		
		//当前库存商品-预警值
		BigDecimal alertnum  = stockProductEntity.getAlertnum();
		/**库存状态 1 充足 2 缺货 3 补货*/
		String status = "1";
		if(detail_num.compareTo(new BigDecimal(0.00)) > 0 && detail_num.compareTo(alertnum) > 0){
			status = "1";
		}
		if(detail_num.compareTo(new BigDecimal(0.00)) > 0 && detail_num.compareTo(alertnum) <= 0){
			status = "2";
		}
		if(detail_num.compareTo(new BigDecimal(0.00)) <= 0){
			status = "3";
		}
		
		stockProductEntity.setStatus(status);
		stockProductEntity.setTotalprice(detail_totalprice);
		stockProductEntity.setStocknum(detail_num);
		stockProductEntity.setAverageprice(detail_averageprice);
		save(stockProductEntity);
		return true;
	}

	/**
	 * 自定义：保存或更新库存商品信息状况信息
	 * @param stockproduct
	 * @param type
	 * @param stock
	 * @param stockIODetailEntity
	 * @return
	 */
	public boolean saveOrUpdateStockProduct(Map<String, Object> params){
		try {
			StockEntity stockEntity = (StockEntity) params.get("stockEntity");
			if(stockEntity == null){
				return false;
			}
			StockSegmentEntity segmentEntity = (StockSegmentEntity) params.get("segmentEntity");
			if(segmentEntity == null){
				return false;
			}
			ProductEntity productEntity = (ProductEntity) params.get("productEntity");
			if(productEntity == null){
				return false;
			}
			//根据仓库、产品、分区条件获取库存商品状况
			Map<String, Object> stockProductParams = this.getParams(null,productEntity, stockEntity, segmentEntity,null,null,null);
			List<StockProductEntity> stockProductList = this.findListByParams(stockProductParams);
			if(null != stockProductList && stockProductList.size() > 0){
				StockProductEntity stockProductEntity = stockProductList.get(0);
				params.put("stockNum",stockProductEntity.getStocknum());
				params.put("avgPrice",stockProductEntity.getAverageprice());
				params.put("totalPrice",stockProductEntity.getTotalprice());
				params.put("alertNum",stockProductEntity.getAlertnum());
				params.put("stockProductEntity", stockProductEntity);
				//更新库存商品状况
				this.updateStockProductEntity(params);
			}else{
				//保存库存商品状况
				this.saveStockProductEntity(params);
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<StockProductEntity> checkStockProduct(Map<String, Object> params) {
		TSUser user = ResourceUtil.getSessionUserName();
		ProductEntity productEntity = (ProductEntity) params.get("productEntity");
		if(productEntity == null){
			return null;
		}
		if(user == null){
			return null;
		}
		//检查仓库
		Map<String, Object> stockParams = new HashMap<String, Object>();
		stockParams.put("companyid", user.getCompany().getId());
		stockParams.put("status", "1");
		List<StockEntity> stocks = stockService.findListByParams(stockParams);
		
		List<StockProductEntity> list = null;
		if(stocks.size() > 0){
			//检查库存商品状况
			for (StockEntity stockEntity : stocks) {
				Map<String, Object> stockProductParams = new HashMap<String,Object>();
				stockProductParams.put("stockEntity", stockEntity);
				stockProductParams.put("productEntity", productEntity);
				stockProductParams.put("status", "1");
				List<StockProductEntity> stockProducts = this.findListByParams(stockProductParams);
				
				if(stockProducts.size() > 0){
					list = new ArrayList<StockProductEntity>();
					for (StockProductEntity stockProductEntity : stockProducts) {
						list.add(stockProductEntity);
					}
				}
			}
		}
		return list;
	}
}