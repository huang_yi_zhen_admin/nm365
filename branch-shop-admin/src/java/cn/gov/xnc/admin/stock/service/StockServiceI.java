package cn.gov.xnc.admin.stock.service;

import java.util.List;
import java.util.Map;

import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockServiceI extends CommonService{

	public List<StockEntity> findListByParams(Map<String, Object> params);
	
	public void saveWithSegment( StockEntity stock );
}
