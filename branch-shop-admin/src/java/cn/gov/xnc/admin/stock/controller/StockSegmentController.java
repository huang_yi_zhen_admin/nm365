package cn.gov.xnc.admin.stock.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockSegmentServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 出库详细单
 * @author zero
 * @date 2017-04-20 14:38:36
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockSegmentController")
public class StockSegmentController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockSegmentController.class);

	@Autowired
	private StockSegmentServiceI stockSegmentService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 出库详细单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView stockSegment(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockSegmentList");
	}
	
	/**
	 * 仓库区域选择弹框页面
	 * */
	@RequestMapping(value = "selstocksegment")
	public ModelAndView selstocksegment( HttpServletRequest request) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		String stockid = request.getParameter("stockid");
		if( StringUtil.isNotEmpty(stockid) ){
			request.setAttribute("stockid", stockid);
		}
		
		String productid = request.getParameter("productid");
		if( StringUtil.isNotEmpty(productid)){
			request.setAttribute("productid", productid);
		}
		
		return new ModelAndView("admin/stock/selStockSegment");
	}
	
	/**
	 * 新增仓库子区域
	 * 
	 * */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate( HttpServletRequest request) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		String preid = request.getParameter("preid");
		String id = request.getParameter("id");
		
		if( StringUtil.isNotEmpty(preid)){//新增
			request.setAttribute("preid", preid);
		}
		
		if( StringUtil.isNotEmpty(id)){//编辑
			request.setAttribute("id", id);
			StockSegmentEntity stockSegment = stockSegmentService.getEntity(StockSegmentEntity.class, id);
			if( null != stockSegment ){
				request.setAttribute("name", stockSegment.getName());
				request.setAttribute("remarks", stockSegment.getRemarks());
			}
		}
		
		return new ModelAndView("admin/stock/stockSegment");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(StockSegmentEntity stockSegment,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(StockSegmentEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockSegment, request.getParameterMap());
		this.stockSegmentService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	

	
	@RequestMapping(value = "getAllStockInfo")
	@ResponseBody
	public AjaxJson getAllStockInfo( HttpServletRequest request) {
		AjaxJson j = stockSegmentService.getAllStockInfo(request, systemService);
		
		
		return j;
	}
	
	
	
	

	/**
	 * 删除仓库区域
	 * 
	 * @return
	 */
	@RequestMapping(value = "delsegment")
	@ResponseBody
	public AjaxJson del(StockSegmentEntity stockSegment, HttpServletRequest request) {
		AjaxJson j = stockSegmentService.del(stockSegment, request, systemService, stockSegmentService);
		
		return j;
	}
	
	
	

	/**
	 * 新增或保存
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockSegmentEntity stockSegment, HttpServletRequest request) {
		AjaxJson j = stockSegmentService.save(stockSegment, request, systemService, stockSegmentService);
		
		return j;
	}

	/**
	 * 出库详细单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(StockSegmentEntity stockSegment, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockSegment.getId())) {
			stockSegment = stockSegmentService.getEntity(StockSegmentEntity.class, stockSegment.getId());
			req.setAttribute("stockSegmentPage", stockSegment);
		}
		return new ModelAndView("admin/stock/stockSegment");
	}
	
	
	
	
	
	
	
	
}
