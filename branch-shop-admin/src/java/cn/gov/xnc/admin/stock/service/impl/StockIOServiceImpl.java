package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.configure.service.TSconfigureServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.admin.stock.service.StockIOServiceI;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("stockIOService")
@Transactional
public class StockIOServiceImpl extends CommonServiceImpl implements StockIOServiceI {
	
	private static final Logger logger = Logger.getLogger(StockProductServiceImpl.class);
	
	@Autowired
	private TSconfigureServiceI configureService;
	@Autowired
	private StockIODetailServiceI stockIODetailService;
	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private StockProductPriceServiceI stockProductPriceServiceI;
	@Autowired
	private SystemService systemService;

	@Override
	public StockEntity buildStockOrder() {
		return null;
	}

	@Override
	public List<StockIOEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class);
		String productId = (String) params.get("productId");
		if(productId != null){
			cq.eq("productid.id", productId);
		}
		
		String stockId = (String) params.get("stockId");
		if(stockId != null){
			cq.eq("stockid.id", stockId);
		}
		
		String companyid = (String) params.get("companyid");
		if(companyid != null){
			cq.eq("companyid.id", companyid);
		}
		
		cq.add();
		List<StockIOEntity> list = this.getListByCriteriaQuery(cq, false);
		return list;
	}

	@Override
	public Map<String, Object> getParams(ProductEntity product, StockEntity stock) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		if(product != null){
			params.put("productid", product.getId());
		}
		if(stock != null){
			params.put("stockid", stock.getId());
		}
		if(user != null){
			params.put("companyid", user.getCompany().getId());
		}
		return params;
	}

	@Override
	public AjaxJson saveStockIO(StockIOEntity companyStockOrder, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		String message;
		TSUser user = ResourceUtil.getSessionUserName();
		
		StockEntity checkStockEntity = companyStockOrder.getStockid();
		if(checkStockEntity == null){
			result.setMsg("请选择仓库！");
			result.setSuccess(false);
			return result;
		}else if(!StringUtil.isNotEmpty(checkStockEntity.getId())){
			result.setMsg("请选择仓库！");
			result.setSuccess(false);
			return result;
		}
		
		TSDictionary checkStockIOTypeEntity = companyStockOrder.getStocktypeid();
		if(checkStockIOTypeEntity == null){
			result.setMsg("请选择出入库类型！");
			result.setSuccess(false);
			return result;
		}else if(!StringUtil.isNotEmpty(checkStockIOTypeEntity.getId())){
			result.setMsg("请选择出入库类型！");
			result.setSuccess(false);
			return result;
		}
				
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		TSUser operator = systemService.findUniqueByProperty(TSUser.class, "id", companyStockOrder.getOperator().getId());
		StockEntity stock = systemService.findUniqueByProperty(StockEntity.class, "id", companyStockOrder.getStockid().getId());
		TSDictionary stockIOTypeEntity = systemService.findUniqueByProperty(TSDictionary.class, "id", companyStockOrder.getStocktypeid().getId());
		
		//创建基础数据
		StockIOEntity stockIOEntity = new StockIOEntity();
		stockIOEntity.setIdentifier(IdWorker.generateSequenceNo());
		stockIOEntity.setCompanyid(company);
		stockIOEntity.setCreatetime(DateUtils.getDate());
		stockIOEntity.setOperator(operator);
		stockIOEntity.setRemark(companyStockOrder.getRemark());
		stockIOEntity.setStockid(stock);
		stockIOEntity.setStocktypeid(stockIOTypeEntity);
		stockIOEntity.setStockIODetailList(null);
		
		//创建出入库详细数据
		List<Map<String, Object>> exps = stockIODetailService.buildStockIODetailParams(request);
		if(exps.size() <= 0){
			result.setMsg("请选择商品信息！");
			result.setSuccess(false);
			return result;
		}
		
		List<StockIODetailEntity> stockIODetailList = stockIODetailService.createStockIODetail(stockIOEntity, exps);
		
		result = saveStockIO(stockIOEntity, stockIODetailList, request);
		
		return result;
	}

	@Override
	public AjaxJson saveStockIO(StockIOEntity stockIOEntity, List<StockIODetailEntity> stockIODetailList, HttpServletRequest req) {
		AjaxJson result = new AjaxJson();
		String message;
		
		StockEntity checkStockEntity = stockIOEntity.getStockid();
		if(checkStockEntity == null){
			result.setMsg("出入仓库不能为空！");
			result.setSuccess(false);
			return result;
		}else if(!StringUtil.isNotEmpty(checkStockEntity.getId())){
			result.setMsg("出入仓库不能为空！");
			result.setSuccess(false);
			return result;
		}
		
		TSDictionary checkStockIOTypeEntity = stockIOEntity.getStocktypeid();
		if(checkStockIOTypeEntity == null){
			result.setMsg("出入库类型不能为空！");
			result.setSuccess(false);
			return result;
		}else if(!StringUtil.isNotEmpty(checkStockIOTypeEntity.getId())){
			result.setMsg("出入库类型不能为空！");
			result.setSuccess(false);
			return result;
		}
				
		//检查出入库类型
		if(null == checkStockIOTypeEntity){
			result.setSuccess(false);
			message = "未定义出入库类型";
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			result.setMsg(message);
			return result;
		}
		
		//出入库类型
		String type = checkStockIOTypeEntity.getDictionaryType();
		String typeName = checkStockIOTypeEntity.getDictionaryName();
		
		//检查是否锁仓
		String lockstockid = configureService.getLockedStockId();
		if( StringUtil.isNotEmpty(lockstockid)){
			if( lockstockid.equals(checkStockEntity.getId())){
				result.setSuccess(false);
				message = "仓库已经处于上锁状态，不能进行出入库操作";
				systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
				result.setMsg(message);
				return result;
			}
		}
		
		//创建出入库详细数据
		if(stockIODetailList.size() <= 0){
			result.setMsg("出入库商品详细不能为空！");
			result.setSuccess(false);
			return result;
		}
		
		//创建出入库详细数据
		if(stockIODetailList.size() > 0 ){
			//保存入库基本信息
			save(stockIOEntity);
			//批量保存出/入库详情信息
			batchSave(stockIODetailList);
			stockIOEntity.setStockIODetailList(stockIODetailList);
			updateEntitie(stockIOEntity);
			
			for (StockIODetailEntity stockIODetail : stockIODetailList) {
					
					StockEntity stockEntity = stockIOEntity.getStockid();
					ProductEntity productEntity = stockIODetail.getProductid();
					StockSegmentEntity segmentEntity = stockIODetail.getStockSegment();
					
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("ioType", type);
					params.put("stockEntity", stockEntity);
					params.put("segmentEntity", segmentEntity);
					params.put("companyEntity", checkStockEntity.getCompanyid());
					params.put("productEntity",productEntity);
					
					params.put("op_stockNum",ConvertTool.toBigDecimal(stockIODetail.getIostocknum()));
					params.put("op_totalPrice",ConvertTool.toBigDecimal(stockIODetail.getTotalprice()));
					params.put("op_avgPrice",ConvertTool.toBigDecimal(stockIODetail.getUnitprice()));
					//更新库存商品状况
					boolean check = stockProductService.saveOrUpdateStockProduct( params);
					if(!check){
						delete(stockIOEntity);
						deleteAllEntitie(stockIODetailList);
						result.setSuccess(false);
						message = typeName + "失败";
						systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
						result.setMsg(message);
						return result;
					}
					
					//更新库存商品所有统计价格
					stockProductPriceServiceI.updateStockProductPrice(productEntity, stockEntity,req);
					
			}
			opLog( req, OperationLogUtil.STOCK_IN, stockIOEntity, "入库操作,入库单号："+stockIOEntity.getId(), checkStockEntity.getCompanyid(), stockIOEntity.getOperator(), checkStockEntity);
		}else{
			result.setSuccess(false);
			message = "入库操作失败，没有找到您要入库的商品详细信息";
		}
		systemService.addLog("出入库操作", Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		return result;
	}
	
}