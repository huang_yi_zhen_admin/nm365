package cn.gov.xnc.admin.stock.service;

import java.util.List;
import java.util.Map;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockProductServiceI extends CommonService {

	/**
	 * 自定义：更新库存商品信息状况信息 - 根据出入库情况更新
	 * 
	 * @param stockproduct
	 * @param type
	 * @param stock
	 * @param stockIODetailEntity
	 * @return
	 */
//	public boolean updateStockProductByStockIO(ProductEntity product, StockEntity stock, Map<String, Object> commonParams);
	
	/**
	 * 检测库存商品状况
	 * @param params
	 * @return
	 */
	public List<StockProductEntity> checkStockProduct(Map<String, Object> params);
	
	/**
	 * 自定义：更新库存商品信息
	 * @param params
	 * @return
	 */
	public boolean updateStockProductEntity(Map<String, Object> params);
	
	/**
	 * 自定义：保存库存商品信息
	 * @param params
	 * @return
	 */
	public boolean saveStockProductEntity(Map<String, Object> params);

	/**
	 * 自定义：根据条件获取列表
	 * 
	 * @param stockProductEntity
	 * @return
	 */
	public List<StockProductEntity> findListByParams(Map<String, Object> params);
	
	/**
	 * 自定义：保存或更新库存商品信息状况信息
	 * @param stockproduct
	 * @param type
	 * @param stock
	 * @param stockIODetailEntity
	 * @return
	 */
	public boolean saveOrUpdateStockProduct(Map<String, Object> params);

	/**
	 * 自定义：封装条件的参数
	 * @param productEntity
	 * @param stockEntity
	 * @param stockSegmentEntity
	 * @return
	 */
	public Map<String, Object> getParams(String ioType,ProductEntity productEntity,
			StockEntity stockEntity, StockSegmentEntity stockSegmentEntity,
			StockProductPriceEntity stockProductPriceEntity,
			StockProductEntity stockProductEntity,
			StockIODetailEntity stockIODetailEntity);
}
