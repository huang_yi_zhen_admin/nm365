package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.entity.StockTransferDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockTransferEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.admin.stock.service.StockIOServiceI;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockTransferServiceI;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;

@Service("stockTransferService")
@Transactional(rollbackForClassName={"Exception"})
public class StockTransferServiceImpl extends CommonServiceImpl implements StockTransferServiceI {

	@Autowired
	private StockIOServiceI stockIOServiceI;
	@Autowired
	private StockIODetailServiceI stockIODetailService;
	@Autowired
	private DictionaryService dictionaryService;
	
	@Override
	public AjaxJson saveStockTransfer(StockTransferEntity stockTransfer, HttpServletRequest req) throws Exception {
		AjaxJson json = new AjaxJson();
		//创建出库记录
		StockIOEntity outStockIO = buildStockIOByTransfer(stockTransfer, StockIOEntity.STOCK_OUT, req);
		List<StockIODetailEntity> outStockIODetailList = buildStockIODetailListByTransfer(outStockIO, StockIOEntity.STOCK_OUT, req);
		stockIOServiceI.saveStockIO(outStockIO, outStockIODetailList, req);
		
		//创建调拨记录
		StockTransferEntity stockTransferEntity = buildStockTransfer(stockTransfer, req);
		List<StockTransferDetailEntity> stockTransferDetailList = buildStockTransferDetailList(stockTransferEntity, req);
		saveStockTransfer(stockTransferEntity, stockTransferDetailList, req);
		
		//创建入库记录
		StockIOEntity inStockIO = buildStockIOByTransfer(stockTransfer, StockIOEntity.STOCK_IN, req);
		List<StockIODetailEntity> inStockIODetailList = buildStockIODetailListByTransfer(inStockIO, StockIOEntity.STOCK_IN, req);
		stockIOServiceI.saveStockIO(inStockIO, inStockIODetailList, req);
		
		json.setSuccess(true);

		json.setMsg("调拨单录入成功");
		
		return json;
	}

	@Override
	public AjaxJson saveStockTransfer(StockTransferEntity stockTransfer, List<StockTransferDetailEntity> stockTransferDetailList, HttpServletRequest req) throws Exception {
		AjaxJson json = new AjaxJson();
		if(null != stockTransferDetailList && stockTransferDetailList.size() > 0){
			
			save( stockTransfer );
			
			batchSave(stockTransferDetailList);
			
			json.setSuccess(true);

			json.setMsg("调拨单录入成功");
		}
		return json;
	}
	
	
	private StockIOEntity buildStockIOByTransfer(StockTransferEntity stockTransfer, String stockType, HttpServletRequest req) throws Exception{
		
		StockEntity stock = null;
		StockEntity fromStock = findUniqueByProperty(StockEntity.class,"id",stockTransfer.getFromstockid().getId());
		StockEntity toStock = findUniqueByProperty(StockEntity.class,"id",stockTransfer.getTostockid().getId());
		if(fromStock == null || StringUtil.isEmpty(fromStock.getId())){
			throw new Exception("调拨仓库不能为空！");
		}
		if(toStock == null || StringUtil.isEmpty(toStock.getId())){
			throw new Exception("目的仓库不能为空！");
		}
		TSUser operator = ResourceUtil.getSessionUserName();
		TSCompany company = findUniqueByProperty(TSCompany.class, "id", operator.getCompany().getId());
		
		//获取配置的出入库字典类型
		TSDictionary dict = new TSDictionary();
		if(StockIOEntity.STOCK_OUT.equals(stockType)){
			dict.setDictionaryType(StockIOType.OUT);
			dict.setDictionaryName(StockIOType.StockOutType.TRANSFER_OUT.getName());
			dict.setDictionaryDesc(StockIOType.StockOutType.TRANSFER_OUT.getName());
			dict.setDictionaryValue(StockIOType.StockOutType.TRANSFER_OUT.getValue());
			stock = fromStock;
		}else{
			dict.setDictionaryType(StockIOType.IN);
			dict.setDictionaryName(StockIOType.StockInType.TRANSFER_IN.getName());
			dict.setDictionaryDesc(StockIOType.StockInType.TRANSFER_IN.getName());
			dict.setDictionaryValue(StockIOType.StockInType.TRANSFER_IN.getValue());
			stock = toStock;
		}
		
		TSDictionary dictEntity = dictionaryService.check2addDictionaryItem(dict);
		
		//创建基础数据
		StockIOEntity stockio = new StockIOEntity();
		stockio.setIdentifier(IdWorker.generateSequenceNo());
		stockio.setCompanyid(company);
		stockio.setCreatetime(DateUtils.getDate());
		stockio.setOperator(operator);
		stockio.setRemark(stockTransfer.getRemark());
		stockio.setStockid(stock);
		stockio.setStocktypeid(dictEntity);
		stockio.setStockIODetailList(null);
		
		return stockio;
	};
	
	/**
	 * 组装调拨的出入库信息
	 * @param stockio
	 * @param stockType
	 * @param req
	 * @return
	 */
	private List<StockIODetailEntity> buildStockIODetailListByTransfer(StockIOEntity stockio, String stockType, HttpServletRequest req)  throws Exception{
		List<Map<String, Object>> paramlist = buildStockIODetailParams(stockType, req);
		List<StockIODetailEntity> stockIODetailList = stockIODetailService.createStockIODetail(stockio, paramlist);
		return stockIODetailList;
	};
	
	/**
	 * 组装调拨基础信息
	 * @param stockTransfer
	 * @param req
	 * @return
	 */
	private StockTransferEntity buildStockTransfer(StockTransferEntity stockTransfer, HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		StockTransferEntity stockTransferEntity = new StockTransferEntity();
		stockTransferEntity.setId( getTabelSequence(IdWorker.XNC_COMPANY_STOCK_TRANSFER) );
		stockTransferEntity.setCreator(user);
		stockTransferEntity.setCompanyid(user.getCompany());
		stockTransferEntity.setFromstockid(stockTransfer.getFromstockid());
		stockTransferEntity.setRemark(stockTransfer.getRemark());
		stockTransferEntity.setTostockid(stockTransfer.getTostockid());
		stockTransferEntity.setCreatetime(new Date());
		
		return stockTransferEntity;
	}
	
	/**
	 * 
	 * @param stockTransfer
	 * @param req
	 * @return
	 */
	private List<StockTransferDetailEntity> buildStockTransferDetailList(StockTransferEntity stockTransfer, HttpServletRequest req)  throws Exception{
		List<StockTransferDetailEntity> stockTransferDetailList = new ArrayList<StockTransferDetailEntity>();
		Integer exps = ConvertTool.toInt(ResourceUtil.getParameter("Len"));
		for (int i = 0; i < exps; i++) {
			String pid = req.getParameter("id-" + i);
			if(StringUtil.isEmpty(pid)){
				continue;
			}
			ProductEntity product = findUniqueByProperty(ProductEntity.class, "id", req.getParameter("id-" + i));
			ProductUnitEntity pUnit = findUniqueByProperty(ProductUnitEntity.class, "id", req.getParameter("unitid-" + i));
			
			StockSegmentEntity toSegment = findUniqueByProperty(StockSegmentEntity.class, "id", req.getParameter("toSegmentid-" + i));
			
			StockSegmentEntity fromSegment = findUniqueByProperty(StockSegmentEntity.class, "id", req.getParameter("fromSegmentid-" + i));
			
			StockTransferDetailEntity stockTransferDetail = new StockTransferDetailEntity();
			stockTransferDetail.setId(getTabelSequence( IdWorker.XNC_COMPANY_STOCK_TRANSFER_DETAIL) );
			stockTransferDetail.setProductid( product );
			stockTransferDetail.setFreightprice( ConvertTool.toBigDecimal(req.getParameter("freightprice-" + i)) );
			stockTransferDetail.setFromsegmentid(fromSegment);
			stockTransferDetail.setTosegmentid(toSegment);
			stockTransferDetail.setTransfernum( ConvertTool.toBigDecimal(req.getParameter("transferNum-" + i)) );
			stockTransferDetail.setUnitprice( ConvertTool.toBigDecimal(req.getParameter("unitprice-" + i)) );
			stockTransferDetail.setRemark( req.getParameter("remark-" + i) );
			stockTransferDetail.setTotalprice( stockTransferDetail.getTransfernum().multiply(stockTransferDetail.getUnitprice()) );
			stockTransferDetail.setTransferid(stockTransfer);
			stockTransferDetail.setUnitid(pUnit);
			
			stockTransferDetailList.add(stockTransferDetail);
			
		}
		return stockTransferDetailList;
	};
	
	/**
	 * 
	 * @param stockType
	 * @param request
	 * @return
	 */
	private List<Map<String, Object>> buildStockIODetailParams(String stockType, HttpServletRequest request)  throws Exception{
		Integer exps = ConvertTool.toInt(ResourceUtil.getParameter("Len"));
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < exps; i++) {
			Map<String, Object> params = new HashMap<String,Object>();
			String productid = request.getParameter("id-" + i);
			if(!StringUtil.isNotEmpty(productid)){
				return list;
			}
			//商品编号
			params.put("id", productid);
			//商品名称
			String productName = request.getParameter("name-" + i);
			if(StringUtil.isNotEmpty(productName)){
				params.put("name", productName);
			}
			//备注
			String remark = request.getParameter("remark-" + i);
			if(StringUtil.isNotEmpty(remark)){
				params.put("detailremark", remark);
			}
			//单位
			String unitid = request.getParameter("unitid-" + i);
			if(StringUtil.isNotEmpty(unitid)){
				params.put("unitid", unitid);
			}
			//调拨仓储区域
			String segmentid = request.getParameter("fromSegmentid-" + i);
			if(StockIOEntity.STOCK_IN.equals(stockType)){
				//目的仓储区域
				segmentid = request.getParameter("toSegmentid-" + i);
				
			}
			if(StringUtil.isNotEmpty(segmentid)){
				params.put("segmentid", segmentid);
			}
			//供应商
			String supplierid = request.getParameter("supplierid");
			if(StringUtil.isNotEmpty(supplierid)){
				params.put("supplierid", supplierid);
			}
			//运费
			BigDecimal freightprice = ConvertTool.toBigDecimal(request.getParameter("freightprice-" + i));
			if(freightprice.compareTo(new BigDecimal(0.00)) > 0){
				params.put("freightprice", freightprice);
			}
			//出入库单价
			BigDecimal unitprice = ConvertTool.toBigDecimal(request.getParameter("unitprice-" + i));
			if(unitprice.compareTo(new BigDecimal(0.00)) > 0){
				params.put("unitprice", unitprice);
			}
			//出入库总价
			BigDecimal totalprice  = ConvertTool.toBigDecimal(request.getParameter("totalprice-" + i));
			if(totalprice.compareTo(new BigDecimal(0.00)) > 0){
				params.put("totalprice", totalprice);
			}
			//出入库数量
			BigDecimal transferNum = ConvertTool.toBigDecimal(request.getParameter("transferNum-" + i));
			if(transferNum.compareTo(new BigDecimal(0.00)) > 0){
				params.put("iostocknum", transferNum);
			}
			list.add(params);
		}
		return list;
	}
}