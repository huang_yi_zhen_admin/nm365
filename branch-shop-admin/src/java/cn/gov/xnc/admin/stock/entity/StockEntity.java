package cn.gov.xnc.admin.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 进销存仓库管理，记录公司的仓库名称和地址等
 * @author zero
 * @date 2017-01-23 15:00:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**公司编码*/
	private TSCompany companyid;
	/**仓库名称*/
	private java.lang.String stockname;
	/**仓库地址*/
	private java.lang.String stockaddress;
	/**所属地区*/
	private TSTerritory territoryid;
	/**仓库负责人*/
	private TSUser dutyperson;
	/**仓库负责人*/
	private java.lang.String contactphone;
	/**创建时间*/
	private java.util.Date createtime;
	/**仓库类型*/
	private java.lang.String stocktype;
	
	/**状态，0正常，1已删除*/
	private int status = 0;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司编码
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓库名称
	 */
	@Column(name ="STOCKNAME",nullable=false,length=200)
	public java.lang.String getStockname(){
		return this.stockname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  仓库名称
	 */
	public void setStockname(java.lang.String stockname){
		this.stockname = stockname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓库地址
	 */
	@Column(name ="STOCKADDRESS",nullable=false,length=1000)
	public java.lang.String getStockaddress(){
		return this.stockaddress;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  仓库地址
	 */
	public void setStockaddress(java.lang.String stockaddress){
		this.stockaddress = stockaddress;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属地区
	 */
	@JsonIgnore    //getList查询转换为列表时处理json转换异常
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TERRITORYID")
	public TSTerritory getTerritoryid(){
		return this.territoryid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属地区
	 */
	public void setTerritoryid(TSTerritory territoryid){
		this.territoryid = territoryid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: TSUser  仓库负责人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DUTYPERSON")
	public TSUser getDutyperson() {
		return dutyperson;
	}

	public void setDutyperson(TSUser dutyperson) {
		this.dutyperson = dutyperson;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.lang.String  仓库负责人联系电话
	 */
	public java.lang.String getContactphone() {
		return contactphone;
	}

	public void setContactphone(java.lang.String contactphone) {
		this.contactphone = contactphone;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓库类型
	 */
	@Column(name ="STOCKTYPE",nullable=true,length=4)
	public java.lang.String getStocktype() {
		return stocktype;
	}

	public void setStocktype(java.lang.String stocktype) {
		this.stocktype = stocktype;
	}
	
	/**
	 * @return the status
	 */
	@Column(name ="STATUS",nullable=true,length=4)
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
}
