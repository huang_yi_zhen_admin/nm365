package cn.gov.xnc.admin.mobile.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.admin.copartner.entity.CopartnerWithdrawEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerAccountEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerWithdrawServiceI;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsFlowEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.service.ProductServiceI;
import cn.gov.xnc.admin.salesman.entity.SalesmanAuditEntity;
import cn.gov.xnc.admin.salesman.entity.UserDrawmoneyEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanAuditServiceI;
import cn.gov.xnc.admin.salesman.service.UserDrawmoneyServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserClientsEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserStaffEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserTypeEntity;
import cn.gov.xnc.system.web.system.service.SystemService;


@Controller
@RequestMapping("/mobileController")
public class MobileController {
	@Autowired
	private PayBillsServiceI payBillsService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private ProductServiceI productService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private SalesmanAuditServiceI salesmanAuditService;
	@Autowired
	private UserDrawmoneyServiceI userDrawmoneyService;
	@Autowired
	private CopartnerWithdrawServiceI copartnerWithdrawOrderService;
	@Autowired
	private OrderServiceI orderService;
	private static final Logger logger = Logger.getLogger(MobileController.class);
	
	/**
	 * 移动端首页
	 * @return
	 */
	@RequestMapping(value = "/index" )
	public ModelAndView index(HttpServletRequest request){
		return new ModelAndView("admin/mobile/index"); 
	}
	
	
	/**
	 * 收支明细统计数据
	 * 
	 * */
	@RequestMapping(value = "getMoneyStatis")
	@ResponseBody
	public AjaxJson getMoneyStatis(String createdate1, String createdate2, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		
		StringBuffer timesql = new StringBuffer();
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			timesql.append(" AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "' " );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			timesql.append(" AND a.createDate >= '" + createdate1 + "' " );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			timesql.append(" AND a.createDate <= '" + createdate2 + "' " );
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT '预存款' Name1,SUM(b.useMoney) value1,count(1) value2 FROM t_s_user a,xnc_user_salesman b ");
		sql.append(" WHERE a.company = '").append(user.getCompany().getId()).append("' AND a.tsuserSalesman = b.id ");
		sql.append(" UNION ALL ");
		sql.append(" SELECT '已付款' Name1,IFNULL(SUM(a.alreadyPaidMoney), 0) value1, count(1) value2 FROM xnc_pay_bills_order a ");
		sql.append(" WHERE a.company = '").append(user.getCompany().getId()).append("' AND a.state ='2' ").append(timesql);
		sql.append(" UNION ALL ");
		sql.append(" SELECT '应付款' Name1,IFNULL(SUM(a.payableMoney), 0) value1, count(1) value2 FROM xnc_pay_bills_order a ");
		sql.append(" WHERE a.company = '").append(user.getCompany().getId()).append("' AND a.state in ('1','2','3') ").append(timesql);
		sql.append(" UNION ALL ");
		sql.append(" SELECT '待付款' Name1,IFNULL(SUM(a.obligationsMoney), 0) value1, count(1) value2 FROM xnc_pay_bills_order a ");
		sql.append(" WHERE a.company = '").append(user.getCompany().getId()).append("' AND a.state in ('1','3') ").append(timesql);
		
		List<StatisPageVO>  list = systemService.queryListByJdbc(sql.toString(), StatisPageVO.class);	
		
		JSONArray array = new JSONArray();
		for(StatisPageVO vo : list ){
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("name", vo.getName1());
			map.put("money", vo.getValue1());
			map.put("count", vo.getValue2());
			
			array.add(map);
			
		}
		
		j.setObj(array);
		j.setSuccess(true);
		j.setMsg("查询成功");
		
		return j;
	}
	
	/**
	 * 支付审核根据id获取详情
	 * */
	@RequestMapping(value = "getPayBillsById")
	@ResponseBody
	public AjaxJson getPayBillsById(PayBillsEntity payBills, HttpServletRequest request, HttpServletResponse response) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		
		payBills = payBillsService.findUniqueByProperty(PayBillsEntity.class, "id", payBills.getId());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", payBills.getId());
		map.put("identifierOr", payBills.getIdentifier());
		if( null != payBills.getClientid() ){
			map.put("clientId", payBills.getClientid().getId());
			map.put("clientName", payBills.getClientid().getRealname());
		}
		if( "1".equals(payBills.getType()) ){
			map.put("type", "支付");
		}else if( "2".equals(payBills.getType()) ){
			map.put("type", "充值");
		}
		if( "1".equals(payBills.getState()) ){
			map.put("state", "状态 确认");
		}else if( "2".equals(payBills.getState()) ){
			map.put("state", "待审核");
		}else if( "3".equals(payBills.getState()) ){
			map.put("state", "未通过");
		}else if( "4".equals(payBills.getState()) ){
			map.put("state", "系统确认");
		}else if( "5".equals(payBills.getState()) ){
			map.put("state", "支付失败");
		}
		/**支付宝_1,微信支付_2,线下转账_3,预存款支付_4,银联支付_5,先货后款_6,*/
		if( "1".equals(payBills.getPaymentmethod()) ){
			map.put("paymentmethod", "支付宝");
		}else if( "2".equals(payBills.getPaymentmethod()) ){
			map.put("paymentmethod", "微信支付");
		}else if( "3".equals(payBills.getPaymentmethod()) ){
			map.put("paymentmethod", "线下转账");
		}else if( "4".equals(payBills.getPaymentmethod()) ){
			map.put("paymentmethod", "预存款支付");
		}else if( "5".equals(payBills.getPaymentmethod()) ){
			map.put("paymentmethod", "银联支付");
		}else if( "6".equals(payBills.getPaymentmethod()) ){
			map.put("paymentmethod", "先货后款");
		}
		
		map.put("payableMoney", payBills.getPayablemoney());
		map.put("alreadyPaidMoney", payBills.getAlreadypaidmoney());
		map.put("balanceMoney", payBills.getBalancemoney());
		map.put("voucher", payBills.getVoucher());
		map.put("bank", payBills.getBank());
		map.put("bankAccount", payBills.getBankAccount());
		map.put("bankDate", payBills.getBankDate());
		map.put("bankUrl", payBills.getBankUrl());
		map.put("remark", payBills.getRemark());
		if( null != payBills.getUpdatedate() ){
			map.put("updatedate", DateUtils.date3Str(payBills.getUpdatedate(), "yyyy-MM-dd HH:mm:ss"));
		}
		if( null != payBills.getCreatedate()){
			map.put("createDate", DateUtils.date3Str(payBills.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
		}
		if( null != payBills.getUpdateuser() ){
			map.put("updateUser", payBills.getUpdateuser());
		}
		if( null != payBills.getCreateuser() ){
			map.put("createUser", payBills.getCreateuser());
		}
		if( null != payBills.getAudituser() ){
			map.put("audituser", payBills.getAudituser().getId());
			map.put("audituserName", payBills.getAudituser().getRealname());
			if( null != payBills.getAuditdate() ){
				map.put("auditDate", DateUtils.date3Str(payBills.getAuditdate(), "yyyy-MM-dd HH:mm:ss"));
			}
		}
		
		j.setObj(map);
		
		j.setSuccess(true);
		j.setMsg("查询成功");
		
		return j;
	}
	
	/**
	 * 提现审核根据id获取详情
	 * */
	@RequestMapping(value = "getUserDrawmoneyById")
	@ResponseBody
	public AjaxJson getUserDrawmoneyById(UserDrawmoneyEntity userDrawmoney, HttpServletRequest request, HttpServletResponse response) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		userDrawmoney = userDrawmoneyService.findUniqueByProperty(UserDrawmoneyEntity.class, "id", userDrawmoney.getId());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", userDrawmoney.getId());
		if( null != userDrawmoney.getUser() ){
			map.put("user", userDrawmoney.getUser().getRealname());
		}
		map.put("createDate", DateUtils.date3Str(userDrawmoney.getCreatedate(),"yyyy-MM-dd HH:mm:ss"));
		if( "1".equals(userDrawmoney.getState()) ){
			map.put("status", "申请中");
		}else if( "2".equals(userDrawmoney.getState())){
			map.put("status", "已通过");
		}
		map.put("applicationmoney", userDrawmoney.getApplicationmoney());
		map.put("withdrawalsMoney", userDrawmoney.getWithdrawalsmoney());
		if( "1".equals(userDrawmoney.getMode()) ){
			map.put("mode", "支付宝");
		}else if( "2".equals(userDrawmoney.getMode())){
			map.put("mode", "微信");
		}else if( "3".equals(userDrawmoney.getMode())){
			map.put("mode", "银行");
		}
		map.put("bankaccount", userDrawmoney.getBankaccount());
		map.put("bankname", userDrawmoney.getBankname());
		map.put("username", userDrawmoney.getUsername());
		if( null!=userDrawmoney.getCheckuser() ){
			map.put("checkUser", userDrawmoney.getCheckuser().getRealname());
			map.put("checkUserId", userDrawmoney.getCheckuser().getId());
		}
		if( null != userDrawmoney.getCheckdate() ){
			map.put("checkDate", DateUtils.date3Str(userDrawmoney.getCheckdate(),"yyyy-MM-dd HH:mm:ss"));
		}
		map.put("Remarks", userDrawmoney.getRemarks());
		
		j.setObj(map);
		j.setSuccess(true);
		j.setMsg("查询成功");
		
		return j;
	}
	
	/**
	 * 根据大单id获取小单列表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getOrderListByPayBillsId")
	@ResponseBody
	public AjaxJson getOrderListByPayBillsId(PayBillsOrderEntity paybillsorder, int pageNum, int showNum, HttpServletRequest request, HttpServletResponse response) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
		
		cq.eq("billsorderid", paybillsorder);
		cq.eq("company", user.getCompany());
		
		cq.setPageSize(showNum);
		cq.setCurPage(pageNum-1);

		cq.addOrder("identifieror", SortDirection.asc);
		
		List<OrderEntity> list = systemService.getListByCriteriaQuery(cq, true);
		
		JSONArray array = new JSONArray();
		for( OrderEntity o : list ){
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", o.getId());
			map.put("identifierOr", o.getIdentifieror());
			if( null != o.getClientid() ){
				map.put("clientId", o.getClientid().getId());
				map.put("clientName", o.getClientid().getRealname());
			}
			if( null != o.getYewu() ){
				map.put("yewu", o.getYewu().getId());
				map.put("yewuName", o.getYewu().getRealname());
			}
			map.put("productName", o.getProductname());
			map.put("Number", o.getNumber().toString());
			map.put("warehouse", o.getWarehouse());
			map.put("CustomerName", o.getCustomername());
			map.put("Telephone", o.getTelephone());
			map.put("address", o.getAddress());
			map.put("province", o.getProvince());
			map.put("city", o.getCity());
			map.put("area", o.getArea());
			map.put("platform", o.getPlatform());
			map.put("DepartureDate", DateUtils.date3Str(o.getDeparturedate(),"yyyy-MM-dd HH:mm:ss"));
			map.put("cancelDate", DateUtils.date3Str(o.getCanceldate(),"yyyy-MM-dd HH:mm:ss"));
			map.put("payDate",  DateUtils.date3Str(o.getPaydate(),"yyyy-MM-dd HH:mm:ss"));
			map.put("createDate", DateUtils.date3Str(o.getCreatedate(),"yyyy-MM-dd HH:mm:ss"));
			map.put("logisticsCompany", o.getLogisticscompany());
			map.put("logisticsNumber", o.getLogisticsnumber());
			map.put("Remarks", o.getRemarks());
			if( null != o.getFreightpic()){
				map.put("freightPic", o.getFreightpic().toPlainString());
			}
			map.put("Price", o.getPrice().toPlainString());
			map.put("priceC", o.getPricec().toPlainString());
			map.put("priceS", o.getPrices().toPlainString());
			map.put("priceYW", o.getPriceyw().toPlainString());
			/**发货状态  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7 */
			if( "1".equals(o.getState()) ){
				map.put("state", "待付款");
			}else if( "2".equals( o.getState() )){
				map.put("state", "待发货");
			}else if( "3".equals( o.getState() )){
				map.put("state", "已发货");
			}else if( "4".equals( o.getState() )){
				map.put("state", "已签收");
			}else if( "5".equals( o.getState() )){
				map.put("state", "已取消");
			}else if( "6".equals( o.getState() )){
				map.put("state", "待审核");
			}else if( "7".equals( o.getState() )){
				map.put("state", "未通过");
			}
			map.put("productId", o.getProductid().getId());
			map.put("billsOrderID", o.getBillsorderid().getId());
			map.put("company", o.getCompany().getId());
			
			if( null!=o.getTSTerritory() ){
				map.put("territoryid", o.getTSTerritory().getId());
			}
			if( "Y".equals(o.getState()) ){
				map.put("ordersend", "是");
			}else if( "N".equals(o.getState() )){
				map.put("ordersend", "否");
			}
			if( "1".equals(o.getSendtype())){
				map.put("sendtype", "自提");
			}else if( "2".equals(o.getSendtype())){
				map.put("sendtype", "走物流");
			}

			array.add(map);
		}

		JSONObject obj = new JSONObject();
		obj.put("data", array);
		obj.put("pageNum", pageNum);
		obj.put("showNum", showNum);
		
		j.setObj(obj);
		j.setSuccess(true);
		j.setMsg("查询成功");
		
		return j;

	}
	
	
	/**
	 * 业务审核根据id获取详情接口
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getSalesmanAuditById")
	@ResponseBody
	public AjaxJson getSalesmanAuditById(SalesmanAuditEntity salesmanAudit, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		salesmanAudit = salesmanAuditService.findUniqueByProperty(SalesmanAuditEntity.class, "id", salesmanAudit.getId());
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", salesmanAudit.getId());
		if( null != salesmanAudit.getYwuser() ){
			map.put("ywuser", salesmanAudit.getYwuser().getRealname());
		}
		map.put("createDate", DateUtils.date3Str(salesmanAudit.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
		if( "1".equals(salesmanAudit.getState()) ){
			map.put("state", "申请中");
		}else if( "2".equals(salesmanAudit.getState()) ){
			map.put("state", "已通过");
		}else if( "3".equals(salesmanAudit.getState()) ){
			map.put("state", "未通过");
		}
		map.put("applicationmoney", salesmanAudit.getApplicationmoney());
		map.put("ordermoney", salesmanAudit.getOrdermoney());
		map.put("checkmoney", salesmanAudit.getCheckmoney());
		map.put("frozenMoney", salesmanAudit.getFrozenmoney());
		if( null!= salesmanAudit.getCheckuser() ){
			map.put("checkUserId", salesmanAudit.getCheckuser().getId());
			map.put("checkUser", salesmanAudit.getCheckuser().getRealname());
		}
		if( null != salesmanAudit.getCheckdate()){
			map.put("checkDate", DateUtils.date3Str(salesmanAudit.getCheckdate(),"yyyy-MM-dd HH:mm:ss"));
		}
		map.put("Remarks", salesmanAudit.getRemarks());
		
		map.put("success", true);
		map.put("Msg", "查询成功");
		
		j.setObj(map);
		j.setSuccess(true);
		j.setMsg("查询成功");
		
		
		return j;
	}
	
	/**
	 * 资金审核统计接口
	 * 
	 * @param 
	 * @return
	 */
	@RequestMapping(value = "getAuditFundStatistics")
	@ResponseBody
	public AjaxJson getAuditFundStatistics(HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		//TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT '业务' as Name1, IFNULL(SUM(1),0) as value1, IFNULL(SUM(a.applicationmoney),0) as value2 FROM xnc_salesman_audit as a ");
		sql.append("WHERE a.company = '" + user.getCompany().getId() + "'");
		//sql.append(" AND a.state in ('2','3') ");
		sql.append(" UNION ALL ");
		sql.append("SELECT '提现' as Name1, IFNULL(SUM(1),0) as value1, IFNULL(SUM(a.applicationmoney),0) as value2 FROM xnc_user_drawmoney as a ");
		sql.append("WHERE a.company = '" + user.getCompany().getId() + "'");
		sql.append(" UNION ALL ");
		sql.append("SELECT '支付' as Name1, IFNULL(SUM(1),0) as value1, IFNULL(SUM(a.alreadyPaidMoney),0) as value2 FROM xnc_pay_bills as a ");
		sql.append("WHERE a.company = '" + user.getCompany().getId() + "'");
		sql.append(" AND a.state in ( '1','2','3') ");
		sql.append(" UNION ALL ");
		sql.append("SELECT '发货结算' as Name1, IFNULL(SUM(1),0) as value1, IFNULL(SUM(a.waitToDrawMoney),0) as value2 FROM xnc_copartner_withdraw_order as a ");
		sql.append("WHERE a.companyid = '" + user.getCompany().getId() + "'");
		
		List<StatisPageVO> list = systemService.queryListByJdbc(sql.toString(), StatisPageVO.class);
		JSONArray array = new JSONArray();
		for(StatisPageVO vo : list ){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name", vo.getName1());
			map.put("count", vo.getValue1());
			map.put("money", vo.getValue2());
			array.add(map);
		}
		j.setObj(array);
		j.setSuccess(true);
		j.setMsg("查询成功");
		return j;
	}
	
	/**
	 * 大订单详情页
	 * @return
	 */
	@RequestMapping(value = "/payBillsOrderDetail" )
	@ResponseBody
	public AjaxJson  payBillsOrderDetail(PayBillsOrderEntity payBillsOrder, HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		AjaxJson j = new AjaxJson();
		String paymentmethod = "";
		String state = "";
		String freightstatus = "";
		
		if (StringUtil.isNotEmpty(payBillsOrder.getId())) {
			payBillsOrder = payBillsOrderService.getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
		}else{
			j.setMsg("参数有误");
			j.setSuccess(false);
			return j;
		}
		//定义客户
		try {
			if(StringUtil.isEmpty(payBillsOrder.getClientid().getId())){
				TSUser clientid = new TSUser();
				payBillsOrder.setClientid(clientid);
			}
		} catch (Exception e) {
			logger.error("定义客户异常：" + e);
			TSUser clientid = new TSUser();
			payBillsOrder.setClientid(clientid);
			j.setMsg("定义客户异常");
			j.setSuccess(false);
			return j;
		}
		
		//定义业务员空
		try {
			if(StringUtil.isEmpty(payBillsOrder.getYewu().getId())){
				TSUser yewu = new TSUser();
				payBillsOrder.setYewu(yewu);
			}
		} catch (Exception e) {
			logger.error("定义业务员异常：" + e);
			TSUser yewu = new TSUser();
			payBillsOrder.setYewu(yewu);
			j.setMsg("定义业务员异常");
			j.setSuccess(false);
			return j;
		}
		switch(Integer.parseInt(payBillsOrder.getPaymentmethod() == null ? "7" : payBillsOrder.getPaymentmethod())){
			case 1 : paymentmethod = "支付宝";
			 break;
			case 2 : paymentmethod = "微信";
			 break;
			case 3 : paymentmethod = "线下转账";
			 break;
			case 4 : paymentmethod = "预存款支付";
			 break;
			case 5 : paymentmethod = "银联支付";
			 break;
			case 6 : paymentmethod = "先货后款";
			 break; 
		}
		switch(Integer.parseInt(payBillsOrder.getState() == null ? "0" : payBillsOrder.getState())){
			case 1 : state = "待付款";
			 break;
			case 2 : state = "已付款";
			 break;
			case 3 : state = "未付清";
			 break;
			case 4 : state = "已取消";
			 break;
		}
		
		if(OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(payBillsOrder.getFreightState().getDictionaryValue())){
			freightstatus = "待发货";
		}else if(OrderServiceI.FreightState.ALREADY_DELIVER.getValue().equals(payBillsOrder.getFreightState().getDictionaryValue())){
			freightstatus = "已发货";
		}else if(OrderServiceI.FreightState.PART_DELIVER.getValue().equals(payBillsOrder.getFreightState().getDictionaryValue())){
			freightstatus = "部分发货";
		}
		
		map.put("id", payBillsOrder.getId());
		map.put("identifier", payBillsOrder.getIdentifier());
		map.put("createdate", payBillsOrder.getCreatedate());
		map.put("updatedate", payBillsOrder.getUpdatedate());
		map.put("clientRealname", payBillsOrder.getClientid().getRealname());
		map.put("yewuRealname", payBillsOrder.getYewu().getRealname());
		map.put("isSaleManOrder", payBillsOrder.getIsSaleManOrder());
		map.put("paymentmethod", paymentmethod);
		map.put("payablemoney", payBillsOrder.getPayablemoney());
		map.put("alreadypaidmoney", payBillsOrder.getAlreadypaidmoney());
		map.put("obligationsmoney", payBillsOrder.getObligationsmoney());
		map.put("state", state);
		map.put("type", payBillsOrder.getType().equals("1") ? "销售" : "充值");
		map.put("freightstatus", freightstatus);
		//map.put("orderS", payBillsOrder.getOrderS());
		j.setMsg("查询成功！");
		j.setObj(map);
		return j;
	}
	/**
	 * 小订单详情
	 * @return
	 */
	@RequestMapping(value = "/orderDetail" )
	@ResponseBody
	public AjaxJson orderDetail(OrderEntity order, HttpServletRequest request, HttpServletResponse response){
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		Map<String, Object> params = new HashMap<String, Object>();
		String state = "";
		String paymentmethod  = "";
		if (StringUtil.isNotEmpty(order.getId())) {
			order = systemService.getEntity(OrderEntity.class, order.getId());
			//存在地址信息的，状态为1的情况  对地址进行拆分
			String useraddress ="";
			if( "1".equals(order.getState()) ){//未付款订单允许修改地址
				String address =  order.getAddress();
				
				if(  StringUtil.isNotEmpty(address)){
					if( address.indexOf("_") > 0 ){
						useraddress = address.substring(0,address.lastIndexOf("_"));
					}
					order.setAddress( order.getAddress().replaceAll(useraddress, "").replace("_", "") );
				}
			}
			 TSTerritory territory = order.getTSTerritory();
			 if(null == territory){
				 territory = new TSTerritory();
			 }
			//定义客户
			try {
				if(StringUtil.isEmpty(order.getClientid().getId())){
					TSUser clientid = new TSUser();
					order.setClientid(clientid);
				}
			} catch (Exception e) {
				TSUser clientid = new TSUser();
				order.setClientid(clientid);
			}
			//定义业务员空
			try {
				if(StringUtil.isEmpty(order.getYewu().getId())){
					TSUser yewu = new TSUser();
					order.setYewu(yewu);
				}
			} catch (Exception e) {
				TSUser yewu = new TSUser();
				order.setYewu(yewu);
			}
			 switch(Integer.parseInt(order.getState() == null ? "0" : order.getState())){
			 	case 1 : state = "待付款";
			 		break;
			 	case 2 : state = "待发货";
		 			break;
			 	case 3 : state = "已发货";
		 			break;
			 	case 4 : state = "已签收";
		 			break;
			 	case 5 : state = "已取消";
		 			break;
			 	case 6 : state = "待审核";
		 			break;
			 	case 7 : state = "未通过";
		 			break;
			 }
			 switch(Integer.parseInt(order.getBillsorderid().getPaymentmethod() == null ? "7" : order.getBillsorderid().getPaymentmethod())){
				case 1 : paymentmethod = "支付宝";
				 break;
				case 2 : paymentmethod = "微信";
				 break;
				case 3 : paymentmethod = "线下转账";
				 break;
				case 4 : paymentmethod = "预存款支付";
				 break;
				case 5 : paymentmethod = "银联支付";
				 break;
				case 6 : paymentmethod = "先货后款";
				 break; 
			}
			 
			 params.put("useraddress", useraddress);
			 params.put("yewuRealname", order.getYewu().getRealname());
			 params.put("yewuId", order.getYewu().getId());
			 params.put("id", order.getId());
			 params.put("identifieror", order.getIdentifieror());
			 params.put("ordersend", order.getOrdersend());
			 params.put("productid", order.getProductid().getId());
			 params.put("productSpecifications", order.getProductid().getSpecifications());
			 params.put("productPrice", order.getProductid().getPrice());
			 params.put("productGuidepic", order.getProductid().getGuidepic());
			 params.put("productname", order.getProductname());
			 params.put("billsorderType", order.getBillsorderid().getType().equals("1") ? "销售" : "充值");
			 params.put("billsorderPaymentmethod", paymentmethod);
			 params.put("clientRealname", order.getClientid().getRealname());
			 
			 params.put("customername", order.getCustomername());
			 params.put("telephone", order.getTelephone());
			 params.put("number", order.getNumber());
			 params.put("price", order.getPrice());
			 params.put("freightpic", order.getFreightpic());
			 params.put("address", order.getAddress());
			 params.put("createdate", order.getCreatedate());
			 params.put("departuredate", order.getDeparturedate());
			 params.put("state", state);
			 params.put("logisticsnumber", order.getLogisticsnumber());
			 params.put("logisticscompany", order.getLogisticscompany());
			 params.put("remarks", order.getRemarks());
			 j.setMsg("查询成功");
			 j.setObj(params);
		}else{
			j.setMsg("参数有误");
			j.setSuccess(false);
		}
		//params.put("user", user);
		
		return j;
	}
	
	/**
	 * 小订单价格、运费、数量修改
	 * @return
	 */
	@RequestMapping(value = "/orderUpdate" )
	@ResponseBody
	public AjaxJson orderUpdate(OrderEntity order, HttpServletResponse response, HttpServletRequest request){
		//Map<String, Object> params = new HashMap<String, Object>();
		request.setAttribute("param", "m");
		AjaxJson j = orderService.orderSave(order, request);
		return j;
		/*String message = "";
		byte success = 1;
		Map<String, Object> params = new HashMap<String, Object>();
		BigDecimal totalPrice = new BigDecimal("0.00");
		BigDecimal priceMultiply = new BigDecimal("0.00");
		BigDecimal priceAdd = new BigDecimal("0.00");
		String sql = "";
		if (StringUtil.isNotEmpty(order.getId())) {
			OrderEntity orderEntity = systemService.getEntity(OrderEntity.class, order.getId());
			String payBillsOrderId  = orderEntity.getBillsorderid().getId();
			if( ("1".equals(orderEntity.getState()) || "2".equals(orderEntity.getState())) && "1".equals(orderEntity.getBillsorderid().getState()) ){

				if (StringUtil.isNotEmpty(order.getPrice())) {
					try{
						sql = "update xnc_order set Price=" + order.getPrice() + " where id='" + order.getId() + "'";
						if(systemService.updateBySqlString(sql) > 0){
							message = "更新价格成功";
						}else{
							message = "更新价格失败";
							success = 0;
						}
						
					}catch(RuntimeException e){
						message = "更新价格异常";
						success = 0;
					}
					
				}
				if (StringUtil.isNotEmpty(order.getFreightpic())) {
					try{
						sql = "update xnc_order set freightPic=" + order.getFreightpic() + " where id='" + order.getId() + "'";
						if(systemService.updateBySqlString(sql) > 0){
							message = "更新运费成功";
						}else{
							message = "更新运费失败";
							success = 0;
						}
						
					}catch(RuntimeException e){
						message = "更新运费异常";
						success = 0;
					}
				}
				if (StringUtil.isNotEmpty(order.getNumber())) {
					try{
						sql = "update xnc_order set Number=" + order.getNumber() + " where id='" + order.getId() + "'";
						if(systemService.updateBySqlString(sql) > 0){
							message = "更新数量成功";
						}else{
							message = "更新数量失败";
							success = 0;
						}
						
					}catch(RuntimeException e){
						message = "更新数量异常";
						success = 0;
					}
				}
			}
			sql = "SELECT Number, Price, freightPic from xnc_order where billsOrderID='" + payBillsOrderId + "'";
			List<OrderEntity> orderS = systemService.queryListByJdbc(sql, OrderEntity.class);
			for(OrderEntity o : orderS){
				priceMultiply = o.getPrice().multiply(new BigDecimal(o.getNumber()));
				priceAdd =priceMultiply.add(o.getFreightpic());
				totalPrice = totalPrice.add(priceAdd);
			}
			if(orderS.size()>0){
				PayBillsOrderEntity payBillsOrderEntity = payBillsOrderService.get(PayBillsOrderEntity.class, payBillsOrderId);
				System.out.println("totalPrice:"+totalPrice);
				payBillsOrderEntity.setPayablemoney(totalPrice);
				payBillsOrderEntity.setObligationsmoney(totalPrice.subtract(payBillsOrderEntity.getAlreadypaidmoney()));
				try{
					payBillsOrderService.saveOrUpdate(payBillsOrderEntity);
					message = "更新数据成功!";
				}catch(RuntimeException r){
					logger.error("应付金额:"+totalPrice);
					message = "更新应付金额异常：" + r;
					success = 0;
				}
				
			}
		}
		
		params.put("message", message);
		params.put("success", success);
		DisplayMsgUtil.printMsg(response, JsonUtil.toJson(params));*/
	}
	
	/**
	 * 商品详情
	 * @return
	 */
	@RequestMapping(value = "/productDetail" )
	@ResponseBody
	public AjaxJson productDetail(ProductEntity product, HttpServletRequest request, HttpServletResponse response){
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(product.getId())) {
			product = productService.getEntity(ProductEntity.class, product.getId());
			Map<String, Object> map = new HashMap<String, Object>();			
			/*ProductBrandEntity productBrand = null;
			if(null != product.getBrandid() && StringUtil.isNotEmpty (product.getBrandid().getId().trim())){
				
				productBrand = systemService.findUniqueByProperty(ProductBrandEntity.class, "id", product.getBrandid().getId());
			}
			if(null == productBrand){
				product.setBrandid(new ProductBrandEntity());
				
			}else if(null != productBrand 
					&& StringUtil.isEmpty (product.getBrandid().getBrandname().trim())){
				
				product.setBrandid(new ProductBrandEntity());
			}
			
			FreightEntity freight = null;
			if(null != product.getFreightid() && StringUtil.isNotEmpty (product.getFreightid().getId().trim())){
				
				freight = systemService.findUniqueByProperty(FreightEntity.class, "id", product.getFreightid().getId());
				
			}
			if(null == freight){
				product.setFreightid(new FreightEntity());
				
			}else if(null != freight 
					&& StringUtil.isEmpty (product.getFreightid().getName().trim())){
				
				product.setFreightid(new FreightEntity());
			}
			
			ProductClassifyEntity classify = null;
			if(null != product.getClassifyid() && StringUtil.isNotEmpty (product.getClassifyid().getId().trim())){
				classify = systemService.findUniqueByProperty(ProductClassifyEntity.class, "id", product.getClassifyid().getId());
			}
			if(null == classify){
				product.setClassifyid(new ProductClassifyEntity());
				
			}else if(null != classify 
					&& StringUtil.isEmpty (product.getClassifyid().getClassifyname().trim())){
				
				product.setClassifyid(new ProductClassifyEntity());
				
			}*/
			map.put("id", product.getId());
			map.put("subtitle", product.getSubtitle());
			map.put("name", product.getName());
			map.put("code", product.getCode());
			map.put("specifications", product.getSpecifications());
			map.put("state", product.getState());
			map.put("guidepic", product.getGuidepic());
			map.put("keyword", product.getKeyword());
			map.put("unit", product.getUnitBase());
			map.put("minorde", product.getMinorde());
			map.put("price", product.getPrice());
			map.put("pricec", product.getPricec());
			map.put("prices", product.getPrices());
			map.put("priceyw", product.getPriceyw());
			map.put("sort", product.getSort());
			map.put("stock", product.getStock());
			map.put("content", product.getContent());
			map.put("summary", product.getSummary());
			map.put("createdate", product.getCreatedate());
			map.put("notsale", product.getNotsale());
			j.setObj(map);;
			j.setMsg("查询成功");
		}else{
			j.setMsg("参数有误");
			j.setSuccess(false);
		}
		return j;
	}
	/**
	 * 用户信息详情
	 * @return
	 */
	@RequestMapping(value = "/sUserDetail" )
	@ResponseBody
	public AjaxJson sUserDetail(TSUser user, HttpServletRequest request, HttpServletResponse response){
		//Map<String, Object> map = new HashMap<String, Object>();
		AjaxJson j = new AjaxJson();
		TSUser loginUser = ResourceUtil.getSessionUserName();
		if(loginUser != null){
			String userType = "";
			if(!loginUser.getType().equals("1")){
				user.setId(loginUser.getId());
			}
			if (StringUtil.isNotEmpty(user.getId())) {
				user = systemService.getEntity(TSUser.class, user.getId());
				userType = user.getType();//根据所传ID判断用户类型
			}
			switch(Integer.parseInt(userType)){
				case 3 : j = this.userClients(user, response);
				return j;
				case 6 : j = this.userCopartner(user, response);
				return j;
				default : j = this.userStaff(user, response);
				return j;
			}
		}else{
			j.setMsg("您未登录");
			j.setSuccess(false);
			return j;
		}
		
	}
	/**
	 * 资金收支明细详情页
	 * */
	@RequestMapping(value = "/cashBillsDetail" )
	@ResponseBody
	public AjaxJson cashBillsDetail(PayBillsOrderEntity payBillsOrder, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> billsflowMap = null;
		AjaxJson j = new AjaxJson();
		List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
		if (StringUtil.isNotEmpty(payBillsOrder.getId())) {
			payBillsOrder = payBillsOrderService.getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
		}else{
			payBillsOrder = new PayBillsOrderEntity();
		}
		//定义客户
		try {
			if(payBillsOrder != null ){
				if(StringUtil.isEmpty(payBillsOrder.getClientid().getId())){
					TSUser clientid = new TSUser();
					payBillsOrder.setClientid(clientid);
				}
			}
		} catch (Exception e) {
			TSUser clientid = new TSUser();
			payBillsOrder.setClientid(clientid);
		}
		
		//定义业务员空
		try {
			if(payBillsOrder != null ){
				if(StringUtil.isEmpty(payBillsOrder.getYewu().getId())){
					TSUser yewu = new TSUser();
					payBillsOrder.setYewu(yewu);
				}
			}
		} catch (Exception e) {
			TSUser yewu = new TSUser();
			payBillsOrder.setYewu(yewu);
		}
		if(payBillsOrder != null ){
			//List<PayBillsFlowEntity> PayBillsFlowList =  payBillsOrder.getPayBillsFlows();
			for(PayBillsFlowEntity p : payBillsOrder.getPayBillsFlows()){
				billsflowMap = new HashMap<String, Object>();
				billsflowMap.put("identifier", p.getBillsid().getIdentifier());
				billsflowMap.put("clientidRealname", p.getBillsid().getClientid().getRealname());
				billsflowMap.put("companyCompanyName", p.getBillsid().getCompany().getCompanyName());
				billsflowMap.put("paymentmethod", p.getBillsid().getPaymentmethod());
				billsflowMap.put("paymentmethod", p.getBillsid().getPaymentmethod());
				billsflowMap.put("alreadypaidmoney", p.getBillsid().getAlreadypaidmoney());
				billsflowMap.put("state", p.getBillsid().getState());
				billsflowMap.put("createdate", p.getBillsid().getCreatedate());
				billsflowMap.put("audituserRealname", p.getBillsid().getAudituser() == null ? null : p.getBillsid().getAudituser().getRealname());
				billsflowMap.put("auditdate", p.getBillsid().getAuditdate());
				listMap.add(billsflowMap);
			}
			map.put("id", payBillsOrder.getId());
			map.put("identifier", payBillsOrder.getIdentifier());
			map.put("clientidRealname", payBillsOrder.getClientid().getRealname());
			map.put("companyCompanyName", payBillsOrder.getCompany().getCompanyName());
			map.put("type", payBillsOrder.getType());
			map.put("state", payBillsOrder.getState());
			map.put("paymentmethod", payBillsOrder.getPaymentmethod());
			map.put("payablemoney", payBillsOrder.getPayablemoney());
			map.put("alreadypaidmoney", payBillsOrder.getAlreadypaidmoney());
			map.put("obligationsmoney", payBillsOrder.getObligationsmoney());
			map.put("createdate", payBillsOrder.getCreatedate());
			map.put("yewuRealname", payBillsOrder.getYewu().getRealname());
			map.put("isSaleManOrder", payBillsOrder.getIsSaleManOrder());
			map.put("balancemoney", payBillsOrder.getBalancemoney());
			map.put("freightstatus", payBillsOrder.getFreightState());
			map.put("orderS", payBillsOrder.getOrderS());
			map.put("billsflow", listMap);
		}
		j.setMsg("查询成功");
		j.setObj(map);
		return j;
	}
	/**
	 * 支付审核详情页
	 * @return
	 */
	@RequestMapping(value = "/payBillsDetail" )
	@ResponseBody
	public AjaxJson payBillsDetail(PayBillsEntity payBills, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		AjaxJson j = new AjaxJson();
		payBills =  payBillsService.findUniqueByProperty(PayBillsEntity.class, "id", payBills.getId());//支付审核
		map.put("id", payBills.getId());
		map.put("identifier", payBills.getIdentifier());
		map.put("clientidRealname", payBills.getClientid().getRealname());
		map.put("companyCompanyName", payBills.getCompany().getCompanyName());
		map.put("type", payBills.getType());
		map.put("state", payBills.getState());
		map.put("paymentmethod", payBills.getPaymentmethod());
		map.put("alreadypaidmoney", payBills.getAlreadypaidmoney());
		map.put("payablemoney", payBills.getPayablemoney());
		map.put("balancemoney", payBills.getBalancemoney());
		map.put("bank", payBills.getBank());
		map.put("bankAccount", payBills.getBankAccount());
		map.put("bankUrl", payBills.getBankUrl());
		map.put("bankDate", payBills.getBankDate());
		map.put("createdate", payBills.getCreatedate());
		map.put("audituserRealname", payBills.getAudituser() == null ? "" : payBills.getAudituser().getRealname());
		
		map.put("auditdate", payBills.getAuditdate());
		j.setMsg("查询成功");
		j.setObj(map);
		return j;
	}
	/**
	 * 退单审核详情页
	 * @return
	 */
	@RequestMapping(value = "/copartnerWithdrawDetail" )
	@ResponseBody
	public AjaxJson copartnerWithdrawDetail(CopartnerWithdrawEntity copartnerWithdrawOrder, HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		AjaxJson j = new AjaxJson();
		String status = "";
		if(StringUtil.isNotEmpty(copartnerWithdrawOrder.getId())){
			copartnerWithdrawOrder = copartnerWithdrawOrderService.getEntity(CopartnerWithdrawEntity.class, copartnerWithdrawOrder.getId());
		}else{
			copartnerWithdrawOrder = new CopartnerWithdrawEntity();
		}
		switch(Integer.parseInt(copartnerWithdrawOrder.getStatus())){
			case 1 : status = "取消结算";
				break;
			case 2 : status = "待结算";
				break;
			case 3 : status = "通过结算";
				break;	
		}
		map.put("id", copartnerWithdrawOrder.getId());
		map.put("usercopartneridUserName", copartnerWithdrawOrder.getUsercopartnerid().getUserName());
		map.put("waittodrawmoney", copartnerWithdrawOrder.getWaittodrawmoney());
		map.put("actualdrawmoney", copartnerWithdrawOrder.getActualdrawmoney());
		map.put("status", status);
		map.put("createdate", copartnerWithdrawOrder.getCreatedate());
		map.put("updatetime", copartnerWithdrawOrder.getUpdatetime());
		map.put("paymentevidence", copartnerWithdrawOrder.getPaymentevidence());
		map.put("remarks", copartnerWithdrawOrder.getRemarks());
		map.put("checkuserRealname", copartnerWithdrawOrder.getCheckuser() == null ? "" : copartnerWithdrawOrder.getCheckuser().getRealname());
		map.put("companyidCompanyName", copartnerWithdrawOrder.getCompanyid().getCompanyName());
		j.setMsg("查询成功");
		j.setObj(map);
		return j;
	}
	/**
	 * 企业信息详情页
	 */
	@RequestMapping(value = "/companyInfoDetail" )
	@ResponseBody
	public AjaxJson companyInfoDetail(){
		AjaxJson j = new AjaxJson();
		Map<String, Object> map = new HashMap<String, Object>();
		TSCompany tScompany = new TSCompany();
		TSTerritory top1Territory = null;
		TSTerritory top2Territory = null;
		TSTerritory top3Territory = null;
		TSUser user = ResourceUtil.getSessionUserName();
		if(StringUtil.isNotEmpty(user.getCompany().getId()) ){
			tScompany = systemService.getEntity(TSCompany.class, user.getCompany().getId());
			top1Territory = tScompany.getTSTerritory();
			if(null != top1Territory && StringUtil.isNotEmpty(top1Territory.getId())){
				top2Territory = top1Territory.getTSTerritory();
				map.put("province", top1Territory.getId());
			}
			if(null != top2Territory && StringUtil.isNotEmpty(top2Territory.getId())){
				top3Territory = top2Territory.getTSTerritory();
				map.put("province", top2Territory.getId());
				map.put("city", top1Territory.getId());
			}
			if(null != top3Territory && StringUtil.isNotEmpty(top3Territory.getId())){
				map.put("province", top3Territory.getId());
				map.put("city", top2Territory.getId());
				map.put("area", top1Territory.getId());
			}
			map.put("id", tScompany.getId());
			map.put("companyName", tScompany.getCompanyName());
			map.put("mobilephone", tScompany.getMobilephone());
			map.put("contactname", tScompany.getContactname());
			map.put("servicephone", tScompany.getServicephone());
			map.put("address", tScompany.getAddress());
			map.put("createdate", tScompany.getCreatedate());
			j.setObj(map);
			j.setMsg("查询成功！");
		}else{
			j.setMsg("查询失败");
			j.setSuccess(false);
		}
		return j;
	}
	/**
	 * 企业平台详情页
	 */
	@RequestMapping(value = "/companyPlatformDetail" )
	@ResponseBody
	public AjaxJson companyPlatformDetail(){
		AjaxJson j = new AjaxJson();
		Map<String, Object> map = new HashMap<String, Object>();
		TSCompany tScompany = new TSCompany();
		CompanyPlatformEntity companyPlatform = new CompanyPlatformEntity();
		TSUser user = ResourceUtil.getSessionUserName();
		if(StringUtil.isNotEmpty(user.getCompany().getId()) ){
			tScompany = systemService.getEntity(TSCompany.class, user.getCompany().getId());
			companyPlatform = systemService.getEntity(CompanyPlatformEntity.class, tScompany.getCompanyplatform().getId());
			map.put("id", companyPlatform.getId());
			map.put("platformname", companyPlatform.getPlatformname());
			map.put("website", companyPlatform.getWebsite());
			map.put("advertisement", companyPlatform.getAdvertisement());
			map.put("keyword", companyPlatform.getKeyword());
			map.put("logourl", companyPlatform.getLogourl());
			map.put("companypicture", companyPlatform.getCompanypicture());
			map.put("shareprice", companyPlatform.getShareprice());
			j.setObj(map);
			j.setMsg("查询成功！");
		}else{
			j.setMsg("查询失败");
			j.setSuccess(false);
		}
		return j;
	}
	/**
	 * 个人中心
	 */
	@RequestMapping(value = "/personalCenter" )
	@ResponseBody
	public AjaxJson personalCenter(){
		AjaxJson j = new AjaxJson();
		Map<String, Object> map = new HashMap<String, Object>();
		TSUser user =  ResourceUtil.getSessionUserName();
		user = systemService.findUniqueByProperty(TSUser.class, "id", user.getId());
		if(user != null){
			map.put("realname", user.getRealname());
			map.put("usemoney", user.getTsuserSalesman().getUsemoney());
			map.put("withdrawalsmoney", user.getTsuserSalesman().getWithdrawalsmoney());
			map.put("frozenmoney", user.getTsuserSalesman().getFrozenmoney());
			j.setObj(map);
			j.setMsg("查询数据成功");
			if("6".equals(user.getType())){
				map.clear();
				UserCopartnerAccountEntity userCopartnerAccountEntity  = systemService.findUniqueByProperty(UserCopartnerAccountEntity.class, "id", user.getId());
				if(userCopartnerAccountEntity != null){
					map.put("realname", userCopartnerAccountEntity.getUsercopartnerid().getUserName());
					map.put("usemoney", userCopartnerAccountEntity.getTotalmoney());
					map.put("withdrawalsmoney", userCopartnerAccountEntity.getUsablemoney());
					map.put("frozenmoney", userCopartnerAccountEntity.getFrozenmoney());
					j.setObj(map);
					j.setMsg("查询数据成功");
				}
			}
		}else{
			j.setMsg("暂无数据");
			j.setSuccess(false);
		}
		
		return j;
	}

	/**
	 * 采购商用户信息详情页
	 * @param TSUser HttpServletResponse
	 */
	private AjaxJson userClients(TSUser user, HttpServletResponse response){
		AjaxJson j = new AjaxJson();
		Map<String, Object> map = new HashMap<String, Object>();
		String statusName = "";
		String settlementName = "";
		if (StringUtil.isNotEmpty(user.getId())) {
			
			user = systemService.getEntity(TSUser.class, user.getId());
						
			if( StringUtil.isEmpty (user.getTsuserclients().getId().trim())){
				UserClientsEntity tsc = new UserClientsEntity() ;
				user.setTsuserclients(tsc);
			}
			
			//获取客户省会信息
			UserClientsEntity client = user.getTsuserclients();
			if(null != client && StringUtil.isNotEmpty(client.getId())){
				TSTerritory top1Territory = client.getTSTerritory();
				TSTerritory top2Territory = null;
				TSTerritory top3Territory = null;
				map.put("provinceid", "");
				map.put("cityid", "");
				map.put("regionid", "");
				if(null != top1Territory && StringUtil.isNotEmpty(top1Territory.getId())){
					top2Territory = top1Territory.getTSTerritory();
					map.put("provinceid", top1Territory.getId());
					map.put("provinceName", top1Territory.getTerritoryName());
				}
				if(null != top2Territory && StringUtil.isNotEmpty(top2Territory.getId())){
					top3Territory = top2Territory.getTSTerritory();
					map.put("provinceid", top2Territory.getId());
					map.put("cityid", top2Territory.getId());
					map.put("provinceName", top2Territory.getTerritoryName());
					map.put("cityName", top2Territory.getTerritoryName());
				}
				if(null != top3Territory && StringUtil.isNotEmpty(top3Territory.getId())){
					map.put("provinceid", top3Territory.getId());
					map.put("cityid", top2Territory.getId());
					map.put("regionid", top1Territory.getId());
					map.put("provinceName", top3Territory.getTerritoryName());
					map.put("cityName", top2Territory.getTerritoryName());
					map.put("regionName", top1Territory.getTerritoryName());
				}
				
			}
			
			try {
				//业务员空处理
				if( null == user.getTsuserclients().getYewuid() 
						|| (null != user.getTsuserclients().getYewuid() 
								&& StringUtil.isEmpty ( user.getTsuserclients().getYewuid().getId().trim() )) ){
					
					TSUser yewuUser = new TSUser();
					UserClientsEntity userClient =user.getTsuserclients();
					userClient.setYewuid(yewuUser);
					user.setTsuserclients(userClient);
				}
				
				//客户等级空处理
				UserTypeEntity userType = new UserTypeEntity();
				if( null !=user.getTsuserclients().getUsertpyrid()){
					try{
						userType = systemService.findUniqueByProperty(UserTypeEntity.class, "id", user.getTsuserclients().getUsertpyrid().getId());
					}catch(Exception e){
					}
					
					if(null == userType){
						userType = new UserTypeEntity();
					}
				}
				
				UserClientsEntity tsc =user.getTsuserclients();
				tsc.setUsertpyrid(userType);
				user.setTsuserclients(tsc);
				
			} catch (Exception e) {
				logger.error("查询异常："+e);
				UserClientsEntity tsc =user.getTsuserclients();
				TSUser yewuUser = new TSUser();
				tsc.setYewuid(yewuUser);
				user.setTsuserclients(tsc);
				
				UserTypeEntity userType = new UserTypeEntity();
				tsc.setUsertpyrid(userType);
				user.setTsuserclients(tsc);
				j.setMsg("查询异常");
				j.setSuccess(false);
				return j;
			}
			}else{
				user = new TSUser();
			}
			switch(user.getStatus()){
				case 1 : statusName = "启用";
					break;
				case 2 : statusName = "冻结";
					break;
				case 3 : statusName = "过期";
					break;
				case 4 : statusName = "待审核";
					break;
			}
			switch(Integer.parseInt(user.getTsuserclients().getSettlement() == null ? "0" : user.getTsuserclients().getSettlement())){
			case 1 : settlementName = "现款现货";
				break;
			case 2 : settlementName = "预付";
				break;
			case 3 : settlementName = "先货后款";
				break;
		}
			
			map.put("id", user.getId());
			map.put("userName", user.getUserName());
			map.put("realname", user.getRealname());
			map.put("realname", user.getRealname());
			map.put("yewuid", user.getTsuserclients().getYewuid().getId());
			map.put("yewuRealname", user.getTsuserclients().getYewuid().getRealname());
			map.put("usertpyrid", user.getTsuserclients().getUsertpyrid().getId());
			map.put("usertpyrTypename", user.getTsuserclients().getUsertpyrid().getTypename());
			map.put("status", user.getStatus());
			map.put("statusName", statusName);
			map.put("settlement", user.getTsuserclients().getSettlement());
			map.put("settlementName", settlementName);
			map.put("createdate", user.getCreateDate());
			map.put("companyName", user.getCompany().getCompanyName());
			map.put("companyAddress", user.getCompany().getAddress());
			j.setMsg("查询成功");
			j.setObj(map);
			return j;
	}
	/**
	 * 员工用户信息详情页
	 * @param TSUser HttpServletResponse
	 */
	private AjaxJson userStaff(TSUser user, HttpServletResponse response){
		AjaxJson j = new AjaxJson();
		Map<String, Object> map = new HashMap<String, Object>();
		String status = "";
		String roleUserId ="";
		String roleUserName ="";
		if (StringUtil.isNotEmpty(user.getId())) {
			user = systemService.getEntity(TSUser.class, user.getId());
			List<TSRoleUser> ru = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());//更新用户权限
			try {
				if( ru != null && ru.size() > 0){
					for(TSRoleUser tSRoleUser : ru){
						roleUserName +=tSRoleUser.getTSRole().getRoleName()+",";
						roleUserId +=tSRoleUser.getTSRole().getId()+",";
					}
					//过滤最后一个,
					roleUserId = roleUserId.substring(0, roleUserId.lastIndexOf(",")) ; 
					roleUserName = roleUserName.substring(0, roleUserName.lastIndexOf(",")) ; 
				}
			} catch (Exception e) {
				
			}
		}else{
			user = new TSUser();
			UserStaffEntity tsuserstaff = new UserStaffEntity();
			user.setTsuserstaff(tsuserstaff);
		}
		switch(user.getStatus()){
			case 1 : status = "启用";
					break;
			case 2 : status = "冻结";
					break;
			case 3 : status = "过期 ";
					break;
		}
		map.put("id", user.getId());
		map.put("userName", user.getUserName());
		map.put("realName", user.getRealname());
		map.put("mobilephone", user.getMobilephone());
		map.put("department", user.getTsuserstaff().getDepartment());
		map.put("department", user.getTsuserstaff().getDepartment());
		map.put("roleUserId", roleUserId);
		map.put("roleUserName", roleUserName);
		map.put("status", status);
		j.setObj(map);
		return j;
	}
	/**
	 * 发货商用户信息详情页
	 * @param TSUser HttpServletResponse
	 */
	private AjaxJson userCopartner(TSUser user, HttpServletResponse response){
		AjaxJson j = new AjaxJson();
		Map<String, Object> map = new HashMap<String, Object>();
		String status = "";
		String withdrawtype = "";
		String roleUserId ="";
		String roleUserName ="";
		//String partneraccountdes = "";
		if (StringUtil.isNotEmpty(user.getId())) {
			user = systemService.getEntity(TSUser.class, user.getId());
			if( null == user.getTsuserclients() || StringUtil.isEmpty (user.getTsuserclients().getId().trim())){
				UserClientsEntity tsc = new UserClientsEntity() ;
				user.setTsuserclients(tsc);
			}
			List<TSRoleUser> ru = systemService.findByProperty(TSRoleUser.class, "TSUser.id", user.getId());//更新用户权限
			try {
				if( ru != null && ru.size() > 0){
					for(TSRoleUser tSRoleUser : ru){
						roleUserName +=tSRoleUser.getTSRole().getRoleName()+",";
						roleUserId +=tSRoleUser.getTSRole().getId()+",";
					}
					//过滤最后一个,
					roleUserId = roleUserId.substring(0, roleUserId.lastIndexOf(",")) ; 
					roleUserName = roleUserName.substring(0, roleUserName.lastIndexOf(",")) ; 
				}
			} catch (Exception e) {
				
			}
		}
		switch(user.getStatus()){
			case 1 : status = "启用";
					break;
			case 2 : status = "冻结";
					break;
			case 3 : status = "过期 ";
					break;
			case 4 : status = "待审核";
					break;
		}
		switch(Integer.parseInt(user.getTsuserCopartner().getWithdrawtype() == null ? "0" : user.getTsuserCopartner().getWithdrawtype())){
			case 1 : withdrawtype = "支付宝支付";
					break;
			case 2 : withdrawtype = "微信支付";
					break;
			case 3 : withdrawtype = "线下转账 ";
					break;
			case 4 : withdrawtype = "现金支付";
					break;
		}
		/*switch(Integer.parseInt(user.getTsuserCopartner().getPartneraccountdes() == null ? "" : user.getTsuserCopartner().getPartneraccountdes())){
			case 1 : partneraccountdes = "中国银行";
					break;
			case 2 : partneraccountdes = "交通银行";
					break;
			case 3 : partneraccountdes = "工商银行 ";
					break;
			case 4 : partneraccountdes = "农业银行";
					break;
			case 5 : partneraccountdes = "建设银行";
					break;
			case 6 : partneraccountdes = "光大银行";
					break;
			case 7 : partneraccountdes = "民生银行";
					break;
			case 8 : partneraccountdes = "兴业银行";
				break;
			case 9 : partneraccountdes = "中信银行";
				break;
			case 10 : partneraccountdes = "招商银行";
				break;
			case 11 : partneraccountdes = "邮储银行";
				break;
			case 12 : partneraccountdes = "华夏银行";
				break;	
		}*/
		map.put("userName", user.getUserName());
		map.put("status", status);
		map.put("partnercompany", user.getTsuserCopartner().getPartnercompany());
		map.put("partnercontactman", user.getTsuserCopartner().getPartnercontactman());
		map.put("partnercontactphone", user.getTsuserCopartner().getPartnercontactphone());
		map.put("partneraccountname", user.getTsuserCopartner().getPartneraccountname());
		map.put("withdrawtype", withdrawtype);
		map.put("withdrawpiont", user.getTsuserCopartner().getWithdrawpiont());
		//map.put("partneraccountdes", partneraccountdes);
		map.put("partneraccount", user.getTsuserCopartner().getPartneraccount());
		map.put("roleUserId", roleUserId);
		map.put("roleUserName", roleUserName);
		j.setMsg("查询成功");
		j.setObj(map);
		return j;
	}
	
}
