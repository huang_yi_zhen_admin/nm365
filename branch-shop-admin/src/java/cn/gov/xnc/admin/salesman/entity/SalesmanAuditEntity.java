package cn.gov.xnc.admin.salesman.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 业务结算流水
 * @author zero
 * @date 2016-11-27 19:14:04
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_salesman_audit", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class SalesmanAuditEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**业务员*/
	private TSUser  ywuser;
	/**公司信息*/
	private TSCompany company;
	/**申请时间*/
	private java.util.Date createdate;
	/**申请_1,通过_2,未通过_3*/
	private java.lang.String state;
	/**申请金额*/
	private BigDecimal applicationmoney;
	/**订单总额*/
	private BigDecimal ordermoney;
	/**核定金额*/
	private BigDecimal checkmoney;
	/**锁定金额*/
	private BigDecimal frozenmoney;
	/**核定人*/
	private TSUser checkuser;
	/**核定时间*/
	private java.util.Date checkdate;
	/**remarks*/
	private java.lang.String remarks;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户ID
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "YWUSER")
	public TSUser getYwuser(){
		return this.ywuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户ID
	 */
	public void setYwuser(TSUser ywuser){
		this.ywuser = ywuser;
	}
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  申请时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  申请时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  申请_1,通过_2,未通过_3
	 */
	@Column(name ="STATE",nullable=true,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  申请_1,通过_2,未通过_3
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  申请金额
	 */
	@Column(name ="APPLICATIONMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getApplicationmoney(){
		return this.applicationmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  申请金额
	 */
	public void setApplicationmoney(BigDecimal applicationmoney){
		this.applicationmoney = applicationmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  订单总额
	 */
	@Column(name ="ORDERMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getOrdermoney(){
		return this.ordermoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  订单总额
	 */
	public void setOrdermoney(BigDecimal ordermoney){
		this.ordermoney = ordermoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  核定金额
	 */
	@Column(name ="CHECKMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getCheckmoney(){
		return this.checkmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  核定金额
	 */
	public void setCheckmoney(BigDecimal checkmoney){
		this.checkmoney = checkmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  锁定金额
	 */
	@Column(name ="FROZENMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getFrozenmoney(){
		return this.frozenmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  锁定金额
	 */
	public void setFrozenmoney(BigDecimal frozenmoney){
		this.frozenmoney = frozenmoney;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  核定人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHECKUSER")
	public TSUser getCheckuser(){
		return this.checkuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  核定人
	 */
	public void setCheckuser(TSUser checkuser){
		this.checkuser = checkuser;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  核定时间
	 */
	@Column(name ="CHECKDATE",nullable=true)
	public java.util.Date getCheckdate(){
		return this.checkdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  核定时间
	 */
	public void setCheckdate(java.util.Date checkdate){
		this.checkdate = checkdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  remarks
	 */
	@Column(name ="REMARKS",nullable=true,length=2000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  remarks
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
}
