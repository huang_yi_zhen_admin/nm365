package cn.gov.xnc.admin.salesman.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.salesman.service.SalesmanCommissionAuditServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("salesmanCommissionAuditService")
@Transactional
public class SalesmanCommissionAuditServiceImpl extends CommonServiceImpl implements SalesmanCommissionAuditServiceI {
	
}