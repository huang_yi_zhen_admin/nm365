package cn.gov.xnc.admin.salesman.controller;


import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.salesman.entity.SalesmanCommissionEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanCommissionServiceI;

/**   
 * @Title: Controller
 * @Description: 业务提现流水
 * @author zero
 * @date 2016-09-30 10:58:31
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/salesmanCommissionController")
public class SalesmanCommissionController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SalesmanCommissionController.class);

	@Autowired
	private SalesmanCommissionServiceI salesmanCommissionService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 业务提现流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView salesmanCommission(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/salesmanBonusList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(SalesmanCommissionEntity salesmanCommission,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(SalesmanCommissionEntity.class, dataGrid);
		OrderEntity order = salesmanCommission.getOrderid();
		if(null != order){
			cq.createAlias("orderid", "order");
			TSUser client = order.getClientid();
			if(null != client && StringUtil.isNotEmpty(client.getRealname())){
				cq.add(Restrictions.eq("order.clientid.realname", salesmanCommission));
			}
			if(null != client && StringUtil.isNotEmpty(client.getMobilephone())){
				cq.add(Restrictions.eq("order.clientid.mobilephone", salesmanCommission));
			}
			cq.add(Restrictions.eq("order.identifieror", order.getIdentifieror()));
		}
		//下单时间
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.add(Restrictions.between("orderid.createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf)));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.add(Restrictions.ge("orderid.createdate", DateUtils.str2Date(createdate1,sdf)));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.add(Restrictions.le("orderid.createdate", DateUtils.str2Date(createdate2,sdf)));
		}
		cq.eq("ywuser", user);
		cq.eq("company", user.getCompany());
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, salesmanCommission, request.getParameterMap());
		this.salesmanCommissionService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除业务提现流水
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(SalesmanCommissionEntity salesmanCommission, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		salesmanCommission = systemService.getEntity(SalesmanCommissionEntity.class, salesmanCommission.getId());
		message = "业务提现流水删除成功";
		salesmanCommissionService.delete(salesmanCommission);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加业务提现流水
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(SalesmanCommissionEntity salesmanCommission, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(salesmanCommission.getId())) {
			message = "业务提现流水更新成功";
			SalesmanCommissionEntity t = salesmanCommissionService.get(SalesmanCommissionEntity.class, salesmanCommission.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(salesmanCommission, t);
				salesmanCommissionService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "业务提现流水更新失败";
			}
		} else {
			message = "业务提现流水添加成功";
			salesmanCommissionService.save(salesmanCommission);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 业务提现流水列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(SalesmanCommissionEntity salesmanCommission, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(salesmanCommission.getId())) {
			salesmanCommission = salesmanCommissionService.getEntity(SalesmanCommissionEntity.class, salesmanCommission.getId());
			req.setAttribute("salesmanCommissionPage", salesmanCommission);
		}
		return new ModelAndView("admin/salesman/salesmanCommission");
	}
}
