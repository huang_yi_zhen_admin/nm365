package cn.gov.xnc.admin.salesman.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 业务提现流水
 * @author zero
 * @date 2016-09-30 10:58:31
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_salesman_commission", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class SalesmanCommissionEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**订单ID*/
	private OrderEntity orderid;
	/**用户ID*/
	private TSUser ywuser;
	
	/**公司信息*/
	private TSCompany company;
	
	/**创建时间*/
	private java.util.Date createdate;
	/**流水类型  支付_1,充值_2,业务奖励_3,业务提现_4*/
	private java.lang.String type;
	/**金额*/
	private BigDecimal money;
	
	/**状态  1 锁定    2 申请中   3 已经完成 */
	private java.lang.String received;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单ID
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDERID")
	public OrderEntity getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单ID
	 */
	public void setOrderid(OrderEntity orderid){
		this.orderid = orderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户ID
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "YWUSER")
	public TSUser getYwuser(){
		return this.ywuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户ID
	 */
	public void setYwuser(TSUser ywuser){
		this.ywuser = ywuser;
	}
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  流水类型 支付_1,充值_2,业务奖励_3,业务提现_4
	 */
	@Column(name ="TYPE",nullable=true,length=2)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  流水类型 支付_1,充值_2,业务奖励_3,业务提现_4
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  金额
	 */
	@Column(name ="MONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getMoney(){
		return this.money;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  金额
	 */
	public void setMoney(BigDecimal money){
		this.money = money;
	}


	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态 1 锁定    2 申请中   3 已经完成
	 */
	@Column(name ="RECEIVED",nullable=true,length=2)
	public java.lang.String getReceived(){
		return this.received;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态  1 锁定    2 申请中   3 已经完成
	 */
	public void setReceived(java.lang.String received){
		this.received = received;
	}

	

}
