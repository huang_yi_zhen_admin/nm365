package cn.gov.xnc.admin.order.entity;

/**
 * 单条物流轨迹
 * */

public class OrderLogisticsDetailEntity implements java.io.Serializable{

	public OrderLogisticsDetailEntity() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 轨迹详情
	 * */
	private String traceDetail;
	
	/**
	 * 轨迹发生时间
	 * */
	private String traceDetailTime;
	
	/**
	 * @return the traceDetail
	 */
	public String getTraceDetail() {
		return traceDetail;
	}
	/**
	 * @param traceDetail the traceDetail to set
	 */
	public void setTraceDetail(String traceDetail) {
		this.traceDetail = traceDetail;
	}
	/**
	 * @return the traceDetailTime
	 */
	public String getTraceDetailTime() {
		return traceDetailTime;
	}
	/**
	 * @param traceDetailTime the traceDetailTime to set
	 */
	public void setTraceDetailTime(String traceDetailTime) {
		this.traceDetailTime = traceDetailTime;
	}
	
	

}
