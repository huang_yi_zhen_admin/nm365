package cn.gov.xnc.admin.order.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 物流单号列表
 * @author zero
 * @date 2016-11-17 16:59:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_order_Logistics", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class OrderLogisticsEntity implements java.io.Serializable {
	
	public final static String CHANNEL_KDN = "KDN";
	public final static String CHANNEL_KD100 = "KD100";
	
	
	/**id*/
	private java.lang.String id;
	/**订单号*/
	private OrderEntity orderid;
	/**物流公司*/
	private java.lang.String logisticscompany;
	/**物流号*/
	private java.lang.String logisticsnumber;
	/**信息说明*/
	private java.lang.String message;
	/**确认状态*/
	private java.lang.String ischeck;
	/**物流公司*/
	private java.lang.String com;
	/**通信信息状态码*/
	private java.lang.String status;
	/**订单状态  在途中_0,已发货_1,疑难件_2,已签收_3,已退货_4,异常_5*/
	private java.lang.String state;
	///**condition*/
	//private java.lang.String conditionol;
	/**物流更新时间*/
	private java.util.Date logisticsdate;
	/**创建时间*/
	private java.util.Date departuredate;
	
	/**查询渠道，KDN表示快递鸟，KD100表示快递100，空为默认KDN，message的json根据渠道解析*/
	private String channel;
	
	/**数据更新时间*/
	private java.util.Date updatetime;
	
	
	
	
	
	
	/**
	 * @return the channel
	 */
	@Column(name ="CHANNEL",nullable=true,length=255)
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * @return the updatetime
	 */
	@Column(name ="UPDATETIME",nullable=true)
	public java.util.Date getUpdatetime() {
		return updatetime;
	}

	/**
	 * @param updatetime the updatetime to set
	 */
	public void setUpdatetime(java.util.Date updatetime) {
		this.updatetime = updatetime;
	}

	

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	
	
	
	
	
	/**
	 *方法: 设置java.lang.String 订单号
	 *@param: java.lang.String 订单号
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderid")
	public OrderEntity getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单号
	 */
	public void setOrderid(OrderEntity orderid){
		this.orderid = orderid;
	}
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流公司
	 */
	@Column(name ="logisticscompany",nullable=true,length=200)
	public java.lang.String getLogisticscompany() {
		return logisticscompany;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流公司
	 */
	public void setLogisticscompany(java.lang.String logisticscompany) {
		this.logisticscompany = logisticscompany;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流号
	 */
	@Column(name ="LOGISTICSNUMBER",nullable=true,length=50)
	public java.lang.String getLogisticsnumber(){
		return this.logisticsnumber;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  物流号
	 */
	public void setLogisticsnumber(java.lang.String logisticsnumber){
		this.logisticsnumber = logisticsnumber;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  信息说明
	 */
	@Column(name ="MESSAGE",nullable=true,length=2000)
	public java.lang.String getMessage(){
		return this.message;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  信息说明
	 */
	public void setMessage(java.lang.String message){
		this.message = message;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  确认状态
	 */
	@Column(name ="ISCHECK",nullable=true,length=10)
	public java.lang.String getIscheck(){
		return this.ischeck;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  确认状态
	 */
	public void setIscheck(java.lang.String ischeck){
		this.ischeck = ischeck;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流公司
	 */
	@Column(name ="COM",nullable=true,length=100)
	public java.lang.String getCom(){
		return this.com;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  物流公司
	 */
	public void setCom(java.lang.String com){
		this.com = com;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  通信信息状态码
	 */
	@Column(name ="STATUS",nullable=true,length=10)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  通信信息状态码
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单状态  在途中_0,已发货_1,疑难件_2,已签收_3,已退货_4,异常_5
	 */
	@Column(name ="STATE",nullable=true,length=10)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单状态  在途中_0,已发货_1,疑难件_2,已签收_3,已退货_4,异常_5
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
//	/**
//	 *方法: 取得java.lang.String
//	 *@return: java.lang.String  condition
//	 */
//	@Column(name ="CONDITION",nullable=true,length=500)
//	public java.lang.String getCondition(){
//		return this.condition;
//	}
//
//	/**
//	 *方法: 设置java.lang.String
//	 *@param: java.lang.String  condition
//	 */
//	public void setCondition(java.lang.String condition){
//		this.condition = condition;
//	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  物流更新时间
	 */
	@Column(name ="LOGISTICSDATE",nullable=true)
	public java.util.Date getLogisticsdate(){
		return this.logisticsdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  物流更新时间
	 */
	public void setLogisticsdate(java.util.Date logisticsdate){
		this.logisticsdate = logisticsdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  发货时间
	 */
	@Column(name ="DEPARTUREDATE",nullable=true)
	public java.util.Date getDeparturedate(){
		return this.departuredate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  发货时间
	 */
	public void setDeparturedate(java.util.Date departuredate){
		this.departuredate = departuredate;
	}
	
	
}
