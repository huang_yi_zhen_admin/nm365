package cn.gov.xnc.admin.order.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.product.entity.ProductEntity;

/**   
 * @Title: Entity
 * @Description: 退单信息
 * @author zero
 * @date 2017-12-27 11:54:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_order_reback_detail", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class OrderRebackDetailEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**退订单号*/
	private OrderRebackEntity cancelid;
	/**关联的小订单号*/
	private OrderEntity orderid;
	/**productid*/
	private ProductEntity productid;
	/**退订数量*/
	private BigDecimal rebacknum;
	/**每个商品退单金额*/
	private BigDecimal moneydrawback;
	/**单品退单总成本*/
	private BigDecimal basecost;
	/**备注*/
	private java.lang.String remarks;
	/**创建时间*/
	private java.util.Date createtime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  退订单号
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CANCELID")
	public OrderRebackEntity getCancelid(){
		return this.cancelid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  退订单号
	 */
	public void setCancelid(OrderRebackEntity cancelid){
		this.cancelid = cancelid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  关联的小订单号
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDERID")
	public OrderEntity getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  关联的小订单号
	 */
	public void setOrderid(OrderEntity orderid){
		this.orderid = orderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  productid
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  productid
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  退订数量
	 */
	@Column(name ="REBACKNUM",nullable=false,precision=12,scale=2)
	public BigDecimal getRebacknum(){
		return this.rebacknum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  退订数量
	 */
	public void setRebacknum(BigDecimal rebacknum){
		this.rebacknum = rebacknum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  每个商品退单金额
	 */
	@Column(name ="MONEYDRAWBACK",nullable=false,precision=15,scale=2)
	public BigDecimal getMoneydrawback(){
		return this.moneydrawback;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  每个商品退单金额
	 */
	public void setMoneydrawback(BigDecimal moneydrawback){
		this.moneydrawback = moneydrawback;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  单品退单总成本
	 */
	@Column(name ="BASECOST",nullable=false,precision=15,scale=2)
	public BigDecimal getBasecost(){
		return this.basecost;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  单品退单总成本
	 */
	public void setBasecost(BigDecimal basecost){
		this.basecost = basecost;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARKS",nullable=true,length=4000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
}
