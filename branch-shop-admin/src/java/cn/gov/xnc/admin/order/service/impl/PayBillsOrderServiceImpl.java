package cn.gov.xnc.admin.order.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.sql.ordering.antlr.OrderByFragmentRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsFlowEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockIOTypeServiceI;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.ComponentService;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("payBillsOrderService")

public class PayBillsOrderServiceImpl extends CommonServiceImpl implements PayBillsOrderServiceI {
	
	private Logger logger = Logger.getLogger(PayBillsOrderServiceImpl.class);
	@Autowired
	private SystemService systemService;
	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private StockProductPriceServiceI stockProductPriceService;
	@Autowired
	private StockIODetailServiceI stockIODetailService;
	@Autowired
	private StockIOTypeServiceI stockIOTypeService;
	@Autowired
	private ComponentService componentService;
	@Autowired
	private DictionaryService dictionaryService;
	
	
	/**
	 *创建一个充值类型的流水订单 
	 * 
	 **/
	public PayBillsOrderEntity  payBillsOrderUser(  PayBillsOrderEntity payBillsOrder  ){
		
		
		
		
		
		
		return null;
		
	}

	@Override
	public StatisPageVO statisUserPaybills(TSCompany company, TSUser user,HttpServletRequest request) {
		String type = request.getParameter("type");
		String ordertype = request.getParameter("ordertype");//支付单类型
		String identifier = request.getParameter("identifier");
		String clientid = request.getParameter("clientid.id");
//		String state = request.getParameter("state");
		String paymentmethod = request.getParameter("paymentmethod");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(a.alreadyPaidMoney), 0) value1, IFNULL(SUM(a.payableMoney), 0) value2, IFNULL(SUM(a.obligationsMoney), 0) value3, count(1) value4 FROM xnc_pay_bills_order a ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		
		if(StringUtil.isNotEmpty(type) && StringUtil.isNotEmpty(type.trim())){
			Sql.append(" AND a.type = '" + type + "'");
		}
		if(StringUtil.isNotEmpty(ordertype) 
				&& ordertype.indexOf(",") > -1){//应付
			
			Sql.append(" AND a.state in (" + ordertype + ") ");
			
		}else if(StringUtil.isNotEmpty(ordertype) 
				&& ordertype.indexOf(",") == -1){
			
			Sql.append(" AND a.state ='"+ ordertype + "'");
		}
		
		if(StringUtil.isNotEmpty(identifier) && StringUtil.isNotEmpty(identifier.trim())){
			Sql.append(" AND a.identifier like '%" + identifier.trim() + "%'");
		}
//		if(StringUtil.isNotEmpty(state) && StringUtil.isNotEmpty(state.trim())){
//			Sql.append("AND a.state = '" + state.trim() + "'");
//		}
		if(null != user && StringUtil.isNotEmpty(user.getId())){
			Sql.append("AND a.clientId = '" + user.getId() + "'");
		}
		if(StringUtil.isNotEmpty(clientid) && StringUtil.isNotEmpty(clientid.trim())){
			Sql.append("AND a.clientId = '" + clientid.trim() + "'");
		}
		if(StringUtil.isNotEmpty(paymentmethod) && StringUtil.isNotEmpty(paymentmethod.trim())){
			Sql.append("AND a.PaymentMethod = '" + paymentmethod.trim() + "'");
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append("AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append("AND a.createDate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append("AND a.createDate <= '" + createdate2 + "'" );
		}

		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		
		return statisPageVO;
	}
	
	
	@Override
	public List<PayBillsOrderEntity> getBillsOrderListByIds(String billsIds) {
		List<PayBillsOrderEntity> billsorderlist = new ArrayList<PayBillsOrderEntity>();
		
		if(StringUtil.isNotEmpty(billsIds)){
			String[] billsIdList = billsIds.split(",");
			
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
			cq.setCurPage(1);
			cq.setPageSize(30);
			cq.in("id", billsIdList);
			cq.add();
			
			billsorderlist = getListByCriteriaQuery(cq, false);
		}
		
		
		return billsorderlist;
	}
	
	/**
	 * 先货后款审核
	 * @param payBillsOrder
	 * @param request
	 * @return
	 */
	@Override
	public AjaxJson sendFirstPaymentAudit(PayBillsOrderEntity payBillsOrder, HttpServletRequest request) {
		
		boolean success = true;
		int orderFailSize = 0;
		String letitgo = request.getParameter("letItGo");
		
		String msg = "先货后款通过审核";
		if("0".equals(letitgo)){
			msg = "取消先货后款";
		}
		
		AjaxJson json = new AjaxJson();
		//只有客户选择了先货后款的订单，才可以使用先货后款审核功能
		if(null != payBillsOrder && StringUtil.isNotEmpty(payBillsOrder.getId()) && "6".equals(payBillsOrder.getPaymentmethod())){
			
			try {
				List<OrderEntity> orderlist = payBillsOrder.getOrderS();
				if(null != orderlist && orderlist.size() > 0){
					
					for (OrderEntity orderEntity : orderlist) {
						
						String state = orderEntity.getState();
						if("1".equals(letitgo) && "1".equals( state )){//只有未付款状态的单子可以先货后款
							orderEntity.setState("2");
							
						}else if("0".equals(letitgo) && "2".equals(state)){//只有待发货状态的单子才可以取消先货后款
							orderEntity.setState("1");
							
						}else {
							msg = "不符合先货后款状态，" + msg + "失败！订单号：" + orderEntity.getId();
							systemService.addLog(msg, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
							orderFailSize++;
							break;
						}
					}
					//更新订单状态
					if(orderFailSize == 0){
						batchUpdate(orderlist);
						msg += "成功！";
						success = true;
						
					}else{
						success = false;
						msg += "失败！抱歉~您处理的单子有部分不符合先货后款状态，请查看详细！";
					}
					
					//取消订单的先货后款状态
					if("0".equals(letitgo) && success){
						PayBillsOrderEntity payBillsOrderEntity = findUniqueByProperty(PayBillsOrderEntity.class,"id",payBillsOrder.getId());
						payBillsOrderEntity.setPaymentmethod(null);//设置下单支付方式为 取消先货后款
						systemService.updateEntitie(payBillsOrderEntity);
					}
				}
			} catch (Exception e) {
				msg= "先货后款通过审核失败，请联系系统管理人员";
				success = false;
				logger.error("------------" + msg + e);
			}
			
		}else{
			msg= "先货后款通过审核失败~订单信息获取失败【或者】不是现货后款订单！";
			success = false;
			systemService.addLog(msg, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			logger.error("------------" + msg);
		}
		json.setMsg(msg);
		json.setSuccess(success);
		return json;
	}
	
	/**
	 * 获取大单物流状态
	 */
	@Override
	public TSDictionary getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder) {
		TSDictionary freightState = null;
		Integer hasFreightOrderNum = 0;
		if(paybillsOrder ==  null){
			return null;
		}
		
		PayBillsOrderEntity payBills = findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
		if(payBills ==  null){
			return null;
		}
		
		List<OrderEntity> orderList = payBills.getOrderS();
		if(!ListUtil.isNotEmpty(orderList)){
			return null;
		}
		
		//当前关联的订单
		for (OrderEntity orderEntity : orderList) {
			TSDictionary orderFreightState = orderEntity.getFreightState();
			if(orderFreightState == null){
				continue;
			}
			//记录物流状态 已签收、已发货 的订单
			if(orderFreightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_DELIVER.getValue())){
				hasFreightOrderNum++;
			}else if(orderFreightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_SIGN.getValue())){
				hasFreightOrderNum++;
			}
		}
		//根据订单已发货和已签收的数量对比订单的数量，决定物流状态是否已发货和部分发货
		if( orderList.size() == hasFreightOrderNum ){
			//已发货
			freightState = dictionaryService.checkDictionaryItem("freightStates", OrderServiceI.FreightState.ALREADY_DELIVER.getValue());
			
		}else if( hasFreightOrderNum > 0 && orderList.size() > hasFreightOrderNum ){
			//部分发货
			freightState = dictionaryService.checkDictionaryItem("freightStates", OrderServiceI.FreightState.PART_DELIVER.getValue());
			
		}
		
		return freightState;
	}

	@Override
	public AjaxJson cancelPayBillsOrder(PayBillsOrderEntity paybillsOrder, HttpServletRequest request) throws Exception {
		AjaxJson json = new AjaxJson();
		
		if(StringUtil.isEmpty(paybillsOrder.getId())){
			json.setSuccess(false);
			json.setMsg( "系统没有找到您要取消的订单！" );
			return json;
		}
		
		PayBillsOrderEntity payBillsOrderEntity = findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
		
		if(PayBillsOrderServiceI.State.UNPAID.getValue().equals( payBillsOrderEntity.getState() )){
			
			List<OrderEntity> orderlist = payBillsOrderEntity.getOrderS();
			for (OrderEntity orderEntity : orderlist) {
				
				if( !OrderServiceI.State.DELIVERED.getValue().equals( orderEntity.getState() ) 
						|| !OrderServiceI.State.RECEIVED.getValue().equals( orderEntity.getState() )
						|| !OrderServiceI.State.CANCELREJECT.getValue().equals( orderEntity.getState() )){
					
					orderEntity.setState( OrderServiceI.State.CANCEL.getValue());
					
				}else{
					json.setSuccess(false);
					json.setMsg( "取消订单失败，您有部分订单已经发货或者签收，请联系供货商进行处理或者申请退单！" );
					break;
				}
			}
			
			if(json.isSuccess()){
				
				batchSave(orderlist);
				
				payBillsOrderEntity.setState( PayBillsOrderServiceI.State.CANCEL.getValue() );
				
				updateEntitie( payBillsOrderEntity );
			}
		}
		else if(PayBillsOrderServiceI.State.CANCEL.getValue().equals( payBillsOrderEntity.getState() )){
			json.setSuccess(false);
			json.setMsg( "不要闹，您的订单已经取消！" );
			
		}else{
			
			json.setSuccess(false);
			json.setMsg( "取消订单失败，只有未支付订单才能取消，其它情况请申请退单！" );
		}
		return json;
	}
	
	/**
	 * 退单更新订单状态
	 * 
	 * */
	public void setRebackStatus(PayBillsOrderEntity payBillsOrder){
		
		payBillsOrder = findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrder.getId());
		
		List<OrderEntity> orderlist = payBillsOrder.getOrderS();
		
		int cancelcnt = 0;
		if( null != orderlist && orderlist.size() > 0 ){
			for( OrderEntity order : orderlist ){
				if( OrderServiceI.State.REBACK.getValue().equals(order.getState())){
					cancelcnt ++ ;
				}
			}
		}
		
		if( cancelcnt > 0 &&  cancelcnt == orderlist.size() ){
			payBillsOrder.setState( PayBillsOrderServiceI.State.REBACK.getValue() );
		}
	}
	
	/**
	 * 下单自动出库
	 * @param payBills
	 * @return
	 */
	public AjaxJson payBillsOrderStockOut(PayBillsEntity payBills,HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson result = new AjaxJson();
		String message = "";
		
		PayBillsEntity payBillsEntity = systemService.get(PayBillsEntity.class, payBills.getId());
		List<PayBillsFlowEntity> payBillsFlows = payBillsEntity.getPayBillsFlowS();
		
		if(payBillsFlows != null && payBillsFlows.size() > 0){
			for( int i=0 ; i < payBillsFlows.size() ; i++ ){
				 PayBillsOrderEntity payBillsOrder  =payBillsFlows.get(i).getBillsorderid();
				 List<OrderEntity> orders = payBillsOrder.getOrderS();
				 
				 List<Map<String, Object>> exps = new ArrayList<Map<String, Object>>();
				 for (OrderEntity orderEntity : orders) {
					ProductEntity productEntity = orderEntity.getProductid();
					
					//检查库存商品
					//根据商品获取库存商品状况
					Map<String, Object> stockProductParams = new HashMap<String,Object>();
					stockProductParams.put("productEntity", productEntity);
					List<StockProductEntity> stockProducts = stockProductService.checkStockProduct(stockProductParams);
					if(stockProducts == null){
						result.setSuccess(false);
						result.setMsg("当前"+productEntity.getName()+"商品已售清");
						return result;
					}
					
					//检查下单自动出库服务开关
					boolean orderStockOutSwitch = componentService.checkOrderStockOutSwitch();
					if(!orderStockOutSwitch){
						return result;
					}
					
					//对库存商品进行出库操作
					if(stockProducts != null){
						StockProductEntity stockProductEntity = stockProducts.get(0);
						StockEntity stockEntity = stockProductEntity.getStockid();
						ProductUnitEntity productUnitEntity = productEntity.getUnitBase();
						StockSegmentEntity stockSegmentEntity = stockProductEntity.getStockSegment();
						TSCompany company =  user.getCompany();
						//出库类型
						TSDictionary dict = new TSDictionary();
						dict.setDictionaryType(StockIOType.OUT);
						dict.setDictionaryName(StockIOType.StockOutType.SALE_OUT.getName());
						dict.setDictionaryDesc(StockIOType.StockOutType.SALE_OUT.getName());
						dict.setDictionaryValue(StockIOType.StockOutType.SALE_OUT.getValue());
						
						TSDictionary dictEntity = dictionaryService.check2addDictionaryItem(dict);
						
						//订单的商品数量
						BigDecimal orderNum = ConvertTool.toBigDecimal(orderEntity.getNumber());
						
						//基本信息
						StockIOEntity stockIOEntity = new StockIOEntity();
						stockIOEntity.setIdentifier(IdWorker.generateSequenceNo());
						stockIOEntity.setRemark("下单自动出库");
						stockIOEntity.setStockid(stockEntity);
						stockIOEntity.setStocktypeid(dictEntity);
						stockIOEntity.setCompanyid(company);
						stockIOEntity.setCreatetime(DateUtils.getDate());
						stockIOEntity.setOperator(user);
						stockIOEntity.setStockid(stockEntity);
						stockIOEntity.setStockIODetailList(null);
						
						//当前库存商品单价
						BigDecimal stockNum = stockProductEntity.getStocknum();
						BigDecimal unitPrice = stockProductEntity.getAverageprice();
						
						//当前出库总价
						BigDecimal cur_totalPrice = orderNum.multiply(unitPrice).setScale(4,BigDecimal.ROUND_HALF_UP);
						BigDecimal cur_stockNum = new BigDecimal(0.00);
						
						//缺货的情况，备注说明
						StringBuffer remark = new StringBuffer();
						if(stockNum.compareTo(orderNum) < 0){
							cur_stockNum = stockNum.subtract(orderNum);
							remark.append(productEntity.getName()).append("在库存中缺少").append(cur_stockNum.abs()).append(productUnitEntity.getName());
						}
						
						//出库商品详情
						Map<String, Object> stockIODetailParams = new HashMap<String, Object>();
						stockIODetailParams.put("id", productEntity.getId());
						stockIODetailParams.put("name", productEntity.getName());
						stockIODetailParams.put("detailremark", remark.toString());
						stockIODetailParams.put("unitid", productEntity.getUnitBase().getId());
						stockIODetailParams.put("segmentid", stockSegmentEntity.getId());
						stockIODetailParams.put("freightprice", 0);
						stockIODetailParams.put("unitprice", unitPrice);
						stockIODetailParams.put("totalprice", cur_totalPrice);
						stockIODetailParams.put("iostocknum", orderNum);
						exps.add(stockIODetailParams);
						
						List<StockIODetailEntity> stockIODetailList = stockIODetailService.createStockIODetail(stockIOEntity, exps);
						if(stockIODetailList.size() > 0 ){
							//保存出库基本信息
							systemService.save(stockIOEntity);
							//批量保存出库详情信息
							systemService.batchSave(stockIODetailList);
							stockIOEntity.setStockIODetailList(stockIODetailList);
							systemService.updateEntitie(stockIOEntity);
							for (StockIODetailEntity stockIODetail : stockIODetailList) {
								Map<String, Object> params = new HashMap<String, Object>();
								StockSegmentEntity segmentEntity = stockIODetail.getStockSegment();
								params.put("ioType", StockIOType.OUT);
								params.put("stockEntity", stockEntity);
								params.put("segmentEntity", segmentEntity);
								params.put("companyEntity", company);
								params.put("productEntity",productEntity);
								
								params.put("op_stockNum",ConvertTool.toBigDecimal(stockIODetail.getIostocknum()));
								params.put("op_totalPrice",ConvertTool.toBigDecimal(stockIODetail.getTotalprice()));
								params.put("op_avgPrice",ConvertTool.toBigDecimal(stockIODetail.getUnitprice()));
								//更新库存商品状况
								boolean check = stockProductService.saveOrUpdateStockProduct( params);
								if(!check){
									systemService.delete(stockIOEntity);
									systemService.deleteAllEntitie(stockIODetailList);
									result.setSuccess(false);
									message = StockIOType.StockOutType.SALE_OUT.getName() + "失败";
									systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
									result.setMsg(message);
									return result;
								}
								//更新库存商品所有统计价格
								stockProductPriceService.updateStockProductPrice(productEntity, stockEntity,request);
							}
						}
					}
				 }
			}
		}
		return result;
	}
}