package cn.gov.xnc.admin.order.service;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderLogisticsServiceI extends CommonService{

	public void saveOrderLogistics(OrderEntity order)  throws Exception;
	
	public void deleteAllOrderLogistics(OrderEntity order)  throws Exception;
	
	public void deleteOrderLogistics(String orderid, String logisticsCode)  throws Exception;
	
	public void updateOrderLogistics(String orderid, String logisticsCode, String logisticsCompany)  throws Exception;
	
	public AjaxJson getExpressInfo(String orderid, String logisticsCode, String logisticsCompany) throws Exception;
}
