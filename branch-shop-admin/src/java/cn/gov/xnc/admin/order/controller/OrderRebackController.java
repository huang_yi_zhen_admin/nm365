package cn.gov.xnc.admin.order.controller;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderRebackEntity;
import cn.gov.xnc.admin.order.service.OrderRebackServiceI;
import cn.gov.xnc.admin.order.service.OrderRebackTypeServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 退单信息
 * @author zero
 * @date 2017-12-27 11:53:47
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderRebackController")
public class OrderRebackController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderRebackController.class);

	@Autowired
	private OrderRebackServiceI orderRebackService;
	@Autowired
	private OrderRebackTypeServiceI orderRebackTService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 申请退单详细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView orderCancel(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		
		HashMap<String, String> param = new HashMap<String, String>();
		try {
			param.put("status", OrderRebackServiceI.Status.AUDIT.getValue());
			StatisPageVO statisVo1 = orderRebackService.statisCancelOrder(company, param);
			
			param.put("status", OrderRebackServiceI.Status.PASS.getValue());
			StatisPageVO statisVo2 = orderRebackService.statisCancelOrder(company, param);
			
			param.put("status", OrderRebackServiceI.Status.UNPASS.getValue());
			StatisPageVO statisVo3 = orderRebackService.statisCancelOrder(company, param);
			
			request.setAttribute("cancelApply", statisVo1);
			request.setAttribute("cancelPass", statisVo2);
			request.setAttribute("cancelReject", statisVo3);
			
		} catch (Exception e) {
			logger.error(e);
		}
		
		return new ModelAndView("admin/order/orderRebackList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderRebackEntity orderCancel,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		//查询条件组装器
		CriteriaQuery cq = getDatagridCriteriaQuery(orderCancel, request, response, dataGrid);
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderCancel, request.getParameterMap());
		this.orderRebackService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**组建Criteria查询语句*/
	private CriteriaQuery getDatagridCriteriaQuery(OrderRebackEntity orderCancel,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid){
		CriteriaQuery cq = new CriteriaQuery(OrderRebackEntity.class, dataGrid);
		
		if(StringUtil.isNotEmpty(orderCancel.getId()) ){
			cq.like("id", "%"+orderCancel.getId()+"%");
			orderCancel.setId(null);
		}
		
		
		cq.createAlias("billsorderid", "billsorder");
		
		if(null != orderCancel.getBillsorderid() ){
			
			if(null != orderCancel.getBillsorderid() && StringUtil.isNotEmpty(orderCancel.getBillsorderid().getClientid().getUserName())){
				cq.createAlias("billsorder.clientid", "client");
				cq.add(Restrictions.or(Restrictions.like("client.userName", "%"+orderCancel.getBillsorderid().getClientid().getUserName()+"%"), Restrictions.like("client.realname", "%"+orderCancel.getBillsorderid().getClientid().getUserName()+"%")));
				orderCancel.getBillsorderid().setClientid(null);
			}
			
//			if(null != orderCancel.getBillsorderid().get && StringUtil.isNotEmpty(orderCancel.getBillsorderid().getSalemanid().getUserName())){
//				cq.createAlias("billsorder.salemanid", "saleman");
//				cq.add(Restrictions.or(Restrictions.like("saleman.userName", "%"+orderCancel.getBillsorderid().getSalemanid().getUserName()+"%"), Restrictions.like("saleman.realname", "%"+orderCancel.getBillsorderid().getSalemanid().getUserName()+"%")));
//				orderCancel.getBillsorderid().setSalemanid(null);
//			}
			
			if(StringUtil.isNotEmpty(orderCancel.getBillsorderid().getId())){
				cq.add(Restrictions.like("billsorder.id", "%"+orderCancel.getBillsorderid().getId() + "%"));
			}
			
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createtime", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat), DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createtime", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createtime", DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}
		cq.addOrder("createtime", SortDirection.desc);
		cq.add();
		
		return cq;
	}
	
	
	@RequestMapping(value = "datagridStatis")
	@ResponseBody
	public AjaxJson datagridStatis(OrderRebackEntity orderCancel,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		AjaxJson j = new AjaxJson();
		j.setSuccess(false);
		j.setMsg("查询失败");
		
		CriteriaQuery cq = getDatagridCriteriaQuery(orderCancel, request, response, dataGrid);
		
		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.sum("totalmoneytdrawback"));
		projectionList.add(Projections.sum("totalbasecost"));

		
		cq.setProjectionList(projectionList);
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderCancel, request.getParameterMap());
		orderRebackService.getDataGridReturn(cq, true);
		
		dataGrid = cq.getDataGrid();
		
		List<Object[]> list = dataGrid.getResults();
		
		if( null!= list && list.size() > 0 ){
			Object[] objlist = list.get(0);
			Map<String,Object> map = new HashMap<>();
			map.put("moneydrawback", (BigDecimal)objlist[0]);
			map.put("basecost",(BigDecimal) objlist[1]);
			
			j.setObj(map);
			j.setSuccess(true);
			j.setMsg("查询成功");
		}
		
		
		return j;
	}
	
	/**
	 * 删除申请退单详细
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(OrderRebackEntity orderCancel, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		orderCancel = systemService.getEntity(OrderRebackEntity.class, orderCancel.getId());
		message = "申请退单详细删除成功";
		orderRebackService.delete(orderCancel);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 更新申请退单详细
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderRebackEntity orderCancel, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "申请退单更新成功";
		
		
		try {
			
			j = orderRebackService.audit(orderCancel);
			OrderRebackEntity t = orderRebackService.get(OrderRebackEntity.class, orderCancel.getId());
			
			
			systemService.opLog(request, OperationLogUtil.ORDER_CANCLE, t, "退单审核操作，申请人："+t.getClient().getRealname());
			
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			
		} catch (Exception e) {
			e.printStackTrace();
			message = "申请退单更新失败," + e.getMessage();
			j.setMsg(message);
			j.setSuccess(false);
		}
		if( j.isSuccess() ){
			j.setMsg(message);
		}
		return j;
	}

	/**
	 * 申请退单详细列表页面跳转
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderRebackEntity orderCancel, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(orderCancel.getId())) {
			OrderRebackEntity t = orderRebackService.getEntity(OrderRebackEntity.class, orderCancel.getId());
			t.setStatus(orderCancel.getStatus());
			
			req.setAttribute("orderCancelPage", t);
		}
		return new ModelAndView("admin/order/orderReback");
	}
	
	/**
	 * 新建退单
	 * @return
	 */
	@RequestMapping(value = "addOrderCancel")
	public ModelAndView addOrderCancel(OrderRebackEntity orderCancel, HttpServletRequest req) {
		String cancelIds = req.getParameter("cancelIds");
		req.setAttribute("cancelIds", cancelIds);
		req.setAttribute("billsorderid", req.getParameter("bid"));
		req.setAttribute("orderRebackList", orderRebackTService.getOrderRebackTypeList());
		
		return new ModelAndView("admin/order/addOrderReback");
	}
	
	/**
	 * 保存退单申请
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "saveOrderCancel")
	@ResponseBody
	public AjaxJson saveOrderCancel(OrderRebackEntity orderCancel, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "退单申请成功";
		try {
			orderRebackService.addOrderCancel(orderCancel, request);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			message = "申请退单详细更新失败:" +e;
			j.setSuccess(false);
		}
		j.setMsg(message);
		return j;
	}
	
	
	/**
	 * 申请退单详细列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "getCancelOrderList")
	@ResponseBody
	public AjaxJson getCancelOrderList(HttpServletRequest req, HttpServletResponse resp) {
		AjaxJson jsonObj = new AjaxJson();
				
		String cancelIds = req.getParameter("cancelIds");
		
		if (StringUtil.isNotEmpty(cancelIds)) {
			
			try {
				String jsonCancelList = orderRebackService.jsonCancelOrderList(cancelIds);
				jsonObj.setObj(jsonCancelList);
				
			} catch (Exception e) {
				
				jsonObj.setSuccess(false);
				jsonObj.setMsg("获取退订商品失败~");
			}
			
		}else{
			jsonObj.setSuccess(false);
			jsonObj.setMsg("传入取消单号不能为空~");
		}
		return jsonObj;
	}
}
