package cn.gov.xnc.admin.order.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.order.entity.OrderRebackTypeEntity;
import cn.gov.xnc.admin.order.service.OrderRebackTypeServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("orderRebackTypeService")
@Transactional
public class OrderRebackTypeServiceImpl extends CommonServiceImpl implements OrderRebackTypeServiceI {

	@Override
	public List<OrderRebackTypeEntity> getOrderRebackTypeList() {
		TSUser user = ResourceUtil.getSessionUserName();
		List<OrderRebackTypeEntity> orderRebackList = null;
		CriteriaQuery cq = new CriteriaQuery(OrderRebackTypeEntity.class);
		cq.eq("companyid.id", user.getCompany().getId());
		cq.add();
		
		orderRebackList = getListByCriteriaQuery(cq, false);
		return orderRebackList;
	}
	
}