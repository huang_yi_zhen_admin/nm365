package cn.gov.xnc.admin.order.service;

import java.util.List;

import cn.gov.xnc.admin.order.entity.OrderRebackTypeEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderRebackTypeServiceI extends CommonService{

	public List<OrderRebackTypeEntity> getOrderRebackTypeList();
}
