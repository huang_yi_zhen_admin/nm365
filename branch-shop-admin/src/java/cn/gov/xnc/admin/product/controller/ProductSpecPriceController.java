package cn.gov.xnc.admin.product.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.product.entity.ProductSpecPriceEntity;
import cn.gov.xnc.admin.product.service.ProductSpecPriceServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 商品价格信息对应表
 * @author zero
 * @date 2018-02-03 11:29:20
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/productSpecPriceController")
public class ProductSpecPriceController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductSpecPriceController.class);

	@Autowired
	private ProductSpecPriceServiceI productSpecPriceService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 商品价格信息对应表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView productSpecPrice(HttpServletRequest request) {
		return new ModelAndView("admin/product/productSpecPriceList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ProductSpecPriceEntity productSpecPrice, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ProductSpecPriceEntity.class, dataGrid);
		cq.eq("status", "U");
		cq.add();
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, productSpecPrice, request.getParameterMap());
		this.productSpecPriceService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除商品价格信息对应表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ProductSpecPriceEntity productSpecPrice, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		boolean isdel = true;
		message = "商品价格信息对应表删除成功";
		try {
			isdel = productSpecPriceService.delSpecPriceById(productSpecPrice.getId());
			
		} catch (Exception e) {
			isdel = false;
			logger.error(e);
		}
		
		if(!isdel){
			message = "商品价格信息删除失败，请联系管理员！";
		}
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加商品价格信息对应表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ProductSpecPriceEntity productSpecPrice, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(productSpecPrice.getId())) {
			message = "商品价格信息对应表更新成功";
			ProductSpecPriceEntity t = productSpecPriceService.get(ProductSpecPriceEntity.class, productSpecPrice.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(productSpecPrice, t);
				productSpecPriceService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "商品价格信息对应表更新失败";
			}
		} else {
			message = "商品价格信息对应表添加成功";
			productSpecPriceService.save(productSpecPrice);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 商品价格信息对应表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ProductSpecPriceEntity productSpecPrice, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productSpecPrice.getId())) {
			productSpecPrice = productSpecPriceService.getEntity(ProductSpecPriceEntity.class, productSpecPrice.getId());
			req.setAttribute("productSpecPricePage", productSpecPrice);
		}
		return new ModelAndView("admin/product/productSpecPrice");
	}
}
