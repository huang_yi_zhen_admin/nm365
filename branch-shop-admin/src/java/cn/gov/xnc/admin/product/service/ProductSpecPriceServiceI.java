package cn.gov.xnc.admin.product.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductSpecPriceEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

public interface ProductSpecPriceServiceI extends CommonService{
	
	/**
	 * 保存多商品多规格价格
	 * @param req
	 * @param p
	 * @param company
	 * @return
	 * @throws Exception
	 */
	public List<ProductSpecPriceEntity> saveSpecPriceByRequest(HttpServletRequest req, ProductEntity p, TSCompany company) throws Exception;
	
	/**
	 * 删除多规格价格
	 * @param specid
	 * @return
	 * @throws Exception
	 */
	public boolean delSpecPriceById(String specid) throws Exception;
	
	/**
	 * 查看商品是否有多规格价格
	 * @param specid
	 * @return
	 * @throws Exception
	 */
	public Integer getCountSpecPriceByProductID(String productid) throws Exception;
	
	/**
	 * 获取多规格价格中最小的价格
	 * @param productid
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getMinSpecPriceByProductID(String productid) throws Exception;
	
}
