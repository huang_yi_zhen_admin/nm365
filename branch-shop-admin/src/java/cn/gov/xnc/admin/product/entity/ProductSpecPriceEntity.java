package cn.gov.xnc.admin.product.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

/**   
 * @Title: Entity
 * @Description: 商品价格信息对应表
 * @author zero
 * @date 2018-02-03 11:29:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_product_spec_price", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ProductSpecPriceEntity implements java.io.Serializable {
	/**多规格编码*/
	private java.lang.String id;
	/**商品编码*/
	private ProductEntity productid;
	/**多规格名称*/
	private java.lang.String specname;
	/**多规格组合单位*/
	private ProductUnitEntity unitid;
	/**采购价格*/
	private BigDecimal orderprice;
	/**市场价格*/
	private BigDecimal normalprice;
	/**成本价格*/
	private BigDecimal costprice;
	/**虚拟库存数量*/
	private BigDecimal stocknum;
	/**所属公司*/
	private TSCompany companyid;
	/**状态：U 正在使用 D 删除*/
	private java.lang.String status;
	/**updatetime*/
	private java.util.Date updatetime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  多规格编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  多规格编码
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品编码
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  多规格名称
	 */
	@Column(name ="SPECNAME",nullable=false,length=255)
	public java.lang.String getSpecname(){
		return this.specname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  多规格名称
	 */
	public void setSpecname(java.lang.String specname){
		this.specname = specname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  多规格组合单位
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNITID")
	public ProductUnitEntity getUnitid(){
		return this.unitid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  多规格组合单位
	 */
	public void setUnitid(ProductUnitEntity unitid){
		this.unitid = unitid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  采购价格
	 */
	@Column(name ="ORDERPRICE",nullable=false,precision=12,scale=2)
	public BigDecimal getOrderprice(){
		return this.orderprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  采购价格
	 */
	public void setOrderprice(BigDecimal orderprice){
		this.orderprice = orderprice;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  市场价格
	 */
	@Column(name ="NORMALPRICE",nullable=false,precision=12,scale=2)
	public BigDecimal getNormalprice(){
		return this.normalprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  市场价格
	 */
	public void setNormalprice(BigDecimal normalprice){
		this.normalprice = normalprice;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  虚拟库存数量
	 */
	@Column(name ="STOCKNUM",nullable=true,precision=12,scale=2)
	public BigDecimal getStocknum(){
		return this.stocknum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  虚拟库存数量
	 */
	public void setStocknum(BigDecimal stocknum){
		this.stocknum = stocknum;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属公司
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属公司
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态：U 正在使用 D 删除
	 */
	@Column(name ="STATUS",nullable=false,length=2)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态：U 正在使用 D 删除
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  updatetime
	 */
	@Column(name ="UPDATETIME",nullable=false)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  updatetime
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}

	public BigDecimal getCostprice() {
		return costprice;
	}

	public void setCostprice(BigDecimal costprice) {
		this.costprice = costprice;
	}
}
