package cn.gov.xnc.admin.product.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductSpecPriceEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.product.service.ProductSpecPriceServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

@Service("productSpecPriceService")
@Transactional
public class ProductSpecPriceServiceImpl extends CommonServiceImpl implements ProductSpecPriceServiceI {

	@Override
	public List<ProductSpecPriceEntity> saveSpecPriceByRequest(HttpServletRequest req, ProductEntity p, TSCompany company) throws Exception {
		
		List<ProductSpecPriceEntity> productSpecPriceList = new ArrayList<ProductSpecPriceEntity>();
		List<ProductSpecPriceEntity> addProductSpecPrice = new ArrayList<ProductSpecPriceEntity>();
		List<ProductSpecPriceEntity> updateProductSpecPrice = new ArrayList<ProductSpecPriceEntity>();
		
		if(null != p && null != company){
			Integer exps = ConvertTool.toInt(ResourceUtil.getParameter("specLen"));
			for (int i = 0; i < exps; i++) {
				String psid = req.getParameter("id-" + i);
				String normalPrice = req.getParameter("normalPrice-" + i);
				String orderPrice = req.getParameter("orderPrice-" + i);
				String stockNum = req.getParameter("virtualStockNum-" + i);
				String costPrice = req.getParameter("costPrice-" + i);
				String unitId = req.getParameter("unitID-" + i);
				String specName = req.getParameter("specName-" + i);
				
				ProductUnitEntity unitEntity = findUniqueByProperty(ProductUnitEntity.class, "id", unitId);
				
				if(StringUtil.isEmpty(psid)){
					ProductSpecPriceEntity ps = new ProductSpecPriceEntity();
					ps.setCompanyid(company);
					ps.setNormalprice( true == StringUtil.isEmpty(normalPrice) ? null : (new BigDecimal(normalPrice)));
					ps.setOrderprice( true == StringUtil.isEmpty(orderPrice) ? null : (new BigDecimal(orderPrice)));
					ps.setCostprice(true == StringUtil.isEmpty(costPrice) ? null : (new BigDecimal(costPrice)));
					ps.setStocknum( true == StringUtil.isEmpty(stockNum) ? null : (new BigDecimal(stockNum)));
					ps.setSpecname(specName);
					ps.setProductid(p);
					ps.setUnitid(unitEntity);
					ps.setStatus("U");
					
					addProductSpecPrice.add(ps);
				}else{
					
					ProductSpecPriceEntity psentity = findUniqueByProperty(ProductSpecPriceEntity.class, "id", psid);
					psentity.setCompanyid(company);
					psentity.setNormalprice( true == StringUtil.isEmpty(normalPrice) ? null : (new BigDecimal(normalPrice)));
					psentity.setOrderprice( true == StringUtil.isEmpty(orderPrice) ? null : (new BigDecimal(orderPrice)));
					psentity.setCostprice(true == StringUtil.isEmpty(costPrice) ? null : (new BigDecimal(costPrice)));
					psentity.setStocknum( true == StringUtil.isEmpty(stockNum) ? null : (new BigDecimal(stockNum)));
					psentity.setSpecname(specName);
					psentity.setProductid(p);
					psentity.setUnitid(unitEntity);
					psentity.setStatus("U");
					
					updateProductSpecPrice.add(psentity);
				}
				
				if(null != updateProductSpecPrice && updateProductSpecPrice.size() > 0){
					productSpecPriceList.addAll(updateProductSpecPrice);
				}
				
				if(null != addProductSpecPrice && addProductSpecPrice.size() > 0){
					productSpecPriceList.addAll(addProductSpecPrice);
				}
				
			}
			
		}
		if(null != addProductSpecPrice && addProductSpecPrice.size() > 0){
			batchSave(addProductSpecPrice);
		}
		
		if(null != updateProductSpecPrice && updateProductSpecPrice.size() > 0){
			batchUpdate(updateProductSpecPrice);
		}
		
		return productSpecPriceList;
	}

	@Override
	public boolean delSpecPriceById(String specid) throws Exception {
		String sql = "UPDATE xnc_product_spec_price SET status = 'D' where id = ?";
		if( executeSql(sql, specid) > 0 ){
			
			return true;
		}else{
			
			return false;
		}
	}

	@Override
	public Integer getCountSpecPriceByProductID(String productid) throws Exception {
		
		String sql = "select count(1) from xnc_product_spec_price where productid = ?";
		
		return getCountForJdbcParam(sql, new String[]{productid}).intValue();
	}

	@Override
	public Map<String, Object> getMinSpecPriceByProductID(String productid) throws Exception {
		
		String sql = "select * from xnc_product_spec_price where prodcutid = ? order by orderPrice desc LIMIT 1";
		List<Map<String, Object> > productSpecPriceList = findForJdbc(sql, productid);
		
		if(null != productSpecPriceList && productSpecPriceList.size() > 0){
			return productSpecPriceList.get(0);
		}
		return null;
	}
	
}