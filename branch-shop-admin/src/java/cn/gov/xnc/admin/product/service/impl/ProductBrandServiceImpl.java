package cn.gov.xnc.admin.product.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.service.ProductBrandServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("productBrandService")

public class ProductBrandServiceImpl extends CommonServiceImpl implements ProductBrandServiceI {
	
}