package cn.gov.xnc.admin.verify.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.verify.entity.VerifyOrderLogEntity;
import cn.gov.xnc.admin.verify.service.VerifyOrderService;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.BeanToMapUtils;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("verifyOrderService")
@Transactional
public class VerifyOrderServiceImpl extends CommonServiceImpl implements VerifyOrderService {

	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private SystemService systemService;
	
	/**
	 * 根据条件查询列表
	 * @param params
	 * @return
	 */
	public List<VerifyOrderLogEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(VerifyOrderLogEntity.class);
		for (Entry<String, Object> entry : params.entrySet()) {
			cq.eq(entry.getKey(), entry.getValue());
		}
		
		String startTime = (String) params.get("startTime");
		String endTime = (String) params.get("endTime");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(startTime)&&StringUtil.isNotEmpty(endTime)){
			cq.between("createTime", DateUtils.str2Date(startTime,sdf), DateUtils.str2Date(endTime,sdf));
		}else if( StringUtil.isNotEmpty(startTime) ){
			cq.ge("createTime", DateUtils.str2Date(startTime,sdf));
		}else if( StringUtil.isNotEmpty(endTime) ){
			cq.le("createTime", DateUtils.str2Date(endTime,sdf));
		}
		cq.addOrder("createTime", SortDirection.desc);
		
		cq.add();
		List<VerifyOrderLogEntity> list = this.getListByCriteriaQuery(cq, false);
		return list;
	}

	/**
	 * AJAX 根据条件查询列表并分页
	 * @param params
	 */
	public void findListPageByParams(Map<String, Object> params) {
		DataGrid dataGrid = (DataGrid) params.get("dataGrid");
		CriteriaQuery cq = new CriteriaQuery(VerifyOrderLogEntity.class,dataGrid);
		for (Entry<String, Object> entry : params.entrySet()) {
			cq.eq(entry.getKey(), entry.getValue());
		}
		
		String startTime = (String) params.get("startTime");
		String endTime = (String) params.get("endTime");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(startTime)&&StringUtil.isNotEmpty(endTime)){
			cq.between("createTime", DateUtils.str2Date(startTime,sdf), DateUtils.str2Date(endTime,sdf));
		}else if( StringUtil.isNotEmpty(startTime) ){
			cq.ge("createTime", DateUtils.str2Date(startTime,sdf));
		}else if( StringUtil.isNotEmpty(endTime) ){
			cq.le("createTime", DateUtils.str2Date(endTime,sdf));
		}
		cq.addOrder("createTime", SortDirection.desc);
		
		cq.add();
		this.getDataGridReturn(cq, true);
	}

	/**
	 * 根据对象封装参数
	 * @param component
	 * @return
	 */
	public Map<String, Object> getParams(VerifyOrderLogEntity verifySaleOrderLogEntity) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = BeanToMapUtils.convertBeanToMap(verifySaleOrderLogEntity);
		params.put("company", user.getCompany());
		return params;
	}

	/**
	 * 根据物流单号、订单号，检查是否核销过
	 * @param log
	 * @param 物流单号
	 * @param 订单号
	 * @return
	 */
	public boolean checkVerifyOrderLog(VerifyOrderLogEntity log){
		Map<String,Object> params = this.getParams(log);
		List<VerifyOrderLogEntity> list = this.findListByParams(params);
		return ListUtil.isNotEmpty(list);
	}

	/**
	 * 核单处理
	 * @param request
	 * @return
	 */
	public boolean commitVerifyOrder(List<Map<String, Object>> params) throws Exception{
		TSUser user = ResourceUtil.getSessionUserName();
		
		boolean auth = false;
		
		if(params.size() < 0 && ListUtil.isNotEmpty(params)){
			return auth;
		}
		
		
		for (Map<String, Object> param : params) {
			//未核单的物流单号
			List<String> freightStateMiss =  new ArrayList<String>();
			
			//核单的订单编号
			String verify_orderId = (String) param.get("orderId");
			if(!StringUtil.isNotEmpty(verify_orderId)){
				continue;
			}
			
			//核单的物流单号
			String verify_LogisticsNumber =  (String) param.get("logisticsId");
			if(!StringUtil.isNotEmpty(verify_LogisticsNumber)){
				continue;
			}
			
			//根据订单编号查找订单
			OrderEntity order = systemService.findUniqueByProperty(OrderEntity.class, "id", verify_orderId);
			if(order == null){
				continue;
			}
			
			//订单的物流单号
			String orderLogisticsNumber = order.getLogisticsnumber();
			if(!StringUtil.isNotEmpty(orderLogisticsNumber)){
				continue;
			}
			
			//订单的多个物流单号
			List<String> orderLogisticsNumbers = StringUtil.splitToList(",", orderLogisticsNumber);
			if(!ListUtil.isNotEmpty(orderLogisticsNumbers)){
				continue;
			}
		
			//检查订单中所有的物流单号是否存在已核单的物流单号
			//过滤订单中已核单的物流单号，返回未核单的物流单号
			for (String single : orderLogisticsNumbers) {
				if(!StringUtil.isNotEmpty(single)){
					continue;
				}
				
				//检查核单记录是否存在已核单
				VerifyOrderLogEntity verifyOrderLog = new VerifyOrderLogEntity();
				verifyOrderLog.setLogisticsId(single);
				boolean skip = this.checkVerifyOrderLog(verifyOrderLog);
				if(skip){
					continue;
				}
				
				if(verify_LogisticsNumber.equals(single)){
					//对应核单的物流单号进行核单记录
					VerifyOrderLogEntity log = new VerifyOrderLogEntity();
					log.setCompany(user.getCompany());
					log.setLogisticsId(verify_LogisticsNumber);
					log.setOrder(order);
					log.setOperateUser(user);
					log.setCreateTime(DateUtils.getDate());
					systemService.save(log);
				}else{
					//未核单的物流单号
					freightStateMiss.add(single);
				}
			}
			
			/**
			 * 更新订单物流状态
			 */
			
			//订单商品数量
			Integer number = order.getNumber().intValue();
			//订单物流数量
			Integer orderFreightStateSize = orderLogisticsNumbers.size();
			//未核单物流数量
			Integer missFreightStateSize = freightStateMiss.size();
			//已核单物流数量
			Integer restSize = 0;
			if(orderFreightStateSize > missFreightStateSize){
				restSize = orderFreightStateSize - missFreightStateSize;
				
			}else if(orderFreightStateSize < missFreightStateSize){
				restSize = missFreightStateSize - orderFreightStateSize;
			}
			
			String freightStateValue = "";
			
			if(missFreightStateSize > 0){
				//物流状态 - 部分发货
				freightStateValue = OrderServiceI.FreightState.PART_DELIVER.getValue();
			}else if(missFreightStateSize == 0 && orderFreightStateSize == restSize && restSize == number){
				//物流状态 - 已发货
				freightStateValue = OrderServiceI.FreightState.ALREADY_DELIVER.getValue();
			}
			
			//最后判断已核单数量是否小于商品数量
			if(restSize < number){
				//物流状态 - 部分发货
				freightStateValue = OrderServiceI.FreightState.PART_DELIVER.getValue();
			}
			
			//发货时间
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String deparTureDate = sdf.format(order.getDeparturedate() == null ? new Date() : order.getDeparturedate());
			if(!StringUtil.isNotEmpty(deparTureDate)){
				order.setDeparturedate(new Date());
			}
			
			Map<String, Object> freightParams = new HashMap<String, Object>();
			freightParams.put("dictionaryValue", freightStateValue);
			TSDictionary freightStates = orderService.findFreightState(freightParams);
			
			//记录核单次数
			Integer addVerifyNum = order.getVerifyNum();
			order.setVerifyNum(++addVerifyNum);
			
			//订单处理 - 更新物流状态
			order.setFreightState(freightStates);
			systemService.updateEntitie(order);
			
			auth = true;
		}
		
		return auth;
	}

	/**
	 * 单个订单手动核单 - 检测
	 * @param orderEntity
	 * @param reques
	 * @return
	 */
	public AjaxJson manualSingleVerifyOrder(OrderEntity orderEntity)  throws Exception{
		AjaxJson result = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		//物流状态
		TSDictionary freightState = orderEntity.getFreightState();
		if(freightState == null){
			return null;
		}
		
		boolean checkFireghtState = false;
		    
		if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_PACK.getValue())){
			//待打包
			checkFireghtState = true;
		}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_DELIVER.getValue())){
			//待发货
			checkFireghtState = true;
		}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.PART_DELIVER.getValue())){
			//部分发货
			checkFireghtState = true;
		}
		
		if(!checkFireghtState){
			return null;
		}
		
		//订单的物流单号
		String orderLogisticsNumber = orderEntity.getLogisticsnumber();
		if(!StringUtil.isNotEmpty(orderLogisticsNumber)){
			return null;
		}
		
		//订单的多个物流单号
		List<String> orderLogisticsNumbers = StringUtil.splitToList(",", orderLogisticsNumber);
		if(!ListUtil.isNotEmpty(orderLogisticsNumbers)){
			return null;
		}
	
		//检查订单中所有的物流单号是否存在已核单的物流单号
		//过滤订单中已核单的物流单号，返回未核单的物流单号
		for (String single : orderLogisticsNumbers) {
			
			if(!StringUtil.isNotEmpty(single)){
				continue;
			}
			
			//检查核单记录是否存在已核单
			
			VerifyOrderLogEntity verifyOrderLog = new VerifyOrderLogEntity();
			verifyOrderLog.setLogisticsId(single);
			boolean skip = this.checkVerifyOrderLog(verifyOrderLog);
			if(skip){
				continue;
			}
			
			//物流单号进行核单记录
			VerifyOrderLogEntity log = new VerifyOrderLogEntity();
			log.setCompany(user.getCompany());
			log.setLogisticsId(single);
			log.setOrder(orderEntity);
			log.setOperateUser(user);
			log.setCreateTime(DateUtils.getDate());
			systemService.save(log);
		}
		
		/**
		 * 更新订单物流状态
		 */
		
		//订单商品数量
		Integer number = orderEntity.getNumber().intValue();
		//订单物流数量
		Integer orderFreightStateSize = orderLogisticsNumbers.size();
		
		//默认物流状态 - 已发货
		String freightStateValue = OrderServiceI.FreightState.ALREADY_DELIVER.getValue();
		
		//最后判断已核单数量是否小于商品数量
		if(orderFreightStateSize < number){
			//物流状态 - 部分发货
			freightStateValue = OrderServiceI.FreightState.PART_DELIVER.getValue();
		}
		
		//发货时间
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String deparTureDate = sdf.format(orderEntity.getDeparturedate() == null ? new Date() : orderEntity.getDeparturedate());
		if(!StringUtil.isNotEmpty(deparTureDate)){
			orderEntity.setDeparturedate(new Date());
		}
		
		Map<String, Object> freightParams = new HashMap<String, Object>();
		freightParams.put("dictionaryValue", freightStateValue);
		TSDictionary freightStates = orderService.findFreightState(freightParams);
		
		result.setSuccess(true);
		result.setObj(freightStates);
		return result;
	}

	/**
	 * 根据订单的物流单号检查是否核单
	 * @param orderEntity
	 * @return
	 * @throws Exception
	 */
	public AjaxJson checkVerifyByOrderLogistics(OrderEntity orderEntity) throws Exception {
		AjaxJson result = new AjaxJson();
		//订单的物流单号
		String orderLogisticsNumber = orderEntity.getLogisticsnumber();
		if(!StringUtil.isNotEmpty(orderLogisticsNumber)){
			result.setSuccess(false);
			result.setMsg("订单物流单号为空");
			return result;
		}
		
		//订单的多个物流单号
		List<String> orderLogisticsNumbers = StringUtil.splitToList(",", orderLogisticsNumber);
		if(!ListUtil.isNotEmpty(orderLogisticsNumbers)){
			result.setSuccess(false);
			result.setMsg("订单物流单号为空");
			return result;
		}
	
		//检查订单中所有的物流单号是否存在已核单的物流单号
		//过滤订单中已核单的物流单号，返回未核单的物流单号
		for (String single : orderLogisticsNumbers) {
			
			//检查核单记录是否存在已核单
			VerifyOrderLogEntity verifyOrderLog = new VerifyOrderLogEntity();
			verifyOrderLog.setLogisticsId(single);
			boolean skip = this.checkVerifyOrderLog(verifyOrderLog);
			if(skip){
				result.setSuccess(false);
				result.setMsg("订单号：" + orderEntity.getIdentifieror() + "的物流单号：" + single + "已核单！");
				return result;
			}
		}
		result.setSuccess(true);
		return result;
	}
	
}
