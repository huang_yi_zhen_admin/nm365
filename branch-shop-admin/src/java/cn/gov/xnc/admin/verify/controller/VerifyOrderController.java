package cn.gov.xnc.admin.verify.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.verify.entity.VerifyOrderLogEntity;
import cn.gov.xnc.admin.verify.service.VerifyOrderService;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.JsonUtil;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

/**
 * 订单核单
 * 
 * @author Leiante
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/verifyOrderController")
public class VerifyOrderController extends BaseController {
	private static final Logger logger = Logger.getLogger(VerifyOrderController.class);
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private VerifyOrderService verifyOrderService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private DictionaryService dictionaryService;
	
	private Set<String> payBillsOrderIds;
	
	/**
	 * 扫码核单界面
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "toScanVerifyList")
	public ModelAndView toScanVerifyList(HttpServletRequest request) {
		return new ModelAndView("admin/verify/scanVerifyList");
	}
	
	/**
	 * 手动核单界面
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "toManualVerifyList")
	public ModelAndView toManualVerifyList(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("admin/verify/manualVerifyList");
		
		String orderIds = ResourceUtil.getParameter("ids");
		if(!StringUtil.isNotEmpty(orderIds)){
			return mav;
		}
		
		List<String> orderIdList = StringUtil.splitToList(",", orderIds);
		if(!ListUtil.isNotEmpty(orderIdList)){
			return mav;
		}
		
		List<OrderEntity> orderList = new ArrayList<OrderEntity>();
		
		try {
			for (String orderId : orderIdList) {
				
				OrderEntity orderEntity = systemService.findUniqueByProperty(OrderEntity.class, "id", orderId);
				if(orderEntity == null){
					continue;
				}
				
				//手动核单物流状态的前提条件
				TSDictionary freightState = orderEntity.getFreightState();
				if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.PART_DELIVER.getValue())){
					//部分发货
					orderList.add(orderEntity);
				}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_DELIVER.getValue())){
					//已发货
					orderList.add(orderEntity);
				}else if(freightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_PACK.getValue())){
					//等待打包
					orderList.add(orderEntity);
				}
			}
			
			//物流单号
			request.setAttribute("orderList", orderList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 手动核单 - 搜索订单列表
	 * @param order
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "manualVerifyQuery")
	@ResponseBody
	public AjaxJson manualVerifyQuery(OrderEntity orderEntity,HttpServletRequest request, HttpServletResponse response) {
		StringBuffer message = new StringBuffer();
		AjaxJson result = new AjaxJson();
		boolean auth = false;
		try {
			String identifieror = orderEntity.getIdentifieror();
			if(!StringUtil.isNotEmpty(identifieror)){
				message.append("请录入订单编号！");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
			
			//根据物流单号查找订单详情
			Map<String, Object> params = orderService.getParams(orderEntity);
			List<OrderEntity> list = orderService.findListByParams(params);
			if(!ListUtil.isNotEmpty(list)){
				message.append("该订单号：").append(identifieror).append("不存在");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
			
			JSONArray jsonArray = new JSONArray();
			for (OrderEntity order : list) {
				if(order.getId() == null){
					continue;
				}
				if(order.getIdentifieror() == null){
					continue;
				}
				if(order.getProductid() == null){
					continue;
				}
				if(order.getLogisticsnumber() == null){
					continue;
				}
				if(order.getLogisticscompany() == null){
					continue;
				}
				if(order.getFreightState() == null){
					continue;
				}
				if(order.getNumber() == null){
					continue;
				}
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", order.getId());
				jsonObject.put("identifieror", order.getIdentifieror());
				jsonObject.put("productName", order.getProductid().getName());
				jsonObject.put("logisticsnumber", order.getLogisticsnumber());
				jsonObject.put("logisticscompany", order.getLogisticscompany());
				jsonObject.put("freightState_id", order.getFreightState().getId());
				jsonObject.put("freightState_dictionaryName", order.getFreightState().getDictionaryName());
				jsonObject.put("number", order.getNumber());
				jsonArray.put(jsonObject);
			}
			
			auth = true;
			result.setObj(jsonArray.toString());
			result.setSuccess(auth);
		} catch (Exception e) {
			e.printStackTrace();
			message.append("手动核单，搜索功能，出现系统异常，请联系管理");
			result.setMsg(message.toString());
			result.setSuccess(false);
		}
		return result;
	}
	
	
	/**
	 * 扫码核单 - 搜索订单列表
	 * @param order
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "scanVerifyQuery")
	@ResponseBody
	public AjaxJson scanVerifyQuery(OrderEntity orderEntity,HttpServletRequest request, HttpServletResponse response) {
		StringBuffer message = new StringBuffer();
		AjaxJson result = new AjaxJson();
		boolean auth = false;
		try{
			//物流单号
			String logisticsNumber = orderEntity.getLogisticsnumber();
			if(!StringUtil.isNotEmpty(logisticsNumber)){
				message.append("请录入物流单号！");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
			
			//检查"核销单记录表"是否核销过物流单号
			VerifyOrderLogEntity verifyOrderLogEntity = new VerifyOrderLogEntity();
			verifyOrderLogEntity.setLogisticsId(logisticsNumber);
			boolean checkVerify = verifyOrderService.checkVerifyOrderLog(verifyOrderLogEntity);
			if(checkVerify){
				message.append("该物流单号：").append(logisticsNumber).append("已经核销");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
				
			//根据物流单号查找订单详情
			Map<String, Object> params = orderService.getParams(orderEntity);
			List<OrderEntity> list = orderService.findListByParams(params);
			if(!ListUtil.isNotEmpty(list)){
				message.append("该物流单号：").append(logisticsNumber).append("查找订单详情不存在");
				result.setMsg(message.toString());
				result.setSuccess(auth);
				return result;
			}
			
			JSONArray jsonArray = new JSONArray();
			for (OrderEntity order : list) {
				if(order.getId() == null){
					continue;
				}
				if(order.getIdentifieror() == null){
					continue;
				}
				if(order.getProductid() == null){
					continue;
				}
				if(order.getLogisticsnumber() == null){
					continue;
				}
				if(order.getLogisticscompany() == null){
					continue;
				}
				if(order.getFreightState() == null){
					continue;
				}
				if(order.getNumber() == null){
					continue;
				}
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", order.getId());
				jsonObject.put("identifieror", order.getIdentifieror());
				jsonObject.put("productName", order.getProductid().getName());
				jsonObject.put("logisticsnumber", order.getLogisticsnumber());
				jsonObject.put("logisticscompany", order.getLogisticscompany());
				jsonObject.put("freightState_id", order.getFreightState().getId());
				jsonObject.put("freightState_dictionaryName", order.getFreightState().getDictionaryName());
				jsonObject.put("number", order.getNumber());
				jsonArray.put(jsonObject);
			}
			
			auth = true;
			result.setObj(jsonArray.toString());
			result.setSuccess(auth);
			
		}catch(Exception e){
			e.printStackTrace();
			message.append("扫码核单，搜索功能，出现系统异常，请联系管理");
			result.setMsg(message.toString());
			result.setSuccess(false);
			return result;
		}
		
		return result;
	}
	
	/**
	 * 核单 - 提交
	 * @param companyStockOrder
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "submit")
	@ResponseBody
	public AjaxJson submit(HttpServletRequest request) {
		payBillsOrderIds = new HashSet<String>();
		AjaxJson result = new AjaxJson();
		boolean auth = false;
		
		try{
			//构建核单信息
			AjaxJson paramResult = this.buildVerifyOrderLogParams(request);
			if(!paramResult.isSuccess()){
				result.setMsg(paramResult.getMsg());
				result.setSuccess(auth);
				return result;
			}
			
			List<Map<String,Object>> params = (List<Map<String, Object>>) paramResult.getObj();
			
			//提交核单处理，对商品订单物流状态进行处理
			auth = verifyOrderService.commitVerifyOrder(params);
			if(!auth){
				result.setMsg("商品订单物流状态进行处理出现异常，请联系管理员");
				result.setSuccess(auth);
				return result;
			}
			
			//更新商品订单对应的支付订单物流状态
			for (Iterator<String> it = payBillsOrderIds.iterator(); it.hasNext();) {
				String payBillsOrderId = it.next().toString();
				if(!StringUtil.isNotEmpty(payBillsOrderId)){
					continue;
				}
				
				PayBillsOrderEntity  payBillsOrderEntity = systemService.findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrderId);
				if(payBillsOrderEntity == null){
					continue;
				}
				
				TSDictionary freightState = payBillsOrderService.getPayBillsFreightStatus(payBillsOrderEntity);
				
				payBillsOrderEntity.setFreightState(freightState);
				systemService.updateEntitie(payBillsOrderEntity);
			}
			
			result.setSuccess(auth);
			result.setMsg("提交完成");
		}catch(Exception e){
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg("核单提交失败，系统出现异常，请联系管理员");
			return result;
		}
		return result;
	}
	
	/**
	 * 构建核单信息
	 * @param request
	 * @return
	 */
	private AjaxJson buildVerifyOrderLogParams(HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		
		List<Map<String, Object>> lists = new ArrayList<Map<String,Object>>();
		
		//核单数量
		Integer rowDataSize = ConvertTool.toInt(ResourceUtil.getParameter("rowDataSize"));
		
		for (int i = 0; i < rowDataSize; i++) {
			//物流单号
			String logisticsNumber = request.getParameter("logisticsnumber-" + i);
			if(!StringUtil.isNotEmpty(logisticsNumber)){
				result.setSuccess(false);
				result.setMsg("物流单号：" + logisticsNumber + "信息不完整");
				return result;
			}
			
			//多个物流单号
			List<String> logisticsNumbers = StringUtil.splitToList(",", logisticsNumber);
			if(!ListUtil.isNotEmpty(logisticsNumbers)){
				result.setSuccess(false);
				result.setMsg("物流单号：" + logisticsNumber + "信息不完整");
				return result;
			}
			
			//订单编号
			String id = request.getParameter("id-" + i);
			if(!StringUtil.isNotEmpty(id)){
				result.setSuccess(false);
				result.setMsg("订单编号：" + id + "信息不完整");
				return result;
			}
			
			OrderEntity order = systemService.findUniqueByProperty(OrderEntity.class, "id",id);
			if(order == null){
				result.setSuccess(false);
				result.setMsg("订单号：" + id + "信息不完整");
				return result;
			}
			
			PayBillsOrderEntity payBillsOrderEntity = order.getBillsorderid();
			if(payBillsOrderEntity == null){
				result.setSuccess(false);
				result.setMsg("订单号：" + id + "的支付订单信息不完整");
				return result; 
			}
			payBillsOrderIds.add(payBillsOrderEntity.getId());
			
			//订单号
			String identifieror = request.getParameter("identifieror-" + i);
			if(!StringUtil.isNotEmpty(identifieror)){
				result.setSuccess(false);
				result.setMsg("订单号：" + identifieror + "信息不完整");
				return result;
			}
			
			for (String single : logisticsNumbers) {
				//物流单号
				if(!StringUtil.isNotEmpty(single)){
					continue;
				}
				
				//检查"核销单记录表"是否核销过物流单号
				VerifyOrderLogEntity verifyOrderLogEntity = new VerifyOrderLogEntity();
				verifyOrderLogEntity.setLogisticsId(single);
				boolean skip = verifyOrderService.checkVerifyOrderLog(verifyOrderLogEntity);
				if(skip){
					result.setSuccess(false);
					result.setMsg("订单编号：" + identifieror + "的物流单号" + single + "已核单过！");
					return result;
				}
				
				Map<String, Object> params = new HashMap<String,Object>();
				params.put("orderId", id);
				params.put("identifieror", identifieror);
				params.put("logisticsId", single);
				
				lists.add(params);
			}
		}
		
		if(lists.size() <= 0){
			result.setSuccess(false);
			result.setMsg("请添加订单信息");
			return result;
		}
		
		result.setSuccess(true);
		result.setObj(lists);
		return result;
	}
}
