package cn.gov.xnc.admin.weixin.mp.utils.josn;


import cn.gov.xnc.admin.weixin.mp.entity.common.JsapiTicket;
import cn.gov.xnc.admin.weixin.mp.entity.common.WxAccessToken;
import cn.gov.xnc.admin.weixin.mp.entity.result.WxMpOAuth2AccessToken;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WxMpGsonBuilder {
	
	public static final GsonBuilder INSTANCE = new GsonBuilder();

	  static {
	    INSTANCE.disableHtmlEscaping();
	    
	    INSTANCE.registerTypeAdapter(WxMpOAuth2AccessToken.class, new WxMpOAuth2AccessTokenAdapter());//OAuth2
	    INSTANCE.registerTypeAdapter(WxAccessToken.class, new WxAccessTokenAdapter());//AccessToken
	    INSTANCE.registerTypeAdapter(JsapiTicket.class, new JsapiTicketAdapter());//jsapi_ticket
	  }

	  public static Gson create() {
	    return INSTANCE.create();
	  }

	
	
}	
