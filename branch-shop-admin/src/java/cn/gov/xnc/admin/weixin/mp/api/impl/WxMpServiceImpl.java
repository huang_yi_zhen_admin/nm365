package cn.gov.xnc.admin.weixin.mp.api.impl;



import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.weixin.mp.api.WxMpService;
import cn.gov.xnc.admin.weixin.mp.entity.common.JsapiTicket;
import cn.gov.xnc.admin.weixin.mp.entity.common.WxAccessToken;
import cn.gov.xnc.admin.weixin.mp.entity.result.WxMpOAuth2AccessToken;
import cn.gov.xnc.system.core.util.HttpGetPost;
import cn.gov.xnc.system.core.util.StringUtil;

import com.google.gson.JsonParser;






@Service("wxMpService")
public class WxMpServiceImpl implements WxMpService {

	  private static final JsonParser JSON_PARSER = new JsonParser();
	  

	@Override
	public WxMpOAuth2AccessToken oauth2getAccessToken(String code , String appId , String secret) throws Exception {
		 StringBuilder url = new StringBuilder();
		    url.append("https://api.weixin.qq.com/sns/oauth2/access_token?");
		    url.append("appid=").append(appId);
		    url.append("&secret=").append(secret);
		    url.append("&code=").append(code);
		    url.append("&grant_type=authorization_code");

		    return this.getOAuth2AccessToken(url);
	}


	private WxMpOAuth2AccessToken getOAuth2AccessToken(StringBuilder url) throws Exception {
		String   result =HttpGetPost.sendGet(url.toString());
	    return WxMpOAuth2AccessToken.fromJson(result);
	}

	
	@Override
	public String getAccessToken( String  appId ,String  secret ) throws Exception {
		return getAccessToken(false, appId, secret);
	}


	@Override
	public String getAccessToken(boolean forceRefresh  ,String  appId ,String  secret ) throws Exception {
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential" +"&appid=" + appId + "&secret="+ secret;
		
		String accessToken = "";
		String   json =HttpGetPost.sendGet(url.toString());
		WxAccessToken  wxAccessToken =  WxAccessToken.fromJson(json);
		if( wxAccessToken != null && StringUtil.isNotEmpty( wxAccessToken.getAccessToken() )){
			accessToken = wxAccessToken.getAccessToken();
		}

		return accessToken;
	}


	@Override
	public String getJsapiTicket(String accessToken) throws Exception {
		// TODO Auto-generated method stub
		return getJsapiTicket( false ,  accessToken);
	}


	@Override
	public String getJsapiTicket(boolean forceRefresh, String accessToken)throws Exception {
		
		String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+accessToken+"&type=jsapi";
		
		String jsapi = "";
		String   json =HttpGetPost.sendGet(url.toString());
		JsapiTicket  ticket =  JsapiTicket.fromJson(json);
		if( ticket != null && StringUtil.isNotEmpty( ticket.getTicket() )){
			jsapi = ticket.getTicket();
		}
		
		
		return jsapi ;
	}

	  
	  
	  

}
