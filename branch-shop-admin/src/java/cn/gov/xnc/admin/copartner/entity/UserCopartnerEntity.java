package cn.gov.xnc.admin.copartner.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 商业合作伙伴信息表
 * @author zero
 * @date 2017-01-12 15:06:26
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_user_copartner", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class UserCopartnerEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**合作伙伴所属公司单位或者是经营体名称*/
	private java.lang.String partnercompany;
	/**合作联系人*/
	private java.lang.String partnercontactman;
	/**合作联系人电话*/
	private java.lang.String partnercontactphone;
	/**合作人经营体地址或者是注册地址*/
	private java.lang.String partnercompanyaddress;
	/**结算方式 1 alipay(支付宝) 2 wxpay(微信支付) 3 unionpay(银联转账) 4 cash(现金)*/
	private java.lang.String withdrawtype;
	/**结算时间点:
	 * 1 月初——每月1日 
	 * 2 月末——每月最后一天*/
	private java.lang.String withdrawpiont;
	/**合作人结算帐号*/
	private java.lang.String partneraccount;
	/**合作人结算帐号对应的户名*/
	private java.lang.String partneraccountname;
	/**合作人经营范围或者产品*/
	private java.lang.String partnerbusiness;
	/**合作人经营账户开户行*/
	private java.lang.String partneraccountdes;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作伙伴所属公司单位或者是经营体名称
	 */
	@Column(name ="PARTNERCOMPANY",nullable=false,length=1000)
	public java.lang.String getPartnercompany(){
		return this.partnercompany;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作伙伴所属公司单位或者是经营体名称
	 */
	public void setPartnercompany(java.lang.String partnercompany){
		this.partnercompany = partnercompany;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作联系人
	 */
	@Column(name ="PARTNERCONTACTMAN",nullable=false,length=500)
	public java.lang.String getPartnercontactman(){
		return this.partnercontactman;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作联系人
	 */
	public void setPartnercontactman(java.lang.String partnercontactman){
		this.partnercontactman = partnercontactman;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作联系人电话
	 */
	@Column(name ="PARTNERCONTACTPHONE",nullable=false,length=15)
	public java.lang.String getPartnercontactphone(){
		return this.partnercontactphone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作联系人电话
	 */
	public void setPartnercontactphone(java.lang.String partnercontactphone){
		this.partnercontactphone = partnercontactphone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作人经营体地址或者是注册地址
	 */
	@Column(name ="PARTNERCOMPANYADDRESS",nullable=true,length=1000)
	public java.lang.String getPartnercompanyaddress(){
		return this.partnercompanyaddress;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作人经营体地址或者是注册地址
	 */
	public void setPartnercompanyaddress(java.lang.String partnercompanyaddress){
		this.partnercompanyaddress = partnercompanyaddress;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算方式 1 alipay(支付宝) 2 wxpay(微信支付) 3 unionpay(银联转账) 4 cash(现金)
	 */
	@Column(name ="WITHDRAWTYPE",nullable=false,length=10)
	public java.lang.String getWithdrawtype(){
		return this.withdrawtype;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结算方式 1 alipay(支付宝) 2 wxpay(微信支付) 3 unionpay(银联转账) 4 cash(现金)
	 */
	public void setWithdrawtype(java.lang.String withdrawtype){
		this.withdrawtype = withdrawtype;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算时间点 1 月初(每月1日) 2 月末(每月最后一天)
	 */
	@Column(name ="WITHDRAWPIONT",nullable=false,length=2)
	public java.lang.String getWithdrawpiont(){
		return this.withdrawpiont;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结算时间点 1 月初(每月1日) 2 月末(每月最后一天)
	 */
	public void setWithdrawpiont(java.lang.String withdrawpiont){
		this.withdrawpiont = withdrawpiont;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作人结算帐号
	 */
	@Column(name ="PARTNERACCOUNT",nullable=false,length=32)
	public java.lang.String getPartneraccount(){
		return this.partneraccount;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作人结算帐号
	 */
	public void setPartneraccount(java.lang.String partneraccount){
		this.partneraccount = partneraccount;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作人结算帐号对应的户名
	 */
	@Column(name ="PARTNERACCOUNTNAME",nullable=true,length=500)
	public java.lang.String getPartneraccountname(){
		return this.partneraccountname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作人结算帐号对应的户名
	 */
	public void setPartneraccountname(java.lang.String partneraccountname){
		this.partneraccountname = partneraccountname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作人经营范围或者产品
	 */
	@Column(name ="PARTNERBUSINESS",nullable=true,length=1000)
	public java.lang.String getPartnerbusiness(){
		return this.partnerbusiness;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作人经营范围或者产品
	 */
	public void setPartnerbusiness(java.lang.String partnerbusiness){
		this.partnerbusiness = partnerbusiness;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作人经营账户开户行
	 */
	@Column(name ="PARTNERACCOUNTDES",nullable=true,length=1000)
	public java.lang.String getPartneraccountdes() {
		return partneraccountdes;
	}

	public void setPartneraccountdes(java.lang.String partneraccountdes) {
		this.partneraccountdes = partneraccountdes;
	}
}
