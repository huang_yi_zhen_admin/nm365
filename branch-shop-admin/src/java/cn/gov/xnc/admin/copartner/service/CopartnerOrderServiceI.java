package cn.gov.xnc.admin.copartner.service;

import java.util.List;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerAccountEntity;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface CopartnerOrderServiceI extends CommonService{
	
	public CopartnerOrderEntity buildCopartnerOrder(OrderEntity order);
	
	/**
	 * @Description: 根据合作伙伴用户和时间，获取合作伙伴的订单
	 * @param userCopartner
	 * @param endDateTime
	 * @return
	 */
	public List<CopartnerOrderEntity> getCopartnerOrdersByUser(TSUser userCopartner, String endDateTime);
	
	/**
	 * @Description: 结算汇总之后更新合作伙伴的订单状态
	 * @param userCopartner
	 * @param endDateTime
	 * @return
	 */
	public int updateCopartnerOrderAfterDrawCal(TSUser userCopartner, String endDateTime) throws Exception;
	
	/**
	 * @Description: 申请结算之后更新合作伙伴的订单和结算申请单关联
	 * @param userCopartner
	 * @param endDateTime
	 * @return
	 */
	public int updateCopartnerOrder2withdraw(TSUser userCopartner, String withdrawId) throws Exception;
	
	
	/**
	 * 根据订单号获取处理订单的发货商信息
	 * @param orderid
	 * @return
	 */
	public TSUser getOrderCopartnerByOrderId(String orderid) throws Exception;
}
