package cn.gov.xnc.admin.copartner.service.impl;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerOrderServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("copartnerOrderService")
@Transactional
public class CopartnerOrderServiceImpl extends CommonServiceImpl implements CopartnerOrderServiceI {

	@Override
	public CopartnerOrderEntity buildCopartnerOrder(OrderEntity order) {
		
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class);
		cq.eq("orderid", order);
		cq.eq("status", "1");//选择处在派单环节的订单
		cq.add();
		
		List<OrderSendEntity> OrderSendList = this.getListByCriteriaQuery(cq, false);
		if(null != OrderSendList && OrderSendList.size() > 0){
			OrderSendEntity orderSend =  OrderSendList.get(0);
			CopartnerOrderEntity copartnerOrder = new CopartnerOrderEntity();
			copartnerOrder.setId(IdWorker.generateSequenceNo());
			copartnerOrder.setcopartnerid(orderSend.getReceiver());
			copartnerOrder.setOrderid(order);
			copartnerOrder.setWithdraworderid(null);
			copartnerOrder.setWithdrawstatus("1");
			
			return copartnerOrder;
		}else{
			
			return null;
		}
	}

	@Override
	public List<CopartnerOrderEntity> getCopartnerOrdersByUser(TSUser userCopartner, String endDateTime) {
		//时间
		SimpleDateFormat dateformate = new SimpleDateFormat ("yyyy-MM-dd"); 
		CriteriaQuery cq = new CriteriaQuery(CopartnerOrderEntity.class);
		cq.eq("usercopartnerid", userCopartner);
		cq.eq("withdrawstatus", "1");//选择处在派单环节的订单
		cq.le("orderid.createdate", DateUtils.str2Date(endDateTime,dateformate));
		cq.add();
		
		List<CopartnerOrderEntity> CopartnerOrderList = this.getListByCriteriaQuery(cq, false);
		return CopartnerOrderList;
	}

	public int updateCopartnerOrderAfterDrawCal(TSUser userCopartner, String endDateTime) throws Exception {
		String Sql =" UPDATE xnc_copartner_order "
				+ " SET xnc_copartner_order.withdrawStatus = '2' "
				+ " WHERE  EXISTS( select 1 from xnc_order O where O.id = xnc_copartner_order.orderid and O.createDate <= '"+ endDateTime + "') "
				+ " and xnc_copartner_order.withdrawStatus = '1' " 
				+ " and xnc_copartner_order.userCopartnerId = '" +  userCopartner.getId() + "'";
		
		return updateBySqlString(Sql);
	}

	public int updateCopartnerOrder2withdraw(TSUser userCopartner,String withdrawId) throws Exception {
		
		String Sql =" UPDATE xnc_copartner_order"
				+ " SET xnc_copartner_order.withdrawOrderId = '" + withdrawId + "'"
				+ " WHERE xnc_copartner_order.withdrawStatus = '2' " 
				+ " and xnc_copartner_order.userCopartnerId = '" +  userCopartner.getId() + "'";
		
		return updateBySqlString(Sql);
	}

	@Override
	public TSUser getOrderCopartnerByOrderId(String orderid) throws Exception {
		TSUser copartner = null;
		//时间
		CriteriaQuery cq = new CriteriaQuery(CopartnerOrderEntity.class);
		cq.eq("orderid.id", orderid);
		cq.add();
		
		List<CopartnerOrderEntity> copartnerOrderList = this.getListByCriteriaQuery(cq, false);
		if(null != copartnerOrderList && copartnerOrderList.size() > 0){
			copartner = copartnerOrderList.get(0).getcopartnerid();
		}
		return copartner;
	}

}