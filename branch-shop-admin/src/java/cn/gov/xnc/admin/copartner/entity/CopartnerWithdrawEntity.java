package cn.gov.xnc.admin.copartner.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 合作伙伴相关订单
 * @author zero
 * @date 2017-01-12 15:12:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_copartner_withdraw_order", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CopartnerWithdrawEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**申请结算人，即合作伙伴用户唯一编码*/
	private TSUser usercopartnerid;
	/**结算对象*/
	private TSCompany companyid;
	/**申请结算金额或者是结算金额*/
	private BigDecimal waittodrawmoney;
	/**实际结算金额*/
	private BigDecimal actualdrawmoney;
	/**结算凭据，通常是结算的单据截图*/
	private java.lang.String paymentevidence;
	/**1 结算审核不通过 2 申请结算审核 3 结算审核通过*/
	private java.lang.String status;
	/**申请结算时间*/
	private java.util.Date createdate;
	/**实际结算时间*/
	private java.util.Date updatetime;
	/**结算备注*/
	private java.lang.String remarks;
	/**审核人*/
	private TSUser checkuser;
	/**结算帐号*/
	private java.lang.String copartneraccount;
	/**结算账户名*/
	private java.lang.String copartneraccountname;
	
	
	
	/**合作伙伴相关订单*/
	private List<CopartnerOrderEntity> copartnerOrderList = new ArrayList<CopartnerOrderEntity>();
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  申请结算人，即合作伙伴唯一编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERCOPARTNERID")
	public TSUser getUsercopartnerid(){
		return this.usercopartnerid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  申请结算人，即合作伙伴唯一编码
	 */
	public void setUsercopartnerid(TSUser copartnerid){
		this.usercopartnerid = copartnerid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算对象
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结算对象
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  申请结算金额或者是结算金额
	 */
	@Column(name ="WAITTODRAWMONEY",nullable=false,precision=10,scale=2)
	public BigDecimal getWaittodrawmoney(){
		return this.waittodrawmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  申请结算金额或者是结算金额
	 */
	public void setWaittodrawmoney(BigDecimal waittodrawmoney){
		this.waittodrawmoney = waittodrawmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  实际结算金额
	 */
	@Column(name ="ACTUALDRAWMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getActualdrawmoney(){
		return this.actualdrawmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  实际结算金额
	 */
	public void setActualdrawmoney(BigDecimal actualdrawmoney){
		this.actualdrawmoney = actualdrawmoney;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算凭据，通常是结算的单据截图
	 */
	@Column(name ="PAYMENTEVIDENCE",nullable=true,length=1000)
	public java.lang.String getPaymentevidence(){
		return this.paymentevidence;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结算凭据，通常是结算的单据截图
	 */
	public void setPaymentevidence(java.lang.String paymentevidence){
		this.paymentevidence = paymentevidence;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  1 申请状态 2 结算完结
	 */
	@Column(name ="STATUS",nullable=false,length=2)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  1 申请状态 2 结算完结
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  申请结算时间
	 */
	@Column(name ="CREATEDATE",nullable=false)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  申请结算时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  实际结算时间
	 */
	@Column(name ="UPDATETIME",nullable=true)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  实际结算时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算备注
	 */
	@Column(name ="REMARKS",nullable=true,length=4000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结算备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}

	/**
	 * @return 订单明细list
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "withdraworderid")
	public List<CopartnerOrderEntity> getCopartnerOrderList() {
		return copartnerOrderList;
	}

	public void setCopartnerOrderList(List<CopartnerOrderEntity> copartnerOrderList) {
		this.copartnerOrderList = copartnerOrderList;
	}

	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHECKUSER")
	public TSUser getCheckuser() {
		return checkuser;
	}

	public void setCheckuser(TSUser checkuser) {
		this.checkuser = checkuser;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算帐号
	 */
	@Column(name ="COPARTNERACCOUNT",nullable=true,length=50)
	public java.lang.String getCopartneraccount() {
		return copartneraccount;
	}

	public void setCopartneraccount(java.lang.String copartneraccount) {
		this.copartneraccount = copartneraccount;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算账户名
	 */
	@Column(name ="COPARTNERACCOUNTNAME",nullable=true,length=50)
	public java.lang.String getCopartneraccountname() {
		return copartneraccountname;
	}

	public void setCopartneraccountname(java.lang.String copartneraccountname) {
		this.copartneraccountname = copartneraccountname;
	}
}
