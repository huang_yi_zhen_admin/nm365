package cn.gov.xnc.admin.thirdorder.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.thirdorder.entity.ThirdorderConfigureEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderImportEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderOrderEntity;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderImportServiceI;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderOrderServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("thirdorderImportService")
@Transactional(rollbackForClassName={"Exception"})
public class ThirdorderImportServiceImpl extends CommonServiceImpl implements ThirdorderImportServiceI {
	
	@Autowired
	private ThirdorderOrderServiceI thirdorderOrderService;
	
	public AjaxJson batchDel(String ids){
		AjaxJson j = new AjaxJson();
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(ThirdorderImportEntity.class);
		String[] idlist = ids.split(",");
		cq.eq("company", user.getCompany());
		cq.in("id", idlist);
		List<ThirdorderImportEntity> importlist = getListByCriteriaQuery(cq, false);
		if( null == importlist || importlist.size() <= 0 ){
			j.setSuccess(false);
			j.setMsg("输入参数有误，请稍后重试");
			return j;
		}
		
		CriteriaQuery cb = new CriteriaQuery(ThirdorderOrderEntity.class);
		cb.eq("company", user.getCompany());
		cb.in("importid", importlist.toArray());
		cb.add();
		List<ThirdorderOrderEntity> orderlist = thirdorderOrderService.getListByCriteriaQuery(cb, false);
		if( null == orderlist || orderlist.size() <= 0 ){
			j.setSuccess(false);
			j.setMsg("输入参数有误，请稍后重试");
			return j;
		}
		
		for( ThirdorderImportEntity imEntity : importlist ){
			imEntity.setStatus(ThirdorderImportEntity.STATUS_DEL);
		}
		
		for( ThirdorderOrderEntity order : orderlist){
			order.setStatus(ThirdorderOrderEntity.STATUS_DEL);
		}
		
		batchUpdate(importlist);
		batchUpdate(orderlist);
		
		return j;
	}
	
}