package cn.gov.xnc.admin.thirdorder.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 第三方订单导入记录
 * @author zero
 * @date 2017-07-21 17:47:42
 * @version V1.0   
 *
 */
@Entity
@Table(name = "thirdorder_import", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ThirdorderImportEntity implements java.io.Serializable {
	
	public static final int STATUS_NORMAL = 0;
	public static final int STATUS_DEL = 1;
	
	/**id*/
	private java.lang.String id;
	/**公司*/
	private TSCompany company;
	/**文件名*/
	private java.lang.String filename;
	/**文件存储服务器全路径*/
	private java.lang.String fullpath;
	/**渠道id*/
	private ThirdorderChannelEntity channelid;
	/**创建时间*/
	private java.util.Date createtime;
	/**创建者*/
	private TSUser createuser;

	/**状态值，0表示正常状态，1表示已删除*/
	private int status;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
//	@GeneratedValue(generator = "paymentableGenerator")
//	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  文件名
	 */
	@Column(name ="FILENAME",nullable=true,length=512)
	public java.lang.String getFilename(){
		return this.filename;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  文件名
	 */
	public void setFilename(java.lang.String filename){
		this.filename = filename;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  文件存储服务器全路径
	 */
	@Column(name ="FULLPATH",nullable=true,length=1024)
	public java.lang.String getFullpath(){
		return this.fullpath;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  文件存储服务器全路径
	 */
	public void setFullpath(java.lang.String fullpath){
		this.fullpath = fullpath;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  渠道id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNELID")
	public ThirdorderChannelEntity getChannelid(){
		return this.channelid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  渠道id
	 */
	public void setChannelid(ThirdorderChannelEntity channelid){
		this.channelid = channelid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=true)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建者
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATEUSER")
	public TSUser getCreateuser(){
		return this.createuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建者
	 */
	public void setCreateuser(TSUser createuser){
		this.createuser = createuser;
	}
	
	/**
	 * @return the company
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}



	/**
	 * @return the status
	 */
	@Column(name ="STATUS",nullable=true,length=512)
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
}
