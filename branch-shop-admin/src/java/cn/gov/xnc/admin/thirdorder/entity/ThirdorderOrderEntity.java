package cn.gov.xnc.admin.thirdorder.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 第三方订单
 * @author zero
 * @date 2017-07-21 17:48:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "thirdorder_order", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ThirdorderOrderEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**公司*/
	private TSCompany company;
	/**渠道id*/
	private ThirdorderChannelEntity channelid;
	/**导入记录表id*/
	private ThirdorderImportEntity importid;
	/**导入的订单id*/
	private java.lang.String ogOrderid;
	/**导入的产品编码*/
	private java.lang.String ogProductcode;
	/**导入的产品名称*/
	private java.lang.String ogProductname;
	/**导入的产品数量*/
	private BigDecimal ogProductnum;
	/**导入的订单总价格*/
	private BigDecimal ogOrderprice;
	/**导入的下单时间*/
	private java.lang.String ogOrdertime;
	/**导入的订单支付时间*/
	private java.lang.String ogOrderpaytime;
	/**导入的购买人*/
	private java.lang.String ogBuyer;
	/**导入的收货地址*/
	private java.lang.String ogBuyeraddress;
	/**导入的购买人电话*/
	private java.lang.String ogBuyermobile;
	/**导入的运费*/
	private BigDecimal ogFreight;
	/**导入的物流公司*/
	private java.lang.String ogFreightcompany;
	/**导入的快递编码*/
	private java.lang.String ogFreightcode;
	/**导入的订单状态*/
	private java.lang.String ogStatus;
	/**本系统物流公司编码*/
	private java.lang.String shippercode;
	/**电子面单号码*/
	private java.lang.String shipernum;
	/**创建时间*/
	private java.util.Date createtime;
	/**更新时间*/
	private java.util.Date updatetime;
	/**状态 1导入，2快递下单，3已揽收，4签收，5已删除*/
	private java.lang.Integer status;
	
	public static final int STATUS_IMPORT = 1;
	public static final int STATUS_EXPRESS = 2;
	public static final int STATUS_EXPRESS_SEND = 3;
	public static final int STATUS_EXPRESS_RECV = 4;
	public static final int STATUS_DEL = 5;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
//	@GeneratedValue(generator = "paymentableGenerator")
//	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  渠道id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNELID")
	public ThirdorderChannelEntity getChannelid(){
		return this.channelid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  渠道id
	 */
	public void setChannelid(ThirdorderChannelEntity channelid){
		this.channelid = channelid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入记录表id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IMPORTID")
	public ThirdorderImportEntity getImportid(){
		return this.importid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入记录表id
	 */
	public void setImportid(ThirdorderImportEntity importid){
		this.importid = importid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的订单id
	 */
	@Column(name ="OG_ORDERID",nullable=true,length=255)
	public java.lang.String getOgOrderid(){
		return this.ogOrderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的订单id
	 */
	public void setOgOrderid(java.lang.String ogOrderid){
		this.ogOrderid = ogOrderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的产品编码
	 */
	@Column(name ="OG_PRODUCTCODE",nullable=true,length=255)
	public java.lang.String getOgProductcode(){
		return this.ogProductcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的产品编码
	 */
	public void setOgProductcode(java.lang.String ogProductcode){
		this.ogProductcode = ogProductcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的产品名称
	 */
	@Column(name ="OG_PRODUCTNAME",nullable=true,length=255)
	public java.lang.String getOgProductname(){
		return this.ogProductname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的产品名称
	 */
	public void setOgProductname(java.lang.String ogProductname){
		this.ogProductname = ogProductname;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  导入的产品数量
	 */
	@Column(name ="OG_PRODUCTNUM",nullable=true,precision=15,scale=4)
	public BigDecimal getOgProductnum(){
		return this.ogProductnum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  导入的产品数量
	 */
	public void setOgProductnum(BigDecimal ogProductnum){
		this.ogProductnum = ogProductnum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  导入的订单总价格
	 */
	@Column(name ="OG_ORDERPRICE",nullable=true,precision=15,scale=4)
	public BigDecimal getOgOrderprice(){
		return this.ogOrderprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  导入的订单总价格
	 */
	public void setOgOrderprice(BigDecimal ogOrderprice){
		this.ogOrderprice = ogOrderprice;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  导入的下单时间
	 */
	@Column(name ="OG_ORDERTIME",nullable=true,length=32)
	public java.lang.String getOgOrdertime(){
		return this.ogOrdertime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  导入的下单时间
	 */
	public void setOgOrdertime(java.lang.String ogOrdertime){
		this.ogOrdertime = ogOrdertime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  导入的订单支付时间
	 */
	@Column(name ="OG_ORDERPAYTIME",nullable=true,length=32)
	public java.lang.String getOgOrderpaytime(){
		return this.ogOrderpaytime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  导入的订单支付时间
	 */
	public void setOgOrderpaytime(java.lang.String ogOrderpaytime){
		this.ogOrderpaytime = ogOrderpaytime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的购买人
	 */
	@Column(name ="OG_BUYER",nullable=true,length=255)
	public java.lang.String getOgBuyer(){
		return this.ogBuyer;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的购买人
	 */
	public void setOgBuyer(java.lang.String ogBuyer){
		this.ogBuyer = ogBuyer;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的收货地址
	 */
	@Column(name ="OG_BUYERADDRESS",nullable=true,length=1024)
	public java.lang.String getOgBuyeraddress(){
		return this.ogBuyeraddress;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的收货地址
	 */
	public void setOgBuyeraddress(java.lang.String ogBuyeraddress){
		this.ogBuyeraddress = ogBuyeraddress;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的购买人电话
	 */
	@Column(name ="OG_BUYERMOBILE",nullable=true,length=32)
	public java.lang.String getOgBuyermobile(){
		return this.ogBuyermobile;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的购买人电话
	 */
	public void setOgBuyermobile(java.lang.String ogBuyermobile){
		this.ogBuyermobile = ogBuyermobile;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  导入的运费
	 */
	@Column(name ="OG_FREIGHT",nullable=true,precision=15,scale=4)
	public BigDecimal getOgFreight(){
		return this.ogFreight;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  导入的运费
	 */
	public void setOgFreight(BigDecimal ogFreight){
		this.ogFreight = ogFreight;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的物流公司
	 */
	@Column(name ="OG_FREIGHTCOMPANY",nullable=true,length=1024)
	public java.lang.String getOgFreightcompany(){
		return this.ogFreightcompany;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的物流公司
	 */
	public void setOgFreightcompany(java.lang.String ogFreightcompany){
		this.ogFreightcompany = ogFreightcompany;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的快递编码
	 */
	@Column(name ="OG_FREIGHTCODE",nullable=true,length=1024)
	public java.lang.String getOgFreightcode(){
		return this.ogFreightcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的快递编码
	 */
	public void setOgFreightcode(java.lang.String ogFreightcode){
		this.ogFreightcode = ogFreightcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的订单状态
	 */
	@Column(name ="OG_STATUS",nullable=true,length=255)
	public java.lang.String getOgStatus(){
		return this.ogStatus;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的订单状态
	 */
	public void setOgStatus(java.lang.String ogStatus){
		this.ogStatus = ogStatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  本系统物流公司编码
	 */
	@Column(name ="SHIPPERCODE",nullable=true,length=512)
	public java.lang.String getShippercode(){
		return this.shippercode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  本系统物流公司编码
	 */
	public void setShippercode(java.lang.String shippercode){
		this.shippercode = shippercode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电子面单号码
	 */
	@Column(name ="SHIPERNUM",nullable=true,length=1024)
	public java.lang.String getShipernum(){
		return this.shipernum;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电子面单号码
	 */
	public void setShipernum(java.lang.String shipernum){
		this.shipernum = shipernum;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=true)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATETIME",nullable=true)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  状态 1导入，2快递下单，3已揽收，4签收，5已删除
	 */
	@Column(name ="STATUS",nullable=true,precision=10,scale=0)
	public java.lang.Integer getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  状态 1导入，2快递下单，3已揽收，4签收，5已删除
	 */
	public void setStatus(java.lang.Integer status){
		this.status = status;
	}
	
	/**
	 * @return the company
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
}
