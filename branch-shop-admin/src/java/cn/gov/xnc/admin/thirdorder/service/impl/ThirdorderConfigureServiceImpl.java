package cn.gov.xnc.admin.thirdorder.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.thirdorder.service.ThirdorderConfigureServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("thirdorderConfigureService")
@Transactional
public class ThirdorderConfigureServiceImpl extends CommonServiceImpl implements ThirdorderConfigureServiceI {
	
}