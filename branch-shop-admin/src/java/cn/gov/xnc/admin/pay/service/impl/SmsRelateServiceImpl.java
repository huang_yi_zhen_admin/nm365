package cn.gov.xnc.admin.pay.service.impl;


import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.pay.entity.PaySmsRecordEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyReviewedEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("smsRelateService")
public class SmsRelateServiceImpl {
	@Resource(name="commonService")
	private CommonService commonService;
	private static final Logger logger = Logger.getLogger(SmsRelateServiceImpl.class);
	
	/**
	 *查询公司剩余短信条数 
	 **/
	public Integer getCompanySmsNum(){
		TSUser user = ResourceUtil.getSessionUserName();
		Integer msgnumberTotal = 0;
		
		if(user.getCompany() != null){
			if(user.getCompany().getCompanyreviewed() != null){
				CompanyReviewedEntity companyReviewedEntity = commonService.findUniqueByProperty(CompanyReviewedEntity.class, "id", user.getCompany().getCompanyreviewed().getId());
				msgnumberTotal = companyReviewedEntity.getMsgnumberTotal();
			}
		}
		return msgnumberTotal;
	}
	
	/**
	 * 短信购买成功后更新公司资费信息逻辑
	 * */
	public Integer companyReviewedUpdateById(String payBillsId){
		Integer result = 0;
		Integer number = 0;
		String companyId = "";
		String companyreviewedId = "";
		PaySmsRecordEntity paymentRecord = commonService.findUniqueByProperty(PaySmsRecordEntity.class, "payBillsId", payBillsId);
		if(paymentRecord != null){
			number = paymentRecord.getNumber();
			companyId = paymentRecord.getCompany_id();
		}
		if(!companyId.equals("")){
			TSCompany tSCompany = this.getCompanySingleData(companyId);
			if(tSCompany != null){
				if(tSCompany.getCompanyreviewed() != null){
					companyreviewedId = tSCompany.getCompanyreviewed().getId();
				}
			}
		}
		if(!companyreviewedId.equals("")){
			result = this.companyReviewedUpdateById(companyreviewedId, number);
		}
		return result;
	}
	
	/**
	 * 设置公司资费信息更新属性
	 * @return 
	 * */
	public Integer companyReviewedUpdateById(String id, Integer msgnumberTotal){
		Integer result = 0;
		CompanyReviewedEntity companyReviewed = new CompanyReviewedEntity();
		companyReviewed.setId(id);
		companyReviewed.setMsgnumberTotal(msgnumberTotal);
		result = this.companyRevMsgnumTotUpdateById(companyReviewed);
		return result;
	}
	
	/**
	 * 公司资费信息更新
	 * @return 
	 * */
	private Integer companyRevMsgnumTotUpdateById(CompanyReviewedEntity companyReviewed){
		StringBuilder sql = new StringBuilder();
		Integer result = 0 ;
		//where.put("id", companyReviewed.getId());
		//companyReviewed.setId(null);
		//String sql = SqlUtil.makeUpdateSql("t_s_company_reviewed", MyBeanUtils.beanToMapNew(companyReviewed), where);
		sql.append("update");
		sql.append(" t_s_company_reviewed");
		sql.append(" set");
		if(companyReviewed.getMsgnumberTotal() != null){
			sql.append(" msgnumberTotal = msgnumberTotal+" + companyReviewed.getMsgnumberTotal());
		}
		if(companyReviewed.getId() != null && !companyReviewed.getId().equals("")){
			sql.append(" where id='" + companyReviewed.getId() + "'");
		}else{
			sql.append(" where id=''");
		}
		logger.equals("companyRevMsgnumTotUpdateById:"+sql);
		result = commonService.updateBySqlString(sql.toString());
		return result;
	}
	
	/**
	 * 公司信息查询，单条数据
	 * */
	private TSCompany getCompanySingleData(String companyId){
		return commonService.findUniqueByProperty(TSCompany.class, "id", companyId);
	}
	
}
