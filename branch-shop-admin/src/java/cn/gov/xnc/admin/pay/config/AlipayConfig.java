﻿package cn.gov.xnc.admin.pay.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	//public static String app_id = "";//正式环境
	public static String app_id = "2016081900288623";//沙箱环境
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    //public static String merchant_private_key = "";//正式环境
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCRy+GWN2FTLPdgoIhbf12IgBwUqStEfzd7n2MwSdVpDYRh9/00/bCFC3T80noD6Ks66MKmOpayUWoHKQyVMsYUu5sXweu5iwN+Cmi4zbYZHjJ8o6n/EADYocM9zqRhQkDd0t5d8faJ+etUmQ3RTyzixzJ78/Q7l9XxlRSn0fSIKYmSSP5iABncJtR6O/uEMSOigXRKWu5bk65P3HyDdab4AEmNR/2ZR5aHiXkO03bI02yCqk6WKTx9sG3U90zVNiPvk+QH3/GFiDpqoIhVvKvDe+f7iiFnTMhl0zd+DLukuo4n5Vmn7G/q3zTVBqbi8OoVpaja3kV9/5Uka3RLweqlAgMBAAECggEALQrW48iyObcfqeuOlO3xYJVrl3p4Eo5W/stMIoP8+Zqidn3d7TwfB8t1VhJUfdao5+c3E5HwMqDyZ8nnyERKa+dVz+4qPEVVS+A9YDK1emcY4+bQQyuXh1XacsLfh+inD6Dw8Acryo66hIVQfoS+ckVmNGM25h5Pi3YqTnqP2Gy50OLrpM9ER4akAUXWQWTd4bfZYCQtrI/WW4vKcgQbEdMZZJxLctSeAjewZUBDKVSJRQFsUkAMK9tvg/mQ38AgZq4q692K9/4vriU/6b4DLYx1h1T8ZTYG1AyGYpYh8FUxTsVzwi5X+Frjfn1EeqFzBSOrVRReeyh6Yz6P4JWLIQKBgQD2l7gN9nixTVkxHtY36d4OL4Utjb7QFaG5XApbRNY7BCpq3bsepXxvZU2/p9dfq6m6Mwe+dNI+ZhW7Z3heEFhlhGeBrFrdLF9m9zoQpvPGZepPbuOWEe2C0Vk+vThf1RbjCm2TeOsvFqwhCQ60atFgItVyqHi8prk02fgqBb27CQKBgQCXW8NEgUpGOEHUD+2qhvPfQ0vXxkNCdpReO5+UcfZijxmJeEm3+34qoCHFc9+Fymmnd+wVNfuIxWleXcDSWrIe4YmzarMBCMsQNoIk51D7ktrAkeDso1doJbY9S0xkh4rdoLOPakPsHnIsCvnRny5S7XtG6krjCRRc4WfovKxtvQKBgQD1oCq1vcx6z5EIXK8FAu8tMih2N7rZirKM8CFgyDCcmImn6TV6UVyhmUbYoVbm9RcbYblU/QhvH0CGa4p6J83lUP3JLOVVad2dsqUj8SjhdqLufIY19aWfqOg8lj6RNrCVds2vF8MsJGlQfrUnAT+NgyUaELYzM99HxdZsBUv4AQKBgBCsH/dE0pynh8DHPGPDV8upCwqqoTAKBc/Xe32LjBtRLfvxjxVAE6a4Wt/4IpAHOtoioJO5jYqETFYDt8C0EtjJ+TQVNcCQamXPy0grXsVHaOc7am5Gk9M6DDf2OmT8dTRyhjTu/KpetC7byqbnhN/HnwOxvSwDDQYqIhsF5Wx1AoGAQT5OJVvJ5I2j/P44K/R3VUKWKXc6GkoMZLwvxDV4MJ0WdZk3uphXBzyDDiXHJaEJ9QuqYoWQ9Un0E98/Kyhs6tuPxnNkySt2mVDFCry/8H9HsbQD1KziJeRptdN1lVw4/sLJmkMTDocSozm8EQsb7lLO5Hr88/eMu06WJodlqbU=";//沙箱环境
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    //public static String alipay_public_key = "";//正式环境
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2T1SE5tXbIwwNzZ9Pdm/mbFr/nQAtdV3DoUIQ12VUa390s8BCif0VVhowVJ07eZ77LMys7KgzDi/KsjxwlFyeMYVkxLiC7nSqtRw6bBvUv3Zn9vJfca3diDNoyxYlwZVw0wf4NflnXzAJVLv+/zVPZ0BtiFXPM1wbLvthRCRABVYeoIb9nobtMdoZO+yTEVTYo1O6dq9rnT1W31d7kmJ8Zm87AkCj85rcZwhPZjTJ1oP2eM2q/Q9BQ8vsxznNOcwgkIDz0mE3RQgcDgZequ2VxqC/2LMQdvtThhOuh/P0ZTsSQsf20x+C6FrzGyqjRvORXDtmzsrfwUhPH8tX6LfEwIDAQAB";//沙箱环境

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://admin.my.nongmao365.com/aliPayController/notifyUrl";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://admin.my.nongmao365.com/aliPayController/returnUrl";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	//public static String gatewayUrl = "https://openapi.alipay.com/gateway.do";//正式环境
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";//沙箱环境
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

