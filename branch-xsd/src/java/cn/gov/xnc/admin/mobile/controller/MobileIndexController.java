package cn.gov.xnc.admin.mobile.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;


@Controller
@RequestMapping("/m")
public class MobileIndexController extends BaseController {

	public MobileIndexController() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * 移动端首页
	 * @return
	 */
	@RequestMapping(value = "**" )
	public ModelAndView index(HttpServletRequest request){
		return new ModelAndView("m/index"); 
	}

}
