package cn.gov.xnc.admin.product.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OrderBy;

import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.UserGradePriceEntity;


/**   
 * @Title: Entity
 * @Description: 产品基本信息
 * @author zero
 * @date 2016-09-28 15:44:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_product", schema = "")
@SuppressWarnings("serial")
public class ProductEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**商品简称*/
	private java.lang.String subtitle;
	/**商品名称*/
	private java.lang.String name;
	/**商品编码*/
	private java.lang.String code;
	/**规格*/
	private java.lang.String specifications;
	/**状态  1上架   2下架*/
	private java.lang.String state;
	/**导图，最多上传10张;隔开*/
	private java.lang.String guidepic;
	/**所属品牌*/
	private ProductBrandEntity brandid;
	/**商品等级*/
	private ProductClassifyEntity classifyid;
	/**公司信息*/
	private TSCompany company;
	/**keyword*/
	private java.lang.String keyword;
	/**单位*/
	private java.lang.String unit;
	/**最小起订量*/
	private BigDecimal minorde;
	/**销售状态 1正常 2预售 3停售*/
	private java.lang.String salesstatus;
	/**运费模板*/
	private FreightEntity freightid;
	/**订货价格*/
	private BigDecimal price;
	/**进货价格*/
	private BigDecimal pricec;
	/**市场价格*/
	private BigDecimal prices;
	/**业务奖励*/
	private BigDecimal priceyw;
	/**排序值 默认500*/
	private java.lang.Integer sort;
	/**库存数*/
	private BigDecimal stock;
	/**商品信息*/
	private java.lang.String content;
	/**商品简介*/
	private java.lang.String summary;
	/**创建时间*/
	private java.util.Date createdate;
	/**更新时间*/
	private java.util.Date updatedate;
	/**是否删除  Y 已删除  N 未删除*/
	private java.lang.String isDelete;
	/**是否非卖品  Y 是  N 否*/
	private java.lang.String notsale;
	/**商品扩展属性*/
	private java.lang.String strpro1;
	
	/**产品阶梯价格*/
	//private List<ProductLadderEntity> productLadderList = new ArrayList<ProductLadderEntity>();
	
	private List<UserGradePriceEntity> productUserGradelist = new ArrayList<UserGradePriceEntity>();
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品简称
	 */
	@Column(name ="SUBTITLE",nullable=true,length=255)
	public java.lang.String getSubtitle(){
		return this.subtitle;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品简称
	 */
	public void setSubtitle(java.lang.String subtitle){
		this.subtitle = subtitle;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品名称
	 */
	@Column(name ="NAME",nullable=true,length=300)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品编码
	 */
	@Column(name ="CODE",nullable=true,length=300)
	public java.lang.String getCode(){
		return this.code;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品编码
	 */
	public void setCode(java.lang.String code){
		this.code = code;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  规格
	 */
	@Column(name ="SPECIFICATIONS",nullable=true,length=400)
	public java.lang.String getSpecifications(){
		return this.specifications;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  规格
	 */
	public void setSpecifications(java.lang.String specifications){
		this.specifications = specifications;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态  1上架   2下架
	 */
	@Column(name ="STATE",nullable=true,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态  1上架   2下架
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导图，最多上传10张;隔开
	 */
	@Column(name ="GUIDEPIC",nullable=true,length=3000)
	public java.lang.String getGuidepic(){
		return this.guidepic;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导图，最多上传10张;隔开
	 */
	public void setGuidepic(java.lang.String guidepic){
		this.guidepic = guidepic;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属品牌
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BRANDID")
	public ProductBrandEntity getBrandid(){
		return this.brandid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属品牌
	 */
	public void setBrandid(ProductBrandEntity brandid){
		this.brandid = brandid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品等级
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLASSIFYID")
	public ProductClassifyEntity getClassifyid(){
		return this.classifyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品等级
	 */
	public void setClassifyid(ProductClassifyEntity classifyid){
		this.classifyid = classifyid;
	}
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  keyword
	 */
	@Column(name ="KEYWORD",nullable=true,length=4000)
	public java.lang.String getKeyword(){
		return this.keyword;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  keyword
	 */
	public void setKeyword(java.lang.String keyword){
		this.keyword = keyword;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  单位
	 */
	@Column(name ="UNIT",nullable=true,length=255)
	public java.lang.String getUnit(){
		return this.unit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  单位
	 */
	public void setUnit(java.lang.String unit){
		this.unit = unit;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  最小起订量
	 */
	@Column(name ="MINORDE",nullable=true,precision=10,scale=0)
	public BigDecimal getMinorde(){
		return this.minorde;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  最小起订量
	 */
	public void setMinorde(BigDecimal minorde){
		this.minorde = minorde;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  销售状态 1正常 2预售 3停售
	 */
	@Column(name ="SALESSTATUS",nullable=true,length=2)
	public java.lang.String getSalesstatus(){
		return this.salesstatus;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  销售状态 1正常 2预售 3停售
	 */
	public void setSalesstatus(java.lang.String salesstatus){
		this.salesstatus = salesstatus;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  freightid
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FREIGHTID")
	public FreightEntity getFreightid(){

		return this.freightid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  freightid
	 */
	public void setFreightid(FreightEntity freightid){
		this.freightid = freightid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  订货价格
	 */
	@Column(name ="PRICE",nullable=true,precision=12,scale=2)
	public BigDecimal getPrice(){
		return this.price;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  订货价格
	 */
	public void setPrice(BigDecimal price){
		this.price = price;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  进货价格
	 */
	@Column(name ="PRICEC",nullable=true,precision=12,scale=2)
	public BigDecimal getPricec(){
		return this.pricec;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  进货价格
	 */
	public void setPricec(BigDecimal pricec){
		this.pricec = pricec;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  市场价格
	 */
	@Column(name ="PRICES",nullable=true,precision=12,scale=2)
	public BigDecimal getPrices(){
		return this.prices;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  市场价格
	 */
	public void setPrices(BigDecimal prices){
		this.prices = prices;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  业务奖励
	 */
	@Column(name ="PRICEYW",nullable=true,precision=12,scale=2)
	public BigDecimal getPriceyw(){
		return this.priceyw;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  业务奖励
	 */
	public void setPriceyw(BigDecimal priceyw){
		this.priceyw = priceyw;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  排序值 默认500
	 */
	@Column(name ="SORT",nullable=true,precision=10,scale=0)
	public java.lang.Integer getSort(){
		return this.sort;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  排序值 默认500
	 */
	public void setSort(java.lang.Integer sort){
		this.sort = sort;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  content
	 */
	@Column(name ="CONTENT",nullable=true )
	public java.lang.String getContent(){
		return this.content;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  content
	 */
	public void setContent(java.lang.String content){
		this.content = content;
	}
	
	
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}

//	/**
//	 * @return 产品阶梯价格
//	 */
//	
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "product")
//	@OrderBy(clause = "ladderminorde desc")
//	public List<ProductLadderEntity> getProductLadderList() {
//		return productLadderList;
//	}
//
//	/**
//	 * @param 产品阶梯价格
//	 */
//	public void setProductLadderList(List<ProductLadderEntity> productLadderList) {
//		this.productLadderList = productLadderList;
//	}
	
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productid")
	public List<UserGradePriceEntity> getProductUserGradelist() {
		return productUserGradelist;
	}

	public void setProductUserGradelist(List<UserGradePriceEntity> productUserGradelist) {
		this.productUserGradelist = productUserGradelist;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否删除  Y 已删除  N 未删除
	 */
	@Column(name ="ISDELETE",nullable=true,length=2)
	public java.lang.String getIsDelete() {
		return isDelete;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  是否删除  Y 已删除  N 未删除
	 */
	public void setIsDelete(java.lang.String isDelete) {
		this.isDelete = isDelete;
	}

	/**
	 * @return 库存数
	 */
	@Column(name ="STOCK",nullable=true,precision=12,scale=2)
	public BigDecimal getStock() {
		return stock;
	}

	/**
	 * @param 库存数
	 */
	public void setStock(BigDecimal stock) {
		this.stock = stock;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  content
	 */
	@Column(name ="SUMMARY",nullable=true )
	public java.lang.String getSummary() {
		return summary;
	}

	public void setSummary(java.lang.String summary) {
		this.summary = summary;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否非卖品  Y 是  N 否
	 */
	@Column(name ="NOTSALE",nullable=true,length=2)
	public java.lang.String getNotsale() {
		return notsale;
	}

	public void setNotsale(java.lang.String notsale) {
		this.notsale = notsale;
	}

	@Column(name ="STRPRO1",nullable=true,length=4000)
	public java.lang.String getStrpro1() {
		return strpro1;
	}

	public void setStrpro1(java.lang.String strpro1) {
		this.strpro1 = strpro1;
	}
	
}
