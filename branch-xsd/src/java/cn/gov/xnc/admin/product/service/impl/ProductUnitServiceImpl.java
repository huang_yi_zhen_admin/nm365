package cn.gov.xnc.admin.product.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.service.ProductUnitServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("productUnitService")
@Transactional
public class ProductUnitServiceImpl extends CommonServiceImpl implements ProductUnitServiceI {
	
}