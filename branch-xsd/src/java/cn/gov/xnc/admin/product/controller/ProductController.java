package cn.gov.xnc.admin.product.controller;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.product.entity.ProductClassifyEntity;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.product.service.ProductServiceI;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.RoletoJson;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserGradePriceEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserTypeEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;

/**   
 * @Title: Controller
 * @Description: 产品基本信息
 * @author zero
 * @date 2016-09-28 15:44:49
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/productController")
public class ProductController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductController.class);

	@Autowired
	private ProductServiceI productService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserGradePriceServiceI userGradePriceService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 产品基本信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView product(HttpServletRequest request) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		List<ProductClassifyEntity> classifyList = systemService.findByProperty(ProductClassifyEntity.class, "company", user.getCompany());
		request.setAttribute("classifyList", classifyList);
		
		List<ProductBrandEntity> brandList = systemService.findByProperty(ProductBrandEntity.class, "company", user.getCompany());
		request.setAttribute("brandList", brandList);
		
		request.setAttribute("productname", request.getParameter("productname"));
		
		return new ModelAndView("admin/product/productList");
	}
	
	/**
	 * 入库商品选择
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "stockInSelectProduct")
	public ModelAndView stockInSelectProduct(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockInSelectProduct");
	}
	/**
	 * 出库商品选择
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "stockOutSelectProduct")
	public ModelAndView stockOutSelectProduct(HttpServletRequest request) {
		String stockid = request.getParameter("stockid");
		request.setAttribute("stockid", stockid);
		return new ModelAndView("admin/stock/stockOutSelectProduct");
	}
	

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ProductEntity product,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ProductEntity.class, dataGrid);

		//查询条件组装器
		cq.eq("isDelete", "N");
		if( StringUtil.isNotEmpty (product.getName())){
			cq.like("name", "%"+product.getName()+"%");
			product.setName(null);
		}
		if( StringUtil.isNotEmpty(product.getCode())){
			cq.like("code", "%"+product.getCode()+"%");
			product.setCode(null);
		}
		if( StringUtil.isNotEmpty(product.getSpecifications())){
			cq.like("specifications", "%"+product.getSpecifications()+"%");
			product.setSpecifications(null);
		}
		cq.addOrder("sort", SortDirection.asc);
		cq.addOrder("createdate", SortDirection.desc);
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, product, request.getParameterMap());
		this.productService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "stockproductdatagrid")
	public void stockproductdatagrid(StockProductEntity stockProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockProductEntity.class, dataGrid);
		cq.eq("companyid.id", user.getCompany().getId());
		if(null != stockProduct.getStockid() && StringUtil.isNotEmpty(stockProduct.getStockid().getId())){
			cq.eq("stockid.id", stockProduct.getStockid().getId());
		}
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockProduct, request.getParameterMap());
		this.productService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除产品基本信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ProductEntity product, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		product = systemService.getEntity(ProductEntity.class, product.getId());
		message = "产品基本信息删除成功";
		product.setIsDelete("Y");
		productService.updateEntitie(product);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加产品基本信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ProductEntity product, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		if(StringUtil.isEmpty(product.getIsDelete())){
			product.setIsDelete("N");
		}
		String productCode = product.getCode();
		
		if(null != productCode){
			//新添加的商品编码必须唯一
			if(StringUtil.isEmpty(product.getId())){
				CriteriaQuery cq = new CriteriaQuery(ProductEntity.class);
				cq.eq("code", productCode);
				
				List<ProductEntity> plist = systemService.getListByCriteriaQuery(cq, false);
				if(null != plist && plist.size() > 0){
					message = "商品编码已经存在，请输入其它编码";
					j.setMsg(message);
					j.setSuccess(false);
					return j;
				}
			}
			
		}else{
			message = "商品编码不能为空";
			j.setMsg(message);
			j.setSuccess(false);
			return j;
		}
		
		if (StringUtil.isNotEmpty(product.getId())) {
			message = "产品基本信息更新成功";
			ProductEntity t = productService.get(ProductEntity.class, product.getId());
			try {
					try {
						if( product.getBrandid() != null && StringUtil.isNotEmpty(product.getBrandid().getId())){
							ProductBrandEntity brand = null;
							try {
								 	CriteriaQuery cq = new CriteriaQuery(ProductBrandEntity.class);
								 	
								 		cq.eq("id", product.getBrandid().getId());
								 		cq.eq("company", user.getCompany());
								 	brand = (ProductBrandEntity) systemService.getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色
		
							} catch (Exception e) {
								 
							}
							if(brand == null){
								//创建新品牌信息
								product.setBrandid(null);
							}else {
								product.setBrandid(brand);
							}
						}
					}catch (Exception brandex) {
						brandex.printStackTrace();
					}
					
				//创建新分类
				try {
					if(   product.getClassifyid()!= null && StringUtil.isNotEmpty(product.getClassifyid().getId()) ){
						ProductClassifyEntity classify = null;
						try {
							 	CriteriaQuery cq = new CriteriaQuery(ProductClassifyEntity.class);
							 	
							 		cq.eq("id", product.getClassifyid().getId());
							 		cq.eq("company", user.getCompany());
							 		classify = (ProductClassifyEntity) systemService.getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色
	
						} catch (Exception e) {
							 
						}
						if(classify == null){
							//创建新品牌信息
							product.setClassifyid(null);
						}else {
							product.setClassifyid(classify);
							
						}
					}
				}catch (Exception classifyex) {
					classifyex.printStackTrace();
				}
				//更新用户等级价格
				try {
					List<UserGradePriceEntity> productUserGradelist = product.getProductUserGradelist();
					if(null != productUserGradelist && productUserGradelist.size() > 0){
						
						for (UserGradePriceEntity userGradePriceEntity : productUserGradelist) {
							CriteriaQuery cq = new CriteriaQuery(UserGradePriceEntity.class);
						 		cq.eq("productid", product.getId());
						 		cq.eq("usertypeid", userGradePriceEntity.getUsertypeid());
						 		cq.add();
						 		UserGradePriceEntity gradeprice = (UserGradePriceEntity) systemService.getObjectByCriteriaQuery(cq, false);
						 		
						 		
						 		if(null != gradeprice){
						 			gradeprice.setManualprice(userGradePriceEntity.getManualprice());
						 			gradeprice.setGradeprice(userGradePriceEntity.getGradeprice());
						 			//更新
						 			systemService.updateEntitie(gradeprice);
						 			
						 		}else{
						 			//添加
						 			userGradePriceEntity.setId(IdWorker.generateSequenceNo());
						 			userGradePriceEntity.setProductid(product.getId());
						 			systemService.save(userGradePriceEntity);
						 		}
						}
					}
				} catch (Exception e) {
					logger.error(e);
				}
				String content = product.getContent();
				if(StringUtil.isNotEmpty(content)){
					content = content.replaceAll("<p>[\\s]*<br>[\\s]*</p>|<p>[\\s]*</p>", "");
				}
				product.setContent(content);
				product.setProductUserGradelist(null);
				MyBeanUtils.copyBeanNotNull2Bean(product, t);
				productService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
				
			} catch (Exception e) {
					e.printStackTrace();
					message = "产品基本信息更新失败";
				}
		} else {
			//判定是否新增加品牌  分类
			
			try {
				if(   product.getBrandid() != null && StringUtil.isNotEmpty(product.getBrandid().getId())){
					ProductBrandEntity brand = null;
					try {
						 	CriteriaQuery cq = new CriteriaQuery(ProductBrandEntity.class);
						 	
						 		cq.eq("id", product.getBrandid().getId());
						 		cq.eq("company", user.getCompany());
						 	brand = (ProductBrandEntity) systemService.getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色

					} catch (Exception e) {
						 
					}
					if(brand == null){
						product.setBrandid(null);
					}else {
						product.setBrandid(brand);
						
					}
				}
			}catch (Exception brandex) {
				
			}
	
		//创建新分类
		try {
			//判定是否新增加品牌  分类
			if(   product.getClassifyid()!= null && StringUtil.isNotEmpty(product.getClassifyid().getId()) &&  product.getClassifyid().getId().indexOf("add_") ==0  ){
				ProductClassifyEntity classify = null;
				try {
					 	CriteriaQuery cq = new CriteriaQuery(ProductClassifyEntity.class);
					 	System.out.println(product.getClassifyid().getId().substring(4));
					 		cq.eq("classifyname", product.getClassifyid().getId().substring(4));
					 		cq.eq("company", user.getCompany());
					 		classify = (ProductClassifyEntity) systemService.getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色

				} catch (Exception e) {
					 
				}
				if(classify == null){
					product.setClassifyid(null);
				}else {
					product.setClassifyid(classify);
					
				}
			}
		}catch (Exception classifyex) {
				
		}
		
		//用户等级价格
		List<UserGradePriceEntity> newUserGradelist = new ArrayList<UserGradePriceEntity>();
		try {
			List<UserGradePriceEntity> productUserGradelist = product.getProductUserGradelist();
			if(null != productUserGradelist && productUserGradelist.size() > 0){
				for (UserGradePriceEntity userGradePriceEntity : productUserGradelist) {
					userGradePriceEntity.setId(IdWorker.generateSequenceNo());
					userGradePriceEntity.setProductid("tempid");
					newUserGradelist.add(userGradePriceEntity);
				}
				
				systemService.batchSave(productUserGradelist);
			}
		} catch (Exception e) {
			
		}
			
			message = "产品基本信息添加成功";
			productService.save(product);
			
			//把产品id注入到等级价格中去
			for (UserGradePriceEntity gradeprice : newUserGradelist) {
				gradeprice.setProductid(product.getId());
			}
			
			systemService.batchUpdate(newUserGradelist);
			
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		 }
		j.setMsg(message);
		return j;
	}

	/**
	 * 产品基本信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ProductEntity product, HttpServletRequest req) {
		
		//读取全部的品牌分类信息
		TSUser user = ResourceUtil.getSessionUserName();
		List<ProductBrandEntity> brandList = systemService.findByProperty(ProductBrandEntity.class, "company", user.getCompany());
		
		List<ProductClassifyEntity> classifyList = systemService.findByProperty(ProductClassifyEntity.class, "company", user.getCompany());
		
		List<ProductUnitEntity> unitlist = systemService.getList(ProductUnitEntity.class);
		
		
		if (StringUtil.isNotEmpty(product.getId())) {
			product = productService.getEntity(ProductEntity.class, product.getId());
			
			ProductBrandEntity productBrand = null;
			if(null != product.getBrandid() && StringUtil.isNotEmpty (product.getBrandid().getId().trim())){
				
				productBrand = systemService.findUniqueByProperty(ProductBrandEntity.class, "id", product.getBrandid().getId());
			}
			if(null == productBrand){
				product.setBrandid(new ProductBrandEntity());
				
			}else if(null != productBrand 
					&& StringUtil.isEmpty (product.getBrandid().getBrandname().trim())){
				
				product.setBrandid(new ProductBrandEntity());
			}
			
			FreightEntity freight = null;
			if(null != product.getFreightid() && StringUtil.isNotEmpty (product.getFreightid().getId().trim())){
				
				freight = systemService.findUniqueByProperty(FreightEntity.class, "id", product.getFreightid().getId());
				
			}
			if(null == freight){
				product.setFreightid(new FreightEntity());
				
			}else if(null != freight 
					&& StringUtil.isEmpty (product.getFreightid().getName().trim())){
				
				product.setFreightid(new FreightEntity());
			}
			
			ProductClassifyEntity classify = null;
			if(null != product.getClassifyid() && StringUtil.isNotEmpty (product.getClassifyid().getId().trim())){
				classify = systemService.findUniqueByProperty(ProductClassifyEntity.class, "id", product.getClassifyid().getId());
			}
			if(null == classify){
				product.setClassifyid(new ProductClassifyEntity());
				
			}else if(null != classify 
					&& StringUtil.isEmpty (product.getClassifyid().getClassifyname().trim())){
				
				product.setClassifyid(new ProductClassifyEntity());
				
			}

			req.setAttribute("productPage", product);
		}else {
			 product = new ProductEntity();
			req.setAttribute("productPage", product);
		}
		req.setAttribute("brandList", brandList);
		req.setAttribute("classifyList", classifyList);
		req.setAttribute("unitlist", unitlist);
		
		return new ModelAndView("admin/product/product");
	}
	
	
	/**
	 * 库存产品编辑 
	 * @return
	 */
	@RequestMapping(value = "addorupdatestock")
	public ModelAndView addorupdatestock(ProductEntity product, HttpServletRequest req) {
		
		//读取全部的品牌分类信息
		TSUser user = ResourceUtil.getSessionUserName();
		List<ProductBrandEntity> brandList = systemService.findByProperty(ProductBrandEntity.class, "company", user.getCompany());
		List<ProductClassifyEntity> classifyList = systemService.findByProperty(ProductClassifyEntity.class, "company", user.getCompany());
		List<ProductUnitEntity> unitlist = systemService.getList(ProductUnitEntity.class);
		
		if (StringUtil.isNotEmpty(product.getId())) {
			product = productService.getEntity(ProductEntity.class, product.getId());
			
			if(  product.getBrandid() == null  ||  StringUtil.isEmpty (product.getBrandid().getId().trim())){
				product.setBrandid(new ProductBrandEntity());
			}
			if(product.getFreightid() == null ||  StringUtil.isEmpty (product.getFreightid().getId().trim())){
				product.setFreightid(new FreightEntity());
			}
			if( product.getClassifyid() == null ||  StringUtil.isEmpty (product.getClassifyid().getId().trim())){
				product.setClassifyid(new ProductClassifyEntity());
			}
		}
		req.setAttribute("productPage", product);
		req.setAttribute("brandList", brandList);
		req.setAttribute("classifyList", classifyList);
		req.setAttribute("unitlist", unitlist);
		
		return new ModelAndView("admin/stock/stockProductInfo");
	}
	
	/**
	 * 产品基本信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "details")
	public ModelAndView details(ProductEntity product, HttpServletRequest req) {
		
		//读取全部的品牌分类信息
		TSUser user = ResourceUtil.getSessionUserName();
		List<ProductBrandEntity> brandList = systemService.findByProperty(ProductBrandEntity.class, "company", user.getCompany());
		
		List<ProductClassifyEntity> classifyList = systemService.findByProperty(ProductClassifyEntity.class, "company", user.getCompany());
		
		
		if (StringUtil.isNotEmpty(product.getId())) {
			product = productService.getEntity(ProductEntity.class, product.getId());
			
			if(  product.getBrandid() == null  ||  StringUtil.isEmpty (product.getBrandid().getId().trim())){
				product.setBrandid(new ProductBrandEntity());
			}
			if(product.getFreightid() == null ||  StringUtil.isEmpty (product.getFreightid().getId().trim())){
				product.setFreightid(new FreightEntity());
			}
			if( product.getClassifyid() == null ||  StringUtil.isEmpty (product.getClassifyid().getId().trim())){
				product.setClassifyid(new ProductClassifyEntity());
			}

			req.setAttribute("productPage", product);
		}
		req.setAttribute("brandList", brandList);
		req.setAttribute("classifyList", classifyList);
		
		return new ModelAndView("admin/product/productDetails");
	}
	
	
	
	/**
	 * 商城列表页跳转
	 */
	@RequestMapping(value = "initProductlist")
	public String initProductList(HttpServletRequest request) {
		return "productlist";
	}
	

	/**
	 * 产品表选择页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "choiceProductList")
	public ModelAndView choiceProductList(HttpServletRequest request) {
		return new ModelAndView("admin/product/choiceProductList");
	}
	
	/**
	 * 产品基本信息 easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "choiceProduct")
	public void choiceProduct(ProductEntity product,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		product.setState("1");//上架产品
		CriteriaQuery cq = new CriteriaQuery(ProductEntity.class, dataGrid);
		//查询条件组装器
		cq.eq("isDelete", "N");
		if( StringUtil.isNotEmpty (product.getName())){
			cq.like("name", "%"+product.getName()+"%");
			product.setName(null);
		}
		if( StringUtil.isNotEmpty(product.getCode())){
			cq.like("code", "%"+product.getCode()+"%");
			product.setCode(null);
		}
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, product, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 手工初始化所有用户等级数据
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "testgrade")
	@ResponseBody
	public String TestUpdateGrade(HttpServletRequest request) {
		CriteriaQuery cq1 = new CriteriaQuery(TSCompany.class);
		cq1.add();
		List<TSCompany> companylist = systemService.getListByCriteriaQuery(cq1, false);
		
		List<ProductEntity> productlist = null;
		List<UserTypeEntity> usertypelist = null;
		for (TSCompany tsCompany : companylist) {
			
			//创建默认的会员身份
			UserTypeEntity userTypeadd = new UserTypeEntity();
				userTypeadd.setCompany(tsCompany);
				userTypeadd.setLevelscale( new BigDecimal("100"));
				userTypeadd.setType("1");		
				userTypeadd.setTypename("系统默认");
				userTypeadd.setRemarks("系统默认级别");
			systemService.save(userTypeadd);
	
				//产品
				CriteriaQuery cq3 = new CriteriaQuery(ProductEntity.class);
					cq3.eq("company", tsCompany);
					cq3.add();
				productlist = systemService.getListByCriteriaQuery(cq3, false);
				
				
				List<UserGradePriceEntity> gradelist = new ArrayList<UserGradePriceEntity>();
				for (ProductEntity productEntity : productlist) {
					
					BigDecimal price = productEntity.getPrice();
					if(price == null ){
						price = new BigDecimal("0.00");
					}
					
					//用户等级
					CriteriaQuery cq2 = new CriteriaQuery(UserTypeEntity.class);
					cq2.eq("company", tsCompany);
					cq2.add();
					usertypelist = systemService.getListByCriteriaQuery(cq2, false);
					for (UserTypeEntity userTypeEntity : usertypelist) {
						
						try {
							BigDecimal scale= userTypeEntity.getLevelscale();
							BigDecimal scaleprice = price.multiply(scale);
							scaleprice = scaleprice.divide(new BigDecimal(100));
							UserGradePriceEntity gradeEntity = new UserGradePriceEntity();
							
							gradeEntity.setId(IdWorker.generateSequenceNo());
							gradeEntity.setGradeprice(scaleprice);
							gradeEntity.setManualprice(scaleprice);
							gradeEntity.setProductid(productEntity.getId());
							gradeEntity.setUsertypeid(userTypeEntity.getId());
							gradeEntity.setCreatedate(DateUtils.getDate());
							gradeEntity.setUpdatedate(DateUtils.getDate());
							
							systemService.save(gradeEntity);
						} catch (Exception e) {
							cq2 = null;
						}
					}
					cq2 = null;
				}
			}
		return "OK";
	}
}
