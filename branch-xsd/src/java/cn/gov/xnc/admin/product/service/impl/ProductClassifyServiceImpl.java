package cn.gov.xnc.admin.product.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.service.ProductClassifyServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("productClassifyService")

public class ProductClassifyServiceImpl extends CommonServiceImpl implements ProductClassifyServiceI {
	
}