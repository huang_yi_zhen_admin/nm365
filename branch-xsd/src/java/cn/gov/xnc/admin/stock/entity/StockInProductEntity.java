package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.product.entity.ProductEntity;

/**   
 * @Title: Entity
 * @Description: 入库产品信息
 * @author zero
 * @date 2017-01-23 15:03:57
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stockin_product", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockInProductEntity implements java.io.Serializable {
	/**入库编码*/
	private java.lang.String id;
	/**产品编码*/
	private ProductEntity productid;
	/**产品名称*/
	private java.lang.String productname;
	/**产品品牌*/
	private ProductBrandEntity productbrandid;
	/**产品规格*/
	private java.lang.String specifications;
	/**产品单位*/
	private java.lang.String unit;
	/**入库数量*/
	private BigDecimal stockinnum;
	/**当前库存量*/
	private BigDecimal stockcurnum;
	/**库存预警值*/
	private BigDecimal alertnum;
	/**进货单价*/
	private BigDecimal unitprice;
	/**进货总价*/
	private BigDecimal totalprice;
	/**备注*/
	private java.lang.String remark;
	/**入库仓库编码*/
	private StockIOEntity stockorderid;
	/**库存状态 */
	private String status;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  入库编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  入库编码
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品编码
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品名称
	 */
	@Column(name ="PRODUCTNAME",nullable=false,length=300)
	public java.lang.String getProductname(){
		return this.productname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品名称
	 */
	public void setProductname(java.lang.String productname){
		this.productname = productname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品品牌
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTBRANDID")
	public ProductBrandEntity getProductbrandid(){
		return this.productbrandid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品品牌
	 */
	public void setProductbrandid(ProductBrandEntity productbrandid){
		this.productbrandid = productbrandid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品规格
	 */
	@Column(name ="SPECIFICATIONS",nullable=true,length=500)
	public java.lang.String getSpecifications(){
		return this.specifications;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品规格
	 */
	public void setSpecifications(java.lang.String specifications){
		this.specifications = specifications;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品单位
	 */
	@Column(name ="UNIT",nullable=false,length=2)
	public java.lang.String getUnit(){
		return this.unit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品单位
	 */
	public void setUnit(java.lang.String unit){
		this.unit = unit;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  入库数量
	 */
	@Column(name ="STOCKINNUM",nullable=false,precision=10,scale=2)
	public BigDecimal getStockinnum(){
		return this.stockinnum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  入库数量
	 */
	public void setStockinnum(BigDecimal stockinnum){
		this.stockinnum = stockinnum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  进货单价
	 */
	@Column(name ="UNITPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getUnitprice(){
		return this.unitprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  进货单价
	 */
	public void setUnitprice(BigDecimal unitprice){
		this.unitprice = unitprice;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  进货总价
	 */
	@Column(name ="TOTALPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getTotalprice(){
		return this.totalprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  进货总价
	 */
	public void setTotalprice(BigDecimal totalprice){
		this.totalprice = totalprice;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=4000)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  入库单编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKORDERID")
	public StockIOEntity getStockorderid(){
		return this.stockorderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  入库单编码
	 */
	public void setStockorderid(StockIOEntity stockorderid){
		this.stockorderid = stockorderid;
	}

	public BigDecimal getAlertnum() {
		return alertnum;
	}

	public void setAlertnum(BigDecimal alertnum) {
		this.alertnum = alertnum;
	}

	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  入库数量
	 */
	@Column(name ="STOCKCURNUM",nullable=false,precision=10,scale=2)
	public BigDecimal getStockcurnum() {
		return stockcurnum;
	}

	public void setStockcurnum(BigDecimal stockcurnum) {
		this.stockcurnum = stockcurnum;
	}

	@Column(name ="STATUS",nullable=true,length=2)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
