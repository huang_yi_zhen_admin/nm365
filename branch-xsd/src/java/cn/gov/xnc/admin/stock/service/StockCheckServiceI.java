package cn.gov.xnc.admin.stock.service;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockCheckServiceI extends CommonService{
	
	public AjaxJson saveStockCheck(StockCheckEntity stockCheckOrder, HttpServletRequest request);

}
