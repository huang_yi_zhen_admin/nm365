package cn.gov.xnc.admin.stock.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.service.StockIOTypeServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("stockTypeService")
@Transactional
public class StockIOTypeServiceImpl extends CommonServiceImpl implements StockIOTypeServiceI {
	
}