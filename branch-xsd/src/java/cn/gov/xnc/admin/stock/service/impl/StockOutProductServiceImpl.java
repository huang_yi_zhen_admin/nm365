package cn.gov.xnc.admin.stock.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.service.StockOutProductServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("stockoutProductService")
@Transactional
public class StockOutProductServiceImpl extends CommonServiceImpl implements StockOutProductServiceI {
	
}