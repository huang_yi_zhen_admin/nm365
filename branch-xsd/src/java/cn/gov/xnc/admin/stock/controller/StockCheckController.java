package cn.gov.xnc.admin.stock.controller;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.RoletoJson;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.service.StockCheckDetailServiceI;
import cn.gov.xnc.admin.stock.service.StockCheckServiceI;

/**   
 * @Title: Controller
 * @Description: 进销存盘点总单
 * @author zero
 * @date 2017-01-23 15:39:08
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockCheckController")
public class StockCheckController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockCheckController.class);

	@Autowired
	private StockCheckServiceI stockCheckService;
	@Autowired
	private StockCheckDetailServiceI stockCheckDetailService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 进销存盘点总单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockCheckOrder(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", company);
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		
		request.setAttribute("stocklist", stockList);
		
		//获取公司所有员工作为盘点员
		CriteriaQuery cq1 = new CriteriaQuery(TSUser.class);
		cq1.eq("company", company);
		cq1.add();
		
		List<TSUser> userlist = systemService.getListByCriteriaQuery(cq1,false);
		request.setAttribute("userlist", userlist);
		
		return new ModelAndView("admin/stock/stockCheckList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockCheckEntity stockCheckOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(StockCheckEntity.class, dataGrid);
		TSUser user = ResourceUtil.getSessionUserName();
		if(StringUtil.isNotEmpty(stockCheckOrder.getName())){
			cq.like("name", "%" + stockCheckOrder.getName() + "%");
			stockCheckOrder.setName(null);
		}
		cq.eq("companyid", user.getCompany());
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createtime", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createtime", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createtime", DateUtils.str2Date(createdate2,sdf));
		}
		if(null != stockCheckOrder.getStockid()){
			stockCheckOrder.getStockid().setCompanyid(user.getCompany());
		}else{
			StockEntity stock = new StockEntity();
			stock.setCompanyid(user.getCompany());
			stockCheckOrder.setStockid(stock);
		}
		cq.addOrder("createtime", SortDirection.desc);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockCheckOrder, request.getParameterMap());
		this.stockCheckService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除进销存盘点总单
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockCheckOrder = systemService.getEntity(StockCheckEntity.class, stockCheckOrder.getId());
		message = "进销存盘点总单删除成功";
		stockCheckService.delete(stockCheckOrder);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加进销存盘点总单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			j = stockCheckService.saveStockCheck(stockCheckOrder, request);
			
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("加进销存盘点总单出错：" + e);
			
			systemService.addLog("加进销存盘点总单出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return j;
	}

	/**
	 * 进销存盘点总单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockCheckEntity stockCheck, HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		req.setAttribute("operater", user);
		if (StringUtil.isNotEmpty(stockCheck.getId())) {
			stockCheck = stockCheckService.getEntity(StockCheckEntity.class, stockCheck.getId());
			
		}
		
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		req.setAttribute("stocklist", stockList);
				
		req.setAttribute("stockCheck", stockCheck);
		return new ModelAndView("admin/stock/stockCheckAdd");
	}
	
	/**
	 * 进销存盘点总单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockCheckDetail")
	public ModelAndView stockCheckDetail(StockCheckEntity stockCheckOrder, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockCheckOrder.getId())) {
			stockCheckOrder = stockCheckService.getEntity(StockCheckEntity.class, stockCheckOrder.getId());
			
		}
		req.setAttribute("stockCheckDetail", stockCheckOrder);
		return new ModelAndView("admin/stock/stockCheckDetail");
	}
}
