package cn.gov.xnc.admin.stock.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockInProductEntity;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.service.StockCheckDetailServiceI;

/**   
 * @Title: Controller
 * @Description: 盘点库存产品详细
 * @author zero
 * @date 2017-01-23 15:09:54
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockCheckDetailController")
public class StockCheckDetailController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockCheckDetailController.class);

	@Autowired
	private StockCheckDetailServiceI stockCheckProductService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 盘点库存产品详细列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView companyStockCheckProduct(HttpServletRequest request) {
		request.setAttribute("checkorderid", request.getParameter("checkorderid"));
		return new ModelAndView("admin/stock/stockCheckProductList");
	}

	
	@RequestMapping(value = "stockCheckDetailInfos")
	public ModelAndView stockCheckDetailInfos(HttpServletRequest request) {
		String stockCheckId = request.getParameter("id");
		if(StringUtil.isNotEmpty(stockCheckId)){
			StockCheckEntity stockCheckEntity = systemService.findUniqueByProperty(StockCheckEntity.class, "id", stockCheckId);
			request.setAttribute("stockCheck", stockCheckEntity);
		}
		
		return new ModelAndView("admin/stock/stockCheckDetailInfos");
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockCheckDetailEntity stockCheckProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		String checkorderid = request.getParameter("orderid");
		CriteriaQuery cq = new CriteriaQuery(StockCheckDetailEntity.class, dataGrid);
		cq.eq("checkorderid.id", checkorderid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockCheckProduct, request.getParameterMap());
		this.stockCheckProductService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除盘点库存产品详细
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockCheckDetailEntity companyStockCheckProduct, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		companyStockCheckProduct = systemService.getEntity(StockCheckDetailEntity.class, companyStockCheckProduct.getId());
		message = "盘点库存产品详细删除成功";
		stockCheckProductService.delete(companyStockCheckProduct);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加盘点库存产品详细
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		StringBuffer errorBuf = new StringBuffer();
		TSUser user=  ResourceUtil.getSessionUserName();
		String orderid = IdWorker.generateSequenceNo();
		stockCheckOrder.setId(orderid);
		stockCheckOrder.setCreatetime(DateUtils.getDate());
		stockCheckOrder.setOperator(user);
		
		List<StockCheckDetailEntity> stockCheckProductList = stockCheckOrder.getstockCheckDetailList();
		if(stockCheckProductList != null && stockCheckProductList.size() > 0){
			for (StockCheckDetailEntity stockCheckProductEntity : stockCheckProductList) {
				if (StringUtil.isNotEmpty(stockCheckProductEntity.getId())) {
					message = "盘点库存产品详细更新成功";
					StockCheckDetailEntity t = stockCheckProductService.get(StockCheckDetailEntity.class, stockCheckProductEntity.getId());
					try {
						MyBeanUtils.copyBeanNotNull2Bean(stockCheckProductEntity, t);
						stockCheckProductService.saveOrUpdate(t);
						systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
					} catch (Exception e) {
						e.printStackTrace();
						errorBuf.append("【"+ stockCheckProductEntity.getStockproductid().getProductid().getName() + "】");
					}
				} else {
					message = "盘点库存产品详细添加成功";
					//构建入库产品单
					stockCheckProductEntity.setId(IdWorker.generateSequenceNo());
					stockCheckProductEntity.setCheckorderid(stockCheckOrder);
//					stockinProductService.save(stockInProductEntity);
					systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
				}
			}
			
			stockCheckProductService.batchSave(stockCheckProductList);
			
			systemService.save(stockCheckOrder);
		}
		if(errorBuf.length() > 0){
			message += "其中：" + errorBuf.toString() + "失败！";
		}
		j.setMsg(message);
		
		return j;
	}

	/**
	 * 盘点库存产品详细列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockCheckDetailEntity companyStockCheckProduct, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(companyStockCheckProduct.getId())) {
			companyStockCheckProduct = stockCheckProductService.getEntity(StockCheckDetailEntity.class, companyStockCheckProduct.getId());
			req.setAttribute("companyStockCheckProductPage", companyStockCheckProduct);
		}
		
		//获取品牌
		List<ProductBrandEntity> productBrandList = systemService.getList(ProductBrandEntity.class);
		req.setAttribute("brandList", productBrandList);
		
		//获取仓库
		List<StockEntity> stockList = systemService.getList(StockEntity.class);
		req.setAttribute("stockList", stockList);
		
		//获取所有进销存入库类型
		CriteriaQuery cq1 = new CriteriaQuery(StockIOTypeEntity.class);
		cq1.eq("typecode", "I");
		cq1.add();
		List<StockIOTypeEntity> stockTypeList = systemService.getListByCriteriaQuery(cq1, false);
		req.setAttribute("stockTypeList", stockTypeList);
		
		req.setAttribute("operator", ResourceUtil.getSessionUserName());
		
		return new ModelAndView("admin/stock/stockCheckDetail");
	}
}
