package cn.gov.xnc.admin.stock.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockCheckDetailServiceI extends CommonService{

	/**
	 * 构建出入库详细数据
	 * @param stockIOEntity
	 * @param req
	 * @return
	 * @throws Exception
	 */
	public List<StockCheckDetailEntity> buildStockCheckDetail(final StockCheckEntity stockCheckEntity, HttpServletRequest req) throws Exception;
}
