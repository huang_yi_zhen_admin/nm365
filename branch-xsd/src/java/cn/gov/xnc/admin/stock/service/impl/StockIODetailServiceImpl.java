package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightDetailsTerritoryEntity;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;

@Service("stockIODetailService")
@Transactional
public class StockIODetailServiceImpl extends CommonServiceImpl implements StockIODetailServiceI {
	
	@Override
	public List<StockIODetailEntity> buildStockInDetail(final StockIOEntity stockIOEntity, HttpServletRequest req) throws Exception{
		List<StockIODetailEntity> stockIODetailList = new ArrayList<StockIODetailEntity>();
		String expLen = ResourceUtil.getParameter("Len");
		if(StringUtil.isNotEmpty(expLen)){
			
			int stockIODetailsNum = Integer.parseInt(expLen);
			for (int i = 0; i < stockIODetailsNum; i++) {
				String productid = req.getParameter("id-" + i);
				String productName = req.getParameter("name-" + i);
				if(StringUtil.isNotEmpty(productid) && StringUtil.isNotEmpty(productName)){
					
					ProductEntity productEntity = findUniqueByProperty(ProductEntity.class, "id", req.getParameter("id-" + i));
					StockIODetailEntity stockIODetail = new StockIODetailEntity();
//					stockIODetail.setId(IdWorker.generateSequenceNo());
					stockIODetail.setIdentifier(IdWorker.generateSequenceNo());
					stockIODetail.setIostocknum(new BigDecimal(req.getParameter("iostocknum-" + i)));
					stockIODetail.setProductid(productEntity);
					stockIODetail.setRemark(req.getParameter("detailremark-" + i));
					stockIODetail.setStockorderid(stockIOEntity);
					stockIODetail.setUnitprice(new BigDecimal(req.getParameter("unitprice-" + i)));
					stockIODetail.setTotalprice(new BigDecimal(req.getParameter("totalprice-" + i)));
					stockIODetailList.add(stockIODetail);
				}
				
			}
		}
		
		if(stockIODetailList.size() > 0){
			
			batchSave(stockIODetailList);
			
		}
		
		return stockIODetailList;
	}
}