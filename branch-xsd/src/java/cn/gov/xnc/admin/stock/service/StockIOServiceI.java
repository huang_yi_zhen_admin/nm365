package cn.gov.xnc.admin.stock.service;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.service.SystemService;

public interface StockIOServiceI extends CommonService{

	public StockEntity buildStockOrder();
	
	public AjaxJson save(StockIOEntity companyStockOrder, HttpServletRequest request,StockIODetailServiceI stockIODetailService,StockProductServiceI stockProductService,SystemService systemService);
}
