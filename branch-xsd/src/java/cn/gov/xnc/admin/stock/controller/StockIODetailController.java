package cn.gov.xnc.admin.stock.controller;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;

/**   
 * @Title: Controller
 * @Description: 出库详细单
 * @author zero
 * @date 2017-03-22 14:40:07
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockIODetailController")
public class StockIODetailController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockIODetailController.class);

	@Autowired
	private StockIODetailServiceI stockIODetailService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 出库详细单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockIODetailList(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockIODetailList");
	}
	/**
	 * 出库详细单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockIODetailInfos")
	public ModelAndView stockInDetailInfos(HttpServletRequest request) {
		String stockioid = request.getParameter("id");
		StockIOEntity stockInEntity = new StockIOEntity();
		if(StringUtil.isNotEmpty(stockioid)){
			stockInEntity = stockIODetailService.findUniqueByProperty(StockIOEntity.class, "id", stockioid);
		}
		request.setAttribute("stockInEntity", stockInEntity);
		request.setAttribute("type", request.getParameter("type"));
		
		return new ModelAndView("admin/stock/stockIODetailInfos");
	}
	
	
	/**
	 * easyui 统计总价
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(value = "datagridStatis")
	@ResponseBody
	public AjaxJson datagridStatis(StockIODetailEntity stockIOOrderDetail,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		AjaxJson j = new AjaxJson();
		
		CriteriaQuery cq = getDatagridCriteriaQuery(stockIOOrderDetail, request, response, dataGrid);
		
		
		
		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.sum("totalprice"));

		
		cq.setProjectionList(projectionList);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockIOOrderDetail, request.getParameterMap());
				
		this.stockIODetailService.getDataGridReturn(cq, true);
		
		dataGrid = cq.getDataGrid();
		
		List<BigDecimal> list = dataGrid.getResults();
		
		BigDecimal totalprice = list.get(0);
		
		Map<String,Object> map = new HashMap<>();
		map.put("totalprice", totalprice);
		
		j.setObj(map);
		j.setSuccess(true);
		
		return j;
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(value = "datagrid")
	public void datagrid(StockIODetailEntity stockIOOrderDetail,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = getDatagridCriteriaQuery(stockIOOrderDetail, request, response, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockIOOrderDetail, request.getParameterMap());
		this.stockIODetailService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	private CriteriaQuery getDatagridCriteriaQuery(StockIODetailEntity stockIOOrderDetail,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid){
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class, dataGrid);
		
		return cq;
	}

	/**
	 * 删除出库详细单
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockIODetailEntity stockIOOrderDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockIOOrderDetail = systemService.getEntity(StockIODetailEntity.class, stockIOOrderDetail.getId());
		message = "出库详细单删除成功";
		stockIODetailService.delete(stockIOOrderDetail);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加出库详细单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockIODetailEntity stockIOOrderDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(stockIOOrderDetail.getId())) {
			message = "出库详细单更新成功";
			StockIODetailEntity t = stockIODetailService.get(StockIODetailEntity.class, stockIOOrderDetail.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockIOOrderDetail, t);
				stockIODetailService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "出库详细单更新失败";
			}
		} else {
			message = "出库详细单添加成功";
			stockIODetailService.save(stockIOOrderDetail);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 出库详细单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockIODetailEntity stockIOOrderDetail, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockIOOrderDetail.getId())) {
			stockIOOrderDetail = stockIODetailService.getEntity(StockIODetailEntity.class, stockIOOrderDetail.getId());
			req.setAttribute("stockIOOrderDetailPage", stockIOOrderDetail);
		}
		return new ModelAndView("admin/stock/stockIOOrderDetail");
	}
}
