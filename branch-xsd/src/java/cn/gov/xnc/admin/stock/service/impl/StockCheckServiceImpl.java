package cn.gov.xnc.admin.stock.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.service.StockCheckDetailServiceI;
import cn.gov.xnc.admin.stock.service.StockCheckServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("stockCheckService")
@Transactional
public class StockCheckServiceImpl extends CommonServiceImpl implements StockCheckServiceI {
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private StockCheckDetailServiceI stockCheckDetailService;
	@Autowired
	private StockProductServiceI stockProductService;
	
	private String message;

	public AjaxJson saveStockCheck(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		TSUser user = ResourceUtil.getSessionUserName();
		if (StringUtil.isNotEmpty(stockCheckOrder.getId())) {
			message = "进销存盘点总单更新成功";
			StockCheckEntity t = get(StockCheckEntity.class, stockCheckOrder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockCheckOrder, t);
				saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
				
			} catch (Exception e) {
				message = "进销存盘点总单更新失败" + e;
			}
		} else {
			
			try {
				message = "进销存盘点添加成功";
				
				if(StringUtil.isEmpty(stockCheckOrder.getName())){
					throw new Exception("盘点名称不能为空！");
				}
				
				TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
				TSUser operator = systemService.findUniqueByProperty(TSUser.class, "id", stockCheckOrder.getOperator().getId());
				StockEntity stock = systemService.findUniqueByProperty(StockEntity.class, "id", stockCheckOrder.getStockid().getId());
				//创建基础数据
				StockCheckEntity stockCheckEntity = new StockCheckEntity();
				stockCheckEntity.setIdentifier(IdWorker.generateSequenceNo());
				stockCheckEntity.setCompanyid(company);
				stockCheckEntity.setCreatetime(DateUtils.getDate());
				stockCheckEntity.setName(stockCheckOrder.getName());
				stockCheckEntity.setOperator(operator);
				stockCheckEntity.setstockCheckDetailList(null);
				stockCheckEntity.setStockid(stock);
				
				save(stockCheckEntity);
				
				//创建出入库详细数据
				List<StockCheckDetailEntity> stockCheckDetaillList = stockCheckDetailService.buildStockCheckDetail(stockCheckEntity, request);
				if(stockCheckDetaillList.size() > 0){
					stockCheckEntity.setstockCheckDetailList(stockCheckDetaillList);
					updateEntitie(stockCheckEntity);
					//重新获取商品对象，避免no proxy -session 错误
					try {
						//更新库存状况
						for (StockCheckDetailEntity stockCheckDetailEntity : stockCheckDetaillList) {
							
								ProductEntity productEntity = this.findUniqueByProperty(ProductEntity.class, "id", stockCheckDetailEntity.getStockproductid().getProductid().getId());
								stockProductService.saveOrUpdateStockProduct(productEntity, 
										"CK", 
										stockCheckEntity.getStockid(),
										stockCheckDetailEntity.getCheckstocknum(),null,null);
								
							
						}
					} catch (Exception e) {
						j.setSuccess(false);
						message = "盘点库存更新库存产品数量出错：" + e;
					}
				}else{
					j.setSuccess(false);
					message = "进销存盘点失败，您没有操作任何商品！";
					delete(stockCheckEntity);//回滚
				}
				
			} catch (Exception e) {
				message = "进销存盘点添加失败" + e;
				j.setSuccess(false);
			}
		}
		
		j.setMsg(message);
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		
		return j;
	}
	
}