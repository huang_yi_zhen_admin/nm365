package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 出入库表单，记录出入库总体信息
 * @author zero
 * @date 2017-01-23 15:03:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_io", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockIOEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**identifier*/
	private java.lang.String identifier;
	/**经办人*/
	private TSUser operator;
	/**仓库*/
	private StockEntity stockid;
	/**出/入库类型*/
	private StockIOTypeEntity stocktypeid;
	/**创建时间*/
	private java.util.Date createtime;
	/**备注*/
	private java.lang.String remark;
	/**公司*/
	private TSCompany companyid;
	/**入库产品*/
	private List<StockIODetailEntity> stockIODetailList;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  经办人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OPERATOR")
	public TSUser getOperator(){
		return this.operator;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  经办人
	 */
	public void setOperator(TSUser operator){
		this.operator = operator;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓库
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKID")
	public StockEntity getStockid(){
		return this.stockid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  仓库
	 */
	public void setStockid(StockEntity stockid){
		this.stockid = stockid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  出/入库类型
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKTYPEID")
	public StockIOTypeEntity getStocktypeid(){
		return this.stocktypeid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  出/入库类型
	 */
	public void setStocktypeid(StockIOTypeEntity stocktypeid){
		this.stocktypeid = stocktypeid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="REMARK",nullable=true)
	public java.lang.String getRemark() {
		return remark;
	}

	public void setRemark(java.lang.String remark) {
		this.remark = remark;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司编码
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  出库单普通编号
	 */
	@Column(name ="IDENTIFIER",nullable=true,length=32)
	public java.lang.String getIdentifier(){
		return this.identifier;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单编号
	 */
	public void setIdentifier(java.lang.String identifier){
		this.identifier = identifier;
	}

	/**出库库详细*/
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "stockorderid")
	public List<StockIODetailEntity> getStockIODetailList() {
		return stockIODetailList;
	}

	public void setStockIODetailList(List<StockIODetailEntity> stockIODetailList) {
		this.stockIODetailList = stockIODetailList;
	}
}
