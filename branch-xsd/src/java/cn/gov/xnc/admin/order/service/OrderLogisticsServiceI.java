package cn.gov.xnc.admin.order.service;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderLogisticsServiceI extends CommonService{

	public void saveOrderLogistics(OrderEntity order, String logisticsCode, String logisticsCompany);
}
