package cn.gov.xnc.admin.order.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.excel.vo.XiadanProductVO;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.mobile.util.ParamUtil;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanCommissionServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;

@Service("orderService")

public class OrderServiceImpl extends CommonServiceImpl implements OrderServiceI {

	@Autowired
	private SystemService systemService;
	
	@Autowired
	private UserGradePriceServiceI userGradePriceService;
	@Autowired
	private FreightDetailsServiceI freightDetailsService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private MessageTemplateServiceI messageTemplateService ;
	@Autowired
	private SalesmanCommissionServiceI salesmanCommissionService ;
	private Logger logger = Logger.getLogger(OrderServiceImpl.class);

	
	
	/**
	 * 订单生成
	 * @param request
	 * @param response
	 * @param xiadanlist
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<OrderEntity> orderGenerator(HttpServletRequest request, List<XiadanProductVO> xiadanlist) throws Exception{
		
		//ImportParams params = new ImportParams();
		///**
		// * 名称不能为空
		// */
		//params.setBeginRows(2);
		//params.setNeedSave(true);
		TSUser u = null ;
		TSUser buyerUser = null;//代客下单对应的采购商
		try {
			u = ResourceUtil.getSessionUserName();
			if(u == null ){//第二种获取方式
				u = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
			}
		} catch (Exception e) {
			
		}
		u = systemService.getEntity(TSUser.class, u.getId());
		
		List<OrderEntity> addlist = new ArrayList<OrderEntity>();// 对应的订单数据

		Map<String, Integer> orderNumber = new HashMap<String, Integer>();
		
		final List<XiadanProductVO> list = xiadanlist;
		for (XiadanProductVO e : list) {
			
			//代客下单
			if( "4".equals(u.getType()) && StringUtil.isNotEmpty(e.getUserBuyerId()) ){
				buyerUser = systemService.findUniqueByProperty(TSUser.class, "id", e.getUserBuyerId());
			}
			
			boolean add = true;
			
			if (StringUtils.isEmpty(e.getAddress())) {
				// 收货地址为空
				add = false;
				
			} else if (StringUtils.isEmpty(e.getCustomername())) {
				// 收货姓名为空
				add = false;
				
			} else if (StringUtils.isEmpty(e.getTelephone())) {
				// 收货联系方式
				add = false;
				
			} else if (StringUtils.isEmpty(e.getProductname())) {
				// 产品名称为空
				add = false;
				
			} else if (e.getNumber() < 1) {
				// 产品件数不匹配
				add = false;
				
			} else if (StringUtils.isEmpty(e.getCode())) {
				// 产品信息不存在
				add = false;
				
			}else if ( ("4".equals(u.getType()) && null == buyerUser)
					|| ("4".equals(u.getType()) && null != buyerUser && StringUtil.isEmpty(buyerUser.getId())) ){
				add = false;
			}
			
			if (add) {
				e.setAddress(StringUtil.replaceBlank(e.getAddress()));// 过滤地址自带换行符
				e.setCustomername(StringUtil.replaceBlank(e.getCustomername()));
				e.setTelephone(StringUtil.replaceBlank(e.getTelephone()));
				ProductEntity p = null;
				CriteriaQuery cq = new CriteriaQuery(ProductEntity.class);
				cq.eq("code", e.getCode());
				cq.eq("company", u.getCompany());
				cq.add();
				List<ProductEntity> productlist = systemService.getListByCriteriaQuery(cq, false);
				if (productlist != null && productlist.size() > 0) {
//					p = productlist.get(0);
					BeanUtils.copyProperties(productlist.get(0), p);//拷贝对象避免hibernate 自动提交更新
//					userGradePriceService.getProductFinalPrice(p);
				}

				// 产品名称
				if ((p != null && StringUtil.isNotEmpty(p.getId()))) {
					e.setSpecifications(p.getSpecifications());
					// e.setPrice(p.getPrice());//销售价格需要重新计算
					e.setCreatedate(null);
					// 创建对应的订单数据
					OrderEntity o = new OrderEntity();
					String province = "";// 对于省份信息
					try {
						if( StringUtil.isNotEmpty(e.getTSTerritory()) ){//系统提交的区域表id
							TSTerritory territory = new TSTerritory();
								territory.setId(e.getTSTerritory());
								o.setTSTerritory(territory);
								e.setTSTerritory(null);
						}
						MyBeanUtils.copyBeanNotNull2Bean(e, o);
						BigDecimal freightMoney = new BigDecimal("0.00"); // 运费价格
						try {
//							freightMoney = p.getFreightid().FreighToMap().get(o.getAddress().substring(0, 2));
//							province = p.getFreightid().territoryToMap().get(o.getAddress().substring(0, 2));
//							if (freightMoney == null) {
//								freightMoney = new BigDecimal("0.00");
//							}
							freightMoney = freightDetailsService.getProductFreightByFullAddr(p, o.getAddress(), o.getNumber());
							
						} catch (Exception ex) {
							freightMoney = new BigDecimal("0.00");
						}

						// 统计同类产品的数量，方便计算阶梯价格
						/*if (orderNumber.containsKey(p.getId())) {
							int t = orderNumber.get(p.getId()) + o.getNumber();
							orderNumber.put(p.getId(), t);
						} else {
							int t = o.getNumber();
							orderNumber.put(p.getId(), t);
						}*/
						

						freightMoney = freightMoney.multiply(new BigDecimal(e.getNumber()));
						o.setProductid(p);
						o.setFreightpic(freightMoney);
						o.setState("1");
						o.setPlatform(u.getRealname());
						o.setCreatedate(DateUtils.getDate());
						if("4".equals(u.getType())){//如果是代客 下单
							o.setClientid(buyerUser);
							o.setYewu(u);
						}else{
							o.setClientid(u);
							o.setYewu(u.getTsuserclients().getYewuid());
						}
						o.setProvince(province);
						o.setCompany(u.getCompany());
						addlist.add(o);
					} catch (Exception e1) {

					}
				}
			}

		}
		if (addlist != null && addlist.size() > 0) {
			BigDecimal money = new BigDecimal("0.00"); // 订单结算总价
			PayBillsOrderEntity payBillsOrder = new PayBillsOrderEntity();// 创建对应支付信息
			// 根据产品属性 计算产品阶梯价格

			for (int k = 0; k < addlist.size(); k++) {
				OrderEntity o = addlist.get(k);
				ProductEntity product = o.getProductid();
				BigDecimal productpice = product.getPrice().multiply(o.getNumber());// 产品预订价，单价X件数
				BigDecimal productpiceYw = product.getPriceyw().multiply(o.getNumber());// 产品预订价，单价X件数

//				if (orderNumber.containsKey(product.getId()) && product.getProductLadderList() != null && product.getProductLadderList().size() > 0) {// 存在量价体系
//					int number = orderNumber.get(product.getId());
//
//					for (ProductLadderEntity productLadder : product.getProductLadderList()) {
//						if (number >= productLadder.getLadderminorde()) {
//							productpice = productLadder.getPrice().multiply(new BigDecimal(o.getNumber()));// 产品预订价，单价X件数
//							productpiceYw = productLadder.getPriceyw().multiply(new BigDecimal(o.getNumber()));// 产品预订价，单价X件数
//						}
//					}
//				}
				o.setPrice(productpice);
				o.setPriceyw(productpiceYw);
				money = money.add(o.getFreightpic());
				money = money.add(productpice);
				addlist.set(k, o);// 修改对应的元素
			}

			payBillsOrder.setIdentifier(IdWorker.generateSequenceNo());
			payBillsOrder.setCompany(u.getCompany());
			if("4".equals(u.getType())){//如果是代客 下单
				payBillsOrder.setClientid(buyerUser);
				payBillsOrder.setYewu(u);
			}else{
				payBillsOrder.setClientid(u);
				payBillsOrder.setYewu(u.getTsuserclients().getYewuid());
			}
			
			payBillsOrder.setType("1");// 销售类型流水
			payBillsOrder.setState("1");// 待支付
			payBillsOrder.setFreightstatus("1");// 未发货
			payBillsOrder.setPayablemoney(money);
			payBillsOrder.setAlreadypaidmoney(new BigDecimal("0.00"));
			payBillsOrder.setObligationsmoney(money);
			if("4".equals(u.getType())){
				payBillsOrder.setIsSaleManOrder("1");//代客下单标志
			}
			Serializable id = systemService.save(payBillsOrder);
			payBillsOrder.setId(id.toString());

			for (int k = 0; k < addlist.size(); k++) {
				OrderEntity o = addlist.get(k);
				o.setBillsorderid(payBillsOrder);
				o.setIdentifieror(payBillsOrder.getIdentifier() + "_" + (k + 1));
				addlist.set(k, o);// 修改对应的元素
			}
			systemService.batchSave(addlist);// 订单数据写入数据库
		}
		
		return addlist;
	}
	
	
	
	/**
	* 公司产品销售数据分析
	* String companyid 公司ID
	* String startTime 开始时间
	* String endTime   截至时间
	*/
	public List<OrderEntity> salesCPList(String companyid, String startTime, String endTime) {
		
		
		String purchasecompanySQL ="";//公司统计
		if(StringUtil.isNotEmpty(companyid) && companyid.indexOf(",") > 0){
			purchasecompanySQL = " and  xnc_order.purchasecompanyid in("+companyid+")   ";
		}else if(StringUtil.isNotEmpty(companyid) ){
			purchasecompanySQL = " and  xnc_order.purchasecompanyid = '"+companyid+"' ";
		}

		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}
		String Sql =" SELECT  DATE_FORMAT( createDate, \"%Y-%m-%d\" ) as createDate ,  productname , SUM(Price) as Price , SUM(Number) as Number , SUM(PriceJ) as PriceJ , SUM(freightPic) as freightPic  FROM xnc_order  WHERE  xnc_order.state in(2,3,4)   " + purchasecompanySQL+ createDateSQL + "   GROUP BY   DATE_FORMAT( createDate, \"%Y-%m-%d\" ) ,  productId  ORDER BY  createDate asc  " ;
		List<OrderEntity>  list = queryListByJdbc(Sql,OrderEntity.class);	
		return list;
	}
	
	
	
	/**
	* 首页数据展示用  待发货单  今天已发货单   今天的销售单  昨天销售单  本月销售单  上月销售单
	* String companyid 公司ID
	* String startTime 开始时间
	* String endTime   截至时间
	*/
	public Map<String, OrderEntity> salesOPMap ( String companyid  ) {
		
		Map<String, OrderEntity> oPMap = new HashMap<String, OrderEntity>();
		
		String purchasecompanySQL ="";//公司统计
		if(StringUtil.isNotEmpty(companyid) && companyid.indexOf(",") > 0){
			purchasecompanySQL = " and  xnc_order.company in("+companyid+")   ";
		}else if(StringUtil.isNotEmpty(companyid) ){
			purchasecompanySQL = " and  xnc_order.company = '"+companyid+"' ";
		}
		String enddate = DateUtils.getDate("yyyy-MM-dd");//当前时间
		String endTime =enddate +" 23:59:59";
		String startTime =enddate +" 00:00:00";
		
		//String 	createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
				
		String DepartureDate= "  and xnc_order.DepartureDate >='"+startTime+" "+"' and xnc_order.DepartureDate <='"+endTime+"' ";
		
		
		//待付款
		String Sql_1 =" SELECT    IFNULL(SUM(Price),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 1   " + purchasecompanySQL   ;
		
		
		List<OrderEntity>  list_1 = queryListByJdbc(Sql_1,OrderEntity.class);	
		
		OrderEntity  o1 = new OrderEntity();//待付款订单
		o1.setNumber(new BigDecimal(0));
		if( list_1 != null && list_1.size() > 0){
			o1 = list_1.get(0);
		}
		oPMap.put("o1", o1);
		
		
		//待付款
		String Sql_2 =" SELECT    IFNULL(SUM(Price),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 2   " + purchasecompanySQL   ;
		List<OrderEntity>  list_2 = queryListByJdbc(Sql_2,OrderEntity.class);	
		OrderEntity  o2 = new OrderEntity();//待付款订单
		o2.setNumber(new BigDecimal(0));
		if( list_2 != null && list_2.size() > 0){
			o2 = list_2.get(0);
		}
		oPMap.put("o2", o2);
		
		
		//待付款
		String Sql_3 =" SELECT    IFNULL(SUM(Price),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 6   " + purchasecompanySQL   ;
		List<OrderEntity>  list_3 = queryListByJdbc(Sql_3,OrderEntity.class);	
		OrderEntity  o3 = new OrderEntity();//待付款订单
		o3.setNumber(new BigDecimal(0));
		if( list_3 != null && list_3.size() > 0){
			o3 = list_3.get(0);
		}
		oPMap.put("o3", o3);
		
		
		
		//今日订单
		
		String paydateSQL= "   xnc_order.paydate >='"+startTime+" "+"' and xnc_order.paydate <='"+endTime+"' ";
		
		String Sql_4 =" SELECT    IFNULL(SUM(Price),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE    "+paydateSQL + purchasecompanySQL   ;
		List<OrderEntity>  list_4 = queryListByJdbc(Sql_4,OrderEntity.class);	
		OrderEntity  o4 = new OrderEntity();//待付款订单
		o4.setNumber(new BigDecimal(0));
		if( list_4 != null && list_4.size() > 0){
			o4 = list_4.get(0);
		}
		oPMap.put("o4", o4);
		
		//今日发货

		String Sql_5 =" SELECT    IFNULL(SUM(Price),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 3    "+DepartureDate + purchasecompanySQL   ;
		List<OrderEntity>  list_5 = queryListByJdbc(Sql_5,OrderEntity.class);	
		OrderEntity  o5 = new OrderEntity();//待付款订单
		o5.setNumber(new BigDecimal(0));
		if( list_5 != null && list_5.size() > 0){
			o5 = list_5.get(0);
		}
		oPMap.put("o5", o5);
		
		
		//今日退单
		String canceldateSQL= "  and   xnc_order.canceldate >='"+startTime+" "+"' and xnc_order.canceldate <='"+endTime+"' ";
		
		
		String Sql_6 =" SELECT    IFNULL(SUM(Price),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 5    "+canceldateSQL + purchasecompanySQL   ;
		List<OrderEntity>  list_6 = queryListByJdbc(Sql_6,OrderEntity.class);	
		OrderEntity  o6 = new OrderEntity();//待付款订单
		o6.setNumber(new BigDecimal(0));
		if( list_6 != null && list_6.size() > 0){
			o6 = list_6.get(0);
		}
		oPMap.put("o6", o6);
		
		
		
		return oPMap;
		
		
		
	}



	@Override
	public AjaxJson orderGeneratorJson(HttpServletRequest request, List<XiadanProductVO> xiadanlist) throws Exception {
		AjaxJson json = new AjaxJson();
		json.setSuccess(true);//默认成功
		String msg = "订单生成成功！";
		List<OrderEntity> orderlist = orderGenerator(request, xiadanlist);
		
		int intDataNum = xiadanlist.size();
		int outDataNum = null != orderlist ? orderlist.size() : 0;
		if(outDataNum > 0 && outDataNum < intDataNum){
			msg = "共需要生成  " + intDataNum + " 条订单，其中成功 " + outDataNum + "条，失败 " + (intDataNum - outDataNum) + "条。";
		}
		if(outDataNum == 0){
			json.setSuccess(false);
			msg = "订单生成失败，请联系系统管理远！";
		}
		
		return json;
	}
	
	
	/**
	* 月销售统计
	* 
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/

	@Override
	public List<OrderEntity> salesTIC( String userCLid, String productId, String startTime, String endTime ,String companyId) {


		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}

		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		String Sql =" SELECT  DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" ) as createDate , SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " +productSQL+ userCLidSQL+ createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"'  GROUP BY   DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" )   ORDER BY  xnc_order.createDate asc  " ;

		List<OrderEntity>  list = queryListByJdbc(Sql,OrderEntity.class);	
		
		return list;
	}
	
	
	/**
	* 产品销售统计销售(按月统计量  ，金额)
	* 
	* String purchasecompanyid 公司ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesCPBL(String companyId, String userCLid  , String startTime, String endTime) {
		
		
		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		String createDateSQL ="";//日期
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		String Sql =" SELECT    productname ,  SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " + userCLidSQL+ createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"'    GROUP BY    xnc_order.productname  ORDER BY Price   desc  " ;
		
		List<OrderEntity>  list = queryListByJdbc(Sql,OrderEntity.class);	
		
		
		
		return list;
	}
	
	
	
	/**
	* 渠道销售统计销售情况
	* 
	* String productId 商品id
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesQDXS(String  companyId , String productId, String startTime, String endTime) {
		
		
		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}

		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		String Sql =" SELECT     xnc_pay_bills_order.clientId as id ,  SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " + productSQL + createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"'     GROUP BY     xnc_pay_bills_order.clientId    ORDER BY Price   desc " ;
		List<OrderEntity>  list = queryListByJdbc(Sql,OrderEntity.class);		
		return list;
	}

	/**
	* 用户消费区域分析
	* 
	* String userCLid 采购商id
	* String province 省份中文名称
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesYH( String companyId ,String userCLid ,String province, String startTime, String endTime) {
		
		
		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		
		String provinceSQL ="";//省份
		if(StringUtil.isNotEmpty(province) ){
			provinceSQL = " and  xnc_order.province = '"+province+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		String Sql =" SELECT  province , SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " +provinceSQL+ userCLidSQL+ createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"'    GROUP BY  province  ORDER BY Price   desc   " ;
		
		List<OrderEntity>  list = queryListByJdbc(Sql,OrderEntity.class);	
		
		return list;
	}

	/**
	* 业务情况统计
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/

	@Override
	public List<OrderEntity> salesYewu( String yewuid, String productId, String startTime, String endTime ,String companyId) {


		String userCLidSQL ="";//业务员
		if(StringUtil.isNotEmpty(yewuid) && yewuid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_order.yewu in("+yewuid+") ";
		}else if(StringUtil.isNotEmpty(yewuid) ){
			userCLidSQL = " and  xnc_order.yewu = '"+yewuid+"'   ";
		}

		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		String Sql =" SELECT  DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" ) as createDate , SUM(Price) as Price  , SUM(priceyw) as priceyw  , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order       WHERE  xnc_order.state in(2,3,4)   " +productSQL+ userCLidSQL+ createDateSQL + "  and  xnc_order.company = '"+companyId+"'  GROUP BY   DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" )   ORDER BY  xnc_order.createDate asc  " ;

		List<OrderEntity>  list = queryListByJdbc(Sql,OrderEntity.class);	
		
		return list;
	}
	
	/**
	* 产品发货状态图
	* 
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/

	@Override
	public List<OrderEntity> salesdeparture( String companyId ,String userCLid ,String productId, String startTime, String endTime) {
	

		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		
		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.departuredate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.departuredate >='"+startTime+"' and xnc_order.departuredate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.departuredate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.departuredate <='"+endTime+"' ";
		}

		String Sql =" SELECT  province , SUM(Price) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic  FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id    WHERE  xnc_order.state in(2,3,4)   " +productSQL+ userCLidSQL+ createDateSQL + "  and  xnc_pay_bills_order.company = '"+companyId+"'    GROUP BY  province  ORDER BY Price   desc   " ;
		
		List<OrderEntity>  list = queryListByJdbc(Sql,OrderEntity.class);	
		
		return list;

	}

	@Override
	public StatisPageVO statisCancelOrder(TSCompany company, HttpServletRequest request) {
		
		String state = request.getParameter("state");
		if(StringUtil.isEmpty(state)){
			state = "5,6,7";
		}
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("state", state);
		
		StatisPageVO statisPageVO = statisOrderDetails(company, param);
		
		return statisPageVO;
	}
	
	/**
	 * 订单统计共有函数
	 * @param company
	 * @param param 统计订单参数，请根据需要调整，目前支持的参数：
	 * state : 订单状态  identifieror : 订单号  customername ： 收件人 telephone ： 收件人电话  createdate1,createdate2 ： 时间范围, yewuid : 业务员id
	 * @return
	 */
	private StatisPageVO statisOrderDetails(TSCompany company, HashMap<String, String> param) {
		String state = param.get("state");
		String identifieror = param.get("identifieror");
		String customername = param.get("customername");
		String telephone = param.get("telephone");
		String createdate1 = param.get("createdate1");
		String createdate2 = param.get("createdate2");
		String yewuid = param.get("yewuid");//业务员id
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.price),0) value2 FROM xnc_order a,xnc_pay_bills_order b ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		Sql.append(" AND a.billsOrderID = b.id ");
		
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") > 0){
			
			String[] states = state.trim().split(",");
			StringBuffer tmp = new StringBuffer();
			
			for (int i = 0; i < states.length; i++) {
				if(tmp.length() == 0){
					tmp.append("'");
					tmp.append(states[i]);
					tmp.append("'");
				}else{
					tmp.append(",'");
					tmp.append(states[i]);
					tmp.append("'");
				}
			}
			Sql.append(" AND a.state in (" + tmp.toString() + ")");//全部
			
		}
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") == -1){
			
			Sql.append(" AND a.state = '" + state + "' ");//待审核和已审核
			
		}
		if(StringUtil.isNotEmpty(identifieror) 
				&& StringUtil.isNotEmpty(identifieror.trim())){
			
			Sql.append(" AND a.identifieror like '%" + identifieror.trim() + "%' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(customername) 
				&& StringUtil.isNotEmpty(customername.trim())){
			
			Sql.append(" AND a.CustomerName = '" + customername.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(telephone) 
				&& StringUtil.isNotEmpty(telephone.trim())){
			
			Sql.append(" AND a.Telephone = '" + telephone.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(yewuid) 
				&& StringUtil.isNotEmpty(yewuid.trim())){
			
			Sql.append(" AND a.yewu = '" + yewuid.trim() + "' ");//具体订单
			
		}

		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append(" AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append(" AND a.createDate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append(" AND a.createDate <= '" + createdate2 + "'" );
		}
		
		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		return statisPageVO;
	}

	/**
	 * 合作伙伴订单统计共有函数
	 * @param company
	 * @param param 统计订单参数，请根据需要调整，目前支持的参数：
	 * state : 订单状态  identifieror : 订单号  customername ： 收件人 telephone ： 收件人电话  createdate1,createdate2 ： 时间范围, yewuid : 业务员id
	 * @return
	 */
	private StatisPageVO statisCopartnerOrderSql(TSCompany company, HashMap<String, String> param) {
		String state = param.get("state");
		String identifieror = param.get("identifieror");
		String customername = param.get("customername");
		String telephone = param.get("telephone");
		String createdate1 = param.get("createdate1");
		String createdate2 = param.get("createdate2");
		String yewuid = param.get("yewuid");//业务员id
		String receiverid = param.get("receiverid");//订单受理商
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.pricec*a.number),0) value2 FROM xnc_order a,xnc_pay_bills_order b,xnc_order_send c ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		Sql.append(" AND a.id = c.orderid ");
		Sql.append(" AND a.billsOrderID = b.id ");
		Sql.append(" AND c.receiver= '" + receiverid + "' ");
		Sql.append(" AND c.status in ('1','2') ");
		
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") > 0){
			
			String[] states = state.trim().split(",");
			StringBuffer tmp = new StringBuffer();
			
			for (int i = 0; i < states.length; i++) {
				if(tmp.length() == 0){
					tmp.append("'");
					tmp.append(states[i]);
					tmp.append("'");
				}else{
					tmp.append(",'");
					tmp.append(states[i]);
					tmp.append("'");
				}
			}
			Sql.append(" AND a.state in (" + tmp.toString() + ")");//全部
			
		}
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") == -1){
			
			Sql.append(" AND a.state = '" + state + "' ");//待审核和已审核
			
		}
		if(StringUtil.isNotEmpty(identifieror) 
				&& StringUtil.isNotEmpty(identifieror.trim())){
			
			Sql.append(" AND a.identifieror like '%" + identifieror.trim() + "%' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(customername) 
				&& StringUtil.isNotEmpty(customername.trim())){
			
			Sql.append(" AND a.CustomerName = '" + customername.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(telephone) 
				&& StringUtil.isNotEmpty(telephone.trim())){
			
			Sql.append(" AND a.Telephone = '" + telephone.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(yewuid) 
				&& StringUtil.isNotEmpty(yewuid.trim())){
			
			Sql.append(" AND a.yewu = '" + yewuid.trim() + "' ");//具体订单
			
		}

		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append(" AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append(" AND a.createDate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append(" AND a.createDate <= '" + createdate2 + "'" );
		}
		
		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		return statisPageVO;
	}

	@Override
	public StatisPageVO statisOrderByState(TSCompany company, String orderState) {
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("state", orderState);
		
		StatisPageVO statisPageVO = statisOrderDetails(company, param);
		return statisPageVO;
	}

	@Override
	public StatisPageVO statisOrderByRequest(TSCompany company, HttpServletRequest request) {
		String ordertype = request.getParameter("ordertype");
		String state = request.getParameter("state");
		if(StringUtil.isNotEmpty(ordertype) && StringUtil.isNotEmpty(ordertype.trim())){
			state = ordertype.trim();
		}
		String identifieror = request.getParameter("identifieror");
		String customername = request.getParameter("customername");
		String telephone = request.getParameter("telephone");
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		String yewuid = request.getParameter("yewuid");//业务员id
		
		
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("state", state);
		param.put("identifieror", identifieror);
		param.put("customername", customername);
		param.put("telephone", telephone);
		param.put("createdate1", createdate1);
		param.put("createdate2", createdate2);
		param.put("yewuid", yewuid);
		
		StatisPageVO statisPageVO = statisOrderDetails(company, param);
		return statisPageVO;
	}

	@Override
	public StatisPageVO statisCopartnerOrderByState(TSCompany company, String orderState, TSUser receiver) {
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("state", orderState);
		param.put("receiverid", receiver.getId());
		
		StatisPageVO statisPageVO = statisCopartnerOrderSql(company, param);
		return statisPageVO;
	}
	
	@Override
	public StatisPageVO statisCopartnerOrders(TSCompany company, HttpServletRequest request, TSUser receiver) {
		String ordertype = request.getParameter("ordertype");
		String state = request.getParameter("state");
		if(StringUtil.isNotEmpty(ordertype) && StringUtil.isNotEmpty(ordertype.trim())){
			state = ordertype.trim();
		}
		String identifieror = request.getParameter("identifieror");
		String customername = request.getParameter("customername");
		String telephone = request.getParameter("telephone");
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		String yewuid = request.getParameter("yewuid");//业务员id
		
		
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("state", state);
		param.put("identifieror", identifieror);
		param.put("customername", customername);
		param.put("telephone", telephone);
		param.put("createdate1", createdate1);
		param.put("createdate2", createdate2);
		param.put("yewuid", yewuid);
		param.put("receiverid", receiver.getId());
		
		StatisPageVO statisPageVO = statisCopartnerOrderSql(company, param);
		return statisPageVO;
	}
	
	
	public List<OrderEntity> getOrderListByIds(String orderIds){
		List<OrderEntity> orderList = new ArrayList<OrderEntity>();
		
		if(StringUtil.isNotEmpty(orderIds)){
			String[] orderIdList = orderIds.split(",");
			
			CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
			cq.setCurPage(1);
			cq.setPageSize(30);
			cq.in("id", orderIdList);
			cq.add();
			
			orderList = getListByCriteriaQuery(cq, false);
		}
		
		
		return orderList;
	}



	@Override
	public AjaxJson orderSave(OrderEntity order, HttpServletRequest request) {
		String param = request.getAttribute("param") + "";
		AjaxJson j = new AjaxJson();
		String message = "";
		boolean success = true;
		if (StringUtil.isNotEmpty(order.getId())) {
			message = "订单明细更新成功";
			OrderEntity t = systemService.get(OrderEntity.class, order.getId());
			
			//初始化order，兼容触屏版的一项逻辑流程
			if(param!=null && param.equals("m")){
				if(t == null){
					j.setMsg("商品订单号有误");
					j.setSuccess(false);
					return j;
				}
				order.setAddress(t.getAddress());
				order.setClientid(t.getClientid());
				order.setIdentifieror(t.getIdentifieror());
				order.setLogisticscompany(t.getLogisticscompany());
				order.setLogisticsnumber(t.getLogisticsnumber());
				if(order.getPrice() == null){
					j.setMsg("价格有误");
					j.setSuccess(false);
					return j;
				}
				if(order.getFreightpic() == null){
					j.setMsg("运费有误");
					j.setSuccess(false);
					return j;
				}
				if(order.getNumber() == null){
					j.setMsg("数量有误");
					j.setSuccess(false);
					return j;
				}
				order.setProductname(t.getProductname());
				order.setRemarks(t.getRemarks());
				order.setState(t.getState());
				order.setTelephone(t.getTelephone());
			}
			
			PayBillsOrderEntity billsorderid = t.getBillsorderid();
			
			//只有大单未支付的情况（包括先货后款）才可以修改订单价格
			if( ("1".equals(t.getState()) || "2".equals(t.getState())) && "1".equals(billsorderid.getState()) ){
				String useraddress = request.getParameter("useraddress");
				if(StringUtil.isNotEmpty(useraddress)){
					order.setAddress(useraddress +"_"+ order.getAddress());
				}
				ProductEntity p = systemService.findUniqueByProperty (ProductEntity.class,"id" ,t.getProductid().getId());
				if( (StringUtil.isNotEmpty(order.getAddress()) && !order.getAddress().equals(t.getAddress()))
						|| order.getNumber().compareTo(t.getNumber()) !=0 ){
					//重新计算商品运费
					/**
					 * 通过比较页面传回的运费和原运费，判断运费是否人工干涉
					 * 1、如果页面传回的运费不等于元订单运费，则说明人工干涉，此时采用页面传回的运费
					 * 2、如果页面传回的运费和元订单运费相同，则说明没有人工干涉，此时重新计算系统运费
					 */
					if( order.getFreightpic().compareTo(t.getFreightpic()) ==0 ){
						BigDecimal freightMoney = new BigDecimal("0.00"); // 运费价格
						try {
							freightMoney = freightDetailsService.getProductFreightByFullAddr(p, order.getAddress(), order.getNumber());
							
						} catch (Exception ex) {
							freightMoney = new BigDecimal("0.00");
						}
						order.setFreightpic(freightMoney);//写入新的运费信息	
					}
				}
				//如果 发生价格变化 ，按输入的标准更新  计算修改后的价格，与原价格差，更新支付单
				BigDecimal picMoneyO = new BigDecimal(t.getPrice().add(t.getFreightpic()).doubleValue()); // 运费价格
				//页面传回订单价格和系统订单价格相同，说明人工并未干预商品价格
				if(order.getPrice().compareTo(t.getPrice()) ==0 && order.getNumber().compareTo(t.getNumber()) != 0){
					//计算用户等级价格
					TSUser client = systemService.findUniqueByProperty(TSUser.class, "id", order.getClientid().getId());
					BigDecimal productprice = userGradePriceService.getProductFinalPrice(p, client);
					order.setPrice( productprice.multiply(order.getNumber()));
				}
				//重新计算订单价格
				BigDecimal picMoneyN = new BigDecimal( order.getPrice().add(order.getFreightpic()).doubleValue() );
				
				if( picMoneyO.compareTo(picMoneyN) !=0 ){ //存在价格差
					//读取支付单修改支付价格
					if( picMoneyN.compareTo(picMoneyO) == 1){ //金额价格变大
						//变化金额
						BigDecimal picMoneyB = picMoneyN.subtract(picMoneyO);
						  //payablemoney   应付款 增加 
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().add(picMoneyB)) ;//
						  //obligationsmoney 待付金额 增加
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().add(picMoneyB));//
						
					}else if(  picMoneyN.compareTo(picMoneyO) == -1 ){//金额价格减小
						  //payablemoney   应付款 减少
						BigDecimal picMoneyB = picMoneyO.subtract(picMoneyN);
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().subtract(picMoneyB)) ;//
						  //obligationsmoney 待付金额  减少
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().subtract(picMoneyB));//
					}
					systemService.saveOrUpdate(billsorderid);
				}
			}
			
			
			
			try {
				
				boolean messageSend = false ;
				if(  "2".equals(t.getState()) &&  StringUtil.isNotEmpty(order.getLogisticscompany()) && StringUtil.isNotEmpty(order.getLogisticsnumber() )  ){//发货信息，状态为待发货   同时填写物流公司  物流单号   自动修改为  发货状态
					order.setState("3");
					order.setDeparturedate(DateUtils.getDate());
					
					messageSend = true;
				}
				
				if(messageSend  
						|| (StringUtil.isEmpty(t.getLogisticsnumber()) && StringUtil.isNotEmpty(order.getLogisticsnumber())) 
						|| (StringUtil.isNotEmpty(t.getLogisticsnumber()) && StringUtil.isNotEmpty(order.getLogisticsnumber()) && !t.getLogisticsnumber().equals(order.getLogisticsnumber())) ){
					//如果已经存在的信息则删除全部,重新录入
					List<OrderLogisticsEntity> orderLogisticsListDel = null ;
					try {
						orderLogisticsListDel = t.getOrderLogisticsList();
					} catch (Exception e) {
						
					}
					if( orderLogisticsListDel != null && orderLogisticsListDel.size() > 0){
						t.setOrderLogisticsList(null);
						for( OrderLogisticsEntity delol :  orderLogisticsListDel ){
							
							systemService.delete(delol);
						}
					}
				    String logisticsnumberS[] = order.getLogisticsnumber().trim().split(",");
				    List<OrderLogisticsEntity> orderLogisticsList = new ArrayList<OrderLogisticsEntity>();
					if( logisticsnumberS.length > 0 ){
						for(int i=0 ;i <logisticsnumberS.length ; i++){
							OrderLogisticsEntity ol = new OrderLogisticsEntity();
							ol.setOrderid(t);
							ol.setLogisticscompany(order.getLogisticscompany());
							ol.setLogisticsnumber(logisticsnumberS[i]);
							ol.setDeparturedate(order.getDeparturedate());
						 orderLogisticsList.add(ol);
						}
						if(orderLogisticsList.size() > 0){
							systemService.batchSave(orderLogisticsList);
						}
					}
					
				}
				//更新小单
				MyBeanUtils.copyBeanNotNull2Bean(order, t);
				systemService.saveOrUpdate(t);
				
				if(messageSend){
					billsorderid.setFreightstatus(payBillsOrderService.getPayBillsFreightStatus(billsorderid));
					systemService.saveOrUpdate(billsorderid);
					//发货通知下单客户
					messageTemplateService.mosSendSmsDelivery_6(t);
					//发货通知收货人
					messageTemplateService.mosSendSmsDelivery_5(t);
					salesmanCommissionService.SalesmanCommissionSave(t);
				}
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				success = false;
				message = "订单明细更新异常";
			}
		}else{
			j.setMsg("参数有误");
			j.setSuccess(false);
			return j;
		} 
//		else {
//			message = "订单明细添加成功";
//			systemService.save(order);
//			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
//		}
		j.setSuccess(success);
		j.setMsg(message);
		return j;
	}
}