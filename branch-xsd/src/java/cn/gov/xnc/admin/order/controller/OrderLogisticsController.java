package cn.gov.xnc.admin.order.controller;

import javax.servlet.http.HttpServletRequest;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.admin.kuaidi.service.Kuaid100ServiceI;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticsServiceI;

/**   
 * @Title: Controller
 * @Description: 物流单号列表
 * @author zero
 * @date 2016-11-17 16:59:09
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderLogisticsController")
public class OrderLogisticsController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderLogisticsController.class);
	
	
	@Autowired
	private Kuaid100ServiceI kuaid100ServiceI;
	@Autowired
	private OrderLogisticsServiceI orderLogisticsService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


//	/**
//	 * 物流单号列表列表 页面跳转
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "list")
//	public ModelAndView orderLogistics(HttpServletRequest request) {
//		return new ModelAndView("admin/order/orderLogisticsList");
//	}
//
//	/**
//	 * easyui AJAX请求数据
//	 * 
//	 * @param request
//	 * @param response
//	 * @param dataGrid
//	 * @param user
//	 */
//
//	@RequestMapping(params = "datagrid")
//	public void datagrid(OrderLogisticsEntity orderLogistics,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
//		CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class, dataGrid);
//		//查询条件组装器
//		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderLogistics, request.getParameterMap());
//		this.orderLogisticsService.getDataGridReturn(cq, true);
//		TagUtil.datagrid(response, dataGrid);
//	}
//
//	/**
//	 * 删除物流单号列表
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "del")
//	@ResponseBody
//	public AjaxJson del(OrderLogisticsEntity orderLogistics, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		orderLogistics = systemService.getEntity(OrderLogisticsEntity.class, orderLogistics.getId());
//		message = "物流单号列表删除成功";
//		orderLogisticsService.delete(orderLogistics);
//		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
//		
//		j.setMsg(message);
//		return j;
//	}
//
//
//	/**
//	 * 添加物流单号列表
//	 * 
//	 * @param ids
//	 * @return
//	 */
//	@RequestMapping(params = "save")
//	@ResponseBody
//	public AjaxJson save(OrderLogisticsEntity orderLogistics, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		if (StringUtil.isNotEmpty(orderLogistics.getId())) {
//			message = "物流单号列表更新成功";
//			OrderLogisticsEntity t = orderLogisticsService.get(OrderLogisticsEntity.class, orderLogistics.getId());
//			try {
//				MyBeanUtils.copyBeanNotNull2Bean(orderLogistics, t);
//				orderLogisticsService.saveOrUpdate(t);
//				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
//			} catch (Exception e) {
//				e.printStackTrace();
//				message = "物流单号列表更新失败";
//			}
//		} else {
//			message = "物流单号列表添加成功";
//			orderLogisticsService.save(orderLogistics);
//			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
//		}
//		j.setMsg(message);
//		return j;
//	}
//
//	/**
//	 * 物流单号列表列表页面跳转
//	 * 
//	 * @return
//	 */
//	@RequestMapping(params = "addorupdate")
//	public ModelAndView addorupdate(OrderLogisticsEntity orderLogistics, HttpServletRequest req) {
//		if (StringUtil.isNotEmpty(orderLogistics.getId())) {
//			orderLogistics = orderLogisticsService.getEntity(OrderLogisticsEntity.class, orderLogistics.getId());
//			req.setAttribute("orderLogisticsPage", orderLogistics);
//		}
//		return new ModelAndView("admin/order/orderLogistics");
//	}
	
	
	/**
	 * 快递物流查询
	 * 
	 * @return
	 */
	@RequestMapping(params = "kuaidi")
	public String kuaidi(OrderLogisticsEntity orderLogistics,HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(orderLogistics.getId())) {
			orderLogistics = systemService.getEntity(OrderLogisticsEntity.class, orderLogistics.getId());
			
			req.setAttribute("orderid", orderLogistics.getOrderid().getId());
		}
		
		return "express/expresslist";
	}
	
	
}
