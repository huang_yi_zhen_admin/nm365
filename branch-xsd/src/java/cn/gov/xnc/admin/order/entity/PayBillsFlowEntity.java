package cn.gov.xnc.admin.order.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;


/**   
 * @Title: Entity
 * @Description: 支付流水订单对应关系
 * @author zero
 * @date 2016-10-05 16:33:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_pay_bills_flow", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class PayBillsFlowEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**支付流水id*/
	private PayBillsEntity billsid;
	/**订单id*/
	private PayBillsOrderEntity billsorderid;
	
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  支付流水id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BILLSID")
	public PayBillsEntity getBillsid(){
		return this.billsid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  支付流水id
	 */
	public void setBillsid(PayBillsEntity billsid){
		this.billsid = billsid;
	}
	
	
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BILLSORDERID")
	public PayBillsOrderEntity getBillsorderid(){
		return this.billsorderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单id
	 */
	public void setBillsorderid(PayBillsOrderEntity billsorderid){
		this.billsorderid = billsorderid;
	}
}
