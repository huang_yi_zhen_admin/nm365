package cn.gov.xnc.admin.order.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsFlowEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;
import cn.gov.xnc.admin.zhifu.alipay.config.AlipayConfig;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.CompanyThirdpartyEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.impl.SystemServiceImpl;


@Service("payBillsService")

public class PayBillsServiceImpl extends CommonServiceImpl implements PayBillsServiceI {

	/**
	 *创建支付信息 
	 * 
	 */
	@Autowired
	private MessageTemplateServiceI messageTemplateService ;
	
	@Autowired
	private SystemService systemService;
	
	
	public PayBillsEntity payment( String idS ,  TSUser user , PayBillsEntity payment) {
		
		//创建一条支付记录  payBills
		//PayBillsEntity payBills = new PayBillsEntity();
		
		
		
		if(   StringUtil.isNotEmpty(idS) ){
			
			
			String payName="";
			
			String ids[] = idS.split(",");
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
				cq.in("id", ids);//产品编码作为唯一编码
				cq.eq("company", user.getCompany());//公司信息
				cq.add();	
			List<PayBillsOrderEntity> payBillsOrderlist = getListByCriteriaQuery(cq,false);
			BigDecimal moneyPage = new BigDecimal("0.00");
			if( payBillsOrderlist != null && payBillsOrderlist.size() > 0 ){
				List<PayBillsFlowEntity> addlist = new ArrayList<PayBillsFlowEntity>();//对应的订单数据	
				for( PayBillsOrderEntity p: payBillsOrderlist ){
					PayBillsFlowEntity payBillsFlow = new PayBillsFlowEntity();
					try {
						List<OrderEntity> orderS  = p.getOrderS();
						for( OrderEntity o: orderS ){
							payName += o.getProductname()+",";
						}
					} catch (Exception e) {
						
					}

						payBillsFlow.setBillsorderid(p);
						addlist.add(payBillsFlow);
					moneyPage =moneyPage.add(p.getObligationsmoney());
					payment.setAlreadypaidmoney(moneyPage);
					payment.setClientid(p.getClientid());
				}
				Serializable id = save(payment);
				payment.setId((String) id);
				for( int k = 0 ; k <addlist.size() ; k++ ){
					PayBillsFlowEntity pBillsFlow  =   addlist.get(k);
						pBillsFlow.setBillsid(payment);
						pBillsFlow.setBillsorderid(pBillsFlow.getBillsorderid());
					addlist.set(k,pBillsFlow);//修改对应的元素
				}
				batchSave(addlist);
			}
			
			
			
			if( payName == null || "".equals(payName)){
				payName ="购买商品";
			}else if( StringUtil.isNotEmpty(payName)  && payName.length() > 20 ){
				payName = payName.substring(0,payName.length()-1);//去除最后的标点
				payName = payName.substring(0,19) +"..";
			}
			
			payment.setPayName(payName);
		}
		return payment;
	}

	
	public PayBillsEntity paymentUser( PayBillsEntity payment  ) {
		//创建一条个人账号充值记录

		Serializable id = save(payment);
			payment.setId((String) id);
	
		return payment;
	}
	
	
	/**
	 *支付完成后修改订单状态，更新订单信息 
	 */
	
	public  boolean  saveState( TSUser user ,PayBillsEntity payBills) {
		
		if (StringUtil.isNotEmpty(payBills.getId())) {
			PayBillsEntity t = get(PayBillsEntity.class, payBills.getId());
			
			if( "1".equals(t.getType()) ){
				BigDecimal totalPay = t.getAlreadypaidmoney();
				if( "1".equals(payBills.getState())  || "4".equals(payBills.getState()) || "5".equals(payBills.getState())  ){//由审核状态修改为 确定
					TSUser clientid = t.getClientid();
					List<PayBillsFlowEntity> payBillsFlowS = t.getPayBillsFlowS();
					if( payBillsFlowS != null && payBillsFlowS.size() > 0){
						for( int i=0 ; i<payBillsFlowS.size() ; i++ ){
							 PayBillsOrderEntity payBillsOrder  =payBillsFlowS.get(i).getBillsorderid();
							 
							 if( !"2".equals(payBillsOrder.getState())  ){
								 BigDecimal obligationsmoney = payBillsOrder.getObligationsmoney();
								 //无欠款（不是最后一笔单）：应付款 <= 付款总额 && 付款总额 >= 0 && 
								 if( obligationsmoney.compareTo(totalPay) <= 0 
										 && totalPay.compareTo(new BigDecimal("0.00")) >= 0 
										 && i != payBillsFlowS.size() - 1){
									 payBillsOrder.setAlreadypaidmoney(obligationsmoney);
									 payBillsOrder.setObligationsmoney(new BigDecimal("0.00"));
									 
								//无欠款（付完最后一笔，付款总额还有剩余）：应付款 <= 付款总额 && 付款总额 > 0 
								 }else if(obligationsmoney.compareTo(totalPay) <= 0 && totalPay.compareTo(new BigDecimal("0.00")) == 1 
										 && i == payBillsFlowS.size() - 1){//最后一笔付款总额还有剩余
									 payBillsOrder.setAlreadypaidmoney(obligationsmoney);
									 payBillsOrder.setObligationsmoney(obligationsmoney.subtract(totalPay));
									 	 
								 //应付款（有欠款）  > 付款总额  && 付款总额 > 0
								 }else if(obligationsmoney.compareTo(totalPay) == 1 && totalPay.compareTo(new BigDecimal("0.00")) == 1 ){
									 payBillsOrder.setAlreadypaidmoney(totalPay);
									 payBillsOrder.setObligationsmoney(obligationsmoney.subtract(totalPay));
								
								//其它情况视为没钱支付，待付金额不变，已付金额0元
								 }else{
									 payBillsOrder.setAlreadypaidmoney(new BigDecimal("0.00"));
								 }
								 
								 totalPay = totalPay.subtract(obligationsmoney);
								 
								 
								 //payBillsOrder.setAlreadypaidmoney(payBillsOrder.getAlreadypaidmoney().add(t.getAlreadypaidmoney()));
								 //payBillsOrder.setObligationsmoney(payBillsOrder.getPayablemoney().subtract(payBillsOrder.getAlreadypaidmoney()));
								 
								 
								 
								 if( payBillsOrder.getObligationsmoney().compareTo(new BigDecimal("0.00")) < 1 ){
									 payBillsOrder.setState("2");
								 }else  if  ( payBillsOrder.getObligationsmoney().compareTo(new BigDecimal("0.00")) == 1  
										    &&  payBillsOrder.getObligationsmoney().compareTo(payBillsOrder.getPayablemoney()) == 0   ) {
									 payBillsOrder.setState("1");
								 }else   {
									 payBillsOrder.setState("3");
								 } 
								 
								 //当付款总额出现负数，说明用户账户余额已经不够支付订单，可能是人为审核通过（如财务）
								 //如果用户账户余额不为零，此时需要重新更新用户账户余额置 0，保证支出平衡
								 if(totalPay.compareTo(new BigDecimal("0.00")) == -1 
										 && clientid.getTsuserSalesman().getUsemoney().compareTo(new BigDecimal("0.00")) == 1){
									 
									 clientid.getTsuserSalesman().setUsemoney(new BigDecimal("0.00"));
								 }
								 //更新支付单的信息 支付信息 账户信息
								 payBillsOrder.setPaymentmethod(t.getPaymentmethod());
								 payBillsOrder.setBalancemoney(clientid.getTsuserSalesman().getUsemoney());
								 
								 
								 
								  saveOrUpdate(payBillsOrder);//更新支付状态
								  
								  if( "2".equals(payBillsOrder.getState()) ||  "3".equals(payBillsOrder.getState())){
										//付款待发货通知订单员
										messageTemplateService.mosSendSmsDelivery_2(payBillsOrder);
										//付款待发货通知业务员
										messageTemplateService.mosSendSmsDelivery_3(payBillsOrder);
										//现货后款通过通知客户
										messageTemplateService.mosSendSmsDelivery_4(payBillsOrder);
										
										//付款后计算业务提成流水
										
										
									 }
								  
							 }
							 
							  
							  if(  "2".equals(payBillsOrder.getState())){//更新对于的订单修改为待发货状态
								  List<OrderEntity> orderS = payBillsOrder.getOrderS();
								  if( orderS != null && orderS.size() > 0){
									  for( OrderEntity o : orderS){
										  if( "1".equals(o.getState())){
											  o.setState("2");
											  o.setPaydate(DateUtils.getDate());
											  saveOrUpdate(o);//更新订单为发货状态
										  } 
									  }
								  } 
							  }
						}
						
						 //重新更新用户账户余额,保证支出平衡
						UserSalesmanEntity userSaleMan = systemService.findUniqueByProperty(UserSalesmanEntity.class, "id", clientid.getTsuserSalesman().getId());
						userSaleMan.setUsemoney(clientid.getTsuserSalesman().getUsemoney());
						systemService.updateEntitie(userSaleMan);
					}
					//更新支付流水状态
					if( "2".equals(t.getState()) || "3".equals(t.getState()) ){
						t.setState(payBills.getState());
						if(StringUtil.isNotEmpty(payBills.getVoucher()) ){
							t.setVoucher(payBills.getVoucher());
						}
						t.setAuditdate(DateUtils.getDate());
						if( user != null && StringUtil.isNotEmpty(user.getId()) ){
							t.setAudituser(user);
						}
						saveOrUpdate(t);//更新支付状态
					}
					
				}
			}else if(  "2".equals(t.getType()) ){//充值类型
				  
					//更新支付流水状态
					if( "2".equals(t.getState()) || "3".equals(t.getState()) || "5".equals(t.getState())  ){
						t.setState(payBills.getState());
						if(StringUtil.isNotEmpty(payBills.getVoucher()) ){
							t.setVoucher(payBills.getVoucher());
						}
						t.setAuditdate(DateUtils.getDate());
						if( user != null && StringUtil.isNotEmpty(user.getId()) ){
							t.setAudituser(user);
						}
						UserSalesmanEntity  userSalesman  = get(UserSalesmanEntity.class, t.getClientid().getTsuserSalesman().getId());
						if( "1".equals( payBills.getState() ) || "4".equals( payBills.getState() )){
							//把对于支付金额
							userSalesman.setUsemoney(userSalesman.getUsemoney().add( t.getAlreadypaidmoney() ));
							saveOrUpdate(userSalesman);//更新支付状态
						}	
						saveOrUpdate(t);//更新支付状态
					}
					
					
				}
		}
		return true;
	}
	
	/**
	 *根据订单id 支付单id 获得当前公司的支付宝对应验证信息
	 *@param  支付单类型PayBillsEntity
	 */
	public AlipayConfig getAlipayConfig(PayBillsEntity payBills ){
		
		//根据out_trade_no  构建公司原来的支付信息
		AlipayConfig alipayConfig = new AlipayConfig();
				 
		boolean alipay =true ;
		if ( StringUtil.isNotEmpty( payBills.getId() ) ){
			//SystemService SystemService =  (SystemService) ApplicationContextUtil.getContext().getBean("systemService"); 
			//PayBillsEntity payBills= SystemService.getEntity(PayBillsEntity.class, payBillsId);
			try {
				//获取访问域名
				 CompanyThirdpartyEntity  companyThirdparty = payBills.getCompany().getCompanythirdparty();
				 CompanyPlatformEntity companyplatform = payBills.getCompany().getCompanyplatform();
			if(  alipay && companyThirdparty != null && StringUtil.isNotEmpty(companyThirdparty.getAlipayid()) ){
				alipayConfig.setPartner(companyThirdparty.getAlipayid());
				alipayConfig.setSeller_id(companyThirdparty.getAlipayid());
			}else{
				alipay = false ;
			}
						 	
			if( alipay && companyThirdparty != null && StringUtil.isNotEmpty(companyThirdparty.getAlipaykey()) ){
				alipayConfig.setKey(companyThirdparty.getAlipaykey());
				}else{
				alipay = false ;
			}
			
			String url =  "";
			
			if ( StringUtil.isNotEmpty( companyplatform.getWebsite() ) ){
				url= companyplatform.getWebsite();
			}else {
				url ="http://www.nongnao365.com" ;
			}
			
			
			
			String notify_url =url+"/alipayaController/notifyAlipaya.do"; 
			String return_url =url+"/webpage/admin/alipay/return_url.jsp";
				alipayConfig.setNotify_url(notify_url);
				alipayConfig.setReturn_url(return_url);
				alipayConfig.setAlipay(alipay);
			} catch (Exception e) {
				alipay = false ;
			}
			
		}
		
		
		return alipayConfig;

	}


	/**
	 * 线下转账支付
	 */
	public AjaxJson saveOfflinePayBills(PayBillsEntity payBills,  String paymentmethod , HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String message = null;
		String type = payBills.getType();
		TSUser user  = ResourceUtil.getSessionUserName();
		if (StringUtil.isNotEmpty(payBills.getId()) && "1".equals(type) ) {//线下支付购买
			String idS = payBills.getId();
	        PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
		        payment.setIdentifier(IdWorker.generateSequenceNo());
		        payment.setCompany(user.getCompany());
		        payment.setType(type);
		        payment.setState(PayBillsServiceI.State.PASS.getValue());
		        payment.setPaymentmethod(paymentmethod);
		        payment.setRemark(payBills.getRemark());
		        payment.setAlreadypaidmoney(payBills.getAlreadypaidmoney());
				payment.setBank(payBills.getBank());
				payment.setBankAccount(payBills.getBankAccount());
				payment.setBankDate(payBills.getBankDate());
				payment.setRemark(payBills.getRemark());
				payment.setBankUrl(payBills.getBankUrl());
				
		        if( StringUtil.isNotEmpty(idS) ){
					String ids[] = idS.split(",");
					CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
						cq.in("id", ids);//产品编码作为唯一编码
						cq.eq("company", user.getCompany());//公司信息
						cq.add();
					List<PayBillsOrderEntity> payBillsOrderlist = getListByCriteriaQuery(cq,false);
					BigDecimal money2paid = new BigDecimal("0.00");
					if( payBillsOrderlist != null && payBillsOrderlist.size() > 0 ){
						List<PayBillsFlowEntity> addlist = new ArrayList<PayBillsFlowEntity>();//对应的订单数据	
						for( PayBillsOrderEntity p: payBillsOrderlist ){
							PayBillsFlowEntity payBillsFlow = new PayBillsFlowEntity();
								payBillsFlow.setBillsorderid(p);
								addlist.add(payBillsFlow);
								
							money2paid = money2paid.add( p.getPayablemoney() );
							if(null == payment.getClientid()){
								payment.setClientid(p.getClientid());
							}
							
						}
						
						payment.setPayablemoney(money2paid);
						
						Serializable id = save(payment);
						payment.setId((String) id);
						for( int k = 0 ; k <addlist.size() ; k++ ){
							PayBillsFlowEntity pBillsFlow  =   addlist.get(k);
								pBillsFlow.setBillsid(payment);
								pBillsFlow.setBillsorderid(pBillsFlow.getBillsorderid());
							addlist.set(k,pBillsFlow);//修改对应的元素
						}
						batchSave(addlist);
					}
				}    
		        
		    saveState(user, payment);  
	        //payment  = payBillsService.payment(idS, user, payment);
			//payBillsService.save(payBills);
		    message = "线下转账成功，恭喜您又完成了一笔交易！！";
		    
			j.setSuccess(true);
			j.setMsg(message);
		}else if( "2".equals(type) ){ //线下充值
			 PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
		        payment.setIdentifier(IdWorker.generateSequenceNo());
		        payment.setCompany(user.getCompany());
		        payment.setType(type);
		        payment.setState("2");
		        payment.setPaymentmethod("3");
		        payment.setRemark(payBills.getRemark());
		        payment.setClientid(user);
		        payment.setAlreadypaidmoney(payBills.getAlreadypaidmoney());
				payment.setBank(payBills.getBank());
				payment.setBankAccount(payBills.getBankAccount());
				payment.setBankDate(payBills.getBankDate());
				payment.setRemark(payBills.getRemark());
				payment.setBankUrl(payBills.getBankUrl());
				
				save(payment);  
		}else{
			
			 message = "提交订单异常，请联系管理人员！";
			j.setSuccess(false);
			j.setMsg(message);
		}
		
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		return j;
	}
	
	
	
	
	/**
	 * 账户金额支付
	 */
	public AjaxJson saveUsemoneyPayBills(PayBillsEntity payBills,  String paymentmethod , HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String message = null;
		String type = payBills.getType();
		TSUser user  = ResourceUtil.getSessionUserName();
		
		if (StringUtil.isNotEmpty(payBills.getId()) && "1".equals(type) ) {//线下支付购买
			String idS = payBills.getId();
			
			
			//创建支付单
	        PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
		        payment.setIdentifier(IdWorker.generateSequenceNo());
		        payment.setCompany(user.getCompany());
		        payment.setType(type);
		        payment.setState("4");
		        payment.setPaymentmethod(paymentmethod);
		        payment.setRemark(payBills.getRemark());
		        payment.setRemark(payBills.getRemark());
		        
		        if( StringUtil.isNotEmpty(idS) ){
					String ids[] = idS.split(",");
					CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
						cq.in("id", ids);//产品编码作为唯一编码
						cq.eq("company", user.getCompany());//公司信息
						cq.add();
					List<PayBillsOrderEntity> payBillsOrderlist = getListByCriteriaQuery(cq,false);
					BigDecimal moneyPage = new BigDecimal("0.00");
					if( payBillsOrderlist != null && payBillsOrderlist.size() > 0 ){
						List<PayBillsFlowEntity> addlist = new ArrayList<PayBillsFlowEntity>();//对应的订单数据	
						for( PayBillsOrderEntity p: payBillsOrderlist ){
							PayBillsFlowEntity payBillsFlow = new PayBillsFlowEntity();
								payBillsFlow.setBillsorderid(p);
								addlist.add(payBillsFlow);
								moneyPage =moneyPage.add(p.getObligationsmoney());
							payment.setClientid(p.getClientid());
						}
						
						
						
						
						payment.setAlreadypaidmoney(moneyPage);//购买支付价格
						
						//计算用户的账户金额是否满足支付
						UserSalesmanEntity  userSalesman  = systemService.get(UserSalesmanEntity.class, user.getTsuserSalesman().getId());
						 
						BigDecimal moneyp = userSalesman.getUsemoney();//账户余额
					 	 
						if(  moneyp.compareTo( moneyPage ) > -1    ){  //账户是否足够支付
							moneyp = moneyp.subtract(moneyPage);
							userSalesman.setUsemoney(moneyp);//更新个人账户信息 
							saveOrUpdate(userSalesman);
							
							
							payment.setBalancemoney(moneyp);//记录账户余额
							
							Serializable id = save(payment);
							payment.setId((String) id);
							for( int k = 0 ; k <addlist.size() ; k++ ){
								PayBillsFlowEntity pBillsFlow  =   addlist.get(k);
									pBillsFlow.setBillsid(payment);
									pBillsFlow.setBillsorderid(pBillsFlow.getBillsorderid());
								addlist.set(k,pBillsFlow);//修改对应的元素
							}
							batchSave(addlist);
							
							//修改发货数据
							saveState(null,payment);
							
							
							
						}else {
							 message = "余额不足，支付失败！";
							 j.setSuccess(true);
							 j.setMsg(message);
							 return j;
						}
					}
				}      
	        //payment  = payBillsService.payment(idS, user, payment);
			//payBillsService.save(payBills);
		    message = "支付成功，请等待发货处理！";
		    
			j.setSuccess(true);
			j.setMsg(message);
		}else{
			
			 message = "提交订单异常，请联系管理人员！";
			j.setSuccess(false);
			j.setMsg(message);
		}
		
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		return j;
	}


	@Override
	public StatisPageVO statisPaybills(TSCompany company, HttpServletRequest request) {
		String state = request.getParameter("state");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.alreadyPaidMoney),0) value2 FROM xnc_pay_bills a ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") > 0){
			
			String[] states = state.trim().split(",");
			StringBuffer tmp = new StringBuffer();
			
			for (int i = 0; i < states.length; i++) {
				if(tmp.length() == 0){
					tmp.append("'");
					tmp.append(states[i]);
					tmp.append("'");
				}else{
					tmp.append(",'");
					tmp.append(states[i]);
					tmp.append("'");
				}
			}
			Sql.append(" AND a.state in (" + tmp.toString() + ")");//全部
			
		}else if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") == -1){
			
			Sql.append(" AND a.state = '" + state + "' ");//待审核和已审核
			
		}else{
			Sql.append(" AND a.state in ( '1','2','3') ");//默认全部
		}

		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		return statisPageVO;
	}
	
	
}