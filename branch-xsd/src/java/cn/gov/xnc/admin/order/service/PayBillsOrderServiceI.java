package cn.gov.xnc.admin.order.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface PayBillsOrderServiceI extends CommonService{
	//订单状态枚举
	public enum  State{
		UNPAID("待付", "1"), PAID("已付", "2"), NOTALLPAID("未付清", "3"), CANCEL("已取消", "4"), REBACK("已退单", "5");
	    
		private String name ;
	    private String value ;
	     
	    private State( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
	
	//订单货运状态枚举
	public enum  FreightStatus{
		UNDELIVERED("待发货", "1"), DELIVERED("已发货", "2"), NOTALLDELIVERED("部分发货", "3"), WAIT2PACK("等待打包", "4"),  STOCKOUT("已出库","5"), RECEIVED("已签收","6");

		private String name ;
	    private String value ;
	     
	    private FreightStatus( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
	
	//批量创建产品订单list
	
	/**
	 * 获取采购商支付流水统计数据
	 * @param company
	 * @param user
	 * @return
	 */
	public StatisPageVO statisUserPaybills(TSCompany company, TSUser user, HttpServletRequest request);
	
	/**
	 * 根据订单id获取订单列表
	 * @param billsIds
	 * @return
	 */
	public List<PayBillsOrderEntity> getBillsOrderListByIds(String billsIds);
	
	/**
	 * 先货后款
	 * @param payBillsOrder
	 * @param request
	 * @return
	 */
	public AjaxJson sendFirstPaymentAudit(PayBillsOrderEntity payBillsOrder, HttpServletRequest request);
	
	/**
	 * 获取订单物流状态
	 * @param paybillsOrder
	 * @return
	 */
	public String getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder);
	
	
	public AjaxJson cancelPayBillsOrder(PayBillsOrderEntity paybillsOrder, HttpServletRequest request) throws Exception;
}
