package cn.gov.xnc.admin.order.controller;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.CompanyThirdpartyEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 收支流水
 * @author zero
 * @date 2016-10-02 02:02:44
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/payBillsController")
public class PayBillsController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PayBillsController.class);

	@Autowired
	private PayBillsServiceI payBillsService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 收支流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView payBills(HttpServletRequest request) {
		//增加产品列表查询
		String payBills_id = request.getParameter("payBillsId");
		request.setAttribute("payBills_id", payBills_id);
		return new ModelAndView("admin/order/payBillsList");
	}
	
	/**
	 * 收支流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "payBillsAdminList")
	public ModelAndView payBillsAdminList(HttpServletRequest request) {
		return new ModelAndView("admin/order/payBillsAdminList");
	}
	

	
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(PayBillsEntity payBills,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(PayBillsEntity.class, dataGrid);
		//查询条件组装器
		if(!"1".equals(user.getType()) && !"2".equals(user.getType())){
			cq.eq("clientid", user);
		}
		//时间
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		cq.addOrder("createdate", SortDirection.desc);
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, payBills, request.getParameterMap());
		this.payBillsService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	
	/**
	 * 给管理员用，可以看到所有人的支付单
	 * @param payBills
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(value = "adminDatagrid")
	public void adminDatagrid(PayBillsEntity payBills,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		CriteriaQuery cq = new CriteriaQuery(PayBillsEntity.class, dataGrid);
		//查询条件组装器
		cq.addOrder("createdate", SortDirection.desc);
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, payBills, request.getParameterMap());
		this.payBillsService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 删除收支流水
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(PayBillsEntity payBills, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		payBills = systemService.getEntity(PayBillsEntity.class, payBills.getId());
		message = "收支流水删除成功";
		payBillsService.delete(payBills);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 审核支付支付通过
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(PayBillsEntity payBills, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		if (StringUtil.isNotEmpty(payBills.getId())) {
			message = "收支流水审核修改成功！";
			try {
				payBillsService.saveState(user,payBills);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "收支流水审核修改失败";
			}
		} else {
			
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 收支流水列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(PayBillsEntity payBills, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(payBills.getId())) {
			payBills = payBillsService.getEntity(PayBillsEntity.class, payBills.getId());
			req.setAttribute("payBillsPage", payBills);
		}
		return new ModelAndView("admin/order/payBills");
	}
	
	
	/**
	 * 线下支付跳转页
	 * 
	 * @return
	 */
	@RequestMapping(value = "offlinePayment")
	public ModelAndView offlinePayment( HttpServletRequest req) {
		TSUser user  = ResourceUtil.getSessionUserName();
		String idS = req.getParameter("id");
		UserSalesmanEntity userSaleMan = user.getTsuserSalesman();
		if(StringUtil.isNotEmpty(userSaleMan.getId())){
			userSaleMan = systemService.findUniqueByProperty(UserSalesmanEntity.class, "id", userSaleMan.getId());
		}
		String type = req.getParameter("type");
		if( "2".equals(type)){
			 PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
		        payment.setIdentifier(IdWorker.generateSequenceNo());
		        payment.setClientid(user);
		        payment.setCompany(user.getCompany());
		      
		        payment.setType(type);
		        payment.setState("5");
		        payment.setPaymentmethod("3");
		    	String paymoney = req.getParameter("paymoney");
		    	BigDecimal moneyPage = new BigDecimal(paymoney);
				payment.setAlreadypaidmoney(moneyPage);

			
			CompanyThirdpartyEntity companyThirdparty = systemService.get(CompanyThirdpartyEntity.class,user.getCompany().getCompanythirdparty().getId());
			
			req.setAttribute("companyThirdpartyPage", companyThirdparty);
			req.setAttribute("idsPage", payment.getId());
			req.setAttribute("identifierPage", payment.getIdentifier());
			req.setAttribute("type", "2");
			req.setAttribute("moneyPage", moneyPage);
			req.setAttribute("userSaleMan", userSaleMan);
			
			
		}else  if( "1".equals(type) && StringUtil.isNotEmpty(idS) ){
			String ids[] = idS.split(",");
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
				cq.in("id", ids);//产品编码作为唯一编码
				cq.eq("company", user.getCompany());//公司信息
				cq.add();	
			List<PayBillsOrderEntity> payBillsOrderlist = systemService.getListByCriteriaQuery(cq,false);
			String idsPage="";
			String identifierPage="";
			BigDecimal moneyPage = new BigDecimal("0.00");
			if( payBillsOrderlist != null && payBillsOrderlist.size() > 0 ){
				for( PayBillsOrderEntity p: payBillsOrderlist){
					idsPage +=p.getId()+",";
					identifierPage += p.getIdentifier()+ ",";
					moneyPage =moneyPage.add(p.getObligationsmoney());
				}
			}

			
			CompanyThirdpartyEntity companyThirdparty = systemService.get(CompanyThirdpartyEntity.class,user.getCompany().getCompanythirdparty().getId());
			
			req.setAttribute("companyThirdpartyPage", companyThirdparty);
			req.setAttribute("idsPage", idsPage);
			req.setAttribute("type", "1");
			req.setAttribute("identifierPage", identifierPage);
			req.setAttribute("moneyPage", moneyPage);
			req.setAttribute("userSaleMan", userSaleMan);
		}

		return new ModelAndView("cash/clientCashIn");
	}
	
	/**
	 * 线下支付跳转页
	 * 
	 * @return
	 */
	@RequestMapping(value = "jsonOfflinePayment")
	@ResponseBody
	public AjaxJson jsonOfflinePayment( HttpServletRequest req) {
		AjaxJson json = new AjaxJson();
		
		TSUser user  = ResourceUtil.getSessionUserName();
		String idS = req.getParameter("id");
		
		String type = req.getParameter("type");
		if( "2".equals(type)){
			 PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
		        payment.setIdentifier(IdWorker.generateSequenceNo());
		        payment.setClientid(user);
		        payment.setCompany(user.getCompany());
		      
		        payment.setType(type);
		        payment.setState("5");
		        payment.setPaymentmethod("3");
		    	String paymoney = req.getParameter("paymoney");
		    	BigDecimal moneyPage = new BigDecimal(paymoney);
				payment.setAlreadypaidmoney(moneyPage);

			
			CompanyThirdpartyEntity companyThirdparty = systemService.get(CompanyThirdpartyEntity.class,user.getCompany().getCompanythirdparty().getId());
			
			req.setAttribute("companyThirdpartyPage", companyThirdparty);
			req.setAttribute("idsPage", payment.getId());
			req.setAttribute("identifierPage", payment.getIdentifier());
			req.setAttribute("type", "2");
			req.setAttribute("moneyPage", moneyPage);
			json.setSuccess(false);
			json.setMsg("线下转账失败！");
			
		}else  if( "1".equals(type) && StringUtil.isNotEmpty(idS) ){
			String ids[] = idS.split(",");
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
				cq.in("id", ids);//产品编码作为唯一编码
				cq.eq("company", user.getCompany());//公司信息
				cq.add();	
			List<PayBillsOrderEntity> payBillsOrderlist = systemService.getListByCriteriaQuery(cq,false);
			String idsPage="";
			String identifierPage="";
			BigDecimal moneyPage = new BigDecimal("0.00");
			if( payBillsOrderlist != null && payBillsOrderlist.size() > 0 ){
				for( PayBillsOrderEntity p: payBillsOrderlist){
					idsPage +=p.getId()+",";
					identifierPage += p.getIdentifier()+ ",";
					moneyPage =moneyPage.add(p.getObligationsmoney());
				}
			}

			
			CompanyThirdpartyEntity companyThirdparty = systemService.get(CompanyThirdpartyEntity.class,user.getCompany().getCompanythirdparty().getId());
			
			req.setAttribute("companyThirdpartyPage", companyThirdparty);
			req.setAttribute("idsPage", idsPage);
			req.setAttribute("type", "1");
			req.setAttribute("identifierPage", identifierPage);
			req.setAttribute("moneyPage", moneyPage);
			
			json.setSuccess(true);
			json.setMsg("线下转账成功！");
		}

		return json;
	}
	
	
	/**
	 * 账户余额支付跳转页
	 * 
	 * @return
	 */
	@RequestMapping(value = "usemoneyPayment")
	public ModelAndView usemoneyPayment( HttpServletRequest req) {
		TSUser user  = ResourceUtil.getSessionUserName();
		String idS = req.getParameter("id");
		String type = req.getParameter("type");
		
		if( "1".equals(type) && StringUtil.isNotEmpty(idS) ){
			
			String ids[] = idS.split(",");
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
				cq.in("id", ids);//产品编码作为唯一编码
				cq.eq("company", user.getCompany());//公司信息
				cq.add();	
			List<PayBillsOrderEntity> payBillsOrderlist = systemService.getListByCriteriaQuery(cq,false);
			String idsPage="";
			String identifierPage="";
			BigDecimal moneyPage = new BigDecimal("0.00");
			if( payBillsOrderlist != null && payBillsOrderlist.size() > 0 ){
				for( PayBillsOrderEntity p: payBillsOrderlist){
					idsPage +=p.getId()+",";
					identifierPage += p.getIdentifier()+ ",";
					moneyPage =moneyPage.add(p.getObligationsmoney());
				}
			}
			//输出用户账户信息
			UserSalesmanEntity  userSalesman  = systemService.get(UserSalesmanEntity.class, user.getTsuserSalesman().getId());
			
			//req.setAttribute("userSalesman", userSalesman);
			req.setAttribute("userSalesman", userSalesman);
			req.setAttribute("idsPage", idsPage);
			req.setAttribute("type", "1");
			req.setAttribute("identifierPage", identifierPage);
			req.setAttribute("moneyPage", moneyPage);
		}
		
		
		
		return new ModelAndView("admin/order/usemoneyPayment");
	}
	
	/**
	 * 账户余额支付
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "usemoneyPaymentSave")
	@ResponseBody
	public AjaxJson usemoneyPaymentSave(PayBillsEntity payBills, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		j = payBillsService.saveUsemoneyPayBills(payBills,"4", request);
		return j;
	}
	
	
	
	/**
	 * 线下支付收支流水
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "offlinePaymentSave")
	@ResponseBody
	public AjaxJson offlinePaymentSave(PayBillsEntity payBills, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		j = payBillsService.saveOfflinePayBills(payBills,"3" ,request);
		return j;
	}
	
	
	
	/**
	 * 支付成功
	 * 
	 * @param ids
	 * @return
	 */

	@RequestMapping(value = "saveAddZFBOrupdate")
	public  ModelAndView   saveAddZFBOrupdate(  HttpServletRequest request ) {
		
	  try{
		  
		    String out_trade_no  = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
			//支付宝交易号
			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
			//交易状态
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
			
			if (trade_status.equals("TRADE_SUCCESS") || trade_status.equals("TRADE_SUCCESS") ){
					//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
					//如果有做过处理，不执行商户的业务程序
						
					//注意：
					//付款完成后，支付宝系统发送该交易状态通知
				PayBillsEntity payBills = new PayBillsEntity();
				payBills.setId(out_trade_no);
				payBills.setState("4");
				payBills.setVoucher(trade_no);
				payBillsService.saveState( null, payBills);
			}

		  }catch (Exception e) {
			
		  }
		return new ModelAndView("redirect:payBillsController.do?list&payment=1&clickFunctionId=40288030542e920501542e972d840006");	
	}
	
	
	
	/**
	 * 查看支付凭证
	 * 
	 * @return
	 */
	@RequestMapping(value = "setbankUrl")
	public ModelAndView setbankUrl(HttpServletRequest request) {
		request.setAttribute("id", request.getParameter("id"));
		
		return new ModelAndView("admin/order/setbankUrl");
	}
	
	
	/**
	 * 查看个人充值流水
	 * @return
	 */
	@RequestMapping(value = "userSalesmanPayBills")
	public ModelAndView userSalesmanPayBills(HttpServletRequest request) {
		
		String userId = request.getParameter("userId");
		
		if( StringUtil.isNotEmpty(userId) ){
			TSUser user  = ResourceUtil.getSessionUserName();
			userId = user.getId();
		}
		
		request.setAttribute("userId", userId);
		request.setAttribute("type", "2");
		return new ModelAndView("admin/order/payBillsList");
	}
	
	
	@RequestMapping(value= "statisAdminPayBills")
	@ResponseBody
	public AjaxJson statisAdminPayBills(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计支付审核单失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = payBillsService.statisPaybills(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计支付审核单成功！");
			
		}
		
		return json;
	}
	
	/**
	 * 收支流水列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "paybillsAudit")
	public ModelAndView paybillsAudit(PayBillsEntity payBills, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(payBills.getId())) {
			payBills = payBillsService.getEntity(PayBillsEntity.class, payBills.getId());
			req.setAttribute("paybills", payBills);
		}
		return new ModelAndView("audit/paybillsAudit");
	}
}
