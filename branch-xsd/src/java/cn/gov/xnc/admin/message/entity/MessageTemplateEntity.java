package cn.gov.xnc.admin.message.entity;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;


/**   
 * @Title: Entity
 * @Description: 信息模板
 * @author zero
 * @date 2016-10-18 16:44:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_message_template", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class MessageTemplateEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**消息类型*/
	private java.lang.String type;
	/**发送方式 短信_1,微信_2,其他_3*/
	private java.lang.String settype;
	/**状态  1 启用 2关闭*/
	private int state;
	/**已经发送条数*/
	private int smsnumber;
	/**内容模板*/
	private java.lang.String content;
	/**模板字段说明*/
	private java.lang.String instructions;
	/**指定发送人手机号，分割*/
	private java.lang.String despatcher;
	/**公司信息*/
	private TSCompany  company;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  消息类型
	 */
	@Column(name ="TYPE",nullable=true,length=2)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  消息类型
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发送方式 短信_1,微信_2,其他_3
	 */
	@Column(name ="SETTYPE",nullable=true,length=2)
	public java.lang.String getSettype(){
		return this.settype;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发送方式 短信_1,微信_2,其他_3
	 */
	public void setSettype(java.lang.String settype){
		this.settype = settype;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  状态  1 启用 2关闭
	 */
	@Column(name ="STATE",nullable=true,precision=10,scale=0)
	public int getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  状态  1 启用 2关闭
	 */
	public void setState(int state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  已经发送条数
	 */
	@Column(name ="SMSNUMBER",nullable=true,precision=10,scale=0)
	public int getSmsnumber(){
		return this.smsnumber;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  已经发送条数
	 */
	public void setSmsnumber(int smsnumber){
		this.smsnumber = smsnumber;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  内容模板
	 */
	@Column(name ="CONTENT",nullable=true,length=3000)
	public java.lang.String getContent(){
		return this.content;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  内容模板
	 */
	public void setContent(java.lang.String content){
		this.content = content;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  模板字段说明
	 */
	@Column(name ="INSTRUCTIONS",nullable=true,length=2000)
	public java.lang.String getInstructions(){
		return this.instructions;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  模板字段说明
	 */
	public void setInstructions(java.lang.String instructions){
		this.instructions = instructions;
	}
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}

	/**
	 * 方法: 设置 指定发送人手机号，分割
	 * @return 指定发送人手机号，分割
	 */
	@Column(name ="DESPATCHER",nullable=true,length=5000)
	public java.lang.String getDespatcher() {
		return despatcher;
	}

	/**
	 * 方法: 设置 指定发送人手机号，分割
	 * @param 指定发送人手机号，分割
	 */
	public void setDespatcher(java.lang.String despatcher) {
		this.despatcher = despatcher;
	}
	
	
	
}
