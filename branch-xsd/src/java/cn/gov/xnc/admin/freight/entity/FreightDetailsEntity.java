package cn.gov.xnc.admin.freight.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 运费信息
 * @author zero
 * @date 2017-02-03 10:14:35
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_freight_details", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class FreightDetailsEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**基础信息*/
	private FreightEntity freightid;
	/**首重量*/
	private int qyfreightunit;
	/**首重价格*/
	private BigDecimal qyfreightpic;
	/**续重价格*/
	private BigDecimal qyfreightmorepic;
	/**续重量*/
	private int qyfreightmoreunit;
	
	
	/**运费信息*/
	private List<FreightDetailsTerritoryEntity> freightDetailsTerritoryList = new ArrayList<FreightDetailsTerritoryEntity>();
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}

	/**
	 *方法: 设置java.lang.String 基础信息
	 *@param: java.lang.String 基础信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "freightid")
	public FreightEntity getFreightid(){
		return this.freightid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  基础信息
	 */
	public void setFreightid(FreightEntity freightid){
		this.freightid = freightid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  首总量
	 */
	@Column(name ="QYFREIGHTUNIT",nullable=true,length=2)
	public int getQyfreightunit(){
		return this.qyfreightunit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  首总量
	 */
	public void setQyfreightunit(int qyfreightunit){
		this.qyfreightunit = qyfreightunit;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  首重价格
	 */
	@Column(name ="QYFREIGHTPIC",nullable=true,precision=10,scale=2)
	public BigDecimal getQyfreightpic(){
		return this.qyfreightpic;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  首重价格
	 */
	public void setQyfreightpic(BigDecimal qyfreightpic){
		this.qyfreightpic = qyfreightpic;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  续重价格
	 */
	@Column(name ="QYFREIGHTMOREPIC",nullable=true,precision=10,scale=2)
	public BigDecimal getQyfreightmorepic(){
		return this.qyfreightmorepic;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  续重价格
	 */
	public void setQyfreightmorepic(BigDecimal qyfreightmorepic){
		this.qyfreightmorepic = qyfreightmorepic;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  续重单位
	 */
	@Column(name ="QYFREIGHTMOREUNIT",nullable=true,length=2)
	public int getQyfreightmoreunit(){
		return this.qyfreightmoreunit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  续重单位
	 */
	public void setQyfreightmoreunit(int qyfreightmoreunit){
		this.qyfreightmoreunit = qyfreightmoreunit;
	}

	
	
	/**地区运费信息*/
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "freighdetailsid")
	public List<FreightDetailsTerritoryEntity> getFreightDetailsTerritoryList() {
		return freightDetailsTerritoryList;
	}

	public void setFreightDetailsTerritoryList(
			List<FreightDetailsTerritoryEntity> freightDetailsTerritoryList) {
		this.freightDetailsTerritoryList = freightDetailsTerritoryList;
	}
	
	
}
