package cn.gov.xnc.admin.freight.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightDetailsTerritoryEntity;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;
import cn.gov.xnc.admin.freight.service.FreightDetailsTerritoryServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;

@Service("freightDetailsService")
@Transactional
public class FreightDetailsServiceImpl extends CommonServiceImpl implements FreightDetailsServiceI {

	@Autowired
	private FreightDetailsTerritoryServiceI freightDetailsTerritoryService;
	
	@Override
	public List<FreightDetailsEntity> buildFreightDetail(final FreightEntity freightEntity, HttpServletRequest req) {
		List<FreightDetailsEntity> freightDetaillist = new ArrayList<FreightDetailsEntity>();
		List<FreightDetailsTerritoryEntity> freightDetailsTerritoryList = new ArrayList<FreightDetailsTerritoryEntity>();
		String expLen = ResourceUtil.getParameter("expLen");
		if(StringUtil.isNotEmpty(expLen)){
			String districtStr = null;
			
			int freightDetailsNum = Integer.parseInt(expLen);
			for (int i = 0; i < freightDetailsNum; i++) {
				
				FreightDetailsEntity freightDetail = new FreightDetailsEntity();
				freightDetail.setId(IdWorker.generateSequenceNo());
				freightDetail.setFreightid(freightEntity);
				freightDetail.setQyfreightunit(Integer.parseInt(req.getParameter("qyfreightunit-" + i)));
				freightDetail.setQyfreightpic(new BigDecimal(req.getParameter("qyfreightpic-" + i)));
				freightDetail.setQyfreightmoreunit(Integer.parseInt(req.getParameter("qyfreightmoreunit-" + i)));
				freightDetail.setQyfreightmorepic(new BigDecimal(req.getParameter("qyfreightmorepic-" + i)));
				freightDetaillist.add(freightDetail);
				
				districtStr = req.getParameter("zone-" + i);
				if(StringUtil.isNotEmpty(districtStr)){
					String[] districtArray = districtStr.split(",");
					
					for (int j = 0; j < districtArray.length; j++) {
						//添加运费区域
						freightDetailsTerritoryList.add(freightDetailsTerritoryService.buildFreightTerritory(freightDetail, districtArray[j], req));
					}
					
				}else{
					
				}
				
			}
		}
		
		if(freightDetaillist.size() > 0 && freightDetailsTerritoryList.size() > 0){
			
			batchSave(freightDetaillist);
			
			batchSave(freightDetailsTerritoryList);
		}
		
		return freightDetaillist;
	}

	@Override
	public String freightDetail2jsonStr(List<FreightDetailsEntity> freightDetailsList, HttpServletRequest reg) throws Exception{
		StringBuffer jsonTemp = new StringBuffer();
		
		 if( freightDetailsList != null && freightDetailsList.size() > 0){
			 jsonTemp.append("{\"rows\":\"" + freightDetailsList.size() + "\", \"data\":[");
			 for(int fd=0 ;fd <freightDetailsList.size() ; fd ++  ){//运费情况
				 FreightDetailsEntity freightDetails = freightDetailsList.get(fd);
				 //遍历地区数据，构建地区string
				 String to = "";
				 if( freightDetails.getFreightDetailsTerritoryList() != null && freightDetails.getFreightDetailsTerritoryList().size() > 0 ){
					 for( int fdt = 0 ; fdt< freightDetails.getFreightDetailsTerritoryList().size() ; fdt ++   ){
						 FreightDetailsTerritoryEntity freightDetailsTerritory = freightDetails.getFreightDetailsTerritoryList().get(fdt);
						 to += freightDetailsTerritory.getTerritoryid() + ":" + freightDetailsTerritory.getTerritoryname() ;
						 if( fdt != freightDetails.getFreightDetailsTerritoryList().size() - 1 ){
							 to +=",";
						 }
					 }
				 }
				 jsonTemp.append("[{\"zone\":\""+to+"\"},{\"qyfreightunit\":\"" +freightDetails.getQyfreightunit() + "\"},{\"qyfreightpic\":\"" + freightDetails.getQyfreightpic() +"\"},{\"qyfreightmoreunit\":\"" + freightDetails.getQyfreightmoreunit() + "\"},{\"qyfreightmorepic\":\"" + freightDetails.getQyfreightmorepic() + "\"},{\"freightDetailId\":\""+ freightDetails.getId() + "\"}]");
				if (fd != freightDetailsList.size() - 1) {
					jsonTemp.append(",");
				} 
			 }
		 }else{
			 jsonTemp.append("{\"rows\":\"0\", \"data\":[");
		 }
		 
		jsonTemp.append("]}");
		return jsonTemp.toString();
	}

	@Override
	public void batchUpdatefreightDetails(final FreightEntity freightEntity, HttpServletRequest req) throws Exception {
		List<FreightDetailsEntity> freightDetaillistUpdate = new ArrayList<FreightDetailsEntity>();
		List<FreightDetailsEntity> freightDetaillistAdd = new ArrayList<FreightDetailsEntity>();
		List<FreightDetailsTerritoryEntity> freightDetailsTerritoryList = new ArrayList<FreightDetailsTerritoryEntity>();
		String expLen = ResourceUtil.getParameter("expLen");
		if(StringUtil.isNotEmpty(expLen)){
			String districtStr = null;
			
			int freightDetailsNum = Integer.parseInt(expLen);
			for (int i = 0; i < freightDetailsNum; i++) {
				
				FreightDetailsEntity freightDetail = findUniqueByProperty(FreightDetailsEntity.class, "id", req.getParameter("freightDetailId-" + i));
				if(freightDetail ==null){
					freightDetail = new FreightDetailsEntity();
					freightDetail.setId(IdWorker.generateSequenceNo());
					freightDetaillistAdd.add(freightDetail);
				}else{
					freightDetaillistUpdate.add(freightDetail);
				}
				
				freightDetail.setFreightid(freightEntity);
				freightDetail.setQyfreightunit(Integer.parseInt(req.getParameter("qyfreightunit-" + i)));
				freightDetail.setQyfreightpic(new BigDecimal(req.getParameter("qyfreightpic-" + i)));
				freightDetail.setQyfreightmoreunit(Integer.parseInt(req.getParameter("qyfreightmoreunit-" + i)));
				freightDetail.setQyfreightmorepic(new BigDecimal(req.getParameter("qyfreightmorepic-" + i)));
				
				/* 删掉区域运费存储的地域数据，从新构建
				 * 目前地域数据这块更新较为繁琐，因为前台页面并未传送这部分数据id,想到的更新方法有两种：
				 * 1、批量更新无效，然后重新添加，此种方式导致数据库存放太多垃圾数据
				 * 2、批量删除数据再批量添加，数据库较为干净，但是会产生磁盘碎片
				 */
				
				//删除旧数据
				
				List<FreightDetailsTerritoryEntity> freightDetailsTerritoryOlds =  freightDetail.getFreightDetailsTerritoryList();
				
				if(freightDetailsTerritoryOlds.size() > 0){
					
					for (FreightDetailsTerritoryEntity freightDetailsTerritoryEntity : freightDetailsTerritoryOlds) {
						freightDetailsTerritoryEntity.setFreighdetailsid(null);
					}
					
					batchUpdate(freightDetailsTerritoryOlds);
					deleteAllEntitie(freightDetailsTerritoryOlds);
					freightDetail.setFreightDetailsTerritoryList(null);
				}
				
				
				//重新添加
				districtStr = req.getParameter("zone-" + i);
				if(StringUtil.isNotEmpty(districtStr)){
					String[] districtArray = districtStr.split(",");
					for (int j = 0; j < districtArray.length; j++) {
						//添加运费区域
						freightDetailsTerritoryList.add(freightDetailsTerritoryService.buildFreightTerritory(freightDetail, districtArray[j], req));
					}
					
				}else{
					
				}
				
			}
		}
		
		if(freightDetaillistUpdate.size() > 0){
			batchUpdate(freightDetaillistUpdate);
		}
		
		if(freightDetaillistAdd.size() > 0){
			batchSave(freightDetaillistAdd);
		}
		
		if(freightDetailsTerritoryList.size() > 0){
			
			batchSave(freightDetailsTerritoryList);
			
		}
	}
	
	/**
	 * 计算订购商品运费
	 * @param product 订购商品
	 * @param provinceid 发送地址
	 * @param orderNUm 订购件数
	 * @return
	 * @throws Exception
	 */
	@Override
	public BigDecimal getProductFreightByProvinceId(ProductEntity product, String provinceid, BigDecimal orderNUm) throws Exception {
		BigDecimal freight = new BigDecimal(0.00);
		boolean hasTerritoryFreight = false;
		if(null != product && StringUtil.isNotEmpty(product.getId())){
			
			FreightEntity freightEngtity = product.getFreightid();
			
			if(null != freightEngtity && StringUtil.isNotEmpty(freightEngtity.getId())){
				//去取运费模板
				freightEngtity = findUniqueByProperty(FreightEntity.class, "id", freightEngtity.getId());
				
				if(null != freightEngtity){
					
					List<FreightDetailsEntity> freightDetailList = freightEngtity.getFreightDetailsList();
					
					//区域运费 : 根据运费模板详细和寄送省份，查早是否存在区域运费
					for (FreightDetailsEntity freightDetailsEntity : freightDetailList) {
						
						CriteriaQuery cq = new CriteriaQuery( FreightDetailsTerritoryEntity.class );
						cq.eq("freighdetailsid", freightDetailsEntity);
						cq.eq("territoryid", provinceid);
						cq.add();
						
						List<FreightDetailsTerritoryEntity> freightTerritoryList = getListByCriteriaQuery(cq, false);
						
						//存在区域运费根据区域运费计算
						if(null != freightTerritoryList && freightTerritoryList.size() > 0){
							hasTerritoryFreight = true;
							
							FreightDetailsTerritoryEntity freightTerritory = freightTerritoryList.get(0);
							FreightDetailsEntity freightDetail = freightTerritory.getFreighdetailsid();
							
							BigDecimal unitfreight = new BigDecimal(freightDetail.getQyfreightunit());
							BigDecimal unitmorefreight = new BigDecimal(freightDetail.getQyfreightmoreunit());
							
							if(orderNUm.compareTo(unitfreight) > 0){
								BigDecimal subunit = orderNUm.subtract(unitfreight);//计算多出的件数
								BigDecimal moreunit = subunit.divide(unitmorefreight);//根据区域续件单位计算续件数
								int intMoreUnit =  (int) Math.ceil(moreunit.doubleValue());//根据区域续件单位计算续件数取整，只要是小数的都取整+1
								BigDecimal moreFreight = freightDetail.getQyfreightmorepic().multiply(new BigDecimal(intMoreUnit));//计算续件价格
								
								freight = freightDetail.getQyfreightpic().add(moreFreight);//总运费= 首件 + 续件
							}else{
								
								freight = freightDetail.getQyfreightpic();
							}
							
							break;
						}
					}
					//没有区域运费 : 获取模版默认运费
					if(!hasTerritoryFreight){
						BigDecimal unitfreight = new BigDecimal(freightEngtity.getFreightunit());
						BigDecimal unitmorefreight = new BigDecimal(freightEngtity.getFreightmoreunit());
						if(orderNUm.compareTo(unitfreight) > 0){
							double subunit = (orderNUm.subtract(unitfreight)).doubleValue();//计算多出的件数
							double moreunit = subunit/unitfreight.doubleValue();//根据区域续件单位计算续件数
							int intMoreUnit =  (int) Math.ceil(moreunit);//根据区域续件单位计算续件数取整，只要是小数的都取整+1
							BigDecimal moreFreight = freightEngtity.getFreightmorepic().multiply(new BigDecimal(intMoreUnit));//计算续件价格
							
							freight = freightEngtity.getFreightpic().add(moreFreight);//总运费= 首件 + 续件
						}else{
							
							freight = freightEngtity.getFreightpic();
						}
					}
				}
			}
		}
		
		return freight;
	}

	@Override
	public BigDecimal getProductFreightByFullAddr(ProductEntity product, String fullAddr, BigDecimal orderNUm)
			throws Exception {
		BigDecimal freight = new BigDecimal(0.00);
		boolean hasTerritoryFreight = false;
		if(null != product && StringUtil.isNotEmpty(product.getId())){
			
			FreightEntity freightEngtity = product.getFreightid();
			
			if(null != freightEngtity && StringUtil.isNotEmpty(freightEngtity.getId())){
				//去取运费模板
				freightEngtity = findUniqueByProperty(FreightEntity.class, "id", freightEngtity.getId());
				
				if(null != freightEngtity){//如果绑定运费模板
					
					List<FreightDetailsEntity> freightDetailList = freightEngtity.getFreightDetailsList();
					
					//区域运费 : 根据运费模板详细和寄送省份，查早是否存在区域运费
					for (FreightDetailsEntity freightDetailsEntity : freightDetailList) {
						
						CriteriaQuery cq = new CriteriaQuery( FreightDetailsTerritoryEntity.class );
						cq.eq("freighdetailsid", freightDetailsEntity);
						cq.add();
						
						List<FreightDetailsTerritoryEntity> freightTerritoryList = getListByCriteriaQuery(cq, false);
						
						//存在区域运费模板计算开始
						if(null != freightTerritoryList && freightTerritoryList.size() > 0){
							for (FreightDetailsTerritoryEntity freightTerritory : freightTerritoryList) {
								
								if(fullAddr.startsWith(freightTerritory.getTerritoryname().substring(0,2))){
									
									hasTerritoryFreight = true;
									FreightDetailsEntity freightDetail = freightTerritory.getFreighdetailsid();
									BigDecimal unitfreight = new BigDecimal(freightDetail.getQyfreightunit());
									BigDecimal unitmorefreight = new BigDecimal(freightDetail.getQyfreightmoreunit());
									
									if(orderNUm.compareTo(unitfreight) > 0){
										double subunit = (orderNUm.subtract(unitfreight)).doubleValue();//计算多出的件数
										double moreunit = subunit/unitmorefreight.doubleValue();//根据区域续件单位计算续件数
										int intMoreUnit =  (int) Math.ceil(moreunit);//根据区域续件单位计算续件数取整，只要是小数的都取整+1
										BigDecimal moreFreight = freightDetail.getQyfreightmorepic().multiply(new BigDecimal(intMoreUnit));//计算续件价格
										
										freight = freightDetail.getQyfreightpic().add(moreFreight);//总运费= 首件 + 续件
									}else{
										
										freight = freightDetail.getQyfreightpic();
									}
									
									break;
								}
								
							}
							
							if(hasTerritoryFreight){
								break;//已经找到区域运费模板则跳出查找运费模板循环体
							}
						}
					}
					//没有区域运费 : 获取模版默认运费模板计算开始
					if(!hasTerritoryFreight){
						BigDecimal unitfreight = new BigDecimal(freightEngtity.getFreightunit());
						if(orderNUm.compareTo(unitfreight) > 0){
							double subunit = (double) (orderNUm.subtract(unitfreight)).doubleValue();//计算多出的件数
							double moreunit = subunit/(double) freightEngtity.getFreightunit();//根据区域续件单位计算续件数
							int intMoreUnit =  (int) Math.ceil(moreunit);//根据区域续件单位计算续件数取整，只要是小数的都取整+1
							BigDecimal moreFreight = freightEngtity.getFreightmorepic().multiply(new BigDecimal(intMoreUnit));//计算续件价格
							
							freight = freightEngtity.getFreightpic().add(moreFreight);//总运费= 首件 + 续件
						}else{
							
							freight = freightEngtity.getFreightpic();
						}
					}
				}
			}
		}
		
		return freight;
	}
	
}