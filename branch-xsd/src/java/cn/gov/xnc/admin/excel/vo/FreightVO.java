package cn.gov.xnc.admin.excel.vo;

import java.math.BigDecimal;


import cn.gov.xnc.system.excel.annotation.Excel;


/**   
 * @Title: Entity
 * @Description: 运费模板
 * @author zero
 * @date 2016-04-23 01:51:30
 * @version V1.0   
 *
 */

public class FreightVO {
	
	
	
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**商品编码*/
	@Excel(exportName="商品编码",orderNum="1")
	private java.lang.String code;
	
	/**模板名称*/
	private java.lang.String name;
	/**物流公司*/
	@Excel(exportName="物流公司",orderNum="35")
	private java.lang.String freightName;
	/**北京市*/
	@Excel(exportName="北京市",orderNum="4")
	private BigDecimal beijingshi;
	/**天津市*/
	@Excel(exportName="天津市",orderNum="5")
	private BigDecimal tianjinshi;
	/**河北省*/
	@Excel(exportName="河北省",orderNum="6")
	private BigDecimal hebeisheng;
	
	
	/**上海市*/
	@Excel(exportName="上海市",orderNum="7")
	private BigDecimal shanghaishi;
	/**江苏省*/
	@Excel(exportName="江苏省",orderNum="8")
	private BigDecimal jiangsusheng;
	/**浙江省*/
	@Excel(exportName="浙江省",orderNum="9")
	private BigDecimal zhejiangsheng;
	/**安徽省*/
	@Excel(exportName="安徽省",orderNum="10")
	private BigDecimal anhuisheng;
	/**福建省*/
	@Excel(exportName="福建省",orderNum="11")
	private BigDecimal fujiansheng;
	/**湖北省*/
	@Excel(exportName="湖北省",orderNum="12")
	private BigDecimal hubeisheng;
	/**湖南省*/
	@Excel(exportName="湖南省",orderNum="13")
	private BigDecimal hunansheng;
	/**广西壮族自治区*/
	@Excel(exportName="广西壮族自治区",orderNum="14")
	private BigDecimal guangxi;
	
	
	/**山西省*/
	@Excel(exportName="广西壮族自治区",orderNum="15")
	private BigDecimal shanxisheng;
	/**辽宁省*/
	@Excel(exportName="广西壮族自治区",orderNum="16")
	private BigDecimal liaoningsheng;
	/**黑龙江省*/
	@Excel(exportName="广西壮族自治区",orderNum="17")
	private BigDecimal heilongjiangsheng;
	/**山东省*/
	@Excel(exportName="广西壮族自治区",orderNum="18")
	private BigDecimal shandongsheng;
	
	/**吉林省*/
	@Excel(exportName="吉林省",orderNum="19")
	private BigDecimal jilinsheng;
	/**陕西省*/
	@Excel(exportName="广西壮族自治区",orderNum="20")
	private BigDecimal shanxisheng2;
	/**广东省*/
	@Excel(exportName="广西壮族自治区",orderNum="21")
	private BigDecimal guangdongsheng;
	/**海南省*/
	@Excel(exportName="广西壮族自治区",orderNum="22")
	private BigDecimal hainansheng;
	
	/**江西省*/
	@Excel(exportName="江西省",orderNum="23")
	private BigDecimal jiangxisheng;
	/**河南省*/
	@Excel(exportName="河南省",orderNum="24")
	private BigDecimal henansheng;
	/**重庆市*/
	@Excel(exportName="重庆市",orderNum="25")
	private BigDecimal chongqingshi;
	/**四川省*/
	@Excel(exportName="四川省",orderNum="26")
	private BigDecimal sichuansheng;
	/**贵州省*/
	@Excel(exportName="贵州省",orderNum="27")
	private BigDecimal guizhousheng;
	/**云南省*/
	@Excel(exportName="云南省",orderNum="28")
	private BigDecimal yunansheng;
	
	
	
	/**内蒙古自治区*/
	@Excel(exportName="内蒙古自治区",orderNum="29")
	private BigDecimal neimenggu;
	/**西藏自治区*/
	@Excel(exportName="西藏自治区",orderNum="30")
	private BigDecimal xizang;
	/**甘肃省*/
	@Excel(exportName="甘肃省",orderNum="31")
	private BigDecimal gansusheng;
	/**青海省*/
	@Excel(exportName="青海省",orderNum="32")
	private BigDecimal qinghaisheng;
	/**宁夏回族自治区*/
	@Excel(exportName="宁夏回族自治区",orderNum="33")
	private BigDecimal ningxia;
	/**新疆维吾尔自治区*/
	@Excel(exportName="新疆维吾尔自治区",orderNum="34")
	private BigDecimal xinjiang;
	
	

	
//	/**台湾省*/
//	private BigDecimal taiwansheng;
//	/**香港特别行政区*/
//	private BigDecimal xianggang;
//	/**澳门特别行政区*/
//	private BigDecimal anmen;
	
	/**公司信息*/
	private java.lang.String companyid;
	/**创建时间*/
	private java.util.Date createdate;
	/**更新时间*/
	private java.util.Date updatedate;
	
	
	
	
	
	
	/**
	 * @return the 导入的表格id
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param 导入的表格id the 导入的表格id to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the 商品编码
	 */
	public java.lang.String getCode() {
		return code;
	}

	/**
	 * @param 商品编码 the 商品编码 to set
	 */
	public void setCode(java.lang.String code) {
		this.code = code;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  模板名称
	 */
	
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  模板名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流公司名称
	 */

	public java.lang.String getFreightName() {
		return freightName;
	}
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  物流公司名称
	 */
	public void setFreightName(java.lang.String freightName) {
		this.freightName = freightName;
	}

	
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  北京市
	 */

	public BigDecimal getBeijingshi(){
		return this.beijingshi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  北京市
	 */
	public void setBeijingshi(BigDecimal beijingshi){
		this.beijingshi = beijingshi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  天津市
	 */
	
	public BigDecimal getTianjinshi(){
		return this.tianjinshi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  天津市
	 */
	public void setTianjinshi(BigDecimal tianjinshi){
		this.tianjinshi = tianjinshi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  河北省
	 */

	public BigDecimal getHebeisheng(){
		return this.hebeisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  河北省
	 */
	public void setHebeisheng(BigDecimal hebeisheng){
		this.hebeisheng = hebeisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  山西省
	 */

	public BigDecimal getShanxisheng(){
		return this.shanxisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  山西省
	 */
	public void setShanxisheng(BigDecimal shanxisheng){
		this.shanxisheng = shanxisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  内蒙古自治区
	 */

	public BigDecimal getNeimenggu(){
		return this.neimenggu;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  内蒙古自治区
	 */
	public void setNeimenggu(BigDecimal neimenggu){
		this.neimenggu = neimenggu;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  辽宁省
	 */

	public BigDecimal getLiaoningsheng(){
		return this.liaoningsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  辽宁省
	 */
	public void setLiaoningsheng(BigDecimal liaoningsheng){
		this.liaoningsheng = liaoningsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  吉林省
	 */

	public BigDecimal getJilinsheng(){
		return this.jilinsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  吉林省
	 */
	public void setJilinsheng(BigDecimal jilinsheng){
		this.jilinsheng = jilinsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  黑龙江省
	 */

	public BigDecimal getHeilongjiangsheng(){
		return this.heilongjiangsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  黑龙江省
	 */
	public void setHeilongjiangsheng(BigDecimal heilongjiangsheng){
		this.heilongjiangsheng = heilongjiangsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  上海市
	 */

	public BigDecimal getShanghaishi(){
		return this.shanghaishi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  上海市
	 */
	public void setShanghaishi(BigDecimal shanghaishi){
		this.shanghaishi = shanghaishi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  江苏省
	 */

	public BigDecimal getJiangsusheng(){
		return this.jiangsusheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  江苏省
	 */
	public void setJiangsusheng(BigDecimal jiangsusheng){
		this.jiangsusheng = jiangsusheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  浙江省
	 */

	public BigDecimal getZhejiangsheng(){
		return this.zhejiangsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  浙江省
	 */
	public void setZhejiangsheng(BigDecimal zhejiangsheng){
		this.zhejiangsheng = zhejiangsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  安徽省
	 */

	public BigDecimal getAnhuisheng(){
		return this.anhuisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  安徽省
	 */
	public void setAnhuisheng(BigDecimal anhuisheng){
		this.anhuisheng = anhuisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  福建省
	 */

	public BigDecimal getFujiansheng(){
		return this.fujiansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  福建省
	 */
	public void setFujiansheng(BigDecimal fujiansheng){
		this.fujiansheng = fujiansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  江西省
	 */

	public BigDecimal getJiangxisheng(){
		return this.jiangxisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  江西省
	 */
	public void setJiangxisheng(BigDecimal jiangxisheng){
		this.jiangxisheng = jiangxisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  山东省
	 */

	public BigDecimal getShandongsheng(){
		return this.shandongsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  山东省
	 */
	public void setShandongsheng(BigDecimal shandongsheng){
		this.shandongsheng = shandongsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  河南省
	 */

	public BigDecimal getHenansheng(){
		return this.henansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  河南省
	 */
	public void setHenansheng(BigDecimal henansheng){
		this.henansheng = henansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  湖北省
	 */

	public BigDecimal getHubeisheng(){
		return this.hubeisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  湖北省
	 */
	public void setHubeisheng(BigDecimal hubeisheng){
		this.hubeisheng = hubeisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  湖南省
	 */

	public BigDecimal getHunansheng(){
		return this.hunansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  湖南省
	 */
	public void setHunansheng(BigDecimal hunansheng){
		this.hunansheng = hunansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  广东省
	 */

	public BigDecimal getGuangdongsheng(){
		return this.guangdongsheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  广东省
	 */
	public void setGuangdongsheng(BigDecimal guangdongsheng){
		this.guangdongsheng = guangdongsheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  广西壮族自治区
	 */

	public BigDecimal getGuangxi(){
		return this.guangxi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  广西壮族自治区
	 */
	public void setGuangxi(BigDecimal guangxi){
		this.guangxi = guangxi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  海南省
	 */

	public BigDecimal getHainansheng(){
		return this.hainansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  海南省
	 */
	public void setHainansheng(BigDecimal hainansheng){
		this.hainansheng = hainansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  重庆市
	 */

	public BigDecimal getChongqingshi(){
		return this.chongqingshi;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  重庆市
	 */
	public void setChongqingshi(BigDecimal chongqingshi){
		this.chongqingshi = chongqingshi;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  四川省
	 */

	public BigDecimal getSichuansheng(){
		return this.sichuansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  四川省
	 */
	public void setSichuansheng(BigDecimal sichuansheng){
		this.sichuansheng = sichuansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  贵州省
	 */

	public BigDecimal getGuizhousheng(){
		return this.guizhousheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  贵州省
	 */
	public void setGuizhousheng(BigDecimal guizhousheng){
		this.guizhousheng = guizhousheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  云南省
	 */

	public BigDecimal getYunansheng(){
		return this.yunansheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  云南省
	 */
	public void setYunansheng(BigDecimal yunansheng){
		this.yunansheng = yunansheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  西藏自治区
	 */

	public BigDecimal getXizang(){
		return this.xizang;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  西藏自治区
	 */
	public void setXizang(BigDecimal xizang){
		this.xizang = xizang;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  陕西省
	 */

	public BigDecimal getShanxisheng2(){
		return this.shanxisheng2;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  陕西省
	 */
	public void setShanxisheng2(BigDecimal shanxisheng2){
		this.shanxisheng2 = shanxisheng2;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  甘肃省
	 */

	public BigDecimal getGansusheng(){
		return this.gansusheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  甘肃省
	 */
	public void setGansusheng(BigDecimal gansusheng){
		this.gansusheng = gansusheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  青海省
	 */

	public BigDecimal getQinghaisheng(){
		return this.qinghaisheng;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  青海省
	 */
	public void setQinghaisheng(BigDecimal qinghaisheng){
		this.qinghaisheng = qinghaisheng;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  宁夏回族自治区
	 */

	public BigDecimal getNingxia(){
		return this.ningxia;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  宁夏回族自治区
	 */
	public void setNingxia(BigDecimal ningxia){
		this.ningxia = ningxia;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  新疆维吾尔自治区
	 */

	public BigDecimal getXinjiang(){
		return this.xinjiang;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  新疆维吾尔自治区
	 */
	public void setXinjiang(BigDecimal xinjiang){
		this.xinjiang = xinjiang;
	}
	
//	/**
//	 *方法: 取得BigDecimal
//	 *@return: BigDecimal  台湾省
//	 */
//	@Column(name ="TAIWANSHENG",nullable=false,precision=10,scale=2)
//	public BigDecimal getTaiwansheng(){
//		return this.taiwansheng;
//	}
//
//	/**
//	 *方法: 设置BigDecimal
//	 *@param: BigDecimal  台湾省
//	 */
//	public void setTaiwansheng(BigDecimal taiwansheng){
//		this.taiwansheng = taiwansheng;
//	}
//	/**
//	 *方法: 取得BigDecimal
//	 *@return: BigDecimal  香港特别行政区
//	 */
//	@Column(name ="XIANGGANG",nullable=false,precision=10,scale=2)
//	public BigDecimal getXianggang(){
//		return this.xianggang;
//	}
//
//	/**
//	 *方法: 设置BigDecimal
//	 *@param: BigDecimal  香港特别行政区
//	 */
//	public void setXianggang(BigDecimal xianggang){
//		this.xianggang = xianggang;
//	}
//	/**
//	 *方法: 取得BigDecimal
//	 *@return: BigDecimal  澳门特别行政区
//	 */
//	@Column(name ="ANMEN",nullable=false,precision=10,scale=2)
//	public BigDecimal getAnmen(){
//		return this.anmen;
//	}
//
//	/**
//	 *方法: 设置BigDecimal
//	 *@param: BigDecimal  澳门特别行政区
//	 */
//	public void setAnmen(BigDecimal anmen){
//		this.anmen = anmen;
//	}
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司信息
	 */

	public java.lang.String getCompanyid(){
		return this.companyid;
	}



	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司信息
	 */
	public void setCompanyid(java.lang.String companyid){
		this.companyid = companyid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */

	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */

	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}
	

	
	
	
}
