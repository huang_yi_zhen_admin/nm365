package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class CPBLExcel {

	public CPBLExcel() {
		// TODO Auto-generated constructor stub
	}
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**产品名称*/
	@Excel(exportName="产品名称",orderNum="1")
	private java.lang.String productname;
	
	/**价格*/
	@Excel(exportName="价格",orderNum="2")
	private java.lang.String price;
	
	/**运费*/
	@Excel(exportName="运费",orderNum="3")
	private java.lang.String freightpic;
	
	/**数量*/
	@Excel(exportName="数量",orderNum="4")
	private java.lang.String number;

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	

	/**
	 * @return the productname
	 */
	public java.lang.String getProductname() {
		return productname;
	}

	/**
	 * @param productname the productname to set
	 */
	public void setProductname(java.lang.String productname) {
		this.productname = productname;
	}

	/**
	 * @return the price
	 */
	public java.lang.String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(java.lang.String price) {
		this.price = price;
	}

	/**
	 * @return the freightpic
	 */
	public java.lang.String getFreightpic() {
		return freightpic;
	}

	/**
	 * @param freightpic the freightpic to set
	 */
	public void setFreightpic(java.lang.String freightpic) {
		this.freightpic = freightpic;
	}

	/**
	 * @return the number
	 */
	public java.lang.String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(java.lang.String number) {
		this.number = number;
	}

	
}
