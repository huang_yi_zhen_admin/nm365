package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class PayBillsOrderCashExcel {

	public PayBillsOrderCashExcel() {
		// TODO Auto-generated constructor stub
	}
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**订单编号*/
	@Excel(exportName="订单编号",orderNum="1")
	private java.lang.String identifieror;
	
	/**客户信息*/
	@Excel(exportName="客户信息",orderNum="2")
	private java.lang.String clientid_realname;
	
	/**公司名称*/
	@Excel(exportName="公司名称",orderNum="3")
	private java.lang.String company_companyName;
	
	/**业务员*/
	@Excel(exportName="业务员",orderNum="4")
	private java.lang.String yewu_realname;
	
	/**流水类型*/
	@Excel(exportName="流水类型",orderNum="5")
	private java.lang.String type;
	
	/**支付状态*/
	@Excel(exportName="支付状态",orderNum="6")
	private java.lang.String state;
	
	/**货运状态*/
	@Excel(exportName="货运状态",orderNum="7")
	private java.lang.String freightstatus;
	
	/**支付方式*/
	@Excel(exportName="支付方式",orderNum="8")
	private java.lang.String paymentmethod;
	
	/**应付款*/
	@Excel(exportName="应付款",orderNum="9")
	private java.lang.String payablemoney;
	
	/**已付款*/
	@Excel(exportName="已付款",orderNum="10")
	private java.lang.String alreadypaidmoney;
	
	/**
	 * @return the company_companyName
	 */
	public java.lang.String getCompany_companyName() {
		return company_companyName;
	}

	/**
	 * @param company_companyName the company_companyName to set
	 */
	public void setCompany_companyName(java.lang.String company_companyName) {
		this.company_companyName = company_companyName;
	}

	/**待付金额*/
	@Excel(exportName="待付金额",orderNum="11")
	private java.lang.String obligationsmoney;
	
	/**是否代下单*/
	@Excel(exportName="是否代下单",orderNum="12")
	private java.lang.String isSaleManOrder;
	
	/**审核人*/
	@Excel(exportName="审核人",orderNum="13")
	private java.lang.String audituser;
	
	/**审核日期*/
	@Excel(exportName="审核日期",orderNum="14")
	private java.lang.String auditDate;
	
	/**下单时间*/
	@Excel(exportName="下单时间",orderNum="15")
	private java.lang.String createdate;

	/**
	 * @return the type
	 */
	public java.lang.String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(java.lang.String type) {
		this.type = type;
	}

	/**
	 * @return the isSaleManOrder
	 */
	public java.lang.String getIsSaleManOrder() {
		return isSaleManOrder;
	}

	/**
	 * @param isSaleManOrder the isSaleManOrder to set
	 */
	public void setIsSaleManOrder(java.lang.String isSaleManOrder) {
		this.isSaleManOrder = isSaleManOrder;
	}

	/**
	 * @return the audituser
	 */
	public java.lang.String getAudituser() {
		return audituser;
	}

	/**
	 * @param audituser the audituser to set
	 */
	public void setAudituser(java.lang.String audituser) {
		this.audituser = audituser;
	}

	/**
	 * @return the auditDate
	 */
	public java.lang.String getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate the auditDate to set
	 */
	public void setAuditDate(java.lang.String auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the identifieror
	 */
	public java.lang.String getIdentifieror() {
		return identifieror;
	}

	/**
	 * @param identifieror the identifieror to set
	 */
	public void setIdentifieror(java.lang.String identifieror) {
		this.identifieror = identifieror;
	}

	/**
	 * @return the clientid_realname
	 */
	public java.lang.String getClientid_realname() {
		return clientid_realname;
	}

	/**
	 * @param clientid_realname the clientid_realname to set
	 */
	public void setClientid_realname(java.lang.String clientid_realname) {
		this.clientid_realname = clientid_realname;
	}

	/**
	 * @return the yewu_realname
	 */
	public java.lang.String getYewu_realname() {
		return yewu_realname;
	}

	/**
	 * @param yewu_realname the yewu_realname to set
	 */
	public void setYewu_realname(java.lang.String yewu_realname) {
		this.yewu_realname = yewu_realname;
	}

	/**
	 * @return the state
	 */
	public java.lang.String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(java.lang.String state) {
		this.state = state;
	}

	/**
	 * @return the freightstatus
	 */
	public java.lang.String getFreightstatus() {
		return freightstatus;
	}

	/**
	 * @param freightstatus the freightstatus to set
	 */
	public void setFreightstatus(java.lang.String freightstatus) {
		this.freightstatus = freightstatus;
	}

	/**
	 * @return the paymentmethod
	 */
	public java.lang.String getPaymentmethod() {
		return paymentmethod;
	}

	/**
	 * @param paymentmethod the paymentmethod to set
	 */
	public void setPaymentmethod(java.lang.String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}

	/**
	 * @return the payablemoney
	 */
	public java.lang.String getPayablemoney() {
		return payablemoney;
	}

	/**
	 * @param payablemoney the payablemoney to set
	 */
	public void setPayablemoney(java.lang.String payablemoney) {
		this.payablemoney = payablemoney;
	}

	/**
	 * @return the alreadypaidmoney
	 */
	public java.lang.String getAlreadypaidmoney() {
		return alreadypaidmoney;
	}

	/**
	 * @param alreadypaidmoney the alreadypaidmoney to set
	 */
	public void setAlreadypaidmoney(java.lang.String alreadypaidmoney) {
		this.alreadypaidmoney = alreadypaidmoney;
	}

	/**
	 * @return the obligationsmoney
	 */
	public java.lang.String getObligationsmoney() {
		return obligationsmoney;
	}

	/**
	 * @param obligationsmoney the obligationsmoney to set
	 */
	public void setObligationsmoney(java.lang.String obligationsmoney) {
		this.obligationsmoney = obligationsmoney;
	}

	/**
	 * @return the createdate
	 */
	public java.lang.String getCreatedate() {
		return createdate;
	}

	/**
	 * @param createdate the createdate to set
	 */
	public void setCreatedate(java.lang.String createdate) {
		this.createdate = createdate;
	}

}
