package cn.gov.xnc.admin.copartner.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerOrderServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 合作伙伴相关订单
 * @author zero
 * @date 2017-01-12 15:08:48
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/copartnerOrderController")
public class CopartnerOrderController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CopartnerOrderController.class);

	@Autowired
	private CopartnerOrderServiceI copartnerOrderService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 合作伙伴相关订单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView copartnerOrder(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/copartnerOrderList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(CopartnerOrderEntity copartnerOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(CopartnerOrderEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, copartnerOrder, request.getParameterMap());
		this.copartnerOrderService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除合作伙伴相关订单
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(CopartnerOrderEntity copartnerOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		copartnerOrder = systemService.getEntity(CopartnerOrderEntity.class, copartnerOrder.getId());
		message = "合作伙伴相关订单删除成功";
		copartnerOrderService.delete(copartnerOrder);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加合作伙伴相关订单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(CopartnerOrderEntity copartnerOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(copartnerOrder.getId())) {
			message = "合作伙伴相关订单更新成功";
			CopartnerOrderEntity t = copartnerOrderService.get(CopartnerOrderEntity.class, copartnerOrder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(copartnerOrder, t);
				copartnerOrderService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "合作伙伴相关订单更新失败";
			}
		} else {
			message = "合作伙伴相关订单添加成功";
			copartnerOrderService.save(copartnerOrder);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 合作伙伴相关订单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(CopartnerOrderEntity copartnerOrder, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(copartnerOrder.getId())) {
			copartnerOrder = copartnerOrderService.getEntity(CopartnerOrderEntity.class, copartnerOrder.getId());
			req.setAttribute("copartnerOrderPage", copartnerOrder);
		}
		return new ModelAndView("admin/salesman/copartnerOrder");
	}
}
