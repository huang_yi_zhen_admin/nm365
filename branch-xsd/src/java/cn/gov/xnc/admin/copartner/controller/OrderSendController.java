package cn.gov.xnc.admin.copartner.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerEntity;
import cn.gov.xnc.admin.copartner.service.OrderSendServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 派单记录表
 * @author zero
 * @date 2017-01-12 15:13:23
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderSendController")
public class OrderSendController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderSendController.class);

	@Autowired
	private OrderSendServiceI orderSendService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 派单记录表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView orderSendEntity(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/orderSendEntityList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderSendEntity orderSendEntity,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user= ResourceUtil.getSessionUserName();
		String userType= user.getType();
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class, dataGrid);
		if("6".equals(userType)){//如果是第三方发货商非平台方注册的供应商
			cq.eq("receiver", user);
		}
		if(!"3".equals(userType) && !"6".equals(userType)){//员工和管理员
			TSCompany company = user.getCompany();
			cq.eq("distributecompanyid", company);
		}
		if("3".equals(userType)){//客户不让看
			cq.eq("distributecompanyid", null);
		}
		cq.addOrder("createdate", SortDirection.desc);
		cq.add();
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderSendEntity, request.getParameterMap());
		this.orderSendService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除派单记录表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(OrderSendEntity orderSendEntity, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		orderSendEntity = systemService.getEntity(OrderSendEntity.class, orderSendEntity.getId());
		message = "派单记录表删除成功";
		orderSendService.delete(orderSendEntity);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加派单记录表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderSendEntity orderSendEntity, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(orderSendEntity.getId())) {
			message = "派单记录表更新成功";
			OrderSendEntity t = orderSendService.get(OrderSendEntity.class, orderSendEntity.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(orderSendEntity, t);
				orderSendService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "派单记录表更新失败";
			}
		} else {
			message = "派单记录表添加成功";
			orderSendService.save(orderSendEntity);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 派单记录表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderSendEntity orderSendEntity, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(orderSendEntity.getId())) {
			orderSendEntity = orderSendService.getEntity(OrderSendEntity.class, orderSendEntity.getId());
			req.setAttribute("orderSendEntityPage", orderSendEntity);
		}
		return new ModelAndView("admin/salesman/orderSendEntity");
	}
	
	/**
	 * 派单
	 * @return
	 */
	@RequestMapping(value = "sendPaybillsOrder")
	@ResponseBody
	public AjaxJson sendPaybillsOrder(HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		
		String billsorderids = ResourceUtil.getParameter("billsorderids");
		if(!StringUtil.isEmpty(billsorderids)){
			
			List<PayBillsOrderEntity> billsOrderList = payBillsOrderService.getBillsOrderListByIds(billsorderids);
			j.setMsg(orderSendService.sendPaybillsOrder(billsOrderList, req));
		}
		
		return j;
	}
	
	/**
	 * 派单
	 * @return
	 */
	@RequestMapping(value = "sendOrders")
	@ResponseBody
	public AjaxJson sendOrderList(UserCopartnerEntity userCopartner, HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		
		String orderids = ResourceUtil.getParameter("orderids");
		String orderType = ResourceUtil.getParameter("ordertype");
		if(!StringUtil.isEmpty(orderids)){
			
			if("1".equals(orderType)){//直接小单派单
				List<OrderEntity> orderList = orderService.getOrderListByIds(orderids);
				j.setMsg(orderSendService.sendOrders(orderList, req));
				
			}else{//大单派单需要分解成小单
				
				List<PayBillsOrderEntity> billsOrderList = payBillsOrderService.getBillsOrderListByIds(orderids);
				j.setMsg(orderSendService.sendPaybillsOrder(billsOrderList, req));
			}
			
		}else{
			j.setMsg("请选择需要派发的订单！");
			j.setSuccess(false);
		}
		
		return j;
	}
	
}
