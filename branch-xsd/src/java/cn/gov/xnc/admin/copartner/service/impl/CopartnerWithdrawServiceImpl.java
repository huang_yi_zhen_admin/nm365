package cn.gov.xnc.admin.copartner.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerWithdrawServiceI;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

@Service("copartnerWithdrawService")
@Transactional
public class CopartnerWithdrawServiceImpl extends CommonServiceImpl implements CopartnerWithdrawServiceI {

	@Override
	public void withdrawCaculater(List<CopartnerOrderEntity> copartnerOrderList, TSCompany company) throws Exception {
		
		
	}

	@Override
	public StatisPageVO statisWithdrawOrder(TSCompany company, HttpServletRequest request) {
		String state = request.getParameter("state");
		if(StringUtil.isEmpty(state)){
			state = "1,2,3";
		}
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("state", state);
		
		StatisPageVO statisPageVO = statisWithdrawOrderBySql(company, param);
		
		return statisPageVO;
	}
	
	/**
	 * 订单统计共有函数
	 * @param company
	 * @param param 统计订单参数，请根据需要调整，目前支持的参数：
	 * state : 订单状态  identifieror : 订单号  customername ： 收件人 telephone ： 收件人电话  createdate1,createdate2 ： 时间范围, yewuid : 业务员id
	 * @return
	 */
	private StatisPageVO statisWithdrawOrderBySql(TSCompany company, HashMap<String, String> param) {
		String state = param.get("state");
//		String identifieror = param.get("identifieror");
		String usercopartnerid = param.get("usercopartnerid.id");
		String createdate1 = param.get("createdate1");//申请结算时间
		String createdate2 = param.get("createdate2");
		String createdate3 = param.get("createdate3");//实际结算时间
		String createdate4 = param.get("createdate4");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.waitToDrawMoney),0) value2 FROM xnc_copartner_withdraw_order a ");
		Sql.append("WHERE a.companyid = '" + company.getId() + "'");
		
		//筛选需要审核的状态
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") > 0){
			
			String[] states = state.trim().split(",");
			StringBuffer tmp = new StringBuffer();
			
			for (int i = 0; i < states.length; i++) {
				if(tmp.length() == 0){
					tmp.append("'");
					tmp.append(states[i]);
					tmp.append("'");
				}else{
					tmp.append(",'");
					tmp.append(states[i]);
					tmp.append("'");
				}
			}
			Sql.append(" AND a.status in (" + tmp.toString() + ")");//全部
			
		}
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") == -1){
			
			Sql.append(" AND a.status = '" + state + "' ");
			
		}
//		if(StringUtil.isNotEmpty(identifieror) 
//				&& StringUtil.isNotEmpty(identifieror.trim())){
//			
//			Sql.append(" AND a.identifieror like '%" + identifieror.trim() + "%' ");//具体订单
//			
//		}
		if(StringUtil.isNotEmpty(usercopartnerid) 
				&& StringUtil.isNotEmpty(usercopartnerid.trim())){
			
			Sql.append(" AND a.userCopartnerId = '" + usercopartnerid.trim() + "' ");//具体订单
			
		}

		//客户提交申请结算时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append(" AND a.createdate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append(" AND a.createdate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append(" AND a.createdate <= '" + createdate2 + "'" );
		}
		
		//管理人员实际结算时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append(" AND a.updatetime BETWEEN '" + createdate3 + "' AND '" + createdate4 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append(" AND a.updatetime >= '" + createdate3 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append(" AND a.updatetime <= '" + createdate4 + "'" );
		}
		
		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		return statisPageVO;
	}
	
}