package cn.gov.xnc.admin.copartner.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

public interface CopartnerWithdrawServiceI extends CommonService{
	
	/**
	 * 结算计算器，发货商每次发货系统都会把发货单进行汇总计算，待结算使用
	 * @param copartnerOrderList
	 * @param company
	 * @throws Exception
	 */
	public void withdrawCaculater(List<CopartnerOrderEntity> copartnerOrderList, TSCompany company) throws Exception;
	
	/**
	 * 统计发货结算申请
	 * @param company
	 * @param request
	 * @return
	 */
	public StatisPageVO statisWithdrawOrder(TSCompany company, HttpServletRequest request);

}
