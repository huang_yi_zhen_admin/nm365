package cn.gov.xnc.admin.weixin.mp.entity.common;


import cn.gov.xnc.admin.weixin.mp.utils.josn.WxMpGsonBuilder;

public class WxAccessToken {
	
	
	  /**获取到的凭证 */
	  private String accessToken;
	  /**凭证有效时间，单位：秒 */
	  private int expiresIn = -1;


	  /**
	 * @return 获取到的凭证
	 */
	public String getAccessToken() {
		return accessToken;
	}


	/**
	 * @param 获取到的凭证
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return 凭证有效时间，单位：秒
	 */
	public int getExpiresIn() {
		return expiresIn;
	}

	/**
	 * @param 凭证有效时间，单位：秒
	 */
	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}





	public static WxAccessToken fromJson(String json) {
		    return WxMpGsonBuilder.create().fromJson(json, WxAccessToken.class);
	  }
	  
}
