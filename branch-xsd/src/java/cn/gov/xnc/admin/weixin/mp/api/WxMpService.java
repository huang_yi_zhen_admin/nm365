package cn.gov.xnc.admin.weixin.mp.api;

import cn.gov.xnc.admin.weixin.mp.entity.result.WxMpOAuth2AccessToken;

/**
 * 微信API的Service
 */
public interface  WxMpService {

	
	 /**
	   * <pre>
	   * OAuth2.0鉴权服务号用code换取oauth2的access token
	   * 详情请见: http://mp.weixin.qq.com/wiki/index.php?title=网页授权获取用户基本信息
	   * @param code 微信返回用户code
	   * @param appId 微信appid
	   * @param secret 微信 AppSecret
	   * </pre>
	   */
	WxMpOAuth2AccessToken oauth2getAccessToken(String code , String appId  , String secret) throws Exception;
	
  /**
	* 获取access_token, 不强制刷新access_token
	*
	* @see #getAccessToken(boolean)
	*/
	String getAccessToken(  String  appId , String  secret  ) throws Exception;
	
	
	  /**
	   * <pre>
	   * 获取access_token，本方法线程安全
	   * 且在多线程同时刷新时只刷新一次，避免超出2000次/日的调用次数上限
	   *
	   * 另：本service的所有方法都会在access_token过期是调用此方法
	   *
	   * 程序员在非必要情况下尽量不要主动调用此方法
	   *
	   * 详情请见: http://mp.weixin.qq.com/wiki/index.php?title=获取access_token
	   * </pre>
	   *
	   * @param forceRefresh 强制刷新
	   */
	 String getAccessToken(boolean forceRefresh  , String  appId , String  secret ) throws Exception;
	
	 /**
		* 获取jsapi_ticket, 不强制刷新jsapi_ticket
		*
		* @see #getAccessToken(boolean)
		*/
	 String getJsapiTicket(String  accessToken ) throws Exception ;
	 
	 /**
		* 获取jsapi_ticket, 强制刷新jsapi_ticket
		*
		* @param forceRefresh 强制刷新
		*/
	 String getJsapiTicket(boolean forceRefresh  ,String  accessToken ) throws Exception ;
}
