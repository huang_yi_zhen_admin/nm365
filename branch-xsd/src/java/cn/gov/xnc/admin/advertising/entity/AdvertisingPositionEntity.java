package cn.gov.xnc.admin.advertising.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

/**   
 * @Title: Entity
 * @Description: 广告位管理
 * @author zero
 * @date 2016-10-01 02:26:45
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_advertising_position", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class AdvertisingPositionEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**产品*/
	private ProductEntity product;
	/**排序值*/
	private java.lang.Integer sort;
	/**广告导图*/
	private java.lang.String advertisepic;
	/**广告外链*/
	private java.lang.String advertiseurl;
	
	/**公司信息*/
	private TSCompany company;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  管理产品
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT")
	public ProductEntity getProduct(){
		return this.product;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  管理产品
	 */
	public void setProduct(ProductEntity product){
		this.product = product;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  排序值
	 */
	@Column(name ="SORT",nullable=true,precision=10,scale=0)
	public java.lang.Integer getSort(){
		return this.sort;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  排序值
	 */
	public void setSort(java.lang.Integer sort){
		this.sort = sort;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  广告导图
	 */
	@Column(name ="ADVERTISEPIC",nullable=true,length=600)
	public java.lang.String getAdvertisepic(){
		return this.advertisepic;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  广告导图
	 */
	public void setAdvertisepic(java.lang.String advertisepic){
		this.advertisepic = advertisepic;
	}
	
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}

	@Column(name ="ADVERTISEURL",nullable=true,length=1000)
	public java.lang.String getAdvertiseurl() {
		return advertiseurl;
	}

	public void setAdvertiseurl(java.lang.String advertiseurl) {
		this.advertiseurl = advertiseurl;
	}
}
