package cn.gov.xnc.admin.kuaidi.service;


import java.util.Map;

import cn.gov.xnc.system.core.common.model.json.AjaxJson;



public interface  Kuaid100ServiceI {
	
	/**
	 * 快递单号查询
	 * @param logisticscompany 快递公司
	 * @param logisticsnumber  快递单号，多单号使用,隔开
	 * @return
	 * @throws Exception
	 */
	public AjaxJson getkuaidi100(  String logisticscompany    ,String logisticsnumberS );
	
	//public List getNkuaidi100(  String logisticscompany    ,String logisticsnumberS );
		
	public void  upkuaidi(  String orderId    ,  Map<String, Object>  kuaidi ,boolean up );
}
