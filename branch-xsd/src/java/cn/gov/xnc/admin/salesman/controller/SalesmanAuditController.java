package cn.gov.xnc.admin.salesman.controller;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.UserCopartnerAccountEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerWithdrawServiceI;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;
import cn.gov.xnc.admin.salesman.entity.SalesmanAuditEntity;
import cn.gov.xnc.admin.salesman.entity.SalesmanCommissionAuditEntity;
import cn.gov.xnc.admin.salesman.entity.SalesmanCommissionEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanAuditServiceI;
import cn.gov.xnc.admin.salesman.service.UserDrawmoneyServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserSalesmanEntity;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 业务结算流水
 * @author zero
 * @date 2016-11-27 19:14:04
 * @version V1.0
 */
@Scope("prototype")
@Controller
@RequestMapping("/salesmanAuditController")
public class SalesmanAuditController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SalesmanAuditController.class);

	@Autowired
	private SalesmanAuditServiceI salesmanAuditService;
	@Autowired
	private PayBillsServiceI payBillsService;
	@Autowired
	private UserDrawmoneyServiceI userDrawmoneyService;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private CopartnerWithdrawServiceI copartnerWithdrawService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 业务结算流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView salesmanAudit(HttpServletRequest request) {
		TSUser user =  ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		
		request.setAttribute("statisSalmanAudit", salesmanAuditService.statisSalemanAudit(company, request));
		request.setAttribute("statisPaybills", payBillsService.statisPaybills(company, request));
		request.setAttribute("statisDrawmoney", userDrawmoneyService.statisUserDrawmoney(company, request));
		//request.setAttribute("statisCancelOrder", orderService.statisCancelOrder(company, request));
		request.setAttribute("statisWithdrawOrder", copartnerWithdrawService.statisWithdrawOrder(company, request));
		
		return new ModelAndView("admin/salesman/salesmanAuditList");
	}
	
	/**
	 * 业务结算流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "salesmanAccont")
	public ModelAndView salemanAccont(HttpServletRequest request) {
		TSUser user =  ResourceUtil.getSessionUserName();
		user = systemService.findUniqueByProperty(TSUser.class, "id", user.getId());
		request.setAttribute("user", user);
		
		if("4".equals(user.getType())){//业务员
			return new ModelAndView("admin/account/salesmanAcountInfo");
		}
		
		if("6".equals(user.getType())){//发货员
			
			CriteriaQuery cq = new CriteriaQuery(UserCopartnerAccountEntity.class);
			//查询条件组装器
			cq.eq("usercopartnerid", user);
			cq.add();
			
			List<UserCopartnerAccountEntity> copartnerAccountList = systemService.getListByCriteriaQuery(cq, false);
			
			if(null != copartnerAccountList && copartnerAccountList.size() > 0){
				request.setAttribute("copartnerAccount", copartnerAccountList.get(0));
			}
			
			return new ModelAndView("admin/account/copartnerAcountInfo");
		}
		
		return new ModelAndView("admin/account/salesmanAcountInfo");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(SalesmanAuditEntity salesmanAudit,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(SalesmanAuditEntity.class, dataGrid);
		//查询条件组装器
		cq.eq("ywuser", user);
		if(null != salesmanAudit.getCheckuser() && StringUtil.isNotEmpty(salesmanAudit.getCheckuser().getRealname())){
			cq.like("checkuser.realname", "%" + salesmanAudit.getCheckuser().getRealname() + "%");
//			salesmanAudit.getCheckuser().setRealname(null);
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,sdf));
		}
		cq.add();
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, salesmanAudit, request.getParameterMap());
		this.salesmanAuditService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除业务结算流水
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(SalesmanAuditEntity salesmanAudit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		salesmanAudit = systemService.getEntity(SalesmanAuditEntity.class, salesmanAudit.getId());
		message = "业务结算流水删除成功";
		salesmanAuditService.delete(salesmanAudit);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加业务结算流水
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(SalesmanAuditEntity salesmanAudit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(salesmanAudit.getId())) {
			message = "业务结算流水更新成功";
			SalesmanAuditEntity t = salesmanAuditService.get(SalesmanAuditEntity.class, salesmanAudit.getId());
			
			if("2".equals(t.getState())){
				message ="已经通过数据不能修改";
				j.setMsg(message);
				return j;
			}
			try {
				MyBeanUtils.copyBeanNotNull2Bean(salesmanAudit, t);
				salesmanAuditService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "业务结算流水更新失败";
				j.setSuccess(false);
			}
		} else {
			//获取对应的订单id
			String SalesmanID = request.getParameter("SalesmanID");

			String id[] = null ;
			if(StringUtil.isNotEmpty(SalesmanID) ){
				id  = SalesmanID.split(",");
			}
			
			TSUser user = ResourceUtil.getSessionUserName();
			
			CriteriaQuery cq = new CriteriaQuery(SalesmanCommissionEntity.class);
				cq.in("id", id);
				cq.eq("ywuser", user);
				cq.eq("company", user.getCompany());
				cq.eq("received","1");
				cq.add();
			List<SalesmanCommissionEntity> sList = systemService.getListByCriteriaQuery(cq,false);	
			BigDecimal ordermoney = new BigDecimal("0.00");
			
			//从新计算订单总额价格
			if( sList != null && sList.size() > 0 ){
				for( SalesmanCommissionEntity s : sList ){
					SalesmanID +=s.getId()+",";
					ordermoney = ordermoney.add(s.getMoney());
				}
			}
			
			//获取当前用户的账户信息
			UserSalesmanEntity tsuserSalesman  = systemService.get(UserSalesmanEntity.class, user.getTsuserSalesman().getId());
			if( (tsuserSalesman.getFrozenmoney()).compareTo(ordermoney) == -1   ){
				message = "业务结算流水添加失败，订单总额大于锁定金额";
				j.setMsg(message);
				j.setSuccess(false);
				return j;
			}
			
			
			salesmanAudit.setYwuser(user);
			salesmanAudit.setCompany(user.getCompany());
			salesmanAudit.setFrozenmoney(tsuserSalesman.getFrozenmoney().subtract(ordermoney));
			salesmanAudit.setOrdermoney(ordermoney);
			salesmanAudit.setState("1");

			tsuserSalesman.setFrozenmoney(tsuserSalesman.getFrozenmoney().subtract(ordermoney));
			
			
			message = "业务结算流水添加成功";
			salesmanAuditService.save(salesmanAudit);
			systemService.saveOrUpdate(tsuserSalesman);
			
			List<SalesmanCommissionAuditEntity> addList = new ArrayList<SalesmanCommissionAuditEntity>();
			
			List<SalesmanCommissionEntity> addsList = new ArrayList<SalesmanCommissionEntity>();
			
			if( sList != null && sList.size() > 0 ){
				for( SalesmanCommissionEntity s : sList ){
						SalesmanCommissionAuditEntity add = new SalesmanCommissionAuditEntity();
						//SalesmanAuditEntity addSalesmanAudit = new SalesmanAuditEntity();
						//addSalesmanAudit.setId(salesmanAudit.getId());
						add.setAuditid(salesmanAudit);
						add.setSalesmanid(s);
					addList.add(add);
						s.setReceived("2");
					addsList.add(s);
				}
				systemService.batchSave(addList);
				systemService.batchUpdate(addsList);
			}
			
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 业务结算流水列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(SalesmanAuditEntity salesmanAudit, HttpServletRequest req) {
		//计算当前可提现的流水金额
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(SalesmanCommissionEntity.class);
			cq.eq("ywuser", user);
			cq.eq("company", user.getCompany());
			cq.eq("received","1");
			cq.add();
		List<SalesmanCommissionEntity> sList = systemService.getListByCriteriaQuery(cq,false);	
		//订单id
		String SalesmanID = "";
		//订单总额
		BigDecimal ordermoney = new BigDecimal("0.00");
		if( sList != null && sList.size() > 0 ){
			for( SalesmanCommissionEntity s : sList ){
				SalesmanID +=s.getId()+",";
				ordermoney = ordermoney.add(s.getMoney());
			}
		}
		req.setAttribute("SalesmanID", SalesmanID);
		req.setAttribute("ordermoney", ordermoney);
		req.setAttribute("yewu", user.getRealname());

		return new ModelAndView("admin/salesman/salesmanAudit");
	}
	
	
	
	/**
	 * 业务审核 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "salesman")
	public ModelAndView salesman(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/salesmanList");
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "checkadatagrid")
	public void checkadatagrid(SalesmanAuditEntity salesmanAudit,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		
		CriteriaQuery cq = new CriteriaQuery(SalesmanAuditEntity.class, dataGrid);
		//查询条件组装器
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, salesmanAudit, request.getParameterMap());
		this.salesmanAuditService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 业务结算流水审核核定
	 * 
	 * @return
	 */
	@RequestMapping(value = "checkaddorupdate")
	public ModelAndView checkaddorupdate(SalesmanAuditEntity salesmanAudit, HttpServletRequest req) {
		//计算当前可提现的流水金额
		if (StringUtil.isNotEmpty(salesmanAudit.getId())) {
			salesmanAudit = systemService.getEntity(SalesmanAuditEntity.class, salesmanAudit.getId());
			
			
			req.setAttribute("salesmanAuditPage", salesmanAudit);
		}
		return new ModelAndView("audit/adminSalesmanAudit");
	}
	
	
	/**
	 * 结算结算审核更新
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "checksave")
	@ResponseBody
	public AjaxJson checksave(SalesmanAuditEntity salesmanAudit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(salesmanAudit.getId())) {
			
		
			message = "业务结算流水更新成功";
			SalesmanAuditEntity t = salesmanAuditService.get(SalesmanAuditEntity.class, salesmanAudit.getId());
			if("2".equals(t.getState())){
				message ="已经通过数据不能修改";
				j.setMsg(message);
				return j;
			}
			
			String 	stateold = t.getState();	

			try {
				TSUser checkuser = ResourceUtil.getSessionUserName();
				
				salesmanAudit.setCheckuser(checkuser);
				salesmanAudit.setCheckdate(DateUtils.getDate());
				
				MyBeanUtils.copyBeanNotNull2Bean(salesmanAudit, t);
				
				
				salesmanAuditService.saveOrUpdate(t);
				
				//审核通过，
				if("1".equals(stateold) && "2".equals(salesmanAudit.getState())){
					//更新个人账户的可提现金额,审核金额为最终可提现金额
					TSUser user = systemService.get(TSUser.class, t.getYwuser().getId());
					//获取当前用户的账户信息
					UserSalesmanEntity tsuserSalesman  = systemService.get(UserSalesmanEntity.class, user.getTsuserSalesman().getId());
					tsuserSalesman.setWithdrawalsmoney( tsuserSalesman.getWithdrawalsmoney().add(t.getCheckmoney()) );
					systemService.saveOrUpdate(tsuserSalesman);
					//更新对应的订单情况
					List<SalesmanCommissionAuditEntity> scList = systemService.findByProperty(SalesmanCommissionAuditEntity.class, "auditid", t);
					
					List<SalesmanCommissionEntity> upList = new ArrayList<SalesmanCommissionEntity>();
					
					if( scList != null && scList.size() > 0){
						for(SalesmanCommissionAuditEntity sc : scList ){
							SalesmanCommissionEntity salesmanid = sc.getSalesmanid();
							salesmanid.setReceived("3");
							upList.add(salesmanid);
						}
						systemService.batchUpdate(upList);
					}
				}
				
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "业务结算流水更新失败";
			}
		}
			
		j.setMsg(message);
		return j;
	}

	
	
	@RequestMapping(value= "statisSalemanAudit")
	@ResponseBody
	public AjaxJson statisSalemanAudit(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计提现审核单失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
			StatisPageVO statispage = salesmanAuditService.statisSalemanAudit(company, req);
			json.setObj(statispage);
			
			json.setSuccess(true);
			json.setMsg("统计提现审核成功！");
			
		}
		
		return json;
	}
	
	
	
	
	
	
	
	
	
}
