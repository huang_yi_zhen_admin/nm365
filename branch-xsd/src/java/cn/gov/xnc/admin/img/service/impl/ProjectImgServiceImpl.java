package cn.gov.xnc.admin.img.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.img.service.ProjectImgServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("projectImgService")

public class ProjectImgServiceImpl extends CommonServiceImpl implements ProjectImgServiceI {
	
}