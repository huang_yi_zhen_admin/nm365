package cn.gov.xnc.admin.airag.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.airag.entity.AiragWeatherEntity;
import cn.gov.xnc.admin.airag.service.AiragWeatherServiceI;

/**   
 * @Title: Controller
 * @Description: 大气候天气数据
 * @author zero
 * @date 2017-06-12 16:35:17
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/airagWeatherController")
public class AiragWeatherController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AiragWeatherController.class);

	@Autowired
	private AiragWeatherServiceI airagWeatherService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 大气候天气数据列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView airagWeather(HttpServletRequest request) {
		return new ModelAndView("admin/airag/airagWeatherList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(AiragWeatherEntity airagWeather,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(AiragWeatherEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, airagWeather, request.getParameterMap());
		this.airagWeatherService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除大气候天气数据
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(AiragWeatherEntity airagWeather, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		airagWeather = systemService.getEntity(AiragWeatherEntity.class, airagWeather.getId());
		message = "大气候天气数据删除成功";
		airagWeatherService.delete(airagWeather);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加大气候天气数据
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(AiragWeatherEntity airagWeather, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(airagWeather.getId())) {
			message = "大气候天气数据更新成功";
			AiragWeatherEntity t = airagWeatherService.get(AiragWeatherEntity.class, airagWeather.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(airagWeather, t);
				airagWeatherService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "大气候天气数据更新失败";
			}
		} else {
			message = "大气候天气数据添加成功";
			airagWeatherService.save(airagWeather);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 大气候天气数据列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(AiragWeatherEntity airagWeather, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(airagWeather.getId())) {
			airagWeather = airagWeatherService.getEntity(AiragWeatherEntity.class, airagWeather.getId());
			req.setAttribute("airagWeatherPage", airagWeather);
		}
		return new ModelAndView("admin/airag/airagWeather");
	}
}
