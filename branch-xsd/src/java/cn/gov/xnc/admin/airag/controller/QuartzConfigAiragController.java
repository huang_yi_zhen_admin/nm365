package cn.gov.xnc.admin.airag.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.admin.airag.service.QuartzConfigAiragServiceI;

/**   
 * @Title: Controller
 * @Description: 大气候数据自动抓取配置
 * @author zero
 * @date 2017-06-12 16:34:06
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/quartzConfigAiragController")
public class QuartzConfigAiragController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(QuartzConfigAiragController.class);

	@Autowired
	private QuartzConfigAiragServiceI quartzConfigAiragService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 大气候数据自动抓取配置列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView quartzConfigAirag(HttpServletRequest request) {
		return new ModelAndView("admin/airag/quartzConfigAiragList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(QuartzConfigAiragEntity quartzConfigAirag,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(QuartzConfigAiragEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, quartzConfigAirag, request.getParameterMap());
		this.quartzConfigAiragService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除大气候数据自动抓取配置
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(QuartzConfigAiragEntity quartzConfigAirag, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		quartzConfigAirag = systemService.getEntity(QuartzConfigAiragEntity.class, quartzConfigAirag.getId());
		message = "大气候数据自动抓取配置删除成功";
		quartzConfigAiragService.delete(quartzConfigAirag);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加大气候数据自动抓取配置
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(QuartzConfigAiragEntity quartzConfigAirag, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(quartzConfigAirag.getId())) {
			message = "大气候数据自动抓取配置更新成功";
			QuartzConfigAiragEntity t = quartzConfigAiragService.get(QuartzConfigAiragEntity.class, quartzConfigAirag.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(quartzConfigAirag, t);
				quartzConfigAiragService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "大气候数据自动抓取配置更新失败";
			}
		} else {
			message = "大气候数据自动抓取配置添加成功";
			quartzConfigAiragService.save(quartzConfigAirag);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 大气候数据自动抓取配置列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(QuartzConfigAiragEntity quartzConfigAirag, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(quartzConfigAirag.getId())) {
			quartzConfigAirag = quartzConfigAiragService.getEntity(QuartzConfigAiragEntity.class, quartzConfigAirag.getId());
			req.setAttribute("quartzConfigAiragPage", quartzConfigAirag);
		}
		return new ModelAndView("admin/airag/quartzConfigAirag");
	}
}
