package cn.gov.xnc.admin.airag.service;

import java.util.List;

import cn.gov.xnc.admin.airag.entity.AiragSoilEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface AiragSoilServiceI extends CommonService{
	
	public void saveSoilList(List<AiragSoilEntity> list, QuartzConfigAiragEntity config);

}
