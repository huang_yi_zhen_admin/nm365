package cn.gov.xnc.admin.airag.task;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.gov.xnc.admin.airag.entity.AiragPictrueEntity;
import cn.gov.xnc.admin.airag.entity.AiragSoilEntity;
import cn.gov.xnc.admin.airag.entity.AiragWeatherEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.admin.airag.service.AiragPictrueServiceI;
import cn.gov.xnc.admin.airag.service.AiragSoilServiceI;
import cn.gov.xnc.admin.airag.service.AiragWeatherServiceI;
import cn.gov.xnc.admin.airag.service.QuartzConfigAiragServiceI;
import cn.gov.xnc.admin.airag.util.AiragUtils;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.service.SystemService;



public class AiragTask extends BaseController {
	
	
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private QuartzConfigAiragServiceI quartzConfigAiragService;
	
	
	@Autowired
	private AiragPictrueServiceI airagPictrueService;
	
	@Autowired
	private AiragSoilServiceI airagSoilService;
	
	
	@Autowired
	private AiragWeatherServiceI airagWeatherService;

	public AiragTask() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	public void doAiragData(){
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!任务进行中。。。");
		
		List<QuartzConfigAiragEntity> joblist = quartzConfigAiragService.getJobList();
		
		if( null != joblist && joblist.size() > 0 ){
			
			QuartzConfigAiragEntity conf = joblist.get(0);
			String token = AiragUtils.getToken(conf);
			if( StringUtil.isNotEmpty(token) ){
		
				for( QuartzConfigAiragEntity c : joblist ){
					if( "getpictrue".equals(c.getInterfaceName() ) ){
						List<AiragPictrueEntity> list = AiragUtils.getAiragPictrue(token, c);
						if( null == list ){
							for( int i = 0; i < c.getFailTryTimes(); i++ ){
								list = AiragUtils.getAiragPictrue(token, c);
								if( null != list ){
									break;
								}
							}
						}
						if( null != list ){
							airagPictrueService.savePictrueList(list, c);
						}
					}else if( "getsoil".equals(c.getInterfaceName() )){
						List<AiragSoilEntity> list = AiragUtils.getAiragSoil(token, c);
						if( null == list ){
							for( int i = 0; i < c.getFailTryTimes(); i++ ){
								list = AiragUtils.getAiragSoil(token, c);
								if( null != list ){
									break;
								}
							}
						}
						if( null != list ){
							airagSoilService.saveSoilList(list, c);
						}
					}else if( "getweather".equals(c.getInterfaceName() )){
						List<AiragWeatherEntity> list = AiragUtils.getAiragWeather(token, c);
						if( null == list ){
							for( int i = 0; i < c.getFailTryTimes(); i++ ){
								list = AiragUtils.getAiragWeather(token, c);
								if( null != list ){
									break;
								}
							}
						}
						if( null != list ){
							airagWeatherService.saveWeatherList(list, c);
						}
					}
					
				}
			}
		
		
		}
		
	}

}
