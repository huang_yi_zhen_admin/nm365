package cn.gov.xnc.admin.airag.service;

import java.util.List;

import cn.gov.xnc.admin.airag.entity.AiragWeatherEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface AiragWeatherServiceI extends CommonService{
	
	public void saveWeatherList(List<AiragWeatherEntity> list, QuartzConfigAiragEntity config);

}
