package cn.gov.xnc.admin.zhifu.weixinPay;


import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import java.util.SortedMap;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;



import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;
import cn.gov.xnc.admin.product.controller.ProductController;
import cn.gov.xnc.admin.weixin.mp.api.WxMpPayService;
import cn.gov.xnc.admin.weixin.mp.api.WxMpService;

import cn.gov.xnc.admin.weixin.mp.entity.pay.WxPayDto;
import cn.gov.xnc.admin.weixin.mp.entity.pay.WxPayResult;
import cn.gov.xnc.admin.weixin.mp.entity.result.WxMpOAuth2AccessToken;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.constant.Globals;

import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.IpUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;

import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.CompanyThirdpartyEntity;

import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;



/**   
 * @Title: Controller
 * @Description: 微信支付基础信息
 * @author zero
 * @date 2016-04-13 20:22:17
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/WeixinPayController")
public class WeixinPayController extends BaseController {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductController.class);

	@Autowired
	private SystemService systemService;
	@Autowired
	private PayBillsServiceI payBillsService;
	
			
	@Autowired
	private WxMpPayService wxMpPayService ;
	
	@Autowired
	private WxMpService wxMpService ;
	
	
	private static String notifyurl = "/WeixinPayController/returnwxMPpay.do";		
	
	
	/**
	 * 获取微信的code
	 * @return
	 */
	@RequestMapping(params = "wxcode")
	public ModelAndView wxcode(HttpServletRequest request) {
		 // 用户同意授权后，能获取到code
        String code = request.getParameter("code");
        
        request.setAttribute("code", code);
		return new ModelAndView("admin/wxMPpay/wxMPcode");	
	}
	
	
	
	
	/**
	 * 生成微信支付 扫描支付
	 * 
	 * @return
	 */
	@RequestMapping(value = "weixinPay")
	public ModelAndView weixinPay(HttpServletRequest request) {

		String idS = request.getParameter("id");
		String type = request.getParameter("type");
		
		
		
		 TSUser user  = ResourceUtil.getSessionUserName();
		 if( user != null && StringUtil.isNotEmpty(user.getId()) ){
			 user = systemService.get(TSUser.class, user.getId());
		 }
		 CompanyThirdpartyEntity thirdpartyEntity   = user.getCompany().getCompanythirdparty(); 
		 CompanyPlatformEntity companyplatform   = systemService.get(CompanyPlatformEntity.class, user.getCompany().getCompanyplatform().getId()); 
		 if(thirdpartyEntity.wxMPpay()){
			//生成支付记录
			 
			 PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
		        payment.setIdentifier(IdWorker.generateSequenceNo());
		        payment.setClientid(user);
		        payment.setCompany(user.getCompany());
		      
		        payment.setType(type);
		        payment.setState("5");
		        payment.setPaymentmethod("2");
		    if( "1".equals(type)){
		    	payment  = payBillsService.payment(idS, user, payment);
		    }else if ( "2".equals(type) ){
		    	
		    	String paymoney = request.getParameter("paymoney");
		    	BigDecimal moneyPage = new BigDecimal(paymoney);
				payment.setAlreadypaidmoney(moneyPage);

		    	payment  = payBillsService.paymentUser( payment  );
		    }    
		        

			 try {
				//微信支付  扫描支付
				WxPayDto tpWxPay = new WxPayDto();
				tpWxPay.setProductId(payment.getId());
				tpWxPay.setSpbillCreateIp(IpUtil.getRealIp());
				tpWxPay.setBody(payment.getPayName());
				int transAmt = payment.getAlreadypaidmoney().multiply(new BigDecimal(100)).intValue();//乘以100(单位：分)  	
				if( "4028802f58149a320158149cdd570004".equals( payment.getCompany().getId()) ){
					 payment.setAlreadypaidmoney( new BigDecimal("0.01"));
					transAmt =1;
				 }
				tpWxPay.setTotalFee(transAmt);
				tpWxPay.setAttach("");
				//===微信支付  基础信息===
				tpWxPay.setAppid(thirdpartyEntity.getAppid());
				tpWxPay.setAppsecret(thirdpartyEntity.getAppsecret());
				tpWxPay.setMchId(thirdpartyEntity.getAppidsh());
				tpWxPay.setPartnerkey(thirdpartyEntity.getAppsecretsh());
				tpWxPay.setNotifyURL(companyplatform.getWebsite().replace("http://", "")+notifyurl);

				String codeurl = wxMpPayService.getCodeurl(tpWxPay);
				
				
				request.setAttribute("payment", payment.getAlreadypaidmoney());
				request.setAttribute("companyName", user.getCompany().getCompanyName());
				request.setAttribute("codeurl", codeurl);
			} catch (Exception e) {
				
			}
		 }
		 return new ModelAndView("admin/wxMPpay/wxMPpay");
	}
	
	/**
	 * 获取微信扫码支付的url
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "weixinPayUri")
	@ResponseBody
	public AjaxJson weixinPayUri(HttpServletRequest request) {
		AjaxJson json = new AjaxJson();
		json.setSuccess(true);
		String codeurl = "";
		String idS = request.getParameter("id");
		String type = request.getParameter("type");
		
		 TSUser user  = ResourceUtil.getSessionUserName();
		 if( user != null && StringUtil.isNotEmpty(user.getId()) ){
			 user = systemService.get(TSUser.class, user.getId());
		 }
		 CompanyThirdpartyEntity thirdpartyEntity   = user.getCompany().getCompanythirdparty(); 
		 CompanyPlatformEntity companyplatform   = systemService.get(CompanyPlatformEntity.class, user.getCompany().getCompanyplatform().getId());
		 if( thirdpartyEntity .wxMPpay()){
			 PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
		        payment.setIdentifier(IdWorker.generateSequenceNo());
		        payment.setClientid(user);
		        payment.setCompany(user.getCompany());
		        payment.setType(type);
		        payment.setState("5");
		        payment.setPaymentmethod("2");
		        payment  = payBillsService.payment(idS, user, payment);
		       
		      
				
		        
			 try {
				//微信支付  扫描支付
				WxPayDto tpWxPay = new WxPayDto();
				tpWxPay.setProductId(payment.getId());
				tpWxPay.setSpbillCreateIp(IpUtil.getRealIp());
				tpWxPay.setBody(payment.getPayName());
				int transAmt = payment.getAlreadypaidmoney().multiply(new BigDecimal(100)).intValue();//乘以100(单位：分)  	
				if( "4028802f58149a320158149cdd570004".equals( payment.getCompany().getId()) ){
					transAmt =1;
				 }
				tpWxPay.setTotalFee(transAmt);
				tpWxPay.setAttach("");
				//===微信支付  基础信息====
				tpWxPay.setAppid(thirdpartyEntity.getAppid());
				tpWxPay.setAppsecret(thirdpartyEntity.getAppsecret());
				tpWxPay.setMchId(thirdpartyEntity.getAppidsh());
				tpWxPay.setPartnerkey(thirdpartyEntity.getAppsecretsh());
				tpWxPay.setNotifyURL(companyplatform.getWebsite().replace("http://", "")+notifyurl);
				
				codeurl = wxMpPayService.getCodeurl(tpWxPay);
				
			} catch (Exception e) {

			}//获取微信的统一下单
			 
				
			 
			 json.setMsg(codeurl); 
		 }
		 return json;
	}
	


	/**
	 * 微信支付 jsApi 必须是
	 * 
	 * @return
	 */
	@RequestMapping(params = "weixinPayjsJson")
	@ResponseBody
	public AjaxJson weixinPayjsJson(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String idS = request.getParameter("id");
		String type = request.getParameter("type");
		// 用户同意授权后，能获取到code  OAuth2.0鉴权
        String code = request.getParameter("code");
		 TSUser user  = ResourceUtil.getSessionUserName();
		 if( user != null && StringUtil.isNotEmpty(user.getId()) ){
			 user = systemService.get(TSUser.class, user.getId());
		 }
		 
		 CompanyThirdpartyEntity thirdpartyEntity   = user.getCompany().getCompanythirdparty(); 
		 CompanyPlatformEntity companyplatform   = systemService.get(CompanyPlatformEntity.class, user.getCompany().getCompanyplatform().getId());
		 if( thirdpartyEntity.wxMPpay() && StringUtil.isNotEmpty(code) ){
		 
		 PayBillsEntity payment = new PayBillsEntity();   //支付单基础记录
	        payment.setIdentifier(IdWorker.generateSequenceNo());
	        payment.setClientid(user);
	        payment.setCompany(user.getCompany());
	        payment.setType(type);
	        payment.setState("5");
	        payment.setPaymentmethod("2");
	        payment  = payBillsService.payment(idS, user, payment);
		 
	     //获取公司的微信支付信息，暂时先使用天天农贸做测试
	    	

			 try {
				WxMpOAuth2AccessToken  oAuth = wxMpService.oauth2getAccessToken(code, thirdpartyEntity.getAppid(), thirdpartyEntity.getAppsecret()) ;
					
				 //微信支付jsApi
				WxPayDto tpWxPay = new WxPayDto();
				tpWxPay.setOpenid(oAuth.getOpenId());
				tpWxPay.setProductId(payment.getId());
				tpWxPay.setSpbillCreateIp(IpUtil.getRealIp());
				tpWxPay.setBody(payment.getPayName());
				int transAmt = payment.getAlreadypaidmoney().multiply(new BigDecimal(100)).intValue();//乘以100(单位：分)
				if( "4028802f58149a320158149cdd570004".equals( payment.getCompany().getId()) ){
					transAmt =1;
				 }
				
				tpWxPay.setTotalFee(transAmt);
				
				
				tpWxPay.setAttach("");
				//===微信支付  基础信息===
				tpWxPay.setAppid(thirdpartyEntity.getAppid());
				tpWxPay.setAppsecret(thirdpartyEntity.getAppsecret());
				tpWxPay.setMchId(thirdpartyEntity.getAppidsh());
				tpWxPay.setPartnerkey(thirdpartyEntity.getAppsecretsh());
				tpWxPay.setNotifyURL(companyplatform.getWebsite().replace("http://", "")+notifyurl);
				
				SortedMap<String, String>  weixinPayjsApi = wxMpPayService.getPackageJsApi(tpWxPay);
				j.setObj(weixinPayjsApi);
				
			} catch (Exception e) {
			}
		 }
		 return j;
	}
	
	

	
	
	/**
	 * 读取支付结果通知,微信扫描支付回调通知处理
	 * 
	 * @return
	 */
	@RequestMapping("/returnwxMPpay")
	public void returnwxMPpay(HttpServletRequest request,HttpServletResponse response ) {
		
		
		String inputLine;
		String notityXml = "";
		
		try {
			while ((inputLine = request.getReader().readLine()) != null) {
				notityXml += inputLine;
			}
			request.getReader().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		systemService.addLog("获取支微信POST过来反馈信息:"+notityXml, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		String resXml = "";
		try {
			WxPayResult wpr = wxMpPayService.getwxPayResult(notityXml);
			if("SUCCESS".equals(wpr.getResultCode())){
				PayBillsEntity payment = systemService.get(PayBillsEntity.class, wpr.getOutTradeNo());
				CompanyThirdpartyEntity companythirdparty = payment.getCompany().getCompanythirdparty();
				if( payment != null &&  wpr.getMchId().equals(companythirdparty.getAppidsh())   ){  //对应的订单信息
					if( "5".equals(payment.getState())){
						payment.setState("4");
						payment.setAuditdate(DateUtils.getDate());
						payment.setVoucher(wpr.getTransactionId());
						payBillsService.saveState(null, payment);
					}
				}
				//支付成功
				resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
				+ "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
			}else{
				resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
				+ "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
			}
			
		} catch (Exception e) {
			resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
					+ "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
		}
		//通知微信已经接到通知
	    response.setContentType("application/xml");  
		response.setHeader("Cache-Control", "no-store");
		
		try {
			PrintWriter pw=response.getWriter();
			pw.write(resXml.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	//判断供应商是否在系统开通微信支付
	@RequestMapping(params = "hasWxAuth2Pay")
	@ResponseBody
    public AjaxJson hasWxAuth2Pay(){
    	AjaxJson json = new AjaxJson();
    	json.setSuccess(false);
    	TSUser user  = ResourceUtil.getSessionUserName();
		 if( user != null && StringUtil.isNotEmpty(user.getId()) ){
			 user = systemService.get(TSUser.class, user.getId());
		 }
		 
		 CompanyThirdpartyEntity thirdpartyEntity   = user.getCompany().getCompanythirdparty(); 
		 
		 if(thirdpartyEntity.wxMPpay()){
			 json.setSuccess(true);
		 }
		 
		 return json;
    }
  
	
	
}
