package cn.gov.xnc.admin.dataStatistics.controller;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;

import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.TerritoryService;
import cn.gov.xnc.system.web.system.service.UserService;
import cn.gov.xnc.system.core.util.MyBeanUtils;


import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.service.ProductServiceI;


/**   
 * @Title: Controller
 * @Description: 数据统计查询
 * @author zero
 * @date 2016-06-22 16:19:51
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/dataStatisticsController")
public class DataStatisticsController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DataStatisticsController.class);

	//@Autowired
	//private DataStatisticsServiceI dataStatisticsService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private OrderServiceI orderService;

	@Autowired
	private UserService userService;
	@Autowired
	private TerritoryService territoryService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	/**
	 * 数据统计查询列表 页面跳转
	 * 
	 * @return
	 */
	
	@RequestMapping(value = "list")
	public ModelAndView list(HttpServletRequest request) {
		/**
		 *销售数据	   dataStatisticsController.do?list&gettype=ic 
		 *渠道销售情况         dataStatisticsController.do?list&gettype=qdxs
		 *产品销售情况         dataStatisticsController.do?list&gettype=cpbl
		 *区域消费数据         dataStatisticsController.do?list&gettype=yh
		 *物流数据查询         dataStatisticsController.do?list&gettype=wl
		 **/
		String gettype =request.getParameter("gettype");
		TSUser user = ResourceUtil.getSessionUserName();
		//获取全部销售果品信息
		CriteriaQuery cq_p = new CriteriaQuery(ProductEntity.class);
			//cq.eq("code", p.getCode());//产品编码作为唯一编码
		cq_p.eq("company", user.getCompany());
		List<ProductEntity> productlist = systemService.getListByCriteriaQuery(cq_p,false);//获取全部商品列表
			request.setAttribute("productList", productlist);
			
			
			
		CriteriaQuery cq_YW = new CriteriaQuery(TSUser.class);
			cq_YW.eq("company", user.getCompany());
			cq_YW.eq("type", "4");
		List<TSUser> listUserYW = userService.getListByCriteriaQuery(cq_YW,false);//业务员信息
			request.setAttribute("TSUserYWList", listUserYW);
			

		CriteriaQuery cq_CL = new CriteriaQuery(TSUser.class);
			//cq.eq("code", p.getCode());//产品编码作为唯一编码
			cq_CL.eq("company", user.getCompany());
			cq_CL.eq("type", "3");
		List<TSUser> listUsercq_CL = userService.getListByCriteriaQuery(cq_CL,false);//获取全部商品列表
			request.setAttribute("TSUserCLList", listUsercq_CL);
			

		if(StringUtil.isNotEmpty(gettype) && "ic".equals(gettype)){//月销售数据
			return new ModelAndView("admin/dataStatistics/ic");
		}else if(StringUtil.isNotEmpty(gettype) && "cpbl".equals(gettype)){//产品销售统计
			return new ModelAndView("admin/dataStatistics/cpbl");
		}else if(StringUtil.isNotEmpty(gettype) && "qdxs".equals(gettype)){//渠道销售情况
			return new ModelAndView("admin/dataStatistics/qdxs");
		}else if(StringUtil.isNotEmpty(gettype) && "yh".equals(gettype)){//地区销售情况
			//获取身份数据
			
			List<TSTerritory> list =  territoryService.findRootTerritoryByJbdc();
			request.setAttribute("provinceList", list);
			
			return new ModelAndView("admin/dataStatistics/yh");
		}else if(StringUtil.isNotEmpty(gettype) && "wl".equals(gettype)){//用户消费区域数据
			return new ModelAndView("admin/dataStatistics/wl");
		}else if(StringUtil.isNotEmpty(gettype) && "invoice".equals(gettype)){//发货图
			return new ModelAndView("admin/dataStatistics/invoice");
		}else if(StringUtil.isNotEmpty(gettype) && "yewu".equals(gettype)){//业务员数据
			return new ModelAndView("admin/dataStatistics/yewu");
		}
		return  null ;
	}
	


	/**
	 * 添加月销售数据统计查询
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getic")

	public void getic( HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid ) {
		
		String productid = request.getParameter("productid");
		String userCLid =request.getParameter("userCLid");
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		TSUser user = ResourceUtil.getSessionUserName();	
		String companyId = user.getCompany().getId();
		List<OrderEntity> salesOrderList  = orderService.salesTIC( userCLid , productid,  startTime, endTime ,companyId  );
		
		//根据分页构建分页list
		dataGrid.setTotal(salesOrderList.size());
		dataGrid.setResults(salesOrderList);

		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	

	
	
	/**
	 * 产品销售统计
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getcpbl")
	public void  getcpbl( HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid ) {
		
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		String userCLid =request.getParameter("userCLid");
		TSUser user = ResourceUtil.getSessionUserName();
		String companyId = user.getCompany().getId();
		List<OrderEntity> salesOrderList  =orderService.salesCPBL( companyId , userCLid ,  startTime, endTime );
		
		//根据分页构建分页list
		dataGrid.setTotal(salesOrderList.size());
		dataGrid.setResults(salesOrderList);

		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 渠道销售情况
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getqdxs")
	public void getqdxs( HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid ) {
		
		String productid =request.getParameter("productid");
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		TSUser user = ResourceUtil.getSessionUserName();	
		String companyId = user.getCompany().getId();
		
		List<OrderEntity> salesOrderList  = orderService.salesQDXS( companyId ,productid ,  startTime, endTime );

		if( salesOrderList != null && salesOrderList.size() > 0){
			for( OrderEntity o:salesOrderList ){
				TSUser t = new TSUser();
				if( StringUtil.isNotEmpty(o.getId())){
					t = systemService.findUniqueByProperty (TSUser.class,"id" ,o.getId());
			    }
				o.setClientid(t);
			}
		}

		//根据分页构建分页list
		dataGrid.setTotal(salesOrderList.size());
		dataGrid.setResults(salesOrderList);

		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 地区销售情况
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getyh")
	public void getyh( HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid) {
		
		String province = request.getParameter("province");// 省份信息
		
		String userCLid =request.getParameter("userCLid");
		
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		String companyId = user.getCompany().getId();

		List<OrderEntity> salesOrderList  = orderService.salesYH( companyId , userCLid , province,  startTime, endTime );
		if(salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o : salesOrderList){
				if( StringUtil.isNotEmpty(o.getProvince()) ){
					o.setProvince(o.getProvince().substring(0,2));
					if("黑龙".equals(o.getProvince())){
						o.setProvince("黑龙江");
					}else if ( "内蒙".equals(o.getProvince()) ){
						o.setProvince("内蒙古");
					}
				}else {
					o.setProvince("自提");
				}
				
			}
		}
		
		
		
		//根据分页构建分页list
		dataGrid.setTotal(salesOrderList.size());
		dataGrid.setResults(salesOrderList);

		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * 产品发货地区情况
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getinvoice")
	public void getinvoice( HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid) {
		
		String productid =request.getParameter("productid");
		String userCLid =request.getParameter("userCLid");
		
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		String companyId = user.getCompany().getId();
		String address = "";
		//存在地址信息
		if(  StringUtil.isNotEmpty(user.getCompany().getAddress())){
			address = user.getCompany().getAddress().trim().substring(0,2);
		}
		
		
		List<OrderEntity> salesOrderList  = orderService.salesdeparture( companyId , userCLid , productid,  startTime, endTime );
		
		
		if(salesOrderList != null && salesOrderList.size() > 0 ){
			for( OrderEntity o : salesOrderList){
				o.setPlatform(address);
				if( StringUtil.isNotEmpty(o.getProvince()) ){
					o.setProvince(o.getProvince().substring(0,2));
					if("黑龙".equals(o.getProvince())){
						o.setProvince("黑龙江");
					}else if ( "内蒙".equals(o.getProvince()) ){
						o.setProvince("内蒙古");
					}
				}else {
					o.setProvince(o.getPlatform());
				}
			}
		}

		//根据分页构建分页list
		dataGrid.setTotal(salesOrderList.size());
		dataGrid.setResults(salesOrderList);

		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * 业务情况统计
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getyewu")
	public void getyewu( HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid) {
		
		String productid = request.getParameter("productid");
		String userYWid =request.getParameter("userYWid");
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		TSUser user = ResourceUtil.getSessionUserName();	
		String companyId = user.getCompany().getId();
		List<OrderEntity> salesOrderList  = orderService.salesYewu( userYWid , productid,  startTime, endTime ,companyId  );
		
		//根据分页构建分页list
		dataGrid.setTotal(salesOrderList.size());
		dataGrid.setResults(salesOrderList);

		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	//物流数据状态查询
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagridwl")
	public void datagridwl(  OrderLogisticsEntity orderLogistics , HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(OrderLogisticsEntity.class, dataGrid);
		//采购商、收货人、电话、物流单号、物流状态、发货时间
		//查询条件组装器
		String identifieror ="";
		if(orderLogistics.getOrderid() != null &&  StringUtil.isNotEmpty(orderLogistics.getOrderid().getIdentifieror()) ){
			identifieror = orderLogistics.getOrderid().getIdentifieror();
		}
		String productname ="";
		if(orderLogistics.getOrderid() != null &&  StringUtil.isNotEmpty(orderLogistics.getOrderid().getProductname()) ){
			productname = orderLogistics.getOrderid().getProductname();
		}
		String customername ="";
		if(orderLogistics.getOrderid() != null &&  StringUtil.isNotEmpty(orderLogistics.getOrderid().getCustomername()) ){
			customername = orderLogistics.getOrderid().getCustomername();
		}
		String telephone ="";
		if(orderLogistics.getOrderid() != null &&  StringUtil.isNotEmpty(orderLogistics.getOrderid().getTelephone()) ){
			telephone = orderLogistics.getOrderid().getTelephone();
		}
		String realname="";
		if(orderLogistics.getOrderid() != null && orderLogistics.getOrderid().getClientid() != null && StringUtil.isNotEmpty(orderLogistics.getOrderid().getClientid().getRealname()) ){
			realname = orderLogistics.getOrderid().getClientid().getRealname();
		}
		cq.createAlias("orderid", "o"); 
	    if(  StringUtil.isNotEmpty(realname) ){
	    	 cq.createAlias("o.clientid", "cl") ;
	    	 cq.add( Restrictions.like("cl.realname", "%"+realname+"%" ) ) ;
	    }
	   
	    cq.add( Restrictions.eq("o.state", "3" ) );
	    if( StringUtil.isNotEmpty(identifieror) ){
	    	cq.add( Restrictions.eq("o.identifieror", identifieror ) ) ;
	    }
	    if( StringUtil.isNotEmpty(productname) ){
	    	cq.add( Restrictions.like("o.productname", "%"+productname +"%") ) ;
	    }
	    if( StringUtil.isNotEmpty(customername) ){
	    	cq.add( Restrictions.like("o.customername", "%"+customername +"%") ) ;
	    }
	    if( StringUtil.isNotEmpty(telephone) ){
	    	cq.add( Restrictions.like("o.telephone", "%"+telephone +"%") ) ;
	    }
	    cq.createAlias("o.company", "company");
	    cq.add( Restrictions.eq("company.id", user.getCompany().getId()) ) ;
	    
	    String logisticsdate1 = request.getParameter("logisticsdate1");
		String logisticsdate2 = request.getParameter("logisticsdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(logisticsdate1)&&StringUtil.isNotEmpty(logisticsdate2)){
			cq.between("logisticsdate", DateUtils.str2Date(logisticsdate1,sdf), DateUtils.str2Date(logisticsdate2,sdf));
		}else if( StringUtil.isNotEmpty(logisticsdate1) ){
			cq.ge("logisticsdate", DateUtils.str2Date(logisticsdate1,sdf));
		}else if( StringUtil.isNotEmpty(logisticsdate2) ){
			cq.le("logisticsdate", DateUtils.str2Date(logisticsdate2,sdf));
		}
		
		orderLogistics.setOrderid(null);
		cq.addOrder("logisticsdate", SortDirection.desc);
		//下单时间排序
		//cq.addOrder("createdate", SortDirection.desc);
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderLogistics, request.getParameterMap());
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
		
	}
	
	


	
}
