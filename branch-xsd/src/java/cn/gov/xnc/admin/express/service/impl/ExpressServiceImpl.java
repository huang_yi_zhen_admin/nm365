package cn.gov.xnc.admin.express.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.express.entity.ExpressEntity;
import cn.gov.xnc.admin.express.service.ExpressServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticsServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("expressService")
@Transactional
public class ExpressServiceImpl extends CommonServiceImpl implements ExpressServiceI {
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private OrderLogisticsServiceI orderLogisticsService;
	
	
	public AjaxJson setOrderLogistcs(String shippercode,String logisticcode,String ordercode,String printtype){
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq_e = new CriteriaQuery(ExpressEntity.class);
		cq_e.eq("ordercode", ordercode);
		cq_e.eq("logisticcode", logisticcode);
		cq_e.eq("shippercode", shippercode);
		cq_e.eq("success", 1);
		cq_e.createAlias("orderid", "order");
		cq_e.add(Restrictions.eq("order.company", user.getCompany()));
		List<ExpressEntity> expresslist = systemService.getListByCriteriaQuery(cq_e,false);//只会有1条
		if( null == expresslist || expresslist.size() != 1){
			j.setSuccess(false);
			j.setMsg("请确认回传快递单号是否正确,ordercode="+ordercode+" logisticcode="+logisticcode + " shippercode=" + shippercode);
			return j;
		}
		ExpressEntity express = expresslist.get(0);
		
		OrderEntity orderentity = systemService.findUniqueByProperty(OrderEntity.class,"id",express.getOrderid().getId());
		
		String orglogistcscompanys = ExpressCompany.getExpressCompanyName(express.getShippercode());
		if(StringUtil.isNotEmpty(orderentity.getLogisticscompany())){
			orglogistcscompanys = orderentity.getLogisticscompany() + "," + orglogistcscompanys;
		}
		orderentity.setLogisticscompany(orglogistcscompanys);
		
		String orglogistcsnums = logisticcode;
		if(StringUtil.isNotEmpty(orderentity.getLogisticsnumber())){
			orglogistcsnums = orderentity.getLogisticsnumber() + "," + orglogistcsnums;
		}
		orderentity.setLogisticsnumber(orglogistcsnums);
		
		//设置订单状态
		if( null != printtype && printtype.equals("all")){//如果为批量打印，物流单号数量等于商品件数才标记为已发货
			int ordertLogisticsNum = 0;//订单里面已经有的物流单号数量
			if( "2".equals(orderentity.getState()) ){
				if( StringUtil.isNotEmpty(orderentity.getLogisticsnumber())){
					String[] explist = orderentity.getLogisticsnumber().split(",");
					if( null != explist ){
						for( String tmpexp : explist ){
							if( StringUtil.isNotEmpty(tmpexp)){
								ordertLogisticsNum++;
							}
						}
					}
				}
				/*if( ordertLogisticsNum == orderentity.getNumber() ){
					orderentity.setState("3");//设置为发货状态
				}*/
			}
		}else{
			orderentity.setState("3");//设置为发货状态
		}
		systemService.save(orderentity);
		
		//更新系统物流记录
		orderLogisticsService.saveOrderLogistics(orderentity, logisticcode, ExpressCompany.getExpressCompanyName(shippercode));
		
		String paybillsid = orderentity.getBillsorderid().getId();
		if( StringUtil.isNotEmpty(paybillsid)){
			PayBillsOrderEntity payBills = systemService.getEntity(PayBillsOrderEntity.class, paybillsid);
			
			String freightstatus = getPayBillsFreightStatus(payBills);
			payBills.setFreightstatus(freightstatus);

			systemService.updateEntitie(payBills);
		}
		
		express.setSetorderlogistic(1);//标记为已经设置订单状态
		
		j.setSuccess(true);
		j.setMsg("ordercode="+ordercode+" logisticcode="+logisticcode + " shippercode=" + shippercode);
		
		return j;
	}
	
	public String getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder) {
		String freightStatus = "1"; //未发货_1,已发货_2,部分发货_3
		Integer totalOrder = 1;
		Integer hasFreightOrderNum = 0;
		if(null != paybillsOrder){
			PayBillsOrderEntity payBills = systemService.findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
			List<OrderEntity> orderList = payBills.getOrderS();
			totalOrder = orderList.size();
			String isSend = null;
			for (OrderEntity orderEntity : orderList) {
				isSend = orderEntity.getState();//  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7
				if("3".equals(isSend) || "4".equals(isSend)){
					hasFreightOrderNum++;
				}
			}
			
		}
		if(totalOrder > 0){
			
			if( totalOrder == hasFreightOrderNum ){
				
				freightStatus = "2";
			}
			else if( hasFreightOrderNum > 0 && totalOrder > hasFreightOrderNum ){
				
				freightStatus = "3";
			}
		}
		
		return freightStatus;
	}

	@Override
	public void updateManualExpress(OrderEntity order) throws Exception {
		if(null != order && StringUtil.isNotEmpty(order.getId())){
			List<String> nLogistics = null;
			List<String> oLogistics = null;
			
			//旧物理单号
			oLogistics = getArrayOrderLogistcs(order);
			
			if(StringUtil.isNotEmpty(order.getLogisticsnumber()) 
					&& StringUtil.isNotEmpty(order.getLogisticsnumber().trim())){
				
				//来自页面的新物流单号（手动添加）
				String[] nArrayLogisticNums = order.getLogisticsnumber().split(",");
				String[] nArrayLogisticCompanys = order.getLogisticscompany().split(",");
				
				//新物流单号
				nLogistics = Arrays.asList(nArrayLogisticNums);
				
				String logisticsCode = null, orderid = null;
				//保存新物流信息
				for (int i = 0; i < nArrayLogisticNums.length; i++) {
					logisticsCode = nArrayLogisticNums[i];
					orderid = order.getId();
					
					if(!hasDuplicateLogistcs(orderid, logisticsCode)){
						
						if(!oLogistics.contains(logisticsCode) ){
							saveManualOrderLogistcs(order.getId(), nArrayLogisticNums[i], nArrayLogisticCompanys[i]);
						}
						
					}else{
						
						throw new Exception("物理单号：" + logisticsCode + "更新失败，物流单号不能在其他订单中同时出现！");
					}
					
					
				}
				
				//
				if(null != oLogistics && !oLogistics.isEmpty()){
					
					for (String ologistcsCode : oLogistics) {
						
						if(!nLogistics.contains( ologistcsCode.trim() )){//页面没有的物理单号要变为失效
							
							inValidExpressLogistcs(order.getId(), ologistcsCode);
							
						}else{//页面有的物流单号全部更新为有效
							
							if(!hasDuplicateLogistcs(orderid, logisticsCode)){
								
							}else{
								
								throw new Exception("物理单号：" + logisticsCode + "更新失败，物流单号不能在其他订单中同时出现！");
							}
							
						}
					}
				}
			}
			else{
				if(null != oLogistics && oLogistics.size() > 0 ){
					throw new Exception("更新失败:不能全部清空物流单号，请修改或者部分删除！");
				}
			}
		}
	}

	@Override
	public List<String> getArrayOrderLogistcs(OrderEntity order) throws Exception {
		List<String> oLogistics = new ArrayList<String>();
		/*StringBuffer sqlbf = new StringBuffer();
		sqlbf.append("SELECT TRIM(logisticCode) logisticCode FROM xnc_express ex WHERE ex.orderid = ? AND  ex.success = '1' AND ex.setorderlogistic = 1");*/
		
		CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
		cq.eq("orderid.id", order.getId());
		cq.eq("setorderlogistic", 1);
		cq.eq("success", 1);
		
		//差找是否存在无效的手工面单号
		List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
		for (ExpressEntity expressEntity : expresslist) {
			if(StringUtil.isNotEmpty(expressEntity.getLogisticcode())){
				oLogistics.add(expressEntity.getLogisticcode().trim());
			}
			
		}
		
//		oLogistics = findListByJdbc(sqlbf.toString(), String.class, new Object[]{order.getId()});
		return oLogistics;
	}

	@Override
	public void saveManualOrderLogistcs(String orderid, String logisticsCode, String logisticsCompany) throws Exception {
		
		String shipperCode = ExpressCompany.getCompanyCode(logisticsCompany);
		if(StringUtil.isNotEmpty(shipperCode)){
			CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
			cq.eq("orderid.id", orderid);
			cq.eq("logisticcode", logisticsCode);
			cq.eq("shippercode", shipperCode);
			cq.eq("setorderlogistic", 0);
			cq.eq("expresstype", 2);
			
			//差找是否存在无效的手工面单号
			List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
			if(null == expresslist || (null != expresslist && expresslist.size() == 0)){//不存在添加
				
				ExpressEntity express = new ExpressEntity();
				express.setId(IdWorker.generateSequenceNo());
				express.setOrderid(findUniqueByProperty(OrderEntity.class, "id", orderid));
				express.setShippercode(shipperCode);
				express.setLogisticcode(logisticsCode);
				express.setSetorderlogistic(1);// 0 无效 1 有效
				express.setSuccess(1);
				express.setExpresstype(2);//手动物流面单
				
				save(express);
				
			}else{//存在更新成有效
				
				ExpressEntity express = expresslist.get(0);
				express.setSuccess(1);
				express.setSetorderlogistic(1);
				
				updateEntitie(express);
			}
		}
			
	}

	@Override
	public void inValidExpressLogistcs(String orderid, String logisticsCode) throws Exception {
		CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
		cq.eq("orderid.id", orderid);
		cq.eq("logisticcode", logisticsCode);
		cq.eq("success", 1);
		cq.eq("setorderlogistic", 1);
		
		List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
		if(null != expresslist && expresslist.size() > 0){
			ExpressEntity express = expresslist.get(0);
			int expresstype = express.getExpresstype();
			
			StringBuffer sqlbf = new StringBuffer();
			if(1 == expresstype){//电子面单,只更新setorderlogistic
				
				sqlbf.append("Update  xnc_express ex SET ex.setorderlogistic = 0 WHERE ex.orderid = '" + orderid + "' AND ex.logisticCode ='" + logisticsCode + "' AND ex.setorderlogistic = 1 and ex.expresstype=1 ");
				
			}else{//手工面单，更新setorderlogistic和success
				
				sqlbf.append("Update  xnc_express ex SET ex.setorderlogistic = 0, ex.success=0 WHERE ex.orderid = '" + orderid + "' AND ex.logisticCode ='" + logisticsCode + "' AND ex.success=1 AND ex.setorderlogistic = 1 and ex.expresstype=2 ");
			}
			
			updateBySqlString(sqlbf.toString());
		}
	}
	
	
	@Override
	public void validExpressLogistcs(String orderid, String logisticsCode) throws Exception {
		/*CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
		cq.eq("orderid.id", orderid);
		cq.eq("logisticcode", logisticsCode);
		cq.eq("success", 1);
		cq.eq("setorderlogistic", 1);
		
		List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
		if(null != expresslist && expresslist.size() > 0){
			ExpressEntity express = expresslist.get(0);
			int expresstype = express.getExpresstype();
			
			StringBuffer sqlbf = new StringBuffer();
			if(1 == expresstype){//电子面单,只更新setorderlogistic
				sqlbf.append("Update  xnc_express ex SET ex.setorderlogistic = 1 WHERE ex.orderid = '" + orderid + "' AND ex.logisticCode ='" + logisticsCode + "' AND ex.setorderlogistic = 0");
			}else{
				
			}
		
		
		updateBySqlString(sqlbf.toString());
		}*/
		
	}

	@Override
	public boolean hasDuplicateLogistcs(String orderid, String logisticsCode) throws Exception {
		if(StringUtil.isNotEmpty(orderid) && StringUtil.isNotEmpty(logisticsCode)){
			CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
			cq.notEq("orderid.id", orderid);
			cq.eq("logisticcode", logisticsCode);
			cq.eq("success", 1);
			cq.eq("setorderlogistic", 1);
			
			List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
			
			if(null != expresslist && expresslist.size() > 0){
				return true;
			}
		}
		return false;
	}
	
}