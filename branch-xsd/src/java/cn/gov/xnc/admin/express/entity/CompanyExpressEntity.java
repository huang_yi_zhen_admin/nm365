package cn.gov.xnc.admin.express.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 发货地址
 * @author zero
 * @date 2017-03-14 18:04:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_express", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CompanyExpressEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**省份，不能漏省字*/
	private java.lang.String province;
	/**城市，不能漏市字*/
	private java.lang.String city;
	/**不能漏区字*/
	private java.lang.String area;
	/**详细地址*/
	private java.lang.String address;
	/**快递商编码*/
	private java.lang.String shippercode;
	/**快递类型*/
	private java.lang.Integer exptype;
	/**快递网点标识_由快递网点提供*/
	private java.lang.String sendsite;
	/**电子面单账号 由快递网点提供 */
	private java.lang.String customername;
	/**电子面单密码 由快递网点提供 */
	private java.lang.String customerpwd;
	/**月结编码_由快递网点提供 */
	private java.lang.String monthcode;
	/**支付类型1-现付，2-到付，3-月结，4-第三方支付*/
	private java.lang.Integer paytype;
	/**是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0*/
	private java.lang.Integer isnotice;
	/**发件人姓名*/
	private java.lang.String name;
	/**发件人电话*/
	private java.lang.String tel;
	/**发件人手机*/
	private java.lang.String mobile;
	/**发货点邮政编码*/
	private java.lang.String postcode;
	/**地区id*/
	private TSTerritory territory;
	/**发货地址名称*/
	private java.lang.String expressname;
	/**快递公司名称*/
	private java.lang.String shippername;
	/**公司信息*/
	private  TSCompany company;
	
	/**user表关联id，发货商用户id*/
	private java.lang.String shipperuserid;
	
	
	/**快递类型名称*/
	private java.lang.String exptypename;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
//	/**
//	 *方法: 取得java.lang.String
//	 *@return: java.lang.String  company表id
//	 */
//	@Column(name ="COMPANYID",nullable=true,length=32)
//	public java.lang.String getCompanyid(){
//		return this.companyid;
//	}
//
//	/**
//	 *方法: 设置java.lang.String
//	 *@param: java.lang.String  company表id
//	 */
//	public void setCompanyid(java.lang.String companyid){
//		this.companyid = companyid;
//	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  省份，不能漏省字
	 */
	@Column(name ="PROVINCE",nullable=true,length=255)
	public java.lang.String getProvince(){
		return this.province;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  省份，不能漏省字
	 */
	public void setProvince(java.lang.String province){
		this.province = province;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  城市，不能漏市字
	 */
	@Column(name ="CITY",nullable=true,length=255)
	public java.lang.String getCity(){
		return this.city;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  城市，不能漏市字
	 */
	public void setCity(java.lang.String city){
		this.city = city;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  不能漏区字
	 */
	@Column(name ="AREA",nullable=true,length=255)
	public java.lang.String getArea(){
		return this.area;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  不能漏区字
	 */
	public void setArea(java.lang.String area){
		this.area = area;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  详细地址
	 */
	@Column(name ="ADDRESS",nullable=true,length=1024)
	public java.lang.String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  详细地址
	 */
	public void setAddress(java.lang.String address){
		this.address = address;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递商编码
	 */
	@Column(name ="SHIPPERCODE",nullable=true,length=255)
	public java.lang.String getShippercode(){
		return this.shippercode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递商编码
	 */
	public void setShippercode(java.lang.String shippercode){
		this.shippercode = shippercode;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  快递类型
	 */
	@Column(name ="EXPTYPE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getExptype(){
		return this.exptype;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  快递类型
	 */
	public void setExptype(java.lang.Integer exptype){
		this.exptype = exptype;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递网点标识_由快递网点提供
	 */
	@Column(name ="SENDSITE",nullable=true,length=255)
	public java.lang.String getSendsite(){
		return this.sendsite;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递网点标识_由快递网点提供
	 */
	public void setSendsite(java.lang.String sendsite){
		this.sendsite = sendsite;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电子面单账号 由快递网点提供 
	 */
	@Column(name ="CUSTOMERNAME",nullable=true,length=255)
	public java.lang.String getCustomername(){
		return this.customername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电子面单账号 由快递网点提供 
	 */
	public void setCustomername(java.lang.String customername){
		this.customername = customername;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电子面单密码 由快递网点提供 
	 */
	@Column(name ="CUSTOMERPWD",nullable=true,length=255)
	public java.lang.String getCustomerpwd(){
		return this.customerpwd;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电子面单密码 由快递网点提供 
	 */
	public void setCustomerpwd(java.lang.String customerpwd){
		this.customerpwd = customerpwd;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  月结编码_由快递网点提供 
	 */
	@Column(name ="MONTHCODE",nullable=true,length=255)
	public java.lang.String getMonthcode(){
		return this.monthcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  月结编码_由快递网点提供 
	 */
	public void setMonthcode(java.lang.String monthcode){
		this.monthcode = monthcode;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  支付类型1-现付，2-到付，3-月结，4-第三方支付
	 */
	@Column(name ="PAYTYPE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getPaytype(){
		return this.paytype;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  支付类型1-现付，2-到付，3-月结，4-第三方支付
	 */
	public void setPaytype(java.lang.Integer paytype){
		this.paytype = paytype;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 */
	@Column(name ="ISNOTICE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getIsnotice(){
		return this.isnotice;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0
	 */
	public void setIsnotice(java.lang.Integer isnotice){
		this.isnotice = isnotice;
	}

	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: TSTerritory  地区id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TERRITORYID")
	public TSTerritory getTerritory(){
		return this.territory;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  地区id
	 */
	public void setTerritory(TSTerritory territory){
		this.territory = territory;
	}

	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件人姓名
	 */
	@Column(name ="NAME",nullable=true,length=255)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件人姓名
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件人电话
	 */
	@Column(name ="TEL",nullable=true,length=32)
	public java.lang.String getTel(){
		return this.tel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件人电话
	 */
	public void setTel(java.lang.String tel){
		this.tel = tel;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发件人手机
	 */
	@Column(name ="MOBILE",nullable=true,length=32)
	public java.lang.String getMobile(){
		return this.mobile;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发件人手机
	 */
	public void setMobile(java.lang.String mobile){
		this.mobile = mobile;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发货点邮政编码
	 */
	@Column(name ="POSTCODE",nullable=true,length=16)
	public java.lang.String getPostcode(){
		return this.postcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发货点邮政编码
	 */
	public void setPostcode(java.lang.String postcode){
		this.postcode = postcode;
	}
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发货地址名称
	 */
	@Column(name ="EXPRESSNAME",nullable=true,length=255)
	public java.lang.String getExpressname(){
		return this.expressname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发货地址名称
	 */
	public void setExpressname(java.lang.String expressname){
		this.expressname = expressname;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发货地址名称
	 */
	@Column(name ="SHIPPERNAME",nullable=true,length=255)
	public java.lang.String getShippername(){
		return this.shippername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发货地址名称
	 */
	public void setShippername(java.lang.String shippername){
		this.shippername = shippername;
	}

	/**
	 * @return the shipperuserid
	 */
	@Column(name ="SHIPPERUSERID",nullable=true,length=255)
	public java.lang.String getShipperuserid() {
		return shipperuserid;
	}

	/**
	 * @param shipperuserid the shipperuserid to set
	 */
	public void setShipperuserid(java.lang.String shipperuserid) {
		this.shipperuserid = shipperuserid;
	}


}
