package cn.gov.xnc.system.core.common.model.json;

import java.util.List;

import cn.gov.xnc.system.core.util.ContextHolderUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;


/**
 * easyui的datagrid向后台传递参数使用的model
 * 
 * @author
 * 
 */
public class DataGrid {

	private int pageNum = 1;// 当前页
	private int showNum = 10;// 每页显示记录数
	private String sort = null;// 排序字段名
	private SortDirection order = SortDirection.asc;// 按什么排序(asc,desc)
	private String field;//字段
	private String treefield;//树形数据表文本字段
	private List results;// 结果集
	private int total;//总记录数
	private String footer;//合计列
	
	private Integer totalPage;//页面总量
	
	private String success = "1";// 是否成功
	private String msg="";//信息提示

	public String getField() {
		return field;
	}

	public List getResults() {
		return results;
	}

	public void setResults(List results) {
		this.results = results;
	}

	public void setField(String field) {
		this.field = field;
	}
	
	

	/**
	 * @return 当前页
	 */
	public int getPageNum() {
		return pageNum;
	}

	/**
	 * @param 当前页
	 */
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	/**
	 * @return 每页显示记录数
	 */
	public int getShowNum() {
		return showNum;
	}

	/**
	 * @param 每页显示记录数
	 */
	public void setShowNum(int showNum) {
		this.showNum = showNum;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public SortDirection getOrder() {
		return order;
	}

	public void setOrder(SortDirection order) {
		this.order = order;
	}
	public String getTreefield() {
		return treefield;
	}

	public void setTreefield(String treefield) {
		this.treefield = treefield;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}
	/**
	 * @param 总记录数
	 */

	public int getTotal() {
		return total;
	}
	/**
	 * @param 总记录数
	 */
	public void setTotal(int total) {
		this.total = total;
		 if (totalPage == null) {
	         totalPage = total % showNum == 0 ? total / showNum : total / showNum + 1;
	     }
	}
	
	/**
	 * @param 页面总量
	 */
	public Integer getTotalPage() {
		return totalPage;
	}

	/**
	 * @param 页面总量
	 */
	public DataGrid setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
		return this;
	}

	/**
	 * @return the success
	 */
	public String getSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(String success) {
		this.success = success;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
	

    
	
}
