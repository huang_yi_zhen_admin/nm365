/**
 * 
 */
package cn.gov.xnc.system.core.express;

import com.alibaba.fastjson.JSONObject;

/**
 * @author Administrator
 *
 */
public class AddService {

	/**
	 * 增值服务名称
	 */
	private String Name;//增值服务名称
	
	/**
	 * 增值服务值
	 */
	private String Value;//增值服务值
	
	
	/**
	 * 客户标识(选填)
	 */
	private String CustomerID;//客户标识(选填)
	
	public String toJsonString(){
		JSONObject obj = new JSONObject();
		if( null != Name ){
			obj.put("Name", Name);
		}
		if( null != Value ){
			obj.put("Value", Value);
		}
		if( null != CustomerID ){
			obj.put("CustomerID", CustomerID);
		}
		
		return obj.toJSONString();
	}

	/**
	 * 增值服务名称
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * 增值服务名称
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * 增值服务值
	 * @return the value
	 */
	public String getValue() {
		return Value;
	}

	/**
	 * 增值服务值
	 * @param value the value to set
	 */
	public void setValue(String value) {
		Value = value;
	}

	/**
	 * 客户标识(选填)
	 * @return the customerID
	 */
	public String getCustomerID() {
		return CustomerID;
	}

	/**
	 * 客户标识(选填)
	 * @param customerID the customerID to set
	 */
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	
	
}
