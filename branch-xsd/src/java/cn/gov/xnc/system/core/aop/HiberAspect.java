package cn.gov.xnc.system.core.aop;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.stereotype.Component;

import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.oConvertUtils;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**
 * Hiberate拦截器（复合拦截器）：实现创建人，创建时间，创建人名称自动注入;
 *                修改人,修改时间,修改人名自动注入;
 * @author  zero
 */
@Component
public class HiberAspect extends EmptyInterceptor {
	private static final Logger logger = Logger.getLogger(HiberAspect.class);
	private static final long serialVersionUID = 1L;


	public Date getDate(){
		 SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Date d = null;
		try {
			d = sf.parse(sf.format(Calendar.getInstance().getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		 return d;
	}
/**
 * 新增的时候拦截
 */
	 /*  
     * entity - POJO对象  
     * id - POJO对象的主键  
     * state - POJO对象的每一个属性所组成的集合(除了ID)  
     * propertyNames - POJO对象的每一个属性名字组成的集合(除了ID)  
     * types - POJO对象的每一个属性类型所对应的Hibernate类型组成的集合(除了ID)  
     */
public boolean onSave(Object entity, Serializable id, Object[] state,String[] propertyNames, Type[] types) {
	
	TSUser currentUser = null;
	try {
		currentUser = ResourceUtil.getSessionUserName();
	} catch (RuntimeException e) {
		logger.warn("当前session为空,无法获取用户");
	}
	
	try {
		//添加数据
		 for (int index=0;index<propertyNames.length;index++)
		 {
			 /*找到名为"创建时间"的属性*/
		     if ("createDate".equalsIgnoreCase(propertyNames[index])
		    		 ||"createDatetime".equalsIgnoreCase(propertyNames[index])  
		    		 ||"createdate".equalsIgnoreCase(propertyNames[index]) )
		     {
		    	// System.out.println(state[index]);
		         /*使用拦截器将对象的"创建时间"属性赋上值*/
		    	 if(oConvertUtils.isEmpty(state[index])){
						state[index] = getDate();

		    	 }
		         continue;
		     }
		   //  找到名为"创建时间"的属性
		     if ("updateDate".equalsIgnoreCase(propertyNames[index])  || "updatedate".equalsIgnoreCase(propertyNames[index]))
		     {
		        // 使用拦截器将对象的"创建时间"属性赋上值
		    	 if(oConvertUtils.isEmpty(state[index])){
		    		 state[index] = getDate();
		    	 }
		         continue;
		     }
		     //  找到名为"创建时间"的属性
		     
		     
		      if ( currentUser!=null  &&    Globals.createuser.equalsIgnoreCase(propertyNames[index]))
		     {
		        // 使用拦截器将对象的"创建人名称"属性赋上值  createUser
		    	 if(oConvertUtils.isEmpty(state[index])){
		    		 state[index] = currentUser.getUserName();
		    	 }
		         continue;
		     }
		      
		      if ( currentUser!=null  &&  "updateUser".equalsIgnoreCase(propertyNames[index]))
			     {
			        // 使用拦截器将对象的"创建人名称"属性赋上值
			    	 if(oConvertUtils.isEmpty(state[index])){
			    		 state[index] = currentUser.getUserName();
			    	 }
			         continue;
			     }
		      //添加公司属性id
		      if ( currentUser!=null && "company".equalsIgnoreCase(propertyNames[index])  )
			     {
			        // 使用拦截器将对象的"公司"属性赋上值
			    	 if(oConvertUtils.isEmpty(state[index])){
			    		 state[index] = currentUser.getCompany();
			    	 }
			         continue;
			     }     
		 }
	} catch (RuntimeException e) {
		e.printStackTrace();
	}
	 return true;
}

/**
 * 更新的时候拦截
 */
public boolean onFlushDirty(Object entity, Serializable id,
		Object[] currentState, Object[] previousState,
		String[] propertyNames, Type[] types) {
	TSUser currentUser = null;
	try {
		currentUser = ResourceUtil.getSessionUserName();
	} catch (RuntimeException e1) {
		logger.warn("当前session为空,无法获取用户");
	}
	if(currentUser==null){
		return true;
	}
	//添加数据
     for (int index=0;index<propertyNames.length;index++)
     {

	    /*  if (Globals.createuser.equalsIgnoreCase(propertyNames[index]))
	     {
	        // 使用拦截器将对象的"创建人名称"属性赋上值
	    	 if(oConvertUtils.isEmpty(currentState[index])){
	    		 currentState[index] = currentUser.getUserName();
	    	 }
	         continue;
	     }*/
	      if ("updateDate".equalsIgnoreCase(propertyNames[index]) || "updatedate".equalsIgnoreCase(propertyNames[index]))
		     {
		        // 更新时间
		    	// if(oConvertUtils.isEmpty(currentState[index])){
		    		 currentState[index] =  getDate();
		    		// SimpleFormatter sf = new SimpleFormatter("");
		    		 
		    	// }
		         continue;
		     }
	      if ("updateuser".equalsIgnoreCase(propertyNames[index]) )
		     {
		        // 更新时间
		    	// if(oConvertUtils.isEmpty(currentState[index])){
		    		 currentState[index] =  currentUser.getUserName();
		    	 //}
		         continue;
		     }
	      
	      
	      
     }
	 return true;
}
}
