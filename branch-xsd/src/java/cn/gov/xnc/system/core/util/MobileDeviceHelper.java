package cn.gov.xnc.system.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class MobileDeviceHelper {

	 /**
     * 判断移动端或者pc端登录
     * @param userAgent
     * @return
     */
    public static boolean isMobileAccess(HttpServletRequest req){    
    	String userAgent = req.getHeader("user-agent");
        
        /**
         * android : 所有android设备
         * mac os : iphone手机
         * windows phone:Nokia等windows系统的手机
         */
        String[] deviceArray = new String[]{"android","iPhone","windows phone"};
        if(null != userAgent){
        	userAgent = userAgent.toLowerCase();
        	for(int i=0;i<deviceArray.length;i++){
                if(userAgent.indexOf(deviceArray[i])>0){
                    return true;
                }
            }
        }
        /*mac os*/
        String phoneReg = "AppleWebKit.*Mobile.*";
        //苹果移动设备  
        Pattern iphonePat = Pattern.compile(phoneReg, Pattern.CASE_INSENSITIVE);  
        // 匹配    
        Matcher matcheriPhone = iphonePat.matcher(userAgent);    
        if(matcheriPhone.find()){    
            return true;    
        } 
        		
        return false;
    }  
}
