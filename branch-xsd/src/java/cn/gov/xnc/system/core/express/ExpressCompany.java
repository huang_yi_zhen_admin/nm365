/**
 * 
 */
package cn.gov.xnc.system.core.express;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;

/**
 * @author Administrator
 *
 */
public final class ExpressCompany {
	
	//顺丰	SF
	public final static String SF="SF";//顺丰	SF

	//百世快递	HTKY
	public final static String HTKY="HTKY";//百世快递	HTKY(未测试，测试接口不可用)
	
	//中通	ZTO
	public final static String ZTO="ZTO";//中通	ZTO
	
	//申通	STO
	public final static String STO="STO";//申通	STO
	
	//圆通	YTO
	public final static String YTO="YTO";//圆通	YTO
	
	//韵达	YD
	public final static String YD="YD";//韵达	YD
	
	//EMS	EMS
	public final static String EMS="EMS";//EMS	EMS
	
	//全峰	QFKD
	public final static String QFKD="QFKD";//全峰	QFKD(未测试)
	
	//优速	UC
	public final static String UC="UC";//优速	UC
	
	//德邦	DBL
	public final static String DBL="DBL";//德邦	DBL(未测试，德邦暂不提供测试环境)

	//快捷	FAST
	public final static String FAST="FAST";//快捷	FAST(未测试，德邦暂不提供测试环境)
	
	//宅急送	ZJS
	public final static String ZJS="ZJS";//宅急送	ZJS
	
	//国通
	public final static String GTO="GTO";//国通
	//天天
	public final static String HHTT="HHTT";//天天
	//邮政
	public final static String YZPY="YZPY";//邮政
	
	//九曳供应链
	public final static String JIUYE="JIUYE";
	
	public static String getCompanyCode(String company){
		if( "邮政".equals(company.substring(0,2)) ){
			return YZPY;
		}else if( "天天".equals(company.substring(0,2)) ){
			return HHTT;
		}else if( "国通".equals(company.substring(0,2)) ){
			return GTO;
		}else if( "宅急".equals(company.substring(0,2)) ){
			return ZJS;
		}else if( "快捷".equals(company.substring(0,2)) ){
			return FAST;
		}else if( "德邦".equals(company.substring(0,2)) ){
			return DBL;
		}else if( "优速".equals(company.substring(0,2)) ){
			return UC;
		}else if( "全峰".equals(company.substring(0,2)) ){
			return QFKD;
		}else if( "EMS".indexOf(company.substring(0,2)) >=0 || "E邮宝".indexOf(company.substring(0,2)) >=0 ){
			return EMS;
		}else if( "韵达".equals(company.substring(0,2)) ){
			return YD;
		}else if( "圆通".equals(company.substring(0,2)) ){
			return YTO;
		}else if( "申通".equals(company.substring(0,2)) ){
			return STO;
		}else if( "中通".equals(company.substring(0,2)) ){
			return ZTO;
		}else if( "百世".equals(company.substring(0,2)) ){
			return HTKY;
		}else if( "顺丰".equals(company.substring(0,2)) ){
			return SF;
		}else if( "九曳".equals(company.substring(0,2)) ){
			return JIUYE;
		}else{
			return "";
		}
	}
	
	
	public static String getExpressCompanyName(String shippercode){
		if( SF.equals(shippercode) ){
			return "顺丰快递";
		}else if( HTKY.equals(shippercode)){
			return "百世快递";
		}else if( ZTO.equals(shippercode)){
			return "中通快递";
		}else if( STO.equals(shippercode)){
			return "申通快递";
		}else if( YTO.equals(shippercode)){
			return "圆通快递";
		}else if( YD.equals(shippercode)){
			return "韵达快递";
		}else if( EMS.equals(shippercode)){
			return "EMS";
		}else if( QFKD.equals(shippercode)){
			return "全峰快递";
		}else if( UC.equals(shippercode)){
			return "优速快递";
		}else if( DBL.equals(shippercode)){
			return "德邦快递";
		}else if( FAST.equals(shippercode)){
			return "快捷快递";
		}else if( ZJS.equals(shippercode)){
			return "宅急送";
		}else{
			return "";
		}
	}

	
	public static DataGrid getSupportedExpressDataGrid(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> sf = new HashMap<String,String>();
		sf.put("shippercode", "SF");
		sf.put("shippername", "顺丰快递");
		results.add(sf);
		
		HashMap<String,String> htky = new HashMap<String,String>();
		htky.put("shippercode", "HTKY");
		htky.put("shippername", "百世快递");
		results.add(htky);
		
		HashMap<String,String> zto = new HashMap<String,String>();
		zto.put("shippercode", "ZTO");
		zto.put("shippername", "中通");
		results.add(zto);
		
		HashMap<String,String> sto = new HashMap<String,String>();
		sto.put("shippercode", "STO");
		sto.put("shippername", "申通");
		results.add(sto);
		
		HashMap<String,String> yto = new HashMap<String,String>();
		yto.put("shippercode", "YTO");
		yto.put("shippername", "圆通");
		results.add(yto);
		
		HashMap<String,String> yd = new HashMap<String,String>();
		yd.put("shippercode", "YD");
		yd.put("shippername", "韵达");
		results.add(yd);
		
		HashMap<String,String> ems = new HashMap<String,String>();
		ems.put("shippercode", "EMS");
		ems.put("shippername", "EMS");
		results.add(ems);
		
		//HashMap<String,String> qfkd = new HashMap<String,String>();
		//qfkd.put("shippercode", "QFKD");
		//qfkd.put("shippername", "全峰快递");
		//results.add(qfkd);
		
		HashMap<String,String> uc = new HashMap<String,String>();
		uc.put("shippercode", "UC");
		uc.put("shippername", "优速快递");
		results.add(uc);
		
		//HashMap<String,String> dbl = new HashMap<String,String>();
		//dbl.put("shippercode", "DBL");
		//dbl.put("shippername", "德邦快递");
		//results.add(dbl);
		
		//HashMap<String,String> fast = new HashMap<String,String>();
		//fast.put("shippercode", "FAST");
		//fast.put("shippername", "快捷快递");
		//results.add(fast);
		
		HashMap<String,String> zjs = new HashMap<String,String>();
		zjs.put("shippercode", "ZJS");
		zjs.put("shippername", "宅急送");
		results.add(zjs);
		
		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(9);
		data.setPageNum(1);
		data.setShowNum(9);
		data.setResults(results);	
		//data.setField("shippercode,shippername");
		
		
		return data;
	}

}
