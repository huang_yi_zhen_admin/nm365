package cn.gov.xnc.system.core.encache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class EncacheUtil {

	private static EncacheUtil instance;
	//private static Map<String, List<TSTerritory>> map = new HashMap<String, List<TSTerritory>>();
	public static CacheManager manager = CacheManager.create();

	
	
	private EncacheUtil() {

	}

	public static synchronized EncacheUtil getInstance() {
		if (instance == null) {
			instance = new EncacheUtil();
		}
		return instance;
	}

	public static List<TSTerritory> get(String key) {
		return (List<TSTerritory>) get("usecache", key);
	}

	public static void put(String key, Object value) {
		put("usecache", key, value);
	}

	public static Object get(String cacheName, Object key) {
		Cache cache = manager.getCache(cacheName);
		
		if (cache != null) {
			Element element = cache.get(key);
			if (element != null) {
				return element.getObjectValue();
			}
		}else {
			cache = manager.getCache("usecache");
			
			Element element = cache.get(key);
			if (element != null) {
				return element.getObjectValue();
			}
			
		}
		return null;
	}

	public static void put(String cacheName, Object key, Object value) {
		Cache cache = manager.getCache(cacheName);
		if (cache != null) {
			cache.put(new Element(key, value));
		}else {
			cache = manager.getCache("usecache");
			cache.put(new Element(key, value));
		}
	}

	public static boolean remove(String cacheName, Object key) {
		Cache cache = manager.getCache(cacheName);
		if (cache != null) {
			return cache.remove(key);
		}
		return false;
	}

	public static boolean removeAll() {
		manager.removalAll();
		return true;
	}

}
