package cn.gov.xnc.system.web.system.controller.core;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;



/**   
 * @Title: Controller
 * @Description: 公司平台信息
 * @author zero
 * @date 2016-09-26 02:59:28
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/companyPlatformController")
public class CompanyPlatformController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CompanyPlatformController.class);


	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 添加公司平台信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(CompanyPlatformEntity companyPlatform, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(companyPlatform.getId())) {
			message = "公司平台信息更新成功";
			CompanyPlatformEntity t = systemService.get(CompanyPlatformEntity.class, companyPlatform.getId());
			try {
				if( StringUtil.isNotEmpty(companyPlatform.getWebsite())){
					companyPlatform.setWebsite("http://"+companyPlatform.getWebsite()+".nongmao365.com");
					CompanyPlatformEntity w = systemService.findUniqueByProperty(CompanyPlatformEntity.class, "website", companyPlatform.getWebsite());
					if( w!=null && !(w.getWebsite()).equals(t.getWebsite())){
						 j.setMsg("域名已经存在!");
		                 j.setSuccess(false);
		                 return j;
					}
				}
				MyBeanUtils.copyBeanNotNull2Bean(companyPlatform, t);
				
				systemService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "公司平台信息更新失败";
			}
		} 
		j.setMsg(message);
		return j;
	}

	/**
	 * 公司平台信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(HttpServletRequest req) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		if (StringUtil.isNotEmpty(user.getCompany().getCompanyplatform().getId())) {

			CompanyPlatformEntity companyPlatform = systemService.getEntity(CompanyPlatformEntity.class, user.getCompany().getCompanyplatform().getId());
			
			req.setAttribute("companyPlatformPage", companyPlatform);
		}
		return new ModelAndView("system/user/companyPlatform");
	}
}
