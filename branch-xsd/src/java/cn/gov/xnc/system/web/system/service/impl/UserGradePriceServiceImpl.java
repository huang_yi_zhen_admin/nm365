package cn.gov.xnc.system.web.system.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserClientsEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserGradePriceEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserTypeEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;

@Service("userGradePriceService")
public class UserGradePriceServiceImpl extends CommonServiceImpl implements UserGradePriceServiceI {

	@Autowired
	private SystemService systemService;
	
	private Logger logger = Logger.getLogger(UserGradePriceServiceImpl.class);
	
	/**
	 * 根据用户类型修改商品对应的用户等级价格
	 */
	public boolean updateGradePriceByUserType(UserTypeEntity userType) {
		
		CriteriaQuery cq = new CriteriaQuery(UserGradePriceEntity.class);
		cq.eq("usertypeid", userType.getId());//订单信息
		cq.add();
		List<UserGradePriceEntity> gradepricelist = systemService.getListByCriteriaQuery(cq,false);
		
			ProductEntity product = null;
			BigDecimal gradeScale = new BigDecimal(1.00);
			for (UserGradePriceEntity userGradePrice : gradepricelist) {
				String productid = userGradePrice.getProductid();
				product = systemService.findUniqueByProperty(ProductEntity.class, "id", productid);
				
				if(null != product){
					try {
						BigDecimal price = product.getPrice();
						gradeScale = userType.getLevelscale().divide(new BigDecimal(100));
						userGradePrice.setGradeprice(price.multiply(gradeScale));
						userGradePrice.setManualprice(price.multiply(gradeScale));
					} catch (Exception e) {
						logger.error(">>>>>>>>>>>>>>>>>>>>updateGradePriceByUserType : " + e);
					}
				}
				
			}
			//更新等级价格
			systemService.batchUpdate(gradepricelist);
		
		return true;
	}

	public boolean delGradePriceByUserType(UserTypeEntity userType) {
		CriteriaQuery cq = new CriteriaQuery(UserGradePriceEntity.class);
		cq.eq("usertypeid", userType.getId());//订单信息
		cq.add();
		List<UserGradePriceEntity> gradepricelist = systemService.getListByCriteriaQuery(cq,false);
		
		if(null != gradepricelist && gradepricelist.size() > 0){
			//删除等级价格
			systemService.deleteAllEntitie(gradepricelist);
		}
		
		return true;
	}

	/**
	 * 获取产品最终价格
	 */
	public BigDecimal getProductFinalPrice(ProductEntity product, TSUser client) {
		BigDecimal finalPrice = product.getPrices();
		
		if(null != client && "3".equals(client.getType())){
			UserClientsEntity uClients = systemService.findUniqueByProperty(UserClientsEntity.class, "id", client.getId());
			if(null != uClients && null != uClients.getUsertpyrid() && StringUtil.isNotEmpty(uClients.getUsertpyrid().getId())){
				
				CriteriaQuery cq = new CriteriaQuery(UserGradePriceEntity.class);
				cq.eq("usertypeid", uClients.getUsertpyrid().getId());
				cq.eq("productid", product.getId());//订单信息
				cq.add();
				List<UserGradePriceEntity> gradepricelist = systemService.getListByCriteriaQuery(cq,false);
				
				if(null != gradepricelist && gradepricelist.size() > 0){
					product.setPrice(gradepricelist.get(0).getManualprice());
					finalPrice = gradepricelist.get(0).getManualprice();
				}
			}
		}else{
			product.setPrice(product.getPrices());
		}
		return finalPrice;
	}
	
	/**
	 * 添加用户等级价格
	 */
	public void addUserGradePrice(final ProductEntity product, final UserTypeEntity userType){
		
		if(null != product && null != userType){
			
			if(StringUtil.isNotEmpty(product.getId())
					&& StringUtil.isNotEmpty(userType.getId())){
				
				//产品
				CriteriaQuery cq = new CriteriaQuery(UserGradePriceEntity.class);
				cq.eq("productid", product.getId());
				cq.eq("usertypeid", userType.getId());
				cq.add();
				List<UserGradePriceEntity> gradePricelist = systemService.getListByCriteriaQuery(cq, false);
				
				if(null != gradePricelist && gradePricelist.size() > 0){
					return;
				}
				
				UserGradePriceEntity gradeEntity = null;
				try {
					BigDecimal price = product.getPrice();//订购价格
					BigDecimal scale= userType.getLevelscale();//等级价格比例
					BigDecimal scaleprice = price.multiply(scale);
					scaleprice = scaleprice.divide(new BigDecimal(100));
					
					gradeEntity = new UserGradePriceEntity();
					gradeEntity.setId(IdWorker.generateSequenceNo());
					gradeEntity.setGradeprice(scaleprice);
					gradeEntity.setManualprice(scaleprice);
					gradeEntity.setProductid(product.getId());
					gradeEntity.setUsertypeid(userType.getId());
					gradeEntity.setCreatedate(DateUtils.getDate());
					gradeEntity.setUpdatedate(DateUtils.getDate());
					
					systemService.save(gradeEntity);
				} catch (Exception e) {
					logger.error(">>>>>>>>>>>>>>>>>>>>>>addUserGradePrice : " + e);
				}
				
				
				gradeEntity = null;//主动释放对象，避免产品过多建立太对对象，导致垃圾回收负担过重
			}
			
		}
	}
		

	/**
	 * 根据用户等级，初始化用户等级价格
	 */
	public void initUserGradePrice(UserTypeEntity userType) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		if(null != user){
			TSCompany tsCompany = user.getCompany();
			
			//产品
			CriteriaQuery cq3 = new CriteriaQuery(ProductEntity.class);
			cq3.eq("company", tsCompany);
			cq3.add();
			List<ProductEntity> productlist = systemService.getListByCriteriaQuery(cq3, false);
			for (ProductEntity productEntity : productlist) {
				addUserGradePrice(productEntity, userType);
			}
		}
		
	}
	
	
	
}