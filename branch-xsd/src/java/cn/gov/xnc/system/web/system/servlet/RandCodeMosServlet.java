package cn.gov.xnc.system.web.system.servlet;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.message.service.impl.MessageTemplateServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;

public class RandCodeMosServlet extends HttpServlet {
    private static final long serialVersionUID = -1257947018545327308L;
    private static final String SESSION_KEY_OF_RAND_CODE_MOS = "randCodeMos"; // todo 要统一常量
    private static final String SESSION_KEY_OF_RAND_CODE_MOS_PHONENUM= "randCodeMosPhonenum";
    
	@Override
	public void doGet(final HttpServletRequest request,final HttpServletResponse response) throws ServletException,
			IOException {
		// 设置页面不缓存
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		
		
		// 取随机产生的认证码(4位数字)
		final String resultCode = exctractRandCode();
		 String phonenum = request.getParameter("phonenum");
		 String msg ="尊敬的用户你好，你的注册验证码为："+resultCode+"，请勿泄露动态验证码！";
		 System.out.println(msg);
		//发送短信验证
		 MessageTemplateServiceI messageTemplateServiceI = new MessageTemplateServiceImpl();
		 messageTemplateServiceI.sendSMSTTMM("", "", phonenum, "天天农贸", msg);
		// 将认证码存入SESSION
		request.getSession().setAttribute(SESSION_KEY_OF_RAND_CODE_MOS, resultCode);
		
		request.getSession().setAttribute(SESSION_KEY_OF_RAND_CODE_MOS_PHONENUM, phonenum);
		
	}
	
	
	public void randCodePassword( String phonenum ,  final HttpServletRequest request,final HttpServletResponse response) {
		// 设置页面不缓存
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		
		
		// 取随机产生的认证码(4位数字)
		final String resultCode = exctractRandCode();
		 //String phonenum = request.getParameter("phonenum");
		 String msg ="尊敬的用户你好，你的短信验证码为："+resultCode+"，请勿泄露动态验证码！";
			System.out.println(msg);
		//发送短信验证
		 MessageTemplateServiceI messageTemplateServiceI = new MessageTemplateServiceImpl();
		 	messageTemplateServiceI.sendSMSTTMM("", "", phonenum, "天天农贸", msg);
		// 将认证码存入SESSION
		request.getSession().setAttribute(SESSION_KEY_OF_RAND_CODE_MOS, resultCode);
		request.getSession().setAttribute(SESSION_KEY_OF_RAND_CODE_MOS_PHONENUM, phonenum);
		
	}
	
	
	@Override
	public void doPost(final HttpServletRequest request,final HttpServletResponse response) throws ServletException,IOException {	doGet(request, response);
		
		
		
		
	}

	/**
	 * @return 随机码
	 */
	private String exctractRandCode() {
		//final String randCodeType = ResourceUtil.getRandCodeType();
        int randCodeLength = Integer.parseInt(ResourceUtil.getRandCodeLength());
        
        return RandCodeImageEnum.NUMBER_CHAR.generateStr(randCodeLength);
	}

}


