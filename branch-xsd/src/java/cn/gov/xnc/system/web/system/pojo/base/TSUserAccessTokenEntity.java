package cn.gov.xnc.system.web.system.pojo.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 用户访问令牌
 * @author zero
 * @date 2017-06-13 19:54:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_access_token", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class TSUserAccessTokenEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**访问令牌*/
	private java.lang.String token;
	/**令牌使用者*/
	private TSUser userid;
	/**令牌颁发者*/
	private java.lang.String audid;
	/**创建时间*/
	private java.util.Date createtime;
	/**过期时间*/
	private java.util.Date expiretime;
	/**上次访问记录id*/
	private TSUserAccesslogEntity lastaccessid;
	/**令牌状态*/
	private java.lang.String status;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  访问令牌
	 */
	@Column(name ="TOKEN",nullable=false,length=250)
	public java.lang.String getToken(){
		return this.token;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  访问令牌
	 */
	public void setToken(java.lang.String token){
		this.token = token;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  令牌使用者
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERID")
	public TSUser getUserid(){
		return this.userid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  令牌使用者
	 */
	public void setUserid(TSUser userid){
		this.userid = userid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  令牌颁发者
	 */
	@Column(name ="AUDID",nullable=false,length=32)
	public java.lang.String getAudid(){
		return this.audid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  令牌颁发者
	 */
	public void setAudid(java.lang.String audid){
		this.audid = audid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  过期时间
	 */
	@Column(name ="EXPIRETIME",nullable=false)
	public java.util.Date getExpiretime(){
		return this.expiretime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  过期时间
	 */
	public void setExpiretime(java.util.Date expiretime){
		this.expiretime = expiretime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  上次访问记录id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "LASTACCESSID")
	public TSUserAccesslogEntity getLastaccessid(){
		return this.lastaccessid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  上次访问记录id
	 */
	public void setLastaccessid(TSUserAccesslogEntity lastaccessid){
		this.lastaccessid = lastaccessid;
	}

	@Column(name ="STATUS",nullable=false,length=4)
	public java.lang.String getStatus() {
		return status;
	}

	public void setStatus(java.lang.String status) {
		this.status = status;
	}
}
