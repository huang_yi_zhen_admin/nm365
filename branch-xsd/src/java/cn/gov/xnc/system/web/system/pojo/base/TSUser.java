package cn.gov.xnc.system.web.system.pojo.base;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.copartner.entity.UserCopartnerEntity;


/**   
 * @Title: Entity
 * @Description: 系统用户信息表
 * @author huangyz
 * @date 2016-03-03 17:54:13
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_user", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class TSUser implements java.io.Serializable {
	
	/**id*/
	private java.lang.String id;
	/**登陆账号*/
	private java.lang.String userName;
	/**密码*/
	private java.lang.String password;
	/**真实姓名*/
	private java.lang.String realname;
	/**公司信息*/
	private  TSCompany company ;
	/**1 管理员用户 2 员工用户 3 客户用户 4业务员角色6合作伙伴（如发货商）*/
	private java.lang.String type;
	/**账号状态 1 启用 2冻结 3过期*/
	private Short status;
	/**用户密钥*/
	private java.lang.String userkey;
	/**编码*/
	private java.lang.String code;
	/**认证手机*/
	private java.lang.String mobilephone;
	/**创建时间*/
	private java.util.Date createDate;
	/**用户扩展信息*/
	private UserStaffEntity tsuserstaff;
	/**客户扩展信息*/
	private UserClientsEntity tsuserclients;
	
	/**客户个人账户扩展信息*/
	private UserSalesmanEntity tsuserSalesman;
	
	/**商业合作伙伴*/
	private UserCopartnerEntity tsuserCopartner;
	
	

	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}


	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}

	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  登陆账号
	 */
	@Column(name ="USERNAME",nullable=false,length=60)
	public java.lang.String getUserName(){
		return this.userName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  登陆账号
	 */
	public void setUserName(java.lang.String userName){
		this.userName = userName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  密码
	 */
	@Column(name ="PASSWORD",nullable=false,length=200)
	public java.lang.String getPassword(){
		//对密码进行加密
		return this.password;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  密码
	 */
	public void setPassword(java.lang.String password){
		
		
		this.password = password;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  真实姓名
	 */
	@Column(name ="REALNAME",nullable=true,length=200)
	public java.lang.String getRealname(){
		return this.realname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  真实姓名
	 */
	public void setRealname(java.lang.String realname){
		this.realname = realname;
	}
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  1 管理员用户 2 员工用户 3 客户用户 4业务员角色
	 */
	@Column(name ="TYPE",nullable=true,length=2)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  1 管理员用户 2 员工用户 3 客户用户 4业务员角色
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  账号状态 1 启用 2冻结 3过期
	 */
	@Column(name ="STATUS",nullable=false,precision=5,scale=0)
	public Short getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  账号状态 1 启用 2冻结 3过期
	 */
	public void setStatus(Short status){
		this.status = status;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户密钥
	 */
	@Column(name ="USERKEY",nullable=true,length=200)
	public java.lang.String getUserkey(){
		return this.userkey;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户密钥
	 */
	public void setUserkey(java.lang.String userkey){
		this.userkey = userkey;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  编码
	 */
	@Column(name ="CODE",nullable=true,length=100)
	public java.lang.String getCode(){
		return this.code;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  编码
	 */
	public void setCode(java.lang.String code){
		this.code = code;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  认证手机
	 */
	@Column(name ="MOBILEPHONE",nullable=true,length=255)
	public java.lang.String getMobilephone(){
		return this.mobilephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  认证手机
	 */
	public void setMobilephone(java.lang.String mobilephone){
		this.mobilephone = mobilephone;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreateDate(){
		return this.createDate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreateDate(java.util.Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户扩展信息
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TSUSERSTAFF")
	public UserStaffEntity getTsuserstaff(){
		return this.tsuserstaff;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户扩展信息
	 */
	public void setTsuserstaff(UserStaffEntity tsuserstaff){
		this.tsuserstaff = tsuserstaff;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户扩展信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TSUSERCLIENTS")
	public UserClientsEntity getTsuserclients(){
		return this.tsuserclients;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户扩展信息
	 */
	public void setTsuserclients(UserClientsEntity tsuserclients){
		this.tsuserclients = tsuserclients;
	}
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  个人账户信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TSUSERSALESMAN")
	public UserSalesmanEntity getTsuserSalesman() {
		return tsuserSalesman;
	}
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  个人账户信息
	 */
	public void setTsuserSalesman(UserSalesmanEntity tsuserSalesman) {
		this.tsuserSalesman = tsuserSalesman;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作商家信息（如发货商）
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TSUSERCOPARTNER")
	public UserCopartnerEntity getTsuserCopartner() {
		return tsuserCopartner;
	}
	public void setTsuserCopartner(UserCopartnerEntity tsuserCopartner) {
		this.tsuserCopartner = tsuserCopartner;
	}
	
	
	
	
	
}
