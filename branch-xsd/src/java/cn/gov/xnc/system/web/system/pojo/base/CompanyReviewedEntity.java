package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 公司资费信息
 * @author zero
 * @date 2016-09-26 03:00:16
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_company_reviewed", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CompanyReviewedEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**开通时间*/
	private java.util.Date createdate;
	/**有效期*/
	private java.util.Date validdate;
	/** 开通_1,禁用_2,待审核_3,未通过_4*/
	private java.lang.String state;
	/**用户账号数*/
	private java.lang.Integer number;
	/**短信发送数*/
	private java.lang.Integer msgnumber;
	/**短信发送剩余总数*/
	private java.lang.Integer msgnumberTotal;
	
	
	/**审核人*/
	private java.lang.String approveid;
	/**审核时间*/
	private java.util.Date approvedate;
	/**备注*/
	private java.lang.String remarks;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  开通时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  开通时间
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  有效期
	 */
	@Column(name ="VALIDDATE",nullable=true)
	public java.util.Date getValiddate(){
		return this.validdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  有效期
	 */
	public void setValiddate(java.util.Date validdate){
		this.validdate = validdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  开通_1,禁用_2,待审核_3,未通过_4
	 */
	@Column(name ="STATE",nullable=true,length=2)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String   开通_1,禁用_2,待审核_3,未通过_4
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  用户账号数
	 */
	@Column(name ="NUMBER",nullable=true,precision=10,scale=0)
	public java.lang.Integer getNumber(){
		return this.number;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  用户账号数
	 */
	public void setNumber(java.lang.Integer number){
		this.number = number;
	}
	
	
	
	
	/**
	 * @return 短信发送数
	 */
	@Column(name ="MSGNUMBER",nullable=true,precision=10,scale=0)
	public java.lang.Integer getMsgnumber() {
		return msgnumber;
	}

	/**
	 * @param 短信发送数
	 */
	public void setMsgnumber(java.lang.Integer msgnumber) {
		this.msgnumber = msgnumber;
	}

	/**
	 * @return 短信发送剩余总数
	 */
	@Column(name ="MSGNUMBERTOTAL",nullable=true,precision=10,scale=0)
	public java.lang.Integer getMsgnumberTotal() {
		return msgnumberTotal;
	}

	/**
	 * @param 短信发送剩余总数
	 */
	public void setMsgnumberTotal(java.lang.Integer msgnumberTotal) {
		this.msgnumberTotal = msgnumberTotal;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  审核人
	 */
	@Column(name ="APPROVEID",nullable=true,length=32)
	public java.lang.String getApproveid(){
		return this.approveid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  审核人
	 */
	public void setApproveid(java.lang.String approveid){
		this.approveid = approveid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  审核时间
	 */
	@Column(name ="APPROVEDATE",nullable=true)
	public java.util.Date getApprovedate(){
		return this.approvedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  审核时间
	 */
	public void setApprovedate(java.util.Date approvedate){
		this.approvedate = approvedate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARKS",nullable=true,length=3000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
}
