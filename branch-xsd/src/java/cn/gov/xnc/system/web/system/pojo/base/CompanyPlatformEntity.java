package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 公司平台信息
 * @author zero
 * @date 2016-09-26 02:59:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_company_platform", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CompanyPlatformEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**平台名称*/
	private java.lang.String platformname;
	/**网址*/
	private java.lang.String website;
	/**广告语*/
	private java.lang.String advertisement;
	/**网站关键字*/
	private java.lang.String keyword;
	/**网站logo*/
	private java.lang.String logourl;
	/**网站导图*/
	private java.lang.String companypicture;
	/**分享设置 1 市场价 2订购价*/
	private java.lang.String shareprice;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  平台名称
	 */
	@Column(name ="PLATFORMNAME",nullable=true,length=300)
	public java.lang.String getPlatformname(){
		return this.platformname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  平台名称
	 */
	public void setPlatformname(java.lang.String platformname){
		this.platformname = platformname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  网址
	 */
	@Column(name ="WEBSITE",nullable=true,length=300)
	public java.lang.String getWebsite(){
		return this.website;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  网址
	 */
	public void setWebsite(java.lang.String website){
		this.website = website;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  广告语
	 */
	@Column(name ="ADVERTISEMENT",nullable=true,length=4000)
	public java.lang.String getAdvertisement(){
		return this.advertisement;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  广告语
	 */
	public void setAdvertisement(java.lang.String advertisement){
		this.advertisement = advertisement;
	}
	
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  网站关键字
	 */
	@Column(name ="KEYWORD",nullable=true,length=4000)
	public java.lang.String getKeyword() {
		return keyword;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  网站关键字
	 */
	public void setKeyword(java.lang.String keyword) {
		this.keyword = keyword;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  网站logo
	 */
	@Column(name ="LOGOURL",nullable=true,length=600)
	public java.lang.String getLogourl(){
		return this.logourl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  网站logo
	 */
	public void setLogourl(java.lang.String logourl){
		this.logourl = logourl;
	}
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  网站导图
	 */
	@Column(name ="companypicture",nullable=true,length=600)
	public java.lang.String getCompanypicture() {
		return companypicture;
	}
	
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  网站导图
	 */
	public void setCompanypicture(java.lang.String companypicture) {
		this.companypicture = companypicture;
	}

	@Column(name ="SHAREPRICE",nullable=true,length=300)
	public java.lang.String getShareprice() {
		return shareprice;
	}

	public void setShareprice(java.lang.String shareprice) {
		this.shareprice = shareprice;
	}
	
	
	
	
	
}
