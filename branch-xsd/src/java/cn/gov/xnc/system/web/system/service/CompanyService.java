package cn.gov.xnc.system.web.system.service;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

/**
 * 
 * @author  
 *
 */
public interface CompanyService extends CommonService{
	
	/**
	 *根据地址查询公司信息
	 */
	public TSCompany companyByUrl(String url);
	
	public TSCompany getCompanyByDomain(HttpServletRequest request);
	
}
