package cn.gov.xnc.system.web.system.pojo.base;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import cn.gov.xnc.system.core.common.entity.IdEntity;


/**
 * 角色表
 *  @author  zero
 */
@Entity
@Table(name = "t_s_role")
public class TSRole extends IdEntity implements java.io.Serializable {
	
	private String roleName;//角色名称
	private String roleCode;//角色编码
	private String dataScope;//角色的数据权限
	
	private String remark;//角色权限备注
	
	/**公司信息*/
	private  TSCompany company ;
	
	
	@Column(name = "dataScope", nullable = false, length = 10)
	public String getDataScope() {
		return dataScope;
	}

	public void setDataScope(String dataScope) {
		this.dataScope = dataScope;
	}

	@Column(name = "rolename", nullable = false, length = 100)
	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	@Column(name = "rolecode", length = 10)
	public String getRoleCode() {
		return this.roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}

	/**
	 *方法: 设置java.lang.String 角色权限备注
	 *@param: java.lang.String  角色权限备注
	 */
	@Column(name = "REMARK", length = 1000)
	public String getRemark() {
		return remark;
	}

	/**
	 *方法: 设置java.lang.String  角色权限备注
	 *@param: java.lang.String  角色权限备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
}