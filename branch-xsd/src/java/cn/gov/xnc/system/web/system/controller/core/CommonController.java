package cn.gov.xnc.system.web.system.controller.core;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.common.UploadFile;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.model.json.ImportFile;
import cn.gov.xnc.system.core.util.FileUtils;
import cn.gov.xnc.system.core.util.JSONHelper;
import cn.gov.xnc.system.core.util.MyClassLoader;
import cn.gov.xnc.system.core.util.ReflectHelper;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.core.util.oConvertUtils;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.easyui.Autocomplete;
import cn.gov.xnc.system.web.system.service.SystemService;


/**
 * 通用业务处理
 * 
 * @author ljz
 * 
 */
@Scope("prototype")
@Controller
@RequestMapping("/commonController")
public class CommonController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(CommonController.class);
	private SystemService systemService;
	private String message;

	@Autowired
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	/**
	 * 通用列表页面跳转
	 */
	@RequestMapping(params = "listTurn")
	public ModelAndView listTurn(HttpServletRequest request) {
		String turn = request.getParameter("turn");// 跳转的目标页面
		return new ModelAndView(turn);
	}

	


	@RequestMapping(params = "importdata")
	public ModelAndView importdata() {
		return new ModelAndView("system/upload");
	}

	/**
	 * 生成XML文件
	 * 
	 * @return
	 */
	@RequestMapping(params = "createxml")
	public void createxml(HttpServletRequest request, HttpServletResponse response) {
		String field = request.getParameter("field");
		String entityname = request.getParameter("entityname");
		ImportFile importFile = new ImportFile(request, response);
		importFile.setField(field);
		importFile.setEntityName(entityname);
		importFile.setFileName(entityname + ".bak");
		importFile.setEntityClass(MyClassLoader.getClassByScn(entityname));
		systemService.createXml(importFile);
	}

	/**
	 * 生成XML文件parserXml
	 * 
	 * @return
	 */
	@RequestMapping(params = "parserXml")
	@ResponseBody
	public AjaxJson parserXml(HttpServletRequest request, HttpServletResponse response) {
		AjaxJson json = new AjaxJson();
		String fileName = null;
		UploadFile uploadFile = new UploadFile(request);
		String ctxPath = request.getSession().getServletContext().getRealPath("");
		File file = new File(ctxPath);
		if (!file.exists()) {
			file.mkdir();// 创建文件根目录
		}
		MultipartHttpServletRequest multipartRequest = uploadFile.getMultipartRequest();
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile mf = entity.getValue();// 获取上传文件对象
			fileName = mf.getOriginalFilename();// 获取文件名
			String savePath = file.getPath() + "/" + fileName;
			File savefile = new File(savePath);
			try {
				FileCopyUtils.copy(mf.getBytes(), savefile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		systemService.parserXml(ctxPath + "/" + fileName);
		json.setSuccess(true);
		return json;
	}

//	/**
//	 * 自动完成请求返回数据
//	 * 
//	 * @param request
//	 * @param responss
//	 */
//	@RequestMapping(params = "getAutoList")
//	public void getAutoList(HttpServletRequest request, HttpServletResponse response, Autocomplete autocomplete) {
//		String jsonp = request.getParameter("jsonpcallback");
//		String trem = StringUtil.getEncodePra(request.getParameter("trem"));// 重新解析参数
//		autocomplete.setTrem(trem);
//		List autoList = systemService.getAutoList(autocomplete);
//		String labelFields = autocomplete.getLabelField();
//		String[] fieldArr = labelFields.split(",");
//		String valueField = autocomplete.getValueField();
//		String[] allFieldArr = null;
//		if (StringUtil.isNotEmpty(valueField)) {
//			allFieldArr = new String[fieldArr.length+1];
//			for (int i=0; i<fieldArr.length; i++) {
//				allFieldArr[i] = fieldArr[i];
//			}
//			allFieldArr[fieldArr.length] = valueField;
//		}
//		
//		try {
//			String str = TagUtil.getAutoList(autocomplete, autoList);
//			str = "(" + str + ")";
//			response.setContentType("application/json;charset=UTF-8");
//			response.setHeader("Pragma", "No-cache");
//            response.setHeader("Cache-Control", "no-cache");
//            response.setDateHeader("Expires", 0);
//            response.getWriter().write(JSONHelper.listtojson(allFieldArr,allFieldArr.length,autoList));
//            response.getWriter().flush();
//            response.getWriter().close();
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//
//	}

	}
