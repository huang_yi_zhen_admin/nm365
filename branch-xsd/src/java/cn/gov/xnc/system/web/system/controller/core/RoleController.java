package cn.gov.xnc.system.web.system.controller.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.ComboTree;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
//import cn.gov.xnc.system.core.common.model.json.TreeGrid;
import cn.gov.xnc.system.core.common.model.json.ValidForm;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ExceptionUtil;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.NumberComparator;
import cn.gov.xnc.system.core.util.ReflectHelper;
import cn.gov.xnc.system.core.util.ResourceUtil;

import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.core.util.oConvertUtils;

import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.easyui.ComboTreeModel;

import cn.gov.xnc.system.web.system.pojo.base.FunctionCompanyEntity;

import cn.gov.xnc.system.web.system.pojo.base.TSFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSOperation;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleFunction;
import cn.gov.xnc.system.web.system.pojo.base.TSRoleUser;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserService;


/**
 * 角色处理类
 * 
 * @author hyzh
 * 
 */
@Scope("prototype")
@Controller
@RequestMapping("/roleController")
public class RoleController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RoleController.class);
	private UserService userService;
	private SystemService systemService;
	private String message = null;

	@Autowired
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public UserService getUserService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * 角色列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "role")
	public ModelAndView role( TSRole role,HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid ) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(TSRole.class, dataGrid);
//		cq.notEq("roleCode", "caigouyuan");
		cq.eq("company", user.getCompany());
		
//		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, role);
//		cq.add();
//		this.systemService.getDataGridReturn(cq, false);
		
		List<TSRole> roles = systemService.getListByCriteriaQuery(cq, false);
		
		request.setAttribute("roles", roles);
		return new ModelAndView("system/role/roleList");
	}

	/**
	 * easyuiAJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "roleGrid")
	public void roleGrid( TSRole role,HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSRole.class, dataGrid);
		cq.notEq("roleCode", "caigouyuan");
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, role);
		cq.add();
		this.systemService.getDataGridReturn(cq, true);
		
		//构建对应的权限特殊级别
		
		
		TagUtil.datagrid(response, dataGrid);;
	}

	/**
	 * 删除角色
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "delRole")
	@ResponseBody
	public AjaxJson delRole(TSRole role,HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		int count = userService.getUsersOfThisRole(role.getId());
		if(count == 0){
			//删除角色之前先删除角色权限关系
			delRoleFunction(role);
			role = systemService.getEntity(TSRole.class, role.getId());
			userService.delete(role);
			message = "角色: " + role.getRoleName() + "被删除成功";
			systemService.addLog(message, Globals.Log_Type_DEL,
					Globals.Log_Leavel_INFO);
		}else{
			message = "角色: 仍被用户使用，请先删除关联关系";
		}
		j.setMsg(message);
		return j;
	}
	/**
	 * 检查角色
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "checkRole")
	@ResponseBody
	public ValidForm checkRole(TSRole role,HttpServletRequest request,HttpServletResponse response) {
		ValidForm v = new ValidForm();
		String roleCode=oConvertUtils.getString(request.getParameter("param"));
		String code=oConvertUtils.getString(request.getParameter("code"));
		List<TSRole> roles=systemService.findByProperty(TSRole.class,"roleCode",roleCode);
		if(roles.size()>0&&!code.equals(roleCode))
		{
			v.setInfo("角色编码已存在");
			v.setStatus("n");
		}
		return v;
	}
	/**
	 * 删除角色权限
	 * @param role
	 */
	protected void delRoleFunction(TSRole role){
		List<TSRoleFunction> roleFunctions=systemService.findByProperty(TSRoleFunction.class,"TSRole.id",role.getId());	
		if (roleFunctions.size()>0) {
			for (TSRoleFunction tsRoleFunction : roleFunctions) {
			systemService.delete(tsRoleFunction);	
			}
		}
		List<TSRoleUser> roleUsers=systemService.findByProperty(TSRoleUser.class,"TSRole.id",role.getId());
	    for (TSRoleUser tsRoleUser : roleUsers) {
	    	systemService.delete(tsRoleUser);
		}	
	}
	/**
	 * 角色录入
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "saveRole")
	@ResponseBody
	public AjaxJson saveRole(TSRole role, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		if (StringUtil.isNotEmpty(role.getId())) {
			
			try {
				TSRole t = systemService.get(TSRole.class, role.getId());
				MyBeanUtils.copyBeanNotNull2Bean(role, t);
				userService.saveOrUpdate(t);
				message = "角色: " + role.getRoleName() + "被更新成功";
				
			} catch (Exception e) {
				message = "角色: " + role.getRoleName() + "更新失败";
				j.setSuccess(false);
			}
			
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);
			
		} else {
			message = "角色: " + role.getRoleName() + "被添加成功";
			
			CriteriaQuery cr = new CriteriaQuery(TSRole.class);
			cr.eq("roleName", role.getRoleName());
			cr.eq("company", user.getCompany());
			List<TSRole> rolelist = systemService.getListByCriteriaQuery(cr, false);
			if( rolelist != null && rolelist.size() > 0 ){
				message = "角色: " + role.getRoleName() + "已经存在";
				j.setSuccess(false);
			}else{
				role.setId(IdWorker.generateSequenceNo());
				userService.save(role);
				systemService.addLog(message, Globals.Log_Type_INSERT,
						Globals.Log_Leavel_INFO);
			}
		}
		
		
		
		
		Map<String, Object> attributes = MyBeanUtils.beanToMap(role);
		j.setAttributes(attributes);
		j.setMsg(message);
		return j;
	}

	/**
	 * 角色列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "fun")
	public ModelAndView fun(HttpServletRequest request) { //获取当前角色信息
		String roleId = request.getParameter("roleId");
		request.setAttribute("roleId", roleId);
		if(StringUtils.isNotEmpty(roleId)){
			TSRole role = this.systemService.get(TSRole.class, roleId);
			request.setAttribute("role", role);
		}
		
		return new ModelAndView("system/role/roleSet");
	}

	/**
	 * 设置权限 只显示公司具备的权限列表
	 * 
	 * @param role
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "setAuthority")
	public void setAuthority(TSRole role , HttpServletRequest request ,HttpServletResponse response ) {
		//设置权限，最大权限只能显示自己的最大权限
		
		CriteriaQuery cq = new CriteriaQuery(TSFunction.class);
//		if (comboTree.getId() != null) {
//			cq.eq("TSFunction.id",comboTree.getId());
//		}
//		
//		if (comboTree.getId() == null) {
//			cq.isNull("TSFunction");
//		}
		
		cq.eq("functionLevel",Short.parseShort("0"));
		cq.eq("syscode", "ORDER_SYS");
		cq.add();
		
		//全部权限合集
		List<TSFunction> setfunctionList = systemService.getListByCriteriaQuery(cq,false);
		//公司所具备的全部合集，需要对权限合集进行匹配。已到达最新的权限合集
		List<TSFunction> functionList = new ArrayList<TSFunction>();

		//遍历公司权限合集
			TSUser user = ResourceUtil.getSessionUserName();
		List<FunctionCompanyEntity>	 functionCompanyList = systemService.findByProperty(FunctionCompanyEntity.class,"company" , user.getCompany());
		//公司权限
		
		
		
		
		Map<String, TSFunction> mapFunctionCompany = new HashMap<String, TSFunction>();//具备权限列表
		if( functionCompanyList != null && functionCompanyList.size() > 0 ){
			for( FunctionCompanyEntity functionCompany : functionCompanyList  ){
				mapFunctionCompany.put(functionCompany.getFunctionid().getId(), functionCompany.getFunctionid());
			}
		}
		//如果存在权限合计才计入公司合集
		if( setfunctionList != null && setfunctionList.size() > 0){
			for( TSFunction tf: setfunctionList){
				
				if(mapFunctionCompany.containsKey(tf.getId())){
					functionList.add(tf);
				}
			}
		}
		Collections.sort(functionList, new NumberComparator());
		//List<ComboTree> comboTrees = new ArrayList<ComboTree>();
		String roleId = request.getParameter("roleId");//角色id 获取当前角色权限集合
		
		//List<TSFunction> loginActionlist = new ArrayList<TSFunction>();// 已有权限菜单
		Map<String, TSRoleFunction> loginActionMap = new HashMap<String, TSRoleFunction>();
			role = this.systemService.get(TSRole.class, roleId);
		if (role != null) {
			List<TSRoleFunction> roleFunctionList=systemService.findByProperty(TSRoleFunction.class, "TSRole.id", role.getId());
			if (roleFunctionList.size() > 0) {
				for (TSRoleFunction roleFunction : roleFunctionList) {
					TSFunction function = (TSFunction) roleFunction.getTSFunction();
					loginActionMap.put(function.getId(), roleFunction);	
					//loginActionlist.add(function);
				}
			}
		}
		
		//构建系统ui权限数据json
		
		//ComboTreeModel comboTreeModel = new ComboTreeModel("id", "functionName", "TSFunctions");
		
		//comboTrees = systemService.ComboTree(functionList,comboTreeModel,loginActionlist , true);
		
		
		//StringBuffer jsonTemp = new StringBuffer();
		//jsonTemp.append("{ \"success\":\"1\",\"msg\":\"获取数据成功\",\"data\":{\"total\":\""+dg.getTotal()+"\",\"page\":\""+dg.getPageNum()+"\",\"rows\":[");
		
		String functionJson = functionJson(functionList,loginActionMap);
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		//JSONObject object = JSONObject.parseObject(functionJson);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(functionJson.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}

	/**
	 * 更新权限菜单按钮权限
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "updateAuthority")
	@ResponseBody
	public AjaxJson updateAuthority(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try{
			
			//String roleDataScope = request.getParameter("lv2");//按钮信息
			
			
			String roleId =request.getParameter("roleId");//菜单信息
			
			String lv0 = request.getParameter("lv0");
			String lv1 = request.getParameter("lv1");
			
			String lv2 = request.getParameter("lv2");
			//如果存在按钮
			
			
			
			String[] roleFunctionlv0s = null;//第一级菜单合集
			if(StringUtils.isNotEmpty(lv0)){
				roleFunctionlv0s = lv0.split(",");
			}
			
			String[] roleFunctionlv1s = null;///第二级菜单合集
			if(StringUtils.isNotEmpty(lv1)){
				roleFunctionlv1s = lv1.split(",");
			}
			String[] operationcodes =null;//按钮合集

			if(StringUtils.isNotEmpty(lv2)){
				operationcodes = lv2.split(",");
			}
			
//			if(StringUtils.isNotEmpty(roleDataScope)){
//				//对按钮权限进行处理
//				TSRole role = this.systemService.get(TSRole.class, roleId);
//				role.setDataScope(roleDataScope);
//				this.systemService.updateEntitie(role);
//				j.setMsg("权限更新成功");	
//				return j;
//			}
			
			TSRole role = this.systemService.get(TSRole.class, roleId);
			List<TSRoleFunction> roleFunctionList=systemService.findByProperty(TSRoleFunction.class, "TSRole.id", role.getId());
			Map<String,TSRoleFunction> map = new HashMap<String,TSRoleFunction>();//角色下原具备的权限
			for (TSRoleFunction functionOfRole : roleFunctionList){
				map.put(functionOfRole.getTSFunction().getId(),functionOfRole);
			}
			
			
			

			
			//权限新提交的权限合集
			Set<String> set = new HashSet<String>();
			if(roleFunctionlv0s != null && roleFunctionlv0s.length > 0){
				for (String s : roleFunctionlv0s) {
					set.add(s);
				}
			}
			if(roleFunctionlv1s != null && roleFunctionlv1s.length > 0){
				for (String s : roleFunctionlv1s) {
					String[] roleFunctionlv1 = s.split(":");//第二级菜单合集
					for (String s1 : roleFunctionlv1)  {
						set.add(s1);
					}
				}
			}
			
			
			if(operationcodes != null && operationcodes.length > 0){
				for (String s : operationcodes) {
					
					String[] roleFunctionlv2 = s.substring(0,s.lastIndexOf(":")).split(":");//按钮级菜单合集
					for (String s1 : roleFunctionlv2)  {
						set.add(s1);
					}
				}
			}
			
			
			
			updateCompare(set,role,map);
			
			
			if( operationcodes != null && operationcodes.length > 0 ){
				Map<String,Map> mapfunction = new HashMap<String,Map>();//菜单下的权限
				for( String operationcode : operationcodes ){
					//把数组拆分 构建二级菜单下的按钮信息
					String functionId = operationcode.substring(operationcode.indexOf(":")+1,operationcode.lastIndexOf(":"));
					String op = operationcode.substring(operationcode.lastIndexOf(":")+1,operationcode.length());
					Map<String,String> mapop = new HashMap<String,String>();
					if(mapfunction.containsKey(functionId)){
						mapop = mapfunction.get(functionId);
						mapop.put(op, op);
						mapfunction.put(functionId, mapop);
					}else{
						mapop.put(op, op);
						mapfunction.put(functionId, mapop);
					}
//					CriteriaQuery cq1=new CriteriaQuery(TSRoleFunction.class);
//					cq1.eq("TSRole.id",roleId);
//					cq1.eq("TSFunction.id",functionId);
//					cq1.add();
//					List<TSRoleFunction> rFunctions =systemService.getListByCriteriaQuery(cq1,false);
//
//					if(null!=rFunctions && rFunctions.size()>0){
//						TSRoleFunction tsRoleFunction =  rFunctions.get(0);
//						tsRoleFunction.setOperation(operationcodes);
//						systemService.saveOrUpdate(tsRoleFunction);
//					}
				}
				
				if( mapfunction != null ){
					Iterator<Map.Entry<String,Map>> entries = mapfunction.entrySet().iterator();  
					while (entries.hasNext()) {
					    Map.Entry<String,Map> entry = entries.next();  
					    Iterator<Map.Entry<String,String>> entriesOP = entry.getValue().entrySet().iterator();  
					    String ops ="";
					    String functionId =entry.getKey();
					    while (entriesOP.hasNext()) {
					    	Map.Entry<String,String> entryop = entriesOP.next();  
					    	ops +=entryop.getKey()+",";
					    }
					    
						CriteriaQuery cq1=new CriteriaQuery(TSRoleFunction.class);
						cq1.eq("TSRole.id",roleId);
						cq1.eq("TSFunction.id",functionId);
						cq1.add();
						List<TSRoleFunction> rFunctions =systemService.getListByCriteriaQuery(cq1,false);
	
						if(null!=rFunctions && rFunctions.size()>0){
							TSRoleFunction tsRoleFunction =  rFunctions.get(0);
							tsRoleFunction.setOperation(ops);
							systemService.saveOrUpdate(tsRoleFunction);
						}
					    
					}  
				}

			}
			
			
			
			
			
			j.setMsg("权限更新成功");
		}catch (Exception e){
            logger.error(ExceptionUtil.getExceptionMessage(e));    
			j.setMsg("权限更新失败");			
		}
		return j;
	}
	/**
	 * 权限比较
	 * @param set 最新的权限列表
	 * @param role 当前角色
	 * @param map 旧的权限列表
	 * @param entitys 最后保存的权限列表
	 */
	private void updateCompare(Set<String> set,TSRole role,
			Map<String, TSRoleFunction> map) {
		List<TSRoleFunction> entitys = new ArrayList<TSRoleFunction>();//角色需要添加的菜单列表
		List<TSRoleFunction> deleteEntitys = new ArrayList<TSRoleFunction>();//角色需要删除的菜单列表
		
		if( set != null && set.size() > 0 ){
			for (String s : set) {
				if(map.containsKey(s)){
					map.remove(s);
				}else{
					TSRoleFunction rf = new TSRoleFunction();
					TSFunction f = this.systemService.get(TSFunction.class,s);
					rf.setTSFunction(f);
					rf.setTSRole(role);
					entitys.add(rf);
				}
			}
		}
		
		
		Collection<TSRoleFunction> collection = map.values();
		Iterator<TSRoleFunction> it = collection.iterator();
		for (; it.hasNext();) {
			deleteEntitys.add(it.next());
	    }
		systemService.batchSave(entitys);
		systemService.deleteAllEntitie(deleteEntitys);
		
	}

	/**
	 * 角色页面跳转
	 * 
	 * @param role
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(TSRole role, HttpServletRequest req) {
		if (role.getId() != null) {
			role = systemService.getEntity(TSRole.class, role.getId());
			req.setAttribute("role", role);
		}else {
			role = new TSRole();
			req.setAttribute("role", role);
		}
		return new ModelAndView("system/role/role");
	}
	
	
	
//	/**
//	 * 权限操作列表
//	 * 
//	 * @param role
//	 * @param request
//	 * @param response
//	 * @param dataGrid
//	 * @param user
//	 * @return
//	 */
//	@RequestMapping(value = "setOperate")
//	@ResponseBody
//	public List<TreeGrid> setOperate(HttpServletRequest request,
//			TreeGrid treegrid) {
//		
//		String roleId = request.getParameter("roleId");
//		CriteriaQuery cq = new CriteriaQuery(TSFunction.class);
//		if (treegrid.getId() != null) {
//			cq.eq("TSFunction.id",
//					treegrid.getId());
//		}
//		if (treegrid.getId() == null) {
//			cq.isNull("TSFunction");
//		}
//		cq.add();
//		List<TSFunction> functionList = systemService.getListByCriteriaQuery(cq,false);
//		List<TreeGrid> treeGrids = new ArrayList<TreeGrid>();
//		Collections.sort(functionList, new SetListSort());
//		TreeGridModel treeGridModel=new TreeGridModel();
//		treeGridModel.setRoleid(roleId);
//		treeGrids = systemService.treegrid(functionList, treeGridModel);
//		return treeGrids;
//
//	}
	
//	/**
//	 * 更新按钮权限
//	 * 
//	 * @param request
//	 * @return
//	 */
//	@RequestMapping(value = "updateOperation")
//	@ResponseBody
//	public AjaxJson updateOperation(HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		String roleId =request.getParameter("roleId");
//		String functionId = request.getParameter("functionId");
//		String operationcodes = null;
//		try {
//			operationcodes = URLDecoder.decode(request.getParameter("operationcodes"), "utf-8");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		
//		j.setMsg("按钮权限更新成功");
//		return j;
//	}
	
	
//
//	/**
//	 * 操作录入
//	 * 
//	 * @param ids
//	 * @return
//	 */
//	@RequestMapping(value = "saveOperate")
//	@ResponseBody
//	public AjaxJson saveOperate(HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		String fop = request.getParameter("fp");
//		String roleId = request.getParameter("roleId");
//		//录入操作前清空上一次的操作数据
//		clearp(roleId);
//		String[] fun_op = fop.split(",");
//		String aa = "";
//		String bb = "";
//		//只有一个被选中
//		if (fun_op.length == 1) {
//			bb = fun_op[0].split("_")[1];
//			aa = fun_op[0].split("_")[0];
//			savep( roleId,bb,aa);
//		}else{
//			//至少2个被选中
//			for (int i = 0; i < fun_op.length; i++) {
//				String cc = fun_op[i].split("_")[0];  //操作id
//				if (i > 0 && bb.equals(fun_op[i].split("_")[1])) {
//					aa += "," + cc;
//					if (i== (fun_op.length - 1)) {
//						savep( roleId,bb,aa);
//					}
//				} else if (i > 0) {
//					    savep(roleId,bb,aa);
//					    aa = fun_op[i].split("_")[0];   //操作ID
//					    if (i==(fun_op.length-1)){
//					    	bb = fun_op[i].split("_")[1]; //权限id
//					    	savep(roleId,bb,aa);
//						}
//					    
//				} else {
//					aa = fun_op[i].split("_")[0]; //操作ID
//				}
//				bb = fun_op[i].split("_")[1]; //权限id
//
//			}	
//		}
//		
//		
//		return j;
//	}
//     /**
//      *更新操作
//      * @param roleId
//      * @param functionid
//      * @param ids
//      */
//	public void savep(String roleId,String functionid, String ids) {
//		//String hql = "from TSRoleFunction t where" + " t.TSRole.id=" + oConvertUtils.getInt(roleId,0)
//		//		+ " " + "and t.TSFunction.id=" + oConvertUtils.getInt(functionid,0);		
//		CriteriaQuery cq=new CriteriaQuery(TSRoleFunction.class);
//		cq.eq("TSRole.id",roleId);
//		cq.eq("TSFunction.id",functionid);
//		cq.add();
//		List<TSRoleFunction> rFunctions =systemService.getListByCriteriaQuery(cq,false);
//		if (rFunctions.size() > 0) {
//			TSRoleFunction roleFunction = rFunctions.get(0);
//			roleFunction.setOperation(ids);
//			systemService.saveOrUpdate(roleFunction);
//		}
//	}
//	/**
//	 * 清空操作
//	 * @param roleId
//	 */
//	public void clearp(String roleId) {	
//		List<TSRoleFunction> rFunctions = systemService.findByProperty(TSRoleFunction.class,"TSRole.id",roleId);
//		if (rFunctions.size() > 0){
//			for (TSRoleFunction tRoleFunction : rFunctions) {
//				tRoleFunction.setOperation(null);
//				systemService.saveOrUpdate(tRoleFunction);
//			}
//	 	}
//	}
//	/**
//	 * 按钮权限展示
//	 * @param request
//	 * @param functionId
//	 * @param roleId
//	 * @return
//	 */
//	@RequestMapping(params = "operationListForFunction")
//	public ModelAndView operationListForFunction(HttpServletRequest request,String functionId,String roleId) {	
//		
//		
//		CriteriaQuery cq = new CriteriaQuery(TSOperation.class);
//			cq.eq("TSFunction.id", functionId);
//			cq.add(); 
//
//		List<TSOperation> operationList = this.systemService.getListByCriteriaQuery(cq, false);
//		//获取采购商角色，不显示
//		
//		
//		Set<String> operationCodes = systemService.getOperationCodesByRoleIdAndFunctionId(roleId, functionId);
//		
//			request.setAttribute("operationList", operationList);
//			request.setAttribute("operationcodes", operationCodes);
//			request.setAttribute("functionId", functionId);
//			
//		return new ModelAndView("system/role/operationListForFunction");
//	}
//	
	
	/**
	 * 构建返回页面权限json
	 * @param functionList
	 * @param loginActionlist
	 */
	private static String functionJson( List<TSFunction> functionList , Map<String, TSRoleFunction> loginActionMap )  {
		
		
		StringBuffer jsonTemp = new StringBuffer();
		//直接输出分页
		jsonTemp.append("{ \"success\":\"1\",\"msg\":\"获取数据成功\",\"data\":[");

		if (functionList != null && functionList.size() > 0) {
			for( int i = 0; i < functionList.size(); ++i){
				TSFunction f = functionList.get(i);
				if(f.getFunctionLevel() == 0){//构建第一级
					//是否选中
					boolean checked_1 = false;
					if(loginActionMap.containsKey(f.getId())){
						checked_1 = true;
					}
					
					jsonTemp.append("{");
					jsonTemp.append("\"id\":\""+f.getId()+"\",\"text\":\""+f.getFunctionName()+"\",\"checked\":\""+checked_1+"\",\"subs\":[");
				if(f.getTSFunctions() != null && f.getTSFunctions().size() > 0 ){
					for( int j = 0; j < f.getTSFunctions().size(); ++j ){
						TSFunction subs2 = f.getTSFunctions().get(j);
						if(subs2.getFunctionLevel() == 1){//构建第二级
							//是否选中
							boolean checked_2 = false;
							if(loginActionMap.containsKey(subs2.getId())){
								checked_2 = true;
							}
							Map<String, String> OperationSMap = new HashMap<String, String>();
							
							if(checked_2 ){
								TSRoleFunction function =loginActionMap.get(subs2.getId());
								if( StringUtil.isNotEmpty(function.getOperation()) ){
									String [] OperationS = function.getOperation().split(",");
									if( OperationS != null && OperationS.length > 0){
										for(int ops = 0 ;ops < OperationS.length ; ops++ ){
											OperationSMap.put(OperationS[ops], OperationS[ops]);
										}
									}
								}
							}
							
							jsonTemp.append("{");
							jsonTemp.append("\"id\":\""+subs2.getId()+"\",\"text\":\""+subs2.getFunctionName()+"\",\"checked\":\""+checked_2+"\",\"subs\":[");
							//构建按钮权限，只有在第二级下才存在按钮
							if( subs2.getTSOperations() != null && subs2.getTSOperations().size() > 0){
								for( int o = 0 ; o< subs2.getTSOperations().size() ; o++ ){
									TSOperation op = subs2.getTSOperations().get(o);
									boolean Opchecked_1 = false;
									if(OperationSMap.containsKey(op.getOperationcode())){
										Opchecked_1 = true;
									}
									jsonTemp.append("{");
									jsonTemp.append("\"id\":\""+op.getOperationcode()+"\",\"text\":\""+op.getOperationname()+"\",\"checked\":\""+Opchecked_1+"\"");
									
									if (o != subs2.getTSOperations().size() - 1)
										jsonTemp.append("},");
									else {
										jsonTemp.append("}");
									}
									
								}	
							}
							
							if (j != f.getTSFunctions().size() - 1)
								jsonTemp.append("]},");
							else {
								jsonTemp.append("]}");
							}
						}
						
					}
				}
					if (i != functionList.size() - 1){
						jsonTemp.append("]},");
					}else {
						jsonTemp.append("]}");
					}
				}
			}
		}
		jsonTemp.append("]");
		jsonTemp.append("}");
		
		return jsonTemp.toString();
		
	}
}
