package cn.gov.xnc.system.web.system.controller.core;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUserAccesslogEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.web.system.service.UserAccessServiceI;

/**   
 * @Title: Controller
 * @Description: 用户访问记录表
 * @author zero
 * @date 2017-06-13 19:51:21
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/userAccesslogController")
public class UserAccessController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserAccessController.class);

	@Autowired
	private UserAccessServiceI userAccessService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 用户访问记录表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView userAccesslog(HttpServletRequest request) {
		return new ModelAndView("system/user/userAccesslogList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSUserAccesslogEntity userAccesslog,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUserAccesslogEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userAccesslog, request.getParameterMap());
		this.userAccessService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除用户访问记录表
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(TSUserAccesslogEntity userAccesslog, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		userAccesslog = systemService.getEntity(TSUserAccesslogEntity.class, userAccesslog.getId());
		message = "用户访问记录表删除成功";
		userAccessService.delete(userAccesslog);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加用户访问记录表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(TSUserAccesslogEntity userAccesslog, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userAccesslog.getId())) {
			message = "用户访问记录表更新成功";
			TSUserAccesslogEntity t = userAccessService.get(TSUserAccesslogEntity.class, userAccesslog.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userAccesslog, t);
				userAccessService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "用户访问记录表更新失败";
			}
		} else {
			message = "用户访问记录表添加成功";
			userAccessService.save(userAccesslog);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 用户访问记录表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(TSUserAccesslogEntity userAccesslog, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(userAccesslog.getId())) {
			userAccesslog = userAccessService.getEntity(TSUserAccesslogEntity.class, userAccesslog.getId());
			req.setAttribute("userAccesslogPage", userAccesslog);
		}
		return new ModelAndView("system/user/userAccesslog");
	}
}
