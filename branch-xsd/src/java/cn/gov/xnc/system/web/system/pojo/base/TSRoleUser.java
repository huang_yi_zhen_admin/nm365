package cn.gov.xnc.system.web.system.pojo.base;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import cn.gov.xnc.system.core.common.entity.IdEntity;

/**
 * TSRoleUser entity.
 * @author  zero
 */
@Entity
@Table(name = "t_s_role_user")
public class TSRoleUser extends IdEntity implements java.io.Serializable {
	
	
	private TSUser TSUser;
	private TSRole TSRole;
	
	/**公司信息*/
	private  TSCompany company ;
	

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userid")
	public TSUser getTSUser() {
		return this.TSUser;
	}

	public void setTSUser(TSUser TSUser) {
		this.TSUser = TSUser;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roleid")
	public TSRole getTSRole() {
		return this.TSRole;
	}

	public void setTSRole(TSRole TSRole) {
		this.TSRole = TSRole;
	}
	
	
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	

}