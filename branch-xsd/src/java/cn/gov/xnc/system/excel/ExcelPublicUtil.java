package cn.gov.xnc.system.excel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.mapping.Collection;

import cn.gov.xnc.system.excel.annotation.Excel;
import cn.gov.xnc.system.excel.annotation.ExcelCollection;
import cn.gov.xnc.system.excel.annotation.ExcelEntity;
import cn.gov.xnc.system.excel.annotation.ExcelIgnore;

import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTMarker;

public class ExcelPublicUtil {

	public static String GET = "get";
	public static String SET = "set";

	/**
	 * 复制一个单元格样式到目的单元格样式
	 * 
	 * @param fromStyle
	 * @param toStyle
	 */
	public static void copyCellStyle(HSSFWorkbook wb, HSSFCellStyle fromStyle,
			HSSFCellStyle toStyle) {
		toStyle.setAlignment(fromStyle.getAlignment());
		// 边框和边框颜色
		toStyle.setBorderBottom(fromStyle.getBorderBottom());
		toStyle.setBorderLeft(fromStyle.getBorderLeft());
		toStyle.setBorderRight(fromStyle.getBorderRight());
		toStyle.setBorderTop(fromStyle.getBorderTop());
		toStyle.setTopBorderColor(fromStyle.getTopBorderColor());
		toStyle.setBottomBorderColor(fromStyle.getBottomBorderColor());
		toStyle.setRightBorderColor(fromStyle.getRightBorderColor());
		toStyle.setLeftBorderColor(fromStyle.getLeftBorderColor());

		// 背景和前景
		toStyle.setFillBackgroundColor(fromStyle.getFillBackgroundColor());
		toStyle.setFillForegroundColor(fromStyle.getFillForegroundColor());

		toStyle.setDataFormat(fromStyle.getDataFormat());
		toStyle.setFillPattern(fromStyle.getFillPattern());
		// toStyle.setFont(fromStyle.getFont(null));
		toStyle.setHidden(fromStyle.getHidden());
		toStyle.setIndention(fromStyle.getIndention());// 首行缩进
		toStyle.setLocked(fromStyle.getLocked());
		toStyle.setRotation(fromStyle.getRotation());// 旋转
		toStyle.setVerticalAlignment(fromStyle.getVerticalAlignment());
		toStyle.setWrapText(fromStyle.getWrapText());

		// --复制字体样式--
		HSSFFont font = fromStyle.getFont(wb);
		// 生成新的字体
		HSSFFont fonts = wb.createFont();
		fonts.setFontHeightInPoints(font.getFontHeightInPoints()); // 设置字体大小
		fonts.setColor(font.getColor()); // 设置字体颜色
		fonts.setFontName(font.getFontName()); // 设置子是什么字体（如宋体）
		fonts.setBoldweight(font.getBoldweight()); // 设置粗体
		toStyle.setFont(fonts);// 将字体样式设置给单元格

	}

	/**
	 * 合并单元格处理--加入list
	 * 
	 * @param sheet
	 * @return
	 */
	public static void getCombineCell(HSSFSheet sheet, List<CellRangeAddress> list) {
		// 获得一个 sheet 中合并单元格的数量
		int sheetmergerCount = sheet.getNumMergedRegions();
		// 遍历合并单元格
		for (int i = 0; i < sheetmergerCount; i++) {
			// 获得合并单元格加入list中
			CellRangeAddress ca = sheet.getMergedRegion(i);
			list.add(ca);
		}
	}

	/**
	 * 判断单元格是否为合并单元格
	 * 
	 * @param listCombineCell
	 *            存放合并单元格的list
	 * @param cell
	 *            需要判断的单元格
	 * @param sheet
	 *            sheet
	 * @return
	 */
	public static CellRangeAddress isCombineCell(List<CellRangeAddress> listCombineCell,
			HSSFCell cell, HSSFSheet sheet) {
		int firstC = 0;
		int lastC = 0;
		int firstR = 0;
		int lastR = 0;
		for (CellRangeAddress ca : listCombineCell) {
			// 获得合并单元格的起始行, 结束行, 起始列, 结束列
			firstC = ca.getFirstColumn();
			lastC = ca.getLastColumn();
			firstR = ca.getFirstRow();
			lastR = ca.getLastRow();
			
			//System.out.println("cell="+cell);
			if (cell != null &&  cell.getColumnIndex() <= lastC && cell.getColumnIndex() >= firstC) {
				if (cell.getRowIndex() <= lastR && cell.getRowIndex() >= firstR) {
					//return true;
					return ca;
				}
			}
		}
		return null;
	}

	/**
	 * Sheet复制
	 * 
	 * @param fromSheet
	 * @param toSheet
	 * @param copyValueFlag
	 */
	public static void copySheet(int fromrow, HSSFWorkbook wb,
			HSSFSheet fromSheet, HSSFSheet toSheet, boolean copyValueFlag) {
		// 合并区域处理
		mergerRegion(fromrow, fromSheet, toSheet);
		for (Iterator rowIt = fromSheet.rowIterator(); rowIt.hasNext();) {
			HSSFRow tmpRow = (HSSFRow) rowIt.next();
			HSSFRow newRow = toSheet.createRow(tmpRow.getRowNum() + fromrow);
			newRow.setHeight(tmpRow.getHeight());

			// 行复制
			copyRow(wb, tmpRow, newRow, copyValueFlag);
		}
	}
/**
 * copy并且新建合并的单元格
 * @param wb
 * @param fromRow
 * @param toRow
 * @param copyValueFlag
 * @param sheet
 * @return
 */
	public static int copyRowWithCombineCell(HSSFWorkbook wb, HSSFRow fromRow, HSSFRow toRow,
			boolean copyValueFlag,HSSFSheet sheet){
		 List<CellRangeAddress> list = new ArrayList();
		 CellRangeAddress newregin = null;
		 int maxRowNum = 1;
		ExcelPublicUtil.getCombineCell((HSSFSheet)sheet,list);
		
		for (Iterator cellIt = fromRow.cellIterator(); cellIt.hasNext();) {
			HSSFCell tmpCell = (HSSFCell) cellIt.next();
			HSSFCell newCell = toRow.createCell(tmpCell.getCellNum());	
			/**
			 * 判断单元格是否有合并  如果有合并 则新建合并的单元格 并且把样式给copy过去
			 */
			if(sheet!=null){
				//System.out.println("合并");
				CellRangeAddress ca = ExcelPublicUtil.isCombineCell(list, tmpCell, (HSSFSheet)sheet);
				if(ca!=null){
					newregin = new CellRangeAddress(ca.getLastRow()+1, ca.getLastRow()+1+(ca.getLastRow()-ca.getFirstRow()),
							ca.getFirstColumn(), ca.getLastColumn());
					/* Row fRow = sheet.getRow(ca.getFirstRow()); 
					 Cell fCell = fRow.getCell(ca.getFirstColumn());
					 System.out.println(fCell.getStringCellValue());*/
					/**
					 * 最大的合并行数
					 */
					maxRowNum = maxRowNum>(ca.getLastRow()-ca.getFirstRow()+1)?maxRowNum:(ca.getLastRow()-ca.getFirstRow()+1);
					sheet.addMergedRegion(newregin);													        
				}
			}			
			
			copyCell(wb,tmpCell, newCell, copyValueFlag);  
		}
		return maxRowNum;
		
	}
	/**
	 * 行复制功能
	 * 返回合并的行数
	 * @param fromRow
	 * @param toRow
	 */
	    public static void copyRow(HSSFWorkbook wb,HSSFRow fromRow,HSSFRow toRow,boolean copyValueFlag){  
	        for (Iterator cellIt = fromRow.cellIterator(); cellIt.hasNext();) {  
	            HSSFCell tmpCell = (HSSFCell) cellIt.next();  
	            HSSFCell newCell = toRow.createCell(tmpCell.getCellNum());  
	           
	            copyCell(wb,tmpCell, newCell, copyValueFlag);  
	        }  
	    }  

	    /**
	     * 获取所有字段的样式
	     */
	    public static Map<Integer,HSSFCell>  getAllColumnStyle(HSSFWorkbook wb,HSSFRow fromRow){
	    	 Map<Integer,HSSFCell> map = new HashMap();
	    	 int i = 0;
	    	 for (Iterator cellIt = fromRow.cellIterator(); cellIt.hasNext();) {	    		    
		            HSSFCell tmpCell = (HSSFCell) cellIt.next();  
		         
		    		map.put(i++, tmpCell);
		     }  
	    	 return map;
	    }
	    
	/**
	 * 复制原有sheet的合并单元格到新创建的sheet
	 * 
	 * @param sheetCreat
	 *            新创建sheet
	 * @param sheet
	 *            原有的sheet
	 */
	public static void mergerRegion(int fromrow, HSSFSheet fromSheet,
			HSSFSheet toSheet) {
		int sheetMergerCount = fromSheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergerCount; i++) {
			// Region mergedRegionAt = fromSheet.getMergedRegionAt(i);
			// CellRangeAddress region1 = fromSheet.getMergedRegion(i);
			CellRangeAddress oldregin = fromSheet.getMergedRegion(i);
			// CellRangeAddress region1 = new CellRangeAddress(rowNumber,
			// rowNumber, (short) 0, (short) 11);
			// toSheet.addMergedRegion(mergedRegionAt);
			int firstcolumn = oldregin.getFirstColumn();
			int lastcolumn = oldregin.getLastColumn();
			int firrow = oldregin.getFirstRow() + fromrow;
			int lastrow = oldregin.getLastRow() + fromrow;
			CellRangeAddress newregin = new CellRangeAddress(firrow, lastrow,
					firstcolumn, lastcolumn);
			toSheet.addMergedRegion(newregin);
		}
	}

	/**
	 * 复制单元格
	 * 
	 * @param srcCell
	 * @param distCell
	 * @param copyValueFlag
	 *            true则连同cell的内容一起复制
	 */
	public static void copyCell(HSSFWorkbook wb, HSSFCell srcCell,
			HSSFCell distCell, boolean copyValueFlag) {
		HSSFCellStyle newstyle = wb.createCellStyle();
		copyCellStyle(wb, srcCell.getCellStyle(), newstyle);
		// distCell.setEncoding(srcCell.getEncoding());

		// 样式
		distCell.setCellStyle(newstyle);
		
		
		
		// 评论
		if (srcCell.getCellComment() != null) {
			distCell.setCellComment(srcCell.getCellComment());
		}

		// 不同数据类型处理
		int srcCellType = srcCell.getCellType();
		distCell.setCellType(srcCellType);
		if (copyValueFlag) {
			if (srcCellType == HSSFCell.CELL_TYPE_NUMERIC) {
				if (HSSFDateUtil.isCellDateFormatted(srcCell)) {
					distCell.setCellValue(srcCell.getDateCellValue());
				} else {
					distCell.setCellValue(srcCell.getNumericCellValue());
				}
			} else if (srcCellType == HSSFCell.CELL_TYPE_STRING) {
				distCell.setCellValue(srcCell.getRichStringCellValue());
			} else if (srcCellType == HSSFCell.CELL_TYPE_BLANK) {
				distCell.setCellValue(srcCell.getRichStringCellValue());
				// nothing21
			} else if (srcCellType == HSSFCell.CELL_TYPE_BOOLEAN) {
				distCell.setCellValue(srcCell.getBooleanCellValue());
			} else if (srcCellType == HSSFCell.CELL_TYPE_ERROR) {
				distCell.setCellErrorValue(srcCell.getErrorCellValue());
			} else if (srcCellType == HSSFCell.CELL_TYPE_FORMULA) {
				distCell.setCellFormula(srcCell.getCellFormula());
			} else { // nothing29
			}
		}
	}

	/**
	 * 获取class的 包括父类的
	 * 
	 * @param clazz
	 * @return
	 */
	public static Field[] getClassFields(Class<?> clazz) {
		List<Field> list = new ArrayList<Field>();
		Field[] fields;
		do {
			fields = clazz.getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				list.add(fields[i]);
			}
			clazz = clazz.getSuperclass();
		} while (clazz != Object.class && clazz != null);
		return list.toArray(fields);
	}

	/**
	 * 判断是不是集合的实现类
	 * 
	 * @param clazz
	 * @return
	 */
	public static boolean isCollection(Class<?> clazz) {
		return clazz.isAssignableFrom(List.class)
				|| clazz.isAssignableFrom(Set.class)
				|| clazz.isAssignableFrom(Collection.class);
		// String colleciton = "java.util.Collection";
		// Class<?>[] faces = clazz.getInterfaces();
		// for (Class<?> face : faces) {
		// if(face.getName().equals(colleciton)){
		// return true;
		// }else{
		// if(face.getSuperclass()!= Object.class&&face.getSuperclass()!=null){
		// return isCollection(face.getSuperclass());
		// }
		// }
		// }
		// if(clazz.getSuperclass()!=
		// Object.class&&clazz.getSuperclass()!=null){
		// return isCollection(clazz.getSuperclass());
		// }
		// return false;
	}

	/**
	 * 判断是否不要在这个excel操作中
	 * 
	 * @param field
	 * @param targetId
	 * @return
	 */
	public static boolean isNotUserExcelUserThis(Field field, String targetId) {
		boolean boo = true;
		if (field.getAnnotation(ExcelIgnore.class) != null) {
			boo = true;
		} else if (boo
				&& field.getAnnotation(ExcelCollection.class) != null
				&& isUseInThis(field.getAnnotation(ExcelCollection.class)
						.exportName(), targetId)) {
			boo = false;
		} else if (boo
				&& field.getAnnotation(Excel.class) != null
				&& isUseInThis(field.getAnnotation(Excel.class).exportName(),
						targetId)) {
			boo = false;
		} else if (boo
				&& field.getAnnotation(ExcelEntity.class) != null
				&& isUseInThis(field.getAnnotation(ExcelEntity.class)
						.exportName(), targetId)) {
			boo = false;
		}
		return boo;
	}

	/**
	 * 判断是不是使用
	 * 
	 * @param exportName
	 * @param targetId
	 * @return
	 */
	private static boolean isUseInThis(String exportName, String targetId) {
		return targetId == null || exportName.equals("")
				|| exportName.indexOf("_") < 0
				|| exportName.indexOf(targetId) != -1;
	}

	/**
	 * 是不是java基础类
	 * 
	 * @param field
	 * @return
	 */
	public static boolean isJavaClass(Field field) {
		Class<?> fieldType = field.getType();
		boolean isBaseClass = false;
		if (fieldType.isArray()) {
			isBaseClass = false;
		} else if (fieldType.isPrimitive() || fieldType.getPackage() == null
				|| fieldType.getPackage().getName().equals("java.lang")
				|| fieldType.getPackage().getName().equals("java.math")
				|| fieldType.getPackage().getName().equals("java.util")) {
			isBaseClass = true;
		}
		return isBaseClass;
	}

	/**
	 * 彻底创建一个对象
	 * 
	 * @param clazz
	 * @return
	 */
	public static Object createObject(Class<?> clazz, String targetId) {
		Object obj = null;
		String fieldname;
		Method setMethod;
		try {
			obj = clazz.newInstance();
			Field[] fields = getClassFields(clazz);
			for (Field field : fields) {
				if (isNotUserExcelUserThis(field, targetId)) {
					continue;
				}
				if (isCollection(field.getType())) {
					ExcelCollection collection = field
							.getAnnotation(ExcelCollection.class);
					fieldname = field.getName();
					setMethod = getMethod(fieldname, clazz, field.getType());
					setMethod.invoke(obj, ExcelPublicUtil.class
							.getClassLoader().loadClass(collection.type())
							.newInstance());
				} else if (!isJavaClass(field)) {
					fieldname = field.getName();
					setMethod = getMethod(fieldname, clazz, field.getType());
					setMethod.invoke(obj,
							createObject(field.getType(), targetId));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;

	}

	/**
	 * 获取方法
	 * 
	 * @param name
	 * @param pojoClass
	 * @return
	 * @throws Exception
	 */
	public static Method getMethod(String name, Class<?> pojoClass)
			throws Exception {
		StringBuffer getMethodName = new StringBuffer(GET);
		getMethodName.append(name.substring(0, 1).toUpperCase());
		getMethodName.append(name.substring(1));
		return pojoClass.getMethod(getMethodName.toString(), new Class[] {});
	}

	/**
	 * 获取方法
	 * 
	 * @param name
	 * @param pojoClass
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public static Method getMethod(String name, Class<?> pojoClass,
			Class<?> type) throws Exception {
		StringBuffer getMethodName = new StringBuffer(SET);
		getMethodName.append(name.substring(0, 1).toUpperCase());
		getMethodName.append(name.substring(1));
		return pojoClass.getMethod(getMethodName.toString(),
				new Class[] { type });
	}

	/**
	 * 
	 * @param photoByte
	 * @return
	 */
	public static String getFileExtendName(byte[] photoByte) {
		String strFileExtendName = "JPG";
		if ((photoByte[0] == 71) && (photoByte[1] == 73)
				&& (photoByte[2] == 70) && (photoByte[3] == 56)
				&& ((photoByte[4] == 55) || (photoByte[4] == 57))
				&& (photoByte[5] == 97)) {
			strFileExtendName = "GIF";
		} else if ((photoByte[6] == 74) && (photoByte[7] == 70)
				&& (photoByte[8] == 73) && (photoByte[9] == 70)) {
			strFileExtendName = "JPG";
		} else if ((photoByte[0] == 66) && (photoByte[1] == 77)) {
			strFileExtendName = "BMP";
		} else if ((photoByte[1] == 80) && (photoByte[2] == 78)
				&& (photoByte[3] == 71)) {
			strFileExtendName = "PNG";
		}
		return strFileExtendName;
	}

	/**
	 * 获取Excel2003图片
	 * 
	 * @param sheet
	 *            当前sheet对象
	 * @param workbook
	 *            工作簿对象
	 * @return Map key:图片单元格索引（1_1）String，value:图片流PictureData
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, PictureData> getSheetPictrues03(HSSFSheet sheet,
			HSSFWorkbook workbook) {
		Map<String, PictureData> sheetIndexPicMap = new HashMap<String, PictureData>();
		List<HSSFPictureData> pictures = workbook.getAllPictures();
		if (pictures.size() != 0) {
			for (HSSFShape shape : sheet.getDrawingPatriarch().getChildren()) {
				HSSFClientAnchor anchor = (HSSFClientAnchor) shape.getAnchor();
				if (shape instanceof HSSFPicture) {
					HSSFPicture pic = (HSSFPicture) shape;
					int pictureIndex = pic.getPictureIndex() - 1;
					HSSFPictureData picData = pictures.get(pictureIndex);
					String picIndex = String.valueOf(anchor.getRow1()) + "_"
							+ String.valueOf(anchor.getCol1());
					sheetIndexPicMap.put(picIndex, picData);
				}
			}
			return sheetIndexPicMap;
		} else {
			return (Map<String, PictureData>) sheetIndexPicMap.put(null, null);
		}
	}

	/**
	 * 获取Excel2007图片
	 * 
	 * @param sheet
	 *            当前sheet对象
	 * @param workbook
	 *            工作簿对象
	 * @return Map key:图片单元格索引（1_1）String，value:图片流PictureData
	 */
	public static Map<String, PictureData> getSheetPictrues07(XSSFSheet sheet,
			XSSFWorkbook workbook) {
		Map<String, PictureData> sheetIndexPicMap = new HashMap<String, PictureData>();
		for (POIXMLDocumentPart dr : sheet.getRelations()) {
			if (dr instanceof XSSFDrawing) {
				XSSFDrawing drawing = (XSSFDrawing) dr;
				List<XSSFShape> shapes = drawing.getShapes();
				for (XSSFShape shape : shapes) {
					XSSFPicture pic = (XSSFPicture) shape;
					XSSFClientAnchor anchor = pic.getPreferredSize();
					CTMarker ctMarker = anchor.getFrom();
					String picIndex = ctMarker.getRow() + "_"
							+ ctMarker.getCol();
					sheetIndexPicMap.put(picIndex, pic.getPictureData());
				}
			}
		}
		return sheetIndexPicMap;
	}

}
