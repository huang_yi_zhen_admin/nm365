package cn.gov.xnc.system.excel.entity;

/**
 * 模板导出参数设置
 * 
 * @author ljz
 */
public class TemplateExportParams {

	public TemplateExportParams() {

	}

	public TemplateExportParams(String templateUrl) {
		this.templateUrl = templateUrl;
	}

	public TemplateExportParams(String templateUrl,
			int sheetNum ,boolean mergeSheet) {
		this.templateUrl = templateUrl;
		this.sheetNum = sheetNum;
		this.mergeSheet = mergeSheet;
	}
	
	public TemplateExportParams(String templateUrl,
			String sheetName ,boolean mergeSheet) {
		this.templateUrl = templateUrl;
		this.sheetName = sheetName;
		this.mergeSheet = mergeSheet;
	}
	
	public TemplateExportParams(String templateUrl, String sheetName,
			int sheetNum,boolean mergeSheet) {
		this.templateUrl = templateUrl;
		this.sheetName = sheetName;
		this.sheetNum = sheetNum;
		this.mergeSheet = mergeSheet;
	}

	/**
	 * 模板的路径
	 */
	private String templateUrl;

	/**
	 * 需要导出的第几个 sheetNum,默认是第0个
	 */
	private int sheetNum = 0;
	/**
	 * 这只sheetName 不填就使用原来的
	 */
	private String sheetName;
	
	/**
	 * mergeSheet 是否合并工作表 默认false 不合并
	 */
	private boolean mergeSheet = false;

	public String getTemplateUrl() {
		return templateUrl;
	}

	public void setTemplateUrl(String templateUrl) {
		this.templateUrl = templateUrl;
	}

	public int getSheetNum() {
		return sheetNum;
	}

	public void setSheetNum(int sheetNum) {
		this.sheetNum = sheetNum;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	

	/**
	 * @return mergeSheet 是否合并工作表 默认false 不合并
	 */
	public boolean isMergeSheet() {
		return mergeSheet;
	}

	/**
	 * @param mergeSheet 是否合并工作表 默认false 不合并
	 */
	public void setMergeSheet(boolean mergeSheet) {
		this.mergeSheet = mergeSheet;
	}
	
	
	
}
