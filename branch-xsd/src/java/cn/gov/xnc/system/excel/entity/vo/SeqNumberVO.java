package cn.gov.xnc.system.excel.entity.vo;

public class SeqNumberVO {
	private int seqNum = 1;

	public int getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}
	
}
