package cn.gov.xnc.system.excel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

import cn.gov.xnc.system.core.util.ExceptionUtil;
import cn.gov.xnc.system.core.util.RootUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.excel.annotation.Excel;
import cn.gov.xnc.system.excel.annotation.ExcelCollection;
import cn.gov.xnc.system.excel.annotation.ExcelTarget;
import cn.gov.xnc.system.excel.entity.ComparatorExcelField;
import cn.gov.xnc.system.excel.entity.ExcelExportEntity;
import cn.gov.xnc.system.excel.entity.TemplateExportParams;
import cn.gov.xnc.system.excel.entity.vo.POIConstants;
import cn.gov.xnc.system.excel.entity.vo.SeqNumberVO;
/**
 * Excel 导出根据模板导出
 * @author ljz
 * @version 1.0
 */
public final class ExcelExportOfTemplateUtil {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(ExcelExportOfTemplateUtil.class);
	
	
	public static Workbook exportExcelByArray(TemplateExportParams params,
			Class<?> pojoClass, ArrayList<Collection<?>> dataSet, Map<String, Object> map) {		
		return createSheetInUserModel2FileByTemplate(params, pojoClass,
				dataSet, map);
	}
	
	/**
	 * 导出文件通过模板解析
	 * 
	 * @param entity  导出参数类
	 * @param pojoClass 对应实体
	 * @param dataSet 实体集合
	 * @param map 模板集合
	 * @return
	 *//*
	public static Workbook exportExcelByArray(TemplateExportParams params,
			Class<?> pojoClass, ArrayList<Collection<?>> dataSet, Map<String, Object> map) {
		return createSheetInUserModel2FileByTemplate(params, pojoClass,
				dataSet, map);
	}*/
	
	/**
	 * 导出文件通过模板解析只有模板,没有集合
	 * 
	  * @param entity  导出参数类
	 * @param pojoClass 对应实体
	 * @param map 模板集合
	 * @return
	 */
	public static Workbook exportExcel(TemplateExportParams params,
			Map<String, Object> map) {
		return createSheetInUserModel2FileByTemplate(params, null,
				null, map);
	}

	private static Workbook createSheetInUserModel2FileByTemplate(
			TemplateExportParams params, Class<?> pojoClass,
			ArrayList<Collection<?>> dataSet, Map<String, Object> map) {
		// step 1. 判断模板的地址
		if (StringUtils.isEmpty(params.getTemplateUrl())) {
			return null;
		}
		Workbook wb = null;
		// step 2. 判断模板的Excel类型,解析模板
		try {
			wb = getCloneWorkBook(params);
		
			Sheet sheet = null; 
			/**
			 * 记录每一个sheet的总行数
			 */
			Map<Integer,Integer> numMap = new HashMap();
			/**
			 * 序号
			 */
			SeqNumberVO seqNumVo = new SeqNumberVO();
			for (int i= 0;i <= wb.getNumberOfSheets()-1; i++) {
				sheet = wb.getSheetAt(i);
				// step 4. 解析模板
				parseTemplate(sheet, map);				
				
				if(dataSet != null&& dataSet.size()>0&& dataSet.get(i)!=null){
					// step 5. 正常的数据填充,并且把最后行数存起来
					numMap.put(i,addDataToSheet(params, pojoClass, (Collection<Object>)dataSet.get(i), sheet, wb,seqNumVo)+1);
				}else{
					if(dataSet.size()>0&&dataSet.get(i)==null){
						if(i==wb.getNumberOfSheets()-1){
							sheet.removeRow( sheet.getRow(sheet.getLastRowNum()));
							numMap.put(i, sheet.getLastRowNum()-1);
						}else{
							numMap.put(i, sheet.getLastRowNum());
						}
					}
				}
			}
			
			HSSFSheet toSheet = null;
			int beginRowNum = 0;
			HSSFSheet fromSheet = null;
			
			
			/**
			 * 合并sheet
			 */
			if( params.isMergeSheet()){
				for (int i = 0; i <= wb.getNumberOfSheets()-1; i++){
					
					if(i!=0){
						fromSheet = (HSSFSheet) wb.getSheetAt(i);
					}else{
						toSheet = (HSSFSheet) wb.getSheetAt(0); 					
					}
					
					//beginRowNum = wb.getSheetAt(0).getLastRowNum();
					beginRowNum = 0;
					for(int j=0;j<i;j++){
						beginRowNum=beginRowNum+numMap.get(j);
					}
				
					if(i!=0){
						ExcelPublicUtil.copySheet(beginRowNum,(HSSFWorkbook)wb, fromSheet,toSheet, true);
					}
				}
				
			/**
			 * 移除其他多余的sheet  
			 */
				for (int i = wb.getNumberOfSheets()-1; i >= 0; i--) {
					if (i != 0) {
						wb.removeSheetAt(i);
					}
				}
			}
			
			
			
		} catch (Exception e) {
			logger.error("导出出错:"+ExceptionUtil.getExceptionMessage(e));
			return null;
		}
		return wb;
	}
	
	/**
	 * 克隆excel防止操作原对象,workbook无法克隆,只能对excel进行克隆
	 * @param params 
	 * @throws Exception 
	 */
	private static Workbook getCloneWorkBook(TemplateExportParams params) throws Exception {
		Workbook wb = null;
	//	String path = PathUtils.getRootDir();
		//path = 
		/*String path = Thread.currentThread().getContextClassLoader().getResource("").toURI().getPath();
		path = path.replace("WEB-INF/classes/","");
		path = path.replace("file:/","")+params.getTemplateUrl();*/
		String path = RootUtil.getRootDir()+File.separator+params.getTemplateUrl();
		
		FileInputStream fileis  = new FileInputStream(path);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    byte[] buffer = new byte[1024];
	    int len;
	    while ((len = fileis.read(buffer)) > -1 ) {
	        baos.write(buffer, 0, len);
	    }
	    baos.flush();
	    InputStream is = new ByteArrayInputStream(baos.toByteArray()); 
	    wb = WorkbookFactory.create(is);
	    //关闭用到的流
		baos.close();
	    fileis.close();
	    is.close();
		return wb;
		
	}

	/**
	 * 往Sheet 填充正常数据
	 * 
	 * @param params
	 * @param pojoClass
	 * @param dataSet
	 * @param workbook
	 */
	private static int addDataToSheet(TemplateExportParams params,
			Class<?> pojoClass, Collection<?> dataSet, Sheet sheet,
			Workbook workbook,SeqNumberVO seqNumVo) throws Exception {
		Drawing patriarch = sheet.createDrawingPatriarch();
		List<ExcelExportEntity> excelParams = new ArrayList<ExcelExportEntity>();
		// 得到所有字段
		Field fileds[] = ExcelPublicUtil.getClassFields(pojoClass);
		ExcelTarget etarget = pojoClass.getAnnotation(ExcelTarget.class);
		String targetId = null;
		if (etarget != null) {
			targetId = etarget.id();
		}
		getAllExcelField(targetId, fileds, excelParams, pojoClass, null);
		sortAllParams(excelParams);
		Iterator<?> its = dataSet.iterator();
		int index = sheet.getLastRowNum();
		
	
		Map<Integer,HSSFCell> styles = ExcelPublicUtil.getAllColumnStyle((HSSFWorkbook)workbook,(HSSFRow)sheet.getRow(index));
		/**
		 * 主要是为了覆盖参考模板的那一行
		 */
		index = index-1;
		while (its.hasNext()) {			
			Object t = its.next();
			index += createCells(patriarch, index, t, excelParams, sheet,
					workbook,styles,seqNumVo);
			seqNumVo.setSeqNum(seqNumVo.getSeqNum()+1);
		}
		return index;
	}

	private static void parseTemplate(Sheet sheet, Map<String, Object> map)
			throws Exception {
		Iterator<Row> rows = sheet.rowIterator();
		Row row;
		while (rows.hasNext()) {
			row = rows.next();
			for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
				setValueForCellByMap(row.getCell(i), map);
			}
		}
	}

	/**
	 * 给每个Cell通过解析方式set值
	 * 
	 * @param cell
	 * @param map
	 */
	private static void setValueForCellByMap(Cell cell, Map<String, Object> map)
			throws Exception {
		String oldString;
		try {//step 1. 判断这个cell里面是不是函数
			oldString = cell.getStringCellValue();
		} catch (Exception e) {
			return;
		}
		if(StringUtils.isNotEmpty(oldString)&&oldString!=null&&oldString.indexOf("{{") != -1){
			// setp 2. 判断是否含有解析函数
			String params;
			while (oldString.indexOf("{{") != -1) {
				params = oldString.substring(oldString.indexOf("{{")+2,
						oldString.indexOf("}}"));
				oldString = oldString.replace("{{"+params+"}}",
						getParamsValue(params.trim(), map));
			}
			if(StringUtil.isInteger(oldString)){
				cell.setCellValue(Double.valueOf(oldString));
			}else if(StringUtil.isFloatNumeric(oldString)){
				cell.setCellValue(Double.valueOf(oldString));
			}else{
				cell.setCellValue(oldString);				
			}
		
		}
	}

	/**
	 * 获取参数值
	 * 
	 * @param params
	 * @param map
	 * @return
	 */
	private static String getParamsValue(String params, Map<String, Object> map)
			throws Exception {
		if (params.indexOf(".") != -1) {
			String[] paramsArr = params.split("\\.");
			return getValueDoWhile(map.get(paramsArr[0]), paramsArr, 1);
		}
	//	System.out.println(map.get(params)+",ssss");
		 String result = "";
		 
		 if(map.containsKey(params)){
			if( map.get(params)!=null){
				result = map.get(params).toString();
			}
		 }
		
		 return result;
		 
	}

	/**
	 * 通过遍历过去对象值
	 * 
	 * @param object
	 * @param paramsArr
	 * @param index
	 * @return
	 * @throws Exception
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	@SuppressWarnings("rawtypes")
	private static String getValueDoWhile(Object object, String[] paramsArr,
			int index) throws Exception {
		if(object == null){return "";}
		if (object instanceof Map) {
			object = ((Map) object).get(paramsArr[index]);
		} else {
			object = ExcelPublicUtil.getMethod(paramsArr[index],
					object.getClass()).invoke(object, new Object[] {});
		}
		return (index == paramsArr.length - 1) ? (object == null?"":object.toString())
				: getValueDoWhile(object, paramsArr, ++index);
	}

	/**
	 * 对字段根据用户设置排序
	 */
	private static void sortAllParams(List<ExcelExportEntity> excelParams) {
		Collections.sort(excelParams, new ComparatorExcelField());
		for (ExcelExportEntity entity : excelParams) {
			if (entity.getList() != null) {
				Collections.sort(entity.getList(), new ComparatorExcelField());
			}
		}
	}

	/**
	 * 创建 最主要的 Cells
	 * 
	 * @param styles
	 * @throws Exception
	 */
	private static int createCells(Drawing patriarch, int index, Object t,
			List<ExcelExportEntity> excelParams, Sheet sheet, Workbook workbook,Map<Integer,HSSFCell> styles,SeqNumberVO seqNumVo)
			throws Exception {
		ExcelExportEntity entity;
        HSSFRow row = (HSSFRow) sheet.createRow(index+1);  
        int cellNum = 0;
        for (int k = 0, paramSize = excelParams.size(); k < paramSize; k++) {
        	entity = excelParams.get(k);
        	Object value = getCellValue(entity, t);
        	createStringCell(row, cellNum++, value.toString(), entity,
					workbook,styles,seqNumVo);
        }
		
		return 1;
		

	}

	/**
	 * 获取填如这个cell的值,提供一些附加功能
	 * 
	 * @param entity
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	private static Object getCellValue(ExcelExportEntity entity, Object obj)
			throws Exception {
		/*Object value = entity.getGetMethods() != null ? getFieldBySomeMethod(
				entity.getGetMethods(), obj) : entity.getGetMethod().invoke(
				obj, new Object[] {});*/
		Object value = obj.getClass().getMethod(entity.getGetMethod().getName(),new Class[0]).invoke(obj, new Object[] {});
		// step 1 判断是不是日期,需不需要格式化
		if (StringUtils.isNotEmpty(entity.getExportFormat())
				&& StringUtils.isNotEmpty(entity.getDatabaseFormat())) {
			Date temp = null;
			if (value instanceof String) {
				SimpleDateFormat format = new SimpleDateFormat(
						entity.getDatabaseFormat());
				temp = format.parse(value.toString());
			} else if (value instanceof Date) {
				temp = (Date) value;
			}
			if (temp != null) {
				SimpleDateFormat format = new SimpleDateFormat(
						entity.getExportFormat());
				value = format.format(temp);
			}
		}
		return value == null ? "" : value.toString();
	}

	/**
	 * 创建List之后的各个Cells
	 * 
	 * @param styles
	 */
	private static void createListCells(Drawing patriarch, int index,
			int cellNum, Object obj, List<ExcelExportEntity> excelParams,
			Sheet sheet, Workbook workbook) throws Exception {
		/*ExcelExportEntity entity;
		Row row;
		if (sheet.getRow(index) == null) {
			row = sheet.createRow(index);
			row.setHeight((short) 350);
		} else {
			row = sheet.getRow(index);
		}
		for (int k = 0, paramSize = excelParams.size(); k < paramSize; k++) {
			entity = excelParams.get(k);
			Object value = getCellValue(entity, obj);
			if (entity.getType() != 2) {
				createStringCell(row, cellNum++,
						value == null ? "" : value.toString(), entity, workbook,null);
			} else {
				createImageCell(patriarch, entity, row, cellNum++,
						value == null ? "" : value.toString(), obj, workbook);
			}
		}*/
	}

	/**
	 * 多个反射获取值
	 * 
	 * @param list
	 * @param t
	 * @return
	 * @throws Exception
	 */
	private static Object getFieldBySomeMethod(List<Method> list, Object t)
			throws Exception {
		for (Method m : list) {
			if (t == null) {
				t = "";
				break;
			}
			t = m.invoke(t, new Object[] {});
		}
		return t;
	}

	/**
	 * 创建文本类型的Cell
	 * 
	 * @param row
	 * @param index
	 * @param text
	 * @param style
	 * @param entity
	 * @param workbook
	 */
	private static void createStringCell(Row row, int index, String text,
			ExcelExportEntity entity, Workbook workbook,Map<Integer,HSSFCell> styles,SeqNumberVO seqNumVo) {
		Cell cell = row.createCell(index);
		
		HSSFCell sourceCell = styles.get(entity.getOrderNum());
		HSSFCellStyle style2 = sourceCell.getCellStyle();
		if(style2==null){
			HSSFCellStyle newstyle= ExcelExportUtil.getOneStyle((HSSFWorkbook)workbook, true);
			style2 = newstyle; 
		}
		cell.setCellStyle(style2);
		int srcCellType = sourceCell.getCellType();
		
		if(srcCellType== HSSFCell.CELL_TYPE_STRING){
			if(POIConstants.seqNum.equalsIgnoreCase(sourceCell.getStringCellValue())){
				cell.setCellValue(Double.valueOf(seqNumVo.getSeqNum()));
				return;
			}
		}
		

		if(StringUtils.isNotEmpty(text)){
			if(StringUtils.isNotEmpty(entity.getExportFormat())){  
			      short format = cell.getCellStyle().getDataFormat();  
			      SimpleDateFormat sdf = new SimpleDateFormat(entity.getExportFormat());
			      if(format == 14 || format == 31 || format == 57 || format == 58){  
			          //日期  
			          sdf = new SimpleDateFormat("yyyy-MM-dd");  
			      }else if (format == 20 || format == 32) {  
			          //时间  
			          sdf = new SimpleDateFormat("HH:mm");  
			      }  
			      
		     
					
		      try {
					cell.setCellValue(sdf.parse(text));
			  } catch (ParseException e) {
					logger.error("内容值为:"+text+"导出报表出现错误,主要是格式的问题:"+ExceptionUtil.getExceptionMessage(e));
					
			  }
			
			}
//			else if(StringUtil.isInteger(text)){
//				cell.setCellValue(Double.valueOf(text));
//			}else if(StringUtil.isFloatNumeric(text)){
//				cell.setCellValue(Double.valueOf(text));
//			}
			else{
				/**
				 * 函数
				 */
				if (entity.getType()==2) {
					cell.setCellType(Cell.CELL_TYPE_FORMULA);
					cell.setCellFormula(entity.getCellFormula());
				}else{
					RichTextString Rtext = workbook instanceof HSSFWorkbook ? new HSSFRichTextString(
						text) : new XSSFRichTextString(text);
						cell.setCellValue(text);
				}
				
			}
		}
		
		
	}

	/**
	 * 图片类型的Cell
	 * 
	 * @param patriarch
	 * 
	 * @param entity
	 * @param row
	 * @param i
	 * @param string
	 * @param obj
	 * @param workbook
	 * @throws Exception
	 */
	private static void createImageCell(Drawing patriarch,
			ExcelExportEntity entity, Row row, int i, String field,
			Object obj, Workbook workbook) throws Exception {
		

	}

	/**
	 * 获取需要导出的全部字段
	 * 
	 * @param targetId
	 *            目标ID
	 * @param filed
	 * @throws Exception
	 */
	private static void getAllExcelField(String targetId, Field[] fields,
			List<ExcelExportEntity> excelParams, Class<?> pojoClass,
			List<Method> getMethods) throws Exception {
		// 遍历整个filed
		ExcelExportEntity excelEntity;
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			// 先判断是不是collection,在判断是不是java自带对象,之后就是我们自己的对象了
			if (ExcelPublicUtil.isNotUserExcelUserThis(field, targetId)) {
				continue;
			}
			if (ExcelPublicUtil.isCollection(field.getType())) {
				ExcelCollection excel = field
						.getAnnotation(ExcelCollection.class);
				ParameterizedType pt = (ParameterizedType) field
						.getGenericType();
				Class<?> clz = (Class<?>) pt.getActualTypeArguments()[0];
				List<ExcelExportEntity> list = new ArrayList<ExcelExportEntity>();
				getExcelFieldList(targetId,
						ExcelPublicUtil.getClassFields(clz), clz, list, null);
				excelEntity = new ExcelExportEntity();
				excelEntity.setName(getExcelName(excel.exportName(), targetId));
				excelEntity
						.setOrderNum(getCellOrder(excel.orderNum(), targetId));
				excelEntity.setGetMethod(ExcelPublicUtil.getMethod(
						field.getName(), pojoClass));
				excelEntity.setList(list);
				excelParams.add(excelEntity);
			} else if (ExcelPublicUtil.isJavaClass(field)) {
				Excel excel = field.getAnnotation(Excel.class);
				excelEntity = new ExcelExportEntity();
				excelEntity.setType(excel.exportType());
				excelEntity.setExportFormat(excel.exportFormat());				
				getExcelField(targetId, field, excelEntity, excel, pojoClass);
				if (getMethods != null) {
					List<Method> newMethods = new ArrayList<Method>();
					newMethods.addAll(getMethods);
					newMethods.add(excelEntity.getGetMethod());
					excelEntity.setGetMethods(newMethods);
				}
				excelParams.add(excelEntity);
			} else {
				List<Method> newMethods = new ArrayList<Method>();
				if (getMethods != null) {
					newMethods.addAll(getMethods);
				}
				newMethods.add(ExcelPublicUtil.getMethod(field.getName(),
						pojoClass));
				getAllExcelField(targetId,
						ExcelPublicUtil.getClassFields(field.getType()),
						excelParams, field.getType(), newMethods);
			}
		}
	}

	/**
	 * 判断在这个单元格显示的名称
	 * 
	 * @param exportName
	 * @param targetId
	 * @return
	 */
	private static String getExcelName(String exportName, String targetId) {
		if (exportName.indexOf(",") < 0) {
			return exportName;
		}
		String[] arr = exportName.split(",");
		for (String str : arr) {
			if (str.indexOf(targetId) != -1) {
				return str.split("_")[0];
			}
		}
		return null;
	}

	/**
	 * 获取这个字段的顺序
	 * 
	 * @param orderNum
	 * @param targetId
	 * @return
	 */
	private static int getCellOrder(String orderNum, String targetId) {
		if (isInteger(orderNum) || targetId == null) {
			return Integer.valueOf(orderNum);
		}
		String[] arr = orderNum.split(",");
		for (String str : arr) {
			if (str.indexOf(targetId) != -1) {
				return Integer.valueOf(str.split("_")[0]);
			}
		}
		return 0;
	}

	/**
	 * 判断字符串是否是整数
	 */
	private static boolean isInteger(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * 
	 * @param targetId
	 * @param fields
	 * @param pojoClass
	 * @param list
	 * @param getMethods
	 * @throws Exception
	 */
	private static void getExcelFieldList(String targetId, Field[] fields,
			Class<?> pojoClass, List<ExcelExportEntity> list,
			List<Method> getMethods) throws Exception {
		ExcelExportEntity excelEntity;
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			if (ExcelPublicUtil.isNotUserExcelUserThis(field, targetId)) {
				continue;
			}
			if (ExcelPublicUtil.isJavaClass(field)) {
				Excel excel = field.getAnnotation(Excel.class);
				excelEntity = new ExcelExportEntity();
				getExcelField(targetId, field, excelEntity, excel, pojoClass);
				if (getMethods != null) {
					List<Method> newMethods = new ArrayList<Method>();
					newMethods.addAll(getMethods);
					newMethods.add(excelEntity.getGetMethod());
					excelEntity.setGetMethods(newMethods);
				}
				list.add(excelEntity);
			} else {
				List<Method> newMethods = new ArrayList<Method>();
				if (getMethods != null) {
					newMethods.addAll(getMethods);
				}
				newMethods.add(ExcelPublicUtil.getMethod(field.getName(),
						pojoClass));
				getExcelFieldList(targetId,
						ExcelPublicUtil.getClassFields(field.getType()),
						field.getType(), list, newMethods);
			}
		}
	}

	/**
	 * 
	 * @param targetId
	 * @param field
	 * @param excelEntity
	 * @param excel
	 * @param pojoClass
	 * @throws Exception
	 */
	private static void getExcelField(String targetId, Field field,
			ExcelExportEntity excelEntity, Excel excel, Class<?> pojoClass)
			throws Exception {
		excelEntity.setName(getExcelName(excel.exportName(), targetId));
		excelEntity.setWidth(excel.exportFieldWidth());
		excelEntity.setHeight(excel.exportFieldHeight());
		excelEntity.setNeedMerge(excel.needMerge());
		excelEntity.setOrderNum(getCellOrder(excel.orderNum(), targetId));
		excelEntity.setWrap(excel.isWrap());
		excelEntity.setExportImageType(excel.imageType());
		excelEntity.setType(excel.exportType());
		excelEntity.setCellFormula(excel.cellFormula());
		String fieldname = field.getName();
		excelEntity.setGetMethod(ExcelPublicUtil
				.getMethod(fieldname, pojoClass));
		if (excel.exportConvertSign() == 1 || excel.imExConvert() == 1) {
			StringBuffer getConvertMethodName = new StringBuffer("convertGet");
			getConvertMethodName
					.append(fieldname.substring(0, 1).toUpperCase());
			getConvertMethodName.append(fieldname.substring(1));
			Method getConvertMethod = pojoClass.getMethod(
					getConvertMethodName.toString(), new Class[] {});
			excelEntity.setGetMethod(getConvertMethod);
		}
	}

}
