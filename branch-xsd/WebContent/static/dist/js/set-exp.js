function ExpSet(conf){
	var Conf={
		'name':'provs',
		'index':0,
		"split":",",
		"reg":/[国]/,
		"emptyText":'<span class="text-gray">未选择城市</span>'
	}
	$.extend(Conf, conf);
	
	var proceData={
		"海口":["40288030540efeeb01540eff7af50a0d:秀英区","40288030540efeeb01540eff7af50a0e:龙华区","40288030540efeeb01540eff7af50a0f:琼山区","40288030540efeeb01540eff7af50a10:美兰区","40288030540efeeb01540eff7af50a0c:市辖区"]
		},
	domTpl=[
		'<p class="city-show-text text-blue">未选择城市</p>',
		'<div class="btn-group">',
		  '<button class="btn btn-default" type="button">选择区域</button>',
          '<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button" aria-expanded="false">',
            '<span class="caret"></span>',
            '<span class="sr-only">Toggle Dropdown</span>',
          '</button>',
          '<ul role="menu" class="dropdown-menu">',
          '</ul>',
		'</div>'
		
	].join(""),
	wrapCls='sel-provice-wrap';

	this.init=function(div){
		div=div||$("body");
		div.find("."+wrapCls).each(function(i, el) {
			el = $(el);
			el.append(domTpl);
			if(!el.find("input[type=hidden]").size()){
				el.append('<input type="hidden" value="" name="'+Conf.name+'-'+i+'" v="'+i+'" />')
			}else{
				var p=el.find('.city-show-text'),
					input = el.find("input[type=hidden]");
				p.html(input.val().replace(/\w{32}:/g,"")||Conf.emptyText)

			}
		});
	}

	function showSel(ul,ipt){
		var vals = ipt.val()||"",//自己选择的
			all = "",
			html=[],
			ckecked="",
			disabled="",
			v="",
			cv="";
		$("body").find(".sel-provice-wrap input[type=hidden]").each(function(i,el){
			all+=el.value+Conf.split;
		})
		for(var key in proceData){
			html.push('<li><h3 class="zone-ck"><label><input type="checkbox" value="0">'+key+'</label></h3><div class="pro-ck">');
			for(var i=0,l=proceData[key].length;i<l;i++){
				v=proceData[key][i].replace(Conf.reg,"");
				cv = v.split(":")[1];
				ckecked = vals.indexOf(v)!=-1 ? "checked":"";
				disabled = !ckecked && all.indexOf(v)!=-1 ? "disabled":""
				html.push('<label class="'+disabled+'"><input type="checkbox" value="'+v+'" '+ckecked+" "+disabled+'>'+cv+'</label>')
			}
			html.push('</div></li>')
		}
		ul.html(html.join(""));

		var cks= ul.find(".pro-ck");
		cks.each(function(i,el){
			checkParent($(el).find("input:first"));
		});
	}

	function setSelZone(wrap){
		//选中的所有的
		var input = wrap.find("input[type=hidden]"),
			cks = wrap.find("input[type=checkbox]"),
			p=wrap.find(".city-show-text"),
			vals="",
			arr=[];

		cks.each(function(i,el){
			if(el.value!=="0" && el.checked){
				arr.push(el.value.replace(Conf.reg,""));
			}
		});
		vals = arr.length ? arr.join(Conf.split) : "";
		input.val(vals);
		p.html(vals.replace(/\w{32}:/g,"")||Conf.emptyText);
		
	}
	function setSel(ck,isSel){

		if(ck){
			if(isSel){
				ck.prop({
					checked: true,
					indeterminate: false,
				})
			}else{
				ck.prop({
					checked: false,
					indeterminate: false,
				})
			}
		}
		if(ck.val()=="0"){
			var wrap = ck.closest('li'),
				cks =wrap.find(".pro-ck input[type=checkbox]");
				cks.each(function(i,el){
					console.log(el.disabled)
					if(!el.disabled){
						setSel($(el),isSel);
					}
				});
				checkParent($(cks.get(0)));
			return;
		}		
		//		
		
	}

	//事件
	function checkParent(ipt){
		if(ipt.val()=="0"){//第一级
			return false;
		}
		var wrap = ipt.closest('li'),
			pp = wrap.find(".zone-ck input"),
			allNum = wrap.find(".pro-ck input").size(),
			selNum = wrap.find(".pro-ck input:checked").size();

		if(allNum==selNum){
			pp.prop({checked:true,indeterminate:false});
		}else if(selNum==0){
			pp.prop({checked:false,indeterminate:false});
		}else{
			pp.prop({
				checked: false,
				indeterminate: true,
			})
		}
    }

  


	$("body").delegate('.'+wrapCls+" .btn", 'click', function(event) {
		var me = $(this),
			wrap = me.closest('.'+wrapCls),
			group= wrap.find(".btn-group"),
			cls ="open",
			ul=wrap.find(".dropdown-menu"),
			ipt=wrap.find("input[type=hidden]");
		if(group.hasClass(cls)){
			group.removeClass(cls);
		}else{
			group.addClass(cls);
			showSel(ul,ipt)
		}
		return false;
	}).delegate('.'+wrapCls+" label,."+wrapCls+" input", 'click', function(event) {
		var me = $(this),
			wrap = me.closest('.'+wrapCls),
			ck=me.find("input"),
			sel=true;
		
		if(ck.size()){//点击 label
			if(ck.get(0).disabled){
				return;
			}
			if(ck.get(0).checked){
				sel=false;
			}
			setSel(ck,sel);
			checkParent(ck);
			setSelZone(wrap);

		}else {//点击的是 input
			ck=me;
			if(!ck.get(0).checked){
				sel=false;
			}
			setTimeout(function(){
				//console.log("?????"+sel)
				setSel(ck,sel);
				checkParent(ck);
				setSelZone(wrap);
			},0);
			
			return false;
		}
		
		
		
		

	}).delegate('.'+wrapCls+" .dropdown-menu", 'click', function(event) {
			return false;
	});
}

function initExpTable(){
	var Conf={
		tableEl:$(".exp-table"),
		expNumIpt:$("#expLenIpt"),
		trTpl:[
				'<tr>',
					'<td class="sel-provice-wrap"><input name="{n0}" type="hidden" value="{v0}"></td>',
					'<td><input type="text" name="{n1}" value="{v1}"  class="form-control"></td>',
					'<td><input type="text" name="{n2}" value="{v2}"  class="form-control"></td>',
					'<td><input type="text" name="{n3}" value="{v3}"  class="form-control"></td>',
					'<td><input type="text" name="{n4}" value="{v4}" class="form-control"></td>',

					'<td><input type="hidden" name="{n5}" value="{v5}"><button class="btn btn-default del-btn" type="button"><i class="fa fa-fw fa-remove text-red"></i>删除</button></td>',
				'</tr>'
		].join("")
	}

	//刷新行的ext
	function refresh(){
		var num=0;
		Conf.tableEl.find("tr").each(function(i,el){
			if(i!=0){
				$(el).find("input").attr({'ext':i-1});
				num++;
			}
		})
		Conf.expNumIpt.val(num);
	}

	//删除事件
	Conf.tableEl.delegate('.del-btn', 'click', function(event) {
		if(confirm("您是否要删除该行数据？")){
			var tr = $(this).closest('tr');
			var freightDetailid = tr.find("[name*='freightDetailId']").val();
			$.ajax({
		         type: 'GET',
		         url: '/freightDetailsController/del?id='+ freightDetailid,
		         data: null,
		         dataType: "json",
		         success: function (data) {
		        	 if(data && data.success){
		        		 tr.remove();
				 			refresh();
		        	 }else{
		        		 Comm.showMsg("删除运费模板失败：" + data.msg);
		        	 }
		         }
		     });
			
			
		}
	});
	Conf.tableEl.html([
		'<tr>',
			'<th>运送到</th>',
			'<th>首件(件)</th>',
			'<th>首费(元)</th>',
			'<th>续件(件)</th>',
			'<th>续费(元)</th>',
			'<th>操作</th>',
		'</tr>'
	].join(""));

	var expSet=new ExpSet();
	



	var index=0;
	this.addTr=function(arr){
		var i=0,tpl=Conf.trTpl,
		obj = null,
		o={};

		arr= arr||[{"n0":''},{"n1":''},{"n2":''},{"n3":''},{"n4":''},{"n5":''}];
		for(var i=0,l=arr.length;i<l;i++){
			obj = arr[i];

			for(var key in obj){
				o["n"+i] = key;
				o["v"+i] = obj[key];
			}
		}
		o["r"] = index++;
console.log(o)
		
		var tr=$(Conf.trTpl.ZK_format(o));
		Conf.tableEl.append(tr);
		refresh();
		expSet.init(tr);
	}
	
}
