<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="stockProductList" title="库存相关联商品表" actionUrl="stockProductController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="库存关联商品唯一编码" field="productid" ></z:dgCol>
   <z:dgCol title="所属公司" field="companyid" ></z:dgCol>
   <z:dgCol title="商品所在仓库" field="stockid" ></z:dgCol>
   <z:dgCol title="库存数量" field="stocknum" ></z:dgCol>
   <z:dgCol title="库存预警值" field="alertnum" ></z:dgCol>
   <z:dgCol title="库存状态 1 充足 2 缺货 3 补货" field="status" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="stockProductController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="stockProductController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="stockProductController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="stockProductController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>