<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="airagSoilList" title="大气候土壤数据" actionUrl="airagSoilController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="aeid" field="aeid" ></z:dgCol>
   <z:dgCol title="土壤传感器id" field="sensorId" ></z:dgCol>
   <z:dgCol title="采集时间" field="collectTime" ></z:dgCol>
   <z:dgCol title="soilTem" field="soilTem" ></z:dgCol>
   <z:dgCol title="soilTemThreshold" field="soilTemThreshold" ></z:dgCol>
   <z:dgCol title="soilHum" field="soilHum" ></z:dgCol>
   <z:dgCol title="soilHumThreshold" field="soilHumThreshold" ></z:dgCol>
   <z:dgCol title="gpsX" field="gpsX" ></z:dgCol>
   <z:dgCol title="gpsY" field="gpsY" ></z:dgCol>
   <z:dgCol title="sensor" field="sensor" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="airagSoilController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="airagSoilController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="airagSoilController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="airagSoilController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>