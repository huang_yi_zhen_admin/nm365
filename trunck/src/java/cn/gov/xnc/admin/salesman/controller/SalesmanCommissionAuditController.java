package cn.gov.xnc.admin.salesman.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.salesman.entity.SalesmanCommissionAuditEntity;
import cn.gov.xnc.admin.salesman.service.SalesmanCommissionAuditServiceI;

/**   
 * @Title: Controller
 * @Description: 业务结算流水订单关系
 * @author zero
 * @date 2016-11-27 19:14:52
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/salesmanCommissionAuditController")
public class SalesmanCommissionAuditController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SalesmanCommissionAuditController.class);

	@Autowired
	private SalesmanCommissionAuditServiceI salesmanCommissionAuditService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 业务结算流水订单关系列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView salesmanCommissionAudit(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/salesmanCommissionAuditList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(SalesmanCommissionAuditEntity salesmanCommissionAudit,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(SalesmanCommissionAuditEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, salesmanCommissionAudit, request.getParameterMap());
		this.salesmanCommissionAuditService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除业务结算流水订单关系
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(SalesmanCommissionAuditEntity salesmanCommissionAudit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		salesmanCommissionAudit = systemService.getEntity(SalesmanCommissionAuditEntity.class, salesmanCommissionAudit.getId());
		message = "业务结算流水订单关系删除成功";
		salesmanCommissionAuditService.delete(salesmanCommissionAudit);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加业务结算流水订单关系
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(SalesmanCommissionAuditEntity salesmanCommissionAudit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(salesmanCommissionAudit.getId())) {
			message = "业务结算流水订单关系更新成功";
			SalesmanCommissionAuditEntity t = salesmanCommissionAuditService.get(SalesmanCommissionAuditEntity.class, salesmanCommissionAudit.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(salesmanCommissionAudit, t);
				salesmanCommissionAuditService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "业务结算流水订单关系更新失败";
			}
		} else {
			message = "业务结算流水订单关系添加成功";
			salesmanCommissionAuditService.save(salesmanCommissionAudit);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 业务结算流水订单关系列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(SalesmanCommissionAuditEntity salesmanCommissionAudit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(salesmanCommissionAudit.getId())) {
			salesmanCommissionAudit = salesmanCommissionAuditService.getEntity(SalesmanCommissionAuditEntity.class, salesmanCommissionAudit.getId());
			req.setAttribute("salesmanCommissionAuditPage", salesmanCommissionAudit);
		}
		return new ModelAndView("admin/salesman/salesmanCommissionAudit");
	}
}
