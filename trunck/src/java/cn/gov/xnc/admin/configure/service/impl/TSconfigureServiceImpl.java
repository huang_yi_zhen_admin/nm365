package cn.gov.xnc.admin.configure.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.configure.entity.TSconfigureEntity;
import cn.gov.xnc.admin.configure.service.TSconfigureServiceI;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("tSconfigureService")
@Transactional
public class TSconfigureServiceImpl extends CommonServiceImpl implements TSconfigureServiceI {
	
	
	public String getLockedStockId(){
		TSUser user = ResourceUtil.getSessionUserName();
		
		TSconfigureEntity stocklockconf = null;
		CriteriaQuery csp = new CriteriaQuery(TSconfigureEntity.class);
		csp.eq("configurename", TSconfigureEntity.LOCKSTOCK);
		csp.eq("company", user.getCompany().getId());
		csp.add();
		List<TSconfigureEntity> spList = getListByCriteriaQuery(csp, false);
		if( null == spList || spList.size() <= 0 ){
			return null;
		}
		
		stocklockconf = spList.get(0);
		if( StringUtil.isEmpty(stocklockconf.getConfigurevalue()) ){
			return null;
		}else{
			return stocklockconf.getConfigurevalue();
		}
	}
	
	public AjaxJson unlockStock(){
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		TSconfigureEntity stocklockconf = null;
		CriteriaQuery csp = new CriteriaQuery(TSconfigureEntity.class);
		csp.eq("configurename", TSconfigureEntity.LOCKSTOCK);
		csp.eq("company", user.getCompany().getId());
		csp.add();
		List<TSconfigureEntity> spList = getListByCriteriaQuery(csp, false);
		if( null == spList || spList.size() <= 0 ){
			j.setSuccess(true);
			j.setMsg("解锁成功");
			return j;
		}else{
			stocklockconf = spList.get(0);
			if( StringUtil.isEmpty(stocklockconf.getConfigurevalue())) {
				j.setSuccess(true);
				j.setMsg("解锁成功");
				return j;
			}
			
			stocklockconf.setConfigurevalue(null);
			
			updateEntitie(stocklockconf);
		}
		
		j.setSuccess(true);
		j.setMsg("解锁成功");
		
		return j;
	}
	
	
	public AjaxJson lockStock(String stockid){
		AjaxJson j = new AjaxJson();
		
		StockEntity stock = getEntity(StockEntity.class, stockid);
		if( stock == null ){
			j.setMsg("参数有误，请稍后再试");
			j.setSuccess(false);
			return j;
		}
		
		TSUser user = ResourceUtil.getSessionUserName();

		TSconfigureEntity stocklockconf = null;
		CriteriaQuery csp = new CriteriaQuery(TSconfigureEntity.class);
		csp.eq("configurename", TSconfigureEntity.LOCKSTOCK);
		csp.eq("company", user.getCompany().getId());
		csp.add();
		List<TSconfigureEntity> spList = getListByCriteriaQuery(csp, false);
		if( null == spList || spList.size() <= 0 ){
			stocklockconf = new TSconfigureEntity();
			
			stocklockconf.setCompany(user.getCompany().getId());
			stocklockconf.setConfigurename(TSconfigureEntity.LOCKSTOCK);
			stocklockconf.setConfigurevalue(stockid);
			stocklockconf.setRemark("仓库加锁配置，如加锁则value为仓库的id值，否则为null");
			
			save(stocklockconf);
		}else{
			stocklockconf = spList.get(0);
			if( StringUtil.isNotEmpty(stocklockconf.getConfigurevalue()) ){
				j.setMsg("目前已经有仓库("+stock.getStockname()+")处于加锁状态，请先解锁");
				j.setSuccess(false);
				return j;
			}
			stocklockconf.setConfigurevalue(stockid);
			
			updateEntitie(stocklockconf);
		}
		
		j.setSuccess(true);
		j.setMsg("仓库(" + stock.getStockname()+ ")加锁成功");
		
		return j;
	}
	
	
	/**
	 * 获取打印的票据编号序列号
	 * */
	public String getTicketSeq(){
		TSUser user = ResourceUtil.getSessionUserName();

		TSconfigureEntity ticketseq = null;
		CriteriaQuery csp = new CriteriaQuery(TSconfigureEntity.class);
		csp.eq("configurename", TSconfigureEntity.TICKETSEQ);
		csp.eq("company", user.getCompany().getId());
		csp.add();
		List<TSconfigureEntity> spList = getListByCriteriaQuery(csp, false);
		if( null == spList || spList.size() <= 0 ){
			ticketseq = new TSconfigureEntity();
			
			
			ticketseq.setCompany(user.getCompany().getId());
			ticketseq.setConfigurename(TSconfigureEntity.TICKETSEQ);
			ticketseq.setConfigurevalue("1");
			ticketseq.setRemark("打印的出库单的票据编号");
			
			save(ticketseq);
			
			String newString = String.format("%05d", Integer.parseInt(ticketseq.getConfigurevalue()));
			
			return newString;
		}else{
			ticketseq = spList.get(0);
			
			int seq = Integer.parseInt(ticketseq.getConfigurevalue());
			seq++;
			ticketseq.setConfigurevalue(""+seq);
			
			updateEntitie(ticketseq);
			
			String newString = String.format("%05d", seq);
			return newString;
		}
		
	}
	
}