package cn.gov.xnc.admin.configure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 系统静态配置表
 * @author zero
 * @date 2017-08-09 16:17:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_configure", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class TSconfigureEntity implements java.io.Serializable {
	
	public final static String TICKETSEQ = "ticketseq";
	
	public final static String LOCKSTOCK = "stocklock";
	
	/**id*/
	private String id;
	/**公司id*/
	private String company;
	/**配置名称*/
	private String configurename;
	/**配置值*/
	private String configurevalue;
	/**备注*/
	private String remark;
	
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public String getId(){
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  公司id
	 */
	@Column(name ="COMPANY",nullable=true,length=32)
	public String getCompany(){
		return this.company;
	}

	/**
	 *方法: 设置String
	 *@param: String  公司id
	 */
	public void setCompany(String company){
		this.company = company;
	}
	/**
	 *方法: 取得String
	 *@return: String  配置名称
	 */
	@Column(name ="CONFIGURENAME",nullable=true,length=255)
	public String getConfigurename(){
		return this.configurename;
	}

	/**
	 *方法: 设置String
	 *@param: String  配置名称
	 */
	public void setConfigurename(String configurename){
		this.configurename = configurename;
	}
	/**
	 *方法: 取得String
	 *@return: String  配置值
	 */
	@Column(name ="CONFIGUREVALUE",nullable=true,length=255)
	public String getConfigurevalue(){
		return this.configurevalue;
	}

	/**
	 *方法: 设置String
	 *@param: String  配置值
	 */
	public void setConfigurevalue(String configurevalue){
		this.configurevalue = configurevalue;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=255)
	public String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setRemark(String remark){
		this.remark = remark;
	}
}
