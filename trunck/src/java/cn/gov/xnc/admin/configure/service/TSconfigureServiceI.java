package cn.gov.xnc.admin.configure.service;

import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface TSconfigureServiceI extends CommonService{
	
	public String getTicketSeq();
	
	public String getLockedStockId();
	
	public AjaxJson unlockStock();
	
	public AjaxJson lockStock(String stockid);

}
