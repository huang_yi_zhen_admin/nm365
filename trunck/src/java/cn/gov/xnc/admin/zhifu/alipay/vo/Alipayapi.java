package cn.gov.xnc.admin.zhifu.alipay.vo;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;



/**   
 * @Title: 
 * @Description: 支付宝支付信息
 * @author zero
 * @date 2016-04-13 10:53:13
 * @version V1.0   
 *
 */
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")

public class Alipayapi implements java.io.Serializable {
	
	
	/**商户订单号，商户网站订单系统中唯一订单号，必填*/	
	private String out_trade_no ;
	/**id*/
	private String subject;
	/**id*/
	private String total_fee;
	/**id*/
	private String body;
	
	
	/**
	 * @return the 商户订单号，商户网站订单系统中唯一订单号，必填
	 */
	public String getOut_trade_no() {
		return out_trade_no;
	}
	/**
	 * @param 商户订单号，商户网站订单系统中唯一订单号，必填
	 */
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	/**
	 * @return 订单名称，必填
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param 订单名称，必填
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return 付款金额，必填
	 */
	public String getTotal_fee() {
		return total_fee;
	}
	/**
	 * @param 付款金额，必填
	 */
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	/**
	 * @return 商品描述，可空
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param 商品描述，可空
	 */
	public void setBody(String body) {
		this.body = body;
	}
	
	
	
	
}
