package cn.gov.xnc.admin.copartner.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.UserCopartnerEntity;
import cn.gov.xnc.admin.copartner.service.UserCopartnerServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 商业合作伙伴信息表
 * @author zero
 * @date 2017-01-12 15:06:26
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/userCopartnerController")
public class UserCopartnerController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserCopartnerController.class);

	@Autowired
	private UserCopartnerServiceI userCopartnerService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 商业合作伙伴信息表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView userCopartner(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/userCopartnerList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(UserCopartnerEntity userCopartner,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(UserCopartnerEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userCopartner, request.getParameterMap());
		this.userCopartnerService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除商业合作伙伴信息表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(UserCopartnerEntity userCopartner, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		userCopartner = systemService.getEntity(UserCopartnerEntity.class, userCopartner.getId());
		message = "商业合作伙伴信息表删除成功";
		userCopartnerService.delete(userCopartner);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加商业合作伙伴信息表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(UserCopartnerEntity userCopartner, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userCopartner.getId())) {
			message = "商业合作伙伴信息表更新成功";
			UserCopartnerEntity t = userCopartnerService.get(UserCopartnerEntity.class, userCopartner.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userCopartner, t);
				userCopartnerService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "商业合作伙伴信息表更新失败";
			}
		} else {
			message = "商业合作伙伴信息表添加成功";
			userCopartnerService.save(userCopartner);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 商业合作伙伴信息表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(UserCopartnerEntity userCopartner, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(userCopartner.getId())) {
			userCopartner = userCopartnerService.getEntity(UserCopartnerEntity.class, userCopartner.getId());
			req.setAttribute("userCopartnerPage", userCopartner);
		}
		return new ModelAndView("admin/salesman/userCopartner");
	}
	
	@RequestMapping(value = "order2Copartner")
	public ModelAndView order2Copartner(HttpServletRequest req) {
		String orderids = ResourceUtil.getParameter("ids");
		String ordertype = ResourceUtil.getParameter("ordertype");
		TSUser user = ResourceUtil.getSessionUserName();
		if(null != user && StringUtil.isNotEmpty(user.getId())){
			user = systemService.findUniqueByProperty(TSUser.class, "id", user.getId());
			
			short status = 1;
			CriteriaQuery cq = new CriteriaQuery(TSUser.class);
			cq.eq("company", user.getCompany());
			cq.eq("type", "6");
			cq.eq("status", status);
			cq.add();
			
			List<TSUser> userCopartnerList = systemService.getListByCriteriaQuery(cq, false);
			req.setAttribute("userCopartnerList", userCopartnerList);
		}
		
		req.setAttribute("orderids", orderids);
		req.setAttribute("ordertype", ordertype);
		
		return new ModelAndView("admin/copartner/order2Copartner");
	}
	
	@RequestMapping(value = "choiceCopartnerList")
	public ModelAndView chocieCopartnerList(HttpServletRequest req) {

		return new ModelAndView("admin/copartner/choiceCopartnerList");
	}
	
	@RequestMapping(value = "copartnerList")
	public void copartnerList(TSUser userCopartner, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		short status = 1;
		if(null != user && StringUtil.isNotEmpty(user.getId())){
			TSUser userdb = systemService.findUniqueByProperty(TSUser.class, "id", user.getId());
			TSCompany company = userdb.getCompany();
			CriteriaQuery cq = new CriteriaQuery(TSUser.class,dataGrid);
			userCopartner.setCompany(company);
			userCopartner.setType("6");
			userCopartner.setStatus(status);
			cq.add();
			
			cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userCopartner, request.getParameterMap());
			this.systemService.getDataGridReturn(cq, true);
			
			TagUtil.datagrid(response, dataGrid);
		}	
	}
}
