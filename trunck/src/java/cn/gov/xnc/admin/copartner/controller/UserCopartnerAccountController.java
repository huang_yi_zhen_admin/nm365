package cn.gov.xnc.admin.copartner.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.copartner.entity.UserCopartnerAccountEntity;
import cn.gov.xnc.admin.copartner.service.UserCopartnerAccountServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 商业合作伙伴提现账户
 * @author zero
 * @date 2017-01-18 14:21:53
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/userCopartnerAccountController")
public class UserCopartnerAccountController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserCopartnerAccountController.class);

	@Autowired
	private UserCopartnerAccountServiceI userCopartnerAccountService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 商业合作伙伴提现账户列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView userCopartnerAccount(HttpServletRequest request) {
		return new ModelAndView("admin/copartner/userCopartnerAccountList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(UserCopartnerAccountEntity userCopartnerAccount,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(UserCopartnerAccountEntity.class, dataGrid);
		if("6".equals(user.getType())){
			cq.eq("usercopartnerid", user);
		}
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userCopartnerAccount, request.getParameterMap());
		this.userCopartnerAccountService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除商业合作伙伴提现账户
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(UserCopartnerAccountEntity userCopartnerAccount, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		userCopartnerAccount = systemService.getEntity(UserCopartnerAccountEntity.class, userCopartnerAccount.getId());
		message = "商业合作伙伴提现账户删除成功";
		userCopartnerAccountService.delete(userCopartnerAccount);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加商业合作伙伴提现账户
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(UserCopartnerAccountEntity userCopartnerAccount, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userCopartnerAccount.getId())) {
			message = "商业合作伙伴提现账户更新成功";
			UserCopartnerAccountEntity t = userCopartnerAccountService.get(UserCopartnerAccountEntity.class, userCopartnerAccount.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userCopartnerAccount, t);
				userCopartnerAccountService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "商业合作伙伴提现账户更新失败";
			}
		} else {
			message = "商业合作伙伴提现账户添加成功";
			userCopartnerAccountService.save(userCopartnerAccount);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 商业合作伙伴提现账户列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(UserCopartnerAccountEntity userCopartnerAccount, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(userCopartnerAccount.getId())) {
			userCopartnerAccount = userCopartnerAccountService.getEntity(UserCopartnerAccountEntity.class, userCopartnerAccount.getId());
			req.setAttribute("userCopartnerAccountPage", userCopartnerAccount);
		}
		return new ModelAndView("admin/copartner/userCopartnerAccount");
	}
}
