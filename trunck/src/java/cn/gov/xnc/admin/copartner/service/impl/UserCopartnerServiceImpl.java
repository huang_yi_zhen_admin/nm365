package cn.gov.xnc.admin.copartner.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.copartner.service.UserCopartnerServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("userCopartnerService")
@Transactional
public class UserCopartnerServiceImpl extends CommonServiceImpl implements UserCopartnerServiceI {
	
}