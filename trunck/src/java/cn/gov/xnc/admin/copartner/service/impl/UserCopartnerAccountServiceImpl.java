package cn.gov.xnc.admin.copartner.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.entity.CopartnerWithdrawEntity;
import cn.gov.xnc.admin.copartner.entity.UserCopartnerAccountEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerOrderServiceI;
import cn.gov.xnc.admin.copartner.service.UserCopartnerAccountServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("userCopartnerAccountService")
@Transactional
public class UserCopartnerAccountServiceImpl extends CommonServiceImpl implements UserCopartnerAccountServiceI {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private CopartnerOrderServiceI copartnerOrderService;

	@Override
	public void withdrawCaculater(List<CopartnerOrderEntity> copartnerOrderList, TSUser userCopartner) throws Exception {
		if(null != copartnerOrderList && copartnerOrderList.size() > 0){
			CriteriaQuery cq = new CriteriaQuery(UserCopartnerAccountEntity.class);
			cq.eq("usercopartnerid", userCopartner);
			cq.add();
			
			List<UserCopartnerAccountEntity> copartnerAccountList = this.getListByCriteriaQuery(cq, false);
			
			UserCopartnerAccountEntity copartnerAccount = null;
			if(null != copartnerAccountList && copartnerAccountList.size() >0){
				copartnerAccount = copartnerAccountList.get(0);
				
			}else{
				copartnerAccount = new UserCopartnerAccountEntity();
				copartnerAccount.setId(IdWorker.generateSequenceNo());
				copartnerAccount.setStatus("1");
				copartnerAccount.setFrozenmoney(new BigDecimal(0.00));
				copartnerAccount.setTotalmoney(new BigDecimal(0.00));
				copartnerAccount.setUsablemoney(new BigDecimal(0.00));
				copartnerAccount.setUsercopartnerid(userCopartner);
				//生成结算账户
				save(copartnerAccount);
				
			}
			
			BigDecimal totalWithdraw = new BigDecimal(0.00);
			
			//结算汇总
			for (CopartnerOrderEntity copartnerOrder : copartnerOrderList) {
				
				OrderEntity order = findUniqueByProperty(OrderEntity.class, "id", copartnerOrder.getOrderid().getId());
				totalWithdraw = totalWithdraw.add(order.getPricec());
				
			}
			//计算总价
			totalWithdraw = copartnerAccount.getTotalmoney().add(totalWithdraw);
			copartnerAccount.setTotalmoney(totalWithdraw);
			
			//更新发货商账户
			saveOrUpdate(copartnerAccount);
			batchUpdate(copartnerAccountList);
		}
		
		
	}

	@Override
	public void buildUserCopartnerAccount(TSUser userCopartner) throws Exception {
		
		UserCopartnerAccountEntity copartnerAccount = new UserCopartnerAccountEntity();
		copartnerAccount.setId(IdWorker.generateSequenceNo());
		copartnerAccount.setStatus("1");
		copartnerAccount.setFrozenmoney(new BigDecimal(0.00));
		copartnerAccount.setTotalmoney(new BigDecimal(0.00));
		copartnerAccount.setUsablemoney(new BigDecimal(0.00));
		copartnerAccount.setUsercopartnerid(userCopartner);
		//生成结算账户
		save(copartnerAccount);
		
	}

	@Override
	public List<UserCopartnerAccountEntity> getAllUserCopartnerAccount() throws Exception {
		
		return getList(UserCopartnerAccountEntity.class);
	}

	@Override
	public Long getLastMonthDrawByUser(TSUser userCopartner, String endDateTime) {
		
		String Sql = " SELECT SUM(O.priceC) as usablemoney"
				+ "  FROM xnc_copartner_order copartner_order inner join  xnc_order O  "
				+ "  on O.id = copartner_order.orderid  " 
				+ "  WHERE  copartner_order.userCopartnerId = '" +userCopartner.getId() 
				+ "' and  O.createDate <= '"+ endDateTime 
				+ "' and copartner_order.withdrawStatus='1'" ;
		
		return getCountForJdbc(Sql)	;
	}

	@Override
	public void jobMonthWithDraw() throws Exception {
		try {
    		List<UserCopartnerAccountEntity> userCopartnerAccountList = getAllUserCopartnerAccount();
    		String endtime = DateUtils.getFirstDayOfMonth();
    		endtime = DateUtils.date3Str(DateUtils.getDate(), "yyyy-mm-dd HH:mm:ss");
    		if(null != userCopartnerAccountList && userCopartnerAccountList.size() > 0){
    			
    			for (UserCopartnerAccountEntity userCopartnerAccount : userCopartnerAccountList) {
					
    				TSUser userCopartnerid = userCopartnerAccount.getUsercopartnerid();
    				
    				System.out.println("账户====" + userCopartnerid.getUserName());
    				
    				Long wait2draw = getLastMonthDrawByUser(userCopartnerid, endtime);
    				BigDecimal bwait2draw = new BigDecimal(wait2draw);
    				
    				System.out.println("可提现====" + bwait2draw);
    				if(bwait2draw.compareTo(new BigDecimal(0.00)) > 0){
    					BigDecimal total = userCopartnerAccount.getTotalmoney();
//    					BigDecimal frozenMoney	= total.subtract(bwait2draw);
    					userCopartnerAccount.setUsablemoney(userCopartnerAccount.getUsablemoney().add(bwait2draw));//生成可提现金额
    					
//    					if(frozenMoney.compareTo(new BigDecimal(0.00)) >= 0){
//    						userCopartnerAccount.setFrozenmoney(frozenMoney);//生成冻结金额
//    					}
    				}
    				userCopartnerAccount.setStatus("1");
    				System.out.println("账户总额====" + userCopartnerAccount.getTotalmoney());
    				
    				copartnerOrderService.updateCopartnerOrderAfterDrawCal(userCopartnerid, endtime);
    				
				}
    			
    			batchUpdate(userCopartnerAccountList);
    			System.out.println("定时任务结束====");
    		}
		} catch (Exception e) {
			logger.error(">>>>>>JobCopartnerWithdrawCaculate : " + e);
		}
		
	}
	
}