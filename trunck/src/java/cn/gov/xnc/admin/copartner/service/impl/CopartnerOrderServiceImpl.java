package cn.gov.xnc.admin.copartner.service.impl;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.copartner.entity.CopartnerOrderEntity;
import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.service.CopartnerOrderServiceI;
import cn.gov.xnc.admin.copartner.service.OrderSendServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.LogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("copartnerOrderService")
@Transactional
public class CopartnerOrderServiceImpl extends CommonServiceImpl implements CopartnerOrderServiceI {

	@Autowired
	private OrderSendServiceI orderSendService;
	
	@Override
	public CopartnerOrderEntity buildCopartnerOrder(OrderEntity order) {
		
		CriteriaQuery cq = new CriteriaQuery(OrderSendEntity.class);
		cq.eq("orderid", order);
		cq.eq("status", "1");//选择处在派单环节的订单
		cq.add();
		
		List<OrderSendEntity> OrderSendList = this.getListByCriteriaQuery(cq, false);
		if(null != OrderSendList && OrderSendList.size() > 0){
			OrderSendEntity orderSend =  OrderSendList.get(0);
			CopartnerOrderEntity copartnerOrder = new CopartnerOrderEntity();
			copartnerOrder.setId(IdWorker.generateSequenceNo());
			copartnerOrder.setcopartnerid(orderSend.getReceiver());
			copartnerOrder.setOrderid(order);
			copartnerOrder.setWithdraworderid(null);
			copartnerOrder.setWithdrawstatus("1");
			
			return copartnerOrder;
		}else{
			
			return null;
		}
	}

	@Override
	public List<CopartnerOrderEntity> getCopartnerOrdersByUser(TSUser userCopartner, String endDateTime) {
		//时间
		SimpleDateFormat dateformate = new SimpleDateFormat ("yyyy-MM-dd"); 
		CriteriaQuery cq = new CriteriaQuery(CopartnerOrderEntity.class);
		cq.eq("usercopartnerid", userCopartner);
		cq.eq("withdrawstatus", "1");//选择处在派单环节的订单
		cq.le("orderid.createdate", DateUtils.str2Date(endDateTime,dateformate));
		cq.add();
		
		List<CopartnerOrderEntity> CopartnerOrderList = this.getListByCriteriaQuery(cq, false);
		return CopartnerOrderList;
	}

	public int updateCopartnerOrderAfterDrawCal(TSUser userCopartner, String endDateTime) throws Exception {
		String Sql =" UPDATE xnc_copartner_order "
				+ " SET xnc_copartner_order.withdrawStatus = '2' "
				+ " WHERE  EXISTS( select 1 from xnc_order O where O.id = xnc_copartner_order.orderid and O.createDate <= '"+ endDateTime + "') "
				+ " and xnc_copartner_order.withdrawStatus = '1' " 
				+ " and xnc_copartner_order.userCopartnerId = '" +  userCopartner.getId() + "'";
		
		return updateBySqlString(Sql);
	}

	public int updateCopartnerOrder2withdraw(TSUser userCopartner,String withdrawId) throws Exception {
		
		String Sql =" UPDATE xnc_copartner_order"
				+ " SET xnc_copartner_order.withdrawOrderId = '" + withdrawId + "'"
				+ " WHERE xnc_copartner_order.withdrawStatus = '2' " 
				+ " and xnc_copartner_order.userCopartnerId = '" +  userCopartner.getId() + "'";
		
		return updateBySqlString(Sql);
	}

	@Override
	public TSUser getOrderCopartnerByOrderId(String orderid) throws Exception {
		TSUser copartner = null;
		//时间
		CriteriaQuery cq = new CriteriaQuery(CopartnerOrderEntity.class);
		cq.eq("orderid.id", orderid);
		cq.add();
		
		List<CopartnerOrderEntity> copartnerOrderList = this.getListByCriteriaQuery(cq, false);
		if(null != copartnerOrderList && copartnerOrderList.size() > 0){
			copartner = copartnerOrderList.get(0).getcopartnerid();
		}
		return copartner;
	}

	@Override
	public void saveCopartnerOrder(OrderEntity order) {
		TSUser user = ResourceUtil.getSessionUserName();
		if("6".equals(user.getType()) ){//发货商
			//订单进入打包发货状态，将订单归发货商，备之后统计用
			if(OrderServiceI.FreightState.ALREADY_DELIVER.getValue().equals(order.getFreightState().getDictionaryValue())
					|| OrderServiceI.FreightState.PART_DELIVER.getValue().equals(order.getFreightState().getDictionaryValue())
					|| OrderServiceI.FreightState.WAIT_PACK.getValue().equals(order.getFreightState().getDictionaryValue())){
				
				try {
					CopartnerOrderEntity copartnerOrder = getCopartnerOrderByOrderId(order.getId());
					if(null == copartnerOrder){
						save(buildCopartnerOrder(order));
					}
					
					OrderSendEntity orderSend = orderSendService.getSendOrder(order);
					if(null != orderSend){
						orderSend.setStatus("2");//订单被处理更新处理状态
						updateEntitie(orderSend);
					}
				} catch (Exception e) {
					LogUtil.error(">>>>>>>>>>saveCopartnerOrder : " + e.getMessage());
				}
				
			}
			
		}
	}

	@Override
	public CopartnerOrderEntity getCopartnerOrderByOrderId(String orderid) {
		CriteriaQuery cq = new CriteriaQuery(CopartnerOrderEntity.class);
		//时间
		cq.eq("orderid.id", orderid);
		cq.add();
		
		List<CopartnerOrderEntity> copartnerOrderList = this.getListByCriteriaQuery(cq, false);
		if(null != copartnerOrderList && copartnerOrderList.size() > 0){
			return copartnerOrderList.get(0);
		}
		return null;
	}

}