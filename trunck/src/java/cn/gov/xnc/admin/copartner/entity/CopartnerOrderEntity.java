package cn.gov.xnc.admin.copartner.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 合作伙伴相关订单
 * @author zero
 * @date 2017-01-12 15:08:48
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_copartner_order", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CopartnerOrderEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**合作伙伴唯一识别码*/
	private TSUser usercopartnerid;
	/**订单唯一识别吗*/
	private OrderEntity orderid;
	/**结算申请单唯一编码*/
	private CopartnerWithdrawEntity withdraworderid;
	/**订单是否结算 1 否 2 是*/
	private java.lang.String withdrawstatus;
	/**创建时间*/
	private java.util.Date createtime;
	/**修改时间*/
	private java.util.Date updatetime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合作伙伴唯一识别码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERCOPARTNERID")
	public TSUser getcopartnerid(){
		return this.usercopartnerid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合作伙伴唯一识别码
	 */
	public void setcopartnerid(TSUser usercopartnerid){
		this.usercopartnerid = usercopartnerid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单唯一识别吗
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDERID")
	public OrderEntity getOrderid(){
		return this.orderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单唯一识别吗
	 */
	public void setOrderid(OrderEntity orderid){
		this.orderid = orderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算申请单唯一编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "WITHDRAWORDERID")
	public CopartnerWithdrawEntity getWithdraworderid(){
		return this.withdraworderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结算申请单唯一编码
	 */
	public void setWithdraworderid(CopartnerWithdrawEntity withdraworderid){
		this.withdraworderid = withdraworderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单锁 1 未结算 2 正在结算 3 结算完结(当订单要结算的时候就会锁住，标志哪些订单正在结算)
	 */
	@Column(name ="WITHDRAWSTATUS",nullable=false,length=2)
	public java.lang.String getWithdrawstatus(){
		return this.withdrawstatus;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单锁 1 未结算 2 正在结算 3 结算完结(当订单要结算的时候就会锁住，标志哪些订单正在结算)
	 */
	public void setWithdrawstatus(java.lang.String withdrawstatus){
		this.withdrawstatus = withdrawstatus;
	}
	
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}
	
	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="UPDATETIME",nullable=true)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
}
