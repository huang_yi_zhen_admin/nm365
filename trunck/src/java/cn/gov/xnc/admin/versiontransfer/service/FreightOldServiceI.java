package cn.gov.xnc.admin.versiontransfer.service;

import java.util.List;

import cn.gov.xnc.admin.freight.entity.FreightDetailsEntity;
import cn.gov.xnc.admin.freight.entity.FreightEntity;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.admin.versiontransfer.enity.FreightOldEntity;

public interface FreightOldServiceI extends CommonService{

	public List<FreightDetailsEntity> buildFreightDetail(final FreightEntity freightEntity, FreightOldEntity freightOldEntity);
}
