package cn.gov.xnc.admin.pay.util;

import java.util.List;
import java.util.Map;

import cn.gov.xnc.system.core.common.model.json.DataGrid;

public class DataHandle {
	public static String DataConstruction(String[] field, DataGrid dataGrid){
		StringBuilder sbd = new StringBuilder();
		StringBuilder data = new StringBuilder();
		StringBuilder rows = new StringBuilder();
		List<Map<String, Object>> list;
		String msg = "获取数据成功";
		Integer success = 1;
		int i = 0;
		
		list = dataGrid.getResults();
		
		rows.append("[");
		if(list!=null && list.size()>0){
			for(Map<String, Object> m : list){
				if(i>0){
					rows.append(",");
				}
				rows.append("[");
				int j = 0;
				for(String s : field){
					if(j>0){
						rows.append(",");
					}
					if(m.get(s) == null || m.get(s).toString().equals("")){
						rows.append("\"\"");
					}else{
						rows.append("\"" + m.get(s) + "\"");
					}
					j++;
				}
				rows.append("]");
				i++;
			}
		}
		rows.append("]");
		data.append("\"page\"");
		data.append(":");
		data.append("\"" + dataGrid.getPageNum() +"\"");
		data.append(",");
		data.append("\"total\"");
		data.append(":");
		data.append("\"" + dataGrid.getTotal() +"\"");
		data.append(",");
		data.append("\"rows\"");
		data.append(":");
		data.append(rows);
		
		sbd.append("{");
		sbd.append("\"msg\"");
		sbd.append(":");
		sbd.append("\"" + msg +"\"");
		sbd.append(",");
		sbd.append("\"success\"");
		sbd.append(":");
		sbd.append(success);
		sbd.append(",");
		sbd.append("\"data\"");
		sbd.append(":");
		sbd.append("{" + data +"}");
		sbd.append("}"); 
		return sbd.toString();
	}
}
