package cn.gov.xnc.admin.message.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.message.dao.SmsSendDAO;
import cn.gov.xnc.admin.message.entity.SmsSendEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("smsSendService")
public class SmsSendServiceImpl {
	@Resource(name="smsSendDAO")
	private SmsSendDAO smsSendDAO;
	
	/**
	 * 保存
	 * @param type:消息类型
	 * @param content：短信内容
	 * @param sendMobile：发送者手机
	 * @param receiveMobile：接收者手机
	 * @param companyId：企业ID
	 * */
	public Serializable smsSendSave(String type, String content, String sendMobile, String receiveMobile, String companyId){
		TSUser user = ResourceUtil.getSessionUserName();
		SmsSendEntity smsSend = new SmsSendEntity();
		smsSend.setType(type);
		smsSend.setContent(content);
		smsSend.setSendMobile(sendMobile);
		smsSend.setReceiveMobile(receiveMobile);
		if(companyId != null && !companyId.equals("")){
			smsSend.setCompanyId(companyId);
		}else{
			smsSend.setCompanyId(user.getCompany().getId());
		}
		return smsSendDAO.smsSendSave(smsSend);
	}
	
	/**
	 * 保存，多个接收者手机
	 * @param type:消息类型
	 * @param content：短信内容
	 * @param sendMobile：发送者手机
	 * @param receiveMobile：接收者手机
	 * */
	public void smsSendSave(String type, String content, String sendMobile, List<String> receiveMobile){
		SmsSendEntity smsSend = new SmsSendEntity();
		smsSend.setType(type);
		smsSend.setContent(content);
		smsSend.setSendMobile(sendMobile);
		for(String rm : receiveMobile){
			smsSend.setReceiveMobile(rm);
			smsSendDAO.smsSendSave(smsSend);
		}
	}
	
	/**
	 * 更新
	 * */
	public Integer smsSendUpdateType(SmsSendEntity smsSend){
		return smsSendDAO.smsSendUpdateType(smsSend);
	}
	
	/**
	 * 查询，单条数据
	 * */
	public SmsSendEntity getSmsSendData(SmsSendEntity smsSend){
		return smsSendDAO.getSmsSendData(smsSend);
	}
	/**
	 * 列表
	 * */
	public DataGrid getSmsSendListPage(DataGrid dataGrid, SmsSendEntity smsSend){
		dataGrid = smsSendDAO.getSmsSendListPage(dataGrid, smsSend);
		return dataGrid;
	}
}
