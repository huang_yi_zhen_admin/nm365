package cn.gov.xnc.admin.message.service;

public enum SmsTypeEnum {
	
	OPEN_CLIENT_ACCOUNT("开通客户帐号","1"), 
	PAID_NOTICE_ORDER_MANAGER("付款通知订单员","2"), 
	PAID_NOTICE_ORDER_SALEMAN("付款通知业务员","3"),
	PAID_NOTICE_ORDER_CLIENT("付款通知客户","4"), 
	DELIVER_NOTICE_ORDER_RECIEVER("发货通知收货人","5"), 
	DELIVER_NOTICE_ORDER_CLIENT("发货通知客户","6"),
	REFUND_NOTICE_FINANCE_MANAGER("退款通知财务","7"), 
	REFUND_NOTICE_ORDER_CLIENT("退款通知客户","8"),
	OPEN_COPARTNER_ACCOUNT("开通客户帐号","9"), 
	BUSINESS_SHARE_FEEBACK("招商分享回馈信息","10"), 
	BUSINESS_SHARE_FEEBACK_REPLY("招商分享信息答复","11");
	
	
	private String name;
	private String value;

	private SmsTypeEnum(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String index) {
		this.value = index;
	}
	
	public static String getNameByVal(String val){
		String valName = null;
		for (SmsTypeEnum smsType : SmsTypeEnum.values()) {
			if(smsType.getValue().equals(val)){
				valName = smsType.getName();
				break;
			}
		}
		return valName;
	}
}
