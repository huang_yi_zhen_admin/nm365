/**
 * 
 */
package cn.gov.xnc.admin.message.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * @Title: Entity
 * @Description: 用户站内消息记录
 * @author jason
 *
 */
@Entity
@Table(name = "xnc_notify", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class NotifyEntity implements java.io.Serializable{
	private java.lang.String id;
	private java.lang.String title;//标题
	private java.lang.String notify_text_id;//站内消息内容id
	private java.lang.String sender_id;//发送者id
	private java.lang.String receiver_id;//接收者id
	private java.lang.String userid;//两者ID
	private java.lang.Byte status;//1:已读;0;未读
	private java.util.Date readtime;//读取日期
	private java.util.Date createtime;//创建日期
	private java.lang.Byte notifytype;//消息类型,1:Message;2:Remind;3:announce
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
	@Column(name ="title",nullable=true,length=32)
	public java.lang.String getTitle() {
		return title;
	}
	public void setTitle(java.lang.String title) {
		this.title = title;
	}
	@Column(name ="notify_text_id",nullable=false,length=32)
	public java.lang.String getNotify_text_id() {
		return notify_text_id;
	}
	public void setNotify_text_id(java.lang.String notify_text_id) {
		this.notify_text_id = notify_text_id;
	}
	@Column(name ="sender_id",nullable=false,length=32)
	public java.lang.String getSender_id() {
		return sender_id;
	}
	public void setSender_id(java.lang.String sender_id) {
		this.sender_id = sender_id;
	}
	@Column(name ="receiver_id",nullable=false,length=32)
	public java.lang.String getReceiver_id() {
		return receiver_id;
	}
	public void setReceiver_id(java.lang.String receiver_id) {
		this.receiver_id = receiver_id;
	}
	
	public java.lang.String getUserid() {
		return userid;
	}
	public void setUserid(java.lang.String userid) {
		this.userid = userid;
	}
	@Column(name ="status",nullable=true,length=4)
	public java.lang.Byte getStatus() {
		return status;
	}
	public void setStatus(java.lang.Byte status) {
		this.status = status;
	}
	@Column(name ="readtime",nullable=true)
	public java.util.Date getReadtime() {
		return readtime;
	}
	public void setReadtime(java.util.Date readtime) {
		this.readtime = readtime;
	}
	@Column(name ="createtime",nullable=false)
	public java.util.Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(java.util.Date createtime) {
		this.createtime = createtime;
	}
	@Column(name ="notifytype",nullable=true,length=4)
	public java.lang.Byte getNotifytype() {
		return notifytype;
	}
	public void setNotifytype(java.lang.Byte notifytype) {
		this.notifytype = notifytype;
	}
	
}
