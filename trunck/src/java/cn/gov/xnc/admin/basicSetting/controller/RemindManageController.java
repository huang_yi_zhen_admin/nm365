package cn.gov.xnc.admin.basicSetting.controller;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.service.ProductServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserService;



/**
 * @Title: Controller
 * @Description: 提醒管理
 * @author huangxy
 * @date 2015-04-28 11:48:37
 * @version V1.0
 * 
 */
@Scope("prototype")
@Controller
@RequestMapping("/remindManageController")
public class RemindManageController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RemindManageController.class);

	@Autowired
	private SystemService systemService;

	@Autowired
	private ProductServiceI productService;
	@Autowired
	private UserService userService;

	private String message;
	
	@Autowired
	private OrderServiceI orderServiceI;
	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 提醒页面
	 * @return
	 */
	@RequestMapping(value = "/home" )
	public ModelAndView home(HttpServletRequest request) {	
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		//读取公司的销售发货数据，首页展示
		Map<String, OrderEntity> salesOPMap =orderServiceI.salesOPMap(user.getCompany().getId());
		
		OrderEntity  o1 = (OrderEntity)salesOPMap.get("o1");//待付款
		OrderEntity  o2 = (OrderEntity)salesOPMap.get("o2");//待发货数
		OrderEntity  o3 = (OrderEntity)salesOPMap.get("o3");//待审核

		OrderEntity  o4 = (OrderEntity)salesOPMap.get("o4");//今日订单
		OrderEntity  o5 = (OrderEntity)salesOPMap.get("o5");//今日发货
		OrderEntity  o6 = (OrderEntity)salesOPMap.get("o6");//今日退单
		
		
		
		
		int s1 =0 ;
		try{
			s1 = o1.getNumber();
		}catch (Exception e) {
		}
		int s2 =0 ;
		try{
			s2 = o2.getNumber();
		}catch (Exception e) {
		}
		int s3 =0 ;
		try{
			s3 = o3.getNumber();
		}catch (Exception e) {
		}
		
		int s4 =0 ;
		try{
			s4 = o4.getNumber();
		}catch (Exception e) {
		}
		
		int s5 =0 ;
		try{
			s5 = o5.getNumber();
		}catch (Exception e) {
		}
		
		
		int s6 =0 ;
		try{
			s6 = o6.getNumber();
		}catch (Exception e) {
		}
		
		
		request.setAttribute("s1", s1);//今天发货数
		request.setAttribute("s2", s2);//待发货数
		request.setAttribute("s3", s3);	//今日下单数
		
		
		request.setAttribute("s4", s4);	//待审订单
		request.setAttribute("s5", s5);	//待审订单
		request.setAttribute("s6", s6);	//待审核用户
		//输出7天销售统计
		return new ModelAndView("main/home");

	}
	
	
	
	/**
	 * 添加月销售数据统计查询
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "getic")
	@ResponseBody
	public AjaxJson getic( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		DateUtils dateUtils = new DateUtils();
		message = "成功";
		String productid = "";
		String userCLid ="";
		String dateTime = DateUtils.getDate("yyyy-MM-dd");
		String sumtype ="1";
		String startTime= dateUtils.getBeforeNTime("yyyy-MM-dd",6) +" 00:00:00";
		String endTime = DateUtils.getDate("yyyy-MM-dd") +" 23:59:59";
		
		
		//String startTime= "2016-10-30 00:00:00";
		//String endTime = "2016-11-05  23:59:59";
		
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		String companyId = user.getCompany().getId();
		
		List<OrderEntity> salesOrderList  = orderServiceI.salesTIC( userCLid , productid,  startTime, endTime ,companyId  );
		Map<String, OrderEntity> mapList = new HashMap<String, OrderEntity>();
		BigDecimal pic= new BigDecimal("0");
		BigDecimal picNUM= new BigDecimal("0");//计算总额
		int numberNUM= 0;//计算总算
		int number =0;
		if(salesOrderList != null && salesOrderList.size() > 0){
			for( OrderEntity o:salesOrderList ){
				picNUM =picNUM.add(o.getPrice());
				numberNUM = numberNUM+o.getNumber();
				if( o.getPrice().compareTo(pic) ==1 && "1".equals(sumtype)){//大于
					pic = o.getPrice();
				}
				if( number  < o.getNumber() && "2".equals(sumtype) ){//大于
					number = o.getNumber();
				}
				mapList.put(DateUtils.date3Str(o.getCreatedate(), "yyyy-MM-dd"),o );
			}
		}else {
			j.setSuccess(false);
			return j;
		}
		String num=String.valueOf(numberNUM);
		if("1".equals(sumtype)) {
			number = pic.intValue();
			num = picNUM.toString();
		}
		String numberA =String.valueOf(number);
		
		if( Integer.parseInt(numberA.substring(0,1)) <5 ){
			numberA ="1"+numberA.replaceAll("[0-9]","0"); 
			numberA =numberA.replace("10", "5");
		}else {
			numberA ="1"+numberA.replaceAll("[0-9]","0");	  
		}
		 number = Integer.parseInt(numberA);	  	   
		
		String data ="[";//数据
		String title="";//标题
		int end_scale= number ;//Y轴最大值
		int scale_space=(int) Math.ceil(end_scale/10);//Y轴递增值
		String sumtypeName ="元";//设置解析值轴文本
		String  labels = "[";
		String  flow = "[";
		if( StringUtil.isNotEmpty(userCLid)){//图像标题的名称
			TSUser t = systemService.findUniqueByProperty (TSUser.class,"id" ,userCLid);
			title +=t.getRealname();
		}
		
		if( StringUtil.isNotEmpty(productid)){
			ProductEntity p = systemService.findUniqueByProperty (ProductEntity.class,"id" ,productid);
			title +=p.getName();
		}
		//从map中按照日期key获取对应的数据
		   try {
			    Calendar startCalendar = Calendar.getInstance();
		        Calendar endCalendar = Calendar.getInstance();
		        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		        Date startDate = df.parse(startTime);
		        startCalendar.setTime(startDate);
		        Date endDate = df.parse(endTime);
		        endCalendar.setTime(endDate);
		        
		        SimpleDateFormat dfm = new SimpleDateFormat("MM-dd");
		        
		        OrderEntity o = mapList.get(df.format(startCalendar.getTime()));
    			String value ="0";
    			if( o != null && o.getCreatedate() != null){
    				if("1".equals(sumtype)){
    					value=o.getPrice().toString();
    					
    				}else{
    					value=o.getNumber().toString();
    					
    				}
    			}
    			//data +="{name : '"+sumtypeName+"',value : "+value+",color:'#4572a7',line_width:2 }," ;
    			 
    			flow += value+",";
    			//data +="{name : '"+sumtypeName+"',value : "+value+",color:'#4572a7',line_width:2 }," ;
    			labels +="\""+dfm.format(startCalendar.getTime())+"\",";
    			
		        
		        while(true){
		            startCalendar.add(Calendar.DAY_OF_MONTH, 1);
		            if(startCalendar.getTimeInMillis() < endCalendar.getTimeInMillis()){
		            //System.out.println(df.format(startCalendar.getTime()));
		            //System.out.println(dfm.format(startCalendar.getTime()));
		            	//OrderEntity o = mapList.get(df.format(startCalendar.getTime()));
		    			//String value ="0";
		            	o = mapList.get(df.format(startCalendar.getTime()));
		            	value ="0";
		            	if( o != null && o.getCreatedate() != null){
		    				if("1".equals(sumtype)){
		    					value=o.getPrice().toString();
		    					
		    				}else{
		    					value=o.getNumber().toString();
		    					
		    				}
		    			}
		            	flow += value+",";
		            	//data +="{name : '"+sumtypeName+"',value : "+value+",color:'#4572a7',line_width:2 }," ;
		    			labels +="\""+dfm.format(startCalendar.getTime())+"\",";
		        }else{
		            break;
		        }
		     }
		        
		        o = mapList.get(df.format(endCalendar.getTime()));
	    		value ="0";
	    			if( o != null && o.getCreatedate() != null){
	    				if("1".equals(sumtype)){
	    					value=o.getPrice().toString();
	    					
	    				}else{
	    					value=o.getNumber().toString();
	    				
	    				}
	    			}
	    		flow += value+"]";	
	    		data +="{name : '"+sumtypeName+"',value : "+flow+",color:'#4572a7',line_width:2 }," ;
	    		
	    		labels +="\""+dfm.format(endCalendar.getTime())+"\"]";	
	    		
		        
			} catch (Exception e) {
				// TODO: handle exception
			}
		 	
	  

		//去除最后的标点
		data = data.substring(0,data.length()-1);
		data +="]";
		
		title +=dateTime+"7天销售情况:"+"("+num+""+sumtypeName+")";

		Map<String, Object> attributes = new HashMap<String, Object>();
		
		System.out.println("data="+data);
		System.out.println("labels="+labels);
		
		attributes.put("data", data);
		attributes.put("title", "");
		attributes.put("labels", labels);
		attributes.put("end_scale", end_scale);
		attributes.put("scale_space", scale_space);
		attributes.put("sumtypeName", "元");
		j.setAttributes(attributes);
		j.setMsg(message);
		
		
		return j;
	}
	
	/**
	 * 渠道销售情况
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "getqdxs")
	@ResponseBody
	public AjaxJson getqdxs( HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "成功";

		String productid ="";
		//String startTime =request.getParameter("dateTime");
		//String endTime =request.getParameter("dateTime2");
		DateUtils dateUtils = new DateUtils();
		String startTime= dateUtils.getBeforeNTime("yyyy-MM-dd",6) +" 00:00:00";
		String endTime = DateUtils.getDate("yyyy-MM-dd") +" 23:59:59";
		
		String sumtype ="1";
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		String companyId = user.getCompany().getId();
		
		List<OrderEntity> salesOrderList  = orderServiceI.salesQDXS( companyId ,productid ,  startTime, endTime );
		//Map<String, OrderEntity> mapList = new HashMap<String, OrderEntity>();
		BigDecimal pic= new BigDecimal("0");
		int number =0;
		BigDecimal picNUM= new BigDecimal("0");//计算总额
		int numberNUM= 0;//计算总算
		String data ="[";//数据
		
		String sumtypeName ="元";//设置解析值轴文本
		
		//按金额排序  只获取前7位
		
		
		
		
		if(salesOrderList != null && salesOrderList.size() > 0){
			int k =salesOrderList.size();
			if(k >=7 ){
				k =7 ;
			}
			for( int i=0 ; i< k ;i++ ){
				 OrderEntity o =salesOrderList.get(i);
				picNUM =picNUM.add(o.getPrice());
				numberNUM = numberNUM+o.getNumber();
				if( o.getPrice().compareTo(pic) ==1 && "1".equals(sumtype)){//大于
					pic = o.getPrice();
				}
				if( number  < o.getNumber() && "2".equals(sumtype) ){//大于
					number = o.getNumber();
				}
				String value ="0";
				if("1".equals(sumtype)){
						value=o.getPrice().toString();
						
				}else{
						value=o.getNumber().toString();
						
				}
				TSUser t = new TSUser();
				if( StringUtil.isNotEmpty(o.getId())){
					t = systemService.findUniqueByProperty (TSUser.class,"id" ,o.getId());
			    }
				data +="{name : '"+t.getRealname()+"',value : "+value+",color:'#4572a7'}," ;
				//mapList.put(DateUtils.date3Str(o.getCreatedate(), "yyyy-MM-dd"),o );
			}
			//去除最后的标点
			data = data.substring(0,data.length()-1);
			data +="]";
		}else {
			j.setSuccess(false);
			return j;
		}

		String num=String.valueOf(numberNUM);
		if("1".equals(sumtype)) {
			number = pic.intValue();
			num = picNUM.toString();
		}

		String numberA =String.valueOf(number);
		
		if( Integer.parseInt(numberA.substring(0,1)) <5 ){
			numberA ="1"+numberA.replaceAll("[0-9]","0"); 
			numberA =numberA.replace("10", "5");
		}else {
			numberA ="1"+numberA.replaceAll("[0-9]","0");	  
		}
		 number = Integer.parseInt(numberA);	  	   
		
		
		 String title="";//标题
		 int end_scale= number ;//Y轴最大值
		 int scale_space=(int) Math.ceil(end_scale/10);//Y轴递增值
		
			
		 if( StringUtil.isNotEmpty(productid)){
			 ProductEntity p = systemService.findUniqueByProperty (ProductEntity.class,"id" ,productid);
				title +=p.getName();
		 }
		title +=startTime+"~"+endTime+"采购汇总:"+"("+num+""+sumtypeName+")";
			
			Map<String, Object> attributes = new HashMap<String, Object>();
			attributes.put("data", data);
			attributes.put("title", "");
			attributes.put("end_scale", end_scale);
			attributes.put("scale_space", scale_space);
			attributes.put("sumtypeName", sumtypeName);
			j.setAttributes(attributes);
			j.setMsg(message);
		return j;
	}
	
	
	
}
