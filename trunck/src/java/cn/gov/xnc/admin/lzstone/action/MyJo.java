package cn.gov.xnc.admin.lzstone.action;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.gov.xnc.admin.message.entity.MessageTemplateEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;







/**
 * 
 *定时任务执行类 
 **//**   
 * @Title: Controller
 * @Description: 导入导出excel控制类
 * @author ljz
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/MyJo")
public class MyJo {
	
	@Autowired
	private SystemService systemService;
	
    public void work()
    {	
    	
    }


}
