package cn.gov.xnc.admin.order.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsFlowEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.service.ProductEnumMap;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockIOTypeServiceI;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.ComponentService;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("payBillsOrderService")
@Transactional(rollbackFor=Exception.class)
public class PayBillsOrderServiceImpl extends CommonServiceImpl implements PayBillsOrderServiceI {
	
	private Logger logger = Logger.getLogger(PayBillsOrderServiceImpl.class);
	@Autowired
	private SystemService systemService;
	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private StockProductPriceServiceI stockProductPriceService;
	@Autowired
	private StockIODetailServiceI stockIODetailService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private OrderServiceI orderService;
	
	
	/**
	 *创建一个充值类型的流水订单 
	 * 
	 **/
	public PayBillsOrderEntity  payBillsOrderUser(  PayBillsOrderEntity payBillsOrder  ){
		
		
		
		
		
		
		return null;
		
	}

	@Override
	public StatisPageVO statisUserPaybills(TSCompany company, TSUser user,HttpServletRequest request) {
		String type = request.getParameter("type");
		String ordertype = request.getParameter("ordertype");//支付单类型
		String identifier = request.getParameter("identifier");
		String clientid = request.getParameter("clientid.id");
//		String state = request.getParameter("state");
		String paymentmethod = request.getParameter("paymentmethod");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(a.alreadyPaidMoney), 0) value1, IFNULL(SUM(a.payableMoney), 0) value2, IFNULL(SUM(a.obligationsMoney), 0) value3, count(1) value4 FROM xnc_pay_bills_order a ");
		Sql.append("WHERE a.company = '" + company.getId() + "'");
		
		if(StringUtil.isNotEmpty(type) && StringUtil.isNotEmpty(type.trim())){
			Sql.append(" AND a.type = '" + type + "'");
		}
		if(StringUtil.isNotEmpty(ordertype) 
				&& ordertype.indexOf(",") > -1){//应付
			
			Sql.append(" AND a.state in (" + ordertype + ") ");
			
		}else if(StringUtil.isNotEmpty(ordertype) 
				&& ordertype.indexOf(",") == -1){
			
			Sql.append(" AND a.state ='"+ ordertype + "'");
		}
		
		if(StringUtil.isNotEmpty(identifier) && StringUtil.isNotEmpty(identifier.trim())){
			Sql.append(" AND a.identifier like '%" + identifier.trim() + "%'");
		}
//		if(StringUtil.isNotEmpty(state) && StringUtil.isNotEmpty(state.trim())){
//			Sql.append("AND a.state = '" + state.trim() + "'");
//		}
		if(null != user && StringUtil.isNotEmpty(user.getId())){
			Sql.append("AND a.clientId = '" + user.getId() + "'");
		}
		if(StringUtil.isNotEmpty(clientid) && StringUtil.isNotEmpty(clientid.trim())){
			Sql.append("AND a.clientId = '" + clientid.trim() + "'");
		}
		if(StringUtil.isNotEmpty(paymentmethod) && StringUtil.isNotEmpty(paymentmethod.trim())){
			Sql.append("AND a.PaymentMethod = '" + paymentmethod.trim() + "'");
		}
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append("AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append("AND a.createDate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append("AND a.createDate <= '" + createdate2 + "'" );
		}

		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		
		return statisPageVO;
	}
	
	
	@Override
	public List<PayBillsOrderEntity> getBillsOrderListByIds(String billsIds) {
		List<PayBillsOrderEntity> billsorderlist = new ArrayList<PayBillsOrderEntity>();
		
		if(StringUtil.isNotEmpty(billsIds)){
			String[] billsIdList = billsIds.split(",");
			
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
			cq.setCurPage(1);
			cq.setPageSize(30);
			cq.in("id", billsIdList);
			cq.add();
			
			billsorderlist = getListByCriteriaQuery(cq, false);
		}
		
		
		return billsorderlist;
	}
	
	/**
	 * 先货后款审核
	 * @param payBillsOrder
	 * @param request
	 * @return
	 */
	@Override
	public AjaxJson sendFirstPaymentAudit(PayBillsOrderEntity payBillsOrder, HttpServletRequest request) {
		
		boolean success = true;
		int orderFailSize = 0;
		String letitgo = request.getParameter("letItGo");
		
		String msg = "审核通过";
		if("0".equals(letitgo)){
			msg = "取消先货后款成功";
		}
		
		AjaxJson json = new AjaxJson();
		//只有客户选择了先货后款的订单，才可以使用先货后款审核功能
		if(null != payBillsOrder 
				&& StringUtil.isNotEmpty(payBillsOrder.getId()) 
				&& PayBillsOrderServiceI.State.UNPAID.getValue().equals(payBillsOrder.getState())){//支付状态未支付
			
			try {
				PayBillsOrderEntity payBillsOrderEntity = findUniqueByProperty(PayBillsOrderEntity.class,"id",payBillsOrder.getId());
				//取消订单的先货后款状态
				if("0".equals(letitgo) && success){
					payBillsOrderEntity.setPaymentmethod(null);//清除先货后款支付方式
					payBillsOrderEntity.setTradeType(PayBillsOrderServiceI.TradeType.PAYFIRST.getValue());//设定交易方式为到款发货
					systemService.updateEntitie(payBillsOrderEntity);
					
				}else{
					json = payBillsOrderStockOut(payBillsOrderEntity, request);//判断库存是否充足
					List<OrderEntity> orderlist = payBillsOrderEntity.getOrderS();
					
					if(json.isSuccess() && 
							null != orderlist && orderlist.size() > 0){
						
						for (OrderEntity orderEntity : orderlist) {
							
							String state = orderEntity.getState();
							if("1".equals(letitgo) && OrderServiceI.State.UNPAID.getValue().equals( state )){//只有未付款状态的单子可以先货后款
								orderEntity.setState(OrderServiceI.State.WAIT2RECEIPT.getValue());
								
							}
							else if("1".equals(letitgo) 
									&& !OrderServiceI.State.UNPAID.getValue().equals( state )
									&& !OrderServiceI.State.CANCEL.getValue().equals( state )){
								
								msg = "不符合先货后款状态，" + msg + "失败！订单号：" + orderEntity.getId();
								systemService.addLog(msg, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
								orderFailSize++;
								break;
							}
						}
						//更新订单状态
						if(orderFailSize == 0){
							batchUpdate(orderlist);
							msg = "审核通过！";
							success = true;
							//更新订单交易方式为 先货后款
							payBillsOrderEntity.setTradeType(PayBillsOrderServiceI.TradeType.GETFIRST.getValue());//设置交易方式：先货后款
							payBillsOrderEntity.setPaymentmethod(PayBillsServiceI.PaymentMethod.SENDFIRST.getValue());//设置付款方式：先货后款
							updateEntitie(payBillsOrderEntity);
							
						}else{
							success = false;
							msg = "审核失败！抱歉~您有部分单子不符合先货后款状态，请查看订单详细！";
						}
					}else{
						success = false;
						msg = "审核失败，订单库存不足！";
					}
				}
			} catch (Exception e) {
				msg= "先货后款通过审核失败，请联系系统管理人员";
				success = false;
				logger.error("------------" + msg + e);
			}
			
		}else{
			msg= "先货后款通过审核失败~订单信息获取失败【或者】不是现货后款订单！";
			success = false;
			systemService.addLog(msg, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			logger.error("------------" + msg);
		}
		json.setMsg(msg);
		json.setSuccess(success);
		return json;
	}
	
	/**
	 * 获取大单物流状态
	 */
	@Override
	public TSDictionary getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder) {
		//默认大单物流状态为 未发货
		TSDictionary freightState = null;
		//部分发货数量
		Integer partDeliverNum  = 0;
		//已签收数量
		Integer alreadySignNum = 0;
		//已发货数量
		Integer alreadyDeliverNum = 0;
		//待发货数量
		Integer waitDeliverNum = 0;
		//待打包数量
		Integer waitPackNum = 0;
		if(paybillsOrder ==  null){
			return null;
		}
		
		PayBillsOrderEntity payBills = findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
		if(payBills ==  null){
			return null;
		}
		
		List<OrderEntity> orderList = payBills.getOrderS();
		if(!ListUtil.isNotEmpty(orderList)){
			return null;
		}
		
		//当前关联的订单
		for (OrderEntity orderEntity : orderList) {
			TSDictionary orderFreightStateEntity = orderEntity.getFreightState();
			if(orderFreightStateEntity == null){
				continue;
			}
			TSDictionary orderFreightState = dictionaryService.findUniqueByProperty(TSDictionary.class, "id", orderFreightStateEntity.getId());
			if(orderFreightState == null){
				continue;
			}
			
			if(orderFreightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_PACK.getValue())){
				//待打包
				waitPackNum++;
			}else if(orderFreightState.getDictionaryValue().equals(OrderServiceI.FreightState.WAIT_DELIVER.getValue())){
				//待发货
				waitDeliverNum++;
			}else if(orderFreightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_DELIVER.getValue())){
				//已发货
				alreadyDeliverNum++;
			}else if(orderFreightState.getDictionaryValue().equals(OrderServiceI.FreightState.ALREADY_SIGN.getValue())){
				//已签收
				alreadySignNum++;
			}else if(orderFreightState.getDictionaryValue().equals(OrderServiceI.FreightState.PART_DELIVER.getValue())){
				//部分发货
				partDeliverNum++;
			}
		}
		
		Integer alreadySignDifferCount = waitPackNum + waitDeliverNum + alreadyDeliverNum + partDeliverNum;
		
		Integer alreadyDeliverDifferCount = waitPackNum + waitDeliverNum + partDeliverNum;
		
		if(alreadySignNum > 0 && alreadySignDifferCount <= 0){
			//已签收
			freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.ALREADY_SIGN.getValue());
		}else if(alreadyDeliverNum > 0 && alreadyDeliverDifferCount <= 0){
			//已发货
			freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.ALREADY_DELIVER.getValue());
		}else if(alreadySignNum > 0 || alreadyDeliverNum > 0 || partDeliverNum > 0){
			//部分发货
			freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.PART_DELIVER.getValue());
		}else if(waitPackNum > 0){
			//待打包
			freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.WAIT_PACK.getValue());
		}else{
			//待发货
			freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.WAIT_DELIVER.getValue());
		}
		
		return freightState;
	}

	@Override
	public AjaxJson cancelPayBillsOrder(PayBillsOrderEntity paybillsOrder, HttpServletRequest request) throws Exception {
		AjaxJson json = new AjaxJson();
		
		if(StringUtil.isEmpty(paybillsOrder.getId())){
			json.setSuccess(false);
			json.setMsg( "系统没有找到您要取消的订单！" );
			return json;
		}
		
		PayBillsOrderEntity payBillsOrderEntity = findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
		
		if(PayBillsOrderServiceI.State.UNPAID.getValue().equals( payBillsOrderEntity.getState() )){
			
			List<OrderEntity> orderlist = payBillsOrderEntity.getOrderS();
			for (OrderEntity orderEntity : orderlist) {
				
				if( OrderServiceI.State.UNPAID.getValue().equals(orderEntity.getState()) || 
						OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(orderEntity.getFreightState().getDictionaryValue())){
					
					orderEntity.setState( OrderServiceI.State.CANCEL.getValue());
					
				}else{
					json.setSuccess(false);
					json.setMsg( "取消订单失败，您有部分订单已经发货或者签收，请联系供货商进行处理或者申请退单！" );
					break;
				}
			}
			
			if(json.isSuccess()){
				
				batchSave(orderlist);
				
				payBillsOrderEntity.setState( PayBillsOrderServiceI.State.CANCEL.getValue() );
				
				updateEntitie( payBillsOrderEntity );
			}
		}
		else if(PayBillsOrderServiceI.State.CANCEL.getValue().equals( payBillsOrderEntity.getState() )){
			json.setSuccess(false);
			json.setMsg( "不要闹，您的订单已经取消！" );
			
		}else{
			
			json.setSuccess(false);
			json.setMsg( "取消订单失败，只有未支付订单才能取消，其它情况请申请退单！" );
		}
		return json;
	}
	
	/**
	 * 退单更新订单状态
	 * 
	 * */
	public void setRebackStatus(PayBillsOrderEntity payBillsOrder){
		
		payBillsOrder = findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrder.getId());
		
		List<OrderEntity> orderlist = payBillsOrder.getOrderS();
		
		int cancelcnt = 0;
		if( null != orderlist && orderlist.size() > 0 ){
			for( OrderEntity order : orderlist ){
				if( OrderServiceI.State.REBACK.getValue().equals(order.getState())){
					cancelcnt ++ ;
				}
			}
		}
		
		if( cancelcnt > 0 &&  cancelcnt == orderlist.size() ){
			payBillsOrder.setState( PayBillsOrderServiceI.State.REBACK.getValue() );
		}
	}
	
	/**
	 * 下单自动出库
	 * 根据支付订单
	 * @param payBillsOrder
	 * @return
	 */
	public AjaxJson payBillsOrderStockOut(PayBillsOrderEntity payBillsOrder,HttpServletRequest request){
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson result = new AjaxJson();
		result.setMsg("订单出库成功");
		
		try {
			result = checkBillsOrderProductStockEnough(payBillsOrder);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			return result;
		}
		
		 List<OrderEntity> orders = payBillsOrder.getOrderS();
		 
		 for (OrderEntity orderEntity : orders) {
			 
			if(OrderServiceI.State.CANCEL.getValue().equals(orderEntity.getState())
					|| OrderServiceI.State.REBACK.getValue().equals(orderEntity.getState())){
				continue;
			}
			ProductEntity productEntity = orderEntity.getProductid();
			
			//检查商品是否关联销售自动出库
			if(ProductEnumMap.ASSOCIATE_STOCK_NO.getValue().equals(productEntity.getAssociateStock())){
				continue;
			}
			
			//检查库存商品,根据商品获取库存商品状况
			List<StockProductEntity> stockProducts = stockProductService.checkStockProduct(productEntity);
			if(stockProducts == null){
				result.setSuccess(false);
				result.setMsg("订单内商品【" + productEntity.getName() + "】已售罄");
				break;
			}
			
			//订单的商品数量
			BigDecimal orderNum = ConvertTool.toBigDecimal(orderEntity.getNumber());
			
			//对库存商品进行出库操作
			for (StockProductEntity stockProductEntity : stockProducts) {
				StockEntity stockEntity = stockProductEntity.getStockid();
				StockSegmentEntity stockSegmentEntity = stockProductEntity.getStockSegment();
				TSCompany company =  user.getCompany();
				
				//当前库存商品单价
				BigDecimal stockNum = stockProductEntity.getStocknum();
				BigDecimal unitPrice = stockProductEntity.getAverageprice();
				//没有库存的直接下一个仓库出
				if(stockNum.compareTo(ConvertTool.toBigDecimal(0)) <= 0){
					continue;
				}
				//当前出库数量
				BigDecimal stockOutNum = orderNum;
				//缺货的情况，备注说明
				StringBuffer remark = new StringBuffer();
				if(stockNum.compareTo(orderNum) < 0){
					stockOutNum = stockNum;
					remark.append(productEntity.getName()).append("库存不足").append(stockNum.subtract(orderNum).abs());
					//计算剩余出库数量
					orderNum = orderNum.subtract(stockOutNum);
					
				}else{
					orderNum = new BigDecimal(0);
				}
				
				//出库总价
				BigDecimal cur_totalPrice = stockOutNum.multiply(unitPrice).setScale(4,BigDecimal.ROUND_HALF_UP);
				//出库类型
				TSDictionary dict = new TSDictionary();
				dict.setDictionaryType(StockIOType.OUT);
				dict.setDictionaryName(StockIOType.StockOutType.SALE_OUT.getName());
				dict.setDictionaryDesc(StockIOType.StockOutType.SALE_OUT.getName());
				dict.setDictionaryValue(StockIOType.StockOutType.SALE_OUT.getValue());
				
				TSDictionary dictEntity = dictionaryService.check2addDictionaryItem(dict);
				
				//基本信息
				StockIOEntity stockIOEntity = new StockIOEntity();
				stockIOEntity.setIdentifier(IdWorker.generateSequenceNo());
				stockIOEntity.setRemark("下单自动出库");
				stockIOEntity.setStockid(stockEntity);
				stockIOEntity.setStocktypeid(dictEntity);
				stockIOEntity.setCompanyid(company);
				stockIOEntity.setCreatetime(DateUtils.getDate());
				stockIOEntity.setOperator(user);
				stockIOEntity.setStockid(stockEntity);
				stockIOEntity.setStockIODetailList(null);
				
				//出库商品详情
				Map<String, Object> stockIODetailParams = new HashMap<String, Object>();
				stockIODetailParams.put("id", productEntity.getId());
				stockIODetailParams.put("name", productEntity.getName());
				stockIODetailParams.put("detailremark", remark.toString());
				stockIODetailParams.put("unitid", productEntity.getUnit().getId());
				stockIODetailParams.put("segmentid", stockSegmentEntity.getId());
				stockIODetailParams.put("freightprice", 0);
				stockIODetailParams.put("unitprice", unitPrice);
				stockIODetailParams.put("totalprice", cur_totalPrice);
				stockIODetailParams.put("iostocknum", stockOutNum);
				List<Map<String, Object>> exps = new ArrayList<Map<String, Object>>();
				exps.add(stockIODetailParams);
				
				List<StockIODetailEntity> stockIODetailList = stockIODetailService.createStockIODetail(stockIOEntity, exps);
				if(stockIODetailList.size() > 0 ){
					//保存出库基本信息
					systemService.save(stockIOEntity);
					//批量保存出库详情信息
					systemService.batchSave(stockIODetailList);
					stockIOEntity.setStockIODetailList(stockIODetailList);
					systemService.updateEntitie(stockIOEntity);
					
					updateStockProduct(stockIOEntity, request);
				}
				//没有剩余出库数量，循环下一个商品出库
				if(orderNum.compareTo(new BigDecimal(0)) <= 0){
					break;
				}
			}
		 }
		 return result;
	}
	
	
	private AjaxJson updateStockProduct(StockIOEntity stockIOEntity, HttpServletRequest request){
		AjaxJson result = new AjaxJson();
		TSCompany company = stockIOEntity.getCompanyid();
		StockEntity stockEntity = stockIOEntity.getStockid();
		List<StockIODetailEntity> stockIODetailList  = stockIOEntity.getStockIODetailList();
		for (StockIODetailEntity stockIODetail : stockIODetailList) {
			Map<String, Object> params = new HashMap<String, Object>();
			ProductEntity productEntity = stockIODetail.getProductid();
			StockSegmentEntity segmentEntity = stockIODetail.getStockSegment();
			params.put("ioType", StockIOType.OUT);
			params.put("stockEntity", stockEntity);
			params.put("segmentEntity", segmentEntity);
			params.put("companyEntity", company);
			params.put("productEntity",productEntity);
			
			params.put("op_stockNum",ConvertTool.toBigDecimal(stockIODetail.getIostocknum()));
			params.put("op_totalPrice",ConvertTool.toBigDecimal(stockIODetail.getTotalprice()));
			params.put("op_avgPrice",ConvertTool.toBigDecimal(stockIODetail.getUnitprice()));
			//更新库存商品状况
			boolean check = stockProductService.saveOrUpdateStockProduct( params);
			if(!check){
				systemService.delete(stockIOEntity);
				systemService.deleteAllEntitie(stockIODetailList);
				result.setSuccess(false);
				String msg = StockIOType.StockOutType.SALE_OUT.getName() + "失败";
				systemService.addLog(msg, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
				result.setMsg(msg);
				return result;
			}
			//更新库存商品所有统计价格
			stockProductPriceService.updateStockProductPrice(productEntity, stockEntity, request);
		}
		
		return result;
	}
	/**
	 * 下单自动出库
	 * 根据收支流水
	 * @param payBills
	 * @return
	 */
	public AjaxJson payBillsOrderStockOut(PayBillsEntity payBills,HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		
		PayBillsEntity payBillsEntity = systemService.get(PayBillsEntity.class, payBills.getId());
		List<PayBillsFlowEntity> payBillsFlows = payBillsEntity.getPayBillsFlowS();
		
		if(payBillsFlows != null && payBillsFlows.size() > 0){
			for( int i=0 ; i < payBillsFlows.size() ; i++ ){
				 PayBillsOrderEntity payBillsOrder  =payBillsFlows.get(i).getBillsorderid();
				 result = this.payBillsOrderStockOut(payBillsOrder,request);
			}
		}
		return result;
	}

	@Override
	public AjaxJson checkBillsOrderProductStockEnough(PayBillsOrderEntity payBillsOrder) throws Exception{
		AjaxJson msg = new AjaxJson();
		StringBuffer msgbuf = new StringBuffer();
		
		List<OrderEntity> orders = payBillsOrder.getOrderS();
		for (OrderEntity orderEntity : orders) {
			ProductEntity productEntity = orderEntity.getProductid();
			
			if(ProductEnumMap.ASSOCIATE_STOCK_NO.getValue().equals(productEntity.getAssociateStock())){
				continue;
			}
			
			BigDecimal ordernum = new BigDecimal(orderEntity.getNumber());
			
			boolean stockEnough = stockProductService.checkStockProductEnough(productEntity, ordernum) ; 
			
			if(!stockEnough){
				
				if(msgbuf.length() > 0){
					msgbuf.append(",");
					msgbuf.append(productEntity.getName());
					
				}else{
					msgbuf.append(productEntity.getName());
				}
			}
			
		}
		
		String msgstr = msgbuf.toString();
		if(StringUtil.isEmpty(msgstr)){
			msg.setSuccess(true);
			msg.setMsg("商品充足，请放心购买");
		}else{
			msg.setSuccess(false);
			msg.setMsg(msgstr);
		}
		
		return msg;
	}

	@Override
	public AjaxJson billsOrder2Received(PayBillsOrderEntity payBillsOrder) throws Exception {
		AjaxJson json = new AjaxJson();
		json.setMsg("订单签收成功！");
		payBillsOrder = findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrder.getId());
		TSDictionary dict = dictionaryService.checkDictItemWithoutCompany("freightStates", PayBillsOrderServiceI.FreightStatus.RECEIVED.getValue());
		if(null != dict){
			for (OrderEntity order : payBillsOrder.getOrderS()) {
				orderService.changeOrderFreightState(order, dict);
			}
			payBillsOrder.setFreightState(dict);
			
		}
		return json;
	}
	
	/**
	 * 更新订单取消状态
	 * 
	 * */
	@Override
	public void setCancelStatus(PayBillsOrderEntity payBillsOrder){
		
		payBillsOrder = findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrder.getId());
		
		List<OrderEntity> orderlist = payBillsOrder.getOrderS();
		
		int cancelcnt = 0;
		if( null != orderlist && orderlist.size() > 0 ){
			for( OrderEntity order : orderlist ){
				if( OrderServiceI.State.CANCEL.getValue().equals(order.getState())){
					cancelcnt ++ ;
				}
			}
		}
		
		if( cancelcnt > 0 &&  cancelcnt == orderlist.size() ){
			payBillsOrder.setState( PayBillsOrderServiceI.State.CANCEL.getValue() );
		}
	}
}