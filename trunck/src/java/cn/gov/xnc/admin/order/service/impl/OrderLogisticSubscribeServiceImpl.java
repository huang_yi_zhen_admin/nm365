package cn.gov.xnc.admin.order.service.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticSubscribeEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticSubscribeServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ExpressSearchUtil;

@Service("orderLogisticSubscribeService")
@Transactional
public class OrderLogisticSubscribeServiceImpl extends CommonServiceImpl implements OrderLogisticSubscribeServiceI {

	private Logger logger = Logger.getLogger(OrderLogisticSubscribeServiceImpl.class);
	
	@Override
	public boolean orderLogisticSubscribe(OrderEntity order, String logisticCode, String logisticCompany) {
		
		boolean success = false;
		
		//订阅物流信息
		try {
			int subnum = getCountOrderLogisticSubscribe(order.getId(), logisticCode, logisticCompany);
			
			if(subnum < 1){
				//调取快递鸟接口
				String subscribeResult = ExpressSearchUtil.freightInfoBySubscribe(logisticCompany, logisticCode);
				
				logger.error("-------------orderLogisticSubscribe result : " + subscribeResult);
				Map<String, Object>  datas = (Map<String, Object>) JSON.parse(subscribeResult);
				success = (boolean) datas.get("Success");
				
				String subStatus = (true == success? "1" : "0");
				addOrderLogisticSubscribe(order, logisticCode, logisticCompany, subStatus);
			}
		} catch (Exception e) {
			logger.error("-------------orderLogisticSubscribe error : " + e);
		}
		
		logger.error("-------------orderLogisticSubscribe END ----------------------");
		
		return success;
	}
	
	@Override
	public int getCountOrderLogisticSubscribe(String orderid, String logisticCode, String logisticCompany){
		int num = 0;
		StringBuffer sqlbuf = new StringBuffer();
		sqlbuf.append(" select count(1) from xnc_order_logistics_subscribe ");
		sqlbuf.append(" where orderid = ? ");
		sqlbuf.append(" and logisticscode = ? ");
		sqlbuf.append(" and platformcode = ? ");
		sqlbuf.append(" and status = '1' ");
		
		num = getCountForJdbcParam(sqlbuf.toString(), new String[]{orderid, logisticCode, logisticCompany}).intValue();
		
		return num;
	}
	
	private void addOrderLogisticSubscribe(OrderEntity order, String logisticCode, String logisticCompany, String stutas) {
		OrderLogisticSubscribeEntity orderLogisticSubscribe = new OrderLogisticSubscribeEntity();
		orderLogisticSubscribe.setCreatetime(DateUtils.getDate());
		orderLogisticSubscribe.setLogisticcode(logisticCode);
		orderLogisticSubscribe.setOrderid(order);
		orderLogisticSubscribe.setPlatformcode(logisticCompany);
		orderLogisticSubscribe.setStatus(stutas);
		
		save(orderLogisticSubscribe);
	}
	
}