package cn.gov.xnc.admin.order.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;


/**   
 * @Title: Entity
 * @Description: 订单应付流水
 * @author zero
 * @date 2016-10-02 02:02:04
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_pay_bills_order", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class PayBillsOrderEntity implements java.io.Serializable {
	/**id*/
	private String id;
	/**订单编号*/
	private String identifier;
	/**客户信息*/
	private TSUser  clientid;
	/**公司信息*/
	private TSCompany company;
	/**业务员*/
	private TSUser  yewu;
	/**流水类型 1 销售 2充值*/
	private String type;
	/**状态  1 待付 2已付 3 未付清  10未特殊传递可付款状态 已取消_4*/
	private String state;
	/**物流状态*/
	private TSDictionary freightState;
	/**支付方式 支付宝_1,微信支付_2,线下转账_3,预存款支付_4,银联支付_5,先货后款_6*/
	private String paymentmethod;
	/**账户余额*/
	private BigDecimal balancemoney;
	/**应付款*/
	private BigDecimal payablemoney;
	/**已付款*/
	private BigDecimal alreadypaidmoney;
	/**待付金额*/
	private BigDecimal obligationsmoney;
	/**退款金额*/
	private BigDecimal cancelMoney;
	/**退单总成本*/
	private BigDecimal baseCost;
	/**总运费*/
	private BigDecimal allFreightMoney;
	/**审核人*/
	private TSUser audituser;
	/**审核日期*/
	private java.util.Date auditdate;
	/**备注*/
	private String remark;
	/**创建日期*/
	private java.util.Date createdate;
	/**最后一次更新日期*/
	private java.util.Date updatedate;
	/**是否代下单*/
	private String isSaleManOrder;
	/**交易方式 1 款到发货 2 先货后款*/
	private String tradeType;
	
	
	/**订单明细list*/
	private List<OrderEntity> orderS = new ArrayList<OrderEntity>();
	
	
	/**支付流水list*/
	private List<PayBillsFlowEntity> payBillsFlows = new ArrayList<PayBillsFlowEntity>();
	
	
	
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	public String getId(){
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  订单编号
	 */
	@Column(name ="IDENTIFIER",nullable=true,length=32)
	public String getIdentifier(){
		return this.identifier;
	}

	/**
	 *方法: 设置String
	 *@param: String  订单编号
	 */
	public void setIdentifier(String identifier){
		this.identifier = identifier;
	}
	/**
	 *方法: 取得String
	 *@return: String  客户信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLIENTID")
	public TSUser getClientid(){
		return this.clientid;
	}

	/**
	 *方法: 设置String
	 *@param: String  客户信息
	 */
	public void setClientid(TSUser clientid){
		this.clientid = clientid;
	}
	/**
	 *方法: 取得String
	 *@return: String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany(){
		return this.company;
	}

	/**
	 *方法: 设置String
	 *@param: String  公司信息
	 */
	public void setCompany(TSCompany company){
		this.company = company;
	}
	/**
	 *方法: 取得String
	 *@return: String  业务员
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "YEWU")
	public TSUser getYewu(){
		return this.yewu;
	}

	/**
	 *方法: 设置String
	 *@param: String  业务员
	 */
	public void setYewu(TSUser yewu){
		this.yewu = yewu;
	}
	/**
	 *方法: 取得String
	 *@return: String  流水类型 1 销售 2充值
	 */
	@Column(name ="TYPE",nullable=true,length=2)
	public String getType(){
		return this.type;
	}

	/**
	 *方法: 设置String
	 *@param: String  流水类型 1 销售 2充值
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态 1 待付 2已付 3 未付清   10未特殊传递可付款状态, 已取消_4
	 */
	@Column(name ="STATE",nullable=true,length=2)
	public String getState(){
		return this.state;
	}

	/**
	 *方法: 设置String
	 *@param: String  状态 1 待付 2已付 3 未付清     10未特殊传递可付款状态, 已取消_4
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  支付方式 支付宝_1,微信支付_2,线下转账_3,预存款支付_4,线下转账_5
	 */
	@Column(name ="PAYMENTMETHOD",nullable=true,length=2)
	public String getPaymentmethod(){
		return this.paymentmethod;
	}

	/**
	 *方法: 设置String
	 *@param: String  支付方式 支付宝_1,微信支付_2,线下转账_3,预存款支付_4,线下转账_5
	 */
	public void setPaymentmethod(String paymentmethod){
		this.paymentmethod = paymentmethod;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  账户余额
	 */
	@Column(name ="BALANCEMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getBalancemoney(){
		return this.balancemoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  账户余额
	 */
	public void setBalancemoney(BigDecimal balancemoney){
		this.balancemoney = balancemoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  应付款
	 */
	@Column(name ="PAYABLEMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getPayablemoney(){
		return this.payablemoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  应付款
	 */
	public void setPayablemoney(BigDecimal payablemoney){
		this.payablemoney = payablemoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  已付款
	 */
	@Column(name ="ALREADYPAIDMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getAlreadypaidmoney(){
		return this.alreadypaidmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  已付款
	 */
	public void setAlreadypaidmoney(BigDecimal alreadypaidmoney){
		this.alreadypaidmoney = alreadypaidmoney;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  待付金额
	 */
	@Column(name ="OBLIGATIONSMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getObligationsmoney(){
		return this.obligationsmoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  待付金额
	 */
	public void setObligationsmoney(BigDecimal obligationsmoney){
		this.obligationsmoney = obligationsmoney;
	}
	
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  退款金额
	 */
	@Column(name ="CANCELMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getCancelMoney() {
		return cancelMoney;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  退款金额
	 */
	public void setCancelMoney(BigDecimal cancelMoney) {
		this.cancelMoney = cancelMoney;
	}

	/**
	 *方法: 取得String
	 *@return: String  审核人
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUDITUSER")
	public TSUser getAudituser(){
		return this.audituser;
	}

	/**
	 *方法: 设置String
	 *@param: String  审核人
	 */
	public void setAudituser(TSUser audituser){
		this.audituser = audituser;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  审核日期
	 */
	@Column(name ="AUDITDATE",nullable=true)
	public java.util.Date getAuditdate(){
		return this.auditdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  审核日期
	 */
	public void setAuditdate(java.util.Date auditdate){
		this.auditdate = auditdate;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=4000)
	public String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setRemark(String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建日期
	 */
	@Column(name ="CREATEDATE",nullable=true)
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建日期
	 */
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  最后一次更新日期
	 */
	@Column(name ="UPDATEDATE",nullable=true)
	public java.util.Date getUpdatedate(){
		return this.updatedate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  最后一次更新日期
	 */
	public void setUpdatedate(java.util.Date updatedate){
		this.updatedate = updatedate;
	}

	
	/**
	 * @return 订单明细list
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "billsorderid")
	public List<OrderEntity> getOrderS() {
		return orderS;
	}

	/**
	 * @param 订单明细list
	 */
	public void setOrderS(List<OrderEntity> orderS) {
		this.orderS = orderS;
	}

	/**
	 * @return 支付流水list
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "billsorderid")
	public List<PayBillsFlowEntity> getPayBillsFlows() {
		return payBillsFlows;
	}

	/**
	 * @param 支付流水list
	 */
	public void setPayBillsFlows(List<PayBillsFlowEntity> payBillsFlows) {
		this.payBillsFlows = payBillsFlows;
	}

	/**
	 *方法: 取得String
	 *@return: String  是否代客下单  
	 */
	@Column(name ="ISSALEMANORDER",nullable=true,length=2)
	public String getIsSaleManOrder() {
		return isSaleManOrder;
	}
	/**
	 *方法: 写入String
	 *@return: String  是否代客下单  
	 */
	public void setIsSaleManOrder(String isSaleManOrder) {
		this.isSaleManOrder = isSaleManOrder;
	}

	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  总运费
	 */
	@Column(name ="ALLFREIGHTMONEY",nullable=true,precision=10,scale=2)
	public BigDecimal getAllFreightMoney() {
		return allFreightMoney;
	}

	public void setAllFreightMoney(BigDecimal allFreightMoney) {
		this.allFreightMoney = allFreightMoney;
	}

	@Column(name ="BASECOST",nullable=true,precision=10,scale=2)
	public BigDecimal getBaseCost() {
		return baseCost;
	}

	public void setBaseCost(BigDecimal baseCost) {
		this.baseCost = baseCost;
	}

	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "freightState")
	public TSDictionary getFreightState() {
		return freightState;
	}

	public void setFreightState(TSDictionary freightState) {
		this.freightState = freightState;
	}

	/**
	 * 交易方式
	 * @return
	 */
	@Column(name ="TRADETYPE",nullable=true,length=2)
	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	
}
