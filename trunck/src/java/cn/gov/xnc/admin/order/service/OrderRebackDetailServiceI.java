package cn.gov.xnc.admin.order.service;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackDetailEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderRebackDetailServiceI extends CommonService{
	/**
	 * 批量保存退货订单
	 * @param cancelEntiy
	 * @param req
	 * @throws Exception
	 */
	public List<OrderRebackDetailEntity> batchSaveCancelDetails(final OrderRebackEntity cancelEntiy, HttpServletRequest req) throws Exception;
	
	/**
	 * 获取已经退单的总数量
	 * 根据商品最小单位换算之后，返回退单总和
	 * @param o
	 * @return
	 * @throws Exception
	 */
	public BigDecimal getHasCancelOrderTotal2BaseUnit(OrderEntity o) throws Exception;
}
