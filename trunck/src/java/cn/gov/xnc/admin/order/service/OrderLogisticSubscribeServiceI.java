package cn.gov.xnc.admin.order.service;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderLogisticSubscribeServiceI extends CommonService{

	/**
	 * 物流订阅
	 * @param order
	 * @param logisticCode
	 * @param logisticCompany
	 * @return
	 */
	public boolean orderLogisticSubscribe(OrderEntity order, String logisticCode, String logisticCompany);
	
	/**
	 * 获取订阅记录条数
	 * @param orderid
	 * @param logisticCode
	 * @param logisticCompany
	 * @return
	 */
	public int getCountOrderLogisticSubscribe(String orderid, String logisticCode, String logisticCompany);
}
