package cn.gov.xnc.admin.order.service;

import java.util.List;

import cn.gov.xnc.admin.order.entity.OrderChannelEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface OrderChannelServiceI extends CommonService{

	public static final String CHANNEL_DEL = "D"; //删除渠道
	public static final String CHANNEL_USING = "U";//启用渠道
	
	public List<OrderChannelEntity> getCompanyAllChannel();
	/**通过渠道名称获取下单渠道*/
	public OrderChannelEntity getOrderChannelByName(String channelName);
	/**通过渠道名称获取渠道数量,判断渠道名称是否有重复
	 * channelName 必须
	 * channelID 可选，新增渠道不用传入id，更新渠道的时候需要传入id一起确认
	 * */
	public int getCountDuplicateChannelByName(String channelName, String channelID);
	
}
