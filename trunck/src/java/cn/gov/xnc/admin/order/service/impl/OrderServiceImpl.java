package cn.gov.xnc.admin.order.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.excel.vo.XiadanProductVO;
import cn.gov.xnc.admin.freight.service.FreightDetailsServiceI;
import cn.gov.xnc.admin.message.service.MessageTemplateServiceI;
import cn.gov.xnc.admin.order.entity.OrderChannelEntity;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderChannelServiceI;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.service.ProductEnumMap;
import cn.gov.xnc.admin.product.service.ProductServiceI;
import cn.gov.xnc.admin.salesman.service.SalesmanCommissionServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.admin.verify.service.VerifyOrderService;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.BeanToMapUtils;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.manager.ClientManager;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.TerritoryService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;

@Service("orderService")

public class OrderServiceImpl extends CommonServiceImpl implements OrderServiceI {

	@Autowired
	private SystemService systemService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private UserGradePriceServiceI userGradePriceService;
	@Autowired
	private FreightDetailsServiceI freightDetailsService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private MessageTemplateServiceI messageTemplateService ;
	@Autowired
	private VerifyOrderService verifyOrderService;
	@Autowired
	private SalesmanCommissionServiceI salesmanCommissionService ;
	@Autowired
	private TerritoryService territoryService;
	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private ProductServiceI productService;
	@Autowired
	private OrderChannelServiceI orderChannelService;
	
	private Logger logger = Logger.getLogger(OrderServiceImpl.class);

	
	
	/**
	 * 订单生成
	 * @param request
	 * @param response
	 * @param xiadanlist
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<OrderEntity> orderGenerator(HttpServletRequest request, List<XiadanProductVO> xiadanlist) throws Exception{
		
		TSUser u = null ;
		TSUser userClient = null;//代客下单对应的采购商
		try {
			u = ResourceUtil.getSessionUserName();
			if(u == null ){//第二种获取方式
				u = ClientManager.getInstance().getClient(request.getParameter("sessionId")).getUser();
			}
		} catch (Exception e) {
			
		}
		u = systemService.getEntity(TSUser.class, u.getId());
		
//		List<OrderEntity> addlist = new ArrayList<OrderEntity>();// 对应的订单数据
		/**
		 * orderProduct 和orderNumber 是采购综合统计结果
		 * 记录订购商品和数量，以备判断商品订购总数是否满足最小订购量
		 * 采购商身份：1）独立个体，只为自己采购 2）微商，为多人采购，凑订购数量
		 * 服务应根据采购商的综合采购情况，判断是否达到采购标准* */
		Map<String, ProductEntity> orderProduct = new HashMap<String, ProductEntity>();
		Map<String, Integer> orderNumber = new HashMap<String, Integer>();
		final List<XiadanProductVO> list = xiadanlist;
		
		Map<String, PayBillsOrderEntity> payBillsOrderMap = new HashMap<String, PayBillsOrderEntity>();
		
		for (XiadanProductVO e : list) {
			//获取采购渠道
			OrderChannelEntity orderChannel = orderChannelService.getOrderChannelByName(e.getChannelName());
			//代客下单，获取采购客户信息
			if(!"3".equals(u.getType()) ){
				if(StringUtil.isNotEmpty(e.getUserBuyerId()) ){
					userClient = systemService.findUniqueByProperty(TSUser.class, "id", e.getUserBuyerId());
				}
			}else{
				userClient = u;
			}
			
			//是否自提，目前根据备注信息“自提”判断
			String sendType = "2";//走物流
			String remark = e.getRemarks();
			if(null != remark && remark.indexOf("自提") >= 0){
				sendType = "1";//自提
			}
			
			boolean add = true;
			if (add && "2".equals(sendType) && StringUtils.isEmpty(e.getAddress())) {
				// 收货地址为空
				add = false;
				
			} 
			if(add && "2".equals(sendType) && StringUtils.isNotEmpty(e.getAddress())){
				
				add = territoryService.hasProvinceInfo(e.getAddress().substring(0,2));
				
			}
			if (add && StringUtils.isEmpty(e.getCustomername())) {
				// 收货姓名为空
				add = false;
				
			}
			if (add && StringUtils.isEmpty(e.getTelephone())) {
				// 收货联系方式
				add = false;
				
			}
			if (add && StringUtils.isEmpty(e.getProductname())) {
				// 产品名称为空
				add = false;
				
			}
			if (add && e.getNumber() < 1) {
				// 产品件数不匹配
				add = false;
				
			}
			if (add && StringUtils.isEmpty(e.getCode())) {
				// 产品信息不存在
				add = false;
				
			}
			if ( (add &&  !"3".equals(u.getType()) && null == userClient)
					|| (add && !"3".equals(u.getType()) && null != userClient && StringUtil.isEmpty(userClient.getId())) ){
				
				add = false;
			}
			
			if (add) {
				String filterAdrr = StringUtil.replaceBlank(e.getAddress());
				e.setAddress(filterAdrr);// 过滤地址自带换行符
				e.setCustomername(StringUtil.replaceBlank(e.getCustomername()));
				e.setTelephone(StringUtil.getNumberString( StringUtil.replaceBlank(e.getTelephone())) );
				ProductEntity p = null;
				CriteriaQuery cq = new CriteriaQuery(ProductEntity.class);
				cq.eq("code", e.getCode());
				cq.eq("company", u.getCompany());
				cq.eq("isDelete", "N");
				cq.eq("notsale", "N");
				cq.add();
				List<ProductEntity> productlist = systemService.getListByCriteriaQuery(cq, false);
				if (productlist != null && productlist.size() > 0) {
					p = productlist.get(0);
					
				}
				
				// 产品名称
				if ((p != null && StringUtil.isNotEmpty(p.getId()))) {
					
					//记录所有需要采购的商品
					if(!orderProduct.containsKey(p.getId())){
						orderProduct.put(p.getId(), p);
					}
					if(orderNumber.containsKey(p.getId())){
						orderNumber.put(p.getId(), orderNumber.get(p.getId()) + e.getNumber());
					}
					else{
						orderNumber.put(p.getId(), e.getNumber());
					}
					
					
					//检查商品是否关联销售自动出库
					if(ProductEnumMap.ASSOCIATE_STOCK_YES.getValue().equals(p.getAssociateStock())){
						//检查库存商品,根据商品获取库存商品状况
						/*Map<String, Object> stockProductParams = new HashMap<String,Object>();
						stockProductParams.put("productEntity", product);
						List<StockProductEntity> stockProducts = stockProductService.checkStockProduct(stockProductParams, product.getCompany());*/
						
						boolean productEnough = stockProductService.checkStockProductEnough(p, ConvertTool.toBigDecimal(e.getNumber()));
						if(!productEnough){
							throw new Exception("商品：" + p.getName() + "库存不足");
						}
					}
					
					e.setSpecifications(p.getSpecifications());
					// e.setPrice(p.getPrice());//销售价格需要重新计算
					e.setProductname(p.getName());//产品名称以导入的商品编码为主
					e.setCreatedate(null);
					// 创建对应的订单数据
					OrderEntity o = new OrderEntity();
					String province = "";// 对于省份信息
					try {
						//系统提交的区域表id
						TSTerritory territory = new TSTerritory();
						if( StringUtil.isNotEmpty(e.getTSTerritory()) ){
							String terrritoryid = e.getTSTerritory();
							String[] array = terrritoryid.split(",");
							String finalterritory = terrritoryid;
							
							
							if(array.length == 3){//省——市——区
								
								territory.setId( array[2] );
								finalterritory = array[2];
								
							}else if(array.length == 2){//省——市
								
								territory.setId( array[1] );
								finalterritory = array[1];
								
							}else{//省
								territory.setId( terrritoryid );
							}
							territory.setId(finalterritory);
						}
						if(StringUtil.isEmpty(territory.getId())){
							territory = territoryService.getTopTerritoryByAddress(filterAdrr);
						}
						o.setTSTerritory(territory);
						
						MyBeanUtils.copyBeanNotNull2Bean(e, o);
						BigDecimal freightMoney = new BigDecimal("0.00"); // 运费价格
						try {
							if("2".equals(sendType)){
								freightMoney = freightDetailsService.getProductFreightByFullAddr(p, e.getAddress(), e.getNumber());
							}
							
						} catch (Exception ex) {
							freightMoney = new BigDecimal("0.00");
						}

						o.setProductid(p);
						o.setFreightpic(freightMoney);
						o.setState(OrderServiceI.State.UNPAID.getValue());
						o.setPlatform(u.getRealname());
						o.setCreatedate(DateUtils.getDate());
						o.setClientid(userClient);
						o.setYewu(userClient.getTsuserclients().getYewuid());
						o.setProvince(province);
						o.setCompany(u.getCompany());
						BigDecimal baseCost = productService.getProductBaseCost(p.getId());
						baseCost = (null == baseCost ? new BigDecimal(0.00) : baseCost);
						o.setPricec(baseCost.multiply(new BigDecimal(o.getNumber())));
						o.setUnitCost(baseCost);
						o.setChannelID(orderChannel);
						o.setSendtype(sendType);//是否自提
						
						//计算订单价格
						BigDecimal finalprice = userGradePriceService.getProductFinalPrice(p, userClient);
						if(null != e.getPrice()) {//如果页面修改了，以页面为主
							finalprice = e.getPrice();
						}
						BigDecimal totalPrice = finalprice.multiply(new BigDecimal(o.getNumber()));// 产品预订价，单价X件数
						BigDecimal productpiceYw = new BigDecimal(0.00);
						if(null != p.getPriceyw()){
							productpiceYw = p.getPriceyw().multiply(new BigDecimal(o.getNumber()));// 产品预订价，单价X件数
						}
						o.setPrice(finalprice);
						o.setPriceyw(productpiceYw);
						o.setTotalPrice(totalPrice);
						
						//根据用户ID创建MAP对应的 PayBillsOrderEntity
						PayBillsOrderEntity payBillsOrder =null;
						if(payBillsOrderMap.containsKey(userClient.getId())) {
							payBillsOrder = payBillsOrderMap.get(userClient.getId());
							List<OrderEntity> addlist = payBillsOrder.getOrderS();
							addlist.add(o);
							payBillsOrder.setOrderS(addlist);
							payBillsOrderMap.put(userClient.getId(), payBillsOrder);
						}else {
							payBillsOrder =  new PayBillsOrderEntity();
							List<OrderEntity> addlist = new ArrayList<OrderEntity>();		
							addlist.add(o);
							payBillsOrder.setOrderS(addlist);
							
							payBillsOrder.setIdentifier(IdWorker.generateSequenceNo());
							payBillsOrder.setCompany(u.getCompany());
							payBillsOrder.setClientid(userClient);
							payBillsOrder.setYewu(userClient.getTsuserclients().getYewuid());
							payBillsOrder.setType("1");// 销售类型流水
							payBillsOrder.setState(PayBillsOrderServiceI.State.UNPAID.getValue());// 待支付
							//待发货
//							TSDictionary freightState =  dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.WAIT_DELIVER.getValue());
//							payBillsOrder.setFreightState(freightState);
							payBillsOrder.setAlreadypaidmoney(new BigDecimal("0.00"));
//							payBillsOrder.setPayablemoney(money);
//							payBillsOrder.setObligationsmoney(money);
//							payBillsOrder.setAllFreightMoney(totalFreightMoney);
							if("4".equals(u.getType())){
								payBillsOrder.setIsSaleManOrder("1");//代客下单标志
							}
							
							payBillsOrderMap.put(userClient.getId(), payBillsOrder);
						}

					} catch (Exception e1) {
						throw new Exception(e1.getMessage());
					}
				}
			}
		}
		
		if (payBillsOrderMap != null && payBillsOrderMap.size() > 0) {
			//综合判断最小起订量是否满足
			Iterator<String> it = orderProduct.keySet().iterator();
			while (it.hasNext()) {
				String pid = it.next();
				ProductEntity p = orderProduct.get(pid);
				Integer pnum = orderNumber.get(pid);
				
				if(p.getMinorde() > pnum){
					throw new Exception("商品：" + p.getName() + "最少起订量是【" + p.getMinorde() + "】");
				}
			}
		}
		
		List<OrderEntity> relist = new ArrayList<OrderEntity>();
		
		//遍历MAP  payBillsOrderMap
		if (payBillsOrderMap != null && payBillsOrderMap.size() > 0) {
			
			
			//遍历map中的值 
			for (PayBillsOrderEntity payBillsOrder : payBillsOrderMap.values()) { 
				List<OrderEntity> addlist = payBillsOrder.getOrderS();
				try {
					BigDecimal money = new BigDecimal("0.00"); // 订单结算总价（包括运费）
					BigDecimal totalFreightMoney = new BigDecimal("0.00"); // 订单总运费
					//PayBillsOrderEntity payBillsOrder = new PayBillsOrderEntity();// 创建对应支付信息
					// 根据产品属性 计算产品阶梯价格

					for (int k = 0; k < addlist.size(); k++) {
						OrderEntity o = addlist.get(k);
						money = money.add(o.getFreightpic());
						money = money.add(o.getTotalPrice());
						totalFreightMoney = totalFreightMoney.add(o.getFreightpic());
						
					}
					//待发货
					TSDictionary freightState =  dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.WAIT_DELIVER.getValue());
					payBillsOrder.setFreightState(freightState);
					payBillsOrder.setPayablemoney(money);
					payBillsOrder.setObligationsmoney(money);
					payBillsOrder.setAllFreightMoney(totalFreightMoney);
					if("4".equals(u.getType())){
						payBillsOrder.setIsSaleManOrder("1");//代客下单标志
					}
					payBillsOrder.setOrderS(null);//先不创建子订单，反正出错
					Serializable id = systemService.save(payBillsOrder);
					payBillsOrder.setId(id.toString());
					for (int k = 0; k < addlist.size(); k++) {
						OrderEntity o = addlist.get(k);
						o.setBillsorderid(payBillsOrder);
						o.setIdentifieror(payBillsOrder.getIdentifier() + "_" + (k + 1));
						o.setFreightState(freightState);
						addlist.set(k, o);// 修改对应的元素
						relist.add(o);//返回LIST
					}
					systemService.batchSave(addlist);// 订单数据写入数据库
					
				} catch (Exception e2) {
					logger.error("采购提交订单出错：" + e2);
					throw new Exception(e2.getMessage());
				}

			}	
		}
		
		return relist;
	}
	
	
	
	/**
	* 公司产品销售数据分析
	* String companyid 公司ID
	* String startTime 开始时间
	* String endTime   截至时间
	*/
	public List<OrderEntity> salesCPList(String companyid, String startTime, String endTime) {
		
		
		String purchasecompanySQL ="";//公司统计
		if(StringUtil.isNotEmpty(companyid) && companyid.indexOf(",") > 0){
			purchasecompanySQL = " and  xnc_order.purchasecompanyid in("+companyid+")   ";
		}else if(StringUtil.isNotEmpty(companyid) ){
			purchasecompanySQL = " and  xnc_order.purchasecompanyid = '"+companyid+"' ";
		}

		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}
		
		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT  DATE_FORMAT( createDate, \"%Y-%m-%d\" ) as createDate ,  productname , SUM(totalPrice) as Price , SUM(Number) as Number , SUM(PriceJ) as PriceJ , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(purchasecompanySQL);
		buf.append(createDateSQL);
		buf.append(" GROUP BY   DATE_FORMAT( createDate, \"%Y-%m-%d\" ) ,  productId  ORDER BY  createDate ASC ");
		
		List<OrderEntity>  list = queryListByJdbc(buf.toString(),OrderEntity.class);	
		return list;
	}
	
	
	
	/**
	* 首页数据展示用  待发货单  今天已发货单   今天的销售单  昨天销售单  本月销售单  上月销售单
	* String companyid 公司ID
	* String startTime 开始时间
	* String endTime   截至时间
	*/
	public Map<String, OrderEntity> salesOPMap ( String companyid  ) {
		
		Map<String, OrderEntity> oPMap = new HashMap<String, OrderEntity>();
		
		String purchasecompanySQL ="";//公司统计
		if(StringUtil.isNotEmpty(companyid) && companyid.indexOf(",") > 0){
			purchasecompanySQL = " and  xnc_order.company in("+companyid+")   ";
		}else if(StringUtil.isNotEmpty(companyid) ){
			purchasecompanySQL = " and  xnc_order.company = '"+companyid+"' ";
		}
		String enddate = DateUtils.getDate("yyyy-MM-dd");//当前时间
		String endTime =enddate +" 23:59:59";
		String startTime =enddate +" 00:00:00";
		
		//String 	createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
				
		String DepartureDate= "  and xnc_order.DepartureDate >='"+startTime+" "+"' and xnc_order.DepartureDate <='"+endTime+"' ";
		
		
		//待付款
		String Sql_1 =" SELECT    IFNULL(SUM(totalPrice),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 1   " + purchasecompanySQL   ;
		
		
		List<OrderEntity>  list_1 = queryListByJdbc(Sql_1,OrderEntity.class);	
		
		OrderEntity  o1 = new OrderEntity();//待付款订单
		o1.setNumber(0);
		if( list_1 != null && list_1.size() > 0){
			o1 = list_1.get(0);
		}
		oPMap.put("o1", o1);
		
		
		//待付款
		String Sql_2 =" SELECT    IFNULL(SUM(totalPrice),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 2   " + purchasecompanySQL   ;
		List<OrderEntity>  list_2 = queryListByJdbc(Sql_2,OrderEntity.class);	
		OrderEntity  o2 = new OrderEntity();//待付款订单
		o2.setNumber(0);
		if( list_2 != null && list_2.size() > 0){
			o2 = list_2.get(0);
		}
		oPMap.put("o2", o2);
		
		
		//待付款
		String Sql_3 =" SELECT    IFNULL(SUM(totalPrice),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 6   " + purchasecompanySQL   ;
		List<OrderEntity>  list_3 = queryListByJdbc(Sql_3,OrderEntity.class);	
		OrderEntity  o3 = new OrderEntity();//待付款订单
		o3.setNumber(0);
		if( list_3 != null && list_3.size() > 0){
			o3 = list_3.get(0);
		}
		oPMap.put("o3", o3);
		
		
		
		//今日订单
		
		String paydateSQL= "   xnc_order.paydate >='"+startTime+" "+"' and xnc_order.paydate <='"+endTime+"' ";
		
		String Sql_4 =" SELECT    IFNULL(SUM(totalPrice),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE    "+paydateSQL + purchasecompanySQL   ;
		List<OrderEntity>  list_4 = queryListByJdbc(Sql_4,OrderEntity.class);	
		OrderEntity  o4 = new OrderEntity();//待付款订单
		o4.setNumber(0);
		if( list_4 != null && list_4.size() > 0){
			o4 = list_4.get(0);
		}
		oPMap.put("o4", o4);
		
		//今日发货

		String Sql_5 =" SELECT    IFNULL(SUM(totalPrice),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 3    "+DepartureDate + purchasecompanySQL   ;
		List<OrderEntity>  list_5 = queryListByJdbc(Sql_5,OrderEntity.class);	
		OrderEntity  o5 = new OrderEntity();//待付款订单
		o5.setNumber(0);
		if( list_5 != null && list_5.size() > 0){
			o5 = list_5.get(0);
		}
		oPMap.put("o5", o5);
		
		
		//今日退单
		String canceldateSQL= "  and   xnc_order.canceldate >='"+startTime+" "+"' and xnc_order.canceldate <='"+endTime+"' ";
		
		
		String Sql_6 =" SELECT    IFNULL(SUM(totalPrice),0) as Price , IFNULL(SUM(Number),0) as Number , IFNULL(SUM(priceC),0) as priceC , IFNULL(SUM(freightPic),0) as freightPic  FROM xnc_order  WHERE  xnc_order.state = 5    "+canceldateSQL + purchasecompanySQL   ;
		List<OrderEntity>  list_6 = queryListByJdbc(Sql_6,OrderEntity.class);	
		OrderEntity  o6 = new OrderEntity();//待付款订单
		o6.setNumber(0);
		if( list_6 != null && list_6.size() > 0){
			o6 = list_6.get(0);
		}
		oPMap.put("o6", o6);
		
		
		
		return oPMap;
		
		
		
	}



	@Override
	public AjaxJson orderGeneratorJson(HttpServletRequest request, List<XiadanProductVO> xiadanlist) throws Exception {
		AjaxJson json = new AjaxJson();
		json.setSuccess(true);//默认成功
		String msg = "订单生成成功！";
		List<OrderEntity> orderlist = orderGenerator(request, xiadanlist);
		
		int intDataNum = xiadanlist.size();
		int outDataNum = null != orderlist ? orderlist.size() : 0;
		if(outDataNum > 0 && outDataNum < intDataNum){
			msg = "共需要生成  " + intDataNum + " 条订单，其中成功 " + outDataNum + "条，失败 " + (intDataNum - outDataNum) + "条。";
		}
		if(outDataNum == 0){
			json.setSuccess(false);
			msg = "订单生成失败，请联系系统管理远！";
		}
		
		return json;
	}
	
	
	/**
	* 月销售统计
	* 
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/

	@Override
	public List<OrderEntity> salesTIC( String userCLid, String productId, String startTime, String endTime ,String companyId) {


		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " AND  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " AND  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}

		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " AND  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " AND  xnc_order.productId = '"+productId+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL =" AND DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL =" AND xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate <='"+endTime+"' ";
		}
		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT  DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" ) as createDate , SUM(totalPrice) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order  INNER JOIN  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(productSQL);
		buf.append(userCLidSQL);
		buf.append(createDateSQL);
		buf.append(" and  xnc_pay_bills_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY   DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" )   ORDER BY  xnc_order.createDate ASC");

		List<OrderEntity>  list = queryListByJdbc(buf.toString(),OrderEntity.class);	
		
		return list;
	}
	
	
	/**
	* 产品销售统计销售(按月统计量  ，金额)
	* 
	* String purchasecompanyid 公司ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesCPBL(String companyId, String userCLid  , String startTime, String endTime) {
		
		
		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " AND  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " AND  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		String createDateSQL ="";//日期
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL =" AND DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL =" AND xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate <='"+endTime+"' ";
		}

		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT    productname ,  SUM(totalPrice) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(userCLidSQL);
		buf.append(createDateSQL);
		buf.append(" AND  xnc_pay_bills_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY  xnc_order.productname  ORDER BY Price   DESC");
		
		List<OrderEntity>  list = queryListByJdbc(buf.toString(), OrderEntity.class);	
		
		
		
		return list;
	}
	
	/**
	* 产品销售统计销售(按月统计量  ，金额)
	* 
	* String purchasecompanyid 公司ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<Map<String, Object>> salesCPBL_new(String companyId, String userCLid  , String startTime, String endTime) {
		Map<String, Object> mapPriceC = null;
		String productId = "";
		BigDecimal Number = null;
		BigDecimal Price = null;
		BigDecimal priceC = null;
		BigDecimal costPrice = null;
		BigDecimal grossProfit = null;
		Integer numberSum = 0;
		BigDecimal priceSum = new BigDecimal("0.00");
		BigDecimal grossProfitSum = new BigDecimal("0.00");
		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		String createDateSQL ="";//日期
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT    productId, productname ,  SUM(totalPrice) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(userCLidSQL);
		buf.append(createDateSQL);
		buf.append(" AND  xnc_pay_bills_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY    xnc_order.productname, xnc_order.productId  ORDER BY Price   DESC");
		
		List<Map<String, Object>>  list = findForJdbc(buf.toString(), null);
		for(Map<String , Object> m : list){
			productId = m.get("productId")+"";
			Number = new BigDecimal(m.get("Number")+"");
			Price = new BigDecimal(m.get("Price")+"");//总价
			String Sql = "SELECT priceC from xnc_product where id='" + productId + "'";
			mapPriceC = findOneForJdbc(Sql,null);
			priceC = new BigDecimal(mapPriceC.get("priceC")+"");//进货价格
			costPrice = priceC.multiply(Number);//总成本
			grossProfit = Price.subtract(costPrice);//毛利润
			//System.out.println(productId+"#总价:"+Price+"#数量:"+Number+"#成本价:"+priceC+"#总成本:"+costPrice+"#毛利润:" + grossProfit);
			//System.out.println("##################################");
			m.put("costPrice", costPrice);
			m.put("grossProfit", grossProfit);
			numberSum = numberSum + Integer.parseInt(m.get("Number")+"");
			priceSum = priceSum.add(Price);
			grossProfitSum = grossProfitSum.add(grossProfit);
		}
		for(Map<String , Object> m : list){
			m.put("numberSum", numberSum);
			m.put("priceSum", priceSum);
			m.put("grossProfitSum", grossProfitSum);
		}
		return list;
	}
	
	
	
	/**
	* 渠道销售统计销售情况
	* 
	* String productId 商品id
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesQDXS(String  companyId , String productId, String startTime, String endTime) {
		
		
		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " AND  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " AND  xnc_order.productId = '"+productId+"' ";
		}

		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  AND DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  AND xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  AND xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate <='"+endTime+"' ";
		}
		
		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT     xnc_pay_bills_order.clientId as id ,  SUM(totalPrice) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(productSQL);
		buf.append(createDateSQL);
		buf.append(" and  xnc_pay_bills_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY     xnc_pay_bills_order.clientId    ORDER BY totalPrice   DESC");
		
		List<OrderEntity>  list = queryListByJdbc(buf.toString(),OrderEntity.class);		
		return list;
	}
	
	/**
	* 采购商数据统计
	* 
	* String productId 商品id
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesCGXTJ(String  companyId , String userid, String startTime, String endTime) {
		
		
		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(userid) && userid.indexOf(",") > 0){
			productSQL = " AND  xnc_order.clientId in("+userid+")    ";
		}else if(StringUtil.isNotEmpty(userid) ){
			productSQL = " AND  xnc_order.clientId = '"+userid+"' ";
		}

		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  AND DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  AND xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  AND xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate <='"+endTime+"' ";
		}
		
		StringBuffer sqlBuf = new StringBuffer();
		sqlBuf.append(" SELECT  productName, totalPrice, Number, freightPic, xnc_order.createDate as createdate");
		sqlBuf.append(" FROM xnc_order  INNER JOIN  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id ");
		sqlBuf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		sqlBuf.append(productSQL);
		sqlBuf.append(createDateSQL);
		sqlBuf.append(" AND  xnc_pay_bills_order.company = ");
		sqlBuf.append("'").append(companyId).append("'");
		sqlBuf.append(" ORDER BY totalPrice   DESC");
		
		List<OrderEntity>  list = queryListByJdbc(sqlBuf.toString(),OrderEntity.class);		
		return list;
	}
	
	/**
	* 采购商数据统计,供图表使用
	* 
	* String productId 商品id
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/
	public List<OrderEntity> salesCGXTJCharts(String  companyId , String userid, String startTime, String endTime) {
		
		
		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(userid) && userid.indexOf(",") > 0){
			productSQL = " AND  xnc_order.clientId in("+userid+")    ";
		}else if(StringUtil.isNotEmpty(userid) ){
			productSQL = " AND  xnc_order.clientId = '"+userid+"' ";
		}

		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL =" AND DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL =" AND xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" AND xnc_order.createDate <='"+endTime+"' ";
		}
		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT  productName, SUM(totalPrice) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(productSQL);
		buf.append(createDateSQL);
		buf.append(" AND  xnc_pay_bills_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY  productname  ORDER BY totalPrice DESC");
		//System.out.println("#salesCGXTJCharts："+Sql);
		List<OrderEntity>  list = queryListByJdbc(buf.toString(), OrderEntity.class);		
		return list;
	}

	/**
	* 用户消费区域分析
	* 
	* String userCLid 采购商id
	* String province 省份中文名称
	* String startTime 开始时间
	* String endTime  截至时间
	*/
	public List<OrderEntity> salesYH( String companyId ,String userCLid ,String province, String startTime, String endTime) {
		
		
		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		
		String provinceSQL ="";//省份
		if(StringUtil.isNotEmpty(province) ){
			provinceSQL = " and  xnc_order.province = '"+province+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT  province , SUM(totalPrice) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order  inner join  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(provinceSQL);
		buf.append(userCLidSQL);
		buf.append(createDateSQL);
		buf.append(" AND  xnc_pay_bills_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY  province  ORDER BY totalPrice   DESC");
		
		List<OrderEntity>  list = queryListByJdbc(buf.toString(),OrderEntity.class);	
		
		return list;
	}

	/**
	* 业务情况统计
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/

	@Override
	public List<OrderEntity> salesYewu( String yewuid, String productId, String startTime, String endTime ,String companyId) {


		String userCLidSQL ="";//业务员
		if(StringUtil.isNotEmpty(yewuid) && yewuid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_order.yewu in("+yewuid+") ";
		}else if(StringUtil.isNotEmpty(yewuid) ){
			userCLidSQL = " and  xnc_order.yewu = '"+yewuid+"'   ";
		}

		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.createDate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' and xnc_order.createDate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.createDate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.createDate <='"+endTime+"' ";
		}

		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT  DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" ) as createDate , SUM(totalPrice) as Price  , SUM(priceyw) as priceyw  , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(productSQL);
		buf.append(userCLidSQL);
		buf.append(createDateSQL);
		buf.append(" AND  xnc_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY   DATE_FORMAT( xnc_order.createDate, \"%Y-%m-%d\" )   ORDER BY  xnc_order.createDate ASC");

		List<OrderEntity>  list = queryListByJdbc(buf.toString(), OrderEntity.class);	
		
		return list;
	}
	
	/**
	* 产品发货状态图
	* 
	* String userCLid 采购商ID
	* String productId 产品ID
	* String startTime 开始时间
	* String endTime  截至时间
	* String state    状态
	*/

	@Override
	public List<OrderEntity> salesdeparture( String companyId ,String userCLid ,String productId, String startTime, String endTime) {
	

		String userCLidSQL ="";//采购客户
		if(StringUtil.isNotEmpty(userCLid) && userCLid.indexOf(",") > 0){
			userCLidSQL = " and  xnc_pay_bills_order.clientId in("+userCLid+") ";
		}else if(StringUtil.isNotEmpty(userCLid) ){
			userCLidSQL = " and  xnc_pay_bills_order.clientId = '"+userCLid+"'   ";
		}
		
		
		String productSQL ="";//产品
		if(StringUtil.isNotEmpty(productId) && productId.indexOf(",") > 0){
			productSQL = " and  xnc_order.productId in("+productId+")    ";
		}else if(StringUtil.isNotEmpty(productId) ){
			productSQL = " and  xnc_order.productId = '"+productId+"' ";
		}
		
		String createDateSQL ="";//产品
		if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime) && startTime.equals(endTime)){
			createDateSQL ="  and DATE_FORMAT(xnc_order.departuredate,'%Y-%m-%d')  ='"+startTime+"' ";
		}else if(StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)){
			createDateSQL ="  and xnc_order.departuredate >='"+startTime+"' and xnc_order.departuredate <='"+endTime+"' ";
		}else if( StringUtil.isNotEmpty(startTime)){
			createDateSQL ="  and xnc_order.departuredate >='"+startTime+"' ";
		}else if( StringUtil.isNotEmpty(endTime)){
			createDateSQL =" and xnc_order.departuredate <='"+endTime+"' ";
		}

		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT  province , SUM(totalPrice) as Price   , SUM(Number) as Number   , SUM(freightPic) as freightPic");
		buf.append(" FROM xnc_order  INNER JOIN  xnc_pay_bills_order  on   xnc_order.billsOrderID = xnc_pay_bills_order.id");
		buf.append(" WHERE  xnc_order.state IN('").append(OrderServiceI.State.UNPAID.getValue()).append("','").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		buf.append(productSQL);
		buf.append(userCLidSQL);
		buf.append(createDateSQL);
		buf.append(" AND  xnc_pay_bills_order.company =");
		buf.append(" '").append(companyId).append("'");
		buf.append(" GROUP BY  province  ORDER BY totalPrice   DESC");
		
		List<OrderEntity>  list = queryListByJdbc(buf.toString(),OrderEntity.class);	
		
		return list;

	}

	@Override
	public StatisPageVO statisCancelOrder(TSCompany company, HttpServletRequest request) {
		
		String state = request.getParameter("state");
		if(StringUtil.isEmpty(state)){
			state = "5,6,7";
		}
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("state", state);
		
		StatisPageVO statisPageVO = statisOrderDetails(company, param);
		
		return statisPageVO;
	}
	
	/**
	 * 订单统计共有函数
	 * @param company
	 * @param param 统计订单参数，请根据需要调整，目前支持的参数：
	 * state : 订单状态  identifieror : 订单号  customername ： 收件人 telephone ： 收件人电话  createdate1,createdate2 ： 时间范围, yewuid : 业务员id
	 * @return
	 */
	private StatisPageVO statisOrderDetails(TSCompany company, HashMap<String, String> param) {
		String freightStateValue = param.get("freightStateValue");
		String identifieror = param.get("identifieror");
		String customername = param.get("customername");
		String productname = param.get("productname");
		String telephone = param.get("telephone");
		String createdate1 = param.get("createdate1");
		String createdate2 = param.get("createdate2");
		String yewuid = param.get("yewuid");//业务员id
		String state = param.get("state");//订单状态
		String clientName = param.get("clientName");//采购商
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.totalPrice),0) value2 ");
		sql.append(" FROM xnc_order a,xnc_pay_bills_order b,t_s_dictionary d, t_s_user c ");
		sql.append(" WHERE a.company = '").append(company.getId()).append("' ");
		sql.append(" AND a.billsOrderID = b.id ");
		sql.append(" AND a.clientId = c.id ");
		sql.append(" AND a.freightState = d.id ");
		sql.append(" AND d.dictionaryType = 'freightStates' ");
		
		if(StringUtil.isNotEmpty(state) &&  state.indexOf(",") > 0 ){
			String stateS[] = state.split(",");
			sql.append(" AND a.state in (");
			for(int i = 0; i < stateS.length; i++){
				if(0 == i){
					sql.append("'").append(stateS[i]).append("'");
				}else{
					sql.append(",'").append(stateS[i]).append("'");
				}
				
			}
			sql.append(")");
		}else if(StringUtil.isNotEmpty(state) &&  state.indexOf(",") == -1){
			sql.append(" AND a.state = '").append(state).append("'") ;
			
		}else{
			sql.append(" AND a.state in ('");
			sql.append(OrderServiceI.State.UNPAID.getValue()).append("','");
			sql.append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("')");
		}
		
		
		
		if(StringUtil.isNotEmpty(freightStateValue) && StringUtil.isNotEmpty(freightStateValue.trim())){
			sql.append(" AND d.dictionaryValue = '").append(freightStateValue).append("' ");
		}
		
		if(StringUtil.isNotEmpty(identifieror) 
				&& StringUtil.isNotEmpty(identifieror.trim())){
			sql.append(" AND a.identifieror like '%" + identifieror.trim() + "%' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(customername) 
				&& StringUtil.isNotEmpty(customername.trim())){
			
			sql.append(" AND a.CustomerName like '%" + customername.trim() + "%' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(telephone) 
				&& StringUtil.isNotEmpty(telephone.trim())){
			sql.append(" AND a.Telephone = '" + telephone.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(yewuid) 
				&& StringUtil.isNotEmpty(yewuid.trim())){
			sql.append(" AND a.yewu = '" + yewuid.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(productname) 
				&& StringUtil.isNotEmpty(productname.trim())){
			sql.append(" AND a.productName like '%" + productname.trim() + "%' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(clientName) 
				&& StringUtil.isNotEmpty(clientName.trim())){
			sql.append(" AND c.realname like '%" + clientName.trim() + "%' ");//采购商
			
		}
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			sql.append(" AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			sql.append(" AND a.createDate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			sql.append(" AND a.createDate <= '" + createdate2 + "'" );
		}
		
		List<StatisPageVO>  list = queryListByJdbc(sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		return statisPageVO;
	}

	/**
	 * 合作伙伴订单统计共有函数
	 * @param company
	 * @param param 统计订单参数，请根据需要调整，目前支持的参数：
	 * state : 订单状态  identifieror : 订单号  customername ： 收件人 telephone ： 收件人电话  createdate1,createdate2 ： 时间范围, yewuid : 业务员id
	 * @return
	 */
	private StatisPageVO statisCopartnerOrderSql(TSCompany company, HashMap<String, String> param) {
		String freightStateValue = param.get("freightStateValue");
		String identifieror = param.get("identifieror");
		String customername = param.get("customername");
		String productname = param.get("productname");
		String telephone = param.get("telephone");
		String createdate1 = param.get("createdate1");
		String createdate2 = param.get("createdate2");
		String yewuid = param.get("yewuid");//业务员id
		String receiverid = param.get("receiverid");//订单受理商
		String state = param.get("state");//订单状态
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.price*a.number),0) value2 FROM xnc_order a,xnc_pay_bills_order b,xnc_order_send c ");
		sql.append("WHERE a.company = '" + company.getId() + "'");
		sql.append(" AND a.id = c.orderid ");
		sql.append(" AND a.state in (");//订单状态
		if(StringUtil.isNotEmpty(state)){
			String[] statelist = state.split(",");
			for (int i = 0; i < statelist.length; i++) {
				if(0==i){
					sql.append("'").append(statelist[i]).append("'");
				} else {
					sql.append(",'").append(statelist[i]).append("'");
				}
			}
		} else {
			
			sql.append("'").append(OrderServiceI.State.UNPAID.getValue()).append("'");
			sql.append(",'").append(OrderServiceI.State.WAIT2RECEIPT.getValue()).append("'");
			sql.append(",'").append(OrderServiceI.State.REBACK.getValue()).append("'");
		}
		sql.append(")");
		sql.append(" AND a.billsOrderID = b.id ");
		sql.append(" AND c.receiver= '" + receiverid + "' ");
		sql.append(" AND c.status in ('1','2') ");
		
		/*if(StringUtil.isNotEmpty(freightState)  && StringUtil.isNotEmpty(freightState.trim())
					&& state.trim().indexOf(",") > 0){
			
			String[] states = state.trim().split(",");
			StringBuffer tmp = new StringBuffer();
			
			for (int i = 0; i < states.length; i++) {
				if(tmp.length() == 0){
					tmp.append("'");
					tmp.append(states[i]);
					tmp.append("'");
				}else{
					tmp.append(",'");
					tmp.append(states[i]);
					tmp.append("'");
				}
			}
			Sql.append(" AND a.state in (" + tmp.toString() + ")");//全部
			
		}*/
		if(StringUtil.isNotEmpty(freightStateValue)  && StringUtil.isNotEmpty(freightStateValue.trim())){
			
			TSDictionary freightStateId = dictionaryService.checkDictItemWithoutCompany("freightStates", freightStateValue);
			sql.append(" AND a.freightState = '" + freightStateId.getId() + "' ");//物流状态
			
		}
		if(StringUtil.isNotEmpty(identifieror) 
				&& StringUtil.isNotEmpty(identifieror.trim())){
			
			sql.append(" AND a.identifieror like '%" + identifieror.trim() + "%' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(customername) 
				&& StringUtil.isNotEmpty(customername.trim())){
			
			sql.append(" AND a.CustomerName = '" + customername.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(telephone) 
				&& StringUtil.isNotEmpty(telephone.trim())){
			
			sql.append(" AND a.Telephone = '" + telephone.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(yewuid) 
				&& StringUtil.isNotEmpty(yewuid.trim())){
			
			sql.append(" AND a.yewu = '" + yewuid.trim() + "' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(productname) 
				&& StringUtil.isNotEmpty(productname.trim())){
			
			sql.append(" AND a.productName like '%" + productname.trim() + "%' ");//具体订单
			
		}
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			sql.append(" AND a.createDate BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			sql.append(" AND a.createDate >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			sql.append(" AND a.createDate <= '" + createdate2 + "'" );
		}
		
		List<StatisPageVO>  list = queryListByJdbc(sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		return statisPageVO;
	}

	@Override
	public StatisPageVO statisOrderByFreightState(TSCompany company, String orderFreightState) {
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("freightStateValue", orderFreightState);
		
		StatisPageVO statisPageVO = statisOrderDetails(company, param);
		return statisPageVO;
	}

	@Override
	public StatisPageVO statisOrderByRequest(TSCompany company, HttpServletRequest request) {
		String freightStateValue = ResourceUtil.getParameter("freightStateValue");
		String identifieror = request.getParameter("identifieror");
		String customername = request.getParameter("customername");
		String productname = request.getParameter("productname");
		String telephone = request.getParameter("telephone");
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		String yewuid = request.getParameter("yewuid");//业务员id
		String state = request.getParameter("state");
		String clientName = request.getParameter("clientid.realname");
		
		
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("freightStateValue", freightStateValue);
		param.put("identifieror", identifieror);
		param.put("customername", customername);
		param.put("productname", productname);
		param.put("telephone", telephone);
		param.put("createdate1", createdate1);
		param.put("createdate2", createdate2);
		param.put("yewuid", yewuid);
		param.put("state", state);
		param.put("clientName", clientName);
		
		StatisPageVO statisPageVO = statisOrderDetails(company, param);
		return statisPageVO;
	}

	@Override
	public StatisPageVO statisCopartnerOrderByFreightState(TSCompany company, String orderFreightState, TSUser receiver) {
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("freightStateValue", orderFreightState);
		param.put("receiverid", receiver.getId());
		
		StatisPageVO statisPageVO = statisCopartnerOrderSql(company, param);
		return statisPageVO;
	}
	
	@Override
	public StatisPageVO statisCopartnerOrders(TSCompany company, HttpServletRequest request, TSUser receiver) {
		String freightStateValue = ResourceUtil.getParameter("freightStateValue");
		String identifieror = request.getParameter("identifieror");
		String customername = request.getParameter("customername");
		String telephone = request.getParameter("telephone");
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		String yewuid = request.getParameter("yewuid");//业务员id
		String state = request.getParameter("state");//业务员id
		
		
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("freightStateValue", freightStateValue);
		param.put("identifieror", identifieror);
		param.put("customername", customername);
		param.put("telephone", telephone);
		param.put("createdate1", createdate1);
		param.put("createdate2", createdate2);
		param.put("yewuid", yewuid);
		param.put("receiverid", receiver.getId());
		param.put("state", state);
		
		StatisPageVO statisPageVO = statisCopartnerOrderSql(company, param);
		return statisPageVO;
	}
	
	
	public List<OrderEntity> getOrderListByIds(String orderIds){
		List<OrderEntity> orderList = new ArrayList<OrderEntity>();
		
		if(StringUtil.isNotEmpty(orderIds)){
			String[] orderIdList = orderIds.split(",");
			
			CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
			cq.in("state", new String[]{OrderServiceI.State.UNPAID.getValue(),OrderServiceI.State.WAIT2RECEIPT.getValue(),OrderServiceI.State.EVALUATE.getValue()});
			cq.setCurPage(1);
			cq.setPageSize(30);
			cq.in("id", orderIdList);
			cq.add();
			
			orderList = getListByCriteriaQuery(cq, false);
		}
		
		
		return orderList;
	}



	@Override
	public AjaxJson orderSave(OrderEntity order, HttpServletRequest request) {
		String param = request.getAttribute("param") + "";
		AjaxJson j = new AjaxJson();
		String message = "";
		boolean success = true;
		if (StringUtil.isNotEmpty(order.getId())) {
			message = "订单明细更新成功";
			OrderEntity t = systemService.get(OrderEntity.class, order.getId());
			
			//初始化order，兼容触屏版的一项逻辑流程
			if(param!=null && param.equals("m")){
				if(t == null){
					j.setMsg("商品订单号有误");
					j.setSuccess(false);
					return j;
				}
				order.setAddress(t.getAddress());
				order.setClientid(t.getClientid());
				order.setIdentifieror(t.getIdentifieror());
				order.setLogisticscompany(t.getLogisticscompany());
				order.setLogisticsnumber(t.getLogisticsnumber());
				if(order.getPrice() == null){
					j.setMsg("价格有误");
					j.setSuccess(false);
					return j;
				}
				if(order.getFreightpic() == null){
					j.setMsg("运费有误");
					j.setSuccess(false);
					return j;
				}
				if(order.getNumber() == null){
					j.setMsg("数量有误");
					j.setSuccess(false);
					return j;
				}
				order.setProductname(t.getProductname());
				order.setRemarks(t.getRemarks());
				order.setState(t.getState());
				order.setTelephone(t.getTelephone());
			}
			
			PayBillsOrderEntity billsorderid = t.getBillsorderid();
			
			//只有大单未支付的情况（包括先货后款）才可以修改订单价格
			
			if( ("1".equals(t.getState()) || OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(t.getFreightState().getDictionaryValue())) && 
					"1".equals(billsorderid.getState()) ){
				String useraddress = request.getParameter("useraddress");
				if(StringUtil.isNotEmpty(useraddress)){
					order.setAddress(useraddress +"_"+ order.getAddress());
				}
				ProductEntity p = systemService.findUniqueByProperty (ProductEntity.class,"id" ,t.getProductid().getId());
				if( (StringUtil.isNotEmpty(order.getAddress()) && !order.getAddress().equals(t.getAddress()))
						|| order.getNumber().compareTo(t.getNumber()) !=0 ){
					//重新计算商品运费
					/**
					 * 通过比较页面传回的运费和原运费，判断运费是否人工干涉
					 * 1、如果页面传回的运费不等于元订单运费，则说明人工干涉，此时采用页面传回的运费
					 * 2、如果页面传回的运费和元订单运费相同，则说明没有人工干涉，此时重新计算系统运费
					 */
					if( order.getFreightpic().compareTo(t.getFreightpic()) ==0 ){
						BigDecimal freightMoney = new BigDecimal("0.00"); // 运费价格
						try {
							freightMoney = freightDetailsService.getProductFreightByFullAddr(p, order.getAddress(), order.getNumber());
							
						} catch (Exception ex) {
							freightMoney = new BigDecimal("0.00");
						}
						order.setFreightpic(freightMoney);//写入新的运费信息	
					}
				}
				//如果 发生价格变化 ，按输入的标准更新  计算修改后的价格，与原价格差，更新支付单
				BigDecimal picMoneyO = new BigDecimal(t.getPrice().add(t.getFreightpic()).doubleValue()); // 运费价格
				//页面传回订单价格和系统订单价格相同，说明人工并未干预商品价格
				if(order.getPrice().compareTo(t.getPrice()) ==0 && order.getNumber().compareTo(t.getNumber()) != 0){
					//计算用户等级价格
					TSUser client = systemService.findUniqueByProperty(TSUser.class, "id", order.getClientid().getId());
					BigDecimal productprice = userGradePriceService.getProductFinalPrice(p, client);
					order.setPrice( productprice.multiply(new BigDecimal(order.getNumber())));
				}
				//重新计算订单价格
				BigDecimal picMoneyN = new BigDecimal( order.getPrice().add(order.getFreightpic()).doubleValue() );
				
				if( picMoneyO.compareTo(picMoneyN) !=0 ){ //存在价格差
					//读取支付单修改支付价格
					if( picMoneyN.compareTo(picMoneyO) == 1){ //金额价格变大
						//变化金额
						BigDecimal picMoneyB = picMoneyN.subtract(picMoneyO);
						  //payablemoney   应付款 增加 
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().add(picMoneyB)) ;//
						  //obligationsmoney 待付金额 增加
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().add(picMoneyB));//
						
					}else if(  picMoneyN.compareTo(picMoneyO) == -1 ){//金额价格减小
						  //payablemoney   应付款 减少
						BigDecimal picMoneyB = picMoneyO.subtract(picMoneyN);
						billsorderid.setPayablemoney(billsorderid.getPayablemoney().subtract(picMoneyB)) ;//
						  //obligationsmoney 待付金额  减少
						billsorderid.setObligationsmoney(billsorderid.getObligationsmoney().subtract(picMoneyB));//
					}
					systemService.saveOrUpdate(billsorderid);
				}
			}
			
			
			
			try {
				
				boolean messageSend = false ;
				if( OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(t.getFreightState().getDictionaryValue()) &&
						StringUtil.isNotEmpty(order.getLogisticscompany()) &&
						StringUtil.isNotEmpty(order.getLogisticsnumber() )  ){//发货信息，状态为待发货   同时填写物流公司  物流单号   自动修改为  发货状态
					
					AjaxJson result = verifyOrderService.checkVerifyByOrderLogistics(t);
					if(!result.isSuccess()){
						return result;
					}
					order.setFreightState(dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.WAIT_PACK.getValue()));
					order.setDeparturedate(DateUtils.getDate());
					
					messageSend = true;
				}
				
				if(messageSend  
						|| (StringUtil.isEmpty(t.getLogisticsnumber()) && StringUtil.isNotEmpty(order.getLogisticsnumber())) 
						|| (StringUtil.isNotEmpty(t.getLogisticsnumber()) && StringUtil.isNotEmpty(order.getLogisticsnumber()) && !t.getLogisticsnumber().equals(order.getLogisticsnumber())) ){
					//如果已经存在的信息则删除全部,重新录入
					List<OrderLogisticsEntity> orderLogisticsListDel = null ;
					try {
						orderLogisticsListDel = t.getOrderLogisticsList();
					} catch (Exception e) {
						
					}
					if( orderLogisticsListDel != null && orderLogisticsListDel.size() > 0){
						t.setOrderLogisticsList(null);
						for( OrderLogisticsEntity delol :  orderLogisticsListDel ){
							
							systemService.delete(delol);
						}
					}
				    String logisticsnumberS[] = order.getLogisticsnumber().trim().split(",");
				    List<OrderLogisticsEntity> orderLogisticsList = new ArrayList<OrderLogisticsEntity>();
					if( logisticsnumberS.length > 0 ){
						for(int i=0 ;i <logisticsnumberS.length ; i++){
							OrderLogisticsEntity ol = new OrderLogisticsEntity();
							ol.setOrderid(t);
							ol.setLogisticscompany(order.getLogisticscompany());
							ol.setLogisticsnumber(logisticsnumberS[i]);
							ol.setDeparturedate(order.getDeparturedate());
						 orderLogisticsList.add(ol);
						}
						if(orderLogisticsList.size() > 0){
							systemService.batchSave(orderLogisticsList);
						}
					}
					
				}
				//更新小单
				MyBeanUtils.copyBeanNotNull2Bean(order, t);
				systemService.saveOrUpdate(t);
				
				if(messageSend){
					billsorderid.setFreightState(payBillsOrderService.getPayBillsFreightStatus(billsorderid));
					systemService.saveOrUpdate(billsorderid);
					//发货通知下单客户
					messageTemplateService.mosSendSmsDelivery_6(t);
					//发货通知收货人
					messageTemplateService.mosSendSmsDelivery_5(t);
					salesmanCommissionService.SalesmanCommissionSave(t);
				}
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				success = false;
				message = "订单明细更新异常";
			}
		}else{
			j.setMsg("参数有误");
			j.setSuccess(false);
			return j;
		} 
//		else {
//			message = "订单明细添加成功";
//			systemService.save(order);
//			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
//		}
		j.setSuccess(success);
		j.setMsg(message);
		return j;
	}



	/**
	 * 动态添加字典：物流状态
	 */
	public void addFreightState() {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dictionaryType", "freightStates");
		params.put("isdel", "N");
		//待打包
		params.put("dictionaryName", OrderServiceI.FreightState.WAIT_PACK.getName());
		params.put("dictionaryValue", OrderServiceI.FreightState.WAIT_PACK.getValue());
		TSDictionary dictionary = this.findFreightState(params);
		if(dictionary == null){
			TSDictionary waitPack = new TSDictionary();
			waitPack.setCompany(user.getCompany());
			waitPack.setDictionaryType("freightStates");
			waitPack.setDictionaryName(OrderServiceI.FreightState.WAIT_PACK.getName());
			waitPack.setDictionaryDesc("物流状态");
			waitPack.setDictionaryValue(OrderServiceI.FreightState.WAIT_PACK.getValue());
			waitPack.setCreateTime(DateUtils.getDate());
			waitPack.setUpdateTime(DateUtils.getDate());
			waitPack.setIsdel("N");
			dictionaryService.save(waitPack);
		}
		//待发货
		params.put("dictionaryName", OrderServiceI.FreightState.WAIT_DELIVER.getName());
		params.put("dictionaryValue", OrderServiceI.FreightState.WAIT_DELIVER.getValue());
		dictionary = this.findFreightState(params);
		if(dictionary == null){
			TSDictionary waitDeliver = new TSDictionary();
			waitDeliver.setCompany(user.getCompany());
			waitDeliver.setDictionaryType("freightStates");
			waitDeliver.setDictionaryName(OrderServiceI.FreightState.WAIT_DELIVER.getName());
			waitDeliver.setDictionaryDesc("物流状态");
			waitDeliver.setDictionaryValue(OrderServiceI.FreightState.WAIT_DELIVER.getValue());
			waitDeliver.setCreateTime(DateUtils.getDate());
			waitDeliver.setUpdateTime(DateUtils.getDate());
			waitDeliver.setIsdel("N");
			dictionaryService.save(waitDeliver);
		}
		//已发货
		params.put("dictionaryName", OrderServiceI.FreightState.ALREADY_DELIVER.getName());
		params.put("dictionaryValue", OrderServiceI.FreightState.ALREADY_DELIVER.getValue());
		dictionary = this.findFreightState(params);
		if(dictionary == null){
			TSDictionary alreadyDeliver = new TSDictionary();
			alreadyDeliver.setCompany(user.getCompany());
			alreadyDeliver.setDictionaryType("freightStates");
			alreadyDeliver.setDictionaryName(OrderServiceI.FreightState.ALREADY_DELIVER.getName());
			alreadyDeliver.setDictionaryDesc("物流状态");
			alreadyDeliver.setDictionaryValue(OrderServiceI.FreightState.ALREADY_DELIVER.getValue());
			alreadyDeliver.setCreateTime(DateUtils.getDate());
			alreadyDeliver.setUpdateTime(DateUtils.getDate());
			alreadyDeliver.setIsdel("N");
			dictionaryService.save(alreadyDeliver);
		}
		//已签收
		params.put("dictionaryName", OrderServiceI.FreightState.ALREADY_SIGN.getName());
		params.put("dictionaryValue", OrderServiceI.FreightState.ALREADY_SIGN.getValue());
		dictionary = this.findFreightState(params);
		if(dictionary == null){
			TSDictionary alreadySign = new TSDictionary();
			alreadySign.setCompany(user.getCompany());
			alreadySign.setDictionaryType("freightStates");
			alreadySign.setDictionaryName(OrderServiceI.FreightState.ALREADY_SIGN.getName());
			alreadySign.setDictionaryDesc("物流状态");
			alreadySign.setDictionaryValue(OrderServiceI.FreightState.ALREADY_SIGN.getValue());
			alreadySign.setCreateTime(DateUtils.getDate());
			alreadySign.setUpdateTime(DateUtils.getDate());
			alreadySign.setIsdel("N");
			dictionaryService.save(alreadySign);
		}
		//部分发货
		params.put("dictionaryName", OrderServiceI.FreightState.PART_DELIVER.getName());
		params.put("dictionaryValue", OrderServiceI.FreightState.PART_DELIVER.getValue());
		dictionary = this.findFreightState(params);
		if(dictionary == null){
			TSDictionary partDeliver = new TSDictionary();
			partDeliver.setCompany(user.getCompany());
			partDeliver.setDictionaryType("freightStates");
			partDeliver.setDictionaryName(OrderServiceI.FreightState.PART_DELIVER.getName());
			partDeliver.setDictionaryDesc("物流状态");
			partDeliver.setDictionaryValue(OrderServiceI.FreightState.PART_DELIVER.getValue());
			partDeliver.setCreateTime(DateUtils.getDate());
			partDeliver.setUpdateTime(DateUtils.getDate());
			partDeliver.setIsdel("N");
			dictionaryService.save(partDeliver);
		}
	}
	/**
	 * 检查字典：物流状态
	 */
	public List<TSDictionary> checkFreightState(Map<String, Object> params) {
		TSUser user = ResourceUtil.getSessionUserName();
		if(params == null){
			params = new HashMap<String,Object>();
		}
		params.put("dictionaryType", "freightStates");
		//params.put("company", user.getCompany());
		params.put("isdel", "N");
		List<TSDictionary> list = dictionaryService.findListByParams(params);
		if(list == null || list.size() <= 0){
			addFreightState();
			dictionaryService.findListByParams(params);
		}
		return list;
	}

	/**
	 * 获取物流状态
	 * @param params
	 * @return
	 */
	public TSDictionary findFreightState(Map<String, Object> params) {
		List<TSDictionary> list = checkFreightState(params);
		return list.get(0);
	}



	@Override
	public List<OrderEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
		if(StringUtil.isNotEmpty(params.get("company"))){
			cq.eq("company", params.get("company"));
		}
		
		if(StringUtil.isNotEmpty(params.get("logisticsnumber"))){
			cq.add(Restrictions.like("logisticsnumber", "%"+params.get("logisticsnumber")+"%" ));
		}
		
		if(StringUtil.isNotEmpty(params.get("identifieror"))){
			cq.like("identifieror", "%"+params.get("identifieror")+"%");
		}
		
		cq.add();
		List<OrderEntity> list = this.getListByCriteriaQuery(cq, false);
		return list;
	}



	@Override
	public Map<String, Object> getParams(OrderEntity orderEntity) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = BeanToMapUtils.convertBeanToMap(orderEntity);
		params.put("company", user.getCompany());
		return params;
	}

	@Override
	public boolean order2Received(OrderEntity order) {
		order = findUniqueByProperty(OrderEntity.class, "id", order.getId());
		if(OrderServiceI.State.UNPAID.getValue().equals(order.getState()) 
				|| OrderServiceI.State.WAIT2RECEIPT.getValue().equals(order.getState())){
			
			TSDictionary dict = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.ALREADY_SIGN.getValue());
			if(OrderServiceI.FreightState.ALREADY_DELIVER.getValue().equals(order.getFreightState().getDictionaryValue())){
				order.setFreightState(dict);
				updateEntitie(order);
			}
		}
		return true;
	}

	@Override
	public boolean changeOrderFreightState(OrderEntity order, TSDictionary freightState) {
		order = findUniqueByProperty(OrderEntity.class, "id", order.getId());
		if(OrderServiceI.State.UNPAID.getValue().equals(order.getState()) 
				|| OrderServiceI.State.WAIT2RECEIPT.getValue().equals(order.getState())){
			
			order.setFreightState(freightState);
			
			updateEntitie(order);
		}
		return true;
	}
	
	@Override
	public String batchUpdateOrder2Received(String orderids){
		StringBuffer buf = new StringBuffer();
		int index = 0,total = 0;
		if(StringUtil.isNotEmpty(orderids)){
			String[] idArray = orderids.split("\\|");
			total = idArray.length;
			List<OrderEntity> orderList = new ArrayList<OrderEntity>();
			for (String id : idArray){
				OrderEntity order = findUniqueByProperty(OrderEntity.class, "id",id);
				TSDictionary dict = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.ALREADY_SIGN.getValue());
				if(OrderServiceI.FreightState.ALREADY_DELIVER.getValue().equals(order.getFreightState().getDictionaryValue())){
					order.setFreightState(dict);
					orderList.add(order);
					index++;
				}
			}
			
			if(orderList.size() > 0){
				batchUpdate(orderList);
			}
			
			if(total == index){
				buf.append("订单签收成功");
			}else{
				buf.append("共有").append(total).append("张订单，").append("其中").append(index).append("张签收成功，").append(total-index).append("签收失败");
			}
			
		}else{
			buf.append("请选择需要签收的订单！");
		}
		return buf.toString();
	}



	@Override
	public CriteriaQuery getOrderListCriteriaQry(OrderEntity order, HttpServletRequest request, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		String freightStateValue = ResourceUtil.getParameter("freightStateValue");
		
		CriteriaQuery cq = new CriteriaQuery(OrderEntity.class, dataGrid);
		//查询条件组装器
		if(StringUtil.isNotEmpty(order.getIdentifieror()) ){
			cq.like("identifieror", "%"+order.getIdentifieror()+"%");
			order.setIdentifieror(null);
		}
		if(StringUtil.isNotEmpty(order.getProductname()) ){
			cq.like("productname", "%"+order.getProductname()+"%");
			order.setProductname(null);
		}
		if(StringUtil.isNotEmpty(order.getCustomername()) ){
			cq.like("customername", "%"+order.getCustomername()+"%");
			order.setCustomername(null);
		}
		if(StringUtil.isNotEmpty(order.getTelephone()) ){
			cq.like("telephone", "%"+order.getTelephone()+"%");
			order.setTelephone(null);
		}
		if( StringUtil.isNotEmpty(order.getAddress())){
			cq.like("address", "%"+order.getAddress()+"%");
			order.setAddress(null);
		}
		if(StringUtil.isNotEmpty(freightStateValue) 
				&& freightStateValue.indexOf(",") > 0){
			
			cq.createAlias("freightState", "dictionary");
			cq.eq("dictionary.dictionaryType", "freightStates");
			cq.in("dictionary.dictionaryValue", freightStateValue.split(","));
			
		}else if(StringUtil.isNotEmpty(freightStateValue) 
				&& freightStateValue.indexOf(",") == -1 ){
			
			cq.createAlias("freightState", "dictionary");
			cq.eq("dictionary.dictionaryType", "freightStates");
			cq.eq("dictionary.dictionaryValue", freightStateValue);
		}
		
		if(StringUtil.isNotEmpty(order.getState()) 
				&&  order.getState().indexOf(",") > 0 ){
			String stateS[] = order.getState().split(",");
			cq.in("state", stateS);
			order.setState(null);
			
		}else if(StringUtil.isNotEmpty(order.getState()) 
				&&  order.getState().indexOf(",") == -1 ){
			
			cq.eq("state", order.getState());
			order.setState(null);
		}
		
		
		//业务员只能看自己负责客户的单子
		if("4".equals(user.getType())){
			cq.createAlias("yewu", "yw");
			cq.add( Restrictions.eq("yw.id", user.getId()) );
			order.setYewu(null);
		}
		
		//客户方式所搜
		TSUser client = order.getClientid();
		if(null != client && StringUtil.isNotEmpty(client.getRealname())){
			cq.createAlias("clientid", "client");
			cq.add( Restrictions.like("client.realname", "%"+client.getRealname()+"%") );
			order.setClientid(null);
		}
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat), DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}
		
		
		String orderDate = request.getParameter("orderDate"); 
		
		if(StringUtil.isNotEmpty(orderDate) && "departuredate".equals(orderDate)){
			//发货时间排序
			cq.addOrder("departuredate", SortDirection.desc);
		}else if( StringUtil.isNotEmpty(orderDate) && "paydate".equals(orderDate) ){
			//付款时间排序
			cq.addOrder("paydate", SortDirection.desc);
		}else if( StringUtil.isNotEmpty(orderDate) && "canceldate".equals(orderDate) ){
			//付款时间排序
			cq.addOrder("canceldate", SortDirection.desc);
		}else {
			//下单时间排序
			cq.addOrder("createdate", SortDirection.desc);
		}
		cq.add();
		
		return cq;
	}
	
	@Override
	public List<XiadanProductVO> buildXiadanProductVoByHttpReq(HttpServletRequest request) throws Exception {
		List<XiadanProductVO> xiadanlist = new ArrayList<XiadanProductVO>();
		String expLen = ResourceUtil.getParameter("Len");
		if(StringUtil.isNotEmpty(expLen)){
			
			String sendType = request.getParameter("sendType");
			String provinceid = (null == request.getParameter("provinceId") ? "" : request.getParameter("provinceId"));
			String provinceName = territoryService.jdbcFindTerritoryNameById(provinceid);
			provinceName = (null != provinceName ? provinceName : "");
			
			String cityid = request.getParameter("cityId");
			String cityName = territoryService.jdbcFindTerritoryNameById(cityid);
			cityName = (null != cityName ? cityName : "");
			
			String regionid = request.getParameter("regionId");
			String regionName = territoryService.jdbcFindTerritoryNameById(regionid);
			regionName = (null != regionName ? regionName : "");
			
			cityid = (null == cityid ? "" : "," + cityid);
			regionid = (null == regionid ? "" : "," + regionid);
			
			String detailAddress = request.getParameter("addr");
			String clientName = request.getParameter("name");
			String clientid = request.getParameter("clientid");
			String phone = request.getParameter("telphone");
			String remarks = request.getParameter("remarks");
			String channelid = request.getParameter("channelid");
			OrderChannelEntity orderChannel = null;
			if(StringUtil.isNotEmpty(channelid)){
				orderChannel = findUniqueByProperty(OrderChannelEntity.class, "id", channelid);
			}
			
			
			int xiadanProductNum = Integer.parseInt(expLen);
			for (int i = 0; i < xiadanProductNum; i++) {
				String productid = request.getParameter("id-" + i);
				if(StringUtil.isNotEmpty(productid)){
					XiadanProductVO xiadanvo = new XiadanProductVO();
					xiadanvo.setCode(request.getParameter("productcode-" + i));
					xiadanvo.setAddress(provinceName + cityName + regionName + detailAddress);
					xiadanvo.setCustomername(clientName);
					xiadanvo.setNumber(Integer.parseInt(request.getParameter("orderNum-" + i)));
					xiadanvo.setProductname(request.getParameter("productname-" + i));
					xiadanvo.setRemarks(remarks);
					if (StringUtil.isNotEmpty(sendType) && "1".equals(sendType)) {
						String hasremark = (null == xiadanvo.getRemarks() ? "" : xiadanvo.getRemarks());
						if(StringUtil.isNotEmpty(hasremark)){
							xiadanvo.setRemarks(hasremark + "（此单不走物流，客户自提！）");
						}else{
							xiadanvo.setRemarks( "此单不走物流，客户自提！");
						}
					}
					
					if(StringUtil.isNotEmpty(request.getParameter("orderPrice-" + i))){
						xiadanvo.setPrice(new BigDecimal(request.getParameter("orderPrice-" + i)));
					}
					//设置采购商用户
					if(StringUtil.isNotEmpty(clientid)){
						xiadanvo.setUserBuyerId(clientid);
					}
					if(null != orderChannel){
						xiadanvo.setChannelName(orderChannel.getName());
					}
					/*暂时不给自定业务员，系统根据客户绑定
					 * if(StringUtil.isNotEmpty(salemanid)){
						xiadanvo.setSalemanid(salemanid);
					}*/
					xiadanvo.setTelephone(phone);
					xiadanvo.setTSTerritory(provinceid + cityid + regionid);

					xiadanlist.add(xiadanvo);
				}
			}
		}
		return xiadanlist;
	}
}