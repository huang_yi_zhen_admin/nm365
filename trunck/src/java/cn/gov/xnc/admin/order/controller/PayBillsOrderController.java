package cn.gov.xnc.admin.order.controller;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsFlowServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.LogUtil;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.PrintTemplateUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserService;

/**   
 * @Title: Controller
 * @Description: 订单应付流水
 * @author zero
 * @date 2016-10-02 02:02:04
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/payBillsOrderController")
public class PayBillsOrderController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PayBillsOrderController.class);

	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private UserService userService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private PayBillsOrderServiceI billsOrderService;
	private String message;
	@Autowired
	private PayBillsFlowServiceI payBillsFlowService;
	@Autowired
	private DictionaryService dictionaryService;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 订单应付流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView payBillsOrder(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		CriteriaQuery cq = new CriteriaQuery(TSUser.class);
		cq.eq("type", "3");
		cq.eq("company", company);
		
		if(!"1".equals(user.getType()) && !"2".equals(user.getType())){
			cq.eq("id", user.getId());
		}
		cq.add();
		//获取采购商
		List<TSUser> clientlist = systemService.getListByCriteriaQuery(cq, false);
		request.setAttribute("clientlist", clientlist);
		String state = request.getParameter("state");
		request.setAttribute("state", state);
		return new ModelAndView("admin/order/payBillsOrderList");
	}
	
	/**
	 * 订单应付流水列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "cashlist")
	public ModelAndView payBillsOrderCash(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
//		request.setAttribute("billStatis", payBillsOrderService.statisUserPaybills(company, null, request));
		
		request.setAttribute("accountStatis", userService.staticsUserClientAccountMoney(company, null, request));
		
		CriteriaQuery cq = new CriteriaQuery(TSUser.class);
		cq.eq("type", "3");
		cq.eq("company", company);
		
		if(!"1".equals(user.getType()) && !"2".equals(user.getType())){
			cq.eq("id", user.getId());
		}
		cq.add();
		//获取采购商
		List<TSUser> clientlist = systemService.getListByCriteriaQuery(cq, false);
		request.setAttribute("clientlist", clientlist);
		
		
		return new ModelAndView("admin/order/payBillsOrderCashList");
	}
	
	/**
	 * 用户选择角色跳转页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "printAllExpress")
	public String printAllExpress(PayBillsOrderEntity payBillsOrder,HttpServletRequest request, HttpServletResponse response) {
		
		request.setAttribute("payBillsOrderId", payBillsOrder.getId());
		
		
		
		return "admin/order/printAllExpress";
	}
	
	
	
	@RequestMapping(value = "getPayBillsOrderWaitDeliver")
	@ResponseBody
	public AjaxJson getPayBillsOrderWaitDeliver(  HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		String payBillsOrderId = request.getParameter("payBillsOrderId");
		TSUser user = ResourceUtil.getSessionUserName();
		PayBillsOrderEntity payBillsOrder = new PayBillsOrderEntity();
		payBillsOrder.setId(payBillsOrderId);
		
		//获取订单数据
		CriteriaQuery cq_o = new CriteriaQuery(OrderEntity.class);
		cq_o.eq("billsorderid", payBillsOrder);
		cq_o.eq("company", user.getCompany());
		cq_o.createAlias("freightState", "freight");
		cq_o.add(Restrictions.eq("freight.dictionaryType", "freightStates"));
		cq_o.add(Restrictions.eq("freight.dictionaryVale", OrderServiceI.FreightState.WAIT_DELIVER.getValue()));//只取处于未发货状态状态的子订单
		cq_o.in("state", new String[]{OrderServiceI.State.UNPAID.getValue(), OrderServiceI.State.WAIT2RECEIPT.getValue()});
		List<OrderEntity> orderlist = systemService.getListByCriteriaQuery(cq_o,false);//获取全部商品列表
		
		List<String> orderidList = new ArrayList<String>();
		for( int i = 0; i < orderlist.size(); i++ ){
			orderidList.add(orderlist.get(i).getId());
		}
		
		j.setSuccess(true);
		j.setObj(orderidList);
		
		return j;
	}
	

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(PayBillsOrderEntity payBillsOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class, dataGrid);
		
		String freightstatus = request.getParameter("freightstatus");
		
		//查询条件组装器
		if(StringUtil.isNotEmpty(payBillsOrder.getState()) && "10".equals(payBillsOrder.getState()) ){
			String stateS[] = new String[]{PayBillsOrderServiceI.State.UNPAID.getValue(), PayBillsOrderServiceI.State.NOTALLPAID.getValue()};
			cq.in("state", stateS);
			payBillsOrder.setState(null);
			
		}else if(StringUtil.isNotEmpty(payBillsOrder.getState()) && payBillsOrder.getState().indexOf(",") > 0 ){
			String stateS[] = payBillsOrder.getState().split(",");
			cq.in("state", stateS);
			payBillsOrder.setState(null);
		}else if(StringUtil.isNotEmpty(payBillsOrder.getState())){
			cq.eq("state", payBillsOrder.getState());
			payBillsOrder.setState(null);
		}
		
		if(StringUtil.isNotEmpty(payBillsOrder.getIdentifier()) ){
			cq.like("identifier", "%"+payBillsOrder.getIdentifier()+"%");
			payBillsOrder.setIdentifier(null);
		}
		
		if( (null != payBillsOrder.getYewu() && StringUtil.isNotEmpty(payBillsOrder.getYewu().getRealname())) || "4".equals(user.getType()) ){
			cq.createAlias("yewu", "u");
			if(null != payBillsOrder.getYewu() && StringUtil.isNotEmpty(payBillsOrder.getYewu().getRealname()) ){
				cq.add( Restrictions.like("u.realname", "%"+payBillsOrder.getYewu().getRealname()+"%" ) ) ;
				payBillsOrder.setYewu(null);
			}
			if("4".equals(user.getType())){
				cq.add( Restrictions.eq("u.id", user.getId()) ) ;
			}
		}
		
		TSDictionary freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", freightstatus);
		if(freightState != null){
			cq.eq("freightState", freightState);
		}
		
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat), DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}
		cq.addOrder("updatedate", SortDirection.desc);
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, payBillsOrder, request.getParameterMap());
		this.payBillsOrderService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	@RequestMapping(value = "cashDatagrid")
	public void cashDatagrid(PayBillsOrderEntity payBillsOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		String ordertype = ResourceUtil.getParameter("ordertype");
		CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class, dataGrid);
		//查询条件组装器
		if(StringUtil.isNotEmpty(ordertype) && ordertype.indexOf(",") > -1){
			String stateS[] = ordertype.split(",");
			cq.in("state", stateS);
			payBillsOrder.setState(null);
			
		}else if(StringUtil.isNotEmpty(ordertype) && ordertype.indexOf(",") == -1){
			cq.eq("state", ordertype);
			payBillsOrder.setState(null);
			
		}
		
		if(StringUtil.isNotEmpty(payBillsOrder.getIdentifier()) ){
			cq.like("identifier", "%"+payBillsOrder.getIdentifier()+"%");
			payBillsOrder.setIdentifier(null);
		}
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createdate", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat), DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createdate", DateUtils.str2Date(createdate1,DateUtils.datetimeFormat));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createdate", DateUtils.str2Date(createdate2,DateUtils.datetimeFormat));
		}
		//查询条件组装器
		if(!"1".equals(user.getType()) && !"2".equals(user.getType())){
			cq.eq("clientid", user);
		}
		cq.addOrder("updatedate", SortDirection.desc);
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, payBillsOrder, request.getParameterMap());
		this.payBillsOrderService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

//	/**
//	 * 删除订单应付流水
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "del")
//	@ResponseBody
//	public AjaxJson del(PayBillsOrderEntity payBillsOrder, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		payBillsOrder = systemService.getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
//		message = "订单应付流水删除成功";
//		payBillsOrderService.delete(payBillsOrder);
//		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
//		
//		j.setMsg(message);
//		return j;
//	}


//	/**
//	 * 添加订单应付流水
//	 * 
//	 * @param ids
//	 * @return
//	 */
//	@RequestMapping(value = "save")
//	@ResponseBody
//	public AjaxJson save(PayBillsOrderEntity payBillsOrder, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		if (StringUtil.isNotEmpty(payBillsOrder.getId())) {
//			message = "订单应付流水更新成功";
//			PayBillsOrderEntity t = payBillsOrderService.get(PayBillsOrderEntity.class, payBillsOrder.getId());
//			try {
//				MyBeanUtils.copyBeanNotNull2Bean(payBillsOrder, t);
//				payBillsOrderService.saveOrUpdate(t);
//				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
//			} catch (Exception e) {
//				e.printStackTrace();
//				message = "订单应付流水更新失败";
//			}
//		} else {
//			message = "订单应付流水添加成功";
//			payBillsOrderService.save(payBillsOrder);
//			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
//		}
//		j.setMsg(message);
//		return j;
//	}

	/**
	 * 订单应付流水列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(PayBillsOrderEntity payBillsOrder, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(payBillsOrder.getId())) {
			payBillsOrder = payBillsOrderService.getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
		}
		//定义客户
		try {
			if(StringUtil.isEmpty(payBillsOrder.getClientid().getId())){
				TSUser clientid = new TSUser();
				payBillsOrder.setClientid(clientid);
			}
		} catch (Exception e) {
			TSUser clientid = new TSUser();
			payBillsOrder.setClientid(clientid);
		}
		
		//定义业务员空
		try {
			if(StringUtil.isEmpty(payBillsOrder.getYewu().getId())){
				TSUser yewu = new TSUser();
				payBillsOrder.setYewu(yewu);
			}
		} catch (Exception e) {
			TSUser yewu = new TSUser();
			payBillsOrder.setYewu(yewu);
		}
		req.setAttribute("payBillsOrderPage", payBillsOrder);
		
		return new ModelAndView("admin/order/payBillsOrder");
	}
	
	
	
	/**
	 * 应付款单  页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "paymentBillsOrderList")
	public ModelAndView paymentBillsOrderList(HttpServletRequest request) {
		return new ModelAndView("admin/order/paymentBillsOrderList");
	}
	
	/**
	 * 支付调转
	 * 
	 * @return
	 */
	@RequestMapping(value = "paymentBillsOrder")
	public ModelAndView paymentBillsOrder( PayBillsOrderEntity payBillsOrder, HttpServletRequest req) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		if(   StringUtil.isNotEmpty(payBillsOrder.getId()) ){
			
			String ids[] = payBillsOrder.getId().split(",");
			
			CriteriaQuery cq = new CriteriaQuery(PayBillsOrderEntity.class);
				cq.in("id", ids);//产品编码作为唯一编码
				cq.eq("company", user.getCompany());//公司信息
				cq.add();	
			List<PayBillsOrderEntity> payBillsOrderlist = systemService.getListByCriteriaQuery(cq,false);
			
			String idsPage="";
			String identifierPage="";
			
			BigDecimal moneyPage = new BigDecimal("0.00");
			
			if( payBillsOrderlist != null && payBillsOrderlist.size() > 0 ){
				for( PayBillsOrderEntity p: payBillsOrderlist){
					idsPage +=p.getId()+",";
					identifierPage += p.getIdentifier()+ ",";
					moneyPage =moneyPage.add(p.getObligationsmoney());
				}
			}
			req.setAttribute("idsPage", idsPage);
			req.setAttribute("identifierPage", identifierPage);
			req.setAttribute("moneyPage", moneyPage);

		}
		return new ModelAndView("admin/order/paymentBillsOrder");
	}
	
	
	
	@RequestMapping(value= "statisUserPaybills")
	@ResponseBody
	public AjaxJson statisUserPaybills(HttpServletRequest req){
		TSUser user = ResourceUtil.getSessionUserName();
		String buyerUserId = ResourceUtil.getParameter("buyerUserId");//查询具体的采购商
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg("统计采购商支付流水失败！");
		
		if(StringUtil.isNotEmpty(user.getCompany().getId())){
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			TSUser buyerUser = systemService.findUniqueByProperty(TSUser.class, "id", buyerUserId);
			
			StatisPageVO statispagelist = payBillsOrderService.statisUserPaybills(company, buyerUser,req);
			json.setObj(statispagelist);
			
			json.setSuccess(true);
			json.setMsg("统计采购商支付流水成功！");
			
		}
		
		return json;
	}
	
	/**
	 * 订单支付流水详情页
	 * 
	 * @return
	 */
	@RequestMapping(value = "cashBillsDetail")
	public ModelAndView cashBillsDetail(PayBillsOrderEntity payBillsOrder, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(payBillsOrder.getId())) {
			payBillsOrder = payBillsOrderService.getEntity(PayBillsOrderEntity.class, payBillsOrder.getId());
		}
		//定义客户
		try {
			if(StringUtil.isEmpty(payBillsOrder.getClientid().getId())){
				TSUser clientid = new TSUser();
				payBillsOrder.setClientid(clientid);
			}
		} catch (Exception e) {
			TSUser clientid = new TSUser();
			payBillsOrder.setClientid(clientid);
		}
		
		//定义业务员空
		try {
			if(StringUtil.isEmpty(payBillsOrder.getYewu().getId())){
				TSUser yewu = new TSUser();
				payBillsOrder.setYewu(yewu);
			}
		} catch (Exception e) {
			TSUser yewu = new TSUser();
			payBillsOrder.setYewu(yewu);
		}
		
		req.setAttribute("payBillsOrderPage", payBillsOrder);
		
		return new ModelAndView("admin/order/payBillsOrderCash");
	}
	
	/**
	 * 先货后款列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "sendFirstPayBillsOrderList")
	public ModelAndView sendFirstPayBillsOrderList(HttpServletRequest request) {
		request.setAttribute("paymethod", request.getParameter("paymethod"));
		request.setAttribute("state", request.getParameter("state"));
		
		return new ModelAndView("admin/order/sendFirstPayBillsOrderList");
	}
	
	/**
	 * 先货后款审核
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "sendFirstPaymentAudit")
	@ResponseBody
	public AjaxJson sendFirstPaymentAudit(PayBillsOrderEntity payBillsOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		if(null != payBillsOrder && StringUtil.isNotEmpty(payBillsOrder.getId())){
			PayBillsOrderEntity payBillsOrderEntity = payBillsOrderService.findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrder.getId());
			
			j = payBillsOrderService.sendFirstPaymentAudit(payBillsOrderEntity, request);
			
		}else{
			
			j.setMsg("审核失败，审核单号不能为空！");
			j.setSuccess(false);
		}
		return j;
	}
	
	/**
	 * 批量通过先货后款
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "sendFirstBatch2Pass")
	@ResponseBody
	public AjaxJson sendFirstBatch2Pass(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		j.setMsg("审核全部通过！");
		String ids = request.getParameter("ids");
		int successNum = 0, failNum=0;
		if(StringUtil.isNotEmpty(ids)){
			String[] idList =  ids.split("\\|");
			for (String id : idList) {
				PayBillsOrderEntity payBillsOrderEntity = payBillsOrderService.findUniqueByProperty(PayBillsOrderEntity.class, "id", id);
				
				j = payBillsOrderService.sendFirstPaymentAudit(payBillsOrderEntity, request);
				
				if(j.isSuccess()) successNum++;
			}
			
			failNum = idList.length - successNum;
		}else{
			
			j.setMsg("审核失败，审核单号不能为空！");
			j.setSuccess(false);
		}
		
		if(failNum > 0){
			j.setMsg("共 " + successNum + "成功，" + failNum + "审核失败，请检查库存和订单状态！");
		}
		
		return j;
	}
	
	/**
	 * 取消订单
	 * @param payBillsOrder
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "cancelPayBillsOrder")
	@ResponseBody
	public AjaxJson cancelPayBillsOrder(PayBillsOrderEntity payBillsOrder, HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		try {
			j = payBillsOrderService.cancelPayBillsOrder(payBillsOrder, request);
		
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("取消订单失败，请联系统管理人员!");
			
			systemService.addLog(e.getMessage(), Globals.Log_Leavel_ERROR, Globals.Log_Type_UPDATE);
			
		}
		return j;
	}
	
	/**
	 * 签收订单
	 * @param payBillsOrder
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "billsOrder2Received")
	@ResponseBody
	public AjaxJson billsOrder2Received(PayBillsOrderEntity payBillsOrder, HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		try {
			j = payBillsOrderService.billsOrder2Received(payBillsOrder);
		
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("订单签收失败，请联系统管理人员!");
			
			systemService.addLog(e.getMessage(), Globals.Log_Leavel_ERROR, Globals.Log_Type_UPDATE);
			
		}
		return j;
	}
	
	/**
	 * 打印配货单
	 * */
	@RequestMapping(value= "printBillsOrder")
	@ResponseBody
	public AjaxJson printBillsOrder(PayBillsOrderEntity payBillsOrder, HttpServletRequest req){
		AjaxJson j = new AjaxJson();
		
		TSUser user = ResourceUtil.getSessionUserName();
		payBillsOrder = systemService.findUniqueByProperty(PayBillsOrderEntity.class, "id", payBillsOrder.getId());
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", payBillsOrder.getCompany().getId());
		
		String companName = company.getCompanyplatform().getPlatformname();
		String billsorderno = payBillsOrder.getIdentifier();
		String orderdate = DateUtils.date3Str(payBillsOrder.getCreatedate(),"yyyy-MM-dd HH:mm:ss");
		String paystatus = PayBillsOrderServiceI.State.getNameByVal(payBillsOrder.getState());
		String yewuyuan = "";
		try {
			if( null != payBillsOrder.getYewu() && StringUtil.isNotEmpty(payBillsOrder.getYewu().getId()) ){
				TSUser saleman = payBillsOrder.getYewu();
				yewuyuan =  saleman.getUserName();
			}
		} catch (Exception e) {
			LogUtil.error("----获取业务员失败：" + e.getMessage());
		}
		//查找订单详细
		CriteriaQuery co = new CriteriaQuery(OrderEntity.class);
		co.eq("billsorderid.id", payBillsOrder.getId());
		co.notEq("state", OrderServiceI.State.CANCEL.getValue());
		co.notEq("state", OrderServiceI.State.REBACK.getValue());
		co.add();
		co.addOrder("createdate", SortDirection.desc);
		List<OrderEntity> orderlist = systemService.getListByCriteriaQuery(co, false);
		if( null == orderlist || orderlist.size() == 0){
			j.setSuccess(false);
			j.setMsg("无效订单编号");
			return j;
		}
		
		BigDecimal totalOrderNum = new BigDecimal("0");
		//获取订单采购总量
		for (OrderEntity orderEntity : orderlist) {
			totalOrderNum = totalOrderNum.add(new BigDecimal(orderEntity.getNumber()));
		}
		
		//根据大订单id获取order列表，根据order数量确定要打印的页数，每页最多承载26个order
		int pagesize = 26;
		int pagenum = orderlist.size()/pagesize + 1;
		
		int index = 0;//商品订单序号
		List<String> printBufList = new ArrayList<String>();
		
		for( int i = 0; i < pagenum; i++ ){
			StringBuffer tmp = PrintTemplateUtil.getInstance().getTemplate(PrintTemplateUtil.TEMPLATE_SALESLIST);//取出模板
			String tmplate = tmp.toString();
			tmplate = tmplate.replace("{companyname}", companName);
			tmplate = tmplate.replace("{paybillsorderid}", billsorderno);
			tmplate = tmplate.replace("{orderdate}", orderdate);
			tmplate = tmplate.replace("{paystatus}", paystatus);
			tmplate = tmplate.replace("{saleman}", yewuyuan);
			tmplate = tmplate.replace("{printuser}", user.getRealname());
			
			StringBuffer obuf = new StringBuffer();
			BigDecimal pageOrderMoney = new BigDecimal(0.00);
			BigDecimal pageOrderNum = new BigDecimal("0");
			for( int k = 0 ; k < pagesize; k ++, index++ ){
				
				if( index >= orderlist.size() ){
					break;
				}
				
				OrderEntity order = orderlist.get(index);
				ProductEntity product = order.getProductid();
				
				obuf.append("<tr>");
				obuf.append("<td style=\"text-align: center;\">").append(k+1).append("</td>");
				obuf.append("<td style=\"text-align: center;\">").append(product.getCode()).append("</td>");
				obuf.append("<td style=\"text-align: center;\">").append(product.getName()).append("</td>");
				obuf.append("<td style=\"text-align: center;\">").append(product.getSpecifications()).append("</td>");
				obuf.append("<td style=\"text-align: center;\">").append(product.getUnit().getName()).append("</td>");
				obuf.append("<td style=\"text-align: center;\">").append(order.getNumber()).append("</td>");
				obuf.append("<td style=\"text-align: center;\">").append(order.getPrice()).append("</td>");
				obuf.append("<td style=\"text-align: center;\">").append(order.getTotalPrice()).append("</td>");
				obuf.append("<td style=\"text-align: left;\">").append( order.getRemarks() ).append("</td>");
				obuf.append("</tr>");
				
				//每页金额和订单量小计
				pageOrderMoney = pageOrderMoney.add(order.getTotalPrice());
				pageOrderNum = pageOrderNum.add(new BigDecimal(order.getNumber()));
				
			}
			tmplate = tmplate.replace("{orderinfo}", obuf.toString());
			tmplate = tmplate.replace("{pageordernum}", pageOrderNum.toString());
			tmplate = tmplate.replace("{pagemoney}", pageOrderMoney.toString());
			tmplate = tmplate.replace("{billsordernum}", totalOrderNum.toString());
			tmplate = tmplate.replace("{billsordermoney}", payBillsOrder.getPayablemoney().toString());
			
			printBufList.add(tmplate);
		}
		
		systemService.opLog(req, OperationLogUtil.PAYBILLSORDER_PRINTPAYBILLS, payBillsOrder, "打印销售单："+payBillsOrder.getId() );

		j.setSuccess(true);
		j.setMsg("成功");
		j.setObj(printBufList);
		
		
		return j;
	}
}
