package cn.gov.xnc.admin.order.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.order.entity.OrderRebackEntity;
import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.service.OrderSendServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackDetailEntity;
import cn.gov.xnc.admin.order.service.OrderRebackDetailServiceI;
import cn.gov.xnc.admin.order.service.OrderRebackServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;

@Service("orderRebackDetailService")
@Transactional
public class OrderRebackDetailServiceImpl extends CommonServiceImpl implements OrderRebackDetailServiceI {
	
	@Autowired
	private OrderSendServiceI orderSendService;
	
	@Override
	public List<OrderRebackDetailEntity> batchSaveCancelDetails(OrderRebackEntity cancelEntiy, HttpServletRequest req) throws Exception {
		
		List<OrderRebackDetailEntity> orderCancelDetailListAdd = new ArrayList<OrderRebackDetailEntity>();
		
		String expLen = ResourceUtil.getParameter("expLen");
		if(StringUtil.isNotEmpty(expLen)){
			
			int orderCancelDetailsNum = Integer.parseInt(expLen);
			for (int i = 0; i < orderCancelDetailsNum; i++) {
				
				OrderEntity orderEntity = findUniqueByProperty(OrderEntity.class, "id", req.getParameter("id-" + i));
				OrderRebackDetailEntity cancelDetail = new OrderRebackDetailEntity();
				cancelDetail.setCancelid(cancelEntiy);
				cancelDetail.setProductid(orderEntity.getProductid());
				cancelDetail.setMoneydrawback(new BigDecimal(req.getParameter("moneyDrawback-" + i)));
				cancelDetail.setCreatetime(new Date());
				cancelDetail.setId(getTabelSequence(IdWorker.TABLE_ORDER_REMARK_DETAIL));
				cancelDetail.setOrderid(orderEntity);
				cancelDetail.setRebacknum(new BigDecimal(orderEntity.getNumber()));
				cancelDetail.setRemarks(req.getParameter("remark-" + i));
				
				orderCancelDetailListAdd.add(cancelDetail);
				//修改为在同意退单的时候处理
				/*OrderSendEntity orderSendDO = orderSendService.getSendOrder(orderEntity, req);
				if(null != orderSendDO) {orderSendHistoryList.add(orderSendDO);}*/
			}
		}
		
		
		if(orderCancelDetailListAdd.size() > 0){
			batchSave(orderCancelDetailListAdd);
		}
		
		//取消已经派发的订单,修改为在同意退单的时候处理
		/*if(null != orderSendHistoryList && orderSendHistoryList.size() > 0){
			for (OrderSendEntity orderSendDO : orderSendHistoryList) {
				orderSendDO.setStatus(OrderSendServiceI.Status.ORDER_CANCEL.getValue());
			}
			
			batchUpdate(orderSendHistoryList);
		}*/
		
		return orderCancelDetailListAdd;
	}
	
	
	@Override
	public BigDecimal getHasCancelOrderTotal2BaseUnit(OrderEntity o) throws Exception {
		BigDecimal totalNum = new BigDecimal(0.00);
		
		CriteriaQuery cq = new CriteriaQuery(OrderRebackDetailEntity.class);
		cq.createAlias("cancelid", "c");
		cq.add(Restrictions.eq("c.status", OrderRebackServiceI.Status.PASS.getValue()));
		cq.eq("orderid.id", o.getId());
		cq.add();
		
		List<OrderRebackDetailEntity> orderCancelDetailList = this.getListByCriteriaQuery(cq, false);
		
		if(null != orderCancelDetailList && orderCancelDetailList.size() > 0){
			for (OrderRebackDetailEntity orderCancelDetail : orderCancelDetailList) {
				
				ProductEntity p = this.findUniqueByProperty(ProductEntity.class, "id", orderCancelDetail.getProductid().getId());
				
				totalNum = totalNum.add(orderCancelDetail.getRebacknum());
				
				/*BigDecimal unitConvertVal = p.getUnitconvert();
				//如果退单不是按最小单位退单，且商品存在单位转换，那么转换成最小单位退单值
				if(null != unitConvertVal){
					BigDecimal assistCancelNum = orderCancelDetail.getAssistcancelnum().multiply(unitConvertVal);
					totalNum = totalNum.add( assistCancelNum );
				}*/
				
				
			}
		}
		
		return totalNum.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
}