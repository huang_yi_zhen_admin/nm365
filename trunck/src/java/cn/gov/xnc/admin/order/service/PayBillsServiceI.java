package cn.gov.xnc.admin.order.service;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.PayBillsEntity;
import cn.gov.xnc.admin.zhifu.alipay.config.AlipayConfig;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface PayBillsServiceI extends CommonService{
	
public static enum  State{
		
		PAID_INAUDIT("支付待审核", "2"),
		PAID_AUDIT_PASS("支付审核通过", "1"),
		PAID_AUDIT_UNPASS("支付审核未通过", "3"),
		PAID_SYS_AUDIT_PASS("支付系统审核通过", "4"),
		PAID_FAILED("支付失败", "5");
	    
		private String name ;
	    private String value ;
	     
	    private State( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}

	//支付渠道
	public enum  PaymentMethod{
		ALIPAY("支付宝", "1"), WXPAY("微信支付","2"), OFFLINEPAY("线下转账", "3"), ACCOUNTPAY("预存款支付", "4"), UNIONPAY("银联支付", "5"),  SENDFIRST("先货后款","6");
	
		private String name ;
	    private String value ;
	     
	    private PaymentMethod( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
	/**
	 *创建支付信息
	 *@param  idS 订单di合集
	 *@param  TSUser 当前公司信息，用户公司信息
	 *@param  初始化支付单类型PayBillsEntity
	 */
	public PayBillsEntity payment( String idS ,  TSUser user , PayBillsEntity payment) ;
	
	
	/**
	 *个人账号充值记录
	 *
	 *@param  TSUser 当前公司信息，用户公司信息
	 *@param  初始化支付单类型PayBillsEntity
	 */
	public PayBillsEntity paymentUser(  PayBillsEntity payment) ;
	
	
	/**
	 *修改支付流水为通过,适用线下支付情况
	 *@param  TSUser 当前用户信息
	 *@param  初始化支付单类型PayBillsEntity
	 */
	public boolean saveState(TSUser user ,PayBillsEntity payBills) ;
	
	/**
	 *修改支付流水为通过，适用线上支付情况
	 *@param  TSUser 当前用户信息
	 *@param  初始化支付单类型PayBillsEntity
	 */
	public boolean saveState4OnlinePay(TSUser user ,PayBillsEntity payBills) ;

	/**
	 *根据订单id 支付单id 获得当前公司的支付宝对应验证信息
	 *@param  支付单类型PayBillsEntity
	 */
	public AlipayConfig getAlipayConfig(PayBillsEntity payBills ) ;
	
	
	/**
	 *线下转账支付
	 *@param  TSUser 当前用户信息
	 *@param  初始化支付单类型PayBillsEntity
	 */
	public AjaxJson saveOfflinePayBills(PayBillsEntity payBills,String paymentmethod , HttpServletRequest request);
	
	/**
	 * 账户余额支付
	 */
	public AjaxJson saveUsemoneyPayBills(PayBillsEntity payBills,  String paymentmethod , HttpServletRequest request);
	
	/**
	 * 支付审核统计，返回支付审核单子数量和总价
	 * @param company
	 * @param request
	 * @return
	 */
	public StatisPageVO statisPaybills(TSCompany company,HttpServletRequest request);

}
