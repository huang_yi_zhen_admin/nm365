package cn.gov.xnc.admin.order.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.order.entity.OrderRebackTypeEntity;
import cn.gov.xnc.admin.order.service.OrderRebackTypeServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 退单信息
 * @author zero
 * @date 2017-12-27 11:55:17
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderRebackTypeController")
public class OrderRebackTypeController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderRebackTypeController.class);

	@Autowired
	private OrderRebackTypeServiceI orderRebackTypeService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 退单信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView orderRebackType(HttpServletRequest request) {
		return new ModelAndView("admin/order/orderRebackTypeList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderRebackTypeEntity orderRebackType,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(OrderRebackTypeEntity.class, dataGrid);
		cq.eq("status", "U");
		cq.add();
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderRebackType, request.getParameterMap());
		this.orderRebackTypeService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除退单信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(OrderRebackTypeEntity orderRebackType, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		orderRebackType = systemService.getEntity(OrderRebackTypeEntity.class, orderRebackType.getId());
		orderRebackType.setStatus("D");
		message = "退单信息删除成功";
		orderRebackTypeService.updateEntitie(orderRebackType);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加退单信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderRebackTypeEntity orderRebackType, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(orderRebackType.getId())) {
			message = "退单信息更新成功";
			OrderRebackTypeEntity t = orderRebackTypeService.get(OrderRebackTypeEntity.class, orderRebackType.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(orderRebackType, t);
				orderRebackTypeService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "退单信息更新失败";
			}
		} else {
			TSUser user = ResourceUtil.getSessionUserName();
			message = "退单信息添加成功";
			orderRebackType.setCompanyid(user.getCompany());
			orderRebackType.setStatus("U");
			orderRebackTypeService.save(orderRebackType);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 退单信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderRebackTypeEntity orderRebackType, HttpServletRequest req) {
		OrderRebackTypeEntity orderRebackTypePage = new OrderRebackTypeEntity();
		if (StringUtil.isNotEmpty(orderRebackType.getId())) {
			orderRebackTypePage = orderRebackTypeService.getEntity(OrderRebackTypeEntity.class, orderRebackType.getId());
		}
		req.setAttribute("orderRebackTypePage", orderRebackTypePage);
		
		return new ModelAndView("admin/order/orderRebackType");
	}
}
