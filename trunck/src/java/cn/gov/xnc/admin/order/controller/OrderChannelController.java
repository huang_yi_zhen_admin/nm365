package cn.gov.xnc.admin.order.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.LogUtil;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.order.entity.OrderChannelEntity;
import cn.gov.xnc.admin.order.service.OrderChannelServiceI;

/**   
 * @Title: Controller
 * @Description: 公司销售渠道列表
 * @author zero
 * @date 2018-06-25 09:57:33
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/orderChannelController")
public class OrderChannelController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OrderChannelController.class);

	@Autowired
	private OrderChannelServiceI orderChannelService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 公司销售渠道列表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView orderChannel(HttpServletRequest request) {
		return new ModelAndView("admin/order/orderChannelList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(OrderChannelEntity orderChannel,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(OrderChannelEntity.class, dataGrid);
		cq.eq("status", "U");
		cq.add(Restrictions.eqOrIsNull("company", user.getCompany()));
		cq.add();
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, orderChannel, request.getParameterMap());
		this.orderChannelService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除公司销售渠道列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(OrderChannelEntity orderChannel, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		orderChannel = systemService.getEntity(OrderChannelEntity.class, orderChannel.getId());
		orderChannel.setStatus(OrderChannelServiceI.CHANNEL_DEL);
		message = "公司销售渠道列表删除成功";
		orderChannelService.updateEntitie(orderChannel);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加公司销售渠道列表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(OrderChannelEntity orderChannel, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(orderChannel.getId())) {
			if(orderChannelService.getCountDuplicateChannelByName(orderChannel.getName(), orderChannel.getId()) == 0){
				message = "销售渠道更新成功";
				OrderChannelEntity t = orderChannelService.get(OrderChannelEntity.class, orderChannel.getId());
				try {
					MyBeanUtils.copyBeanNotNull2Bean(orderChannel, t);
					t.setStatus(OrderChannelServiceI.CHANNEL_USING);
					orderChannelService.saveOrUpdate(t);
					systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
				} catch (Exception e) {
					LogUtil.error("更新销售渠道失败：" + e.getMessage());
					message = "更新销售渠道失败：" + e.getMessage();
					j.setSuccess(false);
				}
			}else{
				message = "更新销售渠道失败：渠道名称重复";
				LogUtil.error("更新销售渠道失败：渠道名称重复");
			}
			
		} else {
			message = "销售渠道添加成功";
			if(orderChannelService.getCountDuplicateChannelByName(orderChannel.getName(), null) == 0){
				orderChannelService.save(orderChannel);
			}else{
				message = "更新销售渠道失败：渠道名称重复";
				LogUtil.error("更新销售渠道失败：渠道名称重复");
				j.setSuccess(false);
			}
			
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 公司销售渠道列表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(OrderChannelEntity orderChannel, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(orderChannel.getId())) {
			orderChannel = orderChannelService.getEntity(OrderChannelEntity.class, orderChannel.getId());
		}else{
			orderChannel = new OrderChannelEntity();
		}
		req.setAttribute("orderChannelPage", orderChannel);
		
		return new ModelAndView("admin/order/orderChannel");
	}
}
