package cn.gov.xnc.admin.order.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.copartner.entity.OrderSendEntity;
import cn.gov.xnc.admin.copartner.service.OrderSendServiceI;
import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackDetailEntity;
import cn.gov.xnc.admin.order.entity.OrderRebackEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderRebackDetailServiceI;
import cn.gov.xnc.admin.order.service.OrderRebackServiceI;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.admin.product.entity.ProductBrandEntity;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.service.StockIOServiceI;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("orderRebackService")
@Transactional
public class OrderRebackServiceImpl extends CommonServiceImpl implements OrderRebackServiceI {
	@Autowired
	private OrderRebackDetailServiceI orderCancelDetailService;
	@Autowired
	private OrderServiceI orderService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	@Autowired
	private StockIOServiceI stockIOService;
	@Autowired
	private StockProductPriceServiceI stockProductPriceService;
	@Autowired
	private OrderSendServiceI orderSendService;
	
	public AjaxJson audit(OrderRebackEntity orderCancle ) throws Exception {
		
		
		AjaxJson j = new AjaxJson();
		
		//try{
			OrderRebackEntity cancle = get(OrderRebackEntity.class, orderCancle.getId());
			MyBeanUtils.copyBeanNotNull2Bean(orderCancle, cancle);
			cancle.setAuditor(ResourceUtil.getSessionUserName());
			cancle.setAudittime(new Date());
			saveOrUpdate(cancle);
			
			if( "2".equals(cancle.getStatus() )){//同意退单，商品自动退库
//				j = stockIOService.orderCancleStockIn(cancle);
				//更新退单状态
				changeCancelOrderState(cancle);
				//更新退单总额和退单成本
				updateRebackFundsInfo(cancle);
				payBillsOrderService.setRebackStatus(cancle.getBillsorderid());
				//派单历史
				List<OrderSendEntity> orderSendHistoryList = new ArrayList<OrderSendEntity>();
				List<OrderRebackDetailEntity> listOrderRebackDetail = cancle.getCancelOrderDetailList();
				for (OrderRebackDetailEntity orderRebackDetail: listOrderRebackDetail) {
					OrderEntity orderDO = findUniqueByProperty(OrderEntity.class, "id", orderRebackDetail.getOrderid().getId());
					//如果订单还没发货，则将派送给发货商的订单取消
					if(OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(orderDO.getFreightState().getDictionaryValue())){
						OrderSendEntity orderSendDO = orderSendService.getSendOrder(orderDO);
						if(null != orderSendDO) {orderSendHistoryList.add(orderSendDO);}
					}
					
				}
				//取消已经派发的订单
				if(null != orderSendHistoryList && orderSendHistoryList.size() > 0){
					for (OrderSendEntity orderSendDO : orderSendHistoryList) {
						orderSendDO.setStatus(OrderSendServiceI.Status.ORDER_CANCEL.getValue());
					}
					
					batchUpdate(orderSendHistoryList);
				}
			}
		/*catch (Exception e) {
			// TODO: handle exception
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
			j.setMsg(e.getMessage());
			j.setSuccess(false);
			return j;
		}*/
		
		return j;
	}
	
	/**
	 * 获取需要退订的商品
	 */
	@Override
	public String jsonCancelOrderList(String cancelids) throws Exception {
		StringBuffer jsonTemp = new StringBuffer();
		
		if(StringUtil.isNotEmpty( cancelids )){
			
			CriteriaQuery cq = new CriteriaQuery(OrderEntity.class);
			cq.in("id", cancelids.split("\\|"));
			cq.in("state", new String[]{OrderServiceI.State.UNPAID.getValue(),OrderServiceI.State.WAIT2RECEIPT.getValue()});
			cq.add();
			
			List<OrderEntity> orderlist = this.getListByCriteriaQuery(cq, false);
			
			if(null != orderlist && orderlist.size() > 0){
				
				jsonTemp.append("{\"success\":\"1\",\"msg\":\"获取数据成功\",\"data\":[");
				if(null != orderlist && orderlist.size() > 0){
					
					for (int i = 0; i < orderlist.size() ; i++) {
						
						OrderEntity order = orderlist.get(i);
						//订购商品的最小单位换算值
						BigDecimal orderTotal = new BigDecimal(order.getNumber());//orderService.getOrderTotal2BaseUnit( order );
						//一退订商品的最小单位还算值
						/*BigDecimal cancelOrderTotal = orderCancelDetailService.getHasCancelOrderTotal2BaseUnit( order );
						//剩下可对丁商品最小单位换算值
						BigDecimal lessCancelOrderTotal = orderTotal.subtract(cancelOrderTotal).setScale(2, BigDecimal.ROUND_HALF_UP);
						//剩余商品总价
						BigDecimal lessMoneyTotal = order.getTotalPrice().divide(orderTotal).multiply(lessCancelOrderTotal).setScale(2, BigDecimal.ROUND_HALF_UP);
						//虽小单位价格
						BigDecimal baseUnitPrice = order.getTotalPrice().divide(orderTotal).setScale(2, BigDecimal.ROUND_HALF_UP);
						
						BigDecimal baseCancelNum = lessCancelOrderTotal, assistCancelNum = new BigDecimal(0.00);*/
						
						//获取订单商品
						ProductEntity product = this.findUniqueByProperty(ProductEntity.class, "id", order.getProductid().getId());
						String brandName = "";
						try{
							ProductBrandEntity brand = order.getProductid().getBrandid();
							brandName = brand.getBrandname();
						}catch(Exception e){}
						
						//组装json数据
						jsonTemp.append("{");
						jsonTemp.append("\"id\":\"" + order.getId() + "\", \"productId\":\""+ order.getProductid().getId() + "\", \"productCode\":\""+ order.getProductid().getCode() + "\"");
						jsonTemp.append(", \"productName\":\"" + order.getProductid().getName() + "\", \"productBrand\":\""+ brandName + "\"");
						//商品单位描述，如 1 个、1 箱
//						String unitConvertInfo = "1 " + product.getUnit().getName();
						//商品所有单位对象
//						String unitSelInfo = "{\"id\":\"" + product.getUnitbase().getId() + "\",\"name\":\"" + product.getUnitbase().getName() + "\"}";
						String orderTotalStr = orderTotal + product.getUnit().getName();
//						String cancelOrderTotalStr = cancelOrderTotal + product.getUnitbase().getName();
//						String lessCancelOrderTotalStr = lessCancelOrderTotal + product.getUnitbase().getName();
						//商品单位换算比
						/*String unitConvertVal = "";
						if(null != product.getUnitconvert()){
							unitConvertInfo = "1 " + product.getUnitassist().getName() + " = " + product.getUnitconvert() +  product.getUnitbase().getName();
							unitSelInfo += ",{\"id\":\"" + product.getUnitassist().getId() + "\",\"name\":\"" + product.getUnitassist().getName().trim() + "(" + product.getUnitconvert() +  product.getUnitbase().getName() + ")\"}";
							unitConvertVal = product.getUnitconvert().toString();
							//订购数量
							BigDecimal[] orderDivResults = orderTotal.divideAndRemainder(product.getUnitconvert());
							orderTotalStr = orderDivResults[0] + product.getUnitassist().getName() + " " + orderDivResults[1] + product.getUnitbase().getName();
							//已经退订数量
							BigDecimal[] cancelDivResults = cancelOrderTotal.divideAndRemainder(product.getUnitconvert());
							cancelOrderTotalStr = cancelDivResults[0] + product.getUnitassist().getName() + " " + cancelDivResults[1] + product.getUnitbase().getName();
							//还可以退订数量
							BigDecimal[] divideResults = lessCancelOrderTotal.divideAndRemainder(product.getUnitconvert());
							assistCancelNum = divideResults[0];
							baseCancelNum = divideResults[1];
							lessCancelOrderTotalStr = assistCancelNum + product.getUnitassist().getName() + " " + baseCancelNum + product.getUnitbase().getName();
						}
						
						if(null != product.getUnitconvert()){
							unitConvertInfo = "1 " + product.getUnitassist().getName() + " = " + product.getUnitconvert() +  product.getUnitbase().getName();
						}*/
						
						/*jsonTemp.append(", \"unitConvertInfo\":\"" + unitConvertInfo.trim() + "\", \"unitSel\":[" +unitSelInfo + "]");
						jsonTemp.append(", \"unitConvertVal\":\"" + unitConvertVal + "\", \"unitBaseId\":\"" +product.getUnitbase().getId() + "\"");*/
						/*jsonTemp.append(", \"orderTotal\":\"" + orderTotal + "\", \"cancelOrderTotal\":\"" + cancelOrderTotal + "\",\"lessCancelOrderTotal\":\"" + lessCancelOrderTotal + "\"");
						jsonTemp.append(", \"orderTotalStr\":\"" + orderTotalStr + "\", \"cancelOrderTotalStr\":\"" + cancelOrderTotalStr + "\",\"lessCancelOrderTotalStr\":\"" + lessCancelOrderTotalStr + "\"");
						jsonTemp.append(", \"baseCancelNum\":\"" + baseCancelNum + "\", \"assistCancelNum\":\"" + assistCancelNum + "\"");*/
						jsonTemp.append(", \"orderTotalStr\":\"" + orderTotalStr + "\", \"orderUnitId\":\"" + product.getUnit().getId() + "\", \"orderPrice\":\"" + order.getPrice() + "\"");
						jsonTemp.append(", \"orderNum\":\"" + order.getNumber() + "\", \"moneyFreight\":\""+ order.getFreightpic() + "\"");
//						jsonTemp.append(", \"baseUnitPrice\":\"" + baseUnitPrice + "\", \"lessMoneyTotal\":\""+ lessMoneyTotal + "\"");
						jsonTemp.append(", \"orderTotalMoney\":\"" + order.getTotalPrice() + "\", \"remark\":\""+ (null != order.getRemarks()? order.getRemarks() : "") + "\"");
						jsonTemp.append("}");
						
						if (i != orderlist.size() - 1){
							jsonTemp.append(",");
						}
						
					}
					
				}
				
				jsonTemp.append("],\"total\":\""+ orderlist.size() + "\"}");
				
			}else{
				
				jsonTemp.append("{\"success\":\"0\",\"msg\":\"抱歉，系统没有获取到你要退订的商品\",\"data\":[],\"total\":\"0\"}");
			}
			
		}else{
			
			jsonTemp.append("{\"success\":\"0\",\"msg\":\"系统没有获取到退订商品编码\",\"data\":[],\"total\":\"0\"}");
		}
		
		return jsonTemp.toString();
	}

	@Override
	public void addOrderCancel(OrderRebackEntity orderCancel, HttpServletRequest req) throws Exception {
		TSUser user = ResourceUtil.getSessionUserName();
		String billsorderid = orderCancel.getBillsorderid().getId();
		if(StringUtil.isNotEmpty(billsorderid)){
			CriteriaQuery cq = new CriteriaQuery(OrderRebackEntity.class);
			cq.eq("billsorderid.id", billsorderid);
			cq.eq("status", OrderRebackServiceI.Status.AUDIT.getValue());
			cq.add();
			
			List<OrderRebackEntity> ordersCancelList = this.getListByCriteriaQuery(cq, true);
			
			if(null != ordersCancelList && ordersCancelList.size() > 0){
				throw new Exception("抱歉，您还有待退订单，请耐心等待审核！");
			}else{
				
				OrderRebackEntity OrderRebackEntity = new OrderRebackEntity();
				OrderRebackEntity.setBillsorderid(this.findUniqueByProperty(PayBillsOrderEntity.class, "id", billsorderid));
				OrderRebackEntity.setCreatetime(new Date());
				OrderRebackEntity.setId(getTabelSequence(IdWorker.TABLE_ORDER_REBACK));
				OrderRebackEntity.setStatus( OrderRebackServiceI.Status.AUDIT.getValue() );
				OrderRebackEntity.setClient(ResourceUtil.getSessionUserName());
				OrderRebackEntity.setRebacktype(orderCancel.getRebacktype());
				OrderRebackEntity.setTotalmoneytdrawback( orderCancel.getTotalmoneytdrawback() );
				OrderRebackEntity.setAuditremark(orderCancel.getAuditremark());
				OrderRebackEntity.setRebackreason(orderCancel.getRebackreason());
				OrderRebackEntity.setRebackattach(orderCancel.getRebackattach());
				OrderRebackEntity.setCompanyid(user.getCompany());
				OrderRebackEntity.setProductreback(orderCancel.getProductreback());
				
				save(OrderRebackEntity);
				
				List<OrderRebackDetailEntity> orderCancelDetailList = orderCancelDetailService.batchSaveCancelDetails(OrderRebackEntity, req);
				
				if(null != orderCancelDetailList && orderCancelDetailList.size() > 0){
					OrderRebackEntity.setCancelOrderDetailList(orderCancelDetailList);
					BigDecimal baseCost = new BigDecimal(0.00);
					BigDecimal totalDrawback = new BigDecimal(0.00);
					for (OrderRebackDetailEntity OrderRebackDetailEntity : orderCancelDetailList) {
						totalDrawback = totalDrawback.add( OrderRebackDetailEntity.getMoneydrawback() );
					}
					
					OrderRebackEntity.setTotalbasecost(baseCost);//退单总成本，默认0，在确认退单才能计算
					OrderRebackEntity.setTotalmoneytdrawback(totalDrawback);//退单总额
					updateEntitie(OrderRebackEntity);
				}
			}
		}
	}

	@Override
	public StatisPageVO statisCancelOrder(TSCompany company, HashMap<String, String> param) throws Exception {
		TSUser user = ResourceUtil.getSessionUserName();
		String id = param.get("id");
		String state = param.get("status");
		String customername = param.get("client");
		String salemanname = param.get("saleman");
		String createdate1 = param.get("createdate1");
		String createdate2 = param.get("createdate2");
		
		StatisPageVO statisPageVO = new StatisPageVO();
		StringBuffer Sql = new StringBuffer();
		Sql.append("SELECT IFNULL(SUM(1),0) value1, IFNULL(SUM(a.totalMoneytDrawback),0) value2, IFNULL(SUM(a.totalBaseCost),0) value3 FROM xnc_order_reback a, xnc_pay_bills_order b ");
		Sql.append("WHERE b.company = '" + company.getId() + "'");
		Sql.append(" AND a.billsorderid = b.id ");
		//业务员只能看自己负责客户的单子
		if("4".equals(user.getType())){
			Sql.append(" AND b.salemanid = '"+ user.getId() +"'");
		}
//		else{
//			Sql.append(" AND b.clientid = '"+ user.getId() +"'");
//		}
		
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") > 0){
			
			String[] states = state.trim().split(",");
			StringBuffer tmp = new StringBuffer();
			
			for (int i = 0; i < states.length; i++) {
				if(tmp.length() == 0){
					tmp.append("'");
					tmp.append(states[i]);
					tmp.append("'");
				}else{
					tmp.append(",'");
					tmp.append(states[i]);
					tmp.append("'");
				}
			}
			Sql.append(" AND a.status in (" + tmp.toString() + ")");//全部
			
		}
		if(StringUtil.isNotEmpty(state) 
				&& StringUtil.isNotEmpty(state.trim())
					&& state.trim().indexOf(",") == -1){
			
			Sql.append(" AND a.status = '" + state + "' ");//待审核和已审核
			
		}
		if(StringUtil.isNotEmpty(id) 
				&& StringUtil.isNotEmpty(id.trim())){
			
			Sql.append(" AND a.billsorderid like '%" + id.trim() + "%' ");//具体订单
			
		}
		if(StringUtil.isNotEmpty(customername) 
				&& StringUtil.isNotEmpty(customername.trim())){
			
			Sql.append(" AND EXISTS (SELECT 1 FROM t_s_user u WHERE u.id = b.clientid AND u.type = '3' AND u.userName like '%" + customername.trim()+ "%') ");//根据客户查找
			
		}
		if(StringUtil.isNotEmpty(salemanname) 
				&& StringUtil.isNotEmpty(salemanname.trim())){
			
			Sql.append(" AND EXISTS (SELECT 1 FROM t_s_user u WHERE u.id = b.clientid AND u.type = '4' AND u.userName like '%" + salemanname.trim()+ "%') ");//具体业务员
			
		}

		//时间
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			Sql.append(" AND a.createtime BETWEEN '" + createdate1 + "' AND '" + createdate2 + "'" );
		}else if( StringUtil.isNotEmpty(createdate1) ){
			Sql.append(" AND a.createtime >= '" + createdate1 + "'" );
		}else if( StringUtil.isNotEmpty(createdate2) ){
			Sql.append(" AND a.createtime <= '" + createdate2 + "'" );
		}
		
		List<StatisPageVO>  list = queryListByJdbc(Sql.toString(), StatisPageVO.class);	
		
		if(null != list && list.size() > 0){
			statisPageVO = list.get(0);
		}
		return statisPageVO;
	}
	
	@Override
	public void changeCancelOrderState(OrderRebackEntity orderCancel) throws Exception {
		
		if(null != orderCancel && StringUtil.isNotEmpty(orderCancel.getId())){
			List<OrderEntity> statusChangeOrderlist = new ArrayList<OrderEntity>();
			
			OrderRebackEntity OrderRebackEntity = this.findUniqueByProperty(OrderRebackEntity.class, "id", orderCancel.getId());
			List<OrderRebackDetailEntity> orderCancelDetailList = OrderRebackEntity.getCancelOrderDetailList();
			
			
			//暂时不绑定订单状态，退单跟订单分开，因为退单的时候退货跟订单没有关系
			for (OrderRebackDetailEntity OrderRebackDetailEntity : orderCancelDetailList) {
				
				OrderEntity order = OrderRebackDetailEntity.getOrderid();
				//订购商品的最小单位换算值(暂时没有)
				BigDecimal orderTotal = new BigDecimal(order.getNumber()); //orderService.getOrderTotal2BaseUnit( order );
				//一退订商品的最小单位还算值
				BigDecimal cancelOrderTotal = orderCancelDetailService.getHasCancelOrderTotal2BaseUnit( order );
				
				if(orderTotal.compareTo(cancelOrderTotal) == 0){
					order.setState(OrderServiceI.State.REBACK.getValue());
					statusChangeOrderlist.add(order);
				}
			}
			
			//批量更新订单状态
			batchUpdate(statusChangeOrderlist);
		}
	}

	@Override
	public void updateRebackFundsInfo(OrderRebackEntity orderCancel) throws Exception {
		if(null != orderCancel){
			//获取退单详情
			List<OrderRebackDetailEntity> orderCancelDetailList = orderCancel.getCancelOrderDetailList();
			if(null != orderCancelDetailList && orderCancelDetailList.size() > 0){
				//退单总成本
				/*BigDecimal baseCost = new BigDecimal(0.00);
				BigDecimal cancelProductBaseCost = new BigDecimal(0.00);
				StockProductPriceEntity stockProductPrice = null;
				for (OrderRebackDetailEntity OrderRebackDetailEntity : orderCancelDetailList) {
					//获取商品当前的库存信息
					stockProductPrice = stockProductPriceService.getStockProductPriceInfo(OrderRebackDetailEntity.getProductid().getId());
					//获取商品当前的库存最小单位价格
					BigDecimal stockBaseAvgPrice = stockProductPrice.getAveragePrice();
					
					BigDecimal cancelTotal = OrderRebackDetailEntity.getRebacknum();
					
					ProductEntity p = OrderRebackDetailEntity.getProductid();
					p = findUniqueByProperty(ProductEntity.class, "id", p.getId());
					if(null != OrderRebackDetailEntity.getAssistcancelnum() && null != p.getUnitconvert()){
						cancelTotal = cancelTotal.add(OrderRebackDetailEntity.getAssistcancelnum().multiply(p.getUnitconvert()));
						stockBaseAvgPrice = stockBaseAvgPrice.divide(p.getUnitconvert(), 2, BigDecimal.ROUND_HALF_UP);
					}
					
					cancelProductBaseCost = stockBaseAvgPrice.multiply(cancelTotal);
					//设置单个商品退单总成本
					OrderRebackDetailEntity.setBasecost( cancelProductBaseCost );
					//设置整个退单成本
					baseCost = baseCost.add( cancelProductBaseCost );
				}*/
				
				//更新退单详细
				/*batchUpdate(orderCancelDetailList);
				//更新整个退单信息
				orderCancel.setTotalbasecost(baseCost);
				updateEntitie(orderCancel);*/
				
				//更新关联订单的退单总额
				if(null != orderCancel.getBillsorderid() && StringUtil.isNotEmpty(orderCancel.getBillsorderid().getId())){
					
					PayBillsOrderEntity billsOrder = findUniqueByProperty(PayBillsOrderEntity.class, "id", orderCancel.getBillsorderid().getId());
					//退单总额
					BigDecimal billsMoneyRefunds = billsOrder.getCancelMoney();
					billsOrder.setCancelMoney( billsMoneyRefunds.add(orderCancel.getTotalmoneytdrawback()) );
					//计算待付金额
					billsOrder.setObligationsmoney(billsOrder.getPayablemoney().subtract(billsOrder.getAlreadypaidmoney()).subtract(billsOrder.getCancelMoney()));
					//退单成本
					BigDecimal billsBaseRefunds = billsOrder.getBaseCost();
//					billsOrder.setBaseCost(billsBaseRefunds.add(baseCost));
					
					updateEntitie(billsOrder);
				}
				
			}
		}
	}
}