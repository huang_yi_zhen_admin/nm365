package cn.gov.xnc.admin.order.service;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.dataStatistics.entity.StatisPageVO;
import cn.gov.xnc.admin.order.entity.OrderRebackEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

public interface OrderRebackServiceI extends CommonService{
	public static enum  Status{
		AUDIT("退单审核", "1"), UNPASS("退单未通过", "3"), PASS("退单通过","2");
	    private String name ;
	    private String value ;
	     
	    private Status( String name , String value ){
	        this.name = name ;
	        this.value = value ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public String getValue() {
	        return value;
	    }
	    public void setValue(String index) {
	        this.value = index;
	    }
	 
	}
	
	public AjaxJson audit(OrderRebackEntity orderCancle ) throws Exception;
	
	public String jsonCancelOrderList(String cancelids) throws Exception;
	
	public void addOrderCancel(OrderRebackEntity orderCancel, HttpServletRequest req) throws Exception;
	
	/**
	 * 统计退单金额
	 * @param company
	 * @param param[id,status,client,saleman,createdate1,createdate2]
	 * @return
	 * @throws Exception
	 */
	public StatisPageVO statisCancelOrder(TSCompany company, HashMap<String, String> param) throws Exception;
	
	/**
	 * 根据退单数量是否等于采购数量，改变商品订单状态
	 * @param OrderRebackEntity
	 * @return void
	 * @throws Exception
	 */
	public void changeCancelOrderState(OrderRebackEntity orderCancel) throws Exception;
	
	/**
	 * 更新退单资金回退问题，包括退单总额、单品成本和总成本
	 * @param orderCancel
	 * @throws Exception
	 */
	public void updateRebackFundsInfo(OrderRebackEntity orderCancel) throws Exception;
}
