package cn.gov.xnc.admin.order.service.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.order.entity.OrderChannelEntity;
import cn.gov.xnc.admin.order.service.OrderChannelServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

@Service("orderChannelService")
@Transactional
public class OrderChannelServiceImpl extends CommonServiceImpl implements OrderChannelServiceI {

	@Override
	public List<OrderChannelEntity> getCompanyAllChannel() {
		TSCompany company = ResourceUtil.getSessionUserName().getCompany();
		CriteriaQuery cq = new CriteriaQuery(OrderChannelEntity.class);
		cq.eq("status", OrderChannelServiceI.CHANNEL_USING);
		cq.or(Restrictions.isNull("company"), Restrictions.eq("company.id", company.getId()));
		cq.add();
		
		return getListByCriteriaQuery(cq, false);
	}

	@Override
	public OrderChannelEntity getOrderChannelByName(String channelName) {
		OrderChannelEntity orderChannel = null;
		
		if(StringUtil.isNotEmpty(channelName)){
			TSCompany company = ResourceUtil.getSessionUserName().getCompany();
			CriteriaQuery cq = new CriteriaQuery(OrderChannelEntity.class);
			cq.eq("status", OrderChannelServiceI.CHANNEL_USING);
			cq.eq("name", channelName.trim());
			cq.or(Restrictions.isNull("company"), Restrictions.eq("company.id", company.getId()));
			cq.add();
			
			List<OrderChannelEntity> listChannel = getListByCriteriaQuery(cq, true);
			if(null != listChannel && listChannel.size() > 0){
				orderChannel = listChannel.get(0);
			}
		}
		
		return orderChannel;
	}

	@Override
	public int getCountDuplicateChannelByName(String channelName, String channelID) {
		TSCompany company = ResourceUtil.getSessionUserName().getCompany();
		String companyid = company.getId();
		StringBuffer sqlbuf = new StringBuffer();
		sqlbuf.append("SELECT count(1) from XNC_ORDER_CHANNEL t ");
		sqlbuf.append(" WHERE t.name = ? ");
		sqlbuf.append(" AND t.status = 'U' ");
		if(StringUtil.isNotEmpty(channelID)){
			sqlbuf.append(" AND t.id <> '").append(channelID).append("'");
		}
		sqlbuf.append(" AND (t.company is null or t.company = '").append(companyid).append("')");
		
		Long l = this.getCountForJdbcParam(sqlbuf.toString(), new String[]{channelName});
		
		return l.intValue();
	}
	
}