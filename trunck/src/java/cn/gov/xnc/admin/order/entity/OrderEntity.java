package cn.gov.xnc.admin.order.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 订单明细
 * @author zero
 * @date 2016-10-02 02:00:47
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_order", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class OrderEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**客户信息*/
	private TSUser  clientid;
	
	/**业务员*/
	private TSUser  yewu;
	
	/**订单编号*/
	private java.lang.String identifieror;
	/**商品名称*/
	private java.lang.String productname;
	/**数量*/
	private java.lang.Integer number;
	/**发货仓*/
	private java.lang.String warehouse;
	/**收货人*/
	private java.lang.String customername;
	/**电话*/
	private java.lang.String telephone;
	/**收货地址*/
	private java.lang.String address;
	/**省份*/
	private java.lang.String province;
	/**城市*/
	private java.lang.String city;
	/**地区*/
	private java.lang.String area;
	/**自定义平台*/
	private java.lang.String platform;
	/**发货时间*/
	private java.util.Date departuredate;
	/**付款待发货时间*/
	private java.util.Date paydate;
	/**退单时间*/
	private java.util.Date canceldate;
	/**下单时间*/
	private java.util.Date createdate;
	/**物流公司*/
	private java.lang.String logisticscompany;
	/**物流单号*/
	private java.lang.String logisticsnumber;
//	/**物流状态   在途中_0,已发货_1,疑难件_2,已签收_3,已退货_4,部分签收_5,数据异常_6 */
//	private java.lang.String logisticsState;
//	/**物流信息更新时间*/
//	private java.util.Date logisticsdate;
	/**备注*/
	private java.lang.String remarks;
	/**快递运费*/
	private BigDecimal freightpic;
	/**单位成本*/
	private BigDecimal unitCost;
	/**销售成本=单位成本 x 采购数量*/
	private BigDecimal pricec;
	/**订货价格*/
	private BigDecimal price;
	/**市场价格*/
	private BigDecimal prices;
	/**销售总额*/
	private BigDecimal totalPrice;
	/**priceyw*/
	private BigDecimal priceyw;
	/**发货状态  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7 */
	private java.lang.String state;
	/**订单状态:1  生成 2  确认 3 取消  4  待付 5  已付 6  未付清*/
//	private String status;
	/**商品id*/
	private  ProductEntity productid;
	/**提货方式：自提_1,走物流_2,同城物流_3*/
	private  java.lang.String sendtype;
	/**是否派单给发货商或者其他合作伙伴处理  Y 是 N 否*/
	private java.lang.String ordersend;
	/**所属地区*/
	private TSTerritory TSTerritory ;// 地址
	/**公司信息*/
	private TSCompany company;
	/**应付订单id*/
	private PayBillsOrderEntity billsorderid;
	/**物流订单信息*/
	private List<OrderLogisticsEntity> orderLogisticsList = new ArrayList<OrderLogisticsEntity>();
	/**商品单位*/
	private ProductUnitEntity unitid;
	/**出库单号*/
	private String stockIOId;
	/**物流状态*/
	private TSDictionary freightState;
	/**核单次数*/
	private Integer verifyNum;
	/**打印次数*/
	private Integer printNum;
	/**销售渠道*/
	private OrderChannelEntity channelID;
	
	
	/**
	 * @return the orderLogisticsList
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "orderid")
	public List<OrderLogisticsEntity> getOrderLogisticsList() {
		return orderLogisticsList;
	}

	/**
	 * @param orderLogisticsList the orderLogisticsList to set
	 */
	public void setOrderLogisticsList(List<OrderLogisticsEntity> orderLogisticsList) {
		this.orderLogisticsList = orderLogisticsList;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单编号
	 */
	@Column(name ="IDENTIFIEROR",nullable=true,length=32)
	public java.lang.String getIdentifieror(){
		return this.identifieror;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单编号
	 */
	public void setIdentifieror(java.lang.String identifieror){
		this.identifieror = identifieror;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品名称
	 */
	@Column(name ="PRODUCTNAME",nullable=true,length=200)
	public java.lang.String getProductname(){
		return this.productname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品名称
	 */
	public void setProductname(java.lang.String productname){
		this.productname = productname;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  数量
	 */
	@Column(name ="NUMBER",nullable=true,precision=10,scale=0)
	public java.lang.Integer getNumber(){
		return this.number;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  数量
	 */
	public void setNumber(java.lang.Integer number){
		this.number = number;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发货仓
	 */
	@Column(name ="WAREHOUSE",nullable=true,length=40)
	public java.lang.String getWarehouse(){
		return this.warehouse;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发货仓
	 */
	public void setWarehouse(java.lang.String warehouse){
		this.warehouse = warehouse;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收货人
	 */
	@Column(name ="CUSTOMERNAME",nullable=true,length=60)
	public java.lang.String getCustomername(){
		return this.customername;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收货人
	 */
	public void setCustomername(java.lang.String customername){
		this.customername = customername;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电话
	 */
	@Column(name ="TELEPHONE",nullable=true,length=40)
	public java.lang.String getTelephone(){
		return this.telephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电话
	 */
	public void setTelephone(java.lang.String telephone){
		this.telephone = telephone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  收货地址
	 */
	@Column(name ="ADDRESS",nullable=true,length=300)
	public java.lang.String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  收货地址
	 */
	public void setAddress(java.lang.String address){
		this.address = address;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  省份
	 */
	@Column(name ="PROVINCE",nullable=true,length=100)
	public java.lang.String getProvince(){
		return this.province;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  省份
	 */
	public void setProvince(java.lang.String province){
		this.province = province;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  城市
	 */
	@Column(name ="CITY",nullable=true,length=200)
	public java.lang.String getCity(){
		return this.city;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  城市
	 */
	public void setCity(java.lang.String city){
		this.city = city;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地区
	 */
	@Column(name ="AREA",nullable=true,length=300)
	public java.lang.String getArea(){
		return this.area;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  地区
	 */
	public void setArea(java.lang.String area){
		this.area = area;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  自定义平台
	 */
	@Column(name ="PLATFORM",nullable=true,length=255)
	public java.lang.String getPlatform(){
		return this.platform;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  自定义平台
	 */
	public void setPlatform(java.lang.String platform){
		this.platform = platform;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  发货时间
	 */
	@Column(name ="DEPARTUREDATE",nullable=true)
	public java.util.Date getDeparturedate(){
		return this.departuredate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  发货时间
	 */
	public void setDeparturedate(java.util.Date departuredate){
		this.departuredate = departuredate;
	}
	
	
	
	
	
	/**
	 * @return the paydate
	 */
	@Column(name ="PAYDATE",nullable=true)
	public java.util.Date getPaydate() {
		return paydate;
	}

	/**
	 * @param paydate the paydate to set
	 */
	public void setPaydate(java.util.Date paydate) {
		this.paydate = paydate;
	}

	/**
	 * @return the canceldate
	 */
	@Column(name ="CANCELDATE",nullable=true)
	public java.util.Date getCanceldate() {
		return canceldate;
	}

	/**
	 * @param canceldate the canceldate to set
	 */
	public void setCanceldate(java.util.Date canceldate) {
		this.canceldate = canceldate;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  下单时间
	 */
	@Column(name ="CREATEDATE",nullable=true)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public java.util.Date getCreatedate(){
		return this.createdate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  下单时间
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public void setCreatedate(java.util.Date createdate){
		this.createdate = createdate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流公司
	 */
	@Column(name ="LOGISTICSCOMPANY",nullable=true,length=300)
	public java.lang.String getLogisticscompany(){
		return this.logisticscompany;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  物流公司
	 */
	public void setLogisticscompany(java.lang.String logisticscompany){
		this.logisticscompany = logisticscompany;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  物流单号
	 */
	@Column(name ="LOGISTICSNUMBER",nullable=true,length=5000)
	public java.lang.String getLogisticsnumber(){
		return this.logisticsnumber;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  物流单号
	 */
	public void setLogisticsnumber(java.lang.String logisticsnumber){
		this.logisticsnumber = logisticsnumber;
	}
	
	
	


	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARKS",nullable=true,length=5000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  快递运费
	 */
	@Column(name ="FREIGHTPIC",nullable=true,precision=10,scale=2)
	public BigDecimal getFreightpic(){
		return this.freightpic;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  快递运费
	 */
	public void setFreightpic(BigDecimal freightpic){
		this.freightpic = freightpic;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  订货价格
	 */
	@Column(name ="PRICE",nullable=true,precision=10,scale=2)
	public BigDecimal getPrice(){
		return this.price;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  订货价格
	 */
	public void setPrice(BigDecimal price){
		this.price = price;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  进货价格
	 */
	@Column(name ="PRICEC",nullable=true,precision=10,scale=2)
	public BigDecimal getPricec(){
		return this.pricec;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  进货价格
	 */
	public void setPricec(BigDecimal pricec){
		this.pricec = pricec;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  市场价格
	 */
	@Column(name ="PRICES",nullable=true,precision=10,scale=2)
	public BigDecimal getPrices(){
		return this.prices;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  市场价格
	 */
	public void setPrices(BigDecimal prices){
		this.prices = prices;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  priceyw
	 */
	@Column(name ="PRICEYW",nullable=true,precision=10,scale=2)
	public BigDecimal getPriceyw(){
		return this.priceyw;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  priceyw
	 */
	public void setPriceyw(BigDecimal priceyw){
		this.priceyw = priceyw;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  发货状态  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7
	 */
	@Column(name ="STATE",nullable=true,length=10)
	public java.lang.String getState(){
		return this.state;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  发货状态 待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7
	 */
	public void setState(java.lang.String state){
		this.state = state;
	}

//	/**
//	 *方法: 取得java.lang.String
//	 *@return: java.lang.String  订单状态:1 生成、2 确认、3 取消
//	 */
//	@Column(name ="STATUS",nullable=false,length=4)
//	public java.lang.String getStatus(){
//		return this.status;
//	}
//
//	/**
//	 *方法: 设置java.lang.String
//	 *@param: java.lang.String  订单状态:1 生成、2 确认、3 取消
//	 */
//	public void setStatus(java.lang.String status){
//		this.status = status;
//	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品id
	 */
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品id
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  应付订单id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BILLSORDERID")
	public PayBillsOrderEntity getBillsorderid(){
		return this.billsorderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  应付订单id
	 */
	public void setBillsorderid(PayBillsOrderEntity billsorderid){
		this.billsorderid = billsorderid;
	}
	
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地址信息
	 */
	@JsonIgnore    //getList查询转换为列表时处理json转换异常
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TERRITORYID")
	public TSTerritory getTSTerritory() {
		return TSTerritory;
	}

	public void setTSTerritory(TSTerritory tSTerritory) {
		TSTerritory = tSTerritory;
	}
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  客户信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLIENTID")
	public TSUser getClientid(){
		return this.clientid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  客户信息
	 */
	public void setClientid(TSUser clientid){
		this.clientid = clientid;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  业务员
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "YEWU")
	public TSUser getYewu(){
		return this.yewu;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  业务员
	 */
	public void setYewu(TSUser yewu){
		this.yewu = yewu;
	}

	@Column(name ="SENDTYPE",nullable=true,length=2)
	public java.lang.String getSendtype() {
		return sendtype;
	}

	public void setSendtype(java.lang.String sendtype) {
		this.sendtype = sendtype;
	}
	
	/**是否派单给发货商或者其他合作伙伴处理 
	 *  Y 是
	 *  N 否
	 *  */
	@Column(name ="ORDERSEND",nullable=true,length=2)
	public java.lang.String getOrdersend() {
		return ordersend;
	}

	public void setOrdersend(java.lang.String ordersend) {
		this.ordersend = ordersend;
	}
	
	@Column(name ="stockIoId",nullable=true,length=32)
	public String getStockIOId() {
		return stockIOId;
	}

	public void setStockIOId(String stockIOId) {
		this.stockIOId = stockIOId;
	}

	/**
	 *方法: 取得ProductUnitEntity
	 *@return: java.lang.String  商品单位编码，适用不同计量单位价格不同情况
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNITID")
	public ProductUnitEntity getUnitid(){
		return this.unitid;
	}

	/**
	 *方法: 设置ProductUnitEntity
	 *@param: java.lang.String  商品单位编码，适用不同计量单位价格不同情况
	 */
	public void setUnitid(ProductUnitEntity unitid){
		this.unitid = unitid;
	}

	@Column(name ="TOTALPRICE",nullable=true,length=32)
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "freightState")
	public TSDictionary getFreightState() {
		return freightState;
	}

	public void setFreightState(TSDictionary freightState) {
		this.freightState = freightState;
	}

	@Column(name="verifyNum",length=20)
	public Integer getVerifyNum() {
		return verifyNum;
	}

	public void setVerifyNum(Integer verifyNum) {
		this.verifyNum = verifyNum;
	}

	@Column(name="printNum",length=20)
	public Integer getPrintNum() {
		return printNum;
	}

	public void setPrintNum(Integer printNum) {
		this.printNum = printNum;
	}

	@Column(name ="UNITCOST",nullable=true,length=32)
	public BigDecimal getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(BigDecimal unitCost) {
		this.unitCost = unitCost;
	}

	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNELID")
	public OrderChannelEntity getChannelID() {
		return channelID;
	}

	public void setChannelID(OrderChannelEntity channelID) {
		this.channelID = channelID;
	}
	
}
