package cn.gov.xnc.admin.order.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 退单信息
 * @author zero
 * @date 2017-12-27 11:53:47
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_order_reback", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class OrderRebackEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**关联大订单号*/
	private PayBillsOrderEntity billsorderid;
	/**退货关联的入库大单号，多个单号用英文半角逗号分割*/
	private java.lang.String stockioid;
	/**状态: 1 退单申请 2 同意退单 3 拒绝退单*/
	private java.lang.String status;
	/**是否要退货: N 否 Y 是*/
	private String productreback;
	/**退单原因*/
	private OrderRebackTypeEntity rebacktype;
	/**退单总额*/
	private BigDecimal totalmoneytdrawback;
	/**退单成本*/
	private BigDecimal totalbasecost;
	/**退单原因*/
	private java.lang.String rebackreason;
	/**退单附件*/
	private java.lang.String rebackattach;
	/**退单审核说明*/
	private java.lang.String auditremark;
	/**申请退单人*/
	private TSUser client;
	/**申请时间*/
	private java.util.Date createtime;
	/**审核人*/
	private TSUser auditor;
	/**审核时间*/
	private java.util.Date audittime;
	/**修改时间*/
	private java.util.Date updatetime;
	
	private TSCompany companyid;
	
	/**取消订单明细list*/
	private List<OrderRebackDetailEntity> cancelOrderDetailList = null;
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  关联大订单号
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BILLSORDERID")
	public PayBillsOrderEntity getBillsorderid() {
		return billsorderid;
	}

	public void setBillsorderid(PayBillsOrderEntity billsorderid) {
		this.billsorderid = billsorderid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  退货关联的入库大单号，多个单号用英文半角逗号分割
	 */
	@Column(name ="STOCKIOID",nullable=true,length=4000)
	public java.lang.String getStockioid(){
		return this.stockioid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  退货关联的入库大单号，多个单号用英文半角逗号分割
	 */
	public void setStockioid(java.lang.String stockioid){
		this.stockioid = stockioid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态: 1 退单申请 2 同意退单 3 拒绝退单
	 */
	@Column(name ="STATUS",nullable=false,length=4)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态: 1 退单申请 2 同意退单 3 拒绝退单
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  退单原因
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REBACKTYPE")
	public OrderRebackTypeEntity getRebacktype(){
		return this.rebacktype;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  退单原因
	 */
	public void setRebacktype(OrderRebackTypeEntity rebacktype){
		this.rebacktype = rebacktype;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  退单总额
	 */
	@Column(name ="TOTALMONEYTDRAWBACK",nullable=false,precision=15,scale=2)
	public BigDecimal getTotalmoneytdrawback(){
		return this.totalmoneytdrawback;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  退单总额
	 */
	public void setTotalmoneytdrawback(BigDecimal totalmoneytdrawback){
		this.totalmoneytdrawback = totalmoneytdrawback;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  退单成本
	 */
	@Column(name ="TOTALBASECOST",nullable=false,precision=15,scale=2)
	public BigDecimal getTotalbasecost(){
		return this.totalbasecost;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  退单成本
	 */
	public void setTotalbasecost(BigDecimal totalbasecost){
		this.totalbasecost = totalbasecost;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  退单原因
	 */
	@Column(name ="REBACKREASON",nullable=false,length=4000)
	public java.lang.String getRebackreason(){
		return this.rebackreason;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  退单原因
	 */
	public void setRebackreason(java.lang.String rebackreason){
		this.rebackreason = rebackreason;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  退单附件
	 */
	@Column(name ="REBACKATTACH",nullable=false,length=4000)
	public java.lang.String getRebackattach(){
		return this.rebackattach;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  退单附件
	 */
	public void setRebackattach(java.lang.String rebackattach){
		this.rebackattach = rebackattach;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  退单审核说明
	 */
	@Column(name ="AUDITREMARK",nullable=true,length=4000)
	public java.lang.String getAuditremark(){
		return this.auditremark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  退单审核说明
	 */
	public void setAuditremark(java.lang.String auditremark){
		this.auditremark = auditremark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  申请退单人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLIENT")
	public TSUser getClient(){
		return this.client;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  申请退单人
	 */
	public void setClient(TSUser client){
		this.client = client;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  申请时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  申请时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  审核人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUDITOR")
	public TSUser getAuditor(){
		return this.auditor;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  审核人
	 */
	public void setAuditor(TSUser auditor){
		this.auditor = auditor;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  审核时间
	 */
	@Column(name ="AUDITTIME",nullable=true)
	public java.util.Date getAudittime(){
		return this.audittime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  审核时间
	 */
	public void setAudittime(java.util.Date audittime){
		this.audittime = audittime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  修改时间
	 */
	@Column(name ="UPDATETIME",nullable=true)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  修改时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cancelid")
	public List<OrderRebackDetailEntity> getCancelOrderDetailList() {
		return cancelOrderDetailList;
	}

	public void setCancelOrderDetailList(List<OrderRebackDetailEntity> cancelOrderDetailList) {
		this.cancelOrderDetailList = cancelOrderDetailList;
	}
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid() {
		return companyid;
	}

	public void setCompanyid(TSCompany companyid) {
		this.companyid = companyid;
	}

	@Column(name ="PRODUCTREBACK",nullable=true,length=2)
	public String getProductreback() {
		return productreback;
	}

	public void setProductreback(String productreback) {
		this.productreback = productreback;
	}
}
