package cn.gov.xnc.admin.thirdorder.service.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import cn.gov.xnc.admin.thirdorder.entity.ThirdorderConfigureEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderImportEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderOrderEntity;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderOrderServiceI;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("thirdorderOrderService")
@Transactional(rollbackForClassName={"Exception"})
public class ThirdorderOrderServiceImpl extends CommonServiceImpl implements ThirdorderOrderServiceI {

	@Override
	public AjaxJson importOrder(ThirdorderConfigureEntity config, HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		AjaxJson j = new AjaxJson();
		Map<String,Object> attribute = new HashMap<String, Object>();
		TSUser user = ResourceUtil.getSessionUserName();	
		
		String importname = request.getParameter("name");
		
		try {
		
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
			String fileName = "";
			
			for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
				List<ThirdorderOrderEntity> orderlist = new ArrayList<ThirdorderOrderEntity>();
				ThirdorderImportEntity importEntity = new ThirdorderImportEntity();
				
				MultipartFile file = entity.getValue();// 获取上传文件对象
				fileName = file.getName();
				
				
				String originalFilename = file.getOriginalFilename();
				attribute.put("fileid", originalFilename);
				String expname = originalFilename.substring(originalFilename.lastIndexOf("."));
				if( !".xls".equals(expname) && !".xlsx".equals(expname) && !".csv".equals(expname)){
					throw new Exception("导入的表格文件只支持xls和xlsx文件格式");
				}
				
				
				importEntity.setChannelid(config.getChannelid());
				importEntity.setCompany(user.getCompany());
				importEntity.setCreatetime(DateUtils.getDate());
				importEntity.setCreateuser(user);
				importEntity.setFilename(file.getOriginalFilename());
				importEntity.setFullpath(request.getSession().getServletContext().getRealPath("/")+"/uploads/"+file.getName());
				importEntity.setId(IdWorker.generateSequenceNo());
				importEntity.setStatus(ThirdorderImportEntity.STATUS_NORMAL);
				
				
				InputStream inputstream;
			
				inputstream = file.getInputStream();
			
				if( ".csv".equals(expname) ){//csv文件
					List<ThirdorderOrderEntity> list = getCSVEntity(inputstream,config,importEntity);
					orderlist.addAll(list);
				}else{
			
					Workbook book = null;
					boolean isXSSFWorkbook = true;
					if (!(inputstream.markSupported())) {
						inputstream = new PushbackInputStream(inputstream, 8);
					}
					if (POIFSFileSystem.hasPOIFSHeader(inputstream)) {
						book =  new HSSFWorkbook(inputstream);
						isXSSFWorkbook = false;
					}else if (POIXMLDocument.hasOOXMLHeader(inputstream)) {
						book =  new XSSFWorkbook(OPCPackage.open(inputstream));
					}
					
					int sheetnum = book.getNumberOfSheets();
					for(int i = 0 ; i < sheetnum; i++ ){
						Sheet sheet = book.getSheetAt(i);
						
						//获得一个 sheet 中合并单元格的数量    
				        int sheetmergerCount = sheet.getNumMergedRegions();
						if( sheetmergerCount > 0 ){
							throw new Exception("导入的表格不能有合并单元格，请对表格处理后再重新导入");
						}
						
						List<ThirdorderOrderEntity> list = getExcelSheetEntity(config,importEntity,sheet);
						
						orderlist.addAll(list);
					}
				}
				
				save(importEntity);
				batchSave(orderlist);
				
				for( ThirdorderOrderEntity order : orderlist ){
					StringBuffer sql = new StringBuffer();
					sql.append("insert ignore into thirdorder_product(company,productname,status) values(");
					sql.append("'").append(user.getCompany().getId()).append("',");
					sql.append("'").append(order.getOgProductname()).append("',");
					sql.append("").append(0).append(")");
					this.executeSql(sql.toString());
				}
						
			}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if( StringUtil.isNotEmpty(e.getMessage())){
				j.setMsg(e.getMessage());
			}
			j.setSuccess(false);
			return j;
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if( StringUtil.isNotEmpty(e.getMessage())){
				j.setMsg(e.getMessage());
			}
			j.setSuccess(false);
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if( StringUtil.isNotEmpty(e.getMessage())){
				j.setMsg(e.getMessage());
			}else{
				j.setMsg("上传失败，请检查后重试");
			}
			j.setSuccess(false);
			return j;
		}
		
		j.setAttributes(attribute);
		j.setMsg("导入成功");
		j.setSuccess(true);
		
		
		return j;
	}
	
	private List<ThirdorderOrderEntity> getCSVEntity(InputStream inputstream, ThirdorderConfigureEntity config,ThirdorderImportEntity importEntity) throws Exception{ 
		List<ThirdorderOrderEntity> list = new ArrayList<ThirdorderOrderEntity>();
		BufferedReader buffreader = new BufferedReader(new InputStreamReader(inputstream));
		
		String row="";
		int index = 0;
		while((row=buffreader.readLine())!=null){//一行一行读
			index++;
			if( index < config.getBeginrow()){
				continue;
			}
			
			ThirdorderOrderEntity order = getCSVRowEntity(config, importEntity, row, index);
			list.add(order);
		}
		
		buffreader.close();
		
		return list;
	}
	
	private ThirdorderOrderEntity getCSVRowEntity(ThirdorderConfigureEntity config,ThirdorderImportEntity importEntity, String row, int rowindex) throws Exception{
		ThirdorderOrderEntity thirdorder = new ThirdorderOrderEntity();
		
		String[] cell = row.split(",");
		
		if( StringUtil.isNotEmpty(config.getOrderidCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getOrderidCol())];
				thirdorder.setOgOrderid(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getOrderidCol() +"列订单ID数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getProductcodeCol()) ){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getProductcodeCol())];
				thirdorder.setOgProductcode(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getProductcodeCol() +"列产品唯一编码数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getProductnameCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getProductnameCol())];
				thirdorder.setOgProductname(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getProductnameCol() +"列产品名称数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getProductnumCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getProductnumCol())];
				thirdorder.setOgProductnum(new BigDecimal(cellvalue));
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getProductnumCol() +"列订单产品数量数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getOrderpriceCol())){
			
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getOrderpriceCol())];
				thirdorder.setOgOrderprice(new BigDecimal(cellvalue));
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getOrderpriceCol() +"列订单价格数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getOrdertimeCol())){
			
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getOrdertimeCol())];
				thirdorder.setOgOrdertime(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getOrdertimeCol() +"列订单时间数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getOrderpaytimeCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getOrderpaytimeCol())];
				thirdorder.setOgOrderpaytime(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getOrderpaytimeCol() +"列订单时间数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getBuyerCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getBuyerCol())];
				thirdorder.setOgBuyer(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getBuyerCol() +"列订单时间数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getBuyeraddressCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getBuyeraddressCol())];
				thirdorder.setOgBuyeraddress(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getBuyeraddressCol() +"列收货地址数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getBuyermobileCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getBuyermobileCol())];
				thirdorder.setOgBuyermobile(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getBuyermobileCol() +"列收货手机号码数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getFreightCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getFreightCol())];
				thirdorder.setOgFreight(new BigDecimal(cellvalue));
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getFreightCol() +"列运费数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getFreightcompanyCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getFreightcompanyCol())];
				thirdorder.setOgFreightcompany(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getFreightcompanyCol() +"列快递公司数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getFreightcodeCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getFreightcodeCol())];
				thirdorder.setOgFreightcode(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getFreightcodeCol() +"列快递单号数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getStatusCol())){
			try{
				String cellvalue = cell[CellReference.convertColStringToIndex(config.getStatusCol())];
				thirdorder.setOgStatus(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + rowindex + "行第"+config.getStatusCol() +"列订单状态数据格式有误，请检查后重试");
			}
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		thirdorder.setStatus(ThirdorderOrderEntity.STATUS_IMPORT);
		thirdorder.setCreatetime(DateUtils.getDate());
		thirdorder.setChannelid(config.getChannelid());
		thirdorder.setCompany(user.getCompany());
		thirdorder.setImportid(importEntity);
		thirdorder.setId(IdWorker.generateSequenceNo());
		
		return thirdorder;
	}
	
	private List<ThirdorderOrderEntity> getExcelSheetEntity(ThirdorderConfigureEntity config,ThirdorderImportEntity importEntity,  Sheet sheet ) throws Exception{
		
		List<ThirdorderOrderEntity> list = new ArrayList<ThirdorderOrderEntity>();
		
		Iterator<Row> rows = sheet.rowIterator();
		Row row = null;
		for (int j = 1; j < config.getBeginrow(); j++) {
			row = rows.next();
			if(row.getRowNum()+2==(config.getBeginrow())){
				break;
			}
		}
		
		while (rows.hasNext()) {
			row = rows.next();
			ThirdorderOrderEntity order = getExcelRowEntity(config,importEntity,row);
			list.add(order);
		}
		
		return list;
	}
	
	
	
	private ThirdorderOrderEntity getExcelRowEntity(ThirdorderConfigureEntity config,ThirdorderImportEntity importEntity, Row row) throws Exception{
		
		ThirdorderOrderEntity thirdorder = new ThirdorderOrderEntity();
		
		if( StringUtil.isNotEmpty(config.getOrderidCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getOrderidCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgOrderid(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getOrderidCol() +"列订单ID数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getProductcodeCol()) ){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getProductcodeCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgProductcode(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getProductcodeCol() +"列产品唯一编码数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getProductnameCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getProductnameCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgProductname(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getProductnameCol() +"列产品名称数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getProductnumCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getProductnumCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgProductnum(new BigDecimal(cellvalue));
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getProductnumCol() +"列订单产品数量数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getOrderpriceCol())){
			
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getOrderpriceCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgOrderprice(new BigDecimal(cellvalue));
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getOrderpriceCol() +"列订单价格数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getOrdertimeCol())){
			
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getOrdertimeCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgOrdertime(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getOrdertimeCol() +"列订单时间数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getOrderpaytimeCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getOrderpaytimeCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgOrderpaytime(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getOrderpaytimeCol() +"列订单时间数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getBuyerCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getBuyerCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgBuyer(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getBuyerCol() +"列订单时间数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getBuyeraddressCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getBuyeraddressCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgBuyeraddress(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getBuyeraddressCol() +"列收货地址数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getBuyermobileCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getBuyermobileCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgBuyermobile(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getBuyermobileCol() +"列收货手机号码数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getFreightCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getFreightCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgFreight(new BigDecimal(cellvalue));
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getFreightCol() +"列运费数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getFreightcompanyCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getFreightcompanyCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgFreightcompany(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getFreightcompanyCol() +"列快递公司数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getFreightcodeCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getFreightcodeCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgFreightcode(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getFreightcodeCol() +"列快递单号数据格式有误，请检查后重试");
			}
		}
		
		if( StringUtil.isNotEmpty(config.getStatusCol())){
			try{
				Cell cell = row.getCell(CellReference.convertColStringToIndex(config.getStatusCol()));
				String cellvalue = getCellValue(cell);
				thirdorder.setOgStatus(cellvalue);
			}catch (Exception e) {
				// TODO: handle exception
				throw new Exception("导入的表格第" + row.getRowNum() + "行第"+config.getStatusCol() +"列订单状态数据格式有误，请检查后重试");
			}
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		thirdorder.setStatus(ThirdorderOrderEntity.STATUS_IMPORT);
		thirdorder.setCreatetime(DateUtils.getDate());
		thirdorder.setChannelid(config.getChannelid());
		thirdorder.setCompany(user.getCompany());
		thirdorder.setImportid(importEntity);
		thirdorder.setId(IdWorker.generateSequenceNo());
		
		return thirdorder;
	}
	
	
	
	
	/**    
	    * 获取单元格的值    
	    * @param cell    
	    * @return    
	    */      
	    public String getCellValue(Cell cell){   
	    	
	    	if(cell == null) return "";      
	        if(cell.getCellType() == Cell.CELL_TYPE_STRING){      
	            return cell.getStringCellValue();      
	        }else if(cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){      
	            return String.valueOf(cell.getBooleanCellValue());      
	        }else if(cell.getCellType() == Cell.CELL_TYPE_FORMULA){      
	            return cell.getCellFormula() ;      
	        }else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){      
	            return String.valueOf(cell.getNumericCellValue());      
	        }  
	        return "";
	    }
	    
	    
	    /* 合并单元格处理,获取合并行  
	     * @param sheet  
	     * @return List<CellRangeAddress>  
	     */    
	     public List<CellRangeAddress> getCombineCell(Sheet sheet)    
	     {    
	         List<CellRangeAddress> list = new ArrayList<CellRangeAddress>();    
	         //获得一个 sheet 中合并单元格的数量    
	         int sheetmergerCount = sheet.getNumMergedRegions();    
	         //遍历所有的合并单元格    
	         for(int i = 0; i<sheetmergerCount;i++)     
	         {    
	             //获得合并单元格保存进list中    
	             CellRangeAddress ca = sheet.getMergedRegion(i);    
	             list.add(ca);    
	         }    
	         return list;    
	     }  
	     
	     
	     private int getRowNum(List<CellRangeAddress> listCombineCell,Cell cell,Sheet sheet){  
	         int xr = 0;  
	         int firstC = 0;    
	         int lastC = 0;    
	         int firstR = 0;    
	         int lastR = 0;    
	         for(CellRangeAddress ca:listCombineCell)    
	         {  
	             //获得合并单元格的起始行, 结束行, 起始列, 结束列    
	             firstC = ca.getFirstColumn();    
	             lastC = ca.getLastColumn();    
	             firstR = ca.getFirstRow();    
	             lastR = ca.getLastRow();    
	             if(cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR)     
	             {    
	                 if(cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC)     
	                 {    
	                     xr = lastR;  
	                 }   
	             }    
	               
	         }  
	         return xr;  
	           
	     }  
	     /**  
	      * 判断单元格是否为合并单元格，是的话则将单元格的值返回  
	      * @param listCombineCell 存放合并单元格的list  
	      * @param cell 需要判断的单元格  
	      * @param sheet sheet  
	      * @return  
	      */   
	      public String isCombineCell(List<CellRangeAddress> listCombineCell,Cell cell,Sheet sheet)  
	      throws Exception{   
	          int firstC = 0;    
	          int lastC = 0;    
	          int firstR = 0;    
	          int lastR = 0;    
	          String cellValue = null;    
	          for(CellRangeAddress ca:listCombineCell)    
	          {  
	              //获得合并单元格的起始行, 结束行, 起始列, 结束列    
	              firstC = ca.getFirstColumn();    
	              lastC = ca.getLastColumn();    
	              firstR = ca.getFirstRow();    
	              lastR = ca.getLastRow();    
	              if(cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR)     
	              {    
	                  if(cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC)     
	                  {    
	                      Row fRow = sheet.getRow(firstR);    
	                      Cell fCell = fRow.getCell(firstC);    
	                      cellValue = getCellValue(fCell);    
	                      break;    
	                  }   
	              }    
	              else    
	              {    
	                  cellValue = "";    
	              }    
	          }    
	          return cellValue;    
	      }  
	
	      /**    
	       * 获取合并单元格的值    
	       * @param sheet    
	       * @param row    
	       * @param column    
	       * @return    
	       */      
	       public String getMergedRegionValue(Sheet sheet ,int row , int column){      
	           int sheetMergeCount = sheet.getNumMergedRegions();      
	                 
	           for(int i = 0 ; i < sheetMergeCount ; i++){      
	               CellRangeAddress ca = sheet.getMergedRegion(i);      
	               int firstColumn = ca.getFirstColumn();      
	               int lastColumn = ca.getLastColumn();      
	               int firstRow = ca.getFirstRow();      
	               int lastRow = ca.getLastRow();      
	                     
	               if(row >= firstRow && row <= lastRow){      
	                   if(column >= firstColumn && column <= lastColumn){      
	                       Row fRow = sheet.getRow(firstRow);      
	                       Cell fCell = fRow.getCell(firstColumn);      
	                       return getCellValue(fCell) ;      
	                   }      
	               }      
	           }      
	                 
	           return null ;      
	       }  
	       
	       
	       /**   
	        * 判断指定的单元格是否是合并单元格   
	        * @param sheet    
	        * @param row 行下标   
	        * @param column 列下标   
	        * @return   
	        */    
	        private boolean isMergedRegion(Sheet sheet,int row ,int column) {    
	          int sheetMergeCount = sheet.getNumMergedRegions();    
	          for (int i = 0; i < sheetMergeCount; i++) {    
	            CellRangeAddress range = sheet.getMergedRegion(i);    
	            int firstColumn = range.getFirstColumn();    
	            int lastColumn = range.getLastColumn();    
	            int firstRow = range.getFirstRow();    
	            int lastRow = range.getLastRow();    
	            if(row >= firstRow && row <= lastRow){    
	                if(column >= firstColumn && column <= lastColumn){    
	                    return true;    
	                }    
	            }  
	          }    
	          return false;    
	        }
}