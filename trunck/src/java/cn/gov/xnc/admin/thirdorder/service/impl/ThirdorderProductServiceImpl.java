package cn.gov.xnc.admin.thirdorder.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.thirdorder.service.ThirdorderProductServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("thirdorderProductService")
@Transactional
public class ThirdorderProductServiceImpl extends CommonServiceImpl implements ThirdorderProductServiceI {
	
}