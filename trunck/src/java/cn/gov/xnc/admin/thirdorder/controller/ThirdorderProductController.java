package cn.gov.xnc.admin.thirdorder.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.thirdorder.entity.ThirdorderProductEntity;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderProductServiceI;

/**   
 * @Title: Controller
 * @Description: 第三方订单产品信息
 * @author zero
 * @date 2017-07-21 17:48:59
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/thirdorderProductController")
public class ThirdorderProductController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ThirdorderProductController.class);

	@Autowired
	private ThirdorderProductServiceI thirdorderProductService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 第三方订单产品信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView thirdorderProduct(HttpServletRequest request) {
		return new ModelAndView("admin/thirdorder/thirdorderProductList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ThirdorderProductEntity thirdorderProduct,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ThirdorderProductEntity.class, dataGrid);
		
		if( StringUtil.isNotEmpty(thirdorderProduct.getProductname()) ){
			cq.like("productname", "%"+thirdorderProduct.getProductname()+"%");
			thirdorderProduct.setProductname(null);
		}
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, thirdorderProduct, request.getParameterMap());
		this.thirdorderProductService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除第三方订单产品信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(ThirdorderProductEntity thirdorderProduct, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		thirdorderProduct = systemService.getEntity(ThirdorderProductEntity.class, thirdorderProduct.getProductname());
		message = "第三方订单产品信息删除成功";
		thirdorderProductService.delete(thirdorderProduct);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加第三方订单产品信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ThirdorderProductEntity thirdorderProduct, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(thirdorderProduct.getProductname())) {
			message = "第三方订单产品信息更新成功";
			ThirdorderProductEntity t = thirdorderProductService.get(ThirdorderProductEntity.class, thirdorderProduct.getProductname());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(thirdorderProduct, t);
				thirdorderProductService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "第三方订单产品信息更新失败";
			}
		} else {
			message = "第三方订单产品信息添加成功";
			thirdorderProductService.save(thirdorderProduct);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 第三方订单产品信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ThirdorderProductEntity thirdorderProduct, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(thirdorderProduct.getProductname())) {
			thirdorderProduct = thirdorderProductService.getEntity(ThirdorderProductEntity.class, thirdorderProduct.getProductname());
			req.setAttribute("thirdorderProductPage", thirdorderProduct);
		}
		return new ModelAndView("admin/thirdorder/thirdorderProduct");
	}
}
