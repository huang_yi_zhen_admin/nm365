package cn.gov.xnc.admin.thirdorder.service;

import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface ThirdorderImportServiceI extends CommonService{
	
	public AjaxJson batchDel(String ids);

}
