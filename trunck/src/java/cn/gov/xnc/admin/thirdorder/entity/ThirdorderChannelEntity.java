package cn.gov.xnc.admin.thirdorder.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 第三方订单渠道管理
 * @author zero
 * @date 2017-07-21 17:35:09
 * @version V1.0   
 *
 */
@Entity
@Table(name = "thirdorder_channel", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ThirdorderChannelEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**公司*/
	private TSCompany company;
	/**渠道名称*/
	private java.lang.String channelname;
	/**备注*/
	private java.lang.String remark;
	/**创建时间*/
	private java.util.Date createtime;
	/**创建者*/
	private TSUser createuser;
	/**状态，0正常，1已删除*/
	private java.lang.Integer status;
	
	public static final int STATUS_NORMAL = 0;
	public static final int STATUS_DEL = 1;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  渠道名称
	 */
	@Column(name ="CHANNELNAME",nullable=true,length=255)
	public java.lang.String getChannelname(){
		return this.channelname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  渠道名称
	 */
	public void setChannelname(java.lang.String channelname){
		this.channelname = channelname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=255)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=true)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建者
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATEUSER")
	public TSUser getCreateuser(){
		return this.createuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建者
	 */
	public void setCreateuser(TSUser createuser){
		this.createuser = createuser;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  状态，0正常，1已删除
	 */
	@Column(name ="STATUS",nullable=true,precision=10,scale=0)
	public java.lang.Integer getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  状态，0正常，1已删除
	 */
	public void setStatus(java.lang.Integer status){
		this.status = status;
	}

	/**
	 * @return the company
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
}
