package cn.gov.xnc.admin.thirdorder.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderChannelEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderConfigureEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderImportEntity;
import cn.gov.xnc.admin.thirdorder.entity.ThirdorderOrderEntity;
import cn.gov.xnc.admin.thirdorder.service.ThirdorderOrderServiceI;

/**   
 * @Title: Controller
 * @Description: 第三方订单
 * @author zero
 * @date 2017-07-21 17:48:18
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/thirdorderOrderController")
public class ThirdorderOrderController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ThirdorderOrderController.class);

	@Autowired
	private ThirdorderOrderServiceI thirdorderOrderService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 第三方订单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView thirdorderOrder(HttpServletRequest request) {
		return new ModelAndView("admin/thirdorder/thirdorderOrderList");
	}
	
	/**
	 * 导入订单数据
	 *
	 * @return
	 */
	@RequestMapping(value = "thirdorderImportXls")
	public ModelAndView importOrderXls(HttpServletRequest req) {
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cb = new CriteriaQuery(ThirdorderConfigureEntity.class);
		cb.eq("company", user.getCompany());
		cb.eq("status", ThirdorderConfigureEntity.STATUS_NORMAL);
		cb.add();
		List<ThirdorderConfigureEntity> configurelist = systemService.getListByCriteriaQuery(cb, false);
		req.setAttribute("configurelist", configurelist);
		
		return new ModelAndView("admin/thirdorder/thirdorderImportXls");
		
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(ThirdorderOrderEntity thirdorderOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ThirdorderOrderEntity.class, dataGrid);
		
		cq.notEq("status", ThirdorderOrderEntity.STATUS_DEL);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, thirdorderOrder, request.getParameterMap());
		this.thirdorderOrderService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 导入订单数据处理
	 *
	 * @return
	 * 
	 */
	@RequestMapping(value = "importOrder", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importOrder(HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		
		String configid = request.getParameter("configid");
		if( StringUtil.isEmpty(configid) ){
			j.setSuccess(false);
			j.setMsg("请先选择导入配置");
			return j;
		}
		
		ThirdorderConfigureEntity config = thirdorderOrderService.getEntity(ThirdorderConfigureEntity.class, configid);
		if( null == config ){
			j.setSuccess(false);
			j.setMsg("输入参数有误，请重新输入");
			return j;
		}
		
		j = thirdorderOrderService.importOrder(config,request,response);
		
		
		return j;
		
	}

	/**
	 * 删除第三方订单
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		if( StringUtil.isEmpty(ids)){
			j.setMsg("参数有误，请稍后重试");
			j.setSuccess(false);
			return j;
		}
		
		CriteriaQuery cq = new CriteriaQuery(ThirdorderOrderEntity.class);
		cq.eq("company", user.getCompany());
		cq.in("id", ids.split(","));
		List<ThirdorderOrderEntity> orderlist = thirdorderOrderService.getListByCriteriaQuery(cq, false);
		if( null == orderlist || orderlist.size() <= 0 ){
			j.setMsg("参数有误，请稍后重试");
			j.setSuccess(false);
			return j;
		}
		
		for( ThirdorderOrderEntity order : orderlist ){
			order.setStatus(ThirdorderOrderEntity.STATUS_DEL);
		}
		
		thirdorderOrderService.batchUpdate(orderlist);
		
		j.setMsg("删除成功");
		j.setSuccess(true);
		return j;
	}


	/**
	 * 添加第三方订单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(ThirdorderOrderEntity thirdorderOrder, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(thirdorderOrder.getId())) {
			message = "第三方订单更新成功";
			ThirdorderOrderEntity t = thirdorderOrderService.get(ThirdorderOrderEntity.class, thirdorderOrder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(thirdorderOrder, t);
				thirdorderOrderService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "第三方订单更新失败";
			}
		} else {
			message = "第三方订单添加成功";
			thirdorderOrderService.save(thirdorderOrder);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 第三方订单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(ThirdorderOrderEntity thirdorderOrder, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(thirdorderOrder.getId())) {
			thirdorderOrder = thirdorderOrderService.getEntity(ThirdorderOrderEntity.class, thirdorderOrder.getId());
			req.setAttribute("thirdorderOrderPage", thirdorderOrder);
		}
		return new ModelAndView("admin/thirdorder/thirdorderOrder");
	}
}
