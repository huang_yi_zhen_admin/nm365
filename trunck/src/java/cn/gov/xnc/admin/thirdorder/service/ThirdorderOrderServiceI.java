package cn.gov.xnc.admin.thirdorder.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gov.xnc.admin.thirdorder.entity.ThirdorderConfigureEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface ThirdorderOrderServiceI extends CommonService{
	
	public AjaxJson importOrder(ThirdorderConfigureEntity config,HttpServletRequest request, HttpServletResponse response);


}
