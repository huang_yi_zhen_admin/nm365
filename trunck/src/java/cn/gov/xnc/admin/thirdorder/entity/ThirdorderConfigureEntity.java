package cn.gov.xnc.admin.thirdorder.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 第三方订单导入配置
 * @author zero
 * @date 2017-07-21 17:46:38
 * @version V1.0   
 *
 */
@Entity
@Table(name = "thirdorder_configure", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ThirdorderConfigureEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**公司*/
	private TSCompany company;
	/**此配置名称*/
	private java.lang.String name;
	/**渠道id*/
	private ThirdorderChannelEntity channelid;
	/**导入文件起始行*/
	private java.lang.Integer beginrow;
	/**订单id所在列*/
	private java.lang.String orderidCol;
	/**商品唯一编码所在列*/
	private java.lang.String productcodeCol;
	/**商品名称所在列*/
	private java.lang.String productnameCol;
	/**商品数量所在列*/
	private java.lang.String productnumCol;
	/**订单价格所在列*/
	private java.lang.String orderpriceCol;
	/**下单时间所在列*/
	private java.lang.String ordertimeCol;
	/**订单支付时间所在列*/
	private java.lang.String orderpaytimeCol;
	/**购买用户所在列*/
	private java.lang.String buyerCol;
	/**购买地址所在列*/
	private java.lang.String buyeraddressCol;
	/**购买用户手机所在列*/
	private java.lang.String buyermobileCol;
	/**运费所在列*/
	private java.lang.String freightCol;
	/**快递公司所在列*/
	private java.lang.String freightcompanyCol;
	/**快递单号所在列*/
	private java.lang.String freightcodeCol;
	/**订单状态所在列*/
	private java.lang.String statusCol;
	/**状态 0正常，1已删除*/
	private java.lang.Integer status;
	/**创建者*/
	private TSUser createuser;
	/**创建时间*/
	private java.util.Date createtime;
	public static final int STATUS_NORMAL = 0;
	public static final int STATUS_DEL = 1;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  渠道id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANNELID")
	public ThirdorderChannelEntity getChannelid(){
		return this.channelid;
	}
	
	
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=true)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  渠道id
	 */
	public void setChannelid(ThirdorderChannelEntity channelid){
		this.channelid = channelid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入文件起始行
	 */
	@Column(name ="BEGINROW",nullable=true,length=10)
	public java.lang.Integer getBeginrow(){
		return this.beginrow;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入文件起始行
	 */
	public void setBeginrow(java.lang.Integer beginrow){
		this.beginrow = beginrow;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单id所在列
	 */
	@Column(name ="ORDERID_COL",nullable=true,length=8)
	public java.lang.String getOrderidCol(){
		return this.orderidCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单id所在列
	 */
	public void setOrderidCol(java.lang.String orderidCol){
		this.orderidCol = orderidCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品唯一编码所在列
	 */
	@Column(name ="PRODUCTCODE_COL",nullable=true,length=8)
	public java.lang.String getProductcodeCol(){
		return this.productcodeCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品唯一编码所在列
	 */
	public void setProductcodeCol(java.lang.String productcodeCol){
		this.productcodeCol = productcodeCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品名称所在列
	 */
	@Column(name ="PRODUCTNAME_COL",nullable=true,length=8)
	public java.lang.String getProductnameCol(){
		return this.productnameCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品名称所在列
	 */
	public void setProductnameCol(java.lang.String productnameCol){
		this.productnameCol = productnameCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品数量所在列
	 */
	@Column(name ="PRODUCTNUM_COL",nullable=true,length=8)
	public java.lang.String getProductnumCol(){
		return this.productnumCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品数量所在列
	 */
	public void setProductnumCol(java.lang.String productnumCol){
		this.productnumCol = productnumCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  下单时间所在列
	 */
	@Column(name ="ORDERTIME_COL",nullable=true,length=8)
	public java.lang.String getOrdertimeCol(){
		return this.ordertimeCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  下单时间所在列
	 */
	public void setOrdertimeCol(java.lang.String ordertimeCol){
		this.ordertimeCol = ordertimeCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单支付时间所在列
	 */
	@Column(name ="ORDERPAYTIME_COL",nullable=true,length=8)
	public java.lang.String getOrderpaytimeCol(){
		return this.orderpaytimeCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单支付时间所在列
	 */
	public void setOrderpaytimeCol(java.lang.String orderpaytimeCol){
		this.orderpaytimeCol = orderpaytimeCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  购买用户所在列
	 */
	@Column(name ="BUYER_COL",nullable=true,length=8)
	public java.lang.String getBuyerCol(){
		return this.buyerCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  购买用户所在列
	 */
	public void setBuyerCol(java.lang.String buyerCol){
		this.buyerCol = buyerCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  购买地址所在列
	 */
	@Column(name ="BUYERADDRESS_COL",nullable=true,length=8)
	public java.lang.String getBuyeraddressCol(){
		return this.buyeraddressCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  购买地址所在列
	 */
	public void setBuyeraddressCol(java.lang.String buyeraddressCol){
		this.buyeraddressCol = buyeraddressCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  购买用户手机所在列
	 */
	@Column(name ="BUYERMOBILE_COL",nullable=true,length=8)
	public java.lang.String getBuyermobileCol(){
		return this.buyermobileCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  购买用户手机所在列
	 */
	public void setBuyermobileCol(java.lang.String buyermobileCol){
		this.buyermobileCol = buyermobileCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  运费所在列
	 */
	@Column(name ="FREIGHT_COL",nullable=true,length=8)
	public java.lang.String getFreightCol(){
		return this.freightCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  运费所在列
	 */
	public void setFreightCol(java.lang.String freightCol){
		this.freightCol = freightCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递公司所在列
	 */
	@Column(name ="FREIGHTCOMPANY_COL",nullable=true,length=8)
	public java.lang.String getFreightcompanyCol(){
		return this.freightcompanyCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递公司所在列
	 */
	public void setFreightcompanyCol(java.lang.String freightcompanyCol){
		this.freightcompanyCol = freightcompanyCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  快递单号所在列
	 */
	@Column(name ="FREIGHTCODE_COL",nullable=true,length=8)
	public java.lang.String getFreightcodeCol(){
		return this.freightcodeCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  快递单号所在列
	 */
	public void setFreightcodeCol(java.lang.String freightcodeCol){
		this.freightcodeCol = freightcodeCol;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  订单状态所在列
	 */
	@Column(name ="STATUS_COL",nullable=true,length=8)
	public java.lang.String getStatusCol(){
		return this.statusCol;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  订单状态所在列
	 */
	public void setStatusCol(java.lang.String statusCol){
		this.statusCol = statusCol;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  状态 0正常，1已删除
	 */
	@Column(name ="STATUS",nullable=true,precision=10,scale=0)
	public java.lang.Integer getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  状态 0正常，1已删除
	 */
	public void setStatus(java.lang.Integer status){
		this.status = status;
	}

	/**
	 * @return the name
	 */
	@Column(name ="NAME",nullable=true,length=255)
	public java.lang.String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建者
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATEUSER")
	public TSUser getCreateuser(){
		return this.createuser;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建者
	 */
	public void setCreateuser(TSUser createuser){
		this.createuser = createuser;
	}
	
	/**
	 * @return the company
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}

	/**
	 * @return the orderpriceCol
	 */
	@Column(name ="ORDERPRICE_COL",nullable=true,length=8)
	public java.lang.String getOrderpriceCol() {
		return orderpriceCol;
	}

	/**
	 * @param orderpriceCol the orderpriceCol to set
	 */
	public void setOrderpriceCol(java.lang.String orderpriceCol) {
		this.orderpriceCol = orderpriceCol;
	}
}
