package cn.gov.xnc.admin.ylb.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserService;
import cn.gov.xnc.system.core.util.MyBeanUtils;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.ylb.entity.YlbSalesEntity;
import cn.gov.xnc.admin.ylb.service.YlbSalesServiceI;

/**   
 * @Title: Controller
 * @Description: 合同提报成功
 * @author zero
 * @date 2018-01-04 16:01:32
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/ylbSalesController")
public class YlbSalesController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(YlbSalesController.class);

	@Autowired
	private YlbSalesServiceI ylbSalesService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserService userService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 合同提报成功列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView ylbSales(HttpServletRequest request) {
		return new ModelAndView("admin/ylb/ylbSalesList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(YlbSalesEntity ylbSales,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(YlbSalesEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, ylbSales, request.getParameterMap());
		this.ylbSalesService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除合同提报成功
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(YlbSalesEntity ylbSales, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		ylbSales = systemService.getEntity(YlbSalesEntity.class, ylbSales.getId());
		message = "合同提报删除成功";
		ylbSalesService.delete(ylbSales);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加合同提报成功
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(YlbSalesEntity ylbSales, HttpServletRequest request) {
		
		
		String salestime =request.getParameter("salestime");
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(ylbSales.getId())) {
			message = "合同提报更新成功";
			YlbSalesEntity t = ylbSalesService.get(YlbSalesEntity.class, ylbSales.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(ylbSales, t);
				ylbSalesService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "合同提报更新失败";
			}
		} else {
			message = "合同提报添加成功";
			ylbSales.setState("1");
			ylbSales.setCreatedate(new Date());
			
			ylbSalesService.save(ylbSales);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 合同提报成功列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(YlbSalesEntity ylbSales, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(ylbSales.getId())) {
			ylbSales = ylbSalesService.getEntity(YlbSalesEntity.class, ylbSales.getId());
			
			try{
				TSTerritory top1Territory = ylbSales.getTSTerritory();
				TSTerritory top2Territory = null;
				TSTerritory top3Territory = null;
				
				if(null != top1Territory && StringUtil.isNotEmpty(top1Territory.getId())){
					top2Territory = top1Territory.getTSTerritory();
					req.setAttribute("province", top1Territory.getId());
				}
				if(null != top2Territory && StringUtil.isNotEmpty(top2Territory.getId())){
					top3Territory = top2Territory.getTSTerritory();
					req.setAttribute("province", top2Territory.getId());
					req.setAttribute("city", top1Territory.getId());
				}
				if(null != top3Territory && StringUtil.isNotEmpty(top3Territory.getId())){
					
					req.setAttribute("province", top3Territory.getId());
					req.setAttribute("city", top2Territory.getId());
					req.setAttribute("area", top1Territory.getId());
				}
				
				
			}catch (Exception e) {
				req.setAttribute("province", null);
				req.setAttribute("city", null);
				req.setAttribute("area", null);
				
			}
			
			
			
		}else{
			ylbSales = new YlbSalesEntity();
		}
		req.setAttribute("ylbSalesPage", ylbSales);
		return new ModelAndView("admin/ylb/ylbSales");
	}
	
	
	
	/**
	 * 数据统计查询列表 页面跳转
	 * 
	 * @return
	 */
	
	@RequestMapping(value = "ylbSalesDatalist")
	public ModelAndView ylbSalesDatalist(HttpServletRequest request) {
		
		
		TSUser user = ResourceUtil.getSessionUserName();
		//获取全部销售果品信息
		CriteriaQuery cq_p = new CriteriaQuery(ProductEntity.class);
		cq_p.eq("company", user.getCompany());
		
		String sql = "SELECT id, realname from t_s_user where type=3 and company='"+user.getCompany().getId()+"'";
		List<TSUser> userListType3 = userService.queryListByJdbc(sql, TSUser.class);
		request.setAttribute("userListType3", userListType3);
		return new ModelAndView("admin/ylb/ylbSalesData");
	}
	
	/**
	 * 渠道销售情况
	 * 
	 * @param 渠道销售情况
	 * @return
	 */
	@RequestMapping(value = "getylbSalesData")
	public void getylbSalesData( HttpServletRequest request , HttpServletResponse response, DataGrid dataGrid ) {
		
		String productid =request.getParameter("productid");
		String startTime =request.getParameter("dateBegin");
		String endTime =request.getParameter("dateEnd");
		
		if( StringUtil.isNotEmpty(startTime) ){
			startTime =startTime.trim()+" 00:00:00";
		}
		if( StringUtil.isNotEmpty(endTime) ){
			endTime =endTime.trim()+" 24:00:00";
		}
		
		TSUser user = ResourceUtil.getSessionUserName();	
		String companyId = user.getCompany().getId();
		
		List<YlbSalesEntity> ylbSalesList  = new ArrayList<YlbSalesEntity>()  ;
				//orderService.salesQDXS( companyId ,productid ,  startTime, endTime );
		
		if( ylbSalesList != null && ylbSalesList.size() > 0){
//			for( YlbSalesEntity o:ylbSalesList ){
//				TSUser t = new TSUser();
//				if( StringUtil.isNotEmpty(o.getId())){
//					t = systemService.findUniqueByProperty (TSUser.class,"id" ,o.getId());
//			    }
//				o.setClientid(t);
//			}
		}

		//根据分页构建分页list
		dataGrid.setTotal(ylbSalesList.size());
		dataGrid.setResults(ylbSalesList);

		//创建图形报表的json
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject object = TagUtil.getdataJson(dataGrid);
		try {
			PrintWriter pw=response.getWriter();
			pw.write(object.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
