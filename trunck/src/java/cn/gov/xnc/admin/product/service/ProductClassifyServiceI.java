package cn.gov.xnc.admin.product.service;

import java.util.List;

import cn.gov.xnc.admin.product.entity.ProductClassifyEntity;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface ProductClassifyServiceI extends CommonService{
	/**
	 * 获取商品的所有分类
	 * @param productClassify
	 * @return
	 */
	public List<ProductClassifyEntity> getAllProductClassify();

}
