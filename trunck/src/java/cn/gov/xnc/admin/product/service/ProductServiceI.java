package cn.gov.xnc.admin.product.service;

import java.math.BigDecimal;

import cn.gov.xnc.system.core.common.service.CommonService;

public interface ProductServiceI extends CommonService{
	
	/**
	 * 获取商品基础价格
	 * @param productid
	 * @return
	 */
	public BigDecimal getProductBaseCost(String productid);
	
}
