package cn.gov.xnc.admin.product.service.impl;



import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.service.ProductServiceI;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;


@Service("productService")

public class ProductServiceImpl extends CommonServiceImpl implements ProductServiceI {

	private Logger logger = Logger.getLogger(ProductServiceImpl.class);
	@Autowired
	private StockProductPriceServiceI stockProductPriceService;
	
	@Override
	public BigDecimal getProductBaseCost(String productid) {
		BigDecimal baseCost = new BigDecimal(0.00);
		ProductEntity product = findUniqueByProperty(ProductEntity.class, "id", productid);
		try {
			StockProductPriceEntity stockProductPrice = stockProductPriceService.getStockProductPriceInfo(productid);
			if(null != stockProductPrice && null != stockProductPrice.getAveragePrice()){
				baseCost = stockProductPrice.getAveragePrice();
			
			}else{
				
				if(null != product.getPricec()){
					baseCost = product.getPricec();
				}
				
			}
			
		} catch (Exception e) {
			logger.error(e);
		}
		
		return baseCost;
	}
	
	
}