package cn.gov.xnc.admin.product.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;


/**   
 * @Title: Entity
 * @Description: 产品分类
 * @author zero
 * @date 2016-09-28 15:46:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_product_classify", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ProductClassifyEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**分类名称*/
	private java.lang.String classifyname;
	/**级别*/
	private Short level;
	/**上级id*/
	private java.lang.String fatherid;
	/**说明*/
	private java.lang.String caption;
	/**介绍*/
	private java.lang.String remarks;
	/**公司信息*/
	private TSCompany company;
	/**使用状态 U 正在使用 D 已删除*/
//	private java.lang.String status;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  分类名称
	 */
	@Column(name ="CLASSIFYNAME",nullable=true,length=300)
	public java.lang.String getClassifyname(){
		return this.classifyname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  分类名称
	 */
	public void setClassifyname(java.lang.String classifyname){
		this.classifyname = classifyname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  级别
	 */
	@Column(name ="LEVEL",nullable=true,length=10)
	public Short getLevel(){
		return this.level;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  级别
	 */
	public void setLevel(Short level){
		this.level = level;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  上级id
	 */
	@Column(name ="FATHERID",nullable=true,length=32)
	public java.lang.String getFatherid(){
		return this.fatherid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  上级id
	 */
	public void setFatherid(java.lang.String fatherid){
		this.fatherid = fatherid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  说明
	 */
	@Column(name ="CAPTION",nullable=true,length=2000)
	public java.lang.String getCaption(){
		return this.caption;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  说明
	 */
	public void setCaption(java.lang.String caption){
		this.caption = caption;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  介绍
	 */
	@Column(name ="REMARKS",nullable=true,length=5000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  介绍
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
	/**
	 *方法: 设置java.lang.String 公司信息
	 *@param: java.lang.String  公司信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}
	/**
	 *方法: 设置java.lang.String  公司信息
	 *@param: java.lang.String  公司信息
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}

	/*public java.lang.String getStatus() {
		return status;
	}

	public void setStatus(java.lang.String status) {
		this.status = status;
	}*/
}
