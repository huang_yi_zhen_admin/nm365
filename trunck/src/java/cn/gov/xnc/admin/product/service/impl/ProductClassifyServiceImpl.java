package cn.gov.xnc.admin.product.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductClassifyEntity;
import cn.gov.xnc.admin.product.service.ProductClassifyServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("productClassifyService")

public class ProductClassifyServiceImpl extends CommonServiceImpl implements ProductClassifyServiceI {
	@Override
	public List<ProductClassifyEntity> getAllProductClassify() {
		TSUser user = ResourceUtil.getSessionUserName();
		
		CriteriaQuery cq = new CriteriaQuery(ProductClassifyEntity.class);
		//cq.eq("status", "U");
		cq.eq("company", user.getCompany());
		cq.add();
		
		return getListByCriteriaQuery(cq, false);
	}
	
}