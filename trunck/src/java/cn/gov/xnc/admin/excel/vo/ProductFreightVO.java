package cn.gov.xnc.admin.excel.vo;

import java.math.BigDecimal;

import cn.gov.xnc.system.excel.annotation.Excel;


/**   
 * @Title: Entity
 * @Description: 导入产品、运费模板
 * @author zero
 * @date 2016-04-13 20:22:17
 * @version V1.0   
 *
 */

public class ProductFreightVO {

	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**商品编码*/
	@Excel(exportName="商品编码",orderNum="1")
	private java.lang.String code;
	
	/**商品名称*/
	@Excel(exportName="商品名称",orderNum="2")
	private java.lang.String name;
	/**规格*/
	@Excel(exportName="规格",orderNum="3")
	private java.lang.String specifications;
	
	/**进货价格*/
	@Excel(exportName="进货价格",orderNum="4")
	private BigDecimal pricec;
	/**订货价格*/
	@Excel(exportName="订货价格",orderNum="5")
	private BigDecimal price;
	/**市场价格*/
	@Excel(exportName="市场价格",orderNum="6")
	private BigDecimal prices;
	/**业务奖励*/
	@Excel(exportName="业务奖励",orderNum="7")
	private BigDecimal priceyw;
	


	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导入的表格id
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}
	
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导入的表格id
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品编码
	 */
	public java.lang.String getCode() {
		return code;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品编码
	 */
	public void setCode(java.lang.String code) {
		this.code = code;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  商品名称
	 */
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  规格
	 */

	public java.lang.String getSpecifications(){
		return this.specifications;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  规格
	 */
	public void setSpecifications(java.lang.String specifications){
		this.specifications = specifications;
	}
	
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  价格
	 */
	public BigDecimal getPrice(){
		return this.price;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  价格
	 */
	public void setPrice(BigDecimal price){
		this.price = price;
	}

	/**
	 * @return 进货价格
	 */
	public BigDecimal getPricec() {
		return pricec;
	}

	/**
	 * @param 进货价格
	 */
	public void setPricec(BigDecimal pricec) {
		this.pricec = pricec;
	}

	/**
	 * @return 市场价格
	 */
	public BigDecimal getPrices() {
		return prices;
	}

	/**
	 * @param 市场价格
	 */
	public void setPrices(BigDecimal prices) {
		this.prices = prices;
	}

	/**
	 * @return 业务奖励
	 */
	public BigDecimal getPriceyw() {
		return priceyw;
	}

	/**
	 * @param 业务奖励
	 */
	public void setPriceyw(BigDecimal priceyw) {
		this.priceyw = priceyw;
	}
	
	
	

	
	
	


}
