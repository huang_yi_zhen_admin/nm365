package cn.gov.xnc.admin.excel.vo;

import cn.gov.xnc.system.excel.annotation.Excel;

public class StockSupplierExcel {

	public StockSupplierExcel() {
		// TODO Auto-generated constructor stub
	}
	
	/**导入的表格id*/
	@Excel(exportName="导入的表格id",orderNum="0")
	private java.lang.String excelId;
	
	/**供应商编码*/
	@Excel(exportName="供应商编码",orderNum="1")
	private java.lang.String code;
	
	/**供应商名称*/
	@Excel(exportName="供应商名称",orderNum="2")
	private java.lang.String name;
	
	/**供应商电话*/
	@Excel(exportName="供应商电话",orderNum="3")
	private java.lang.String tel;
	
	/**供应商地址*/
	@Excel(exportName="供应商地址",orderNum="4")
	private java.lang.String address;

	/**
	 * @return the excelId
	 */
	public java.lang.String getExcelId() {
		return excelId;
	}

	/**
	 * @param excelId the excelId to set
	 */
	public void setExcelId(java.lang.String excelId) {
		this.excelId = excelId;
	}

	/**
	 * @return the code
	 */
	public java.lang.String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(java.lang.String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public java.lang.String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}

	/**
	 * @return the tel
	 */
	public java.lang.String getTel() {
		return tel;
	}

	/**
	 * @param tel the tel to set
	 */
	public void setTel(java.lang.String tel) {
		this.tel = tel;
	}

	/**
	 * @return the address
	 */
	public java.lang.String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(java.lang.String address) {
		this.address = address;
	}
	
	

}
