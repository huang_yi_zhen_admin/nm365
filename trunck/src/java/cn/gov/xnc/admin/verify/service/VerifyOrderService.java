package cn.gov.xnc.admin.verify.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.verify.entity.VerifyOrderLogEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;

public interface VerifyOrderService extends CommonService{

	/**
	 * 根据条件查询列表
	 * @param params
	 * @return
	 */
	public List<VerifyOrderLogEntity> findListByParams(Map<String, Object> params);
	
	/**
	 * AJAX 根据条件查询列表并分页
	 * @param params
	 */
	public void findListPageByParams(Map<String, Object> params);
	
	/**
	 * 根据对象封装参数
	 * @param component
	 * @return
	 */
	public Map<String, Object> getParams(VerifyOrderLogEntity verifySaleOrderLogEntity);
	
	/**
	 * 根据物流单号、订单号,检查是否核销过
	 * @param log
	 * @param 物流单号
	 * @param 订单号
	 * @return
	 */
	public boolean checkVerifyOrderLog(VerifyOrderLogEntity log);
	
	/**
	 * 核单处理
	 * @param request
	 * @return
	 */
	public AjaxJson commitVerifyOrder(List<Map<String, Object>> params) throws Exception;
	
	/**
	 * 单个订单手动核单 - 检测
	 * @param orderEntity
	 * @param reques
	 * @return
	 */
	public AjaxJson manualSingleVerifyOrder(OrderEntity orderEntity) throws Exception;
	
	/**
	 * 根据订单的物流单号检查是否核单
	 * @param orderEntity
	 * @return
	 * @throws Exception
	 */
	public AjaxJson checkVerifyByOrderLogistics(OrderEntity orderEntity) throws Exception;
}
