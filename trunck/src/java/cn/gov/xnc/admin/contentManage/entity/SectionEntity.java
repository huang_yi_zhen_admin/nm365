package cn.gov.xnc.admin.contentManage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * @Title: Entity
 * @Description: 栏目表
 * @author jason
 *
 */
@Entity
@Table(name = "xnc_section", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class SectionEntity implements java.io.Serializable{
	private java.lang.String id;
	private java.lang.String section_name;//栏目名
	private java.lang.String pre_section_id;//父栏目ID
	private java.lang.String logo;//栏目logo
	private java.lang.Byte sort;//排序
	private java.util.Date create_time;//创建日期
	private java.lang.String create_user;//创建人员
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
	@Column(name ="section_name",nullable=true,length=20)
	public java.lang.String getSection_name() {
		return section_name;
	}
	public void setSection_name(java.lang.String section_name) {
		this.section_name = section_name;
	}
	@Column(name ="pre_section_id",nullable=true,length=32)
	public java.lang.String getPre_section_id() {
		return pre_section_id;
	}
	public void setPre_section_id(java.lang.String pre_section_id) {
		this.pre_section_id = pre_section_id;
	}
	@Column(name ="logo",nullable=true,length=130)
	public java.lang.String getLogo() {
		return logo;
	}
	public void setLogo(java.lang.String logo) {
		this.logo = logo;
	}
	@Column(name ="sort",nullable=true,length=4)
	public java.lang.Byte getSort() {
		return sort;
	}
	public void setSort(java.lang.Byte sort) {
		this.sort = sort;
	}
	@Column(name ="create_time",nullable=false)
	public java.util.Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(java.util.Date create_time) {
		this.create_time = create_time;
	}
	@Column(name ="create_user",nullable=true)
	public java.lang.String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(java.lang.String create_user) {
		this.create_user = create_user;
	}	
}
