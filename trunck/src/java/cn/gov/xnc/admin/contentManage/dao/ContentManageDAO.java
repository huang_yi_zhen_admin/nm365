package cn.gov.xnc.admin.contentManage.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


import cn.gov.xnc.admin.contentManage.entity.ArticleEntity;
import cn.gov.xnc.admin.contentManage.entity.SectionEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.SqlUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
@Service("contentManageDAO")
public class ContentManageDAO {
	@Resource(name="systemService")
	private SystemService systemService;
	//private static ContentManageDAO instance = null;
	private final String ARTICLETB = "xnc_article";
	private final String SECTIONTB = "xnc_section";
	private Map<String,Object> where = new HashMap<String,Object>();
	/**
	 * 返回实例
	 * */
	/*public static ContentManageDAO getInstance(){
		if(instance == null){
			synchronized(ContentManageDAO.class){
				if(instance == null){
					instance = new ContentManageDAO();
				}
			}
		}
		return instance;
	}*/
	private ContentManageDAO(){
		
	}
	/**
	 * 栏目查询，单条数据
	 * */
	public SectionEntity getSectionSingleData(SectionEntity section){
		SectionEntity sectionEntity = systemService.findUniqueByProperty(SectionEntity.class, "id", section.getId());
		return sectionEntity;
	}
	/**
	 * 栏目插入
	 * */
	public Serializable sectionSave(SectionEntity section){
		TSUser user = ResourceUtil.getSessionUserName();
		section.setCreate_time(DateUtils.gettimestamp());
		section.setCreate_user(user.getId());
		Serializable id = systemService.save(section);
		return id;
	}
	
	/**
	 * 栏目更新
	 * @return 
	 * */
	public Integer sectionUpdateById(SectionEntity section){
		where.clear();
		TSUser user = ResourceUtil.getSessionUserName();
		Integer result = 0 ;
		where.put("id", section.getId());
		section.setId(null);
		section.setCreate_user(user.getId());
		String sql = SqlUtil.makeUpdateSql(SECTIONTB, MyBeanUtils.beanToMapNew(section), where);
		System.out.println("#"+sql);
		result = systemService.updateBySqlString(sql);
		return result;
	}
	
	/**
	 * 栏目列表
	 * */
	public DataGrid getSectionListPage(DataGrid dataGrid, SectionEntity section){
		Map<String ,Object> where = MyBeanUtils.beanToMapNew(section);
		String field = dataGrid.getField() == null ? "*" : dataGrid.getField();
		StringBuilder sql = new StringBuilder();
		long count = 0;
		sql.append("select field");
		sql.append(" from " + SECTIONTB);
		sql.append(" where 1=1");
		for(String key : where.keySet()){
			if(where.get(key) != null){
				if(key.equals("section_name")){
					sql.append(" and " + key + " like '%" + where.get(key) + "%'");	
				}else{
					sql.append(" and " + key + "='" + where.get(key) + "'");	
				}
							
			}
		}
		count = systemService.getCountForJdbc(sql.toString().replace("field", "count(1)"));
		List<Map<String, Object>> mapList = systemService.findForJdbc(sql.toString().replace("field", field),dataGrid.getPageNum(),dataGrid.getShowNum());
		dataGrid.setTotal((int) count);
		dataGrid.setResults(mapList);
		return dataGrid;
	}
	/**
	 * 栏目列表所有
	 * */
	public List<SectionEntity> getSectionList(DataGrid dataGrid, SectionEntity section){
		Map<String ,Object> where = MyBeanUtils.beanToMapNew(section);
		String field = dataGrid.getField() == null ? "*" : dataGrid.getField();
		StringBuilder sql = new StringBuilder();
		sql.append("select field");
		sql.append(" from " + SECTIONTB);
		sql.append(" where 1=1");
		for(String key : where.keySet()){
			if(where.get(key) != null){
				sql.append(" and " + key + "='" + where.get(key) + "'");				
			}
		}
		List<SectionEntity> list = systemService.findListByJdbc(sql.toString().replace("field", field), SectionEntity.class, null);
		System.out.println("#########"+list.size());
		return list;
	}
	
	/**
	 * 内容发布
	 * */
	public Serializable articleSave(ArticleEntity article){
		TSUser user = ResourceUtil.getSessionUserName();
		article.setCreate_user(user.getId());
		article.setCreate_time(DateUtils.gettimestamp());
		article.setUpdate_time(DateUtils.gettimestamp());
		Serializable id = systemService.save(article);
		return id;
	}
	/**
	 * 内容更新
	 * */
	public Integer articleUpdate(ArticleEntity article){
		where.clear();
		Integer result = 0 ;
		where.put("id", article.getId());
		article.setId(null);
		article.setCreate_user(null);
		String sql = SqlUtil.makeUpdateSql(ARTICLETB, MyBeanUtils.beanToMapNew(article), where);
		System.out.println("#"+sql);
		result = systemService.updateBySqlString(sql);
		return result;
	}
	/**
	 * 内容列表
	 * */
	public DataGrid getArticleListPage(DataGrid dataGrid, ArticleEntity article){
		Map<String ,Object> where = MyBeanUtils.beanToMapNew(article);
		String field = dataGrid.getField() == null ? "a.*,b.section_name" : dataGrid.getField();
		StringBuilder sql = new StringBuilder();
		SectionEntity sectionEntity = new SectionEntity();
		long count = 0;
		sql.append("select field");
		sql.append(" from " + ARTICLETB + " as a INNER JOIN " + SECTIONTB + " as b on a.section_id=b.id ");
		sql.append(" where 1=1");
		for(String key : where.keySet()){
			if(where.get(key) != null){
				if(key.equals("title")){
					sql.append(" and " + key + " like '%" + where.get(key) + "%'");
				}else{
					sql.append(" and " + key + "=" + where.get(key));
				}
							
			}
		}
		count = systemService.getCountForJdbc(sql.toString().replace("field", "count(1)"));
		sql.append(" order by " + dataGrid.getSort() + " " + dataGrid.getOrder() + " ");
		List<Map<String, Object>> mapList = systemService.findForJdbc(sql.toString().replace("field", field),dataGrid.getPageNum(),dataGrid.getShowNum());
		dataGrid.setTotal((int) count);
		dataGrid.setResults(mapList);
		return dataGrid;
	}
	/**
	 * 单条内容数据
	 * */
	public ArticleEntity getArticleData(ArticleEntity article){
		ArticleEntity articleEntity = null;
		/*StringBuilder sql = new StringBuilder();
		Map<String ,Object> where = MyBeanUtils.beanToMapNew(article);
		sql.append("select id, title, content, source_name, summary, pic, create_time, link");
		sql.append(" from " + ARTICLETB);
		sql.append(" where 1=1");
		for(String key : where.keySet()){
			if(where.get(key) != null){
				sql.append(" and " + key + "='" + where.get(key)+"'");				
			}
		}*/
		articleEntity = systemService.findUniqueByProperty(ArticleEntity.class, "id", article.getId());
		return articleEntity;
	}
}
