package cn.gov.xnc.admin.express.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;

import cn.gov.xnc.admin.express.entity.ExpressEntity;
import cn.gov.xnc.admin.express.service.ExpressServiceI;
import cn.gov.xnc.admin.order.entity.OrderEntity;
import cn.gov.xnc.admin.order.entity.OrderLogisticsEntity;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.order.service.OrderLogisticsServiceI;
import cn.gov.xnc.admin.order.service.OrderServiceI;
import cn.gov.xnc.admin.order.service.PayBillsOrderServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.JsonUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("expressService")
@Transactional
public class ExpressServiceImpl extends CommonServiceImpl implements ExpressServiceI {
	
	private static final Logger logger = Logger.getLogger(ExpressServiceImpl.class);
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private OrderLogisticsServiceI orderLogisticsService;
	@Autowired
	private DictionaryService dictionaryService;
	@Autowired
	private PayBillsOrderServiceI payBillsOrderService;
	
	
	/**
	 * 设置/更新订单物流号
	 * @param shippercode
	 * @param logisticcode 快递单号
	 * @param ordercode 订单编号
	 * @param printtype 打印类型single/all
	 * @return
	 * @throws Exception
	 */
	public AjaxJson setOrderLogistcs(String shippercode,String logisticcode,String ordercode,String printtype) throws Exception{
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq_e = new CriteriaQuery(ExpressEntity.class);
		cq_e.eq("ordercode", ordercode);
		cq_e.eq("logisticcode", logisticcode);
		cq_e.eq("shippercode", shippercode);
		cq_e.eq("success", 1);
		cq_e.createAlias("orderid", "order");
		cq_e.add(Restrictions.eq("order.company", user.getCompany()));
		List<ExpressEntity> expresslist = systemService.getListByCriteriaQuery(cq_e,false);//只会有1条
		if( null == expresslist || expresslist.size() != 1){
			j.setSuccess(false);
			j.setMsg("请确认回传快递单号是否正确,ordercode="+ordercode+" logisticcode="+logisticcode + " shippercode=" + shippercode);
			return j;
		}
		ExpressEntity express = expresslist.get(0);
		
		OrderEntity oderEntity = systemService.findUniqueByProperty(OrderEntity.class,"id",express.getOrderid().getId());
		
		//根据快递公司，更新订单物流公司
		StringBuffer logisticCompanys = new StringBuffer();
		String expressLogistcCompany = ExpressCompany.getExpressCompanyName(express.getShippercode());
		String orderLogisticCompany = oderEntity.getLogisticscompany();
		
		if(StringUtil.isNotEmpty(orderLogisticCompany)){
			logisticCompanys.append(orderLogisticCompany).append(",").append(expressLogistcCompany);
		}else{
			logisticCompanys.append(expressLogistcCompany);
		}
		oderEntity.setLogisticscompany(logisticCompanys.toString());
		
		//根据快递单号，更新订单物流单号
		StringBuffer logisticNumbers = new StringBuffer();
		String logisticNumber = StringUtil.formatNull(oderEntity.getLogisticsnumber());
		if(StringUtil.isNotEmpty(logisticNumber)){
			logisticNumbers.append(logisticNumber).append(",").append(logisticcode);
		}else{
			logisticNumbers.append(logisticcode);
		}
		oderEntity.setLogisticsnumber(logisticNumbers.toString());
		
		//待发货
		if(OrderServiceI.FreightState.WAIT_DELIVER.getValue().equals(oderEntity.getFreightState().getDictionaryValue()) ){
			TSDictionary freightState = dictionaryService.checkDictItemWithoutCompany("freightStates", OrderServiceI.FreightState.WAIT_PACK.getValue());
			oderEntity.setFreightState(freightState);
		}
		//更新订单
		systemService.updateEntitie(oderEntity);
		
		//更新系统物流记录
		try {
			orderLogisticsService.saveOrderLogistics(oderEntity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String paybillsid = oderEntity.getBillsorderid().getId();
		if( StringUtil.isNotEmpty(paybillsid)){
			PayBillsOrderEntity payBills = systemService.getEntity(PayBillsOrderEntity.class, paybillsid);
			payBills.setFreightState(payBillsOrderService.getPayBillsFreightStatus(payBills));

			systemService.updateEntitie(payBills);
		}
		
		express.setSetorderlogistic(1);//标记为已经设置订单状态
		
		j.setSuccess(true);
		j.setMsg("ordercode="+ordercode+" logisticcode="+logisticcode + " shippercode=" + shippercode);
		
		return j;
	}
	
	public String getPayBillsFreightStatus(PayBillsOrderEntity paybillsOrder) {
		String freightStatus = "1"; //未发货_1,已发货_2,部分发货_3
		Integer totalOrder = 1;
		Integer hasFreightOrderNum = 0;
		if(null != paybillsOrder){
			PayBillsOrderEntity payBills = systemService.findUniqueByProperty(PayBillsOrderEntity.class, "id", paybillsOrder.getId());
			List<OrderEntity> orderList = payBills.getOrderS();
			totalOrder = orderList.size();
			String isSend = null;
			for (OrderEntity orderEntity : orderList) {
				isSend = orderEntity.getState();//  待付款_1,待发货_2,已发货_3,已签收_4,已取消_5,待审核_6,未通过_7
				if("3".equals(isSend) || "4".equals(isSend)){
					hasFreightOrderNum++;
				}
			}
			
		}
		if(totalOrder > 0){
			
			if( totalOrder == hasFreightOrderNum ){
				
				freightStatus = "2";
			}
			else if( hasFreightOrderNum > 0 && totalOrder > hasFreightOrderNum ){
				
				freightStatus = "3";
			}
		}
		
		return freightStatus;
	}

	@Override
	public void updateManualExpress(OrderEntity order) throws Exception {
		if(null != order && StringUtil.isNotEmpty(order.getId())){
			List<String> nLogistics = null;
			List<String> oLogistics = null;
			
			//获取旧物流面单号
			oLogistics = getArrayOrderLogistcs(order);
			
			if(StringUtil.isNotEmpty(order.getLogisticsnumber()) 
					&& StringUtil.isNotEmpty(order.getLogisticsnumber().trim())){
				
				//来自页面的新物流单号（手动添加）
				String[] nArrayLogisticNums = order.getLogisticsnumber().split(",");
				String[] nArrayLogisticCompanys = order.getLogisticscompany().split(",");
				
				//新物流单号
				nLogistics = Arrays.asList(nArrayLogisticNums);
				
				String logisticsCode = null, orderid = null;
				//保存新物流单号
				for (int i = 0; i < nArrayLogisticNums.length; i++) {
					logisticsCode = nArrayLogisticNums[i];
					orderid = order.getId();
					
					if(!hasDuplicateLogistcs(orderid, logisticsCode)){
						
						if(!oLogistics.contains(logisticsCode) ){
							saveManualOrderLogistcs(order.getId(), nArrayLogisticNums[i], nArrayLogisticCompanys[i]);
						}
						
					}else{
						
						throw new Exception("物理单号：" + logisticsCode + "更新失败，物流单号不能在其他订单中同时出现！");
					}
					
					
				}
				
				//去除无效物流单号
				if(null != oLogistics && !oLogistics.isEmpty()){
					
					for (String ologistcsCode : oLogistics) {
						
						if(!nLogistics.contains( ologistcsCode.trim() )){//页面没有的物理单号要变为失效
							
							inValidExpressLogistcs(order.getId(), ologistcsCode);
							
						}else{//页面有的物流单号全部更新为有效
							
							if(!hasDuplicateLogistcs(orderid, logisticsCode)){
								
							}else{
								
								throw new Exception("物理单号：" + logisticsCode + "更新失败，物流单号不能在其他订单中同时出现！");
							}
							
						}
					}
				}
			}
			else{
				if(null != oLogistics && oLogistics.size() > 0 ){
					throw new Exception("更新失败:不能全部清空物流单号，请修改或者部分删除！");
				}
			}
		}
	}

	/**
	 * 查找旧物流面单号
	 */
	@Override
	public List<String> getArrayOrderLogistcs(OrderEntity order) throws Exception {
		List<String> oLogistics = new ArrayList<String>();
		/*StringBuffer sqlbf = new StringBuffer();
		sqlbf.append("SELECT TRIM(logisticCode) logisticCode FROM xnc_express ex WHERE ex.orderid = ? AND  ex.success = '1' AND ex.setorderlogistic = 1");*/
		
		CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
		cq.eq("orderid.id", order.getId());
		cq.eq("setorderlogistic", 1);
		cq.eq("success", 1);
		
		//查找是否存在无效的手工面单号
		List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
		for (ExpressEntity expressEntity : expresslist) {
			if(StringUtil.isNotEmpty(expressEntity.getLogisticcode())){
				oLogistics.add(expressEntity.getLogisticcode().trim());
			}
			
		}
		
//		oLogistics = findListByJdbc(sqlbf.toString(), String.class, new Object[]{order.getId()});
		return oLogistics;
	}

	@Override
	public void saveManualOrderLogistcs(String orderid, String logisticsCode, String logisticsCompany) throws Exception {
		
		String shipperCode = ExpressCompany.getCompanyCode(logisticsCompany);
		if(StringUtil.isNotEmpty(shipperCode)){
			CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
			cq.eq("orderid.id", orderid);
			cq.eq("logisticcode", logisticsCode);
			cq.eq("shippercode", shipperCode);
			cq.eq("setorderlogistic", 0);// 0 无效 1 有效
			cq.eq("expresstype", 2);
			
			//差找是否存在无效的手工面单号
			List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
			if(null == expresslist || (null != expresslist && expresslist.size() == 0)){//不存在添加
				
				ExpressEntity express = new ExpressEntity();
				express.setId(IdWorker.generateSequenceNo());
				express.setOrderid(findUniqueByProperty(OrderEntity.class, "id", orderid));
				express.setShippercode(shipperCode);
				express.setLogisticcode(logisticsCode);
				express.setSetorderlogistic(1);// 0 无效 1 有效
				express.setSuccess(1);
				express.setExpresstype(2);//手动物流面单
				
				save(express);
				
			}else{//存在更新成有效
				
				ExpressEntity express = expresslist.get(0);
				express.setSuccess(1);
				express.setSetorderlogistic(1);
				
				updateEntitie(express);
			}
		}
			
	}

	@Override
	public void inValidExpressLogistcs(String orderid, String logisticsCode) throws Exception {
		CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
		cq.eq("orderid.id", orderid);
		cq.eq("logisticcode", logisticsCode);
		cq.eq("success", 1);
		cq.eq("setorderlogistic", 1);
		
		List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
		if(null != expresslist && expresslist.size() > 0){
			ExpressEntity express = expresslist.get(0);
			int expresstype = express.getExpresstype();
			
			StringBuffer sqlbf = new StringBuffer();
			if(1 == expresstype){//电子面单,只更新setorderlogistic
				
				sqlbf.append("Update  xnc_express ex SET ex.setorderlogistic = 0 WHERE ex.orderid = '" + orderid + "' AND ex.logisticCode ='" + logisticsCode + "' AND ex.setorderlogistic = 1 and ex.expresstype=1 ");
				
			}else{//手工面单，更新setorderlogistic和success
				
				sqlbf.append("Update  xnc_express ex SET ex.setorderlogistic = 0, ex.success=0 WHERE ex.orderid = '" + orderid + "' AND ex.logisticCode ='" + logisticsCode + "' AND ex.success=1 AND ex.setorderlogistic = 1 and ex.expresstype=2 ");
			}
			
			updateBySqlString(sqlbf.toString());
		}
	}
	
	
	@Override
	public void validExpressLogistcs(String orderid, String logisticsCode) throws Exception {
		/*CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
		cq.eq("orderid.id", orderid);
		cq.eq("logisticcode", logisticsCode);
		cq.eq("success", 1);
		cq.eq("setorderlogistic", 1);
		
		List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
		if(null != expresslist && expresslist.size() > 0){
			ExpressEntity express = expresslist.get(0);
			int expresstype = express.getExpresstype();
			
			StringBuffer sqlbf = new StringBuffer();
			if(1 == expresstype){//电子面单,只更新setorderlogistic
				sqlbf.append("Update  xnc_express ex SET ex.setorderlogistic = 1 WHERE ex.orderid = '" + orderid + "' AND ex.logisticCode ='" + logisticsCode + "' AND ex.setorderlogistic = 0");
			}else{
				
			}
		
		
		updateBySqlString(sqlbf.toString());
		}*/
		
	}

	@Override
	public boolean hasDuplicateLogistcs(String orderid, String logisticsCode) throws Exception {
		if(StringUtil.isNotEmpty(orderid) && StringUtil.isNotEmpty(logisticsCode)){
			CriteriaQuery cq = new CriteriaQuery(ExpressEntity.class);
			cq.notEq("orderid.id", orderid);
			cq.eq("logisticcode", logisticsCode);
			cq.eq("success", 1);
			cq.eq("setorderlogistic", 1);
			
			List<ExpressEntity> expresslist = getListByCriteriaQuery(cq, false);
			
			if(null != expresslist && expresslist.size() > 0){
				return true;
			}
		}
		return false;
	}

	/**
	 * 接收快递鸟推送物流信息
	 * @param requestData
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public AjaxJson acceptExpress(String requestData) throws Exception{
		AjaxJson result = new AjaxJson();
		
		Map<String, Object>  datas = (Map<String, Object>) JSON.parse(requestData);
		logger.error("###acceptExpress####datas####"+datas);
		//物流信息
		List<Map<String, Object>> data  =  (List<Map<String, Object>>) datas.get("Data");
		for (Map<String, Object> express : data) {
			//物流单号
			String logisticCode = (String) express.get("LogisticCode");
			logger.error("###acceptExpress#####logisticCode###"+logisticCode);
			//物流状态
			String state = (String) express.get("State");
			logger.error("####acceptExpress#####state###"+state);
			//转换物流状态
			state = convertFreightState(state);
			//根据物流单号查找订单物流信息
			List<OrderLogisticsEntity> orderLogistic = orderLogisticsService.getOrderLogisticByLogisticNumber(logisticCode);
			logger.error("###acceptExpress#####orderLogistic###"+orderLogistic);
			if(orderLogistic == null){
				result.setSuccess(false);	
				result.setMsg("没有找到订单物流信息");
				return result;
			}
			
			//更新订单物流信息
			for (OrderLogisticsEntity orderLogisticsEntity : orderLogistic) {
				try {
					String saveResult = JsonUtil.toJson(express);
					orderLogisticsEntity.setMessage(saveResult);
					orderLogisticsEntity.setUpdatetime(new Date());
					orderLogisticsEntity.setChannel("KDN");
					orderLogisticsEntity.setState(state);
					orderLogisticsService.updateEntitie(orderLogisticsEntity);
					logger.error("###acceptExpress###updateFreightInfo###Sucesss");
					
					changeOrderState(orderLogisticsEntity.getOrderid());
				} catch (Exception e) {
					logger.error(e);
				}
				
			}
		}
		result.setSuccess(true);
		return result;
	}
	
	/**
	 * 快递鸟物流状态转换本系统对应的物流状态
	 * @param state
	 * @return
	 */
	private String convertFreightState(String state){
		String convertState = null;
		//物流状态:0-无轨迹，1-已揽收，2-在途中 201-到达派件城市，3-签收,4-问题件
		/**订单状态 在途中_0,已发货_1,疑难件_2,已签收_3,已退货_4,异常_5,无轨迹_6,到达派件城市_7*/
		if(state.equals("0")){
			//无轨迹
			convertState = OrderLogisticsServiceI.State.NOWHERE.getValue();
		}
		else if(state.equals("1")){
			//已发货
			convertState = OrderLogisticsServiceI.State.ALREADY_DELIVER.getValue();
		}
		else if(state.equals("2")){
			//在途中
			convertState = OrderLogisticsServiceI.State.WHERE.getValue();
		}
		else if(state.equals("201")){
			//到达派件城市
			convertState = OrderLogisticsServiceI.State.TERMINUS.getValue();
		}
		else if(state.equals("3")){
			//已签收
			convertState = OrderLogisticsServiceI.State.ALREADY_SIGN.getValue();
		}
		else if(state.equals("4")){
			//疑难件
			convertState = OrderLogisticsServiceI.State.PROBLEM.getValue();
		}
		else{
			//疑难件
			convertState = OrderLogisticsServiceI.State.PROBLEM.getValue();
		}
		
		return convertState;
	}
	
	/**
	 * 实时改变订单在打包之后的物流状态
	 * @param order
	 * @param logisticState
	 */
	private void changeOrderState(OrderEntity order){
		TSDictionary freightState = order.getFreightState();
		if(!OrderServiceI.FreightState.ALREADY_SIGN.getValue().equals(freightState.getDictionaryValue())){
			freightState = orderLogisticsService.changeLogisticState2OrderFreightState(order.getId());
			
			if(null != freightState){
				order.setFreightState(freightState);
				updateEntitie(order);
			}
		}
	}
}