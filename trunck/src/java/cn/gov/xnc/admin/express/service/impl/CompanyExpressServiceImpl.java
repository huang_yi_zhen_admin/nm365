package cn.gov.xnc.admin.express.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.express.service.CompanyExpressServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("companyExpressService")
@Transactional
public class CompanyExpressServiceImpl extends CommonServiceImpl implements CompanyExpressServiceI {
	
}