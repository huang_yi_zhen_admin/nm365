package cn.gov.xnc.admin.commercial.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.commercial.service.BusinessShareFeedbackLogServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("businessShareFeedbackLogService")
@Transactional
public class BusinessShareFeedbackLogServiceImpl extends CommonServiceImpl implements BusinessShareFeedbackLogServiceI {
	
}