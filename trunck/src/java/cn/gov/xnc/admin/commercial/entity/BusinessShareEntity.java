package cn.gov.xnc.admin.commercial.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 公司招商分享信息表
 * @author zero
 * @date 2018-03-29 09:57:42
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_business_share", schema = "")
@SuppressWarnings("serial")
public class BusinessShareEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**分享招商的标题*/
	private java.lang.String title;
	/**分享招商内容*/
	private java.lang.String content;
	/**二维码图片*/
	private java.lang.String qrcode;
	/**说明*/
	private java.lang.String remark;
	/**招商需求*/
	private java.lang.String sharetype;
	/**状态 'U' 使用 'D' 删除*/
	private java.lang.String status;
	/**关联的公司ID*/
	private TSCompany companyid;
	/**创建时间*/
	private java.util.Date createtime;
	/**更新时间*/
	private java.util.Date updatetime;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  分享招商的标题
	 */
	@Column(name ="TITLE",nullable=false,length=255)
	public java.lang.String getTitle(){
		return this.title;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  分享招商的标题
	 */
	public void setTitle(java.lang.String title){
		this.title = title;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  分享招商内容
	 */
	@Column(name ="CONTENT",nullable=false,length=4000)
	public java.lang.String getContent(){
		return this.content;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  分享招商内容
	 */
	public void setContent(java.lang.String content){
		this.content = content;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  二维码图片
	 */
	@Column(name ="QRCODE",nullable=false,length=1000)
	public java.lang.String getQrcode(){
		return this.qrcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  二维码图片
	 */
	public void setQrcode(java.lang.String qrcode){
		this.qrcode = qrcode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  说明
	 */
	@Column(name ="REMARK",nullable=false,length=255)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  说明
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  状态 'U' 使用 'D' 删除
	 */
	@Column(name ="STATUS",nullable=false,length=2)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  状态 'U' 使用 'D' 删除
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: TSCompany  关联的公司ID
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: TSCompany  关联的公司ID
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATETIME",nullable=false)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}

	/**招商需求*/
	@Column(name ="SHARETYPE",nullable=false,length=1000)
	public java.lang.String getSharetype() {
		return sharetype;
	}

	public void setSharetype(java.lang.String sharetype) {
		this.sharetype = sharetype;
	}
}
