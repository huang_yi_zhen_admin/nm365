package cn.gov.xnc.admin.commercial.controller;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.commercial.entity.BusinessShareEntity;
import cn.gov.xnc.admin.commercial.service.BusinessShareServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.DataConstant;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.ListUtil;
import cn.gov.xnc.system.core.util.LogUtil;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.CompanyService;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 公司招商分享信息表
 * @author zero
 * @date 2018-03-29 09:57:41
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/businessShareController")
public class BusinessShareController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BusinessShareController.class);

	@Autowired
	private BusinessShareServiceI businessShareService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private CompanyService companyService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 公司招商分享信息表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView businessShare(HttpServletRequest request) {
		request.setAttribute("sharetypeList", businessShareService.getListBusinessShareType());
		return new ModelAndView("admin/commercial/businessShareList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(BusinessShareEntity businessShare,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		StringBuffer buf = new StringBuffer();
		CriteriaQuery cq = new CriteriaQuery(BusinessShareEntity.class, dataGrid);
		if(StringUtil.isNotEmpty(businessShare.getTitle())){
			cq.like("title", buf.append("%").append(businessShare.getTitle()).append("%").toString());
			businessShare.setTitle(null);
		}
		if(StringUtil.isNotEmpty(businessShare.getSharetype())){
			cq.like("sharetype", buf.append("%").append(businessShare.getSharetype()).append("%").toString());
			businessShare.setSharetype(null);
		}
		cq.eq("status", DataConstant.STATUS_USING);
		cq.add();
		
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, businessShare, request.getParameterMap());
		this.businessShareService.getDataGridReturn(cq, true);
		
		List<BusinessShareEntity> businessList = dataGrid.getResults();
		List<BusinessShareEntity> copyBusinessList = new ArrayList<BusinessShareEntity>(Arrays.asList(new BusinessShareEntity[businessList.size()]));
//		Collections.copy(copyBusinessList, businessList);
		try {
			copyBusinessList = ListUtil.deepCopy(businessList);
		} catch (Exception e) {
			LogUtil.error("拷贝对象失败：", e);;
		}
		for (BusinessShareEntity business : copyBusinessList) {
			business.setSharetype(businessShareService.getBusinessShareTypeName(business.getSharetype()));
		}
		dataGrid.setResults(copyBusinessList);

		
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除公司招商分享信息表
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(BusinessShareEntity businessShare, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		businessShare = systemService.getEntity(BusinessShareEntity.class, businessShare.getId());
		message = "招商分享信息删除成功";
		if(null != businessShare){
			businessShare.setStatus(DataConstant.STATUS_DEL);
		}
		businessShareService.updateEntitie(businessShare);
		
		systemService.addLog(message, Globals.Log_Leavel_INFO, Globals.Log_Type_DEL);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加公司招商分享信息表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(BusinessShareEntity businessShare, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(businessShare.getId())) {
			message = "招商分享信息更新成功";
			BusinessShareEntity t = businessShareService.get(BusinessShareEntity.class, businessShare.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(businessShare, t);
				businessShareService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Leavel_INFO, Globals.Log_Type_UPDATE);
			} catch (Exception e) {
				e.printStackTrace();
				message = "公司招商分享信息更新失败";
			}
		} else {
			message = "招商分享信息添加成功";
			TSUser user = ResourceUtil.getSessionUserName();
			
			businessShare.setId(businessShareService.getTabelSequence(IdWorker.TABLE_COMPANY_STOCK));
			businessShare.setStatus(DataConstant.STATUS_USING);
			businessShare.setCreatetime(DateUtils.getDate());
			businessShare.setCompanyid(user.getCompany());
			
			businessShareService.save(businessShare);
			systemService.addLog(message, Globals.Log_Leavel_INFO, Globals.Log_Type_UPDATE);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 公司招商分享信息表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(BusinessShareEntity businessShare, HttpServletRequest req) {
		
		if (StringUtil.isNotEmpty(businessShare.getId())) {
			businessShare = businessShareService.getEntity(BusinessShareEntity.class, businessShare.getId());
			
		} else {
			businessShare = new BusinessShareEntity();
			
		}
		
		req.setAttribute("sharetypeList", businessShareService.getListBusinessShareType());
		
		req.setAttribute("businessSharePage", businessShare);
		
		return new ModelAndView("admin/commercial/businessShare");
	}
	
	/**
	 * 公司招商分享信息表列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "qrcode")
	public ModelAndView qrcode(BusinessShareEntity businessShare, HttpServletRequest req) {
		
		businessShare = businessShareService.getEntity(BusinessShareEntity.class, businessShare.getId());
		String domain = companyService.getCompanyDomain(businessShare.getCompanyid().getId());
		int posit = domain.lastIndexOf("/");
		if(posit == domain.length()){
			domain = domain.substring(0, posit);
		}
		StringBuffer buf= new StringBuffer();
		buf.append(domain);
		buf.append("/businessShareController/h5share?");
		buf.append("id=").append(businessShare.getId()).append("&co=").append(businessShare.getCompanyid().getId());
		req.setAttribute("codeurl", buf.toString());
		
		return new ModelAndView("admin/commercial/businessShareQrcode");
	}
}
