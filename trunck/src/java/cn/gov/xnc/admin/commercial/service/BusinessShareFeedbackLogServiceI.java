package cn.gov.xnc.admin.commercial.service;

import cn.gov.xnc.system.core.common.service.CommonService;

public interface BusinessShareFeedbackLogServiceI extends CommonService{

	/*消息类型： 'A' accept 接收 ‘R’reply 回复*/
	public static final String TYPE_MSG_REPLY = "R";
	public static final String TYPE_MSG_ACCEPT = "A";
	
	/*消息回复：Y 已回复 N 未回复 */
	public static final String REPLY_YES = "Y";
	public static final String REPLY_NO = "N";
	
}
