package cn.gov.xnc.admin.commercial.service;

import java.util.List;

import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;

public interface BusinessShareServiceI extends CommonService{
	
	public static final String BUSINESS_SHARE_DICT_TYPE = "DIC_TYPE_BUSINESS_SHARE";

	/**
	 * 获取所有招商分享类型
	 * @return List<TSDictionary>
	 */
	public List<TSDictionary> getListBusinessShareType();
	
	public String getBusinessShareTypeName(String businessTypeId);

}
