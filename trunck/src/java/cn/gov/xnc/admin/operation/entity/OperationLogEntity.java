package cn.gov.xnc.admin.operation.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 用户操作记录
 * @author zero
 * @date 2017-05-05 14:45:42
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_operator_log", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class OperationLogEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**operateCode*/
	private java.lang.Integer operateCode;
	/**operateName*/
	private java.lang.String operateName;
	/**操作来源url*/
	private java.lang.String requestUrl;
	/**操作来源参数*/
	private java.lang.String requestParam;
	/**keyEntityId*/
	private java.lang.String keyEntityId;
	/**keyEntityJson*/
	private java.lang.String keyEntityJson;
	/**entitiesJson*/
	private java.lang.String entitiesJson;
	
	/**日志信息*/
	private java.lang.String loginfo;
	
	/**操作者用户id*/
	private TSUser operatorid;
	
	/**操作时间*/
	private java.util.Date updatetime;
	
	/**公司id*/
	private TSCompany company;
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  operateCode
	 */
	@Column(name ="OPERATE_CODE",nullable=true,precision=10,scale=0)
	public java.lang.Integer getOperateCode(){
		return this.operateCode;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  operateCode
	 */
	public void setOperateCode(java.lang.Integer operateCode){
		this.operateCode = operateCode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  operateName
	 */
	@Column(name ="OPERATE_NAME",nullable=true,length=255)
	public java.lang.String getOperateName(){
		return this.operateName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  operateName
	 */
	public void setOperateName(java.lang.String operateName){
		this.operateName = operateName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  操作来源url
	 */
	@Column(name ="REQUEST_URL",nullable=true,length=512)
	public java.lang.String getRequestUrl(){
		return this.requestUrl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  操作来源url
	 */
	public void setRequestUrl(java.lang.String requestUrl){
		this.requestUrl = requestUrl;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  操作来源参数
	 */
	@Column(name ="REQUEST_PARAM",nullable=true,length=255)
	public java.lang.String getRequestParam(){
		return this.requestParam;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  操作来源参数
	 */
	public void setRequestParam(java.lang.String requestParam){
		this.requestParam = requestParam;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  keyEntityId
	 */
	@Column(name ="KEY_ENTITY_ID",nullable=true,length=32)
	public java.lang.String getKeyEntityId(){
		return this.keyEntityId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  keyEntityId
	 */
	public void setKeyEntityId(java.lang.String keyEntityId){
		this.keyEntityId = keyEntityId;
	}
	/**
	 *方法: 取得java.lang.Object
	 *@return: java.lang.Object  keyEntityJson
	 */
	@Column(name ="KEY_ENTITY_JSON",nullable=true)
	public java.lang.String getKeyEntityJson(){
		return this.keyEntityJson;
	}

	/**
	 *方法: 设置java.lang.Object
	 *@param: java.lang.Object  keyEntityJson
	 */
	public void setKeyEntityJson(java.lang.String keyEntityJson){
		this.keyEntityJson = keyEntityJson;
	}
	/**
	 *方法: 取得java.lang.Object
	 *@return: java.lang.Object  entitiesJson
	 */
	@Column(name ="ENTITIES_JSON",nullable=true)
	public java.lang.String getEntitiesJson(){
		return this.entitiesJson;
	}

	/**
	 *方法: 设置java.lang.Object
	 *@param: java.lang.Object  entitiesJson
	 */
	public void setEntitiesJson(java.lang.String entitiesJson){
		this.entitiesJson = entitiesJson;
	}

	/**
	 * @return the loginfo
	 */
	@Column(name ="LOGINFO",nullable=true)
	public java.lang.String getLoginfo() {
		return loginfo;
	}

	/**
	 * @param loginfo the loginfo to set
	 */
	public void setLoginfo(java.lang.String loginfo) {
		this.loginfo = loginfo;
	}

	/**
	 * @return the operatorid
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OPERATORID")
	public TSUser getOperatorid() {
		return operatorid;
	}

	/**
	 * @param operatorid the operatorid to set
	 */
	public void setOperatorid(TSUser operatorid) {
		this.operatorid = operatorid;
	}

	/**
	 * @return the updatetime
	 */
	@Column(name ="UPDATE_TIME",nullable=false)
	public java.util.Date getUpdatetime() {
		return updatetime;
	}

	/**
	 * @param updatetime the updatetime to set
	 */
	public void setUpdatetime(java.util.Date updatetime) {
		this.updatetime = updatetime;
	}

	/**
	 * @return the company
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
}
