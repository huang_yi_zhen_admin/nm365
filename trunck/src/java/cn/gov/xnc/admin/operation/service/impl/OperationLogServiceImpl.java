package cn.gov.xnc.admin.operation.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.operation.service.OperationLogServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("operationLogService")
@Transactional
public class OperationLogServiceImpl extends CommonServiceImpl implements OperationLogServiceI {
	
}