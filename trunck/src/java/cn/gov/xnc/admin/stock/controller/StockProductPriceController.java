package cn.gov.xnc.admin.stock.controller;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;

/**   
 * @Title: Controller
 * @Description: 库存商品均价信息
 * @author zero
 * @date 2017-07-29 16:42:10
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockProductPriceController")
public class StockProductPriceController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockProductPriceController.class);

	@Autowired
	private StockProductPriceServiceI stockProductPriceService;
	@Autowired
	private StockProductServiceI stockProductServiceI;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 库存商品均价信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView stockProductPrice(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockProductPriceList");
	}
	
	/**
	 * 库存商品均价信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "setPrice")
	public ModelAndView setPrice(String id, HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		if( StringUtil.isNotEmpty(id) ){
			String[] tmp = id.split("-");
			String productid = tmp[0];
			String stockid = tmp[1];
			String stockSegmentId = tmp[2];
			
			StockSegmentEntity segmentEntity = systemService.getEntity(StockSegmentEntity.class, stockSegmentId);
			ProductEntity product = systemService.getEntity(ProductEntity.class, productid);
			StockEntity stock = systemService.getEntity(StockEntity.class, stockid);
			
			Map<String, Object> stockProductParams = stockProductServiceI.getParams(null,product, stock, segmentEntity, null,null,null);
			List<StockProductEntity> stockProducts = stockProductServiceI.findListByParams(stockProductParams);
			
			if(stockProducts.size() > 0){
				StockProductEntity stockProduct = stockProducts.get(0);
				request.setAttribute("stockProduct", stockProduct);
			}
			
			Map<String, Object> stockProductPriceParams = stockProductPriceService.getParams(product, stock);
			List<StockProductPriceEntity>  stockProductPrices = stockProductPriceService.findListByParams(stockProductPriceParams);
			if(stockProductPrices.size() > 0){
				StockProductPriceEntity stockProductPrice = stockProductPrices.get(0);
				request.setAttribute("stockProductPrice", stockProductPrice);
			}
			
//			stockProductPriceService.findListByParams(params)
//			CriteriaQuery cq = new CriteriaQuery(StockProductPriceEntity.class);
//			cq.eq("productid", product);
//			cq.eq("stockid", stock);
//			cq.add();
//			
//			List<StockProductPriceEntity> stockProductPriceList = stockProductPriceService.getListByCriteriaQuery(cq, false);
//			if( null != stockProductPriceList && stockProductPriceList.size()>0 ){				
//				request.setAttribute("stockProductPrice", stockProductPriceList.get(0));
//				if( null != stockProductPriceList.get(0).getProductid().getUnitassist() && StringUtil.isNotEmpty(stockProductPriceList.get(0).getProductid().getUnitassist().getId() )){
//					request.setAttribute("unitassist","true" );
//				}else{
//					request.setAttribute("unitassist","false" );
//				}
//				request.setAttribute("id",stockProductPriceList.get(0).getId() ); 
//			}else{
//				request.setAttribute("stockProductPrice", new StockProductPriceEntity());
//			}
		}
		return new ModelAndView("admin/stock/stockSetPrice");
	}
	
	@RequestMapping(value = "savePrice")
	@ResponseBody
	public AjaxJson savePrice(String stockProductId,String stockProductPriceId, BigDecimal price,  HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
//		if( StringUtil.isEmpty(id) ){
//			result.setSuccess(false);
//			result.setMsg("输入的id值有误，请稍后重试");
//			return result;
//		}
		
		if( null == price || price.compareTo(new BigDecimal(0.00))<=0 ){
			result.setSuccess(false);
			result.setMsg("输入的价格有误，请重新输入");
			return result;
		}
		
		result = stockProductPriceService.savePrice(stockProductId,stockProductPriceId, price, request);

		return result;
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(StockProductPriceEntity stockProductPrice,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(StockProductPriceEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockProductPrice, request.getParameterMap());
		this.stockProductPriceService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除库存商品均价信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(StockProductPriceEntity stockProductPrice, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockProductPrice = systemService.getEntity(StockProductPriceEntity.class, stockProductPrice.getId());
		message = "库存商品均价信息删除成功";
		stockProductPriceService.delete(stockProductPrice);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加库存商品均价信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(StockProductPriceEntity stockProductPrice, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(stockProductPrice.getId())) {
			message = "库存商品均价信息更新成功";
			StockProductPriceEntity t = stockProductPriceService.get(StockProductPriceEntity.class, stockProductPrice.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockProductPrice, t);
				stockProductPriceService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "库存商品均价信息更新失败";
			}
		} else {
			message = "库存商品均价信息添加成功";
			stockProductPriceService.save(stockProductPrice);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 库存商品均价信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(StockProductPriceEntity stockProductPrice, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockProductPrice.getId())) {
			stockProductPrice = stockProductPriceService.getEntity(StockProductPriceEntity.class, stockProductPrice.getId());
			req.setAttribute("stockProductPricePage", stockProductPrice);
		}
		return new ModelAndView("admin/stock/stockProductPrice");
	}
}
