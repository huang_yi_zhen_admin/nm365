package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockCheckDetailServiceI;
import cn.gov.xnc.admin.stock.service.StockCheckServiceI;
import cn.gov.xnc.admin.stock.service.StockIOType;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.admin.stock.service.StockServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

@Service("stockCheckService")
@Transactional
public class StockCheckServiceImpl extends CommonServiceImpl implements StockCheckServiceI {
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private StockCheckDetailServiceI stockCheckDetailService;
	@Autowired
	private StockProductServiceI stockProductService;
	@Autowired
	private StockProductPriceServiceI stockProductPriceServiceI;
	
	private String message;
	
	public AjaxJson delCheckOrders(HttpServletRequest request, String checkids){
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson result = new AjaxJson();
		String[] idlist = checkids.split(",");
		CriteriaQuery cq = new CriteriaQuery(StockCheckEntity.class);
		cq.in("id", idlist);
		cq.add();
		List<StockCheckEntity> checklist = getListByCriteriaQuery(cq, false);
		if( null == checklist || checklist.size() == 0){
			result.setSuccess(false);
			result.setMsg("找不到盘点单");
			return result;
		}
		
		StringBuffer namebuf = new StringBuffer();
		for(StockCheckEntity stockcheck : checklist){
			stockcheck.setStatus("4");//设置为已删除
			namebuf.append("[").append(stockcheck.getName()).append("]");
			//记录删除日志
			opLog(request, OperationLogUtil.STOCK_DEL, stockcheck, "删除" );
		}
		batchUpdate(checklist);
		
		

		result.setSuccess(true);
		result.setMsg("删除盘点单成");
		return result;
	}

	/**
	 * 添加盘点单：保存，提交
	 * @param stockCheckOrder
	 * @param request
	 * @return
	 */
	public AjaxJson saveCommit(StockCheckEntity stockCheckOrder,HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		
		TSUser user = ResourceUtil.getSessionUserName();
		
		if(!StringUtil.isNotEmpty(stockCheckOrder.getName())){
			result.setSuccess(false);
			result.setMsg("请填写盘点名称！");
			return result;
		}
		
		StockEntity checkStock = stockCheckOrder.getStockid();
		if(checkStock == null){
			result.setSuccess(false);
			result.setMsg("请添加仓库！");
			return result;
		}else if(!StringUtil.isNotEmpty(checkStock.getId())){
			result.setSuccess(false);
			result.setMsg("请添加仓库！");
			return result;
		}
		
		try {
			if(StringUtil.isEmpty(stockCheckOrder.getName())){
				throw new Exception("盘点名称不能为空！");
			}
			TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			TSUser operator = systemService.findUniqueByProperty(TSUser.class, "id", stockCheckOrder.getOperator().getId());
			StockEntity stock = systemService.findUniqueByProperty(StockEntity.class, "id", stockCheckOrder.getStockid().getId());
			//创建基础数据
			StockCheckEntity stockCheckEntity = new StockCheckEntity();
			stockCheckEntity.setName(stockCheckOrder.getName());
			stockCheckEntity.setIdentifier(IdWorker.generateSequenceNo());
			stockCheckEntity.setCompanyid(company);
			stockCheckEntity.setCreatetime(DateUtils.getDate());
			stockCheckEntity.setOperator(operator);
			stockCheckEntity.setstockCheckDetailList(null);
			stockCheckEntity.setStockid(stock);
			stockCheckEntity.setRemark(stockCheckOrder.getRemark());
			
			save(stockCheckEntity);
			
			//创建盘点详情数据
			List<StockCheckDetailEntity> stockCheckDetaillList = stockCheckDetailService.buildStockCheckDetail(stockCheckEntity, request);
			
			if(stockCheckDetaillList.size() <= 0){
				result.setSuccess(false);
				result.setMsg("请添加商品信息！");
				return result;
			}
			
			if(stockCheckDetaillList.size() > 0){
				stockCheckEntity.setstockCheckDetailList(stockCheckDetaillList);
				//状态：盘点完成
				stockCheckEntity.setStatus("2");
				updateEntitie(stockCheckEntity);
				
				//更新库存状况
				for (StockCheckDetailEntity stockCheckDetailEntity : stockCheckDetaillList) {
						Map<String, Object> params = new HashMap<>();
						StockProductEntity stockProductEntity = stockCheckDetailEntity.getStockproductid();
						StockEntity stockEntity  = stockProductEntity.getStockid();
						StockSegmentEntity segmentEntity = stockProductEntity.getStockSegment();
						ProductEntity productEntity = stockProductEntity.getProductid();
						params.put("stockEntity", stockEntity);
						params.put("segmentEntity", segmentEntity);
						params.put("companyEntity", user.getCompany());
						params.put("productEntity",productEntity);
						
						BigDecimal stockNum =  stockProductEntity.getStocknum();
						BigDecimal avgPrice =  stockProductEntity.getAverageprice();
						
						//盘点数量
						BigDecimal checkStockNum = stockCheckDetailEntity.getCheckstocknum();
						//调整数量
						BigDecimal adJustStockNum = new BigDecimal(0.00);
						//调整金额/盈亏
						BigDecimal adJustStockMoney = new BigDecimal(0.00);
						//盘点调整的总价
						BigDecimal checkTotalPrice = new BigDecimal(0.00);
						String ioType = null;
						
						if(checkStockNum.compareTo(stockNum) == 0){
							ioType = StockIOType.OUT;
							adJustStockNum = new BigDecimal(0.00);
							adJustStockMoney = new BigDecimal(0.00);
							stockCheckDetailEntity.setAdjuststocknum(adJustStockNum);
							stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
						}else if(checkStockNum.compareTo(stockNum) > 0){
							ioType = StockIOType.IN;
							adJustStockNum = checkStockNum.subtract(stockNum);
							adJustStockMoney = adJustStockNum.multiply(avgPrice);
							stockCheckDetailEntity.setAdjuststocknum(adJustStockNum);
							stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
						}else if(checkStockNum.compareTo(stockNum) < 0){
							ioType = StockIOType.OUT;
							if(checkStockNum.compareTo(new BigDecimal(0.00)) == 0){
								adJustStockNum = stockNum;
								adJustStockMoney = (adJustStockNum.negate()).multiply(avgPrice);
								stockCheckDetailEntity.setAdjuststocknum(adJustStockNum.negate());
								stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
							}else{
								checkStockNum = checkStockNum.abs();
								adJustStockNum = stockNum.subtract(checkStockNum);
								adJustStockMoney = (adJustStockNum.negate()).multiply(avgPrice);
								stockCheckDetailEntity.setAdjuststocknum(adJustStockNum.negate());
								stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
							}
						}
						checkTotalPrice = adJustStockNum.multiply(avgPrice);
						params.put("ioType", ioType);
						params.put("op_stockNum",adJustStockNum);
						params.put("op_totalPrice",checkTotalPrice);
						
						updateEntitie(stockCheckDetailEntity);
						//更新库存商品库存状况
						stockProductService.saveOrUpdateStockProduct(params);
						//更新库存商品统一价格
						stockProductPriceServiceI.updateStockProductPrice(productEntity, stockEntity,request);
				}
				message = "进销存盘点添加成功";
				
				//记录操作日志
				opLog(request, OperationLogUtil.STOCK_CHECK_COMMIT, stockCheckEntity, "盘点("+ stockCheckEntity.getName() + ")成功");
			}else{
				result.setSuccess(false);
				message = "进销存盘点失败，您没有操作任何商品！";
				delete(stockCheckEntity);//回滚
			}
			
		} catch (Exception e) {
			message = "进销存盘点添加失败" + e;
			result.setSuccess(false);
		}
		
		result.setMsg(message);
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		
		return result;
	}
	/**
	 * 添加盘点单：保存，不提交
	 * @param stockCheckOrder
	 * @param request
	 * @return
	 */
	public AjaxJson saveCheck(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		
		if(!StringUtil.isNotEmpty(stockCheckOrder.getName())){
			result.setSuccess(false);
			result.setMsg("请填写盘点名称！");
			return result;
		}
		
		StockEntity stockEntity = stockCheckOrder.getStockid();
		if(stockEntity == null){
			result.setSuccess(false);
			result.setMsg("请添加仓库！");
			return result;
		}else if(!StringUtil.isNotEmpty(stockEntity.getId())){
			result.setSuccess(false);
			result.setMsg("请添加仓库！");
			return result;
		}
		
		TSUser user = ResourceUtil.getSessionUserName();
		try{
			if (StringUtil.isNotEmpty(stockCheckOrder.getId())) {
				message = "进销存盘点总单更新成功";
				StockCheckEntity t = get(StockCheckEntity.class, stockCheckOrder.getId());
				MyBeanUtils.copyBeanNotNull2Bean(stockCheckOrder, t);
				saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} else {
				TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
				TSUser operator = systemService.findUniqueByProperty(TSUser.class, "id", stockCheckOrder.getOperator().getId());
				StockEntity stock = systemService.findUniqueByProperty(StockEntity.class, "id", stockCheckOrder.getStockid().getId());
				//创建基础数据
				StockCheckEntity stockCheckEntity = new StockCheckEntity();
				stockCheckEntity.setName(stockCheckOrder.getName());
				stockCheckEntity.setIdentifier(IdWorker.generateSequenceNo());
				stockCheckEntity.setCompanyid(company);
				stockCheckEntity.setCreatetime(DateUtils.getDate());
				stockCheckEntity.setOperator(operator);
				stockCheckEntity.setstockCheckDetailList(null);
				stockCheckEntity.setStockid(stock);
				stockCheckEntity.setRemark(stockCheckOrder.getRemark());
				
				save(stockCheckEntity);
				
				//创建出入库详细数据
				List<StockCheckDetailEntity> stockCheckDetaillList = stockCheckDetailService.buildStockCheckDetail(stockCheckEntity, request);
				
				if(stockCheckDetaillList.size() <= 0){
					result.setSuccess(false);
					result.setMsg("请添加商品信息！");
					return result;
				}
				
				if(stockCheckDetaillList.size() > 0){
					stockCheckEntity.setstockCheckDetailList(stockCheckDetaillList);
					//状态：正在盘点状态
					stockCheckEntity.setStatus("1");
					updateEntitie(stockCheckEntity);
					
					message = "进销存盘点保存成功";
					
					//记录操作日志
					opLog(request, OperationLogUtil.STOCK_CHECK_SAVE, stockCheckEntity, "保存盘点单("+ stockCheckEntity.getName() + ")");
					
				}else{
					result.setSuccess(false);
					message = "进销存盘点失败，您没有操作任何商品！";
					delete(stockCheckEntity);//回滚
				}
			}
		}catch (Exception e) {
			message = "进销存盘点添加失败" + e;
			result.setSuccess(false);
		}
		
		result.setMsg(message);
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		
		return result;
	}
	
	public AjaxJson mergeCommit(HttpServletRequest request, String checkids, String name ){
		AjaxJson result = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		//1、根据checkids把所有盘点大单和所有小单取出来
		String[] idlist = checkids.split(",");
		CriteriaQuery cq = new CriteriaQuery(StockCheckEntity.class);
		cq.in("id", idlist);
		cq.add();
		List<StockCheckEntity> checklist = getListByCriteriaQuery(cq, false);
		if( null == checklist || checklist.size() == 0){
			result.setSuccess(false);
			result.setMsg("找不到盘点单");
			return result;
		}
		
		Map<String, StockEntity> stockidmap = new HashMap<String, StockEntity>();
		for( StockCheckEntity check : checklist){
			stockidmap.put(check.getStockid().getId(), check.getStockid());
		}
		
		if(stockidmap.size() != 1){//合并的盘点单不属于同一个仓库
			result.setSuccess(false);
			result.setMsg("要合并的盘点单必须是同一个仓库的盘点单");
			return result;
		}
		
		StringBuffer namebuf = new StringBuffer();
		StringBuffer remarksbuf = new StringBuffer();
		Map<String, List<StockCheckDetailEntity>> detailMap = new HashMap<String, List<StockCheckDetailEntity>>();
		//合并盘点基本信息
		for(StockCheckEntity check : checklist){
			StockEntity stock = check.getStockid();
			List<StockCheckDetailEntity> detaillist = check.getstockCheckDetailList();
			for( StockCheckDetailEntity checkdetail : detaillist){
				//盘点详情
				String key = checkdetail.getStockproductid().getId();
				List<StockCheckDetailEntity> tmplist = detailMap.get(key);
				if(tmplist == null){
					tmplist = new ArrayList<StockCheckDetailEntity>();
					detailMap.put(key, tmplist);
				}
				tmplist.add(checkdetail);
			}
			//状态：盘点备份
			check.setStatus("3");
			remarksbuf.append(check.getRemark()).append(";");
			namebuf.append("[").append(check.getName()).append("]");
		}
		batchUpdate(checklist);
		
		TSCompany companyEntity = checklist.get(0).getCompanyid();
		StockEntity stockEntity = checklist.get(0).getStockid();
		
		//2、生成新的盘点大单和合并所有小单
		StockCheckEntity newCheck = new StockCheckEntity();
		newCheck.setIdentifier(IdWorker.generateSequenceNo());
		//状态：盘点完成
		newCheck.setStatus("2");
		newCheck.setCreatetime(DateUtils.getDate());
		newCheck.setCompanyid(companyEntity);
		newCheck.setName(name);
		newCheck.setMergeids(checkids);
		newCheck.setOperator(user);
		newCheck.setStockid(stockEntity);
		newCheck.setRemark(remarksbuf.toString());
		save(newCheck);
		
		List<StockCheckDetailEntity> stockCheckDetailList = new ArrayList<StockCheckDetailEntity>();		  
		for (Map.Entry<String, List<StockCheckDetailEntity>> entry : detailMap.entrySet()) {  
			String stockProductId = entry.getKey();
			
			StockProductEntity stockProductEntity = findUniqueByProperty(StockProductEntity.class, "id", stockProductId);		
			ProductEntity productEntity = stockProductEntity.getProductid();
			
			StockCheckDetailEntity detail = null;
			List<StockCheckDetailEntity> tempList = detailMap.get(stockProductId);
			if( null != tempList && tempList.size() > 0 ){
				StringBuffer remarks = new StringBuffer();
				//统计调整数量
				BigDecimal adJustStockNums = new BigDecimal(0.00);
				//统计盘点数量
				BigDecimal checkStockNums = new BigDecimal(0.00);
				//合并相同的盘点详情
				for(StockCheckDetailEntity stockCheckDetailEntity : tempList){
						//合并备注
						if(stockCheckDetailEntity.getUnitid() != null){
							remarks.append(stockCheckDetailEntity.getRemark()).append(";");
						}
						//当前调整数量
//						BigDecimal adJustStockNum = stockCheckDetailEntity.getAdjuststocknum();
						//合并调整数量
//						if(adJustStockNum.compareTo(new BigDecimal(0.00)) < 0){
//							if(adJustStockNums.compareTo(new BigDecimal(0.00)) < 0){
//								adJustStockNum = adJustStockNum.abs();
//								adJustStockNums = adJustStockNums.abs();
//								adJustStockNums = adJustStockNums.add(adJustStockNum);
//								adJustStockNums = adJustStockNums.negate();
//							}else{
//								adJustStockNums = adJustStockNums.add(adJustStockNum);
//							}
//						}else{
//							if(adJustStockNums.compareTo(new BigDecimal(0.00)) < 0){
//								adJustStockNums = adJustStockNum.add(adJustStockNums);
//							}else{
//								adJustStockNums = adJustStockNums.add(adJustStockNum);
//							}
//						}
						BigDecimal checkStockNum = stockCheckDetailEntity.getCheckstocknum();
						checkStockNum = checkStockNum.abs();
						checkStockNums = checkStockNums.add(checkStockNum);
				}
				//当前库存数量
				BigDecimal stockNum = stockProductEntity.getStocknum();
				//当前库存均价
				BigDecimal avgPrice = stockProductEntity.getAverageprice();
				//合并后：调整金额
//				BigDecimal adJustStockMoney = avgPrice.multiply(checkStockNums);
				
				detail = new StockCheckDetailEntity();
				detail.setCheckstocknum(checkStockNums);
				detail.setCurrentstocknum(stockNum);
				detail.setCheckorderid(newCheck);
				detail.setRemark(remarks.toString());
				detail.setStockproductid(stockProductEntity);
				detail.setUnitid(productEntity.getUnit());
				detail.setAdjuststocknum(new BigDecimal(0.00));
				detail.setAdjustmoney(new BigDecimal(0.00));
				detail.setAvgPrice(avgPrice);
				stockCheckDetailList.add(detail);
			}
		  
		}  
		newCheck.setstockCheckDetailList(stockCheckDetailList);
		updateEntitie(newCheck);
		
		//3、更新库存
		for (StockCheckDetailEntity stockCheckDetailEntity : stockCheckDetailList) {
			try {
				ProductEntity productEntity = this.findUniqueByProperty(ProductEntity.class, "id", stockCheckDetailEntity.getStockproductid().getProductid().getId());
				Map<String, Object> params = new HashMap<>();
				StockProductEntity stockProduct = stockCheckDetailEntity.getStockproductid();
				StockEntity stock  = stockProduct.getStockid();
				StockSegmentEntity segmentEntity = stockProduct.getStockSegment();
				ProductEntity product = stockProduct.getProductid();
				params.put("stockEntity", stock);
				params.put("segmentEntity", segmentEntity);
				params.put("companyEntity", user.getCompany());
				params.put("productEntity",product);
				
				BigDecimal stockNum =  stockProduct.getStocknum();
				BigDecimal avgPrice =  stockProduct.getAverageprice();
				
				//盘点数量
				BigDecimal checkStockNum = stockCheckDetailEntity.getCheckstocknum();
				//调整数量
				BigDecimal adJustStockNum = new BigDecimal(0.00);
				//调整金额
				BigDecimal adJustStockMoney = new BigDecimal(0.00);
				//盘点调整的总价
				BigDecimal checkTotalPrice = new BigDecimal(0.00);
				String ioType = null;
				
//				if(adJustStockNum.compareTo(stockNum) == 0){
//					ioType = "O";
//					adJustStockNum = stockNum;
//				}else if(adJustStockNum.compareTo(stockNum) > 0){
//					ioType = "I";
//					adJustStockNum = adJustStockNum.subtract(stockNum);
//				}else if(adJustStockNum.compareTo(stockNum) < 0){
//					ioType = "O";
//					adJustStockNum = adJustStockNum.compareTo(new BigDecimal(0.00)) < 0 ? new BigDecimal(0.00) : adJustStockNum;
//					adJustStockNum = stockNum.subtract(adJustStockNum);
//				}
				
				if(checkStockNum.compareTo(stockNum) == 0){
					ioType = "O";
					adJustStockNum = new BigDecimal(0.00);
					adJustStockMoney = new BigDecimal(0.00);
					stockCheckDetailEntity.setAdjuststocknum(adJustStockNum);
					stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
				}else if(checkStockNum.compareTo(stockNum) > 0){
					ioType = "I";
					adJustStockNum = checkStockNum.subtract(stockNum);
					adJustStockMoney = adJustStockNum.multiply(avgPrice);
					stockCheckDetailEntity.setAdjuststocknum(adJustStockNum);
					stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
				}else if(checkStockNum.compareTo(stockNum) < 0){
					ioType = "O";
					if(checkStockNum.compareTo(new BigDecimal(0.00)) == 0){
						adJustStockNum = stockNum;
						adJustStockMoney = (adJustStockNum.negate()).multiply(avgPrice);
						stockCheckDetailEntity.setAdjuststocknum(adJustStockNum.negate());
						stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
					}else{
						checkStockNum = checkStockNum.abs();
						adJustStockNum = stockNum.subtract(checkStockNum);
						adJustStockMoney = (adJustStockNum.negate()).multiply(avgPrice);
						stockCheckDetailEntity.setAdjuststocknum(adJustStockNum.negate());
						stockCheckDetailEntity.setAdjustmoney(adJustStockMoney);
					}
				}
				checkTotalPrice = adJustStockNum.multiply(avgPrice);
				params.put("ioType", ioType);
				params.put("op_stockNum",adJustStockNum);
				params.put("op_totalPrice",checkTotalPrice);
				//更新盘点详情盈亏情况
				updateEntitie(stockCheckDetailEntity);
				//更新库存商品库存状况
				stockProductService.saveOrUpdateStockProduct(params);
				//更新库存商品统一价格
				stockProductPriceServiceI.updateStockProductPrice(productEntity, stockEntity,request);
			} catch (Exception e) {
				result.setSuccess(false);
				message = "盘点库存更新库存产品数量出错：" + e;
			}
			
		}
		
		//记录操作日志
		opLog(request, OperationLogUtil.STOCK_CHECK_MULTICOMMIT, newCheck, "合并盘点单("+namebuf.toString()+")为" + newCheck.getName() );
		
		message = "进销存盘点单合并成功(new:"+newCheck.getId()+",old:"+checkids+")";
		systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		
		return result;
	}
}