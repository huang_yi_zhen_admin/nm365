package cn.gov.xnc.admin.stock.controller;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserRoleService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.admin.order.entity.PayBillsOrderEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockTransferDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockTransferEntity;
import cn.gov.xnc.admin.stock.service.StockTransferDetailServiceI;

/**   
 * @Title: Controller
 * @Description: 调拨单信息
 * @author zero
 * @date 2018-01-09 09:15:23
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockTransferDetailController")
public class StockTransferDetailController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockTransferDetailController.class);

	@Autowired
	private StockTransferDetailServiceI stockTransferDetailService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserRoleService userRoleService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 调拨单信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockTransferDetail(HttpServletRequest request) {
		return new ModelAndView("admin/stock/stockTransferDetailList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockTransferDetailEntity stockTransferDetail,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = getDatagridCriteriaQuery(stockTransferDetail, request, response, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockTransferDetail, request.getParameterMap());
		this.stockTransferDetailService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	private CriteriaQuery getDatagridCriteriaQuery(StockTransferDetailEntity stockTransferDetail, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid){
		String sortName = request.getParameter("sortName"), sortDesc = request.getParameter("sortDesc");
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockTransferDetailEntity.class, dataGrid);
		if( null!= stockTransferDetail.getProductid() ){
			stockTransferDetail.getProductid().setIsDelete(null);
			stockTransferDetail.getProductid().setNotsale(null);
			if( StringUtil.isNotEmpty(stockTransferDetail.getProductid().getName()) ){
				cq.createAlias("productid", "product");
				cq.add(Restrictions.like("product.name", "%"+stockTransferDetail.getProductid().getName()+"%"));
				stockTransferDetail.getProductid().setName(null);
			}
		}
		
		if( null!=stockTransferDetail.getTransferid() ){
			if( null!=stockTransferDetail.getTransferid().getCreator() && StringUtil.isNotEmpty(stockTransferDetail.getTransferid().getCreator().getRealname()) ){
				cq.createAlias("transferid.creator", "oper");
				cq.add(Restrictions.like("oper.realname", "%"+stockTransferDetail.getTransferid().getCreator().getRealname()+"%"));
				stockTransferDetail.getTransferid().getCreator().setRealname(null);
			}
			if( null != stockTransferDetail.getTransferid().getFromstockid() && StringUtil.isNotEmpty(stockTransferDetail.getTransferid().getFromstockid().getId() )){
				cq.createAlias("transferid.fromstockid", "fromstock");
				cq.add(Restrictions.eq("fromstock.id",stockTransferDetail.getTransferid().getFromstockid().getId()));
				stockTransferDetail.getTransferid().getFromstockid().setId(null);;
			}
			if( null != stockTransferDetail.getTransferid().getTostockid() && StringUtil.isNotEmpty(stockTransferDetail.getTransferid().getTostockid().getId() )){
				cq.createAlias("transferid.tostockid", "tostock");
				cq.add(Restrictions.eq("tostock.id",stockTransferDetail.getTransferid().getTostockid().getId()));
				stockTransferDetail.getTransferid().getTostockid().setId(null);;
			}
			
			stockTransferDetail.getTransferid().setCompanyid(user.getCompany());
			cq.createAlias("transferid.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
		}else{
			stockTransferDetail.setTransferid(new StockTransferEntity());
			stockTransferDetail.getTransferid().setCompanyid(user.getCompany());
			cq.createAlias("transferid.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
		}
		//如果不是管理员类型，添加仓库负责人条件
//		TSRole role = userRoleService.getRoleByUserRole(user);
//		if(!role.getRoleCode().equals("admin")){
//			cq.createAlias("transferid.stockid", "stock");
//			cq.createAlias("stock.dutyperson", "stockUser");
//			cq.eq("stockUser.id", user.getId());
//		}
		
		
		//时间
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("updatetime", DateUtils.str2Date(createdate1,DateUtils.date_sdf), DateUtils.str2Date(createdate2,DateUtils.date_sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("updatetime", DateUtils.str2Date(createdate1,DateUtils.date_sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("updatetime", DateUtils.str2Date(createdate2,DateUtils.date_sdf));
		}
		//cq.addOrder("updatetime", SortDirection.desc);
		
		if( StringUtil.isNotEmpty(sortName) ){
			if( "asc".equals(sortDesc) ){
				cq.addOrder(sortName, SortDirection.asc);
			}else{
				cq.addOrder(sortName, SortDirection.desc);
			}
		}else{
			cq.addOrder("updatetime", SortDirection.desc);
		}
		
		return cq;
	}
	
	@RequestMapping(value = "datagridStatis")
	@ResponseBody
	public AjaxJson datagridStatis(StockTransferDetailEntity stockTransferDetail,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid ) {
		AjaxJson j = new AjaxJson();
		j.setSuccess(false);
		j.setMsg("查询失败");
		
		CriteriaQuery cq = getDatagridCriteriaQuery(stockTransferDetail, request, response, dataGrid);
		
		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.sum("totalprice"));
		projectionList.add(Projections.sum("freightprice"));

		
		cq.setProjectionList(projectionList);
		
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockTransferDetail, request.getParameterMap());
		stockTransferDetailService.getDataGridReturn(cq, true);
		
		dataGrid = cq.getDataGrid();
		
		List<Object[]> list = dataGrid.getResults();
		
		if( null!= list && list.size() > 0 ){
			Object[] objlist = list.get(0);
			Map<String,Object> map = new HashMap<>();
			map.put("totalprice", (BigDecimal)objlist[0]);
			map.put("totalfreight",(BigDecimal) objlist[1]);
			
			j.setObj(map);
			j.setSuccess(true);
			j.setMsg("查询成功");
		}
		
		
		return j;
	}
	/**
	 * 删除调拨单信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockTransferDetailEntity stockTransferDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockTransferDetail = systemService.getEntity(StockTransferDetailEntity.class, stockTransferDetail.getId());
		message = "调拨单信息删除成功";
		stockTransferDetailService.delete(stockTransferDetail);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加调拨单信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockTransferDetailEntity stockTransferDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(stockTransferDetail.getId())) {
			message = "调拨单信息更新成功";
			StockTransferDetailEntity t = stockTransferDetailService.get(StockTransferDetailEntity.class, stockTransferDetail.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockTransferDetail, t);
				stockTransferDetailService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "调拨单信息更新失败";
			}
		} else {
			message = "调拨单信息添加成功";
			stockTransferDetailService.save(stockTransferDetail);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 调拨单信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockTransferDetailEntity stockTransferDetail, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockTransferDetail.getId())) {
			stockTransferDetail = stockTransferDetailService.getEntity(StockTransferDetailEntity.class, stockTransferDetail.getId());
			req.setAttribute("stockTransferDetailPage", stockTransferDetail);
		}
		return new ModelAndView("admin/stock/stockTransferDetail");
	}
	
	/**
	 * 出库详细单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockTransferDetailInfos")
	public ModelAndView stockInDetailInfos(HttpServletRequest request) {
		String stockTransferId = request.getParameter("id");
		StockTransferEntity stockTransfer = new StockTransferEntity();
		if(StringUtil.isNotEmpty(stockTransferId)){
			stockTransfer = stockTransferDetailService.findUniqueByProperty(StockTransferEntity.class, "id", stockTransferId);
		}
		request.setAttribute("stockTransfer", stockTransfer);
		
		return new ModelAndView("admin/stock/stockTransferDetailInfos");
	}
}
