package cn.gov.xnc.admin.stock.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.service.StockSupplierServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("stockSupplierService")
@Transactional
public class StockSupplierServiceImpl extends CommonServiceImpl implements StockSupplierServiceI {
	
}