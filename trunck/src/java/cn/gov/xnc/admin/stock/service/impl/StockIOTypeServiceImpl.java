package cn.gov.xnc.admin.stock.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.service.StockIOTypeServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("stockTypeService")
@Transactional
public class StockIOTypeServiceImpl extends CommonServiceImpl implements StockIOTypeServiceI {

	@Override
	public Map<String, Object> getParams(StockIOTypeEntity stockIOTypeEntity) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		String typeName = stockIOTypeEntity.getTypename();
		if(typeName != null){
			params.put("typename", typeName);
		}
		String typeCode = stockIOTypeEntity.getTypecode();
		if(typeCode != null){
			params.put("typecode", typeCode);
		}
		if(user != null){
			params.put("companyid", user.getCompany().getId());
		}
		return params;
	}

	@Override
	public List<StockIOTypeEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(StockIOTypeEntity.class);
		String typename = (String) params.get("typename");
		if(typename != null){
			cq.eq("typename", typename);
		}
		
		String typecode = (String) params.get("typecode");
		if(typecode != null){
			cq.eq("typecode", typecode);
		}
		
		String companyid = (String) params.get("companyid");
		if(companyid != null){
			cq.eq("companyid.id", companyid);
		}
		
		cq.add();
		List<StockIOTypeEntity> list = this.getListByCriteriaQuery(cq, false);
		if(list.size() <= 0 || list == null){
			return null;
		}
		return list;
	}

	@Override
	public StockIOTypeEntity checkStockIOType(Map<String, Object> params) {
		TSUser user = ResourceUtil.getSessionUserName();
		List<StockIOTypeEntity>  stockIOTypes = this.findListByParams(params);
		if(stockIOTypes == null){
			StockIOTypeEntity stockIOTypeEntity = new StockIOTypeEntity();
			stockIOTypeEntity.setTypecode("O");
			stockIOTypeEntity.setTypename("销售出库");
			stockIOTypeEntity.setCompanyid(user.getCompany());
			save(stockIOTypeEntity);
			return stockIOTypeEntity;
		}
		return stockIOTypes.get(0);
	}
	
}