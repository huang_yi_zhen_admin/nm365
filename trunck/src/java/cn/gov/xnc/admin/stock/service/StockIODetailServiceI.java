package cn.gov.xnc.admin.stock.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;

public interface StockIODetailServiceI extends CommonService{
	
	/**
	 * 统计出入库详情
	 * @param params
	 * @return
	 */
	public Map<String,Object> statisCount(Map<String, Object> params);
	
	/**
	 * 创建出入库详情数据
	 * @param stockIOEntity
	 * @param req
	 * @param result
	 * @return
	 */
//	public List<StockIODetailEntity> createStockIODetail(StockIOEntity stockIOEntity, HttpServletRequest req,AjaxJson result);
	
	/**
	 * 创建出入库详情数据
	 * @param stockIOEntity
	 * @param req
	 * @param result
	 * @return
	 */
	public List<StockIODetailEntity> createStockIODetail(StockIOEntity stockIOEntity, List<Map<String, Object>> params);
	
	/**
	 * 构造出入库详情参数
	 * @param req
	 * @return
	 */
	public List<Map<String, Object>> buildStockIODetailParams(HttpServletRequest req);
	
	/**
	 * 获取最近/上一次入库详情信息
	 * @param productid
	 * @return
	 */
	public StockIODetailEntity getLastStockInDetail(String productid);
	
	public List<StockIODetailEntity> findListByParams(Map<String, Object> params);
	
	public Map<String, Object> getParams(ProductEntity product,StockEntity stock,StockSegmentEntity segment);
}
