package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.service.StockCheckDetailServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

@Service("stockCheckDetailService")
@Transactional
public class StockCheckDetailServiceImpl extends CommonServiceImpl implements StockCheckDetailServiceI {
	
	private Logger logger = Logger.getLogger(StockCheckDetailServiceImpl.class);
	
	@Override
	public List<StockCheckDetailEntity> buildStockCheckDetail(StockCheckEntity stockCheckEntity, HttpServletRequest req) throws Exception {
		List<StockCheckDetailEntity> stockCheckDetailList = new ArrayList<StockCheckDetailEntity>();
		//盘点详情数量
		String expLen = ResourceUtil.getParameter("Len");
		
		if(StringUtil.isNotEmpty(expLen)){
			
			int stockIODetailsNum = Integer.parseInt(expLen);
			for (int i = 0; i < stockIODetailsNum; i++) {
				//商品编号
				String stockproductid = req.getParameter("id-" + i);
				//商品名称
				String productName = req.getParameter("productid_name-" + i);
				
				if(StringUtil.isNotEmpty(stockproductid) && StringUtil.isNotEmpty(productName)){
					
					StockProductEntity stockProductEntity = findUniqueByProperty(StockProductEntity.class, "id", stockproductid);
					
					//当前库存商品数量
					BigDecimal stocknum = ConvertTool.toBigDecimal(req.getParameter("stocknum-" + i));
					//当前库存商品均价
					BigDecimal avgPrice = stockProductEntity.getAverageprice();
					//盘点数量
					BigDecimal checkstocknum = ConvertTool.toBigDecimal(req.getParameter("checkstocknum-" + i));
					//盘点单位
					String unitid = req.getParameter("unitid-" + i);
					ProductUnitEntity unitidEntity = this.findUniqueByProperty(ProductUnitEntity.class, "id", unitid);
					//调整数量
					//BigDecimal adjuststocknum = ConvertTool.toBigDecimal(req.getParameter("adjuststocknum-" + i));
					//调整金额
					//BigDecimal adJustMoney = adjuststocknum.multiply(avgPrice);
					//备注
					String remark = req.getParameter("remark-" + i);
					
					StockCheckDetailEntity stockCheckDetail = new StockCheckDetailEntity();
					stockCheckDetail.setAdjuststocknum(new BigDecimal(0.00));
					stockCheckDetail.setCheckorderid(stockCheckEntity);
					stockCheckDetail.setCheckstocknum(checkstocknum);
					stockCheckDetail.setCurrentstocknum(stocknum);
					stockCheckDetail.setRemark(remark);
					stockCheckDetail.setUnitid(unitidEntity);
					stockCheckDetail.setAvgPrice(avgPrice);
					stockCheckDetail.setAdjustmoney(new BigDecimal(0.00));
					stockCheckDetail.setStockproductid(stockProductEntity);
					stockCheckDetailList.add(stockCheckDetail);
				}
				
			}
		}
		
		if(stockCheckDetailList.size() > 0){
			
			batchSave(stockCheckDetailList);
		}
		return stockCheckDetailList;
	}
	
}