package cn.gov.xnc.admin.stock.controller;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserRoleService;
import cn.gov.xnc.system.web.system.service.UserService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.IdWorker;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.RoletoJson;
import cn.gov.xnc.admin.configure.service.TSconfigureServiceI;
import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckDetailEntity;
import cn.gov.xnc.admin.stock.entity.StockCheckEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockIOTypeEntity;
import cn.gov.xnc.admin.stock.service.StockCheckDetailServiceI;
import cn.gov.xnc.admin.stock.service.StockCheckServiceI;
import cn.gov.xnc.admin.stock.service.StockServiceI;

/**   
 * @Title: Controller
 * @Description: 进销存盘点总单
 * @author zero
 * @date 2017-01-23 15:39:08
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockCheckController")
public class StockCheckController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockCheckController.class);
	
	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private TSconfigureServiceI configureService;
	@Autowired
	private StockCheckServiceI stockCheckService;
	@Autowired
	private StockCheckDetailServiceI stockCheckDetailService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserService userService;
	@Autowired
	private StockServiceI stockService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 进销存盘点总单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView stockCheckOrder(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		//根据用户权限获取相关权限
		TSRole role = userRoleService.getRoleByUserRole(user);
		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("companyid", company.getId());
		params.put("status", "1");
		
		if(!role.getRoleCode().equals("admin")){
			params.put("dutypersonid", user.getId());
		}
		
		List<StockEntity> stockList = stockService.findListByParams(params);
		List<TSUser> userlist = userService.findUserList(company.getId(), "1,2".split(","));
		
		request.setAttribute("userlist", userlist);
		request.setAttribute("stocklist", stockList);
		
		String lockstockid = configureService.getLockedStockId();
		if( StringUtil.isNotEmpty(lockstockid)){
			StockEntity lockedStock = systemService.getEntity(StockEntity.class, lockstockid);
			request.setAttribute("lockedStock", lockedStock);
			request.setAttribute("locked", true);
		}else{
			request.setAttribute("locked", false);
		}
		
		return new ModelAndView("admin/stock/stockCheckList");
	}
	
	@RequestMapping(value = "selectLockStock")
	public ModelAndView selectLockStock(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("companyid", user.getCompany().getId());
		params.put("status", "1");
		
		List<StockEntity> stockList = stockService.findListByParams(params);
		request.setAttribute("stocklist", stockList);
		
		return new ModelAndView("admin/stock/selectLockStock");
	}
	
	/**
	 * 添加盘点单：保存，不提交
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "saveCheck")
	@ResponseBody
	public AjaxJson saveCheck(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		try {
			result = stockCheckService.saveCheck(stockCheckOrder, request);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("加进销存盘点总单出错：" + e);
			
			systemService.addLog("加进销存盘点总单出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return result;
	}
	
	/**
	 * 打开合并单页面
	 * 
	 * */
	@RequestMapping(value = "mergeCheck")
	public ModelAndView mergeCheck(HttpServletRequest request) {
		String ids = request.getParameter("ids");
		if( StringUtil.isNotEmpty(ids) ){
			request.setAttribute("ids", ids);
		}
		
		return new ModelAndView("admin/stock/stockCheckMerge");
	}
	
	/**
	 * 添加进销存盘点总单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "mergeCommit")
	@ResponseBody
	public AjaxJson mergeCommit(String ids, String name, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		
		if( StringUtil.isEmpty(ids) ){
			result.setSuccess(false);
			result.setMsg("请先选择要合并的盘点单");
			return result;
		}
		
		if( StringUtil.isEmpty(name) ){
			result.setSuccess(false);
			result.setMsg("请输入合并盘点单名称");
			return result;
		}
		
		try {
			result = stockCheckService.mergeCommit(request, ids, name);
			
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("加进销存盘点总单出错：" + e);
			
			systemService.addLog("加进销存盘点总单出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return result;
	}
	
	/**
	 * 删除盘点单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delStockCheck")
	public ModelAndView delStockCheck(HttpServletRequest request) {
		
		String ids = request.getParameter("ids");
		if( StringUtil.isNotEmpty(ids) ){
			request.setAttribute("ids", ids);
		}
		
		if( StringUtil.isNotEmpty(ids) ){
			CriteriaQuery cq = new CriteriaQuery(StockCheckEntity.class);
			String[] idlist = ids.split(",");
			cq.in("id", idlist);
			cq.add();
			List<StockCheckEntity> checklist = stockCheckService.getListByCriteriaQuery(cq, false);
			StringBuffer namebuf = new StringBuffer();
			for( StockCheckEntity check:checklist){
				namebuf.append("【").append(check.getName()).append("】 ");
			}
			request.setAttribute("name", namebuf.toString());
		}

		return new ModelAndView("admin/stock/stockCheckDel");
	}
	
	/**
	 * 删除进销存盘点总单
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(String ids, HttpServletRequest request) {
		AjaxJson result;
		
		if( StringUtil.isEmpty(ids) ){
			result = new AjaxJson();
			result.setSuccess(false);
			result.setMsg("请先选择要删除的盘点单");
			return result;
		}
		
		result = stockCheckService.delCheckOrders(request,ids);
		
		if(result.isSuccess() ){
			message = "删除盘点单成功(" + ids + ")";
		}else{
			message = "删除盘点单失败(" + ids + ")";
		}
		systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);

		return result;
	}
	
	/**
	 * 锁定仓库
	 * 
	 * @return
	 */
	@RequestMapping(value = "lockStock")
	@ResponseBody
	public AjaxJson lockStock(String stockid, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		j = configureService.lockStock(stockid);
		
		
		if( j.isSuccess() ){
			StockEntity stock = systemService.getEntity(StockEntity.class, stockid);
			systemService.opLog(request, OperationLogUtil.STOCK_LOCK, stock, "用户"+user.getRealname()+"操作对仓库("+stock.getStockname() +")加锁", user);
		}
		
		return j;
	}
	
	/**
	 * 解锁仓库
	 * 
	 * @return
	 */
	@RequestMapping(value = "unlockStock")
	@ResponseBody
	public AjaxJson unlockStock(String stockid, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		j = configureService.unlockStock();
		
		
		if( j.isSuccess() ){
			systemService.opLog(request, OperationLogUtil.STOCK_LOCK, user, "用户"+user.getRealname()+"操作对仓库解锁" );
		}
		
		return j;
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockCheckEntity stockCheckOrder,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(StockCheckEntity.class, dataGrid);
		TSUser user = ResourceUtil.getSessionUserName();
		//盘点名称
		if(StringUtil.isNotEmpty(stockCheckOrder.getName())){
			cq.like("name", "%" + stockCheckOrder.getName() + "%");
			stockCheckOrder.setName(null);
		}
		//盘点编号
		if(StringUtil.isNotEmpty(stockCheckOrder.getIdentifier())){
			cq.eq("identifier", stockCheckOrder.getIdentifier());
		}
		//仓库
		if(StringUtil.isNotEmpty(stockCheckOrder.getStockid())){
			cq.eq("stockid.id", stockCheckOrder.getStockid().getId());
		}
		//状态
		if(StringUtil.isNotEmpty(stockCheckOrder.getStatus())){
			cq.eq("status", stockCheckOrder.getStatus());
		}
		//经办人
		if(StringUtil.isNotEmpty(stockCheckOrder.getOperator())){
			cq.eq("operator.id", stockCheckOrder.getOperator().getId());
		}
		cq.eq("companyid", user.getCompany());
		
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		//时间
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("createtime", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("createtime", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("createtime", DateUtils.str2Date(createdate2,sdf));
		}
		if(null != stockCheckOrder.getStockid()){
			stockCheckOrder.getStockid().setCompanyid(user.getCompany());
		}else{
			StockEntity stock = new StockEntity();
			stock.setCompanyid(user.getCompany());
			stockCheckOrder.setStockid(stock);
		}
		cq.addOrder("createtime", SortDirection.desc);
		//查询条件组装器
		HqlGenerateUtil.installHql(cq, stockCheckOrder, request.getParameterMap());
		this.stockCheckService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除进销存盘点总单
	 * 
	 * @return
	 */
//	@RequestMapping(value = "del")
//	@ResponseBody
//	public AjaxJson del(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
//		AjaxJson j = new AjaxJson();
//		stockCheckOrder = systemService.getEntity(StockCheckEntity.class, stockCheckOrder.getId());
//		message = "进销存盘点总单删除成功";
//		stockCheckService.delete(stockCheckOrder);
//		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
//		
//		j.setMsg(message);
//		return j;
//	}


	/**
	 * 添加盘点单：保存，提交
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "saveCommit")
	@ResponseBody
	public AjaxJson saveCommit(StockCheckEntity stockCheckOrder, HttpServletRequest request) {
		AjaxJson result = new AjaxJson();
		try {
			result = stockCheckService.saveCommit(stockCheckOrder, request);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("加进销存盘点总单出错：" + e);
			
			systemService.addLog("加进销存盘点总单出错：" + e, Globals.Log_Type_DEL, Globals.Log_Leavel_ERROR);
		}
		return result;
	}

	/**
	 * 进销存盘点总单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockCheckEntity stockCheck, HttpServletRequest req) {
		TSUser user = ResourceUtil.getSessionUserName();
		req.setAttribute("operater", user);
		if (StringUtil.isNotEmpty(stockCheck.getId())) {
			stockCheck = stockCheckService.getEntity(StockCheckEntity.class, stockCheck.getId());
			
		}
		
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		req.setAttribute("stocklist", stockList);
				
		req.setAttribute("stockCheck", stockCheck);
		return new ModelAndView("admin/stock/stockCheckAdd");
	}
	
	/**
	 * 进销存盘点总单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockCheckDetail")
	public ModelAndView stockCheckDetail(StockCheckEntity stockCheckOrder, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockCheckOrder.getId())) {
			stockCheckOrder = stockCheckService.getEntity(StockCheckEntity.class, stockCheckOrder.getId());
			
		}
		req.setAttribute("stockCheckDetail", stockCheckOrder);
		return new ModelAndView("admin/stock/stockCheckDetail");
	}
}
