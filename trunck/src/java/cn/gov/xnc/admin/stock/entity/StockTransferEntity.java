package cn.gov.xnc.admin.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 调拨单信息
 * @author zero
 * @date 2018-01-09 09:14:57
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_transfer", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockTransferEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**经办人*/
	private TSUser creator;
	/**源仓库*/
	private StockEntity fromstockid;
	/**目标仓库*/
	private StockEntity tostockid;
	/**创建时间*/
	private java.util.Date createtime;
	/**备注*/
	private java.lang.String remark;
	/**所属公司*/
	private TSCompany companyid;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  经办人
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATOR")
	public TSUser getCreator(){
		return this.creator;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  经办人
	 */
	public void setCreator(TSUser creator){
		this.creator = creator;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  源仓库
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FROMSTOCKID")
	public StockEntity getFromstockid(){
		return this.fromstockid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  源仓库
	 */
	public void setFromstockid(StockEntity fromstockid){
		this.fromstockid = fromstockid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  目标仓库
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TOSTOCKID")
	public StockEntity getTostockid(){
		return this.tostockid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  目标仓库
	 */
	public void setTostockid(StockEntity tostockid){
		this.tostockid = tostockid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建时间
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.util.Date getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建时间
	 */
	public void setCreatetime(java.util.Date createtime){
		this.createtime = createtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=4000)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属公司
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属公司
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
}
