package cn.gov.xnc.admin.stock.controller;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.stock.entity.ProductSegmentHistoryEntity;
import cn.gov.xnc.admin.stock.service.ProductSegmentHistoryServiceI;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 产品库存历史
 * @author zero
 * @date 2017-04-20 14:41:02
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/productSegmentHistoryController")
public class ProductSegmentHistoryController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ProductSegmentHistoryController.class);

	@Autowired
	private ProductSegmentHistoryServiceI productSegmentHistoryService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 产品入库选择区域
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockInChoiseSegment")
	public ModelAndView productSegmentHistory(HttpServletRequest request) {
		
		String productid = request.getParameter("productid");
		if( StringUtil.isNotEmpty(productid)){
			request.setAttribute("productid", productid);
		}
		
		String stockid = request.getParameter("stockid");
		if( StringUtil.isNotEmpty(stockid)){
			request.setAttribute("stockid", stockid);
		}
		
		return new ModelAndView("admin/stock/stockInChoiseSegment");
	}
	
	
	/**
	 * 产品出库选择区域
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockOutChoiseSegment")
	public ModelAndView stockOutChoiseSegment(HttpServletRequest request) {
		
		String productid = request.getParameter("productid");
		if( StringUtil.isNotEmpty(productid)){
			request.setAttribute("productid", productid);
		}
		
		String stockid = request.getParameter("stockid");
		if( StringUtil.isNotEmpty(stockid)){
			request.setAttribute("stockid", stockid);
		}
		
		return new ModelAndView("admin/stock/stockOutChoiseSegment");
	}
	

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "getLocationHistory")
	public void getLocationHistory(ProductSegmentHistoryEntity productSegmentHistory ,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		//获取当前登录用户
		TSUser user = ResourceUtil.getSessionUserName();//当前登录用户
		
		CriteriaQuery cq = new CriteriaQuery(ProductSegmentHistoryEntity.class, dataGrid);
		cq.createAlias("segmentid", "seg") ;
		cq.add( Restrictions.eq("seg.status", 0 ) ) ;
		
		cq.addOrder("updateTime", SortDirection.desc);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, productSegmentHistory, request.getParameterMap());
		this.productSegmentHistoryService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 产品历史存储区域删除
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(ProductSegmentHistoryEntity productSegmentHistory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		productSegmentHistory = systemService.getEntity(ProductSegmentHistoryEntity.class, productSegmentHistory.getId());
		message = "产品历史存储区域删除成功(segmentid:" + productSegmentHistory.getSegmentid() +")";
		productSegmentHistoryService.delete(productSegmentHistory);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 添加产品库存历史
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ProductSegmentHistoryEntity productSegmentHistory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		String errMsg = "";
		boolean result = productSegmentHistoryService.save(user, productSegmentHistory, errMsg);
		j.setMsg(errMsg);
		j.setSuccess(result);
		
		if( result ){
			message = "产品库存历史添加成功(" + productSegmentHistory.getSegmentfullname() + ")";
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", productSegmentHistory.getId());
		map.put("segmentid", productSegmentHistory.getSegmentid().getId());
		map.put("segmentfullname", productSegmentHistory.getSegmentfullname());
		
		j.setAttributes(map);
		
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加产品库存历史
	 * 
	 * @param ids
	 * @return
	 */
	/*@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(ProductSegmentHistoryEntity productSegmentHistory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		
		//根据Segmentid查历史表，如果已经有记录则直接返回
		CriteriaQuery ch = new CriteriaQuery(ProductSegmentHistoryEntity.class);
		ch.eq("stockid", productSegmentHistory.getStockid());
		ch.eq("segmentid", productSegmentHistory.getSegmentid());
		List<ProductSegmentHistoryEntity> tmplist = systemService.getListByCriteriaQuery(ch, false);
		if( null != tmplist && tmplist.size() > 0 ){
			productSegmentHistory = tmplist.get(0);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", productSegmentHistory.getId());
			map.put("segmentid", productSegmentHistory.getSegmentid().getId());
			map.put("segmentfullname", productSegmentHistory.getSegmentfullname());
			
			j.setAttributes(map);
			return j;
		}
		
		//1、根据传进来的segmentid获取出segment对象和stock对象，获取不到则返回错误
		//
		StockSegmentEntity segment = systemService.getEntity(StockSegmentEntity.class, productSegmentHistory.getSegmentid().getId());
		if( null == segment ){
			j.setMsg("您的输入参数有误，请稍后重试");
			j.setSuccess(false);
			return j;
		}
		StockEntity stock = systemService.getEntity(StockEntity.class, segment.getStockid().getId());
		if( null == stock ){
			j.setMsg("您的输入参数有误，请稍后重试");
			j.setSuccess(false);
			return j;
		}
		
		//2、根据segment对象拼接出他的完成名称
		CriteriaQuery css = new CriteriaQuery(StockSegmentEntity.class);
		css.eq("stockid", segment.getStockid());
		List<StockSegmentEntity> seglist = systemService.getListByCriteriaQuery(css,false);
		Map<String, StockSegmentEntity> segmap = new HashMap<String, StockSegmentEntity>();
		for(StockSegmentEntity s: seglist){
			segmap.put(s.getId(), s);
		}
		
		List<StockSegmentEntity> segfulllink = new ArrayList<StockSegmentEntity>();
		StockSegmentEntity tmp = segment;
		segfulllink.add(tmp);
		while(StringUtil.isNotEmpty(tmp.getPreid())){
			tmp = segmap.get(tmp.getPreid());
			if( null == tmp ){
				break;
			}
			segfulllink.add(tmp);
		}
		StringBuffer buf = new StringBuffer();
		for( int i = segfulllink.size()-1; i >=0 ; i--  ){
			StockSegmentEntity s = segfulllink.get(i);
			buf.append(s.getName());
			if( i != 0 ){
				buf.append("->");
			}
		}
		productSegmentHistory.setSegmentfullname(buf.toString());
		
		//3、保存进数据库
		productSegmentHistory.setCompany(user.getCompany());
		message = "产品库存历史添加成功(" + productSegmentHistory.getSegmentfullname() + ")";
		productSegmentHistoryService.save(productSegmentHistory);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", productSegmentHistory.getId());
		map.put("segmentid", productSegmentHistory.getSegmentid().getId());
		map.put("segmentfullname", productSegmentHistory.getSegmentfullname());
		
		j.setAttributes(map);
		
		systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			
		j.setMsg(message);
		return j;
	}*/

	/**
	 * 产品库存历史列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(ProductSegmentHistoryEntity productSegmentHistory, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(productSegmentHistory.getId())) {
			productSegmentHistory = productSegmentHistoryService.getEntity(ProductSegmentHistoryEntity.class, productSegmentHistory.getId());
			req.setAttribute("productSegmentHistoryPage", productSegmentHistory);
		}
		return new ModelAndView("admin/stock/productSegmentHistory");
	}
}
