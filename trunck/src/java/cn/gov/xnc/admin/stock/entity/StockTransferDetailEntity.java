package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;

/**   
 * @Title: Entity
 * @Description: 调拨单信息
 * @author zero
 * @date 2018-01-09 09:15:23
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_transfer_detail", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockTransferDetailEntity implements java.io.Serializable {
	/**主键*/
	private java.lang.String id;
	/**调拨商品编码*/
	private ProductEntity productid;
	/**调拨数量*/
	private BigDecimal transfernum;
	/**调拨商品成本单价*/
	private BigDecimal unitprice;
	/**总成本*/
	private BigDecimal totalprice;
	/**备注*/
	private java.lang.String remark;
	/**更新时间*/
	private java.util.Date updatetime;
	/**调拨单位id*/
	private ProductUnitEntity unitid;
	/**调拨存储区域*/
	private StockSegmentEntity fromsegmentid;
	/**调拨目的存储区域*/
	private StockSegmentEntity tosegmentid;
	/**运费*/
	private BigDecimal freightprice;
	/**调拨单*/
	private StockTransferEntity transferid;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	
	@Id
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  调拨商品编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  调拨商品编码
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  调拨数量
	 */
	@Column(name ="TRANSFERNUM",nullable=false,precision=10,scale=2)
	public BigDecimal getTransfernum(){
		return this.transfernum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  调拨数量
	 */
	public void setTransfernum(BigDecimal transfernum){
		this.transfernum = transfernum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  调拨商品成本单价
	 */
	@Column(name ="UNITPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getUnitprice(){
		return this.unitprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  调拨商品成本单价
	 */
	public void setUnitprice(BigDecimal unitprice){
		this.unitprice = unitprice;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  总成本
	 */
	@Column(name ="TOTALPRICE",nullable=false,precision=10,scale=2)
	public BigDecimal getTotalprice(){
		return this.totalprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  总成本
	 */
	public void setTotalprice(BigDecimal totalprice){
		this.totalprice = totalprice;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=4000)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  调拨单编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TRANSFERID")
	public StockTransferEntity getTransferid(){
		return this.transferid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  调拨单编码
	 */
	public void setTransferid(StockTransferEntity transferid){
		this.transferid = transferid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新时间
	 */
	@Column(name ="UPDATETIME",nullable=false)
	public java.util.Date getUpdatetime(){
		return this.updatetime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新时间
	 */
	public void setUpdatetime(java.util.Date updatetime){
		this.updatetime = updatetime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  入库时候的单位id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNITID")
	public ProductUnitEntity getUnitid(){
		return this.unitid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  入库时候的单位id
	 */
	public void setUnitid(ProductUnitEntity unitid){
		this.unitid = unitid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  运费
	 */
	@Column(name ="FREIGHTPRICE",nullable=true,precision=20,scale=0)
	public BigDecimal getFreightprice(){
		return this.freightprice;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  运费
	 */
	public void setFreightprice(BigDecimal freightprice){
		this.freightprice = freightprice;
	}
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FROMSEGMENTID")
	public StockSegmentEntity getFromsegmentid() {
		return fromsegmentid;
	}

	public void setFromsegmentid(StockSegmentEntity fromsegmentid) {
		this.fromsegmentid = fromsegmentid;
	}
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TOSEGMENTID")
	public StockSegmentEntity getTosegmentid() {
		return tosegmentid;
	}

	public void setTosegmentid(StockSegmentEntity tosegmentid) {
		this.tosegmentid = tosegmentid;
	}
}
