package cn.gov.xnc.admin.stock.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockProductPriceEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.admin.stock.service.StockProductPriceServiceI;
import cn.gov.xnc.admin.stock.service.StockProductServiceI;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.ConvertTool;
import cn.gov.xnc.system.core.util.OperationLogUtil;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("stockProductPriceService")
@Transactional(rollbackForClassName={"Exception"})
public class StockProductPriceServiceImpl extends CommonServiceImpl implements StockProductPriceServiceI {
	@Autowired
	private StockProductServiceI stockProductServiceI;
	
	public AjaxJson savePrice(String stockProductId,String stockProductPriceId,BigDecimal avgprice ,HttpServletRequest request){
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson result = new AjaxJson();
		
		//调整当前库存商品的均价、总价
		StockProductEntity stockProduct = getEntity(StockProductEntity.class, stockProductId);
		stockProduct.setAverageprice(avgprice);
		stockProduct.setTotalprice(avgprice.multiply(stockProduct.getStocknum()));
		
		StockProductPriceEntity stockProductPriceEntity = this.updateStockProductPrice(stockProduct.getProductid(), stockProduct.getStockid(),request);
		if(stockProductPriceEntity == null){
			result.setSuccess(false);
			result.setMsg("调价失败");
		}
		
		result.setSuccess(true);
		result.setMsg("调价成功");
		updateEntitie(stockProduct);
		
		return result;
	}
	
	public StockProductPriceEntity updateStockProductPrice(ProductEntity product, StockEntity stock,HttpServletRequest request){
		TSUser user = ResourceUtil.getSessionUserName();
		StockProductPriceEntity stockProductPrice = null;
		try {
			//获取所有库存商品信息均价、总数、总价
			Map<String, Object> stockProductParams = stockProductServiceI.getParams(null,product, stock, null, null,null,null);
			List<StockProductEntity> list = stockProductServiceI.findListByParams(stockProductParams);
			BigDecimal totalPriceALL = new BigDecimal(0.00);
			BigDecimal totalNumALL = new BigDecimal(0.00);
			BigDecimal avgPriceALL = new BigDecimal(0.00);
			if(list != null && list.size() > 0){
				for (StockProductEntity stockProductEntity : list) {
					totalPriceALL = totalPriceALL.add(stockProductEntity.getTotalprice());
					totalNumALL = totalNumALL.add(stockProductEntity.getStocknum());
				}
			}
			avgPriceALL = totalPriceALL.divide(totalNumALL, 4, BigDecimal.ROUND_HALF_UP);
			
			Map<String, Object> stockProductPriceParams = this.getParams(product, stock);
			List<StockProductPriceEntity> stockProductPriceList = this.findListByParams(stockProductPriceParams);
			
			BigDecimal oldAveragePrice = new BigDecimal(0.00);
			
			if( stockProductPriceList == null || stockProductPriceList.size() <= 0 ){
				//保存商品库存总价、均价、数量
				stockProductPrice = new StockProductPriceEntity();
				stockProductPrice.setProductid(product);
				stockProductPrice.setStockid(stock);
				stockProductPrice.setTotalNum(totalNumALL);
				stockProductPrice.setTotalPrice(totalPriceALL);
				stockProductPrice.setAveragePrice(avgPriceALL);
				save(stockProductPrice);
			}else{
				//更新商品库存总价、均价、数量
				stockProductPrice = getEntity(StockProductPriceEntity.class, stockProductPriceList.get(0).getId());
				stockProductPrice.setTotalNum(totalNumALL);
				stockProductPrice.setTotalPrice(totalPriceALL);
				stockProductPrice.setAveragePrice(avgPriceALL);
				updateEntitie(stockProductPrice);
			}
			//记录
			StringBuffer loginfo = new StringBuffer();
			loginfo.append("商品：").append(stockProductPrice.getProductid().getName()).append("(").append(stockProductPrice.getProductid().getCode()).append(") ");
			loginfo.append(" 调整前成本单价：").append(oldAveragePrice).append(" 调整后单价：").append(stockProductPrice.getAveragePrice());
			opLog(request, OperationLogUtil.STOCK_PRODUCT_SETPRICE, stockProductPrice, loginfo.toString());
		} catch (Exception e) {
			return null;
		}
		return stockProductPrice;
	}

	@Override
	public StockProductPriceEntity getStockProductPriceInfo(String productid) throws Exception {
		StockProductPriceEntity stockProductPriceEntity = null;
		if(StringUtil.isNotEmpty(productid)){
			CriteriaQuery cq = new CriteriaQuery(StockProductPriceEntity.class);
			cq.eq("productid.id", productid);
			cq.add();
			
			List<StockProductPriceEntity> stockProductPriceList = getListByCriteriaQuery(cq, false);
			if(null != stockProductPriceList && stockProductPriceList.size() > 0){
				stockProductPriceEntity = stockProductPriceList.get(0);
			}
		}
		return stockProductPriceEntity;
	}

	@Override
	public List<StockProductPriceEntity> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(StockProductPriceEntity.class);
		String productid = (String) params.get("productid");
		if(productid != null){
			cq.eq("productid.id", productid);
		}
		String stockid = (String) params.get("stockid");
		if(stockid != null){
			cq.eq("stockid.id", stockid);
		}
		cq.add();
		List<StockProductPriceEntity> list = getListByCriteriaQuery(cq, false);
		return list;
	}

	@Override
	public Map<String, Object> getParams(ProductEntity productEntity,StockEntity stockEntity) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = new HashMap<String, Object>();
		if(productEntity != null){
			params.put("productid", productEntity.getId());
		}
		if(stockEntity != null){
			params.put("stockid", stockEntity.getId());
		}
		if(user != null){
			params.put("companyid", user.getCompany().getId());
		}
		return params;
	}
	
}