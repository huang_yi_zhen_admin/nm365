package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.product.entity.ProductUnitEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

/**   
 * @Title: Entity
 * @Description: 库存相关联商品表
 * @author zero
 * @date 2017-03-22 00:52:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stock_product", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockProductEntity implements java.io.Serializable {
	/**表关键字*/
	private String id;
	/**商品id*/
	private  ProductEntity productid;
	/**公司编码*/
	private TSCompany companyid;
	/**仓库*/
	private StockEntity stockid;
	/**库存数量*/
	private BigDecimal stocknum;
	/**库存预警值*/
	private BigDecimal alertnum;
	/**库存状态 1 充足 2 缺货 3 补货*/
	private String status;
	/**库存金额*/
	private BigDecimal totalprice;
	/**创建时间*/
	private Date updateTime;
	/**库存均价*/
	private BigDecimal averageprice;
	/**仓库区域*/
	private StockSegmentEntity stockSegment;
	/**预警值下限单位*/
	private ProductUnitEntity alertUnit;
	/**预警值上限单位*/
	private ProductUnitEntity alertUpperUnit;
	/**库存预警值上限*/
	private BigDecimal alertUpperNum;
	/**商品库存总价均价记录表id*/
	private StockProductPriceEntity stockProductPrice;
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  库存商品唯一编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  库存商品唯一编码
	 */
	public void setId(String id){
		this.id = id;
	}
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stockSegmentId")
	public StockSegmentEntity getStockSegment() {
		return stockSegment;
	}
	
	public void setStockSegment(StockSegmentEntity stockSegment) {
		this.stockSegment = stockSegment;
	}
	
	@Column(name ="alertUpperNum",nullable=false,precision=10,scale=2)
	public BigDecimal getAlertUpperNum() {
		return alertUpperNum;
	}

	public void setAlertUpperNum(BigDecimal alertUpperNum) {
		this.alertUpperNum = alertUpperNum;
	}
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "alertUpperUnit")
	public ProductUnitEntity getAlertUpperUnit() {
		return alertUpperUnit;
	}

	public void setAlertUpperUnit(ProductUnitEntity alertUpperUnit) {
		this.alertUpperUnit = alertUpperUnit;
	}
	
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "alertUnit")
	public ProductUnitEntity getAlertUnit() {
		return alertUnit;
	}

	public void setAlertUnit(ProductUnitEntity alertUnit) {
		this.alertUnit = alertUnit;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  商品id
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  公司编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANYID")
	public TSCompany getCompanyid(){
		return this.companyid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  公司编码
	 */
	public void setCompanyid(TSCompany companyid){
		this.companyid = companyid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  仓库
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKID")
	public StockEntity getStockid(){
		return this.stockid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  仓库
	 */
	public void setStockid(StockEntity stockid){
		this.stockid = stockid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  库存数量
	 */
	@Column(name ="STOCKNUM",nullable=false,precision=10,scale=2)
	public BigDecimal getStocknum(){
		return this.stocknum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  库存数量
	 */
	public void setStocknum(BigDecimal stocknum){
		this.stocknum = stocknum;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  库存预警值
	 */
	@Column(name ="ALERTNUM",nullable=false,precision=10,scale=2)
	public BigDecimal getAlertnum(){
		return this.alertnum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  库存预警值
	 */
	public void setAlertnum(BigDecimal alertnum){
		this.alertnum = alertnum;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  库存状态 1 充足 2 缺货 3 补货
	 */
	@Column(name ="STATUS",nullable=false,length=2)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  库存状态 1 充足 2 缺货 3 补货
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}

	/**
	 * @return the totalprice
	 */
	@Column(name ="TOTALPRICE",nullable=false,precision=15,scale=2)
	public BigDecimal getTotalprice() {
		return totalprice;
	}

	/**
	 * @param totalprice the totalprice to set
	 */
	public void setTotalprice(BigDecimal totalprice) {
		this.totalprice = totalprice;
	}

	/**
	 * @return the averageprice
	 */
	@Column(name ="AVERAGEPRICE",nullable=false,precision=15,scale=2)
	public BigDecimal getAverageprice() {
		return averageprice;
	}

	/**
	 * @param averageprice the averageprice to set
	 */
	public void setAverageprice(BigDecimal averageprice) {
		this.averageprice = averageprice;
	}
	
	/**
	 * @return the stockproductpriceid
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stockProductPriceId")
	public StockProductPriceEntity getStockProductPrice() {
		return stockProductPrice;
	}

	/**
	 * @param stockproductpriceid the stockproductpriceid to set
	 */
	public void setStockProductPrice(StockProductPriceEntity stockProductPrice) {
		this.stockProductPrice = stockProductPrice;
	}
	
	/**
	 * @return the update_time
	 */
	@Column(name ="updateTime",nullable=false)
	public java.util.Date getUpdate_time() {
		return updateTime;
	}

	/**
	 * @param update_time the update_time to set
	 */
	public void setUpdate_time(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
