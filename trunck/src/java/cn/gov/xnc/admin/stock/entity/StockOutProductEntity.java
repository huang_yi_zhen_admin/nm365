package cn.gov.xnc.admin.stock.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 产品出库详细详细信息
 * @author zero
 * @date 2017-01-23 15:08:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_company_stockout_product", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class StockOutProductEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**出库产品编码和入库产品编码一致*/
	private StockInProductEntity stockproductid;
	/**出库数量*/
	private BigDecimal stockoutnum;
	/**备注*/
	private java.lang.String remark;
	/**仓库编码*/
	private StockIOEntity stockorderid;
	/**出库总价*/
	private BigDecimal totalprice;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  出库产品编码和入库产品编码一致
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKPRODUCTID")
	public StockInProductEntity getStockproductid(){
		return this.stockproductid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  出库产品编码和入库产品编码一致
	 */
	public void setStockproductid(StockInProductEntity stockproductid){
		this.stockproductid = stockproductid;
	}
	/**
	 *方法: 取得BigDecimal
	 *@return: BigDecimal  出库数量
	 */
	@Column(name ="STOCKOUTNUM",nullable=false,precision=10,scale=0)
	public BigDecimal getStockoutnum(){
		return this.stockoutnum;
	}

	/**
	 *方法: 设置BigDecimal
	 *@param: BigDecimal  出库数量
	 */
	public void setStockoutnum(BigDecimal stockoutnum){
		this.stockoutnum = stockoutnum;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARK",nullable=true,length=4000)
	public java.lang.String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemark(java.lang.String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  出库单编码
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKORDERID")
	public StockIOEntity getStockorderid(){
		return this.stockorderid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  出库单编码
	 */
	public void setStockorderid(StockIOEntity stockid){
		this.stockorderid = stockid;
	}

	public BigDecimal getTotalprice() {
		return totalprice;
	}

	public void setTotalprice(BigDecimal totalprice) {
		this.totalprice = totalprice;
	}
}
