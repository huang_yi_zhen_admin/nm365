package cn.gov.xnc.admin.stock.service;

public interface StockIOType {
	
	public static final String OUT = "DIC_TYPE_STOCK_OUT"; //出库在字典中配置的类型名称
	
	public static final String IN = "DIC_TYPE_STOCK_IN";//入库在字典中配置的类型名称

	public enum StockOutType {

		SALE_OUT("销售出库", "STOCK_OUT_SALE"), 
		WASTAGE_OUT("损耗出库", "STOCK_OUT_WASTAGE"), 
		EXCHANGE_OUT("进换货出库","STOCK_OUT_EXCHANGE"), 
		AJUST_OUT("调整出库", "STOCK_OUT_AJUST"),
		CONVERT_OUT("转换出库", "STOCK_OUT_CONVERT"),
		CHECK_LOSS_OUT("盘亏出库", "STOCK_OUT_CHECK_LOSS"),
		TRANSFER_OUT("调拨出库", "STOCK_OUT_TRANSFER"),
		GIFT_OUT("礼品出库", "STOCK_OUT_GIFT"),
		OTHER_OPERATION_OUT("其它运营出库", "STOCK_OUT_OTHER_OPERATION"),
		PACK_STUFF_OUT("包材等物料出库", "STOCK_IN_PACK_STUFF");

		private String name;
		private String value;

		private StockOutType(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String index) {
			this.value = index;
		}

	}
	
	
	public enum StockInType {

		PURCHASE_IN("采购入库", "STOCK_IN_PURCHASE"), 
		PACK_STUFF_IN("包材等物料入库", "STOCK_IN_PACK_STUFF"), 
		EXCHANGE_IN("进换货入库","STOCK_IN_EXCHANGE"), 
		AJUST_IN("调整入库", "STOCK_IN_AJUST"),
		TRANSFER_IN("调拨入库", "STOCK_IN_TRANSFER"),
		GIFT_IN("礼品入库", "STOCK_IN_GIFT"),
		PICK_IN("采摘入库", "STOCK_IN_PICK"),
		PRESELL_IN("预售入库", "STOCK_IN_PRESELL"),
		CONVERT_IN("转换入库", "STOCK_IN_CONVERT"),
		SALE_IN("销售商品入库", "STOCK_IN_SALE"),
		OTHER_OPERATION_IN("其它运营入库", "STOCK_IN_OTHER_OPERATION");

		private String name;
		private String value;

		private StockInType(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String index) {
			this.value = index;
		}

	}
}
