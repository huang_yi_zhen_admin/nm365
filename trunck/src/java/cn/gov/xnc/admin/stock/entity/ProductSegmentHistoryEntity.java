package cn.gov.xnc.admin.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;

/**   
 * @Title: Entity
 * @Description: 产品库存历史
 * @author zero
 * @date 2017-04-20 14:41:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "xnc_stock_product_segment_history", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class ProductSegmentHistoryEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**产品id*/
	private ProductEntity productid;
	/**区域id*/
	private StockSegmentEntity segmentid;
	/**updateTime*/
	private java.util.Date updateTime;
	/**公司id*/
	private TSCompany company;
	
	/**仓储位置全名称拼接：A1仓库->2区->3层 格式*/
	private String segmentfullname;
	
	/**stockid仓库id*/
	private StockEntity stockid;
	
	
	/**
	 * @return the segmentfullname
	 */
	@Column(name ="SEGMENTFULLNAME",nullable=true,length=4000)
	public String getSegmentfullname() {
		return segmentfullname;
	}

	/**
	 * @param segmentfullname the segmentfullname to set
	 */
	public void setSegmentfullname(String segmentfullname) {
		this.segmentfullname = segmentfullname;
	}

	/**
	 * @return the stockid
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STOCKID")
	public StockEntity getStockid() {
		return stockid;
	}

	/**
	 * @param stockid the stockid to set
	 */
	public void setStockid(StockEntity stockid) {
		this.stockid = stockid;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  产品id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTID")
	public ProductEntity getProductid(){
		return this.productid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  产品id
	 */
	public void setProductid(ProductEntity productid){
		this.productid = productid;
	}
	/**
	 *方法: 取得StockSegmentEntity
	 *@return: StockSegmentEntity  区域id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SEGMENTID")
	public StockSegmentEntity getSegmentid(){
		return this.segmentid;
	}

	/**
	 *方法: 设置StockSegmentEntity
	 *@param: StockSegmentEntity  区域id
	 */
	public void setSegmentid(StockSegmentEntity segmentid){
		this.segmentid = segmentid;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  updateTime
	 */
	@Column(name ="UPDATE_TIME",nullable=false)
	public java.util.Date getUpdateTime(){
		return this.updateTime;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime){
		this.updateTime = updateTime;
	}

	/**
	 * @return the company
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY")
	public TSCompany getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
}
