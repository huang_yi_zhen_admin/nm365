package cn.gov.xnc.admin.stock.controller;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserRoleService;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockProductEntity;
import cn.gov.xnc.admin.stock.entity.StockSupplierEntity;
import cn.gov.xnc.admin.stock.service.StockIODetailServiceI;

/**   
 * @Title: Controller
 * @Description: 出库详细单
 * @author zero
 * @date 2017-03-22 14:40:07
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/stockIODetailController")
public class StockIODetailController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StockIODetailController.class);

	@Autowired
	private StockIODetailServiceI stockIODetailService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserRoleService userRoleService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 出库详细单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockIODetailList")
	public ModelAndView stockIODetailList(HttpServletRequest request) {
		TSUser user = ResourceUtil.getSessionUserName();
		//获取仓库
		CriteriaQuery cq = new CriteriaQuery(StockEntity.class);
		cq.eq("companyid", user.getCompany());
		cq.notEq("status", 1);
		cq.add();
		List<StockEntity> stockList = systemService.getListByCriteriaQuery(cq,false);
		request.setAttribute("stocklist", stockList);
		
		//读取供应商列表
		CriteriaQuery cr = new CriteriaQuery(StockSupplierEntity.class);
		cr.eq("company", user.getCompany());
		cr.eq("status", 0);
		cr.addOrder("code", SortDirection.asc);
		cr.add();
		List<StockSupplierEntity> supplierList = systemService.getListByCriteriaQuery(cr, false);
		request.setAttribute("supplierlist", supplierList);
		
		return new ModelAndView("admin/stock/stockIODetailList");
	}
	
	
	/**
	 * 出库详细单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockIODetailInfos")
	public ModelAndView stockInDetailInfos(HttpServletRequest request) {
		String stockioid = request.getParameter("id");
		StockIOEntity stockInEntity = new StockIOEntity();
		if(StringUtil.isNotEmpty(stockioid)){
			stockInEntity = stockIODetailService.findUniqueByProperty(StockIOEntity.class, "id", stockioid);
		}
		request.setAttribute("stockInEntity", stockInEntity);
		request.setAttribute("type", request.getParameter("type"));
		
		return new ModelAndView("admin/stock/stockIODetailInfos");
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(StockIODetailEntity stockIOOrderDetail,String ordername, String orderdirection, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
//		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class, dataGrid);
//		//查询条件组装器
//		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, stockIOOrderDetail, request.getParameterMap());
//		this.stockIODetailService.getDataGridReturn(cq, true);
//		TagUtil.datagrid(response, dataGrid);
		
		TSUser user = ResourceUtil.getSessionUserName();
		//根据用户权限获取相关权限
		TSRole role = userRoleService.getRoleByUserRole(user);
		
		CriteriaQuery cq = getDatagridCriteriaQuery(stockIOOrderDetail, ordername, orderdirection, request, response, dataGrid);
		
		//如果不是管理员类型，添加仓库负责人条件
		if(!role.getRoleCode().equals("admin")){
			cq.createAlias("stockorderid.stockid", "stock");
			cq.createAlias("stock.dutyperson", "stockUser");
			cq.eq("stockUser.id", user.getId());
		}
		
		//查询条件组装器
		HqlGenerateUtil.installHql(cq, stockIOOrderDetail, request.getParameterMap());
		
		this.stockIODetailService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	private CriteriaQuery getDatagridCriteriaQuery(StockIODetailEntity stockIOOrderDetail, String ordername, String orderdirection, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid){
		TSUser user = ResourceUtil.getSessionUserName();
		CriteriaQuery cq = new CriteriaQuery(StockIODetailEntity.class, dataGrid);
		if( null!= stockIOOrderDetail.getProductid() ){
			stockIOOrderDetail.getProductid().setIsDelete(null);
			stockIOOrderDetail.getProductid().setNotsale(null);
			if( StringUtil.isNotEmpty(stockIOOrderDetail.getProductid().getName()) ){
				cq.createAlias("productid", "product");
				cq.add(Restrictions.like("product.name", "%"+stockIOOrderDetail.getProductid().getName()+"%"));
				stockIOOrderDetail.getProductid().setName(null);
			}
		}
		
		if( null != stockIOOrderDetail.getSupplierid() && StringUtil.isNotEmpty(stockIOOrderDetail.getSupplierid().getName()) ){
			cq.createAlias("supplierid", "supplier");
			cq.add(Restrictions.like("supplier.name", "%"+stockIOOrderDetail.getSupplierid().getName()+"%"));
			stockIOOrderDetail.getSupplierid().setName(null);
		}else{
			stockIOOrderDetail.setSupplierid(null);
		}

		
		
		if( null!=stockIOOrderDetail.getStockorderid() ){
			if( null!=stockIOOrderDetail.getStockorderid().getOperator() && StringUtil.isNotEmpty(stockIOOrderDetail.getStockorderid().getOperator().getRealname()) ){
				cq.createAlias("stockorderid.operator", "oper");
				cq.add(Restrictions.like("oper.realname", "%"+stockIOOrderDetail.getStockorderid().getOperator().getRealname()+"%"));
				stockIOOrderDetail.getStockorderid().getOperator().setRealname(null);
			}
			if( null != stockIOOrderDetail.getStockorderid().getStocktypeid() && StringUtil.isNotEmpty(stockIOOrderDetail.getStockorderid().getStocktypeid().getDictionaryType() )){
				cq.createAlias("stockorderid.stocktypeid", "stocktype");
				cq.add(Restrictions.eq("stocktype.dictionaryType",stockIOOrderDetail.getStockorderid().getStocktypeid().getDictionaryType()));
				stockIOOrderDetail.getStockorderid().getStocktypeid().setDictionaryType(null);
			}
			
			stockIOOrderDetail.getStockorderid().setCompanyid(user.getCompany());
			cq.createAlias("stockorderid.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
		}else{
			stockIOOrderDetail.setStockorderid(new StockIOEntity());
			stockIOOrderDetail.getStockorderid().setCompanyid(user.getCompany());
			cq.createAlias("stockorderid.companyid", "com");
			cq.add(Restrictions.eq("com.id", user.getCompany().getId()));
		}
		
		
		
		//时间
		String createdate1 = request.getParameter("createdate1");
		String createdate2 = request.getParameter("createdate2");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(createdate1)&&StringUtil.isNotEmpty(createdate2)){
			cq.between("updatetime", DateUtils.str2Date(createdate1,sdf), DateUtils.str2Date(createdate2,sdf));
		}else if( StringUtil.isNotEmpty(createdate1) ){
			cq.ge("updatetime", DateUtils.str2Date(createdate1,sdf));
		}else if( StringUtil.isNotEmpty(createdate2) ){
			cq.le("updatetime", DateUtils.str2Date(createdate2,sdf));
		}
		//cq.addOrder("updatetime", SortDirection.desc);
		
		if( StringUtil.isNotEmpty(ordername) ){
			if( "asc".equals(orderdirection) ){
				cq.addOrder(ordername, SortDirection.asc);
			}else{
				cq.addOrder(ordername, SortDirection.desc);
			}
		}else{
			cq.addOrder("updatetime", SortDirection.desc);
		}
		
		return cq;
	}
	
	/**
	 * 添加出库详细单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getLastStockInPrice")
	@ResponseBody
	public AjaxJson getLastStockInPrice(StockIODetailEntity stockIOOrderDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String,String> obj = new HashMap<String,String>();
		
		StockIODetailEntity siode = stockIODetailService.getLastStockInDetail(stockIOOrderDetail.getProductid().getId());
		
		if( null != siode  ){
			obj.put("unitprice", siode.getUnitprice().toPlainString());
			obj.put("unitname", siode.getProductid().getUnit().getName());
			obj.put("unitid", siode.getProductid().getUnit().getId());
			j.setSuccess(true);
			j.setMsg("获取数据成功");
		}else{
			j.setSuccess(false);
			j.setMsg("无数据");
		}
		
		
		
		j.setObj(obj);

		return j;
	}
	
	/**
	 * 出入库详情查询 - 总价合计: x元
	 * @param stockIOOrderDetail
	 * @param ordername
	 * @param orderdirection
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @return
	 */
	@RequestMapping(value = "datagridStatis")
	@ResponseBody
	public AjaxJson datagridStatis(StockIODetailEntity stockIOOrderDetail, String startTime,String endTime,
			HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		TSUser user = ResourceUtil.getSessionUserName();
		AjaxJson j = new AjaxJson();
		
		//根据用户权限获取相关权限
		TSRole role = userRoleService.getRoleByUserRole(user);
		
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("companyid", user.getCompany().getId());
		params.put("roleCode", role.getRoleCode());
		params.put("userId", user.getId());
		
		if(stockIOOrderDetail.getProductid()!=null){
			params.put("productName", stockIOOrderDetail.getProductid().getName());
		}
		if(stockIOOrderDetail.getStockorderid() != null){
			if(stockIOOrderDetail.getStockorderid().getStocktypeid()!=null){
				params.put("typeCode", stockIOOrderDetail.getStockorderid().getStocktypeid().getDictionaryType());
			}
		}
		if(stockIOOrderDetail.getSupplierid() != null){
			params.put("supplieridName", stockIOOrderDetail.getSupplierid().getName());
		}
		if(stockIOOrderDetail.getStockorderid() != null){
			if(stockIOOrderDetail.getStockorderid().getId() != null){
				params.put("stockIOOrderId", stockIOOrderDetail.getStockorderid().getId());
			}
			if(stockIOOrderDetail.getStockorderid().getOperator() != null){
				params.put("realname", stockIOOrderDetail.getStockorderid().getOperator().getUserName());
			}
		}
		if(startTime!= null && !startTime.isEmpty()){
			params.put("startTime", startTime);
		}
		if(endTime!=null && !endTime.isEmpty()){
			params.put("endTime", endTime);
		}
		
		Map<String, Object> result = stockIODetailService.statisCount(params);
		
		if(result == null){
			j.setSuccess(false);
			j.setMsg("查询失败");
		}
		
		j.setObj(result);
		j.setSuccess(true);
		j.setMsg("查询成功");
		
		return j;
	}
	
	/**
	 * 出库详细单列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "stockIODetailInfosByDetailId")
	public ModelAndView stockIODetailInfosByDetailId(HttpServletRequest request) {
		
		String stockiodetailid = request.getParameter("id");
		StockIOEntity stockInEntity = new StockIOEntity();
		if( StringUtil.isNotEmpty(stockiodetailid) ){
			StockIODetailEntity stockIODetail = stockIODetailService.getEntity(StockIODetailEntity.class, stockiodetailid);
			if( null != stockIODetail ){
				stockInEntity = stockIODetailService.findUniqueByProperty(StockIOEntity.class, "id", stockIODetail.getStockorderid().getId());
			}
		}
		
		request.setAttribute("stockInEntity", stockInEntity);
		request.setAttribute("type", request.getParameter("type"));
		
		return new ModelAndView("admin/stock/stockIODetailInfos");

	}

	/**
	 * 删除出库详细单
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(StockIODetailEntity stockIOOrderDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		stockIOOrderDetail = systemService.getEntity(StockIODetailEntity.class, stockIOOrderDetail.getId());
		message = "出库详细单删除成功";
		stockIODetailService.delete(stockIOOrderDetail);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加出库详细单
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(StockIODetailEntity stockIOOrderDetail, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(stockIOOrderDetail.getId())) {
			message = "出库详细单更新成功";
			StockIODetailEntity t = stockIODetailService.get(StockIODetailEntity.class, stockIOOrderDetail.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(stockIOOrderDetail, t);
				stockIODetailService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "出库详细单更新失败";
			}
		} else {
			message = "出库详细单添加成功";
			stockIODetailService.save(stockIOOrderDetail);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 出库详细单列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(StockIODetailEntity stockIOOrderDetail, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(stockIOOrderDetail.getId())) {
			stockIOOrderDetail = stockIODetailService.getEntity(StockIODetailEntity.class, stockIOOrderDetail.getId());
			req.setAttribute("stockIOOrderDetailPage", stockIOOrderDetail);
		}
		return new ModelAndView("admin/stock/stockIOOrderDetail");
	}
}
