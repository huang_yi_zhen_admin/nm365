package cn.gov.xnc.admin.stock.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.service.SystemService;

public interface StockSegmentServiceI extends CommonService{
	
	public String segmentListId2SqlStr(List<StockSegmentEntity> list);
	
	public List<StockSegmentEntity> getSonSegment(Map<String, List<StockSegmentEntity>> stockseginfo, StockSegmentEntity stockSegment);
	
	public List<StockSegmentEntity> getAllSonsSegments(StockSegmentEntity stockSegment,SystemService systemService);
	
	public JSONObject Segments2JsonObj(Map<String, List<StockSegmentEntity>> segmeninfo,StockSegmentEntity stockSegmentEntity,Map<String, StockEntity> allstockmap);
	
	public JSONArray StockSegment2Json(List<Map<String, List<StockSegmentEntity>>> allstockseginfolist,Map<String, StockEntity> allstockmap );
	
	public AjaxJson del(StockSegmentEntity stockSegment, HttpServletRequest request,SystemService systemService,StockSegmentServiceI stockSegmentService);
	
	public AjaxJson getAllStockInfo( HttpServletRequest request, SystemService systemService);
	
	public AjaxJson save(StockSegmentEntity stockSegment, HttpServletRequest request,SystemService systemService,StockSegmentServiceI stockSegmentService);

}
