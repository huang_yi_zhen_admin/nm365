package cn.gov.xnc.admin.stock.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.admin.stock.entity.StockEntity;
import cn.gov.xnc.admin.stock.entity.StockIODetailEntity;
import cn.gov.xnc.admin.stock.entity.StockIOEntity;
import cn.gov.xnc.admin.stock.entity.StockSegmentEntity;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.service.SystemService;

public interface StockIOServiceI extends CommonService{

	public enum StockType {

		STOCK_IN("入库", ""), STOCK_OUT("出库", "2");

		private String name;
		private String value;

		private StockType(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String index) {
			this.value = index;
		}

	}
	public StockEntity buildStockOrder();
	
	/**
	 * 保存入库信息
	 * @param companyStockOrder
	 * @param request
	 * @param stockIODetailService
	 * @param stockProductService
	 * @param systemService
	 * @return
	 */
//	public AjaxJson saveInStock(StockIOEntity companyStockOrder, HttpServletRequest request,StockIODetailServiceI stockIODetailService,StockProductServiceI stockProductService,SystemService systemService);

	/**
	 * 保存出库信息
	 * @param companyStockOrder
	 * @param request
	 * @param stockIODetailService
	 * @param stockProductService
	 * @param systemService
	 * @return
	 */
//	public AjaxJson saveOutStock(StockIOEntity companyStockOrder, HttpServletRequest request,StockIODetailServiceI stockIODetailService,StockProductServiceI stockProductService,SystemService systemService);

	/**
	 * 通过页面传入的请求对象，保存出/入库信息
	 * @param stockIOEntity
	 * @return
	 */
	public AjaxJson saveStockIO(StockIOEntity stockIOEntity, HttpServletRequest request);
	
	/**
	 * 根据提供好的出/入库单和出/入库详细保存库存信息
	 * @param stockIOEntity
	 * @param StockIODetailList
	 * @param req
	 * @return
	 */
	public AjaxJson saveStockIO(StockIOEntity stockIOEntity, List<StockIODetailEntity> StockIODetailList, HttpServletRequest req);
	
	public List<StockIOEntity> findListByParams(Map<String, Object> params);
	
	public Map<String, Object> getParams(ProductEntity product,StockEntity stock);
}
