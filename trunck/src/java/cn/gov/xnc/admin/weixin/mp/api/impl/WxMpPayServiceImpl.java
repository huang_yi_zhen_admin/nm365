package cn.gov.xnc.admin.weixin.mp.api.impl;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import cn.gov.xnc.admin.weixin.mp.api.WxMpPayService;
import cn.gov.xnc.admin.weixin.mp.api.WxMpService;
import cn.gov.xnc.admin.weixin.mp.entity.pay.WxPayDto;
import cn.gov.xnc.admin.weixin.mp.entity.pay.WxPayResult;
import cn.gov.xnc.admin.weixin.mp.utils.GetWxOrderno;
import cn.gov.xnc.admin.weixin.mp.utils.RequestHandler;
import cn.gov.xnc.admin.weixin.mp.utils.Sha1Util;
import cn.gov.xnc.admin.weixin.mp.utils.TenpayUtil;
import cn.gov.xnc.system.core.constant.Globals;



/**
 * 
 * 
 */
@Service("wxMpPayService")
public class WxMpPayServiceImpl implements WxMpPayService{
	

	/**
	 * 获取微信扫码支付二维码连接
	 */
	public  String getCodeurl(WxPayDto tpWxPayDto  ){

		String appid = tpWxPayDto.getAppid();//微信appid
		String appsecret = tpWxPayDto.getAppsecret();//微信服务号秘钥
		String partner = tpWxPayDto.getMchId();//微信支付分配的商户号
		String partnerkey = tpWxPayDto.getPartnerkey();//这个参数partnerkey是在商户后台配置的一个32位的key,微信商户平台-账户设置-安全设置-api安全

		// 订单号
		String orderId = tpWxPayDto.getProductId();
		// 附加数据 原样返回
		String attach = tpWxPayDto.getAttach();
		// 总金额以分为单位，不带小数点
		String totalFee = String.valueOf(tpWxPayDto.getTotalFee());
		// 订单生成的机器 IP
		String spbill_create_ip = tpWxPayDto.getSpbillCreateIp();
		// 这里notify_url是 支付完成后微信发给该链接信息，可以判断会员是否支付成功，改变订单状态等。
		String notify_url = tpWxPayDto.getNotifyURL();
		String trade_type = "NATIVE";
		// 商户号
		String mch_id = partner;
		// 随机字符串
		String nonce_str = getNonceStr();
		// 商品描述根据情况修改
		String body = tpWxPayDto.getBody();
		// 商户订单号
		String out_trade_no = orderId;
		

		SortedMap<String, String> packageParams = new TreeMap<String, String>();
			packageParams.put("appid", tpWxPayDto.getAppid());
			packageParams.put("mch_id", mch_id);
			packageParams.put("nonce_str", nonce_str);
			packageParams.put("body", body);
			packageParams.put("attach", "");
			packageParams.put("out_trade_no", out_trade_no);
			// 这里写的金额为1 分到时修改
			packageParams.put("total_fee", totalFee);
			packageParams.put("spbill_create_ip", spbill_create_ip);
			packageParams.put("notify_url", notify_url);
			packageParams.put("trade_type", trade_type);
			RequestHandler reqHandler = new RequestHandler(null, null);
			reqHandler.init(appid, appsecret, partnerkey);
			String sign = reqHandler.createSign(packageParams);
			String xml = "<xml>" + "<appid>" + appid + "</appid>" + "<mch_id>"
					+ mch_id + "</mch_id>" + "<nonce_str>" + nonce_str
					+ "</nonce_str>" + "<sign>" + sign + "</sign>"
					+ "<body><![CDATA[" + body + "]]></body>" 
					+ "<out_trade_no>" + out_trade_no
					+ "</out_trade_no>" + "<attach>" + attach + "</attach>"
					+ "<total_fee>" + totalFee + "</total_fee>"
					+ "<spbill_create_ip>" + spbill_create_ip
					+ "</spbill_create_ip>" + "<notify_url>" + notify_url
					+ "</notify_url>" + "<trade_type>" + trade_type
					+ "</trade_type>" + "</xml>";
			
			String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
			String code_url = new GetWxOrderno().getCodeUrl(createOrderURL, xml);
			
			return code_url;
	}
	

	/**
	 * 获取请求预支付id报文
	 * @return
	 */
	public SortedMap<String, String> getPackageJsApi(WxPayDto tpWxPayDto) {
		
		
		
		String appid = tpWxPayDto.getAppid();//微信appid
		String appsecret = tpWxPayDto.getAppsecret();//微信服务号秘钥
		String partner = tpWxPayDto.getMchId();//微信支付分配的商户号
		String partnerkey = tpWxPayDto.getPartnerkey();//这个参数partnerkey是在商户后台配置的一个32位的key,微信商户平台-账户设置-安全设置-api安全
		String openId = tpWxPayDto.getOpenid();//openId 是微信用户针对公众号的标识，授权的部分这里不解释
		
		
		String orderId = tpWxPayDto.getProductId();
		// 附加数据 原样返回
		String attach = tpWxPayDto.getAttach();
		// 总金额以分为单位，不带小数点
		String totalFee = String.valueOf(tpWxPayDto.getTotalFee());
		// 订单生成的机器 IP
		String spbill_create_ip = tpWxPayDto.getSpbillCreateIp();
		// 这里notify_url是 支付完成后微信发给该链接信息，可以判断会员是否支付成功，改变订单状态等。
		String notify_url = tpWxPayDto.getNotifyURL();
		String trade_type = "JSAPI";
		// 商户号
		String mch_id = partner;
		// 随机字符串
		String nonce_str = getNonceStr();
		// 商品描述根据情况修改
		String body = tpWxPayDto.getBody();
		// 商户订单号
		String out_trade_no = orderId;

		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", appid);
		packageParams.put("mch_id", mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("body", body);
		packageParams.put("attach", attach);
		packageParams.put("out_trade_no", out_trade_no);

		// 这里写的金额为1 分到时修改
		packageParams.put("total_fee", totalFee);
		packageParams.put("spbill_create_ip", spbill_create_ip);
		packageParams.put("notify_url", notify_url);
		packageParams.put("trade_type", trade_type);
		packageParams.put("openid", openId);

		
		RequestHandler reqHandler = new RequestHandler(null, null);
		reqHandler.init(appid, appsecret, partnerkey);
		String sign = reqHandler.createSign(packageParams);
		String xml = "<xml>" + "<appid>" + appid + "</appid>" + "<mch_id>"
				+ mch_id + "</mch_id>" + "<nonce_str>" + nonce_str
				+ "</nonce_str>" + "<sign>" + sign + "</sign>"
				+ "<body><![CDATA[" + body + "]]></body>" 
				+ "<out_trade_no>" + out_trade_no
				+ "</out_trade_no>" + "<attach>" + attach + "</attach>"
				+ "<total_fee>" + totalFee + "</total_fee>"
				+ "<spbill_create_ip>" + spbill_create_ip
				+ "</spbill_create_ip>" + "<notify_url>" + notify_url
				+ "</notify_url>" + "<trade_type>" + trade_type
				+ "</trade_type>" + "<openid>" + openId + "</openid>"
				+ "</xml>";
		
		String prepay_id = "";
		String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

		prepay_id = new GetWxOrderno().getPayNo(createOrderURL, xml);
		//获取prepay_id后，拼接最后请求支付所需要的package
		SortedMap<String, String> finalpackage = new TreeMap<String, String>();
		String timestamp = Sha1Util.getTimeStamp();
		String packages = "prepay_id="+prepay_id;
		finalpackage.put("appId", appid);  
		finalpackage.put("timeStamp", timestamp);  
		finalpackage.put("nonceStr", nonce_str);  
		finalpackage.put("package", packages);  
		finalpackage.put("signType", "MD5");
		//要签名
		
		String signature ="";
		try{
			 //创建支付初始化的jsapi_ticket签名信息
			 WxMpService wxMpService = new WxMpServiceImpl();
			 String accessToken= wxMpService.getAccessToken( tpWxPayDto.getAppid(), tpWxPayDto.getAppsecret()) ;
			 String ticket= wxMpService.getJsapiTicket(accessToken) ;
			 
			String signValue = "jsapi_ticket="+ticket+"&noncestr="+nonce_str+"&timestamp="+timestamp+"&url="+notify_url;
			signature= Sha1Util.getSha1((signValue));
		}catch (Exception e) {
			
		}
		String finalsign = reqHandler.createSign(finalpackage); //signature
		
		finalpackage.put("paySign", finalsign);
		finalpackage.put("signature", signature);
		
		/*String finaPackage = "{\"appId\":\"" + appid + "\",\"timeStamp\":\"" + timestamp
							+ "\",\"nonceStr\":\"" + nonce_str + "\",\"package\":\""
							+ packages + "\",\"signType\" : \"MD5" + "\",\"paySign\":\""
							+ finalsign + "\",\"signature\":\""+signature+"\"}";*/
		return finalpackage;
	}
	
	
	public  WxPayResult getwxPayResult(String notityXml ) throws Exception {

		Map m = parseXmlToList2(notityXml);
		WxPayResult wpr = new WxPayResult();
		wpr.setAppid(m.get("appid").toString());
		wpr.setBankType(m.get("bank_type").toString());
		wpr.setCashFee(m.get("cash_fee").toString());
		wpr.setFeeType(m.get("fee_type").toString());
		wpr.setIsSubscribe(m.get("is_subscribe").toString());
		wpr.setMchId(m.get("mch_id").toString());
		wpr.setNonceStr(m.get("nonce_str").toString());
		wpr.setOpenid(m.get("openid").toString());
		wpr.setOutTradeNo(m.get("out_trade_no").toString());
		wpr.setResultCode(m.get("result_code").toString());
		wpr.setReturnCode(m.get("return_code").toString());
		wpr.setSign(m.get("sign").toString());
		wpr.setTimeEnd(m.get("time_end").toString());
		wpr.setTotalFee(m.get("total_fee").toString());
		wpr.setTradeType(m.get("trade_type").toString());
		wpr.setTransactionId(m.get("transaction_id").toString());
		
//		if("SUCCESS".equals(wpr.getResultCode())){
//			//支付成功
//			resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
//			+ "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
//		}else{
//			resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
//			+ "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
//		}

		return wpr;

	}
	
	/**
	 * description: 解析微信通知xml
	 * 
	 * @param xml
	 * @return
	 * @author ex_yangxiaoyi
	 * @see
	 */
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	private static Map parseXmlToList2(String xml) {
		Map retMap = new HashMap();
		try {
			StringReader read = new StringReader(xml);
			// 创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入
			InputSource source = new InputSource(read);
			// 创建一个新的SAXBuilder
			SAXBuilder sb = new SAXBuilder();
			// 通过输入源构造一个Document
			Document doc = (Document) sb.build(source);
			Element root = doc.getRootElement();// 指向根节点
			List<Element> es = root.getChildren();
			if (es != null && es.size() != 0) {
				for (Element element : es) {
					retMap.put(element.getName(), element.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retMap;
	}

	
	/**
	 * 获取随机字符串
	 * @return
	 */
	private static String getNonceStr() {
		// 随机数
		String currTime = TenpayUtil.getCurrTime();
		// 8位日期
		String strTime = currTime.substring(8, currTime.length());
		// 四位随机数
		String strRandom = TenpayUtil.buildRandom(4) + "";
		// 10位序列号,可以自行调整。
		return strTime + strRandom;
	}

	
	
}
