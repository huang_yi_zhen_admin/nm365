package cn.gov.xnc.admin.weixin.mp.utils.josn;

import java.lang.reflect.Type;



import cn.gov.xnc.admin.weixin.mp.entity.common.JsapiTicket;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class JsapiTicketAdapter implements JsonDeserializer<JsapiTicket> {

	  @Override
	  public JsapiTicket deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		  JsapiTicket jsapiTicket = new JsapiTicket();
	    JsonObject accessTokenJsonObject = json.getAsJsonObject();

	    if (accessTokenJsonObject.get("errcode") != null && !accessTokenJsonObject.get("errcode").isJsonNull()) {
	    	jsapiTicket.setErrcode(GsonHelper.getAsString(accessTokenJsonObject.get("errcode")));
	    }
	    if (accessTokenJsonObject.get("errmsg") != null && !accessTokenJsonObject.get("errmsg").isJsonNull()) {
	    	jsapiTicket.setErrmsg(GsonHelper.getAsString(accessTokenJsonObject.get("errmsg")));
	    }
	    if (accessTokenJsonObject.get("ticket") != null && !accessTokenJsonObject.get("ticket").isJsonNull()) {
	    	jsapiTicket.setTicket(GsonHelper.getAsString(accessTokenJsonObject.get("ticket")));
	    }
	    if (accessTokenJsonObject.get("expires_in") != null && !accessTokenJsonObject.get("expires_in").isJsonNull()) {
	    	jsapiTicket.setExpiresIn(GsonHelper.getAsPrimitiveInt(accessTokenJsonObject.get("expires_in")));
		  }
	    
	    return jsapiTicket;
	  }
}
