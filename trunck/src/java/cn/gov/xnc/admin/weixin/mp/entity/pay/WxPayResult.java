
package cn.gov.xnc.admin.weixin.mp.entity.pay;

/**
 * <pre>
 * 在发起微信支付前，需要调用统一下单接口，获取"预支付交易会话标识"返回的结果
 * 统一下单(详见http://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_1)
 * </pre>
 *
 * @author 
 */
public class WxPayResult {

	 /**返回状态码  是 	String(16) 	SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断*/
	  private String returnCode;
	  /**返回信息 	否 	String(128) 签名失败 	返回信息，如非空，为错误原因签名失败参数格式校验错误*/
	  private String returnMsg;
	  //=====以下字段在return_code为SUCCESS的时候有返回 ======
	  /**公众账号ID  是 	String(32) 	调用接口提交的公众账号ID */
	  private String appid;
	  /**商户号  是 	String(32) 调用接口提交的商户号*/
	  private String mchId;
	  /**随机字符串   是 	String(32) 微信返回的随机字符串*/
	  private String nonceStr;
	  /**签名  是 	String(32) 微信返回的签名，详见签名算法*/
	  private String sign;
	  /**业务结果 	 是 	String(16) SUCCESS/FAIL */
	  private String resultCode;
	  /**错误代码 	err_code */
	  private String errCode;
	  /**错误代码描述 	err_code_des */
	  private String errCodeDes;
	  //=========以下字段在return_code 和result_code都为SUCCESS的时候有返回 ============
	  /**交易类型 	是 	String(16)  	调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP */
	  private String tradeType;
	  /**预支付交易会话标识  是 	String(64) 微信生成的预支付回话标识，用于后续接口调用中使用，该值有效期为2小时 */
	  private String prepayId;
	  /**二维码链接       String(64)   trade_type为NATIVE是有返回，可将该参数值生成二维码展示出来进行扫码支付*/
	  private String codeURL;
	  
	  
	  
	  
	  
	  
	  
	private String outTradeNo;
	private String feeType ;
	private String totalFee;
	private String bankType;
	private String cashFee;
	private String isSubscribe;
	private String openid;
	private String timeEnd;
	private String transactionId;
		
	
	
	
		
		/**返回状态码  是 	String(16) 	SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断*/
		public String getReturnCode() {
			return returnCode;
		}
		/**返回状态码  是 	String(16) 	SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断*/
		public void setReturnCode(String returnCode) {
			this.returnCode = returnCode;
		}
		
		
		/**返回信息 	否 	String(128) 签名失败 	返回信息，如非空，为错误原因签名失败参数格式校验错误*/
		public String getReturnMsg() {
			return returnMsg;
		}
		/**返回信息 	否 	String(128) 签名失败 	返回信息，如非空，为错误原因签名失败参数格式校验错误*/
		public void setReturnMsg(String returnMsg) {
			this.returnMsg = returnMsg;
		}
		/**公众账号ID  是 	String(32) 	调用接口提交的公众账号ID */
		public String getAppid() {
			return appid;
		}
		/**公众账号ID  是 	String(32) 	调用接口提交的公众账号ID */
		public void setAppid(String appid) {
			this.appid = appid;
		}
		/**商户号  是 	String(32) 调用接口提交的商户号*/
		public String getMchId() {
			return mchId;
		}
		/**商户号  是 	String(32) 调用接口提交的商户号*/
		public void setMchId(String mchId) {
			this.mchId = mchId;
		}
		/**随机字符串   是 	String(32) 微信返回的随机字符串*/
		public String getNonceStr() {
			return nonceStr;
		}
		/**随机字符串   是 	String(32) 微信返回的随机字符串*/
		public void setNonceStr(String nonceStr) {
			this.nonceStr = nonceStr;
		}
		/**签名  是 	String(32) 微信返回的签名，详见签名算法*/
		public String getSign() {
			return sign;
		}
		/**签名  是 	String(32) 微信返回的签名，详见签名算法*/
		public void setSign(String sign) {
			this.sign = sign;
		}
		/**业务结果 	 是 	String(16) SUCCESS/FAIL */
		public String getResultCode() {
			return resultCode;
		}
		/**业务结果 	 是 	String(16) SUCCESS/FAIL */
		public void setResultCode(String resultCode) {
			this.resultCode = resultCode;
		}
		/**错误代码 	err_code */
		public String getErrCode() {
			return errCode;
		}
		/**错误代码 	err_code */
		public void setErrCode(String errCode) {
			this.errCode = errCode;
		}
		 /**错误代码描述 	err_code_des */
		public String getErrCodeDes() {
			return errCodeDes;
		}
		 /**错误代码描述 	err_code_des */
		public void setErrCodeDes(String errCodeDes) {
			this.errCodeDes = errCodeDes;
		}
		 /**交易类型 	是 	String(16)  	调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP */
		public String getTradeType() {
			return tradeType;
		}
		 /**交易类型 	是 	String(16)  	调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP */
		public void setTradeType(String tradeType) {
			this.tradeType = tradeType;
		}
		/**预支付交易会话标识  是 	String(64) 微信生成的预支付回话标识，用于后续接口调用中使用，该值有效期为2小时 */
		public String getPrepayId() {
			return prepayId;
		}
		/**预支付交易会话标识  是 	String(64) 微信生成的预支付回话标识，用于后续接口调用中使用，该值有效期为2小时 */
		public void setPrepayId(String prepayId) {
			this.prepayId = prepayId;
		}
		/**二维码链接       String(64)   trade_type为NATIVE是有返回，可将该参数值生成二维码展示出来进行扫码支付*/
		public String getCodeURL() {
			return codeURL;
		}
		/**二维码链接       String(64)   trade_type为NATIVE是有返回，可将该参数值生成二维码展示出来进行扫码支付*/
		public void setCodeURL(String codeURL) {
			this.codeURL = codeURL;
		}
		/**商户订单号   是 	String(32) 商户系统内部的订单号,32个字符内、可包含字*/
		public String getOutTradeNo() {
			return outTradeNo;
		}
		/**商户订单号   是 	String(32) 商户系统内部的订单号,32个字符内、可包含字*/
		public void setOutTradeNo(String outTradeNo) {
			this.outTradeNo = outTradeNo;
		}
		/**货币类型  否 	String(16) 符合ISO 4217标准的三位字母代码，默认人民币：CNY */
		public String getFeeType() {
			return feeType;
		}
		/**货币类型  否 	String(16) 符合ISO 4217标准的三位字母代码，默认人民币：CNY */
		public void setFeeType(String feeType) {
			this.feeType = feeType;
		}
		/**
		 * @return the totalFee
		 */
		public String getTotalFee() {
			return totalFee;
		}
		/**
		 * @param totalFee the totalFee to set
		 */
		public void setTotalFee(String totalFee) {
			this.totalFee = totalFee;
		}
		/**
		 * @return the bankType
		 */
		public String getBankType() {
			return bankType;
		}
		/**
		 * @param bankType the bankType to set
		 */
		public void setBankType(String bankType) {
			this.bankType = bankType;
		}
		/**
		 * @return the cashFee
		 */
		public String getCashFee() {
			return cashFee;
		}
		/**
		 * @param cashFee the cashFee to set
		 */
		public void setCashFee(String cashFee) {
			this.cashFee = cashFee;
		}
		/**
		 * @return the isSubscribe
		 */
		public String getIsSubscribe() {
			return isSubscribe;
		}
		/**
		 * @param isSubscribe the isSubscribe to set
		 */
		public void setIsSubscribe(String isSubscribe) {
			this.isSubscribe = isSubscribe;
		}
		/**
		 * @return the openid
		 */
		public String getOpenid() {
			return openid;
		}
		/**
		 * @param openid the openid to set
		 */
		public void setOpenid(String openid) {
			this.openid = openid;
		}
		/**
		 * @return the timeEnd
		 */
		public String getTimeEnd() {
			return timeEnd;
		}
		/**
		 * @param timeEnd the timeEnd to set
		 */
		public void setTimeEnd(String timeEnd) {
			this.timeEnd = timeEnd;
		}
		/**
		 * @return the transactionId
		 */
		public String getTransactionId() {
			return transactionId;
		}
		/**
		 * @param transactionId the transactionId to set
		 */
		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}
	 
	  
	  
	  


	 

	 

	  

	  
	
	
	
	
}