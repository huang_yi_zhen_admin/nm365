package cn.gov.xnc.admin.airag.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.airag.entity.AiragPictrueEntity;
import cn.gov.xnc.admin.airag.entity.AiragSoilEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.admin.airag.service.AiragSoilServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("airagSoilService")
@Transactional
public class AiragSoilServiceImpl extends CommonServiceImpl implements AiragSoilServiceI {
	
	public void saveSoilList(List<AiragSoilEntity> list, QuartzConfigAiragEntity config){
		batchSave(list);
		updateEntitie(config);
	}
	
}