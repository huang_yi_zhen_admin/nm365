package cn.gov.xnc.admin.airag.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.airag.entity.AiragSoilEntity;
import cn.gov.xnc.admin.airag.entity.AiragWeatherEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.admin.airag.service.AiragWeatherServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("airagWeatherService")
@Transactional
public class AiragWeatherServiceImpl extends CommonServiceImpl implements AiragWeatherServiceI {
	
	public void saveWeatherList(List<AiragWeatherEntity> list, QuartzConfigAiragEntity config){
		batchSave(list);
		updateEntitie(config);
	}
	
}