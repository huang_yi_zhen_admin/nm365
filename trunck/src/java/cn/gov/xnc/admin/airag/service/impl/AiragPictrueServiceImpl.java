package cn.gov.xnc.admin.airag.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.admin.airag.entity.AiragPictrueEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.admin.airag.service.AiragPictrueServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("airagPictrueService")
@Transactional
public class AiragPictrueServiceImpl extends CommonServiceImpl implements AiragPictrueServiceI {
	
	public void savePictrueList(List<AiragPictrueEntity> list, QuartzConfigAiragEntity config){
		batchSave(list);
		updateEntitie(config);
	}
	
}