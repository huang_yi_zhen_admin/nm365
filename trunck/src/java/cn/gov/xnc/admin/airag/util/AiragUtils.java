package cn.gov.xnc.admin.airag.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonArray;

import cn.gov.xnc.admin.airag.entity.AiragPictrueEntity;
import cn.gov.xnc.admin.airag.entity.AiragSoilEntity;
import cn.gov.xnc.admin.airag.entity.AiragWeatherEntity;
import cn.gov.xnc.admin.airag.entity.QuartzConfigAiragEntity;
import cn.gov.xnc.system.core.util.DateUtils;

public class AiragUtils {
	
	/*public final String CLIENT_ID = "thirdclient";
	
	public final String CLIENT_SECRET = "thirdclient";
	
	public final String GRANT_TYPE = "password";
	
	public final String USERNAME = "thirduser";
	
	public final String PASSWORD = "airag1037";*/
	
	

	public AiragUtils() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		/*AiragUtils airag = new AiragUtils();
		String token = airag.getToken();
		System.out.println(token);
		String res = airag.getAiragPictrue(token);
		res = airag.getAiragSoil(token);
		res = airag.getAiragWeather(token);*/
	}
	
	
	public static List<AiragWeatherEntity> getAiragWeather(String access_token ,QuartzConfigAiragEntity config){

		StringBuffer url = new StringBuffer();
		url.append(config.getInterfaceUrl());
		
		Map<String, String> params = new HashMap<String, String>();
		Date lastCallTime = config.getLastCallTime();
		String start_date = DateUtils.date3Str(config.getLastCallTime(), "yy-MM-dd-HH-mm");
		String end_date = DateUtils.getDate("yy-MM-dd-HH-mm");
		
		config.setLastCallTime(DateUtils.getDate());
		
		params.put("start_date", start_date);
		params.put("end_date", end_date);
		params.put("access_token", access_token);
		params.put("user_id", config.getUserId());
		params.put("ae_id", config.getAeid());
		params.put("page_number", "0");
		params.put("page_size", ""+config.getFetchsize());
		
		String result = "";
		try {
			result = sendGet(url.toString(), params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			config.setLastCallTime(lastCallTime);
			return null;
		}
		
		
		System.out.println(result);
		
		JSONObject obj = JSONObject.parseObject(result);
		List<AiragWeatherEntity> list = new ArrayList<AiragWeatherEntity>();
		JSONArray array = obj.getJSONArray("obj");
		for( int i = 0; i < array.size(); i++ ){
			JSONObject weatherobj = array.getJSONObject(i);
			AiragWeatherEntity entity = new AiragWeatherEntity();
			String ae_id = config.getAeid();
			entity.setAeid(ae_id);
			long collect_time = weatherobj.getLong("collect_time");
			entity.setCollectTime(collect_time);
			entity.setCollectDatetime(DateUtils.getDate(collect_time));
			double rainfall = weatherobj.getDouble("rainfall");
			entity.setRainfall(new BigDecimal(rainfall));
			double air_tem = weatherobj.getDouble("air_tem");
			entity.setAirTem(new BigDecimal(air_tem));
			String air_tem_threshold = weatherobj.getString("air_tem_threshold");
			entity.setAirTemThreshold(air_tem_threshold);
			double air_hum = weatherobj.getDouble("air_hum");
			entity.setAirHum(new BigDecimal(air_tem));
			String air_hum_threshold = weatherobj.getString("air_hum_threshold");
			entity.setAirHumThreshold(air_hum_threshold);
			double light_intensity = weatherobj.getDouble("light_intensity");
			entity.setLightIntensity(new BigDecimal(light_intensity));
			double air_pressure = weatherobj.getDouble("air_pressure");
			entity.setAirPressure(new BigDecimal(air_pressure));
			double wind_direction = weatherobj.getDouble("wind_direction");
			entity.setWindDirection(""+wind_direction);
			String wind_direction_desc = weatherobj.getString("wind_direction_desc");
			entity.setWindDirectionDesc(wind_direction_desc);
			double wind_speed = weatherobj.getDouble("wind_speed");
			entity.setWindSpeed(""+wind_speed);
			String wind_scale = weatherobj.getString("wind_scale");
			entity.setWindScale(wind_scale);
			String max_wind_speed = weatherobj.getString("max_wind_speed");
			entity.setMaxWindSpeed(max_wind_speed);
			String dew_point_tem = weatherobj.getString("dew_point_tem");
			entity.setDewPointTem(dew_point_tem);
			String gpsx_real = weatherobj.getString("gpsx_real");
			entity.setGpsxReal(gpsx_real);
			String gpsy_real = weatherobj.getString("gpsy_real");
			entity.setGpsyReal(gpsy_real);
			
			list.add(entity);
			
		}
		
		return list;
	}
	
	
	public static List<AiragSoilEntity> getAiragSoil(String access_token  ,QuartzConfigAiragEntity config){

		StringBuffer url = new StringBuffer();
		url.append(config.getInterfaceUrl());
		
		Map<String, String> params = new HashMap<String, String>();
		Date lastCallTime = config.getLastCallTime();
		String start_date = DateUtils.date3Str(config.getLastCallTime(), "yy-MM-dd-HH-mm");
		String end_date = DateUtils.getDate("yy-MM-dd-HH-mm");
		
		config.setLastCallTime(DateUtils.getDate());
		
		params.put("start_date", start_date);
		params.put("end_date", end_date);
		params.put("access_token", access_token);
		params.put("user_id", config.getUserId());
		params.put("ae_id", config.getAeid());
		params.put("page_number", "0");
		params.put("page_size", ""+config.getFetchsize());
		
		String result = "";
		try {
			result = sendGet(url.toString(), params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			config.setLastCallTime(lastCallTime);
			return null;
		}
		
		JSONObject obj = JSONObject.parseObject(result);
		List<AiragSoilEntity> list = new ArrayList<AiragSoilEntity>();
		JSONArray array = obj.getJSONArray("obj");
		for( int i = 0; i < array.size(); i++ ){
			JSONObject soilobj = array.getJSONObject(i);
			AiragSoilEntity entity = new AiragSoilEntity();
			String ae_id = config.getAeid();
			entity.setAeid(ae_id);
			String sensor_id = soilobj.getString("sensor_id");
			entity.setSensorId(sensor_id);
			long collect_time = soilobj.getLong("collect_time");
			entity.setCollectTime(collect_time);
			entity.setCollectDatetime(DateUtils.getDate(collect_time));
			double soil_tem = soilobj.getDouble("soil_tem");
			entity.setSoilTem(new BigDecimal(soil_tem));
			
			String soil_tem_threshold = soilobj.getString("soil_tem_threshold");
			entity.setSoilTemThreshold(soil_tem_threshold);
			
			double soil_hum = soilobj.getDouble("soil_hum");
			entity.setSoilHum(new BigDecimal(soil_hum));
			
			String soil_hum_threshold = soilobj.getString("soil_hum_threshold");
			entity.setSoilHumThreshold(soil_hum_threshold);
			
			String gps_x = soilobj.getString("gps_x");
			entity.setGpsX(gps_x);
			
			String gps_y = soilobj.getString("gps_y");
			entity.setGpsY(gps_y);
			
			String sensor = soilobj.getString("sensor");
			entity.setSensor(sensor);
			
			list.add(entity);
		}
		
		
		
		return list;
	}
	
	public static List<AiragPictrueEntity> getAiragPictrue(String access_token ,QuartzConfigAiragEntity config){
		StringBuffer url = new StringBuffer();
		url.append(config.getInterfaceUrl());
		
		Map<String, String> params = new HashMap<String, String>();
		Date lastCallTime = config.getLastCallTime();
		String start_date = DateUtils.date3Str(config.getLastCallTime(), "yy-MM-dd-HH-mm");
		String end_date = DateUtils.getDate("yy-MM-dd-HH-mm");
		
		config.setLastCallTime(DateUtils.getDate());
		
		params.put("start_date", start_date);
		params.put("end_date", end_date);
		params.put("access_token", access_token);
		params.put("user_id", config.getUserId());
		params.put("ae_id", config.getAeid());
		params.put("page_number", "0");
		params.put("page_size", ""+config.getFetchsize());
		
		String result = "";
		try {
			result = sendGet(url.toString(), params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			config.setLastCallTime(lastCallTime);
			return null;
		}
		
		
		System.out.println(result);
		
		List<AiragPictrueEntity> list = new ArrayList<AiragPictrueEntity>();
		JSONObject obj = JSONObject.parseObject(result);
		JSONArray array = obj.getJSONArray("obj");
		for( int i = 0; i < array.size(); i++ ){
			JSONObject picobj = array.getJSONObject(i);
			AiragPictrueEntity entity = new AiragPictrueEntity();
			String pictrue = picobj.getString("picture");
			entity.setPic1(pictrue);
			String picture2 = picobj.getString("picture2");
			entity.setPic2(picture2);
			String picture3 = picobj.getString("picture3");
			entity.setPic3(picture3);
			String picture4 = picobj.getString("picture4");
			entity.setPic4(picture4);
			String ratio = picobj.getString("ratio");
			entity.setRatio(Integer.parseInt(ratio));
			String createTime = picobj.getString("createTime");
			String ae_id = picobj.getString("ae_id");
			entity.setAeid(ae_id);
			long collect_time = picobj.getLong("collect_time");
			entity.setCollectTime(collect_time);
			entity.setCollectDatetime(DateUtils.getDate(collect_time));
			
			list.add(entity);
		}
		
		return list;
	}
	
	
	public static String getToken(QuartzConfigAiragEntity config){
		//http://t1307.airag.cn/ae/oauth/token?client_id=airag&client_secret=airag&grant_type=password&username=defaultuser&password=defaultuser
		StringBuffer url = new StringBuffer();
		url.append("http://api.airag.cn/ae/oauth/token");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("client_id", config.getClientId());
		params.put("client_secret", config.getClientSecret());
		params.put("grant_type", config.getGrantType());
		params.put("username", config.getUsername());
		params.put("password", config.getPassword());
		
		
		String result = sendPost(url.toString(), params);
				
		JSONObject obj = JSONObject.parseObject(result);
		
		
		return obj.getString("access_token");
	}
	
	private static String sendGet(String urlStr, Map<String, String> params) throws Exception  
    {  
  
        System.out.println(urlStr);  
        
        // 发送请求参数            
        if (params != null) {
	          StringBuilder param = new StringBuilder(); 
	          for (Map.Entry<String, String> entry : params.entrySet()) {
	        	  if(param.length()>0){
	        		  param.append("&");
	        	  }	        	  
	        	  param.append(entry.getKey());
	        	  param.append("=");
	        	  param.append(entry.getValue());		        	  
	        	  //System.out.println(entry.getKey()+":"+entry.getValue());
	          }
	          System.out.println("param:"+param.toString());
	          urlStr = urlStr +"?"+ param.toString();
        }
        
        System.out.println(urlStr);
  
        // 创建HttpClient对象  
        HttpClient client = HttpClients.createDefault();  
  
        // 创建GET请求（在构造器中传入URL字符串即可）  
        HttpGet get = new HttpGet(urlStr);  
  
        // 调用HttpClient对象的execute方法获得响应  
        HttpResponse response = client.execute(get);  
  
        // 调用HttpResponse对象的getEntity方法得到响应实体  
        HttpEntity httpEntity = response.getEntity();  
  
        // 使用EntityUtils工具类得到响应的字符串表示  
        String result = EntityUtils.toString(httpEntity, "utf-8");  
        System.out.println(result);  
        
        return result;
    }  
	
	
	
	/**
     * 向指定 URL 发送POST方法的请求     
     * @param url 发送请求的 URL    
     * @param params 请求的参数集合     
     * @return 远程资源的响应结果
     */
	@SuppressWarnings("unused")
	private static String sendPost(String url, Map<String, String> params) {
        OutputStreamWriter out = null;
        BufferedReader in = null;        
        StringBuilder result = new StringBuilder(); 
        try {
            URL realUrl = new URL(url);
            HttpURLConnection conn =(HttpURLConnection) realUrl.openConnection();
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // POST方法
            conn.setRequestMethod("POST");
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.connect();
            // 获取URLConnection对象对应的输出流
            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            // 发送请求参数            
            if (params != null) {
		          StringBuilder param = new StringBuilder(); 
		          for (Map.Entry<String, String> entry : params.entrySet()) {
		        	  if(param.length()>0){
		        		  param.append("&");
		        	  }	        	  
		        	  param.append(entry.getKey());
		        	  param.append("=");
		        	  param.append(entry.getValue());		        	  
		        	  //System.out.println(entry.getKey()+":"+entry.getValue());
		          }
		          System.out.println("param:"+param.toString());
		          out.write(param.toString());
            }
            // flush输出流的缓冲
            out.flush();
            
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {            
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result.toString();
    }

}
