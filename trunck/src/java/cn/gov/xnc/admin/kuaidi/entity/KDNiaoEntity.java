package cn.gov.xnc.admin.kuaidi.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KDNiaoEntity {
	
	//电商用户id
	private java.lang.String eBusinessID;
	//订单号
	private java.lang.String orderCode;
	//快递公司编码
	private java.lang.String shipperCode;
	//查询是否成功
	private boolean success;
	//物流单号
	private java.lang.String logisticCode;
	//物流状态 0--物柜机 2--在途中 3--签收  4--问题件 
	private java.lang.String state;
	//失败原因
	private java.lang.String reason;
	//物流轨迹详情
	private String traces;
	
	/** 
     * 快递鸟的查询代码封装
     * @param <T> 
     * @param map   
     * @param class1 
     * @return 
     */  
    public  Map<String, String> kdniaoToKeyMap () {
    	Map<String, String> map = new HashMap<String, String>();
    	//A
    	//B
    	//C
    	//D
    	map.put("德邦物流", "debangwuliu");
    	//E
    	map.put("EMS", "ems");
    	map.put("E邮宝", "ems");
    	//F
    	//G
    	map.put("国通", "guotongkuaidi");
    	//H   	
    	map.put("汇通", "huitongkuaidi");
    	//S
    	map.put("申通", "shentong");
    	map.put("顺丰", "shunfeng");
    	//T
    	map.put("天天", "tiantian");
    	//Y
    	map.put("圆通", "yuantong");
    	map.put("韵达", "yunda");
    	map.put("邮政", "youzhengguonei");
    	//Z
    	map.put("中通", "zhongtong");
    	map.put("宅急", "zhaijisong");
    	
    	
    	
    	
		return map;  

    }




	public java.lang.String geteBusinessID() {
		return eBusinessID;
	}




	public void seteBusinessID(java.lang.String eBusinessID) {
		this.eBusinessID = eBusinessID;
	}




	public java.lang.String getShipperCode() {
		return shipperCode;
	}




	public void setShipperCode(java.lang.String shipperCode) {
		this.shipperCode = shipperCode;
	}




	public boolean getSuccess() {
		return success;
	}




	public void setSuccess(boolean success) {
		this.success = success;
	}




	public java.lang.String getLogisticCode() {
		return logisticCode;
	}




	public void setLogisticCode(java.lang.String logisticCode) {
		this.logisticCode = logisticCode;
	}




	public java.lang.String getState() {
		return state;
	}




	public void setState(java.lang.String state) {
		this.state = state;
	}




	public String getTraces() {
		return traces;
	}




	public void setTraces(String traces) {
		this.traces = traces;
	}




	public java.lang.String getOrderCode() {
		return orderCode;
	}




	public void setOrderCode(java.lang.String orderCode) {
		this.orderCode = orderCode;
	}




	public java.lang.String getReason() {
		return reason;
	}




	public void setReason(java.lang.String reason) {
		this.reason = reason;
	}
	
	
}
