package cn.gov.xnc.admin.addressManage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "xnc_delivery_address", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class DeliveryAddressEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**公司id*/
	private java.lang.String companyid;
	/**用户id*/
	private java.lang.String userid;
	/**区域id*/
	private java.lang.String territoryid;
	/**省份,中文名称*/
	private java.lang.String province;
	/**城市，中文名称*/
	private java.lang.String city;
	/**区*/
	private java.lang.String area;
	/**收货地址*/
	private java.lang.String address;
	/**收货人姓名*/
	private java.lang.String receivername;
	/**收货人电话*/
	private java.lang.String receivertel;
	/**创建时间*/
	private java.util.Date createtime;
	/**更新时间*/
	private java.util.Date updatetime;
	/**状态，1为正常状态，0为已删除*/
	private java.lang.Byte status;
	/**是否默认地址 Y 是 N 否*/
	private java.lang.String isdefault;
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
	@Column(name ="companyid",nullable=false,length=32)
	public java.lang.String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(java.lang.String companyid) {
		this.companyid = companyid;
	}
	@Column(name ="userid",nullable=false,length=32)
	public java.lang.String getUserid() {
		return userid;
	}
	public void setUserid(java.lang.String userid) {
		this.userid = userid;
	}
	@Column(name ="territoryid",nullable=false,length=32)
	public java.lang.String getTerritoryid() {
		return territoryid;
	}
	public void setTerritoryid(java.lang.String territoryid) {
		this.territoryid = territoryid;
	}
	@Column(name ="province",nullable=false,length=50)
	public java.lang.String getProvince() {
		return province;
	}
	public void setProvince(java.lang.String province) {
		this.province = province;
	}
	@Column(name ="city",nullable=false,length=50)
	public java.lang.String getCity() {
		return city;
	}
	public void setCity(java.lang.String city) {
		this.city = city;
	}
	@Column(name ="area",nullable=false,length=50)
	public java.lang.String getArea() {
		return area;
	}
	public void setArea(java.lang.String area) {
		this.area = area;
	}
	@Column(name ="address",nullable=false,length=128)
	public java.lang.String getAddress() {
		return address;
	}
	public void setAddress(java.lang.String address) {
		this.address = address;
	}
	@Column(name ="receivername",nullable=false,length=10)
	public java.lang.String getReceivername() {
		return receivername;
	}
	public void setReceivername(java.lang.String receivername) {
		this.receivername = receivername;
	}
	@Column(name ="receivertel",nullable=false,length=20)
	public java.lang.String getReceivertel() {
		return receivertel;
	}
	public void setReceivertel(java.lang.String receivertel) {
		this.receivertel = receivertel;
	}
	@Column(name ="createtime",nullable=true,length=32)
	public java.util.Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(java.util.Date createtime) {
		this.createtime = createtime;
	}
	
	public java.util.Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(java.util.Date updatetime) {
		this.updatetime = updatetime;
	}
	@Column(name ="status",nullable=false,length=4)
	public java.lang.Byte getStatus() {
		return status;
	}
	public void setStatus(java.lang.Byte status) {
		this.status = status;
	}
	@Column(name ="isdefault",nullable=false,length=1)
	public java.lang.String getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(java.lang.String isdefault) {
		this.isdefault = isdefault;
	}
	
	
	
	
}
