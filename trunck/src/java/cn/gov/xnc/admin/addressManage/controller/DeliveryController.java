package cn.gov.xnc.admin.addressManage.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.addressManage.entity.DeliveryAddressEntity;
import cn.gov.xnc.admin.addressManage.service.impl.DeliveryAddressServiceImpl;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DisplayMsgUtil;
import cn.gov.xnc.system.core.util.ParamUtil;


@Controller
@RequestMapping("/deliveryController")
public class DeliveryController {
	@Resource(name="deliveryAddressService")
	private DeliveryAddressServiceImpl deliveryAddressService;
	//########################### PC版  #############################
	/**
	 * 收货地址列表
	 * */
	@RequestMapping(value = "deliveryAddress")
	public ModelAndView deliveryAddress(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/deliveryAddress/addressList");
	}
	/**
	 * 收货地址管理
	 * */
	@RequestMapping(value = "deliveryAddressHandle")
	public ModelAndView deliveryAddressHandle(DeliveryAddressEntity deliveryAddress,  ModelMap modelMap){
		modelMap.put("id", deliveryAddress.getId());
		return new ModelAndView("admin/deliveryAddress/addressHandle");
	}
	/**
	 * 收货地址选择
	 * */
	@RequestMapping(value = "deliveryAddressChoise")
	public ModelAndView deliveryAddressChoise(HttpServletRequest request,  ModelMap modelMap){
		return new ModelAndView("admin/deliveryAddress/choiseAddress");
	}
	//########################### 手机版  #############################
	/**
	 *手机版-收货地址列表
	*/
	@RequestMapping(value = "deliveryAddressM")
	public String deliveryAddressM() {
		return "admin/deliveryAddress/m/addressList";
		
	}
	/**
	 * 手机版-收货地址管理
	 * */
	@RequestMapping(value = "deliveryAddressHandleM")
	public ModelAndView deliveryAddressHandleM(DeliveryAddressEntity deliveryAddress,  ModelMap modelMap){
		modelMap.put("id", deliveryAddress.getId());
		return new ModelAndView("admin/deliveryAddress/m/addressHandle");
	}
	
	//########################### 逻辑功能方法  #############################
	/**
	 * 自动识别地址信息
	 * */
	@RequestMapping(value = "autoRecogizeAddress")
	@ResponseBody
	public AjaxJson autoRecogizeAddress(DeliveryAddressEntity deliveryAddressEntity){
		AjaxJson j = new AjaxJson();
		 Map<String, Object> map = deliveryAddressService.autoRecogizeAddress(deliveryAddressEntity.getAddress());
		 j.setObj(map);
		return j;
	}
	/**
	 * 保存地址
	 * */
	@RequestMapping(value = "deliveryAddressSave", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson deliveryAddressSave(DeliveryAddressEntity deliveryAddressEntity){
		AjaxJson j = new AjaxJson();
		String id = deliveryAddressEntity.getId();
		String territoryid = deliveryAddressEntity.getTerritoryid();
		String province = deliveryAddressEntity.getProvince();
		String city = deliveryAddressEntity.getCity();
		String area = deliveryAddressEntity.getArea();
		String address = deliveryAddressEntity.getAddress();
		String receivername = deliveryAddressEntity.getReceivername();
		String receivertel = deliveryAddressEntity.getReceivertel();
		String isDefault = deliveryAddressEntity.getIsdefault();
		if(receivername.length() > 10){
			j.setMsg("姓名过长，请重新填写！");
			j.setSuccess(false);
			return j;
		}
		if(receivertel.length() > 20){
			j.setMsg("电话过长，请重新填写！");
			j.setSuccess(false);
			return j;
		}
		if(address.length() > 128){
			j.setMsg("地址过长，请重新填写！");
			j.setSuccess(false);
			return j;
		}
		if(deliveryAddressEntity.getId()!=null && !deliveryAddressEntity.getId().equals("")){
			if(isDefault.equals("Y")){
				deliveryAddressService.updateDefaultDeliveryAddress();
			}
			Integer result = deliveryAddressService.deliveryAddressUpdateById(id, territoryid, province, city, area, address, receivername, receivertel, isDefault);
			if(result<0){
				j.setMsg("地址更新失败");
				j.setSuccess(false);
			}
		}else{
			if(deliveryAddressService.isExistAddress(deliveryAddressEntity)){
				j.setSuccess(false);
				j.setMsg("地址已存在，请勿重复添加！");
			}else{
				if(isDefault!=null && isDefault.equals("Y")){
					deliveryAddressService.updateDefaultDeliveryAddress();
				}else{
					isDefault = deliveryAddressService.isExistData() ? "N":"Y";
				}
				
				id = deliveryAddressService.deliveryAddressSave(territoryid, province, city, area, address, receivername, receivertel, isDefault);
				if(id.equals("")){
					j.setMsg("地址保存失败");
					j.setSuccess(false);
				}
			}
		}
		return j;
	}
	/**
	 * 查询唯一数据
	 * */
	@RequestMapping(value = "getDeliveryAddressUnique")
	@ResponseBody
	public AjaxJson getDeliveryAddressUnique(DeliveryAddressEntity deliveryAddress){
		AjaxJson j = new AjaxJson();
		deliveryAddress = deliveryAddressService.getDeliveryAddressUnique(deliveryAddress);
		if(deliveryAddress == null){
			j.setSuccess(false);
			j.setMsg("查询有误");
		}
		j.setObj(deliveryAddress);
		return j;
	}
	
	/**
	 * 如果没有默认地址，就取订单最新地址
	 * */
	@RequestMapping(value = "getDeliveryAddressDefault")
	@ResponseBody
	public AjaxJson getDeliveryAddressDefault(){
		AjaxJson j = new AjaxJson();
		DeliveryAddressEntity deliveryAddress = deliveryAddressService.getDeliveryAddressDefault("Y");
		if(deliveryAddress != null){
			j.setObj(deliveryAddress);
		}
		return j;
	}
	/**
	 * 更新默认地址
	 * */
	@RequestMapping(value = "updateDefaultDeliveryAddress")
	@ResponseBody
	public AjaxJson updateDefaultDeliveryAddress(DeliveryAddressEntity deliveryAddress){
		AjaxJson j = new AjaxJson();
		Integer result = deliveryAddressService.updateDefaultDeliveryAddress(deliveryAddress.getId());
		if(result<0){
			j.setMsg("更新失败");
			j.setSuccess(false);
			j.setObj(result);
		}
		return j;
	}
	/**
	 * 删除地址
	 * */
	@RequestMapping(value = "deliveryAddressDelete")
	@ResponseBody
	public AjaxJson deliveryAddressDelete(DeliveryAddressEntity deliveryAddress){
		AjaxJson j = new AjaxJson();
		Integer result = deliveryAddressService.deliveryAddressDelete(deliveryAddress.getId());
		if(result<=0){
			j.setSuccess(false);
			j.setMsg("删除失败");
			j.setObj(result);
		}
		return j;
	}
	/**
	 * 获取地址列表信息
	 * */
	@RequestMapping(value = "deliveryAddressListPage")
	@ResponseBody
	public void deliveryAddressListPage(HttpServletRequest request, HttpServletResponse response, DeliveryAddressEntity deliveryAddress){
		DataGrid dataGrid = new DataGrid();
		String[] ids;
		String field = ParamUtil.getParameter(request, "field", "");
		if(field.indexOf(",")>=0){
			ids = field.split(",");
		}else{
			ids = ParamUtil.getArrayParameter(request, "field");
		}
		dataGrid.setPageNum(ParamUtil.getIntParameter(request, "pageNum", 1));
		dataGrid.setShowNum(ParamUtil.getIntParameter(request, "showNum", 10));
		dataGrid.setSort("updatetime");
		dataGrid = deliveryAddressService.deliveryAddressListPage(dataGrid, deliveryAddress);
		DisplayMsgUtil.printMsg(response, DataConstruction(ids, dataGrid));
	}
	//#################函数处理区##########################
		private static String DataConstruction(String[] field, DataGrid dataGrid){
			StringBuilder sbd = new StringBuilder();
			StringBuilder data = new StringBuilder();
			StringBuilder rows = new StringBuilder();
			List<Map<String, Object>> list;
			String msg = "获取数据成功";
			Integer success = 1;
			int i = 0;
			
			list = dataGrid.getResults();
			
			rows.append("[");
			if(list!=null && list.size()>0){
				for(Map<String, Object> m : list){
					if(i>0){
						rows.append(",");
					}
					rows.append("[");
					int j = 0;
					for(String s : field){
						if(j>0){
							rows.append(",");
						}
						if(m.get(s) == null || m.get(s).toString().equals("")){
							rows.append("\"\"");
						}else{
							rows.append("\"" + m.get(s) + "\"");
						}
						j++;
					}
					rows.append("]");
					i++;
				}
			}
			rows.append("]");
			data.append("\"page\"");
			data.append(":");
			data.append("\"" + dataGrid.getPageNum() +"\"");
			data.append(",");
			data.append("\"total\"");
			data.append(":");
			data.append("\"" + dataGrid.getTotal() +"\"");
			data.append(",");
			data.append("\"rows\"");
			data.append(":");
			data.append(rows);
			
			sbd.append("{");
			sbd.append("\"msg\"");
			sbd.append(":");
			sbd.append("\"" + msg +"\"");
			sbd.append(",");
			sbd.append("\"success\"");
			sbd.append(":");
			sbd.append(success);
			sbd.append(",");
			sbd.append("\"data\"");
			sbd.append(":");
			sbd.append("{" + data +"}");
			sbd.append("}"); 
			return sbd.toString();
		}
}
