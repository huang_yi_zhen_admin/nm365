package cn.gov.xnc.admin.addressManage.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.addressManage.dao.DeliveryAddressDAO;
import cn.gov.xnc.admin.addressManage.entity.DeliveryAddressEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;


@Service("deliveryAddressService")
public class DeliveryAddressServiceImpl {
	@Resource(name="deliveryAddressDAO")
	private DeliveryAddressDAO deliveryAddressDAO;
	
	/**
	 * 保存
	 * @param territoryid:区域ID
	 * @param province:省
	 * @param city：市
	 * @param area：区
	 * @param address：收货地址
	 * @param receivername：收货人
	 * @param receivertel：收货电话
	 * @return String
	 * */
	public String deliveryAddressSave(String territoryid, String province, String city, String area, String address, String receivername, String receivertel,
			 String isDefault){
		DeliveryAddressEntity deliveryAddress = new DeliveryAddressEntity();
		deliveryAddress.setTerritoryid(territoryid);
		deliveryAddress.setProvince(province);
		deliveryAddress.setCity(city);
		deliveryAddress.setArea(area);
		deliveryAddress.setAddress(address);
		deliveryAddress.setReceivername(receivername);
		deliveryAddress.setReceivertel(receivertel);
		deliveryAddress.setIsdefault(isDefault);
		Serializable id = deliveryAddressDAO.deliveryAddressSave(deliveryAddress);
		if(id == null || id.equals("")){
			return "";
		}else{
			return id+"";
		}
		
	}
	/**
	 * 单条数据
	 * */
	public DeliveryAddressEntity getDeliveryAddressUnique(DeliveryAddressEntity deliveryAddress){
		deliveryAddress = deliveryAddressDAO.getDeliveryAddressUnique(deliveryAddress);
		Map<String,Object> map = deliveryAddressDAO.getTerritoryById(deliveryAddress.getTerritoryid());
		deliveryAddress.setProvince(map.get("provinceId")+"");
		deliveryAddress.setCity(map.get("cityId")+"");
		deliveryAddress.setArea(map.get("areaId")+"");
		return deliveryAddress;
	}
	
	/**
	 * 如果没有默认地址，就取订单最新地址
	 * */
	public DeliveryAddressEntity getDeliveryAddressDefault(String isdefault){
		DeliveryAddressEntity deliveryAddress = new DeliveryAddressEntity();
		deliveryAddress.setIsdefault(isdefault);
		deliveryAddress = deliveryAddressDAO.getDeliveryAddressDefault(deliveryAddress);
		return deliveryAddress;
	}
	/**
	 * 更新
	 * @return 
	 * */
	public Integer deliveryAddressUpdateById(String id, String territoryid, String province, String city, String area, String address, String receivername, String receivertel,
			 String isDefault){
		DeliveryAddressEntity deliveryAddress = new DeliveryAddressEntity();
		deliveryAddress.setId(id);
		deliveryAddress.setTerritoryid(territoryid);
		deliveryAddress.setProvince(province);
		deliveryAddress.setCity(city);
		deliveryAddress.setArea(area);
		deliveryAddress.setAddress(address);
		deliveryAddress.setReceivername(receivername);
		deliveryAddress.setReceivertel(receivertel);
		deliveryAddress.setUpdatetime(DateUtils.gettimestamp());
		deliveryAddress.setIsdefault(isDefault);
		return deliveryAddressDAO.deliveryAddressUpdateById(deliveryAddress);
	}
	
	/**
	 * 更新默认收货地址为否
	 * @return 
	 * */
	public void updateDefaultDeliveryAddress(){
		deliveryAddressDAO.updateDefaultDeliveryAddress();
	}
	
	/**
	 * 更新默认收货地址
	 * @return 
	 * */
	public Integer updateDefaultDeliveryAddress(String id){
		DeliveryAddressEntity deliveryAddress = new DeliveryAddressEntity();
		deliveryAddress.setId(id);
		deliveryAddress.setIsdefault("Y");
		return deliveryAddressDAO.updateDefaultDeliveryAddress(deliveryAddress);
	}
	/**
	 * 列表
	 * */
	public DataGrid deliveryAddressListPage(DataGrid dataGrid, DeliveryAddressEntity deliveryAddress){
		String territoryid = "";
		dataGrid = deliveryAddressDAO.deliveryAddressListPage(dataGrid, deliveryAddress);
		List<Map<String, Object>> mapList = dataGrid.getResults();
		Map<String, Object> map = null;
		for(Map<String, Object> m : mapList){
			territoryid = m.get("territoryid")+"";
			map = deliveryAddressDAO.getTerritoryById(territoryid);
			m.put("provinceId", map.get("provinceId"));
			m.put("cityId", map.get("cityId"));
			m.put("areaId", map.get("areaId"));
		}
		return dataGrid;
	}
	/**
	 * 删除
	 * */
	public Integer deliveryAddressDelete(String id){
		/*StringBuilder sbd = new StringBuilder();
		for(String s : id){
			if(!sbd.toString().equals("")){
				sbd.append(",");
			}
			sbd.append("'" + s +"'");
		}*/
		return deliveryAddressDAO.deliveryAddressDelete(id);
	}
	/**
	 * 判断地址是否已存在
	 * */
	public boolean isExistAddress(DeliveryAddressEntity deliveryAddress){
		boolean isExist = false;
		String newAddress = deliveryAddress.getReceivername()+deliveryAddress.getReceivertel()+deliveryAddress.getAddress()
							+deliveryAddress.getProvince()+deliveryAddress.getCity()+deliveryAddress.getArea();
		String oldAddress = "";
		List<DeliveryAddressEntity> deliveryAddressList = deliveryAddressDAO.deliveryAddressList();
		for(DeliveryAddressEntity da : deliveryAddressList){
			oldAddress = da.getReceivername()+da.getReceivertel()+da.getAddress()+da.getProvince()+da.getCity()+da.getArea();
			if(newAddress.equals(oldAddress)){
				return true;
			}
		}
		return isExist;
	}
	/**
	 * 判断是否有数据
	 * */
	public boolean isExistData(){
		boolean isExist = false;
		Map<String, Object> map = deliveryAddressDAO.getDeliveryAddressUnique();
		if(map!=null && map.get("id") != null){
			return true;
		}
		return isExist;
		
	}
	/**
	 * 自动识别地址信息
	 * */
	public Map<String, Object> autoRecogizeAddress(String address){
		return deliveryAddressDAO.autoRecogizeAddress(address);
	}
}
