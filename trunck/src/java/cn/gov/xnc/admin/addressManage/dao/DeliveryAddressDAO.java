package cn.gov.xnc.admin.addressManage.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.gov.xnc.admin.addressManage.entity.DeliveryAddressEntity;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.SqlUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

@Service("deliveryAddressDAO")
public class DeliveryAddressDAO {
	@Resource(name="commonService")
	private CommonService commonService;
	private final String TABLE = "xnc_delivery_address";
	private final String FIELD = "id, companyid, userid, territoryid ,province ,city ,area ,address, receivername, receivertel, createtime, updatetime, status, isdefault, IF(isdefault='Y',\"1\",\"0\") AS defaultsort";
	private DeliveryAddressDAO(){}
	/**
	 * 保存
	 * */
	public Serializable deliveryAddressSave(DeliveryAddressEntity deliveryAddress){
		TSUser user = ResourceUtil.getSessionUserName();
		deliveryAddress.setUserid(user.getId());
		deliveryAddress.setCompanyid(user.getCompany().getId());
		deliveryAddress.setCreatetime(DateUtils.gettimestamp());
		deliveryAddress.setUpdatetime(DateUtils.gettimestamp());
		Serializable id = commonService.save(deliveryAddress);
		return id;
	}
	/**
	 * 单条数据
	 * */
	public DeliveryAddressEntity getDeliveryAddressUnique(DeliveryAddressEntity deliveryAddress){
		return  commonService.findUniqueByProperty(DeliveryAddressEntity.class, "id", deliveryAddress.getId());
	}
	
	/**
	 * 单条数据
	 * */
	public Map<String, Object> getDeliveryAddressUnique(){
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> map = commonService.findOneForJdbc("SELECT id from " + TABLE + " where userid='" + user.getId() + "' and companyid='" + user.getCompany().getId() + "' LIMIT 1", null);
		return  map;
	}
	
	/**
	 * 如果没有默认地址，就取订单最新地址
	 * */
	public DeliveryAddressEntity getDeliveryAddressDefault(DeliveryAddressEntity deliveryAddress){
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String ,Object> where = null;
		StringBuilder sql = new StringBuilder();
		sql.append("select " + FIELD);
		sql.append(" from " + TABLE);
		sql.append(" where 1=1");
		deliveryAddress.setUserid(user.getId());
		where = MyBeanUtils.beanToMapNew(deliveryAddress);
		for(String key : where.keySet()){
			if(where.get(key) != null){
				sql.append(" and " + key + "='" + where.get(key).toString().trim() + "'");
			}
		}
		sql.append(" order by createtime desc LIMIT 1");
		
		Map<String, Object> map = commonService.findOneForJdbc(sql.toString(), null);
		if(map != null){
			deliveryAddress.setAddress(map.get("address")+"");
			deliveryAddress.setReceivername(map.get("receivername")+"");
			deliveryAddress.setReceivertel(map.get("receivertel")+"");
			deliveryAddress.setTerritoryid(map.get("territoryid")+"");
		}else{
			map = commonService.findOneForJdbc("SELECT CustomerName, Telephone, address, territoryid from xnc_order where clientId='" + user.getId() + "' ORDER by createDate desc LIMIT 1", null);
			if(map != null){
				deliveryAddress.setAddress(map.get("address")+"");
				deliveryAddress.setReceivername(map.get("CustomerName")+"");
				deliveryAddress.setReceivertel(map.get("Telephone")+"");
				deliveryAddress.setTerritoryid(map.get("territoryid")+"");
			}else{
				deliveryAddress = null;
			}
		}
		if(deliveryAddress != null){
			map  = this.getTerritoryById(deliveryAddress.getTerritoryid());
			/*map = commonService.findOneForJdbc("SELECT id, territorysort, territoryparentid from t_s_territory where id='" + deliveryAddress.getTerritoryid() + "'", null);
			if(map != null){
				if(map.get("territorysort").toString().equals("2")){
					areaId = map.get("id")+"";
					cityId = map.get("territoryparentid")+"";
					map = commonService.findOneForJdbc("SELECT id, territorysort, territoryparentid from t_s_territory where id='" + cityId + "'", null);
					provinceId = map.get("territoryparentid")+"";
				}else if(map.get("territorysort").toString().equals("1")){
					if(map.get("territoryparentid") != null){
						cityId = map.get("id")+"";
						provinceId = map.get("territoryparentid") + "";
					}else{
						provinceId = map.get("id")+"";
					}
				}
				
			}*/
			deliveryAddress.setProvince(map.get("provinceId").toString());
			deliveryAddress.setCity(map.get("cityId").toString());
			deliveryAddress.setArea(map.get("areaId").toString());
		}
		return  deliveryAddress;
	}
	
	/**
	 * 更新
	 * @return 
	 * */
	public Integer deliveryAddressUpdateById(DeliveryAddressEntity deliveryAddress){
		Map<String ,Object> where = new HashMap<String ,Object>();
		TSUser user = ResourceUtil.getSessionUserName();
		Integer result = 0 ;
		where.put("id", deliveryAddress.getId());
		where.put("userid", user.getId());
		where.put("companyid", user.getCompany().getId());
		deliveryAddress.setId(null);
		String sql = SqlUtil.makeUpdateSql(TABLE, MyBeanUtils.beanToMapNew(deliveryAddress), where);
		System.out.println("deliveryAddressUpdateById:"+sql);
		result = commonService.updateBySqlString(sql);
		return result;
	}
	
	/**
	 * 更新默认收货地址为否
	 * @return 
	 * */
	public void updateDefaultDeliveryAddress(){
		TSUser user = ResourceUtil.getSessionUserName();
		String sql = "UPDATE xnc_delivery_address SET isdefault='N' WHERE userid='" + user.getId() + "' and companyid='" + user.getCompany().getId() + "'";
		commonService.updateBySqlString(sql);
	}
	
	/**
	 * 更新默认收货地址
	 * @return 
	 * */
	public Integer updateDefaultDeliveryAddress(DeliveryAddressEntity deliveryAddress){
		TSUser user = ResourceUtil.getSessionUserName();
		Integer result = 0;
		String sql = "UPDATE xnc_delivery_address SET isdefault='N' WHERE userid='" + user.getId() + "' and companyid='" + user.getCompany().getId() + "'";
		result = commonService.updateBySqlString(sql);
		if(result>0){
			deliveryAddress.setUpdatetime(DateUtils.gettimestamp());
			result = this.deliveryAddressUpdateById(deliveryAddress);
		}
		return result;
	}
	/**
	 * 列表
	 * */
	public DataGrid deliveryAddressListPage(DataGrid dataGrid, DeliveryAddressEntity deliveryAddress){
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String ,Object> where = null;
		String field = dataGrid.getField() == null ? FIELD : dataGrid.getField();
		StringBuilder sql = new StringBuilder();
		long count = 0;
		sql.append("select field");
		sql.append(" from " + TABLE);
		//sql.append(" from " + TABLE + " INNER JOIN t_s_company on " + TABLE + ".company_id=t_s_company.id ");
		sql.append(" where 1=1");
		//sql.append(" and company_id='" + user.getCompany().getId() + "'");
		sql.append(" and userid='"+user.getId()+"'");
		sql.append(" and companyid='"+user.getCompany().getId()+"'");
		where = MyBeanUtils.beanToMapNew(deliveryAddress);
		for(String key : where.keySet()){
			if(where.get(key) != null){
				if("receivername".equals(key) ||
						"receivertel".equals(key) || 
						"address".equals(key)){
					if(!where.get(key).toString().equals("")){
						sql.append(" and " + key + " like '%" + where.get(key).toString().trim() + "%'");
					}
				}else{
					sql.append(" and " + key + "='" + where.get(key).toString().trim() + "'");
				}
							
			}
		}
		count = commonService.getCountForJdbc(sql.toString().replace("field", "count(1)"));
		if(dataGrid.getSort()!=null){
			sql.append(" order by defaultsort desc," + dataGrid.getSort() + " desc");
		}
		System.out.println("#SQL:"+sql.toString().replace("field", field));
		List<Map<String, Object>> mapList = commonService.findForJdbc(sql.toString().replace("field", field),dataGrid.getPageNum(),dataGrid.getShowNum());
		dataGrid.setTotal((int) count);
		dataGrid.setResults(mapList);
		return dataGrid;
	}
	
	/**
	 * 当前用户的地址列表
	 * */
	public List<DeliveryAddressEntity> deliveryAddressList(){
		DeliveryAddressEntity deliveryAddress = null;
		TSUser user = ResourceUtil.getSessionUserName();
		List<DeliveryAddressEntity> list = new ArrayList<DeliveryAddressEntity>();
		StringBuilder sql = new StringBuilder();
		sql.append("select " + FIELD);
		sql.append(" from " + TABLE);
		sql.append(" where");
		sql.append(" companyid='" + user.getCompany().getId() + "'");
		sql.append(" and userid='" + user.getId() + "'");
		List<Map<String, Object>> mapList = commonService.findForJdbc(sql.toString(),null);
		if(mapList != null && mapList.size()>0){
			for(Map<String, Object> m : mapList){
				deliveryAddress = new DeliveryAddressEntity();
				deliveryAddress.setReceivername(m.get("receivername")+"");
				deliveryAddress.setReceivertel(m.get("receivertel")+"");
				deliveryAddress.setAddress(m.get("address")+"");
				deliveryAddress.setProvince(m.get("province")+"");
				deliveryAddress.setCity(m.get("city")+"");
				deliveryAddress.setArea(m.get("area")+"");
				list.add(deliveryAddress);
			}
		}
		return list;
	}
	
	/**
	 * 删除
	 * */
	public Integer deliveryAddressDelete(String id){
		TSUser user = ResourceUtil.getSessionUserName();
		String sql = "delete from " + TABLE + " where id='" + id + "' and userid='" + user.getId() + "'";
		System.out.println("###"+sql);
		return commonService.updateBySqlString(sql);
	}
	/**
	 * 根据区域ID，返回省市区信息
	 * */
	public Map<String,Object> getTerritoryById(String id){
		Map<String, Object> map = null;
		String provinceId = "";
		String cityId = "";
		String areaId = "";
		if(!id.equals("")){
			map = commonService.findOneForJdbc("SELECT id, territorysort, territoryparentid from t_s_territory where id='" + id + "'", null);
			if(map != null){
				if(map.get("territorysort").toString().equals("2")){
					areaId = map.get("id")+"";
					cityId = map.get("territoryparentid")+"";
					map = commonService.findOneForJdbc("SELECT id, territorysort, territoryparentid from t_s_territory where id='" + cityId + "'", null);
					provinceId = map.get("territoryparentid")+"";
				}else if(map.get("territorysort").toString().equals("1")){
					if(map.get("territoryparentid") != null){
						cityId = map.get("id")+"";
						provinceId = map.get("territoryparentid") + "";
					}else{
						provinceId = map.get("id")+"";
					}
				}
			}
		}
		map = new HashMap<String,Object>();
		map.put("provinceId", provinceId);
		map.put("cityId", cityId);
		map.put("areaId", areaId);
		return map;
	}
	/**
	 * 自动识别地址信息
	 * */
	public  Map<String,Object> autoRecogizeAddress(String address){
		//TSUser user = ResourceUtil.getSessionUserName();
		Map<String ,Object> map = new HashMap<String,Object>();
		Map<String ,Object> areaMap = null;
		String group = "";
		String phone = "";
		String name = "";
		String addr = "";
		String provinceId = "0";
		String cityId = "0";
		String areaId = "0";
		Pattern pattern;
		Matcher matcher;
		Matcher matcher1;
		String regex = "\\S+";//匹配非空格
		String phoneRegex = "^[0,1,2,3,4,5,7,8,9][0,1,2,3,4,5,7,8,9][0-9]{9}$";//匹配手机号码
		String numRegex = "\\d{11}";//匹配数字
		if(address != null && !address.equals("")){
			pattern = Pattern.compile(regex);
			matcher = pattern.matcher(address);
			while(matcher.find()){
				group = matcher.group();
				System.out.println("源串："+group);
				pattern = Pattern.compile(phoneRegex);
				matcher1 = pattern.matcher(group);
				if(matcher1.find()){
					phone = matcher.group();
					//System.out.println("这是手机号："+matcher.group());
				}else{
					if(matcher.group().length()<=6){
						name = matcher.group();
						//System.out.println("这是名字："+matcher.group());
					}else{
						addr = matcher.group();
						//System.out.println("这是地址："+matcher.group());
					}
					
				}
			}
			System.out.println(addr);
			areaMap = commonService.findOneForJdbc("SELECT id from t_s_territory where territorylevel=0 and POSITION(SUBSTRING(territoryname,1,2) IN '"+addr+"') LIMIT 1", null);
			if(areaMap != null){
				provinceId = areaMap.get("id")+"";
			}
			areaMap = commonService.findOneForJdbc("SELECT id from t_s_territory where territorylevel=1 and territorysort=1 and POSITION(SUBSTRING(territoryname,1,2) IN '"+addr+"') LIMIT 1", null);
			if(areaMap != null){
				cityId = areaMap.get("id")+"";
			}
			areaMap = commonService.findOneForJdbc("SELECT id from t_s_territory where territorylevel=1 and territorysort=2 and POSITION(SUBSTRING(territoryname,1,2) IN '"+addr+"') LIMIT 1", null);
			if(areaMap != null){
				areaId = areaMap.get("id")+"";
			}
		}
		if(phone.equals("") || name.equals("")){
			if(!address.equals("")){
				pattern = Pattern.compile(numRegex);
				matcher = pattern.matcher(address);
				if(matcher.find()){
					phone = matcher.group();
					areaMap = commonService.findOneForJdbc("SELECT territoryid,address,receivername from xnc_delivery_address where receivertel='"+phone+"' and isdefault='Y'", null);
					if(areaMap != null){
						name = areaMap.get("receivername")+"";
						addr = areaMap.get("address")+"";
						areaMap = this.getTerritoryById(areaMap.get("territoryid")+"");
						if(areaMap!= null && areaMap.get("provinceId")!=null){
							provinceId = areaMap.get("provinceId")+"";
							cityId = areaMap.get("cityId")+"";
							areaId = areaMap.get("areaId")+"";
						}
					}
				}
			}
			
		}
		map.put("phone", phone);
		map.put("name", name);
		map.put("addr", addr);
		map.put("provinceId", provinceId);
		map.put("cityId", cityId);
		map.put("areaId", areaId);
		//for(String key : map.keySet()){
			//System.out.println("key:"+key);
			//System.out.println("value:"+map.get(key));
			//System.out.println("#####################3");
		//}
		return map;
	}
	
}
