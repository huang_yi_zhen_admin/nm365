package cn.gov.xnc.admin.mobile.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class DisplayMsgUtil {
	private static final Logger log = Logger.getLogger(DisplayMsgUtil.class);
	public static void printMsg(HttpServletResponse response,Object msg) {
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		try {
			response.getWriter().print(msg);
		} catch (IOException e) {
			log.error("IOException",e);
		}
	}
}
