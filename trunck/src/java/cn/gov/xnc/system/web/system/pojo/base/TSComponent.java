package cn.gov.xnc.system.web.system.pojo.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

/**
 * 服务组件表
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "t_s_component")
public class TSComponent implements Serializable{
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	private String id;
	//公司编码
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	private TSCompany company;
	//组件类型
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "componentType")
	private TSDictionary componentType;
	//组件名称
	@Column(name="componentName",nullable=false, length = 100)
	private String componentName;
	//组件值
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "componentValue")
	private TSDictionary componentValue;
	//组件具体实现/实现路径
	@Column(name="componentUrl", length = 200)
	private String componentURL;
	//描述
	@Column(name="componentDesc")
	private String componentDesc;
	//创建时间
	@Column(name="createTime",nullable=false)
	private Date createTime;
	//更新时间
	@Column(name="updateTime",nullable=false)
	private Date updateTime;
	//Y是N否删除
	@Column(name="isdel")
	private String isdel;
	
	public TSComponent() {
	}

	public TSComponent(String id, TSDictionary componentType,
			TSCompany company, String componentName,
			TSDictionary componentValue, String componentURL,
			String componentDesc, Date createTime, Date updateTime, String isdel) {
		this.id = id;
		this.componentType = componentType;
		this.company = company;
		this.componentName = componentName;
		this.componentValue = componentValue;
		this.componentURL = componentURL;
		this.componentDesc = componentDesc;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.isdel = isdel;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TSDictionary getComponentType() {
		return componentType;
	}

	public void setComponentType(TSDictionary componentType) {
		this.componentType = componentType;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public TSDictionary getComponentValue() {
		return componentValue;
	}

	public void setComponentValue(TSDictionary componentValue) {
		this.componentValue = componentValue;
	}

	public String getComponentURL() {
		return componentURL;
	}

	public void setComponentURL(String componentURL) {
		this.componentURL = componentURL;
	}

	public String getComponentDesc() {
		return componentDesc;
	}

	public void setComponentDesc(String componentDesc) {
		this.componentDesc = componentDesc;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getIsdel() {
		return isdel;
	}

	public void setIsdel(String isdel) {
		this.isdel = isdel;
	}

	public TSCompany getCompany() {
		return company;
	}

	public void setCompany(TSCompany company) {
		this.company = company;
	}
	
	
}
