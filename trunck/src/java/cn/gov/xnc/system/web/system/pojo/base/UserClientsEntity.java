package cn.gov.xnc.system.web.system.pojo.base;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import org.hibernate.annotations.GenericGenerator;


/**   
 * @Title: Entity
 * @Description: 客户信息表
 * @author zero
 * @date 2016-09-26 09:11:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_user_clients", schema = "")
@SuppressWarnings("serial")
public class UserClientsEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**购买打折类型信息*/
	private UserTypeEntity usertpyrid;
	/**所属地区*/
	private TSTerritory TSTerritory ;// 地址
	/**详细地址*/
	private java.lang.String userAddress;
	/**联系人*/
	private java.lang.String contacts;
	/**联系电话*/
	private java.lang.String contactphone;
	/**备注*/
	private java.lang.String remarks;
	/**业务员ID*/
	private TSUser yewuid;
	/**结算方式 1 现付 2预付 3 后付*/
	private java.lang.String settlement;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  购买打折类型信息
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERTPYRID")
	public UserTypeEntity getUsertpyrid(){
		return this.usertpyrid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  购买打折类型信息
	 */
	public void setUsertpyrid(UserTypeEntity usertpyrid){
		this.usertpyrid = usertpyrid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地址信息
	 */
	@JsonIgnore    //getList查询转换为列表时处理json转换异常
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TERRITORYID")
	public TSTerritory getTSTerritory() {
		return TSTerritory;
	}

	public void setTSTerritory(TSTerritory tSTerritory) {
		TSTerritory = tSTerritory;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  详细地址
	 */
	@Column(name ="USERADDRESS",nullable=true,length=1000)
	public java.lang.String getUserAddress(){
		return this.userAddress;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  详细地址
	 */
	public void setUserAddress(java.lang.String userAddress){
		this.userAddress = userAddress;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  联系人
	 */
	@Column(name ="CONTACTS",nullable=true,length=200)
	public java.lang.String getContacts(){
		return this.contacts;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  联系人
	 */
	public void setContacts(java.lang.String contacts){
		this.contacts = contacts;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  联系电话
	 */
	@Column(name ="CONTACTPHONE",nullable=true,length=40)
	public java.lang.String getContactphone(){
		return this.contactphone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  联系电话
	 */
	public void setContactphone(java.lang.String contactphone){
		this.contactphone = contactphone;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="REMARKS",nullable=true,length=2000)
	public java.lang.String getRemarks(){
		return this.remarks;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setRemarks(java.lang.String remarks){
		this.remarks = remarks;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  业务员ID
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "YEWUID")
	public TSUser getYewuid(){
		return this.yewuid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  业务员ID
	 */
	public void setYewuid(TSUser yewuid){
		this.yewuid = yewuid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结算方式 1 现付 2预付 3 后付
	 */
	@Column(name ="SETTLEMENT",nullable=true,length=255)
	public java.lang.String getSettlement(){
		return this.settlement;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结算方式 1 现付 2预付 3 后付
	 */
	public void setSettlement(java.lang.String settlement){
		this.settlement = settlement;
	}
}
