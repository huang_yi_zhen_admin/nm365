package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 个人账户管理
 * @author zero
 * @date 2016-10-09 10:22:04
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_function_company", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class FunctionCompanyEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**权限id*/
	private TSFunction functionid;
	/**公司信息ID*/
	private TSCompany company;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}

	/**
	 * @return 权限id
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "functionid")
	public TSFunction getFunctionid() {
		return functionid;
	}

	/**
	 * @param 权限id
	 */
	public void setFunctionid(TSFunction functionid) {
		this.functionid = functionid;
	}

	/**
	 * @return 公司信息ID
	 */
	@JsonIgnore    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public TSCompany getCompany() {
		return company;
	}

	/**
	 * @param 公司信息ID
	 */
	public void setCompany(TSCompany company) {
		this.company = company;
	}
	
}
