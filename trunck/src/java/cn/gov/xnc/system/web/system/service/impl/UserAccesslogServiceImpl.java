package cn.gov.xnc.system.web.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gov.xnc.system.web.system.service.UserAccesslogServiceI;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;

@Service("userAccesslogService")
@Transactional
public class UserAccesslogServiceImpl extends CommonServiceImpl implements UserAccesslogServiceI {
	
}