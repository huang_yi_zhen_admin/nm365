package cn.gov.xnc.system.web.system.controller.core;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.admin.product.entity.ProductEntity;
import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.pojo.base.UserClientsEntity;
import cn.gov.xnc.system.web.system.pojo.base.UserGradePriceEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserGradePriceServiceI;

/**   
 * @Title: Controller
 * @Description: 个人账户管理
 * @author zero
 * @date 2016-11-20 22:21:30
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/gradePriceController")
public class UserGradePriceController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserGradePriceController.class);

	@Autowired
	private UserGradePriceServiceI userGradePriceService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 个人账户管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "list")
	public ModelAndView userGradePrice(HttpServletRequest request) {
		return new ModelAndView("admin/salesman/userGradePriceList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(value = "datagrid")
	public void datagrid(UserGradePriceEntity userGradePrice,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(UserGradePriceEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userGradePrice, request.getParameterMap());
		this.userGradePriceService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 删除个人账户管理
	 * 
	 * @return
	 */
	@RequestMapping(value = "del")
	@ResponseBody
	public AjaxJson del(UserGradePriceEntity userGradePrice, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		userGradePrice = systemService.getEntity(UserGradePriceEntity.class, userGradePrice.getId());
		message = "个人账户管理删除成功";
		userGradePriceService.delete(userGradePrice);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加个人账户管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "save")
	@ResponseBody
	public AjaxJson save(UserGradePriceEntity userGradePrice, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userGradePrice.getId())) {
			message = "个人账户管理更新成功";
			UserGradePriceEntity t = userGradePriceService.get(UserGradePriceEntity.class, userGradePrice.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userGradePrice, t);
				userGradePriceService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "个人账户管理更新失败";
			}
		} else {
			message = "个人账户管理添加成功";
			userGradePriceService.save(userGradePrice);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 个人账户管理列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "addorupdate")
	public ModelAndView addorupdate(UserGradePriceEntity userGradePrice, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(userGradePrice.getId())) {
			userGradePrice = userGradePriceService.getEntity(UserGradePriceEntity.class, userGradePrice.getId());
			req.setAttribute("userGradePricePage", userGradePrice);
		}
		return new ModelAndView("admin/salesman/userGradePrice");
	}
	
	/**
	 * 
	 * @param productid
	 * @param userGradeId
	 * @return
	 */
	@RequestMapping(value = "getManulScalePrice")
	@ResponseBody
	public String getManulScalePrice(UserGradePriceEntity userGradePrice, HttpServletRequest req){
		String manualPrice = "";
		if(StringUtil.isNotEmpty(userGradePrice.getProductid()) 
				&& StringUtil.isNotEmpty(userGradePrice.getUsertypeid())){
			
			CriteriaQuery cq = new CriteriaQuery(UserGradePriceEntity.class);
			cq.eq("productid", userGradePrice.getProductid());
			cq.eq("usertypeid", userGradePrice.getUsertypeid());
			cq.add();
			
			List<UserGradePriceEntity> gradePriceList = systemService.getListByCriteriaQuery(cq, false);
			if(null != gradePriceList && gradePriceList.size() > 0){
				manualPrice = String.valueOf(gradePriceList.get(0).getManualprice());
			}
		}
		
		
		return manualPrice;
	}
	
	/**
	 * 获取用户商品等级价格
	 * @param productid
	 * @param unitid
	 * @return
	 */
	@RequestMapping(value = "getUserProductPriceJson")
	@ResponseBody
	public AjaxJson getUserProductPriceJson(HttpServletRequest req) {
		AjaxJson j = new AjaxJson();
		j.setSuccess(false);
		
		String productid = req.getParameter("productid");
		String userid = req.getParameter("userid");
		
		if (StringUtil.isNotEmpty(productid) && StringUtil.isNotEmpty(userid)) {
			try {
				ProductEntity product =  systemService.findUniqueByProperty(ProductEntity.class, "id", productid);
				TSUser user =  systemService.findUniqueByProperty(TSUser.class, "id", userid);
				j.setObj(userGradePriceService.getProductFinalPrice(product, user));
				j.setSuccess(true);
				j.setMsg("获取用户商品等级价格成功！");
				
			} catch (Exception e) {
				j.setSuccess(false);
				j.setMsg(e.getMessage());
			}
		}else{
			j.setMsg("抱歉，你传入的参数有误!");
		}
		
		return j;
	}
}
