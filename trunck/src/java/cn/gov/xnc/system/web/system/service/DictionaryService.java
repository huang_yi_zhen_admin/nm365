package cn.gov.xnc.system.web.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;

/**
 * 字典管理接口
 */
public interface DictionaryService  extends CommonService{
	
	public static final String DEL_YES = "Y";
	public static final String DEL_NOT = "N";
	
	/**
	 * 根据条件查询列表
	 * @param params
	 * @return
	 */
	public List<TSDictionary> findListByParams(Map<String, Object> params);
	
	/**
	 * AJAX 根据条件查询列表并分页
	 * @param params
	 */
	public void findListPageByParams(Map<String, Object> params);
	
	/**
	 * 根据对象封装参数
	 * @param component
	 * @return
	 */
	public Map<String, Object> getParams(TSDictionary dictionary);
	
	/**
	 * 根据字典类型和编码值，检测配置项目在字典里是否存在
	 * @param dictType
	 * @param dictVal
	 * @return TSDictionary
	 */
	public TSDictionary checkDictItemWithoutCompany(String dictType, String dictVal);
	
	/**
	 * 根据字典类型和编码值，获取指定字典信息
	 * @param type
	 * @param value
	 * @return
	 */
	public TSDictionary findDictionaryItem(String type, String value);
	
	/**
	 * 添加字典配置
	 * @param dict
	 * @return
	 */
	public TSDictionary addDictionaryItem(TSDictionary dict);
	
	/**
	 * 添加字典配置
	 * @param dict
	 * @return
	 */
	public TSDictionary check2addDictionaryItem(TSDictionary dict);
}
