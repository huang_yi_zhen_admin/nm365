package cn.gov.xnc.system.web.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.util.BeanToMapUtils;
import cn.gov.xnc.system.core.util.DateUtils;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.vo.datatable.SortDirection;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSDictionary;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.DictionaryService;

@Service("dictionaryService")
public class DictionaryServiceImpl extends CommonServiceImpl implements DictionaryService{

	@Override
	public void findListPageByParams(Map<String, Object> params) {
		DataGrid dataGrid = (DataGrid) params.get("dataGrid");
		CriteriaQuery cq = new CriteriaQuery(TSDictionary.class,dataGrid);
		if(StringUtil.isNotEmpty(params.get("company"))){
			cq.eq("company", params.get("company"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryType"))){
			cq.eq("dictionaryType", params.get("dictionaryType"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryName"))){
			cq.eq("dictionaryName", params.get("dictionaryName"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryValue"))){
			cq.eq("dictionaryValue", params.get("dictionaryValue"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryURL"))){
			cq.eq("dictionaryURL", params.get("dictionaryURL"));
		}
		
		if(StringUtil.isNotEmpty(params.get("isdel"))){
			cq.eq("isdel", params.get("isdel"));
		}
		
		String startTime = (String) params.get("startTime");
		String endTime = (String) params.get("endTime");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(startTime)&&StringUtil.isNotEmpty(endTime)){
			cq.between("createTime", DateUtils.str2Date(startTime,sdf), DateUtils.str2Date(endTime,sdf));
		}else if( StringUtil.isNotEmpty(startTime) ){
			cq.ge("createTime", DateUtils.str2Date(startTime,sdf));
		}else if( StringUtil.isNotEmpty(endTime) ){
			cq.le("createTime", DateUtils.str2Date(endTime,sdf));
		}
		cq.addOrder("createTime", SortDirection.desc);
		
		cq.add();
		this.getDataGridReturn(cq, true);
	}
	
	@Override
	public List<TSDictionary> findListByParams(Map<String, Object> params) {
		CriteriaQuery cq = new CriteriaQuery(TSDictionary.class);
		if(StringUtil.isNotEmpty(params.get("company"))){
			cq.eq("company", params.get("company"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryType"))){
			cq.eq("dictionaryType", params.get("dictionaryType"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryName"))){
			cq.eq("dictionaryName", params.get("dictionaryName"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryValue"))){
			cq.eq("dictionaryValue", params.get("dictionaryValue"));
		}
		
		if(StringUtil.isNotEmpty(params.get("dictionaryURL"))){
			cq.eq("dictionaryURL", params.get("dictionaryURL"));
		}
		
		if(StringUtil.isNotEmpty(params.get("isdel"))){
			cq.eq("isdel", params.get("isdel"));
		}
		
		String startTime = (String) params.get("startTime");
		String endTime = (String) params.get("endTime");
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd"); 
		if(StringUtil.isNotEmpty(startTime)&&StringUtil.isNotEmpty(endTime)){
			cq.between("createTime", DateUtils.str2Date(startTime,sdf), DateUtils.str2Date(endTime,sdf));
		}else if( StringUtil.isNotEmpty(startTime) ){
			cq.ge("createTime", DateUtils.str2Date(startTime,sdf));
		}else if( StringUtil.isNotEmpty(endTime) ){
			cq.le("createTime", DateUtils.str2Date(endTime,sdf));
		}
		cq.addOrder("createTime", SortDirection.desc);
		
		cq.add();
		List<TSDictionary> list = this.getListByCriteriaQuery(cq, false);
		return list;
	}

	@Override
	public Map<String, Object> getParams(TSDictionary dictionary) {
		TSUser user = ResourceUtil.getSessionUserName();
		Map<String, Object> params = BeanToMapUtils.convertBeanToMap(dictionary);
		params.put("company", user.getCompany());
		return params;
	}

	@Override
	public TSDictionary checkDictItemWithoutCompany(String dictType, String dictVal) {
		TSDictionary dict = null;
		
		if(StringUtil.isNotEmpty(dictType)
				&& StringUtil.isNotEmpty(dictVal)){
			
			CriteriaQuery cq = new CriteriaQuery(TSDictionary.class);
			cq.eq("dictionaryType", dictType);
			cq.eq("dictionaryValue", dictVal);
			cq.eq("isdel", DictionaryService.DEL_NOT);
			cq.add();
			
			List<TSDictionary> dictList = getListByCriteriaQuery(cq, false);
			if(null != dictList && dictList.size() > 0){
				dict = dictList.get(0);
			}
			
		}
		
		return dict;
	}

	@Override
	public TSDictionary addDictionaryItem(TSDictionary dict) {
		
		TSDictionary addDict = null;
		String dictType = dict.getDictionaryType();
		String dictVal = dict.getDictionaryValue();
		if(StringUtil.isNotEmpty(dictType)
				&& StringUtil.isNotEmpty(dictVal)){
		
			TSUser user = ResourceUtil.getSessionUserName();
			TSCompany company = findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
		
			dict.setCompany(company);
			dict.setCreateTime(new Date());
			dict.setIsdel(DictionaryService.DEL_NOT);
			
			save(dict);
			
			addDict = dict;
		}
		
		return addDict;
	}

	@Override
	public TSDictionary findDictionaryItem(String type, String value) {
		TSUser user = ResourceUtil.getSessionUserName();
		
		if(!StringUtil.isNotEmpty(type)){
			return null;
		}
		if(!StringUtil.isNotEmpty(value)){
			return null;
		}
		
		TSDictionary dictionary =  new TSDictionary();
		dictionary.setDictionaryType(type);
		dictionary.setDictionaryValue(value);
		dictionary.setCompany(user.getCompany());
		
		Map<String,Object> params = this.getParams(dictionary);
		List<TSDictionary> list = findListByParams(params);
		
		if(list.size() <= 0){
			return null;
		}
		return list.get(0);
	}

	@Override
	public TSDictionary check2addDictionaryItem(TSDictionary dict) {
		TSDictionary addDict = checkDictItemWithoutCompany(dict.getDictionaryType(), dict.getDictionaryValue());
		if(null == addDict){
			
			String dictType = dict.getDictionaryType();
			String dictVal = dict.getDictionaryValue();
			if(StringUtil.isNotEmpty(dictType)
					&& StringUtil.isNotEmpty(dictVal)){
			
				TSUser user = ResourceUtil.getSessionUserName();
				TSCompany company = findUniqueByProperty(TSCompany.class, "id", user.getCompany().getId());
			
				dict.setCompany(company);
				dict.setCreateTime(new Date());
				dict.setIsdel(DictionaryService.DEL_NOT);
				
				save(dict);
				
				List<TSDictionary> dictList = findListByParams( getParams(dict) );
				if(null != dictList && dictList.size() > 0){
					addDict = dictList.get(0);
				}
			}
		}
		
		return addDict;
	}
	
}
