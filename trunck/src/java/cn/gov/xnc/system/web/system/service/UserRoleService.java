package cn.gov.xnc.system.web.system.service;

import cn.gov.xnc.system.web.system.pojo.base.TSRole;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;

public interface UserRoleService {

	/**
	 * 根据用户权限获取相关权限
	 * @param user
	 * @return
	 */
	public TSRole getRoleByUserRole(TSUser user);
}
