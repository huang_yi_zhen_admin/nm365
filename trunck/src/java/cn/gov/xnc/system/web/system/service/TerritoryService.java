package cn.gov.xnc.system.web.system.service;


import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.service.CommonService;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;

/**
 * 
 * @author  zero
 *
 */
public interface TerritoryService extends CommonService{
	/**
	 * jdbc方式查询
	 * @param parentId
	 * @return
	 */
	public List<TSTerritory> findTerritoryByJdbc(String parentId);
	
	public List<TSTerritory> findRootTerritoryByJbdc();
	
	public List<HashMap<String, String>> getPageTerritoryInfo(List<TSTerritory> listParentTerritory);
	
	public TSTerritory getTopTerritoryByAddress(String address);
	
	public boolean hasProvinceInfo(String provincename);
	
	public String jdbcFindTerritoryNameById(String territoryid);
}
