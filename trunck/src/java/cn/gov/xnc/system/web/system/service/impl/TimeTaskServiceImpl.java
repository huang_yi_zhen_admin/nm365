package cn.gov.xnc.system.web.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.web.system.service.TimeTaskServiceI;

@Service("timeTaskService")

public class TimeTaskServiceImpl extends CommonServiceImpl implements TimeTaskServiceI {
	
}