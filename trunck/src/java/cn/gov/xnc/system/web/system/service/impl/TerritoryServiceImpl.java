package cn.gov.xnc.system.web.system.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import cn.gov.xnc.system.core.common.service.impl.CommonServiceImpl;
import cn.gov.xnc.system.core.encache.EncacheUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSTerritory;
import cn.gov.xnc.system.web.system.service.TerritoryService;

/**
 * 
 * @author  zero
 *
 */
@Service("territoryService")

public class TerritoryServiceImpl extends CommonServiceImpl implements TerritoryService {

	@Override
	public List<TSTerritory> findTerritoryByJdbc(String parentId) {
		List<TSTerritory> list2 = EncacheUtil.getInstance().get(parentId);
		if(list2!=null&&list2.size()>0){
			return list2;
		}else{
			String sql = "select territoryName,id from t_s_territory where territoryparentid =? order by territorysort desc";	
			List<TSTerritory> list =  findListByJdbc(sql, TSTerritory.class, new Object[]{parentId});
			EncacheUtil.getInstance().put(parentId, list);
			return list;
		}
	}

	
	public List<TSTerritory> findRootTerritoryByJbdc(){	
		String key = "allArea";
		List<TSTerritory> list2 = EncacheUtil.getInstance().get(key);
		if(list2!=null&&list2.size()>0){
			return list2;
		}else{
			String sql  = "select territoryName,id from t_s_territory where territoryparentid is NULL order by territorysort desc";
			//return findListByJdbc(sql,TSTerritory.class);
			List<TSTerritory> list =  queryListByJdbc(sql, TSTerritory.class);
			EncacheUtil.getInstance().put(key, list);
			return list;
		}
	}
	

	
	
	/**
	 * 根据父级区域数据，查找下一级子区域数据，重新组建方便前台页面可获取的区域数据体
	 * @param listParentTerritory 父级区域数据
	 * @return List<HashMap<String， String>>
	 */
	public List<HashMap<String, String>> getPageTerritoryInfo(List<TSTerritory> listParentTerritory){
		List territoryMapList = new ArrayList<HashMap<String, String>>();
		
		List<TSTerritory> citylist = null;
		StringBuffer childrenBuf = new StringBuffer();
		
		//重新组织省份页面数据到前台
		for (TSTerritory parentTsTerritory : listParentTerritory) {
			HashMap<String, String> ParentTerritory = new HashMap<String, String>();
			citylist = findTerritoryByJdbc(parentTsTerritory.getId());
			
			ParentTerritory.put("id", parentTsTerritory.getId());
			ParentTerritory.put("name", parentTsTerritory.getTerritoryName());
			ParentTerritory.put("code", parentTsTerritory.getTerritoryCode());
			
			//清空buf
			childrenBuf.delete(0, childrenBuf.length());
			childrenBuf.append("[");
			TSTerritory tsTerritory = null;
			for (int i = 0; i < citylist.size(); i++) {
				tsTerritory = citylist.get(i);
				
				if(i != 0){
					childrenBuf.append(",");
				}
				
				childrenBuf.append("{");
				childrenBuf.append("\"area_id\" : \"" + tsTerritory.getId() + "\",");
				childrenBuf.append("\"area_code\" : \"" + tsTerritory.getTerritoryCode() + "\",");
				childrenBuf.append("\"parent_id\" : \"" + parentTsTerritory.getId() +  "\",");
				childrenBuf.append("\"area_name\" : \"" + tsTerritory.getTerritoryName() + "\"");
				childrenBuf.append("}");
			}
			childrenBuf.append("]");
			ParentTerritory.put("child_data", childrenBuf.toString().replaceAll("\"", "&quot;"));
			territoryMapList.add(ParentTerritory);
		}
		return territoryMapList;
	}


	@Override
	public TSTerritory getTopTerritoryByAddress(String address) {
		TSTerritory territory = null;
		if(StringUtil.isNotEmpty(address)){
			List<TSTerritory> listTerritory = findRootTerritoryByJbdc();
			if(null != listTerritory && listTerritory.size() > 0){
				for (TSTerritory tsTerritory : listTerritory) {
					if(StringUtil.isNotEmpty(tsTerritory.getTerritoryName()) 
							&& address.startsWith(tsTerritory.getTerritoryName().substring(0, 2))){
						territory = tsTerritory;
						break;
					}
				}
			}
		}
		return territory;
	}

	@Override
	public boolean hasProvinceInfo(String provincename) {
		
		if(StringUtil.isNotEmpty(provincename)){
			
			String sql  = "select territoryName,id from t_s_territory where territoryparentid is NULL and territoryname like concat(?,'%')";
			
			List<TSTerritory> list =  findListByJdbc(sql, TSTerritory.class, new Object[]{provincename});
			
			if(null != list && list.size() > 0){
				return true;
			}
			
		}
		
		return false;
	}
	
	/**
	 * 根据territory id 查找地区名字
	 */
	@Override
	public String jdbcFindTerritoryNameById(String territoryid) {
		String sql = "select territoryName,id from t_s_territory where id =? ";	
		List<TSTerritory> list =  findListByJdbc(sql, TSTerritory.class, new Object[]{territoryid});
		if(null != list && list.size() > 0){
			return list.get(0).getTerritoryName();
			
		}else{
			return null;
		}
	}
	
}
