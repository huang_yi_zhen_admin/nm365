package cn.gov.xnc.system.web.system.pojo.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

/**
 * 字典管理表
 * 
 * @author zero
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "t_s_dictionary")
public class TSDictionary implements Serializable {
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", nullable = true, length = 32)
	private String id;
	// 公司编码
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	private TSCompany company;
	// 字典类型
	@Column(name = "dictionaryType", nullable = false, length = 200)
	private String dictionaryType;
	// 名称
	@Column(name = "dictionaryName", nullable = false, length = 200)
	private String dictionaryName;
	// 内容/值
	@Column(name = "dictionaryValue", length = 200)
	private String dictionaryValue;
	// 描述
	@Column(name = "dictionaryDesc")
	private String dictionaryDesc;
	// 创建时间
	@Column(name = "createTime", nullable = false)
	private Date createTime;
	// 更新时间
	@Column(name = "updateTime", nullable = false)
	private Date updateTime;
	// Y是N否删除
	@Column(name = "isdel")
	private String isdel;

	public TSDictionary() {
	}

	public TSDictionary(String id, TSCompany company, String dictionaryType,
			String dictionaryName, String dictionaryValue,
			String dictionaryDesc, Date createTime, Date updateTime,
			String isdel) {
		this.id = id;
		this.company = company;
		this.dictionaryType = dictionaryType;
		this.dictionaryName = dictionaryName;
		this.dictionaryValue = dictionaryValue;
		this.dictionaryDesc = dictionaryDesc;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.isdel = isdel;
	}

	public String getDictionaryType() {
		return dictionaryType;
	}

	public void setDictionaryType(String dictionaryType) {
		this.dictionaryType = dictionaryType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDictionaryName() {
		return dictionaryName;
	}

	public void setDictionaryName(String dictionaryName) {
		this.dictionaryName = dictionaryName;
	}

	public String getDictionaryValue() {
		return dictionaryValue;
	}

	public void setDictionaryValue(String dictionaryValue) {
		this.dictionaryValue = dictionaryValue;
	}

	public String getDictionaryDesc() {
		return dictionaryDesc;
	}

	public void setDictionaryDesc(String dictionaryDesc) {
		this.dictionaryDesc = dictionaryDesc;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getIsdel() {
		return isdel;
	}

	public void setIsdel(String isdel) {
		this.isdel = isdel;
	}

	public TSCompany getCompany() {
		return company;
	}

	public void setCompany(TSCompany company) {
		this.company = company;
	}

}
