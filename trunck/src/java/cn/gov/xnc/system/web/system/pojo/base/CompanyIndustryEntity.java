package cn.gov.xnc.system.web.system.pojo.base;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 公司行业信息
 * @author zero
 * @date 2016-09-26 02:54:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_company_industry", schema = "")
@DynamicUpdate(true)
@DynamicInsert(true)
@SuppressWarnings("serial")
public class CompanyIndustryEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**行业名称*/
	private java.lang.String industryname;
	/**行业类型*/
	private java.lang.String type;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=true,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  行业名称
	 */
	@Column(name ="INDUSTRYNAME",nullable=true,length=400)
	public java.lang.String getIndustryname(){
		return this.industryname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  行业名称
	 */
	public void setIndustryname(java.lang.String industryname){
		this.industryname = industryname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  行业类型
	 */
	@Column(name ="TYPE",nullable=true,length=2)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  行业类型
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
}
