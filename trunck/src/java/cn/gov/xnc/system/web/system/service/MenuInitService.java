package cn.gov.xnc.system.web.system.service;

import cn.gov.xnc.system.core.common.service.CommonService;

/**
 * 
 * @author  zero
 *
 */
public interface MenuInitService extends CommonService{
	
	public void initMenu();
}
