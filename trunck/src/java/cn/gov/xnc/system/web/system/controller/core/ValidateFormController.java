package cn.gov.xnc.system.web.system.controller.core;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.ValidateForm;
import cn.gov.xnc.system.core.util.ResourceUtil;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.web.system.pojo.base.CompanyPlatformEntity;
import cn.gov.xnc.system.web.system.pojo.base.TSCompany;
import cn.gov.xnc.system.web.system.pojo.base.TSUser;
import cn.gov.xnc.system.web.system.service.CompanyService;
import cn.gov.xnc.system.web.system.service.SystemService;

@Scope("prototype")
@Controller
@RequestMapping("/validateController")
public class ValidateFormController extends BaseController{
	@Autowired
	private SystemService systemService;
	@Autowired
	private CompanyService companyService;
	
	/**
     * 公司明名称验证
     */
    @RequestMapping(value = "check")
    @ResponseBody
    public ValidateForm checkValidate(HttpServletRequest request){
    	
    	String type = ResourceUtil.getParameter("type");
    	ValidateForm validateinfo = new ValidateForm();
    	String checkVal = ResourceUtil.getParameter("param");
    	String roletype = ResourceUtil.getParameter("role");
    	
    	if("company".equals(type)){
    		if(StringUtil.isEmpty(checkVal)){
    			checkVal = ResourceUtil.getParameter("companyName");
    		}
    		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "companyName", checkVal);
    		if(null != company){
    			validateinfo.setInfo("公司名称已经存在");
    			validateinfo.setStatus("n");
    		}
    		
    	}else if("mobilePhone".equals(type)){
    		if(StringUtil.isEmpty(checkVal)){
    			checkVal = ResourceUtil.getParameter("mobilephone");
    		}
    		TSCompany company = systemService.findUniqueByProperty(TSCompany.class, "mobilephone", checkVal);
    		if(null != company){
    			validateinfo.setInfo("电话号码已经存在");
    			validateinfo.setStatus("n");
    		}
    	}else if("userName".equals(type)){
    		if(StringUtil.isEmpty(checkVal)){
    			checkVal = ResourceUtil.getParameter("userName");
    		}
    		TSUser company = systemService.findUniqueByProperty(TSUser.class, "userName", checkVal);
    		if(null != company){
    			validateinfo.setInfo("登录帐号已经存在");
    			validateinfo.setStatus("n");
    		}
    	}else if("userNameClient".equals(type)){
    		if(StringUtil.isEmpty(checkVal)){
    			checkVal = ResourceUtil.getParameter("phonenum");
    		}
    		
    		TSCompany company = new TSCompany();
    		try {
    			company = companyService.getCompanyByDomain(request);

    		} catch (Exception e) {
    			 e.printStackTrace(); 
    		}
    		
    		TSUser user = null;
    		try {
    			 	CriteriaQuery cq = new CriteriaQuery(TSUser.class);
    			 		cq.eq("userName", checkVal);
    			 		cq.eq("company", company);
    			 		if(StringUtil.isNotEmpty(roletype)){
    			 			cq.eq("type", roletype);
    			 		}
    			 	user = (TSUser) systemService.getObjectByCriteriaQuery(cq, true);//公司是否已经存在角色

    		} catch (Exception e) {
    			 e.printStackTrace(); 
    		}
    		
    		if(  company == null  ){
    			validateinfo.setInfo("公司信息异常，请重新申请注册");
    			validateinfo.setStatus("n");
    		}else if(  company != null && user != null && user.getCompany().getId().equals(company.getId())   ){
    			validateinfo.setInfo("该手机号码已经存在");
    			validateinfo.setStatus("n");
    		}
    	}
    	
    	return validateinfo;
    }
    

}
