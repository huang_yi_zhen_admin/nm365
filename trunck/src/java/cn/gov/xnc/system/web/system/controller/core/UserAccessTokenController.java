package cn.gov.xnc.system.web.system.controller.core;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.gov.xnc.system.core.common.controller.BaseController;
import cn.gov.xnc.system.core.common.hibernate.qbc.CriteriaQuery;
import cn.gov.xnc.system.core.common.model.json.AjaxJson;
import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.constant.Globals;
import cn.gov.xnc.system.core.util.MyBeanUtils;
import cn.gov.xnc.system.core.util.StringUtil;
import cn.gov.xnc.system.tag.core.easyui.TagUtil;
import cn.gov.xnc.system.web.system.pojo.base.TSUserAccessTokenEntity;
import cn.gov.xnc.system.web.system.service.SystemService;
import cn.gov.xnc.system.web.system.service.UserAccessTokenServiceI;

/**   
 * @Title: Controller
 * @Description: 用户访问令牌
 * @author zero
 * @date 2017-06-13 19:54:37
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/userAccessTokenController")
public class UserAccessTokenController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserAccessTokenController.class);

	@Autowired
	private UserAccessTokenServiceI userAccessTokenService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 用户访问令牌列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "list")
	public ModelAndView userAccessToken(HttpServletRequest request) {
		return new ModelAndView("system/user/userAccessTokenList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSUserAccessTokenEntity userAccessToken,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUserAccessTokenEntity.class, dataGrid);
		//查询条件组装器
		cn.gov.xnc.system.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, userAccessToken, request.getParameterMap());
		this.userAccessTokenService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除用户访问令牌
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(TSUserAccessTokenEntity userAccessToken, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		userAccessToken = systemService.getEntity(TSUserAccessTokenEntity.class, userAccessToken.getId());
		message = "用户访问令牌删除成功";
		userAccessTokenService.delete(userAccessToken);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加用户访问令牌
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(TSUserAccessTokenEntity userAccessToken, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(userAccessToken.getId())) {
			message = "用户访问令牌更新成功";
			TSUserAccessTokenEntity t = userAccessTokenService.get(TSUserAccessTokenEntity.class, userAccessToken.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(userAccessToken, t);
				userAccessTokenService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "用户访问令牌更新失败";
			}
		} else {
			message = "用户访问令牌添加成功";
			userAccessTokenService.save(userAccessToken);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 用户访问令牌列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(TSUserAccessTokenEntity userAccessToken, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(userAccessToken.getId())) {
			userAccessToken = userAccessTokenService.getEntity(TSUserAccessTokenEntity.class, userAccessToken.getId());
			req.setAttribute("userAccessTokenPage", userAccessToken);
		}
		return new ModelAndView("system/user/userAccessToken");
	}
}
