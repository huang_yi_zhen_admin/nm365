package cn.gov.xnc.system.web.system.service;

/**
 * 字典类型静态变量集合
 * 命名规则：字典标志+ 配置的字典类型名称
 * @author Administrator
 */
public class TSDictionaryType {
	//出库类行
	public static final String DICT_TYPE_STOCK_IO_OUT = "DICT_TYPE_STOCK_IO_OUT";
	//入库类型
	public static final String DICT_TYPE_STOCK_IO_IN = "DICT_TYPE_STOCK_IO_IN";
}
