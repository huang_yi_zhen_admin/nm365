package cn.gov.xnc.system.excel.entity.vo;

/**
 * 基础常量
 */
public interface PoiBaseConstants {

    public static String  GET = "get";

    public static String  SET = "set";

    public static String  CONVERT_GET = "convertGet";

    public static String  CONVERT_SET = "convertSet";
}
