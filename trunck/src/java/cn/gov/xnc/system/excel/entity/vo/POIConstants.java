package cn.gov.xnc.system.excel.entity.vo;

/**
 * @Author ljz
 * 静态常量
 */
public interface POIConstants {
	/**
	 * 生成excel的序列号标识
	 */
	public final static String seqNum = "seq";
    /**
     * 单Sheet导出
     */
    public final static String EXCEL_VIEW = "excelView";
    /**
     * 数据列表
     */
    public final static String DATA_LIST = "data";
    /**
     * 多Sheet 对象
     */
    public final static String MAP_LIST = "mapList";
    /**
     * 注解对象
     */
    public final static String CLASS = "entity";
    /**
     *Excel表头对象
     */
    public final static String EXCEL_TITLE = "title";
    /**
     *下载文件名称
     */
    public final static String FILE_NAME = "fileName";
}
