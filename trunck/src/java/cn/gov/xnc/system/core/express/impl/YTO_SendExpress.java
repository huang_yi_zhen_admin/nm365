/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * @author fyy
 *
 */
public class YTO_SendExpress extends BaseSendExpress {
	
	//快递类型自己联系
	public final static int YTO_ExpType_0 = 0;//快递类型自己联系
	//快递类型  上门揽收
	public final static int YTO_ExpType_1 = 1;//快递类型 上门揽收
	//快递类型  次日达
	public final static int YTO_ExpType_2 = 2;//快递类型 次日达
	//快递类型  次晨达
	public final static int YTO_ExpType_4 = 4;//快递类型 次晨达
	//快递类型  当日达
	public final static int YTO_ExpType_8 = 8;//快递类型 当日达

	/**
	 * 
	 */
	public YTO_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.YTO);
	}

	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_0 = new HashMap<String,String>();
		exptype_0.put("exptype", "0");
		exptype_0.put("exptypename", "自己联系");
		results.add(exptype_0);
		
		HashMap<String,String> exptype_1 = new HashMap<String,String>();
		exptype_1.put("exptype", "1");
		exptype_1.put("exptypename", "上门揽收");
		results.add(exptype_1);
		
		HashMap<String,String> exptype_2 = new HashMap<String,String>();
		exptype_2.put("exptype", "2");
		exptype_2.put("exptypename", "次日达");
		results.add(exptype_2);
		
		HashMap<String,String> exptype_4 = new HashMap<String,String>();
		exptype_4.put("exptype", "4");
		exptype_4.put("exptypename", "次晨达");
		results.add(exptype_4);
		
		HashMap<String,String> exptype_8 = new HashMap<String,String>();
		exptype_8.put("exptype", "8");
		exptype_8.put("exptypename", "当日达");
		results.add(exptype_8);
		
		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(5);
		data.setPageNum(1);
		data.setShowNum(5);
		data.setResults(results);	
		
		return data;
	}

	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		if( StringUtil.isEmpty(getCustomerName()) ){
			errMsg.append("请输入商户代码");
			return false;
		}

		if( StringUtil.isEmpty(getMonthCode())){
			errMsg.append("请输入密钥串");
			return false;
		}
		return super.checkdata(errMsg);
	}

}
