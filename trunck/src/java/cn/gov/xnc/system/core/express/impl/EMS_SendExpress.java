/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * @author fyy
 *
 */
public class EMS_SendExpress extends BaseSendExpress {
	
	//快递类型  标准快递
	public final static int EMS_ExpType_1 = 1;//快递类型标准快递
	//快递类型  经济快递
	public final static int EMS_ExpType_4 = 4;//快递类型经济快递
	//快递类型  代收到付
	public final static int EMS_ExpType_8 = 8;//快递类型代收到付
	//快递类型  快递包裹
	public final static int EMS_ExpType_9 = 9;//快递类型快递包裹

	/**
	 * 
	 */
	public EMS_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.EMS);
	}
	
	
	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_1 = new HashMap<String,String>();
		exptype_1.put("exptype", "1");
		exptype_1.put("exptypename", "标准快递");
		results.add(exptype_1);
		
		HashMap<String,String> exptype_2 = new HashMap<String,String>();
		exptype_2.put("exptype", "4");
		exptype_2.put("exptypename", "经济快递");
		results.add(exptype_2);
		
		HashMap<String,String> exptype_3 = new HashMap<String,String>();
		exptype_3.put("exptype", "8");
		exptype_3.put("exptypename", "代收到付");
		results.add(exptype_3);
		
		HashMap<String,String> exptype_4 = new HashMap<String,String>();
		exptype_4.put("exptype", "9");
		exptype_4.put("exptypename", "快递包裹");
		results.add(exptype_4);

		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(4);
		data.setPageNum(1);
		data.setShowNum(4);
		data.setResults(results);	
		
		return data;
	}
	
	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		/*if( StringUtil.isEmpty(getCustomerName()) ){
			errMsg.append("请输入大客户号");
			return false;
		}
		if(StringUtil.isEmpty(getCustomerPwd())){
			errMsg.append("请输入商家密钥");
			return false;
		}*/
	
		return super.checkdata(errMsg);
	}

}
