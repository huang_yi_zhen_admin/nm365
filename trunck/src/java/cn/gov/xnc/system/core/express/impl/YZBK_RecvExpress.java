/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;

import java.util.List;

import cn.gov.xnc.system.core.express.BaseRecvExpress;
import cn.gov.xnc.system.core.express.Commodity;


/**
 * @author fyy
 *
 */
public class YZBK_RecvExpress extends BaseRecvExpress {

	/**
	 * 
	 */
	public YZBK_RecvExpress() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseRecvExpress#parseJson(java.lang.String)
	 */
	@Override
	public void parseJson(String json) {
		
		
		StringBuffer productinfo = new StringBuffer();
		List<Commodity> commoditylist = getBasesend().getCommodity();
		if( null != commoditylist && commoditylist.size()>0){
			for( int i = 0; i < commoditylist.size(); i++){
				productinfo.append("商品名称：");
				productinfo.append(commoditylist.get(i).getGoodsName());
				productinfo.append("<br>");
			}
		}

		json = json.replace("商家自定义区", productinfo.toString());
				
		// TODO Auto-generated method stub
		super.parseJson(json);
		super.setJsonstr(json);

	}


}
