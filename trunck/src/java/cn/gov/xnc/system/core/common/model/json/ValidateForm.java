package cn.gov.xnc.system.core.common.model.json;

import java.util.Map;

public class ValidateForm {
	private String status="y";// 是否成功
	private String info;// 提示信息
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
}
