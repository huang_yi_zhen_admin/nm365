/**
 * 
 */
package cn.gov.xnc.system.core.express;

/**
 * @author Administrator
 * 收件人信息
 */
public class Receiver {

	/*
	 * 收件人公司
	 */
	private String Company;//收件人公司
	
	/*
	 * 必填！收件人
	 */
	private String Name;//必填！收件人
	
	/*
	 * 必填！电话与手机，必填一个
	 */
	private String Tel;//必填！电话与手机，必填一个
	
	/*
	 * 必填！电话与手机，必填一个
	 */
	private String Mobile;//必填！电话与手机，必填一个
	
	/*
	 * 收件人邮编
	 */
	private String PostCode;//收件人邮编
	
	/*
	 * 必填！收件省（如广东省，不要缺少“省”）
	 */
	private String ProvinceName;//必填！收件省（如广东省，不要缺少“省”）
	
	/*
	 * 必填！收件市（如深圳市，不要缺少“市”）
	 */
	private String CityName;//必填！收件市（如深圳市，不要缺少“市”）
	
	/*
	 * 收件区（如福田区，不要缺少“区”或“县”）
	 */
	private String ExpAreaName;//收件区（如福田区，不要缺少“区”或“县”）
	
	/*
	 * 必填！收件人详细地址
	 */
	private String Address;//必填！收件人详细地址
	
	
	

	/**
	 * 收件人公司
	 * @return the company
	 */
	public String getCompany() {
		return Company;
	}

	/**
	 * 收件人公司
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		Company = company;
	}

	/**
	 * 必填！收件人
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * 必填！收件人
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @return the tel
	 */
	public String getTel() {
		return Tel;
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @param tel the tel to set
	 */
	public void setTel(String tel) {
		Tel = tel;
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @return the mobile
	 */
	public String getMobile() {
		return Mobile;
	}

	/**
	 * 必填！电话与手机，必填一个
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	/**
	 * 收件人邮编
	 * @return the postCode
	 */
	public String getPostCode() {
		return PostCode;
	}

	/**
	 * 收件人邮编
	 * @param postCode the postCode to set
	 */
	public void setPostCode(String postCode) {
		PostCode = postCode;
	}

	/**
	 * 必填！收件省（如广东省，不要缺少“省”）
	 * @return the provinceName
	 */
	public String getProvinceName() {
		return ProvinceName;
	}

	/**
	 * 必填！收件省（如广东省，不要缺少“省”）
	 * @param provinceName the provinceName to set
	 */
	public void setProvinceName(String provinceName) {
		ProvinceName = provinceName;
	}

	/**
	 * 必填！收件市（如深圳市，不要缺少“市”）
	 * @return the cityName
	 */
	public String getCityName() {
		return CityName;
	}

	/**
	 * 必填！收件市（如深圳市，不要缺少“市”）
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		CityName = cityName;
	}

	/**
	 * 收件区（如福田区，不要缺少“区”或“县”）
	 * @return the expAreaName
	 */
	public String getExpAreaName() {
		return ExpAreaName;
	}

	/**
	 * 收件区（如福田区，不要缺少“区”或“县”）
	 * @param expAreaName the expAreaName to set
	 */
	public void setExpAreaName(String expAreaName) {
		ExpAreaName = expAreaName;
	}

	/**
	 * 必填！收件人详细地址
	 * @return the address
	 */
	public String getAddress() {
		return Address;
	}

	/**
	 * 必填！收件人详细地址
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		Address = address;
	}
	
	
	
}
