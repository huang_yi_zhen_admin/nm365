/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;




import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;

import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * 顺丰电子面单
 * @author Administrator
 *
 */
public class SF_SendExpress extends BaseSendExpress {
	
	//快递类型  顺丰次日
	public final static int SF_ExpType_1 = 1;//快递类型  顺丰次日
	
	//快递类型 顺丰隔日
	public final static int SF_ExpType_2 = 2;//快递类型 顺丰隔日
	
	//快递类型  顺丰次晨
	public final static int SF_ExpType_5 = 5;//快递类型  顺丰次晨
	
	//快递类型  顺丰即日
	public final static int SF_ExpType_6 = 6;//快递类型  顺丰即日
	
	//快递类型  顺丰宝平邮
	public final static int SF_ExpType_9 = 9;//快递类型  顺丰宝平邮
	
	//快递类型 顺丰宝挂号
	public final static int SF_ExpType_10 = 10;//快递类型 顺丰宝挂号
	
	//快递类型  医药常温
	public final static int SF_ExpType_11 = 11;//快递类型  医药常温
	
	//快递类型 医药温控
	public final static int SF_ExpType_12 = 12;//快递类型 医药温控
	
	//快递类型  物流普运
	public final static int SF_ExpType_13 = 13;//快递类型  物流普运
	
	//快递类型  冷运宅配
	public final static int SF_ExpType_14 = 14;//快递类型  冷运宅配
	
	//快递类型  生鲜速配
	public final static int SF_ExpType_15 = 15;//快递类型  生鲜速配
	
	//快递类型 大闸蟹专递
	public final static int SF_ExpType_16 = 16;//快递类型 大闸蟹专递
	
	//快递类型 汽配专线
	public final static int SF_ExpType_17 = 17;//快递类型 汽配专线
	
	//快递类型  汽配吉运
	public final static int SF_ExpType_18 = 18;//快递类型  汽配吉运
	
	//快递类型 全球顺
	public final static int SF_ExpType_19 = 19;//快递类型 全球顺
	
	//快递类型  云仓专配次日
	public final static int SF_ExpType_37 = 37;//快递类型  云仓专配次日
	
	//快递类型  云仓专配隔日
	public final static int SF_ExpType_38 = 38;//快递类型  云仓专配隔日	

	/**
	 * 
	 */
	public SF_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.SF);
	}
	
	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_1 = new HashMap<String,String>();
		exptype_1.put("exptype", "1");
		exptype_1.put("exptypename", "顺丰次日");
		results.add(exptype_1);
		
		HashMap<String,String> exptype_2 = new HashMap<String,String>();
		exptype_2.put("exptype", "2");
		exptype_2.put("exptypename", "顺丰隔日");
		results.add(exptype_2);
		
		HashMap<String,String> exptype_5 = new HashMap<String,String>();
		exptype_5.put("exptype", "5");
		exptype_5.put("exptypename", "顺丰次晨");
		results.add(exptype_5);
		
		HashMap<String,String> exptype_6 = new HashMap<String,String>();
		exptype_6.put("exptype", "6");
		exptype_6.put("exptypename", "顺丰即日");
		results.add(exptype_6);
		
		HashMap<String,String> exptype_9 = new HashMap<String,String>();
		exptype_9.put("exptype", "9");
		exptype_9.put("exptypename", "顺丰宝平邮");
		results.add(exptype_9);
		
		HashMap<String,String> exptype_10 = new HashMap<String,String>();
		exptype_10.put("exptype", "10");
		exptype_10.put("exptypename", "顺丰宝挂号");
		results.add(exptype_10);
		
		
		HashMap<String,String> exptype_11 = new HashMap<String,String>();
		exptype_11.put("exptype", "11");
		exptype_11.put("exptypename", "医药常温");
		results.add(exptype_11);
		
		HashMap<String,String> exptype_12 = new HashMap<String,String>();
		exptype_12.put("exptype", "12");
		exptype_12.put("exptypename", "医药温控");
		results.add(exptype_12);
		
		HashMap<String,String> exptype_13 = new HashMap<String,String>();
		exptype_13.put("exptype", "13");
		exptype_13.put("exptypename", "物流普运");
		results.add(exptype_13);
		
		HashMap<String,String> exptype_14 = new HashMap<String,String>();
		exptype_14.put("exptype", "14");
		exptype_14.put("exptypename", "冷运宅配");
		results.add(exptype_14);
		
		HashMap<String,String> exptype_15 = new HashMap<String,String>();
		exptype_15.put("exptype", "15");
		exptype_15.put("exptypename", "生鲜速配");
		results.add(exptype_15);
		
		HashMap<String,String> exptype_16 = new HashMap<String,String>();
		exptype_16.put("exptype", "16");
		exptype_16.put("exptypename", "大闸蟹专递");
		results.add(exptype_16);
		
		HashMap<String,String> exptype_17 = new HashMap<String,String>();
		exptype_17.put("exptype", "17");
		exptype_17.put("exptypename", "汽配专线");
		results.add(exptype_17);
		
		HashMap<String,String> exptype_18 = new HashMap<String,String>();
		exptype_18.put("exptype", "18");
		exptype_18.put("exptypename", "汽配吉运");
		results.add(exptype_18);
		
		HashMap<String,String> exptype_19 = new HashMap<String,String>();
		exptype_19.put("exptype", "19");
		exptype_19.put("exptypename", "全球顺");
		results.add(exptype_19);
		
		HashMap<String,String> exptype_37 = new HashMap<String,String>();
		exptype_37.put("exptype", "37");
		exptype_37.put("exptypename", "云仓专配次日");
		results.add(exptype_37);
		
		HashMap<String,String> exptype_38 = new HashMap<String,String>();
		exptype_38.put("exptype", "38");
		exptype_38.put("exptypename", "云仓专配隔日");
		results.add(exptype_38);
		

		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(17);
		data.setPageNum(1);
		data.setShowNum(17);
		data.setResults(results);	
		
		return data;
	}
	


	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		/*if( StringUtil.isEmpty(getMonthCode()) ){
			errMsg.append("请输入月结编码！");
			return false;
		}*/
		return super.checkdata(errMsg);
	}
	

}
