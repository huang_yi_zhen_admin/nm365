package cn.gov.xnc.system.core.util;

import java.util.Comparator;

import cn.gov.xnc.system.web.system.pojo.base.TSFunction;

/**
* @ClassName: NumberComparator 
* @Description: TODO(字符串比较器) 
* @author hyzh 
* @date 2013-1-31 下午06:18:35 
*
 */
public class NumberComparator implements Comparator<Object> {
	
	public NumberComparator() {
	}

	public int compare(Object obj1, Object obj2) {

			TSFunction c1 = (TSFunction) obj1;
			TSFunction c2 = (TSFunction) obj2;
		
		if (obj1 != null && obj2 != null) {
			int i1 = c1.getFunctionOrder();
			int i2 = c2.getFunctionOrder();
			if (i1 == i2) {
				return 0;
			} else {
				return i1 - i2;
			}
			
		}	
		return 0;
	}


}
