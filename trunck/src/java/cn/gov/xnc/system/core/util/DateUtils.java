package cn.gov.xnc.system.core.util;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.util.StringUtils;

/**
 * 
 * 类描述：时间操作定义类
 * 
 * @author:  ljz
 * @date： 日期：2012-12-8 时间：下午12:15:03
 * @version 1.0
 */
public class DateUtils extends PropertyEditorSupport {
	// 各种时间格式
	public static final SimpleDateFormat date_sdf = new SimpleDateFormat("yyyy-MM-dd");
	// 各种时间格式
	public static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
	// 各种时间格式
	public static final SimpleDateFormat date_sdf_wz = new SimpleDateFormat("yyyy年MM月dd日");
	public static final SimpleDateFormat time_sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static final SimpleDateFormat yyyymmddhhmmss = new SimpleDateFormat("yyyyMMddHHmmss");
	public static final SimpleDateFormat short_time_sdf = new SimpleDateFormat("HH:mm");
	public static final  SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// 以毫秒表示的时间
	private static final long DAY_IN_MILLIS = 24 * 3600 * 1000;
	private static final long HOUR_IN_MILLIS = 3600 * 1000;
	private static final long MINUTE_IN_MILLIS = 60 * 1000;
	private static final long SECOND_IN_MILLIS = 1000;
	// 指定模式的时间格式
	private static SimpleDateFormat getSDFormat(String pattern) {
		return new SimpleDateFormat(pattern);
	}
	
	
	//用来全局控制 上一周，本周，下一周的周数变化   
    private  int weeks = 0;   
    private int MaxDate;//一月最大天数   
    private int MaxYear;//一年最大天数   
    
    
    public static void main(String[] args) throws ParseException {
        Calendar startCalendar = Calendar.getInstance();
        Calendar endCalendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy/M/d");
        Date startDate = df.parse("2012/3/1");
        startCalendar.setTime(startDate);
        Date endDate = df.parse("2012/3/5");
        endCalendar.setTime(endDate);
        
        SimpleDateFormat dfm = new SimpleDateFormat("dd");
        while(true){
            startCalendar.add(Calendar.DAY_OF_MONTH, 1);
            if(startCalendar.getTimeInMillis() < endCalendar.getTimeInMillis()){//TODO 转数组或是集合，楼主看着写吧
            System.out.println(dfm.format(startCalendar.getTime()));
        }else{
            break;
        }
        }
    }
       
//    public static void main(String[] args) {   
//  	  CalendarUtil CalendarUtil = new CalendarUtil();
//       
//  	  System.out.println(CalendarUtil.getBeforeNTime("yyyy-MM-dd",1));
//  	  
//    }  
    
//    /**  
//     * @param args  
//     */  
//    public static void main(String[] args) {   
//    	CalendarUtil tt = new CalendarUtil();   
//        System.out.println("获取当天日期:"+tt.getNowTime("yyyy-MM-dd"));   
//        System.out.println("获取前N天日期:"+tt.getBeforeNTime("yyyy-MM-dd", 4));   
//        System.out.println("获取后N天日期:"+tt.getAfterNTime("yyyy-MM-dd" , 2));   
//       
//        System.out.println("获取本周一日期:"+tt.getMondayOFWeek());   
//        System.out.println("获取本周日的日期~:"+tt.getCurrentWeekday());   
//        System.out.println("获取上周一日期:"+tt.getPreviousWeekday());   
//        System.out.println("获取上周日日期:"+tt.getPreviousWeekSunday());   
//        System.out.println("获取下周一日期:"+tt.getNextMonday());   
//        System.out.println("获取下周日日期:"+tt.getNextSunday());   
//        System.out.println("获得相应周的周六的日期:"+tt.getNowTime("yyyy-MM-dd"));   
//        System.out.println("获取本月第一天日期:"+tt.getFirstDayOfMonth());   
//        System.out.println("获取本月最后一天日期:"+tt.getDefaultDay());   
//        System.out.println("获取上月第一天日期:"+tt.getPreviousMonthFirst());   
//        System.out.println("获取上月最后一天的日期:"+tt.getPreviousMonthEnd());   
//        System.out.println("获取下月第一天日期:"+tt.getNextMonthFirst());   
//        System.out.println("获取下月最后一天日期:"+tt.getNextMonthEnd());   
//        System.out.println("获取本年的第一天日期:"+tt.getCurrentYearFirst());   
//        System.out.println("获取本年最后一天日期:"+tt.getCurrentYearEnd());   
//        System.out.println("获取去年的第一天日期:"+tt.getPreviousYearFirst());   
//        System.out.println("获取去年的最后一天日期:"+tt.getPreviousYearEnd());   
//        System.out.println("获取明年第一天日期:"+tt.getNextYearFirst());   
//        System.out.println("获取明年最后一天日期:"+tt.getNextYearEnd());   
//        System.out.println("获取本季度第一天到最后一天:"+tt.getThisSeasonTime(11));   
//        System.out.println("获取两个日期之间间隔天数2008-12-1~2008-9.29:"+CalendarUtil.getTwoDay("2015-01-01","2014-12-31"));   
//    }   
       
       
      /**  
        * 得到二个日期间的间隔天数  
        */  
    public static String getTwoDay(String sj1, String sj2) {   
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");   
        long day = 0;   
        try {   
         java.util.Date date = myFormatter.parse(sj1);   
         java.util.Date mydate = myFormatter.parse(sj2);   
         day = (date.getTime() - mydate.getTime()) / (24 * 60 * 60 * 1000);   
        } catch (Exception e) {   
         return "";   
        }   
        return day + "";   
    }   
  
  
    /**  
        * 根据一个日期，返回是星期几的字符串  
        *   
        * @param sdate  
        * @return  
        */  
    public static String getWeek(String sdate) {   
        // 再转换为时间   
        Date date = DateUtils.strToDate(sdate);   
        Calendar c = Calendar.getInstance();   
        c.setTime(date);   
        // int hour=c.get(Calendar.DAY_OF_WEEK);   
        // hour中存的就是星期几了，其范围 1~7   
        // 1=星期日 7=星期六，其他类推   
        return new SimpleDateFormat("EEEE").format(c.getTime());   
    }   
  
    /**  
        * 将短时间格式字符串转换为时间 yyyy-MM-dd   
        *   
        * @param strDate  
        * @return  
        */  
    public static Date strToDate(String strDate) {   
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");   
        ParsePosition pos = new ParsePosition(0);   
        Date strtodate = formatter.parse(strDate, pos);   
        return strtodate;   
    }   
  
    /**  
        * 两个时间之间的天数  
        *   
        * @param date1  
        * @param date2  
        * @return  
        */  
    public static long getDays(String date1, String date2) {   
        if (date1 == null || date1.equals(""))   
         return 0;   
        if (date2 == null || date2.equals(""))   
         return 0;   
        // 转换为标准时间   
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");   
        java.util.Date date = null;   
        java.util.Date mydate = null;   
        try {   
         date = myFormatter.parse(date1);   
         mydate = myFormatter.parse(date2);   
        } catch (Exception e) {   
        }   
        long day = (date.getTime() - mydate.getTime()) / (24 * 60 * 60 * 1000);   
        return day;   
    }   
  
  
  
       
    // 计算当月最后一天,返回字符串   
    public String getDefaultDay(){     
       String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
  
       Calendar lastDate = Calendar.getInstance();   
       lastDate.set(Calendar.DATE,1);//设为当前月的1号   
       lastDate.add(Calendar.MONTH,1);//加一个月，变为下月的1号   
       lastDate.add(Calendar.DATE,-1);//减去一天，变为当月最后一天   
          
       str=sdf.format(lastDate.getTime());   
       return str;     
    }   
       
    // 上月第一天   
    public String getPreviousMonthFirst(){     
       String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
       Calendar lastDate = Calendar.getInstance();   
       lastDate.set(Calendar.DATE,1);//设为当前月的1号   
       lastDate.add(Calendar.MONTH,-1);//减一个月，变为下月的1号   
       //lastDate.add(Calendar.DATE,-1);//减去一天，变为当月最后一天   
       str=sdf.format(lastDate.getTime());   
       return str;     
    }   
       
    //获取当月第一天   
    public static String getFirstDayOfMonth(){     
       String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
  
       Calendar lastDate = Calendar.getInstance();   
       lastDate.set(Calendar.DATE,1);//设为当前月的1号   
       str=sdf.format(lastDate.getTime());   
       return str;     
    }   
       
    // 获得本周星期日的日期     
    public String getCurrentWeekday() {   
        weeks = 0;   
        int mondayPlus = this.getMondayPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE, mondayPlus+6);   
        Date monday = currentDate.getTime();   
           
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preMonday = df.format(monday);   
        return preMonday;   
    }   
       
    /**  
     * 获前N天时间      
     */   
    public static String getBeforeNTime(String dateformat , int BeforeN ){   
    	
    	 SimpleDateFormat df=new java.text.SimpleDateFormat(dateformat);
    	 Calendar calendar=java.util.Calendar.getInstance();
    	    calendar.add(java.util.Calendar.DAY_OF_YEAR,-BeforeN);   
        
        
        return df.format(calendar.getTime());   
    }   
    
    /**  
     * 获后N天时间      
     */   
    public static String getAfterNTime(String dateformat , int After ){   
    	
    	 SimpleDateFormat df=new java.text.SimpleDateFormat(dateformat);
    	 Calendar calendar=java.util.Calendar.getInstance();
    	    calendar.add(java.util.Calendar.DAY_OF_YEAR,After);   
        
        
        return df.format(calendar.getTime());   
    }  
    
    /**  
     *  获取当天时间 
     */    
    public static String getNowTime(String dateformat){   
        Date   now   =   new   Date();      
        SimpleDateFormat   dateFormat   =   new   SimpleDateFormat(dateformat);//可以方便地修改日期格式      
        String  hehe  = dateFormat.format(now);      
        return hehe;   
    }
    // 获得当前日期与本周日相差的天数   
    private int getMondayPlus() {   
        Calendar cd = Calendar.getInstance();   
        // 获得今天是一周的第几天，星期日是第一天，星期二是第二天......   
        int dayOfWeek = cd.get(Calendar.DAY_OF_WEEK)-1;         //因为按中国礼拜一作为第一天所以这里减1   
        if (dayOfWeek == 1) {   
            return 0;   
        } else {   
            return 1 - dayOfWeek;   
        }   
    }   
       
    //获得本周一的日期   
    public String getMondayOFWeek(){   
         weeks = 0;   
         int mondayPlus = this.getMondayPlus();   
         GregorianCalendar currentDate = new GregorianCalendar();   
         currentDate.add(GregorianCalendar.DATE, mondayPlus);   
         Date monday = currentDate.getTime();   
            
         SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
         String preMonday = df.format(monday);   
         return preMonday;   
    }   
       
  //获得相应周的周六的日期   
    public String getSaturday() {   
        int mondayPlus = this.getMondayPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE, mondayPlus + 7 * weeks + 6);   
        Date monday = currentDate.getTime();   
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preMonday = df.format(monday);   
        return preMonday;   
    }   
       
 // 获得上周星期日的日期   
    public String getPreviousWeekSunday() {   
        weeks=0;   
        weeks--;   
        int mondayPlus = this.getMondayPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE, mondayPlus+weeks);   
        Date monday = currentDate.getTime();   
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        //SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preMonday = df.format(monday);   
        return preMonday;   
    }   
  
 // 获得上周星期一的日期   
    public String getPreviousWeekday() {   
        weeks--;   
        int mondayPlus = this.getMondayPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE, mondayPlus + 7 * weeks);   
        Date monday = currentDate.getTime();   
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        //SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preMonday = df.format(monday);   
        return preMonday;   
    }   
       
    // 获得下周星期一的日期   
    public String getNextMonday() {   
        weeks++;   
        int mondayPlus = this.getMondayPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE, mondayPlus + 7);   
        Date monday = currentDate.getTime();   
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preMonday = df.format(monday);   
        return preMonday;   
    }   
       
 // 获得下周星期日的日期   
    public String getNextSunday() {   
           
        int mondayPlus = this.getMondayPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE, mondayPlus + 7+6);   
        Date monday = currentDate.getTime();   
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preMonday = df.format(monday);   
        return preMonday;   
    }   
       
       
       
    private int getMonthPlus(){   
        Calendar cd = Calendar.getInstance();   
        int monthOfNumber = cd.get(Calendar.DAY_OF_MONTH);   
        cd.set(Calendar.DATE, 1);//把日期设置为当月第一天    
        cd.roll(Calendar.DATE, -1);//日期回滚一天，也就是最后一天    
        MaxDate=cd.get(Calendar.DATE);    
        if(monthOfNumber == 1){   
            return -MaxDate;   
        }else{   
            return 1-monthOfNumber;   
        }   
    }   
       
  //获得上月最后一天的日期   
    public static String getPreviousMonthEnd(){   
        String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
  
       Calendar lastDate = Calendar.getInstance();   
      lastDate.add(Calendar.MONTH,-1);//减一个月   
      lastDate.set(Calendar.DATE, 1);//把日期设置为当月第一天    
      lastDate.roll(Calendar.DATE, -1);//日期回滚一天，也就是本月最后一天    
       str=sdf.format(lastDate.getTime());   
       return str;     
    }   
       
  //获得下个月第一天的日期   
    public static String getNextMonthFirst(){   
        String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
  
       Calendar lastDate = Calendar.getInstance();   
      lastDate.add(Calendar.MONTH,1);//减一个月   
      lastDate.set(Calendar.DATE, 1);//把日期设置为当月第一天    
       str=sdf.format(lastDate.getTime());   
       return str;     
    }   
       
  //获得下个月最后一天的日期   
    public static String getNextMonthEnd(){   
        String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
  
       Calendar lastDate = Calendar.getInstance();   
      lastDate.add(Calendar.MONTH,1);//加一个月   
      lastDate.set(Calendar.DATE, 1);//把日期设置为当月第一天    
      lastDate.roll(Calendar.DATE, -1);//日期回滚一天，也就是本月最后一天    
       str=sdf.format(lastDate.getTime());   
       return str;     
    }   
       
    //获得明年最后一天的日期   
    public String getNextYearEnd(){   
        String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
  
      Calendar lastDate = Calendar.getInstance();   
      lastDate.add(Calendar.YEAR,1);//加一个年   
      lastDate.set(Calendar.DAY_OF_YEAR, 1);   
     lastDate.roll(Calendar.DAY_OF_YEAR, -1);   
       str=sdf.format(lastDate.getTime());   
       return str;     
    }   
       
  //获得明年第一天的日期   
    public String getNextYearFirst(){   
        String str = "";   
       SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");       
  
      Calendar lastDate = Calendar.getInstance();   
      lastDate.add(Calendar.YEAR,1);//加一个年   
      lastDate.set(Calendar.DAY_OF_YEAR, 1);   
           str=sdf.format(lastDate.getTime());   
      return str;     
           
    }   
       
  //获得本年有多少天   
    private int getMaxYear(){   
        Calendar cd = Calendar.getInstance();   
        cd.set(Calendar.DAY_OF_YEAR,1);//把日期设为当年第一天   
        cd.roll(Calendar.DAY_OF_YEAR,-1);//把日期回滚一天。   
        int MaxYear = cd.get(Calendar.DAY_OF_YEAR);    
        return MaxYear;   
    }   
       
    private int getYearPlus(){   
        Calendar cd = Calendar.getInstance();   
        int yearOfNumber = cd.get(Calendar.DAY_OF_YEAR);//获得当天是一年中的第几天   
        cd.set(Calendar.DAY_OF_YEAR,1);//把日期设为当年第一天   
        cd.roll(Calendar.DAY_OF_YEAR,-1);//把日期回滚一天。   
        int MaxYear = cd.get(Calendar.DAY_OF_YEAR);   
        if(yearOfNumber == 1){   
            return -MaxYear;   
        }else{   
            return 1-yearOfNumber;   
        }   
    }   
  //获得本年第一天的日期   
    public String getCurrentYearFirst(){   
        int yearPlus = this.getYearPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE,yearPlus);   
        Date yearDay = currentDate.getTime();   
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preYearDay = df.format(yearDay);   
        return preYearDay;   
    }   
       
       
  //获得本年最后一天的日期 *   
    public String getCurrentYearEnd(){   
        Date date = new Date();   
        SimpleDateFormat   dateFormat   =   new   SimpleDateFormat("yyyy");//可以方便地修改日期格式      
        String  years  = dateFormat.format(date);      
        return years+"-12-31";   
    }   
       
       
  //获得上年第一天的日期 *   
    public String getPreviousYearFirst(){   
        Date date = new Date();   
        SimpleDateFormat   dateFormat   =   new   SimpleDateFormat("yyyy");//可以方便地修改日期格式      
        String  years  = dateFormat.format(date); int years_value = Integer.parseInt(years);     
        years_value--;   
        return years_value+"-1-1";   
    }   
       
  //获得上年最后一天的日期   
    public String getPreviousYearEnd(){   
        weeks--;   
        int yearPlus = this.getYearPlus();   
        GregorianCalendar currentDate = new GregorianCalendar();   
        currentDate.add(GregorianCalendar.DATE,yearPlus+MaxYear*weeks+(MaxYear-1));   
        Date yearDay = currentDate.getTime();   
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");   
        String preYearDay = df.format(yearDay);   
        getThisSeasonTime(11);   
        return preYearDay;   
    }   
       
  //获得本季度   
    public String getThisSeasonTime(int month){   
        int array[][] = {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};   
        int season = 1;   
        if(month>=1&&month<=3){   
            season = 1;   
        }   
        if(month>=4&&month<=6){   
            season = 2;   
        }   
        if(month>=7&&month<=9){   
            season = 3;   
        }   
        if(month>=10&&month<=12){   
            season = 4;   
        }   
        int start_month = array[season-1][0];   
        int end_month = array[season-1][2];   
           
        Date date = new Date();   
        SimpleDateFormat   dateFormat   =   new   SimpleDateFormat("yyyy");//可以方便地修改日期格式      
        String  years  = dateFormat.format(date);      
        int years_value = Integer.parseInt(years);   
           
        int start_days =1;//years+"-"+String.valueOf(start_month)+"-1";//getLastDayOfMonth(years_value,start_month);   
        int end_days = getLastDayOfMonth(years_value,end_month);   
        String seasonDate = years_value+"-"+start_month+"-"+start_days+";"+years_value+"-"+end_month+"-"+end_days;   
        return seasonDate;   
           
    }   
       
    /**  
     * 获取某年某月的最后一天  
     * @param year 年  
     * @param month 月  
     * @return 最后一天  
     */  
   private int getLastDayOfMonth(int year, int month) {   
         if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8  
                   || month == 10 || month == 12) {   
               return 31;   
         }   
         if (month == 4 || month == 6 || month == 9 || month == 11) {   
               return 30;   
         }   
         if (month == 2) {   
               if (isLeapYear(year)) {   
                   return 29;   
               } else {   
                   return 28;   
               }   
         }   
         return 0;   
   }   
   /**  
    * 是否闰年  
    * @param year 年  
    * @return   
    */  
  public boolean isLeapYear(int year) {   
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);   
  }   
  
  
 
	
	
	/**
	 * 当前日历，这里用中国时间表示
	 * 
	 * @return 以当地时区表示的系统当前日历
	 */
	public static Calendar getCalendar() {
		return Calendar.getInstance();
	}

	/**
	 * 指定毫秒数表示的日历
	 * 
	 * @param millis
	 *            毫秒数
	 * @return 指定毫秒数表示的日历
	 */
	public static Calendar getCalendar(long millis) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(millis));
		return cal;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// getDate
	// 各种方式获取的Date
	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * 当前日期
	 * 
	 * @return 系统当前时间
	 */
	public static Date getDate() {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Date d = null;
		try {
			d = sf.parse(sf.format(Calendar.getInstance().getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		 return d;
	}
	
	
	/**
	 * 当前日期
	 * 
	 * @return 系统当前时间
	 */
	public static Date getDateFsrf(String srf ) {
		SimpleDateFormat sf = new SimpleDateFormat(srf);
		 Date d = null;
		try {
			d = sf.parse(sf.format(Calendar.getInstance().getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		 return d;
	}
	
	

	/**
	 * 指定毫秒数表示的日期
	 * 
	 * @param millis
	 *            毫秒数
	 * @return 指定毫秒数表示的日期
	 */
	public static Date getDate(long millis) {
		return new Date(millis);
	}

	/**
	 * 时间戳转换为字符串
	 * 
	 * @param time
	 * @return
	 */
	public static String timestamptoStr(Timestamp time) {
		Date date = null;
		if (null != time) {
			date = new Date(time.getTime());
		}
		return date2Str(date_sdf);
	}

	/**
	 * 字符串转换时间戳
	 * 
	 * @param str
	 * @return
	 */
	public static Timestamp str2Timestamp(String str) {
		Date date = str2Date(str, date_sdf);
		return new Timestamp(date.getTime());
	}
	/**
	 * 字符串转换成日期
	 * @param str
	 * @param sdf
	 * @return
	 */
	public static Date str2Date(String str, SimpleDateFormat sdf) {
		if (null == str || "".equals(str)) {
			return null;
		}
		Date date = null;
		try {
			date = sdf.parse(str);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 日期转换为字符串
	 * 
	 * @param date
	 *            日期
	 * @param format
	 *            日期格式
	 * @return 字符串
	 */
	public static String date2Str(SimpleDateFormat date_sdf) {
		Date date=getDate();
		if (null == date) {
			return null;
		}
		return date_sdf.format(date);
	}
	/**
	 * 格式化时间
	 * @param data
	 * @param format
	 * @return
	 */
	public static String dataformat(String data,String format)
	{
		SimpleDateFormat sformat = new SimpleDateFormat(format);
		Date date=null;
		try {
			 date=sformat.parse(data);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sformat.format(date);
	}
	/**
	 * 日期转换为字符串
	 * 
	 * @param date
	 *            日期
	 * @param format
	 *            日期格式
	 * @return 字符串
	 */
	public static String date2Str(Date date, SimpleDateFormat date_sdf) {
		if (null == date) {
			return null;
		}
		return date_sdf.format(date);
	}
	
	
	/**
	 * 日期转换为字符串
	 * 
	 * @param date
	 *            日期
	 * @param format
	 *            日期格式
	 * @return 字符串
	 */
	public static String date3Str(Date date, String format) {
		
		if( StringUtil.isEmpty(format) ){
			format ="yyyy-MM-dd HH:mm:ss";
		}
		
		
		SimpleDateFormat sformat = new SimpleDateFormat(format);
		
		if (null == date) {
			return null;
		}
		return sformat.format(date);
	}
	
	
	/**
	 * 日期转换为字符串
	 * 
	 * @param date
	 *            日期
	 * @param format
	 *            日期格式
	 * @return 字符串
	 */
	public static String getDate(String format) {
		Date date=new Date();
		if (null == date) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}


	/**
	 * 指定毫秒数的时间戳
	 * 
	 * @param millis
	 *            毫秒数
	 * @return 指定毫秒数的时间戳
	 */
	public static Timestamp getTimestamp(long millis) {
		return new Timestamp(millis);
	}

	/**
	 * 以字符形式表示的时间戳
	 * 
	 * @param time
	 *            毫秒数
	 * @return 以字符形式表示的时间戳
	 */
	public static Timestamp getTimestamp(String time) {
		return new Timestamp(Long.parseLong(time));
	}

	/**
	 * 系统当前的时间戳
	 * 
	 * @return 系统当前的时间戳
	 */
	public static Timestamp getTimestamp() {
		return new Timestamp(new Date().getTime());
	}

	/**
	 * 指定日期的时间戳
	 * 
	 * @param date
	 *            指定日期
	 * @return 指定日期的时间戳
	 */
	public static Timestamp getTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

	/**
	 * 指定日历的时间戳
	 * 
	 * @param cal
	 *            指定日历
	 * @return 指定日历的时间戳
	 */
	public static Timestamp getCalendarTimestamp(Calendar cal) {
		return new Timestamp(cal.getTime().getTime());
	}

	public static Timestamp gettimestamp() {
		Date dt = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String nowTime = df.format(dt);
		java.sql.Timestamp buydate = java.sql.Timestamp.valueOf(nowTime);
		return buydate;
	}

	// ////////////////////////////////////////////////////////////////////////////
	// getMillis
	// 各种方式获取的Millis
	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * 系统时间的毫秒数
	 * 
	 * @return 系统时间的毫秒数
	 */
	public static long getMillis() {
		return new Date().getTime();
	}

	/**
	 * 指定日历的毫秒数
	 * 
	 * @param cal
	 *            指定日历
	 * @return 指定日历的毫秒数
	 */
	public static long getMillis(Calendar cal) {
		return cal.getTime().getTime();
	}

	/**
	 * 指定日期的毫秒数
	 * 
	 * @param date
	 *            指定日期
	 * @return 指定日期的毫秒数
	 */
	public static long getMillis(Date date) {
		return date.getTime();
	}

	/**
	 * 指定时间戳的毫秒数
	 * 
	 * @param ts
	 *            指定时间戳
	 * @return 指定时间戳的毫秒数
	 */
	public static long getMillis(Timestamp ts) {
		return ts.getTime();
	}

	// ////////////////////////////////////////////////////////////////////////////
	// formatDate
	// 将日期按照一定的格式转化为字符串
	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * 默认方式表示的系统当前日期，具体格式：年-月-日
	 * 
	 * @return 默认日期按“年-月-日“格式显示
	 */
	public static String formatDate() {
		return date_sdf.format(getCalendar().getTime());
	}
	/**
	 * 获取时间字符串
	 */
	public static String getDataString(SimpleDateFormat formatstr) {
		return formatstr.format(getCalendar().getTime());
	}
	/**
	 * 指定日期的默认显示，具体格式：年-月-日
	 * 
	 * @param cal
	 *            指定的日期
	 * @return 指定日期按“年-月-日“格式显示
	 */
	public static String formatDate(Calendar cal) {
		return date_sdf.format(cal.getTime());
	}

	/**
	 * 指定日期的默认显示，具体格式：年-月-日
	 * 
	 * @param date
	 *            指定的日期
	 * @return 指定日期按“年-月-日“格式显示
	 */
	public static String formatDate(Date date) {
		return date_sdf.format(date);
	}

	/**
	 * 指定毫秒数表示日期的默认显示，具体格式：年-月-日
	 * 
	 * @param millis
	 *            指定的毫秒数
	 * @return 指定毫秒数表示日期按“年-月-日“格式显示
	 */
	public static String formatDate(long millis) {
		return date_sdf.format(new Date(millis));
	}

	/**
	 * 默认日期按指定格式显示
	 * 
	 * @param pattern
	 *            指定的格式
	 * @return 默认日期按指定格式显示
	 */
	public static String formatDate(String pattern) {
		return getSDFormat(pattern).format(getCalendar().getTime());
	}

	/**
	 * 指定日期按指定格式显示
	 * 
	 * @param cal
	 *            指定的日期
	 * @param pattern
	 *            指定的格式
	 * @return 指定日期按指定格式显示
	 */
	public static String formatDate(Calendar cal, String pattern) {
		return getSDFormat(pattern).format(cal.getTime());
	}

	/**
	 * 指定日期按指定格式显示
	 * 
	 * @param date
	 *            指定的日期
	 * @param pattern
	 *            指定的格式
	 * @return 指定日期按指定格式显示
	 */
	public static String formatDate(Date date, String pattern) {
		return getSDFormat(pattern).format(date);
	}

	// ////////////////////////////////////////////////////////////////////////////
	// formatTime
	// 将日期按照一定的格式转化为字符串
	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * 默认方式表示的系统当前日期，具体格式：年-月-日 时：分
	 * 
	 * @return 默认日期按“年-月-日 时：分“格式显示
	 */
	public static String formatTime() {
		return time_sdf.format(getCalendar().getTime());
	}

	/**
	 * 指定毫秒数表示日期的默认显示，具体格式：年-月-日 时：分
	 * 
	 * @param millis
	 *            指定的毫秒数
	 * @return 指定毫秒数表示日期按“年-月-日 时：分“格式显示
	 */
	public static String formatTime(long millis) {
		return time_sdf.format(new Date(millis));
	}

	/**
	 * 指定日期的默认显示，具体格式：年-月-日 时：分
	 * 
	 * @param cal
	 *            指定的日期
	 * @return 指定日期按“年-月-日 时：分“格式显示
	 */
	public static String formatTime(Calendar cal) {
		return time_sdf.format(cal.getTime());
	}

	/**
	 * 指定日期的默认显示，具体格式：年-月-日 时：分
	 * 
	 * @param date
	 *            指定的日期
	 * @return 指定日期按“年-月-日 时：分“格式显示
	 */
	public static String formatTime(Date date) {
		return time_sdf.format(date);
	}

	// ////////////////////////////////////////////////////////////////////////////
	// formatShortTime
	// 将日期按照一定的格式转化为字符串
	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * 默认方式表示的系统当前日期，具体格式：时：分
	 * 
	 * @return 默认日期按“时：分“格式显示
	 */
	public static String formatShortTime() {
		return short_time_sdf.format(getCalendar().getTime());
	}

	/**
	 * 指定毫秒数表示日期的默认显示，具体格式：时：分
	 * 
	 * @param millis
	 *            指定的毫秒数
	 * @return 指定毫秒数表示日期按“时：分“格式显示
	 */
	public static String formatShortTime(long millis) {
		return short_time_sdf.format(new Date(millis));
	}

	/**
	 * 指定日期的默认显示，具体格式：时：分
	 * 
	 * @param cal
	 *            指定的日期
	 * @return 指定日期按“时：分“格式显示
	 */
	public static String formatShortTime(Calendar cal) {
		return short_time_sdf.format(cal.getTime());
	}

	/**
	 * 指定日期的默认显示，具体格式：时：分
	 * 
	 * @param date
	 *            指定的日期
	 * @return 指定日期按“时：分“格式显示
	 */
	public static String formatShortTime(Date date) {
		return short_time_sdf.format(date);
	}

	// ////////////////////////////////////////////////////////////////////////////
	// parseDate
	// parseCalendar
	// parseTimestamp
	// 将字符串按照一定的格式转化为日期或时间
	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * 根据指定的格式将字符串转换成Date 如输入：2003-11-19 11:20:20将按照这个转成时间
	 * 
	 * @param src
	 *            将要转换的原始字符窜
	 * @param pattern
	 *            转换的匹配格式
	 * @return 如果转换成功则返回转换后的日期
	 * @throws ParseException
	 * @throws AIDateFormatException
	 */
	public static Date parseDate(String src, String pattern)
			throws ParseException {
		return getSDFormat(pattern).parse(src);

	}

	/**
	 * 根据指定的格式将字符串转换成Date 如输入：2003-11-19 11:20:20将按照这个转成时间
	 * 
	 * @param src
	 *            将要转换的原始字符窜
	 * @param pattern
	 *            转换的匹配格式
	 * @return 如果转换成功则返回转换后的日期
	 * @throws ParseException
	 * @throws AIDateFormatException
	 */
	public static Calendar parseCalendar(String src, String pattern)
			throws ParseException {

		Date date = parseDate(src, pattern);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	public static String formatAddDate(String src, String pattern, int amount)
			throws ParseException {
		Calendar cal;
		cal = parseCalendar(src, pattern);
		cal.add(Calendar.DATE, amount);
		return formatDate(cal);
	}

	/**
	 * 根据指定的格式将字符串转换成Date 如输入：2003-11-19 11:20:20将按照这个转成时间
	 * 
	 * @param src
	 *            将要转换的原始字符窜
	 * @param pattern
	 *            转换的匹配格式
	 * @return 如果转换成功则返回转换后的时间戳
	 * @throws ParseException
	 * @throws AIDateFormatException
	 */
	public static Timestamp parseTimestamp(String src, String pattern)
			throws ParseException {
		Date date = parseDate(src, pattern);
		return new Timestamp(date.getTime());
	}

	// ////////////////////////////////////////////////////////////////////////////
	// dateDiff
	// 计算两个日期之间的差值
	// ////////////////////////////////////////////////////////////////////////////

	/**
	 * 计算两个时间之间的差值，根据标志的不同而不同
	 * 
	 * @param flag
	 *            计算标志，表示按照年/月/日/时/分/秒等计算
	 * @param calSrc
	 *            减数
	 * @param calDes
	 *            被减数
	 * @return 两个日期之间的差值
	 */
	public static int dateDiff(char flag, Calendar calSrc, Calendar calDes) {

		long millisDiff = getMillis(calSrc) - getMillis(calDes);

		if (flag == 'y') {
			return (calSrc.get(calSrc.YEAR) - calDes.get(calDes.YEAR));
		}

		if (flag == 'd') {
			return (int) (millisDiff / DAY_IN_MILLIS);
		}

		if (flag == 'h') {
			return (int) (millisDiff / HOUR_IN_MILLIS);
		}

		if (flag == 'm') {
			return (int) (millisDiff / MINUTE_IN_MILLIS);
		}

		if (flag == 's') {
			return (int) (millisDiff / SECOND_IN_MILLIS);
		}

		return 0;
	}
    /**
     * String类型 转换为Date,
     * 如果参数长度为10 转换格式”yyyy-MM-dd“
     *如果参数长度为19 转换格式”yyyy-MM-dd HH:mm:ss“
     * * @param text
	 *             String类型的时间值
     */
	public void setAsText(String text) throws IllegalArgumentException {
		if (StringUtils.hasText(text)) {
			try {
				if (text.indexOf(":") == -1 && text.length() == 10) {
					setValue(this.date_sdf.parse(text));
				} else if (text.indexOf(":") > 0 && text.length() == 19) {
					setValue(this.datetimeFormat.parse(text));
				} else {
					throw new IllegalArgumentException(
							"Could not parse date, date format is error ");
				}
			} catch (ParseException ex) {
				IllegalArgumentException iae = new IllegalArgumentException(
						"Could not parse date: " + ex.getMessage());
				iae.initCause(ex);
				throw iae;
			}
		} else {
			setValue(null);
		}
	}
	public static int getYear(){
	    GregorianCalendar calendar=new GregorianCalendar();
	    calendar.setTime(getDate());
	    return calendar.get(Calendar.YEAR);
	  }
	
	
	/** 
     * 获取当月的 天数 
     * */  
    public static int getCurrentMonthDay() {  
          
        Calendar a = Calendar.getInstance();  
        a.set(Calendar.DATE, 1);  
        a.roll(Calendar.DATE, -1);  
        int maxDate = a.get(Calendar.DATE);  
        return maxDate;  
    }  
  
    /** 
     * 根据年 月 获取对应的月份 天数 
     * */  
    public static int getDaysByYearMonth(String strDate, String SimpleDateFormat) {  
          
//        Calendar a = Calendar.getInstance();  
//        a.set(Calendar.YEAR, year);  
//        a.set(Calendar.MONTH, month - 1);  
//        a.set(Calendar.DATE, 1);  
//        a.roll(Calendar.DATE, -1);  
        int maxDate = 0;  
        
    	try {
    			//strDate = "2012-02"; 
    	        SimpleDateFormat sdf = new SimpleDateFormat(SimpleDateFormat); 
    	        Calendar calendar = new GregorianCalendar(); 
    	        Date date1 = sdf.parse(strDate); 
    	        calendar.setTime(date1); //放入你的日期 
    	        maxDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    	} catch (ParseException e) {
    		maxDate= 0;
		} 
    	
       
        
        
        return maxDate;  
    }  
      
    /** 
     * 根据日期 找到对应日期的 星期 
     */  
    public static String getDayOfWeekByDate(String date) {  
        String dayOfweek = "-1";  
        try {  
            SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");  
            Date myDate = myFormatter.parse(date);  
            SimpleDateFormat formatter = new SimpleDateFormat("E");  
            String str = formatter.format(myDate);  
            dayOfweek = str;  
              
        } catch (Exception e) {  
            System.out.println("错误!");  
        }  
        return dayOfweek;  
    }  
    
    /**  
     * 计算两个日期之间相差的天数  
     * @param sdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date sdate,Date bdate) throws ParseException    
    {    
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        sdate=sdf.parse(sdf.format(sdate));  
        bdate=sdf.parse(sdf.format(bdate));  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(sdate);    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
            
       return Integer.parseInt(String.valueOf(between_days));           
    }    

}