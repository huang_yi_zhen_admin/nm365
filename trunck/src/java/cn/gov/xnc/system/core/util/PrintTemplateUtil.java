package cn.gov.xnc.system.core.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class PrintTemplateUtil {
	
	public final static String TEMPLATE_SALESLIST = "print/saleslist.html";
	public final static String TEMPLATE_PREPAREGOODS = "print/prepareGoods.html";
	public final static String TEMPLATE_ORDERDETAILS = "print/orderdetails.html";
	public final static String TEMPLATE_ORDERDETAILS_BD = "print/orderdetail-bd.html";

	public PrintTemplateUtil() {
		// TODO Auto-generated constructor stub
		String path = "";
		try {
			path = RootUtil.getRootDir()+File.separator;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		templateMap = new HashMap<String,StringBuffer>();
		StringBuffer saleslist = getSB(path + TEMPLATE_SALESLIST);
		if( saleslist.length() > 0 ){
			templateMap.put(TEMPLATE_SALESLIST, saleslist);
		}
		
		StringBuffer prepareGoods = getSB(path + TEMPLATE_PREPAREGOODS);
		if( prepareGoods.length() > 0 ){
			templateMap.put(TEMPLATE_PREPAREGOODS, prepareGoods);
		}
		
	}
	
	public static PrintTemplateUtil getInstance(){
		if( null == instance ){
			instance = new PrintTemplateUtil();
		}
		
		return instance;
	}
	
	public StringBuffer getTemplate(String filename){
		
		
		//测试专用
		String path = "";
		try {
			path = RootUtil.getRootDir()+File.separator;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuffer saleslist = getSB(path + TEMPLATE_SALESLIST);
		if( saleslist.length() > 0 ){
			templateMap.put(TEMPLATE_SALESLIST, saleslist);
		}
		StringBuffer prepareGoods = getSB(path + TEMPLATE_PREPAREGOODS);
		if( prepareGoods.length() > 0 ){
			templateMap.put(TEMPLATE_PREPAREGOODS, prepareGoods);
		}
		StringBuffer orderdetails = getSB(path + TEMPLATE_ORDERDETAILS);
		if( saleslist.length() > 0 ){
			templateMap.put(TEMPLATE_ORDERDETAILS, orderdetails);
		}
		StringBuffer orderdetails_bd = getSB(path + TEMPLATE_ORDERDETAILS_BD);
		if( prepareGoods.length() > 0 ){
			templateMap.put(TEMPLATE_ORDERDETAILS_BD, orderdetails_bd);
		}
		
		
		return templateMap.get(filename);
	}
	
	private Map<String,StringBuffer> templateMap;
	
	private static PrintTemplateUtil instance ;
	
	public static StringBuffer getSB(String filePath) {  
        StringBuffer sb = new StringBuffer();  
        InputStreamReader isr = null;
        FileInputStream fis = null;
        BufferedReader br = null;  
        try {  
        	fis = new FileInputStream(filePath); 
        	isr = new InputStreamReader(fis, "UTF-8"); 
            br = new BufferedReader(isr); 
            String line = null; 
            while ((line = br.readLine()) != null) { 
                sb.append(line);
            } 
            
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            try {   
            	isr.close();
            	fis.close();
                br.close();  
            } catch (Exception e) {  
                e.printStackTrace();  
            }  
        }  
        return sb;  
    }  

}
