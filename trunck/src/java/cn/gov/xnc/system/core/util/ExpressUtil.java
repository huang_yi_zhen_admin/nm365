/**
 * 
 */
package cn.gov.xnc.system.core.util;

import java.util.HashMap;
import java.io.BufferedReader;
import java.io.IOException; 
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import cn.gov.xnc.system.core.express.ExpressCompany;

import java.security.MessageDigest; 
/**
 * @author Administrator
 *
 */
public class ExpressUtil {

		//电商ID
		public static final String EBusinessID="1280336";	
		//测试的电商ID
		public static final String TEST_EBusinessID="test1280336";
		//电商加密私钥，快递鸟提供，注意保管，不要泄漏
		public static final String AppKey="b527d2ff-a0f5-4022-aac5-b251e1a0228e";	
		//请求url, 正式环境地址：http://api.kdniao.cc/api/Eorderservice    测试环境地址：http://testapi.kdniao.cc:8081/api/EOrderService
		public static final String ReqURL="http://api.kdniao.com/api/Eorderservice";	
		//测试地址—申请电子面单
		public static final String TEST_REQ_URL = "http://sandboxapi.kdniao.com:8080/kdniaosandbox/gateway/exterfaceInvoke.json";
		//public static final String ReqURL="http://testapi.kdniao.cc:8081/api/EOrderService";	
		
		//电子面但请求指令类型：1007
		public static final String RequestType = "1007";
		
		public static final String DataType = "2";
		
		public static String getExpTypeName(String shippercode, int exptype){
			
			if( shippercode.equals(ExpressCompany.JD) ){
				switch(exptype){
					case 1:{
						return "订单来源：京东商城";
					}
					case 2:{
						return "订单来源：天猫";
					}
					case 3:{
						return "订单来源：苏宁";
					}
					case 4:{
						return "订单来源：亚马逊中国";
					}
					case 5:{
						return "订单来源：ChinaSkin";
					}
					case 6:{
						return "订单来源：其他销售平台";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.DBL) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					case 2:{
						return "360特惠件";
					}
					case 3:{
						return "电商尊享";
					}
					case 4:{
						return "特准快件";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.EMS) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					case 4:{
						return "经济快递";
					}
					case 8:{
						return "代收到付";
					}
					case 9:{
						return "快递包裹";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.FAST) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.HTKY) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.QFKD) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.SF) ){
				switch(exptype){
					case 1:{
						return "顺丰次日";
					}
					case 2:{
						return "顺丰隔日";
					}
					case 5:{
						return "顺丰次晨";
					}
					case 6:{
						return "顺丰即日";
					}
					case 9:{
						return "顺丰宝平邮";
					}
					case 10:{
						return "顺丰宝挂号";
					}
					case 11:{
						return "医药常温";
					}
					case 12:{
						return "医药温控";
					}
					case 13:{
						return "物流普运";
					}
					case 14:{
						return "冷运宅配";
					}
					case 15:{
						return "生鲜速配";
					}
					case 16:{
						return "大闸蟹专递";
					}
					case 17:{
						return "汽配专线";
					}
					case 18:{
						return "汽配吉运";
					}
					case 19:{
						return "全球顺";
					}
					case 37:{
						return "云仓专配次日";
					}
					case 38:{
						return "云仓专配隔日";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.STO) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.UC) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.YD) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.YTO) ){
				switch(exptype){
					case 0:{
						return "自己联系";
					}
					case 1:{
						return "上门揽收";
					}
					case 2:{
						return "次日达";
					}
					case 4:{
						return "次晨达";
					}
					case 8:{
						return "当日达";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.ZJS) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			if( shippercode.equals(ExpressCompany.YZBK) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			if( shippercode.equals(ExpressCompany.YZPY) ){
				switch(exptype){
					case 1:{
						return "标准快递";
					}
					default:{
						return "";
					}
				}
			}
			
			if( shippercode.equals(ExpressCompany.ZTO) ){
				switch(exptype){
					case 1:{
						return "普通订单";
					}
					case 2:{
						return "线下订单";
					}
					case 3:{
						return "COD订单";
					}
					case 4:{
						return "限时物流";
					}
					case 5:{
						return "快递保障订单";
					}
					default:{
						return "";
					}
				}
			}
		
			return null;
		}

		/**
	     * Json方式 电子面单
		 * @throws Exception 
	     */
		public String orderOnlineByJson() throws Exception{
			String requestData= "{'OrderCode': '012657700387'," +
	                "'ShipperCode':'SF'," +
	                "'PayType':1," +
	                "'ExpType':1," +
	                "'Cost':1.0," +
	                "'OtherCost':1.0," +
	                "'Sender':" +
	                "{" +
	                "'Company':'LV','Name':'Taylor','Mobile':'15018442396','ProvinceName':'上海','CityName':'上海','ExpAreaName':'青浦区','Address':'明珠路73号'}," +
	                "'Receiver':" +
	                "{" +
	                "'Company':'GCCUI','Name':'Yann','Mobile':'15018442396','ProvinceName':'北京','CityName':'北京','ExpAreaName':'朝阳区','Address':'三里屯街道雅秀大厦'}," +
	                "'Commodity':" +
	                "[{" +
	                "'GoodsName':'鞋子','Goodsquantity':1,'GoodsWeight':1.0}]," +
	                "'Weight':1.0," +
	                "'Quantity':1," +
	                "'Volume':0.0," +
	                "'Remark':'小心轻放'," +
	                "'IsReturnPrintTemplate':1}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("RequestData", urlEncoder(requestData, "UTF-8"));
			params.put("EBusinessID", EBusinessID);
			params.put("RequestType", RequestType);
			String dataSign=encrypt(requestData, AppKey, "UTF-8");
			params.put("DataSign", urlEncoder(dataSign, "UTF-8"));
			params.put("DataType", DataType);
			
			String result=sendPost(ReqURL, params);	
			
			//根据公司业务处理返回的信息......
			
			return result;
		}
		/**
	     * MD5加密
	     * @param str 内容       
	     * @param charset 编码方式
		 * @throws Exception 
	     */
		@SuppressWarnings("unused")
		public static String MD5(String str, String charset) throws Exception {
		    MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(str.getBytes(charset));
		    byte[] result = md.digest();
		    StringBuffer sb = new StringBuffer(32);
		    for (int i = 0; i < result.length; i++) {
		        int val = result[i] & 0xff;
		        if (val <= 0xf) {
		            sb.append("0");
		        }
		        sb.append(Integer.toHexString(val));
		    }
		    return sb.toString().toLowerCase();
		}
		
		/**
	     * base64编码
	     * @param str 内容       
	     * @param charset 编码方式
		 * @throws UnsupportedEncodingException 
	     */
		public static String base64(String str, String charset) throws UnsupportedEncodingException{
			String encoded = Base64.encode(str.getBytes(charset));
			return encoded;    
		}	
		
		@SuppressWarnings("unused")
		public static String urlEncoder(String str, String charset) throws UnsupportedEncodingException{
			String result = URLEncoder.encode(str, charset);
			return result;
		}
		
		/**
	     * 电商Sign签名生成
	     * @param content 内容   
	     * @param keyValue Appkey  
	     * @param charset 编码方式
		 * @throws UnsupportedEncodingException ,Exception
		 * @return DataSign签名
	     */
		@SuppressWarnings("unused")
		public static String encrypt (String content, String keyValue, String charset) throws UnsupportedEncodingException, Exception
		{
			if (keyValue != null)
			{
				return base64(MD5(content + keyValue, charset), charset);
			}
			return base64(MD5(content, charset), charset);
		}
		
		 /**
	     * 向指定 URL 发送POST方法的请求     
	     * @param url 发送请求的 URL    
	     * @param params 请求的参数集合     
	     * @return 远程资源的响应结果
	     */
		@SuppressWarnings("unused")
		public static String sendPost(String url, Map<String, String> params) {
	        OutputStreamWriter out = null;
	        BufferedReader in = null;        
	        StringBuilder result = new StringBuilder(); 
	        try {
	            URL realUrl = new URL(url);
	            HttpURLConnection conn =(HttpURLConnection) realUrl.openConnection();
	            // 发送POST请求必须设置如下两行
	            conn.setDoOutput(true);
	            conn.setDoInput(true);
	            // POST方法
	            conn.setRequestMethod("POST");
	            // 设置通用的请求属性
	            conn.setRequestProperty("accept", "*/*");
	            conn.setRequestProperty("connection", "Keep-Alive");
	            conn.setRequestProperty("user-agent",
	                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	            conn.connect();
	            // 获取URLConnection对象对应的输出流
	            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
	            // 发送请求参数            
	            if (params != null) {
			          StringBuilder param = new StringBuilder(); 
			          for (Map.Entry<String, String> entry : params.entrySet()) {
			        	  if(param.length()>0){
			        		  param.append("&");
			        	  }	        	  
			        	  param.append(entry.getKey());
			        	  param.append("=");
			        	  param.append(entry.getValue());		        	  
			        	  System.out.println(entry.getKey()+":"+entry.getValue());
			          }
			          System.out.println("param:"+param.toString());
			          out.write(param.toString());
	            }
	            // flush输出流的缓冲
	            out.flush();
	            // 定义BufferedReader输入流来读取URL的响应
	            in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream(), "UTF-8"));
	            String line;
	            while ((line = in.readLine()) != null) {
	                result.append(line);
	            }
	        } catch (Exception e) {            
	            e.printStackTrace();
	        }
	        //使用finally块来关闭输出流、输入流
	        finally{
	            try{
	                if(out!=null){
	                    out.close();
	                }
	                if(in!=null){
	                    in.close();
	                }
	            }
	            catch(IOException ex){
	                ex.printStackTrace();
	            }
	        }
	        return result.toString();
	    }
}
