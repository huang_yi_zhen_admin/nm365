/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * @author fyy
 *
 */
public class YZBK_SendExpress extends BaseSendExpress {
	
	//标准快递
	public final static int YZBK_ExpType_1 = 0;//标准快递

	/**
	 * 
	 */
	public YZBK_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.YZBK);
	}

	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_0 = new HashMap<String,String>();
		exptype_0.put("exptype", "1");
		exptype_0.put("exptypename", "标准快递");
		results.add(exptype_0);
		
		
		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(1);
		data.setPageNum(1);
		data.setShowNum(5);
		data.setResults(results);
		
		return data;
	}

	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		/*if( StringUtil.isEmpty(getCustomerName()) ){
			errMsg.append("请输入商户代码");
			return false;
		}

		if( StringUtil.isEmpty(getMonthCode())){
			errMsg.append("请输入密钥串");
			return false;
		}*/
		return super.checkdata(errMsg);
	}

}
