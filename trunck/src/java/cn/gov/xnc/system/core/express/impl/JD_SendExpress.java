/**
 * 
 */
package cn.gov.xnc.system.core.express.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.gov.xnc.system.core.common.model.json.DataGrid;
import cn.gov.xnc.system.core.express.BaseSendExpress;
import cn.gov.xnc.system.core.express.ExpressCompany;
import cn.gov.xnc.system.core.util.ExpressUtil;
import cn.gov.xnc.system.core.util.StringUtil;

/**
 * @author fyy
 *
 */
public class JD_SendExpress extends BaseSendExpress {
	
	//订单来源：京东商城
	public final static int JD_ExpType_1 = 1;//订单来源：京东商城
	//订单来源：天猫 
	public final static int JD_ExpType_2 = 2;//订单来源：天猫 
	//订单来源：苏宁 
	public final static int JD_ExpType_3 = 3;//订单来源：苏宁 
	//订单来源：亚马逊中国 
	public final static int JD_ExpType_4 = 4;//订单来源：亚马逊中国 
	//订单来源：ChinaSkin 
	public final static int JD_ExpType_5 = 5;//订单来源：ChinaSkin 
	//订单来源：其他销售平台 
	public final static int JD_ExpType_6 = 6;//订单来源：其他销售平台 
	
	/**
	 * 获取该快递支持的快递类型
	 */
	public static DataGrid getSupportedExpressExpType(DataGrid data){
		
		
		List<HashMap<String,String>> results = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> exptype_1 = new HashMap<String,String>();
		exptype_1.put("exptype", "1");
		exptype_1.put("exptypename", "订单来源：京东商城");
		results.add(exptype_1);
		
		HashMap<String,String> exptype_2 = new HashMap<String,String>();
		exptype_2.put("exptype", "2");
		exptype_2.put("exptypename", "订单来源：天猫");
		results.add(exptype_2);
		
		HashMap<String,String> exptype_3 = new HashMap<String,String>();
		exptype_3.put("exptype", "3");
		exptype_3.put("exptypename", "订单来源：苏宁");
		results.add(exptype_3);
		
		HashMap<String,String> exptype_4 = new HashMap<String,String>();
		exptype_4.put("exptype", "4");
		exptype_4.put("exptypename", "订单来源：亚马逊中国");
		results.add(exptype_4);
		
		HashMap<String,String> exptype_5 = new HashMap<String,String>();
		exptype_5.put("exptype", "5");
		exptype_5.put("exptypename", "订单来源：ChinaSkin");
		results.add(exptype_5);
		
		HashMap<String,String> exptype_6 = new HashMap<String,String>();
		exptype_6.put("exptype", "6");
		exptype_6.put("exptypename", "订单来源：其他销售平台");
		results.add(exptype_6);

		data.setMsg("success");
		data.setSuccess("1");
		data.setTotal(6);
		data.setPageNum(1);
		data.setShowNum(6);
		data.setResults(results);	
		
		return data;
	}

	/**
	 * 
	 */
	public JD_SendExpress() {
		// TODO Auto-generated constructor stub
		this.setEBusinessID(ExpressUtil.EBusinessID);
		this.setRequestType(ExpressUtil.RequestType);
		this.setDataType(ExpressUtil.DataType);
		this.setShipperCode(ExpressCompany.JD);
	}
	
	/* (non-Javadoc)
	 * @see cn.gov.xnc.system.core.express.BaseSendExpress#checkdata(java.lang.StringBuffer)
	 */
	@Override
	public boolean checkdata(StringBuffer errMsg) {
		// TODO Auto-generated method stub
		if( StringUtil.isEmpty(getCustomerName()) ){
			errMsg.append("请输入客户编码");
			return false;
		}
		
	
		return super.checkdata(errMsg);
	}

}
