package cn.gov.xnc.system.core.constant;


/**  
* 类名称：Globals   
* 类描述：  全局变量定义
* 创建人： ljz      
* @version    
*
 */
public final class Globals {
	
	/**
	 * 创建用户
	 */
	public static final String createuser = "createUser";
	/**
	 * 删除标识符
	 */
	public static final String delete = "isDelete";
	/**
	 * 用户公司
	 */
	public static final String company = "company";
	/**
	 * 新项目类型
	 */
	public static final String StartProject_ShengWai = "1";
	public static final String StartProject_ShengNei = "2";
	public static final String StartProject_ZhengFu = "3";
	/**
	 * 保存用户到SESSION
	 */
	public static final String USER_SESSION="USER_SESSION";
	
	/**
	 * 人员类型  1 管理员用户 2 员工用户 3 客户用户 4其他
	 */
	public static final String User_type_1="1";//管理员用户
	public static final String User_type_2="2";//员工用户
	public static final String User_type_3="3";//客户用户
	public static final String User_type_4 ="4";//其他


	
	/**
	 * 人员状态   1 启用 2冻结 3过期 4未审
	 */
	public static final Short User_status_1=1;//启用
	public static final Short User_status_2=2;//冻结
	public static final Short User_status_3=3;//过期
	public static final Short User_status_4 =4;//未审

	
	/**
	 * 数据权限级别
	 */
	public static final String Data_Scope_Myself = "1";//只能查看自己的数据  企业级别
	public static final String Data_Scope_MyArea = "2";//查看自己的下属数据
	public static final String Data_Scope_All = "3";//所有的数据  管理员级别
		
	/**
	 *日志级别定义
	 */
	public static final Short Log_Leavel_INFO=1;
	public static final Short Log_Leavel_WARRING=2;
	public static final Short Log_Leavel_ERROR=3;
	 /**
	  * 日志类型
	  */
	 public static final Short Log_Type_LOGIN=1; //登陆
	 public static final Short Log_Type_EXIT=2;  //退出
	 public static final Short Log_Type_INSERT=3; //插入
	 public static final Short Log_Type_DEL=4; //删除
	 public static final Short Log_Type_UPDATE=5; //更新
	 public static final Short Log_Type_UPLOAD=6; //上传
	 public static final Short Log_Type_OTHER=7; //其他
	 
	 
	 /**
	  * 词典分组定义
	  */
	 public static final String TypeGroup_Database="database";//数据表分类
	 
	 /**
	  * 权限等级
	  */
	 public static final Short Function_Leave_ONE=0;//一级权限
	 public static final Short Function_Leave_TWO=1;//二级权限
	 
	 /**
	  * 权限等级前缀
	  */
	 public static final String Function_Order_ONE="ofun";//一级权限
	 public static final String Function_Order_TWO="tfun";//二级权限

	 /**
	  * 新闻法规
	  */
	 public static final Short Document_NEW=0; //新建
	 public static final Short Document_PUBLICH=0; //发布

	 /**
	  * 配置系统是否开启按钮权限控制
	  */
	 public static final boolean BUTTON_AUTHORITY_CHECK = true;
	/* static{
		 String button_authority_jeecg = ResourceUtil.getSessionattachmenttitle("button.authority.jeecg");
		 if("true".equals(button_authority_jeecg)){
			 BUTTON_AUTHORITY_CHECK = true;
		 }
	 }*/
}
