package cn.gov.xnc.system.core.util;


import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ser.StdSerializerProvider;
import org.codehaus.jackson.type.TypeReference;


public class JsonUtils {
	 
    final static ObjectMapper objectMapper;
 
    /**
     * 是否打印美观格式
     */
    static boolean isPretty = false;
 
    static {
        StdSerializerProvider sp = new StdSerializerProvider();
        objectMapper = new ObjectMapper(null, sp, null);
    }
 
    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }
    /**
     * JSON串转换为Java泛型对象，可以是各种类型，此方法最为强大。用法看测试用例。
     * @param <T>
     * @param jsonString JSON字符串
    * @param tr TypeReference,例如: new TypeReference< List<FamousUser> >(){}
     * @return List对象列表
     */
    public static <T> T json2GenericObject(String jsonString, TypeReference<T> tr) {
 
        if (jsonString == null || "".equals(jsonString)) {
            return null;
        } else {
            try {
                return (T) objectMapper.readValue(jsonString, tr);
            } catch (Exception e) {
            }
        }
        return null;
    }
 
    /**
     * Java对象转Json字符串
     * 
     * @param object Java对象，可以是对象，数组，List,Map等
     * @return json 字符串
     */
    public static String toJson(Object object) {
        String jsonString = "";
        try {
            if (isPretty) {
                jsonString = objectMapper.defaultPrettyPrintingWriter().writeValueAsString(object);
            } else {
                jsonString = objectMapper.writeValueAsString(object);
            }
        } catch (Exception e) {
        }
        return jsonString;
 
    }
 
    /**
     * Json字符串转Java对象
     * 
     * @param jsonString
     * @param c
     * @return
     */
    public static Object json2Object(String jsonString, Class<?> c) {
 
        if (jsonString == null || "".equals(jsonString)) {
            return "";
        } else {
            try {
                return objectMapper.readValue(jsonString, c);
            } catch (Exception e) {
            }
 
        }
        return "";
    }
    
     public static <T> T readValue(String content, Class<T> valueType) {
        try {
            return objectMapper.readValue(content, valueType);
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return null;
    }
 

   
}
