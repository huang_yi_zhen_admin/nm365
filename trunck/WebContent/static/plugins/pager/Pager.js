if(typeof(ZK)=="undefined"){
    ZK={};
}
if(typeof(ZK.ui)=='undefined'){
    ZK.ui={};
}
ZK.ui.Pager=function(conf){
    this._init(conf);
}
(function (Pager, $) {
    $.extend(Pager.prototype, {
        config:{
            //基础设置
            $container:null, //分页容器，必填，默认模板下，这里需要是一个ul
            pageNo:1, //页码，必填
            pageSize:20, //页面数据量，必填
            dataCount:20, //数据总量，必填
            pageArea:5, //有效页码数量，不含1、max
            //显示设置
            showPageNo:true, //显示页码……论坛、博客之类，没有总数，不能计算总页码的，设置一个超级大的dataCount后，设置showPageNo为false，可以满足这种需求
            direction:"always", //展示上一页、下一页。none：不显示、always：一直显示，不可用时有class="disable"、auto：按需出现
            bound:"number", //展示首页、尾页的方式。none：不显示、number："1...7 8 9 ...100"的形式，text:"首页 7 8 9 尾页"的形式
            showCount:false, //显示总数
            showJumpPage:false, //展示直接跳转（输入框+GO）
            hideJustOne:true, //只有一页时不显示？
            scrollAfterChange:-1,//页码切换之后，滚动条位置。
            //模板设置
            clickable:'<li class="{{className}}"><a href="javascript:void(0);">{{text}}</a></li>', //可点击样式
            current:'<li class="active"><span>{{text}}</span></li>', //当前按钮样式
            disabled:'<li class="{{disabledClass}} {{className}}"><span>{{text}}</span></li>', //不能点击
            jumpPage:'<input type="text" class="{{inputClass}}" value="{{pageNo}}"/><input type="button" class="{{btnClass}}" value="{{text}}"/> ', //showJumpPage为true时显示
            countMsg:'<span>共{{dataCount}}条记录</span>', //showCount为true时显示
            //按钮文字设置
            prevText:"上一页",
            nextText:"下一页",
            firstText:"首页", //bound为text时有效
            lastText:"最后一页", //bound为text时有效
            jumpText:"GO",
            //事件绑定class
            pageClass:"page-no", //页码class
            prevClass:"pager-prev",
            nextClass:"pager-next",
            firstClass:"first-link",
            lastClass:"last-link",
            jumpTextClass:"jump-input", //快速跳转，输入框class
            jumpBtnClass:"jump-btn", //快速跳转按钮class
            disabledClass:"disabled", //不可点击的class
            //事件回调，点击前后你想做………………
            beforePageChange:null, //function (destPageNo,cb),同步直接返回true或false，异步的话不需要返回值，在callback的时候运行第二个参数cb触发后续页码切换。上下文为当前TY.ui.Pager对象，可以通过this获取config.pageNo
            afterPageChange:null//function(destPageNo)，参数为更新后的pageNo，上下文为当前TY.ui.Pager对象
        },
        /**
         * 在页签切换等场景下，需要公用pager时，可以通过这个方法重置pager，不会触发点击系列事件
         * @param pageNo
         * @param pageSize
         * @param dataCount
         */
        resetPager:function (pageNo, pageSize, dataCount) {
            this.config.pageNo = pageNo;
            this.config.pageSize = pageSize;
            this.config.dataCount = dataCount;
            this._render();
        },
        _init:function (customConfig) {
            this.config = $.extend({}, this.config, customConfig);
            this._tmpl = new TY.util.Template({
                "clickable":this.config.clickable,
                "current":this.config.current,
                "disabled":this.config.disabled,
                "jump":this.config.jumpPage,
                "count":this.config.countMsg
            });
            this._render();
            this._bindEvent();
        },
        /**
         * 计算页码边界范围
         */
        _calcArea:function () {
            if (this._maxPageNo <= this.config.pageArea) {//页码最大值小于页码范围，应该是全部显示了吧？！
                this._startNo = 1;
                this._endNo = this._maxPageNo;
            } else {
                //这里开始要分3种情况，即当前页码在在左边界、中间、右边界时
                //(pageArea-1)/2，左边页码个数向上取整，右边页码个数向下取整
                var halfArea = (this.config.pageArea - 1) / 2,
                    left = Math.ceil(halfArea), //左边页码个数
                    right = Math.floor(halfArea), //右边页码个数
                    tmp;
                if ((tmp = this.config.pageNo - 1 - left) < 0) {//左边放不下了
                    right -= tmp;//丢到右边
                    left += tmp;
                } else if ((tmp = this._maxPageNo - right - this.config.pageNo) < 0) {//右边放不下
                    left -= tmp;
                    right += tmp;
                }
                this._startNo = this.config.pageNo - left;
                this._endNo = this.config.pageNo + right;
            }
        },
        /**
         * 绘制页码方向
         * @param type prev、next
         * @return {String}
         */
        _renderDirection:function (type) {
            var html = "",
                isBoundaryPage = (this.config.pageNo == (type == "prev" ? 1 : this._maxPageNo));//是否为边界
            if (!(this.config.direction == this._const_str.none || this.config.direction == this._const_str.auto && isBoundaryPage)) {//不显示方向或者第一页&&auto
                if (isBoundaryPage) {//这里只会出现显示&&不能点击的情况
                    html = this._tmpl.bind("disabled", {disabledClass:this.config.disabledClass, className:this.config[type + "Class"], text:this.config[type + "Text"]});
                } else {
                    html = this._tmpl.bind("clickable", {className:this.config[type + "Class"], text:this.config[type + "Text"]});
                }
            }
            return html;
        },
        /**
         * 绘制边界
         * @param type first、last
         * @return {String}
         */
        _renderBound:function (type) {
            var html = "",
                isFirst = (type == "first"),
                isBoundaryPage = (this.config.pageNo == (isFirst ? 1 : this._maxPageNo));
            if (!(this.config.direction == this._const_str.auto && isBoundaryPage)) {
                if (this.config.bound == this._const_str.number && (isFirst && this._startNo > 1 || !isFirst && this._endNo < this._maxPageNo)) {//数字类型，样式应该跟普通数字一样吧……
                    if (!isFirst && (this._maxPageNo - this._endNo > 1)) {
                        html += this._tmpl.bind("disabled", {disabledClass:"",className:"", text:"..."});
                    }
                    html += this._tmpl.bind("clickable", {className:this.config.pageClass, text:(isFirst ? "1" : this._maxPageNo)});
                    if (isFirst && this._startNo > 2) {
                        html += this._tmpl.bind("disabled", {disabledClass:"",className:"", text:"..."});
                    }
                } else if (this.config.bound == this._const_str.text) {//文字型
                    if (isBoundaryPage) {
                        html = this._tmpl.bind("disabled", {disabledClass:this.config.disabledClass, className:this.config[type + "Class"], text:this.config[type + "Text"]});
                    } else {
                        html = this._tmpl.bind("clickable", {className:this.config[type + "Class"], text:this.config[type + "Text"]});
                    }
                }
            }
            return html;
        },
        /**
         * 绘制直接跳转
         * @return {*}
         */
        _renderJump:function () {
            return this._tmpl.bind("jump", {
                inputClass:this.config.jumpTextClass,
                btnClass:this.config.jumpBtnClass,
                text:this.config.jumpText,
                pageNo:this.config.pageNo
            });
        },
        _renderCount:function () {
            return this._tmpl.bind("count", {dataCount:this.config.dataCount});
        },
        /**
         * 绘制页码
         * @return {String}
         */
        _renderPageNo:function () {
            var html = [];
            for (var i = this._startNo, iMax = this._endNo; i <= iMax; i++) {
                if (i == this.config.pageNo) {
                    html.push(this._tmpl.bind("current", {className:this.config.pageClass, text:i}));
                } else {
                    html.push(this._tmpl.bind("clickable", {className:this.config.pageClass, text:i}));
                }
            }
            return html.join("");
        },
        /**
         * 绘制分页控件
         */
        _render:function () {
            var html = [];
            this._maxPageNo = Math.ceil(this.config.dataCount / this.config.pageSize);
            if (this._maxPageNo > 1 || !this.config.hideJustOne) {//页码大于1，或者只有1页都显示……
                this._calcArea();//计算页码范围
                this.config.showCount && html.push(this._renderCount());
                this.config.bound != this._const_str.none && this.config.bound == this._const_str.text && html.push(this._renderBound("first"));//文字
                this.config.direction != this._const_str.none && html.push(this._renderDirection("prev"));
                this.config.bound != this._const_str.none && this.config.bound == this._const_str.number && html.push(this._renderBound("first"));
                this.config.showPageNo && html.push(this._renderPageNo());
                this.config.bound != this._const_str.none && this.config.bound == this._const_str.number && html.push(this._renderBound("last"));
                this.config.direction != this._const_str.none && html.push(this._renderDirection("next"));
                this.config.bound != this._const_str.none && this.config.bound == this._const_str.text && html.push(this._renderBound("last"));
                this.config.showJumpPage && html.push(this._renderJump());
            }
            this.config.$container.html(html.join(""));
        },
        /**
         * 做真正的页面切换动作
         * @param destPageNo
         */
        _doChange:function (destPageNo) {
            if(this.config.scrollAfterChange!==-1) {
                scroll(0, this.config.scrollAfterChange);
            }
            this.config.pageNo = +destPageNo;
            this._render();
            if ($.isFunction(this.config.afterPageChange)) {
                this.config.afterPageChange.call(this, destPageNo);
            }
        },
        /**
         * 统一处理点击事件
         * @param e
         * @return false，阻止默认事件
         */
        _clickEvent:function (e) {//这么写似乎有点恶心啊……
            var $this = $(e.currentTarget),
                destPageNo = -1;
            if ($this.hasClass(this.config.pageClass)) {
                destPageNo = +$this.text();
            } else {
                if (!$this.hasClass(this.config.disabledClass)) {
                    if ($this.hasClass(this.config.prevClass)) {
                        destPageNo = this.config.pageNo - 1;
                    } else if ($this.hasClass(this.config.nextClass)) {
                        destPageNo = +this.config.pageNo + 1;
                    } else if ($this.hasClass(this.config.firstClass)) {
                        destPageNo = 1;
                    } else if ($this.hasClass(this.config.lastClass)) {
                        destPageNo = +this._maxPageNo;
                    } else if ($this.hasClass(this.config.jumpBtnClass)) {
                        var $pageNo = this.config.$container.find("." + this.config.jumpTextClass),
                            val = $pageNo.val();
                        if (val != "" && val.replace(/\d/g, "") == "") {
                            val = parseInt(val, 10);
                            if (val <= 0) {
                                destPageNo = 1;
                            } else if (val > this._maxPageNo) {
                                destPageNo = +this._maxPageNo;
                            } else {
                                destPageNo = val;
                            }
                        } else {
                            $pageNo.val("");
                        }
                    }
                }
            }
            if (destPageNo !== -1) {//点击前后事件处理
                var result = true;
                if ($.isFunction(this.config.beforePageChange)) {
                    var that = this;
                    //判断有误点击前的拦截函数，如果有，返回则会继续执行，返回false或者undefinded，doChange就不会执行。第二个参数用于异步执行。平时不需要使用。
                    result = this.config.beforePageChange.call(this, destPageNo, function () {
                        that._doChange.call(that, destPageNo);
                    });
                }
                result && this._doChange(destPageNo);
            }
            return false;
        },
        _bindEvent:function () {
            this.config.$container.delegate(
                "." + this.config.pageClass +
                    ",." + this.config.prevClass +
                    ",." + this.config.nextClass +
                    ",." + this.config.firstClass +
                    ",." + this.config.lastClass +
                    ",." + this.config.jumpBtnClass,
                "click",
                $.proxy(this._clickEvent, this)
            );
        },
        _maxPageNo:-1, //最大页码
        _startNo:-1, //页码起始范围
        _endNo:-1, //页码终止范围
        _tmpl:null,
        _const_str:{//方便改名……
            always:"always",
            none:"none",
            auto:"auto",
            number:"number",
            text:"text"
        }
    });
})(ZK.ui.Pager, JQuery);