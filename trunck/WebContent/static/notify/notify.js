var notifyObj = {
	getCountUnread : function(){
		jQuery.ajax({
			type:"get",
			url:"/notifyController/getCountUnreadNotify",
			success: function(data){
				var message = jQuery("#message");
				var notice = jQuery("#notice");
				message.find("a").attr("href","/notifyController/messageList");
				message.find("span").html(data.obj.messageCount);
				if(data.obj.messageCount==1){
					message.find(".header").html(""+data.obj.messageContent);
				}else{
					message.find(".header").html("您有"+data.obj.messageCount+"条站内短信");
				}
				
				notice.find("a").attr("href","/notifyController/noticeList");
				notice.find("span").html(data.obj.noticeCount);
				if(data.obj.noticeCount == 1){
					notice.find(".header").html(""+data.obj.noticeContent);
				}else{
					notice.find(".header").html("您有"+data.obj.noticeCount+"条通知");
				}
				
			},
			dataType: "json"
		});
	}
};
notifyObj.getCountUnread();