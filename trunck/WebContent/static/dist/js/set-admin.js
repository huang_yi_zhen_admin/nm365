function setAdmin(conf){
	var Conf={
		wrapEl:$(".set-admin"),
		lev0:'lev-0',
		lev1:'lev-1',
		lev2:'lev-2'
	},
	data=[];

	$.extend(Conf, conf);

	var labelTpl='<label class="text-{color}"><input type="checkbox" class="l-{lv}" n="{n}" name="{name}" subname="{subname}" {checked} {indeterminate} value="{id}">{text}</label>',
		lv1Tpl=['<div class="lev-1 row">',
                	'<h4 class="checkbox ">{lv1}</h4>',
              	'</div>',
              	'<div class="lev-2 row">',
              	'{lv2}',
              '</div>'
              ].join(""),
        lv0Tpl=[
        	'<div class="box box-default box-group collapsed-box">',
	            '<div class="box-header">',
	                '<h3 class="box-title checkbox">{head}',
	                '<div class="box-tools">',
	                '<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-plus"></i>',
	                '</button>',
	                '</div>',
	              '</h3>',
	            '</div>',
	            '<div class="box-body" style="display:  none;">',
	              '{body}',
	            '</div>',
	        '</div>'
        ].join("");

 	
	function getLabel(o,lv,i){
		o['lv'] = lv;
		o["color"] = ["yellow","blue"][lv]||"";
		o["checked"] = o["checked"]=="true" ? "checked":"";
		return labelTpl.ZK_format(o);
	}

	function getLv1(arr,name,r,pid){
		var html=[];
		if(!arr){
			return "";
		}
		var selNum=0;
		for(var i=0,l=arr.length;i<l;i++){
			arr[i]['name']="lv1";
			arr[i]['n'] = name;
			arr[i]['subname']=name+"-"+i;
			arr[i]['id']=pid+":"+arr[i]['id'];
			if(arr[i]["checked"]=="true"){
				selNum++;
			}

			var o=getLv2(arr[i]["subs"],arr[i]['subname'],r,arr[i]['id']);
			arr[i]['indeterminate'] = o['indeterminate'];
			html.push(lv1Tpl.ZK_format({
				lv1:getLabel(arr[i],1,i),
				lv2:o['tpl']
			}))
		}

		return {"tpl":html.join(""),"indeterminate": selNum&&selNum<l ?"indeterminate":""};
	}
	function getLv2(arr,name,r,pid){
		if(!arr){
			return "";
		}
		var html=[],selNum=0;
		for(var i=0,l=arr.length;i<l;i++){
			arr[i]['name']='lv2';
			arr[i]['subname']="";
			arr[i]['n'] = name;
			arr[i]['id']=pid+":"+arr[i]['id'];
			data[i]['indeterminate'] = "";

			if(arr[i]["checked"]=="true"){
				selNum++;
			}

			html.push('<div class="col-sm-2 ">'+getLabel(arr[i],2,i)+'</div>');
		}
		return {"tpl":html.join(""),indeterminate: selNum && selNum<l ? "indeterminate":""};
	}

	function init(data){
		var head=[],body=[],box=[];

		for(var i=0,l=data.length;i<l;i++){
			data[i]["subname"]="lv1"+"-"+i;
			data[i]['name'] = "lv0";
			data[i]["n"]="";
			var o=getLv1(data[i]['subs'],data[i]['subname'],i,data[i]['id']);
			data[i]['indeterminate'] = o['indeterminate'];
			console.log(o)
			box.push(lv0Tpl.ZK_format({
				'head':getLabel(data[i],0,i),
				'body':	o['tpl']||""	
			}))
		}

		Conf.wrapEl.html(box.join(""));

		$("input[indeterminate]").prop({
			indeterminate: true,
			checked: false
		})
	}
	

	//事件
	function checkParent(ipt){
		var n=ipt.attr("n");
		if(n==""){
			return false;
		}

		var	allNum = $("input[n="+n+"]").size(),
			selNum = $("input[n="+n+"]:checked").size(),
			partSelNum =$("input[n="+n+"]:indeterminate").size(),
 
			pp = $("input[subname="+n+"]");

		if(allNum==selNum){
			pp.prop({checked:true,indeterminate:false});
		}else if(selNum==0 && partSelNum==0){
			pp.prop({checked:false,indeterminate:false});
		}else{

			pp.prop({
				checked: false,
				indeterminate: true,
			})
		}

		checkParent(pp);


    }

	function setChecked(rds,isSet){
		rds.each(function(i,el){
			if(isSet){
				$(el).prop({
					checked: true,
					indeterminate: false,
				})
			}else{
				$(el).prop({
					checked: false,
					indeterminate: false,
				})
			}
			var cls=el.className,
				n=$(el).attr("subname");
			if(n){//还有下一级
				setChecked($("input[n="+n+"]"),isSet);
			}
			
		})
	}

	Conf.wrapEl.delegate('label', 'click', function(event) {
		var el = $(this),
			ck=el.find("input[type=checkbox]"),
			type=true;

		if(ck.get(0).checked){
			type=false;
		}
		setChecked(ck,type);
		checkParent(ck)

		return false;
		
	}).delegate('input[type=checkbox]', 'click', function(event) {
		var ck = $(this),
			type=false;

		if(ck.get(0).checked){
			type=true;
		}
		setTimeout(function(){
			setChecked(ck,type);
			checkParent(ck);
		},0);
	});

	this.initData=function(arr){
		data=arr;
		init(data);
	}
}

function addActorCtrl(item){
	var tpl='<div class="ctr-div"><span class="ctr-btn"><i class="fa fa-fw fa-cog"></i></span><div style="display: none;" class="box ctr-float"><button data-no="{no}" class="btn btn-default detail-btn" type="button"><i class="fa fa-file-text-o"></i>详情</button><button data-no="{no}" class="btn btn-default edit-btn" type="button"><i class="fa  fa-edit"></i>编辑</button><button data-no="{no}" class="btn btn-default del-btn" type="button"><i class="fa fa-times"></i>删除</button></div></div>';

	if(!item){
		item = $(".actor-label");
	}
	item.each(function(i,el){
		el= $(el);
		var ipt = el.find("input"),
			val = ipt.val();
		if(!el.find(".ctr-div").size()){
			el.append(tpl.ZK_format({'no':val}));
		}
	})

}
