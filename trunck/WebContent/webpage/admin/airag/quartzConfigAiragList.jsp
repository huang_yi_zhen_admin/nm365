<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="quartzConfigAiragList" title="大气候数据自动抓取配置" actionUrl="quartzConfigAiragController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="农眼id" field="aeid" ></z:dgCol>
   <z:dgCol title="接口名称" field="interfaceName" ></z:dgCol>
   <z:dgCol title="接口地址" field="interfaceUrl" ></z:dgCol>
   <z:dgCol title="上次调用时间" field="lastCallTime" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="失败尝试次数" field="failTryTimes" ></z:dgCol>
   <z:dgCol title="获取数据条数" field="fetchsize" ></z:dgCol>
   <z:dgCol title="用户id，调接口时用" field="userId" ></z:dgCol>
   <z:dgCol title="客户id，获取token时候用" field="clientId" ></z:dgCol>
   <z:dgCol title="客户密码，获取token时候用" field="clientSecret" ></z:dgCol>
   <z:dgCol title="授权方式，获取token时候用" field="grantType" ></z:dgCol>
   <z:dgCol title="用户名称，获取token时候用" field="username" ></z:dgCol>
   <z:dgCol title="用户密码，获取token时候用" field="password" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="quartzConfigAiragController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="quartzConfigAiragController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="quartzConfigAiragController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="quartzConfigAiragController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>