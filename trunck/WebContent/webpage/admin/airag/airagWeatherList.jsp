<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="airagWeatherList" title="大气候天气数据" actionUrl="airagWeatherController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="农眼id" field="aeid" ></z:dgCol>
   <z:dgCol title="采集时间" field="collectTime" ></z:dgCol>
   <z:dgCol title="雨量" field="rainfall" ></z:dgCol>
   <z:dgCol title="空气温度" field="airTem" ></z:dgCol>
   <z:dgCol title="airTemThreshold" field="airTemThreshold" ></z:dgCol>
   <z:dgCol title="空气湿度" field="airHum" ></z:dgCol>
   <z:dgCol title="airHumThreshold" field="airHumThreshold" ></z:dgCol>
   <z:dgCol title="光照" field="lightIntensity" ></z:dgCol>
   <z:dgCol title="气压" field="airPressure" ></z:dgCol>
   <z:dgCol title="风向值" field="windDirection" ></z:dgCol>
   <z:dgCol title="风向描述" field="windDirectionDesc" ></z:dgCol>
   <z:dgCol title="风速" field="windSpeed" ></z:dgCol>
   <z:dgCol title="风力等级" field="windScale" ></z:dgCol>
   <z:dgCol title="最大风速" field="maxWindSpeed" ></z:dgCol>
   <z:dgCol title="露点温度" field="dewPointTem" ></z:dgCol>
   <z:dgCol title="gpsxReal" field="gpsxReal" ></z:dgCol>
   <z:dgCol title="gpsyReal" field="gpsyReal" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="airagWeatherController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="airagWeatherController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="airagWeatherController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="airagWeatherController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>