<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>大气候天气数据</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="airagWeatherController.do?save">
		<input id="id" name="id" type="hidden" value="${airagWeatherPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">农眼id:</label>
		      <input class="inputxt" id="aeid" name="aeid" 
					   value="${airagWeatherPage.aeid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">采集时间:</label>
		      <input class="inputxt" id="collectTime" name="collectTime" ignore="ignore"
					   value="${airagWeatherPage.collectTime}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">雨量:</label>
		      <input class="inputxt" id="rainfall" name="rainfall" ignore="ignore"
					   value="${airagWeatherPage.rainfall}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">空气温度:</label>
		      <input class="inputxt" id="airTem" name="airTem" ignore="ignore"
					   value="${airagWeatherPage.airTem}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">airTemThreshold:</label>
		      <input class="inputxt" id="airTemThreshold" name="airTemThreshold" ignore="ignore"
					   value="${airagWeatherPage.airTemThreshold}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">空气湿度:</label>
		      <input class="inputxt" id="airHum" name="airHum" ignore="ignore"
					   value="${airagWeatherPage.airHum}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">airHumThreshold:</label>
		      <input class="inputxt" id="airHumThreshold" name="airHumThreshold" ignore="ignore"
					   value="${airagWeatherPage.airHumThreshold}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">光照:</label>
		      <input class="inputxt" id="lightIntensity" name="lightIntensity" ignore="ignore"
					   value="${airagWeatherPage.lightIntensity}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">气压:</label>
		      <input class="inputxt" id="airPressure" name="airPressure" ignore="ignore"
					   value="${airagWeatherPage.airPressure}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">风向值:</label>
		      <input class="inputxt" id="windDirection" name="windDirection" ignore="ignore"
					   value="${airagWeatherPage.windDirection}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">风向描述:</label>
		      <input class="inputxt" id="windDirectionDesc" name="windDirectionDesc" ignore="ignore"
					   value="${airagWeatherPage.windDirectionDesc}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">风速:</label>
		      <input class="inputxt" id="windSpeed" name="windSpeed" ignore="ignore"
					   value="${airagWeatherPage.windSpeed}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">风力等级:</label>
		      <input class="inputxt" id="windScale" name="windScale" ignore="ignore"
					   value="${airagWeatherPage.windScale}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">最大风速:</label>
		      <input class="inputxt" id="maxWindSpeed" name="maxWindSpeed" ignore="ignore"
					   value="${airagWeatherPage.maxWindSpeed}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">露点温度:</label>
		      <input class="inputxt" id="dewPointTem" name="dewPointTem" ignore="ignore"
					   value="${airagWeatherPage.dewPointTem}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">gpsxReal:</label>
		      <input class="inputxt" id="gpsxReal" name="gpsxReal" ignore="ignore"
					   value="${airagWeatherPage.gpsxReal}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">gpsyReal:</label>
		      <input class="inputxt" id="gpsyReal" name="gpsyReal" ignore="ignore"
					   value="${airagWeatherPage.gpsyReal}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>