<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>大气候土壤数据</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="airagSoilController.do?save">
		<input id="id" name="id" type="hidden" value="${airagSoilPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">aeid:</label>
		      <input class="inputxt" id="aeid" name="aeid" 
					   value="${airagSoilPage.aeid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">土壤传感器id:</label>
		      <input class="inputxt" id="sensorId" name="sensorId" 
					   value="${airagSoilPage.sensorId}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">采集时间:</label>
		      <input class="inputxt" id="collectTime" name="collectTime" 
					   value="${airagSoilPage.collectTime}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">soilTem:</label>
		      <input class="inputxt" id="soilTem" name="soilTem" ignore="ignore"
					   value="${airagSoilPage.soilTem}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">soilTemThreshold:</label>
		      <input class="inputxt" id="soilTemThreshold" name="soilTemThreshold" ignore="ignore"
					   value="${airagSoilPage.soilTemThreshold}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">soilHum:</label>
		      <input class="inputxt" id="soilHum" name="soilHum" ignore="ignore"
					   value="${airagSoilPage.soilHum}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">soilHumThreshold:</label>
		      <input class="inputxt" id="soilHumThreshold" name="soilHumThreshold" ignore="ignore"
					   value="${airagSoilPage.soilHumThreshold}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">gpsX:</label>
		      <input class="inputxt" id="gpsX" name="gpsX" ignore="ignore"
					   value="${airagSoilPage.gpsX}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">gpsY:</label>
		      <input class="inputxt" id="gpsY" name="gpsY" ignore="ignore"
					   value="${airagSoilPage.gpsY}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">sensor:</label>
		      <input class="inputxt" id="sensor" name="sensor" ignore="ignore"
					   value="${airagSoilPage.sensor}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>