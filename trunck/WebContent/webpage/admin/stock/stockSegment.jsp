<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>出库详细单</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="stockSegmentController.do?save">
		<input id="id" name="id" type="hidden" value="${stockSegmentPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">仓库分区名称或者是货柜名称:</label>
		      <input class="inputxt" id="name" name="name" 
					   value="${stockSegmentPage.name}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">上级仓储区域或者货柜编码:</label>
		      <input class="inputxt" id="preid" name="preid" 
					   value="${stockSegmentPage.preid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">下级仓储区域或者货柜编码:</label>
		      <input class="inputxt" id="nextid" name="nextid" ignore="ignore"
					   value="${stockSegmentPage.nextid}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">所属仓库编码（仓库最高级别节点）:</label>
		      <input class="inputxt" id="stockid" name="stockid" 
					   value="${stockSegmentPage.stockid}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">备注:</label>
		      <input class="inputxt" id="remarks" name="remarks" ignore="ignore"
					   value="${stockSegmentPage.remarks}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">updateTime:</label>
		      <input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="updateTime" name="updateTime" 
					     value="<fmt:formatDate value='${stockSegmentPage.updateTime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>