<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
  <title>第三方订单产品信息</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="thirdorderProductController.do?save">
		<input id="id" name="id" type="hidden" value="${thirdorderProductPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">渠道id:</label>
		      <input class="inputxt" id="channelid" name="channelid" ignore="ignore"
					   value="${thirdorderProductPage.channelid}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">商品名称:</label>
		      <input class="inputxt" id="productname" name="productname" ignore="ignore"
					   value="${thirdorderProductPage.productname}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">创建时间:</label>
		      <input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="createtime" name="createtime" ignore="ignore"
					     value="<fmt:formatDate value='${thirdorderProductPage.createtime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">状态，0正常，1已删除:</label>
		      <input class="inputxt" id="status" name="status" ignore="ignore"
					   value="${thirdorderProductPage.status}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>