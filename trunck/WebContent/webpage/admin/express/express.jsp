<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>电子面单信息</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="expressController.do?save">
		<input id="id" name="id" type="hidden" value="${expressPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">用户自定义回调信息:</label>
		      <input class="inputxt" id="callback" name="callback" ignore="ignore"
					   value="${expressPage.callback}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">会员标识平台方与快递鸟统一用户标识的商家ID:</label>
		      <input class="inputxt" id="memberid" name="memberid" ignore="ignore"
					   value="${expressPage.memberid}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电子面单客户账号:</label>
		      <input class="inputxt" id="customername" name="customername" ignore="ignore"
					   value="${expressPage.customername}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电子面单密码:</label>
		      <input class="inputxt" id="customerpwd" name="customerpwd" ignore="ignore"
					   value="${expressPage.customerpwd}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件网点标识:</label>
		      <input class="inputxt" id="sendsite" name="sendsite" ignore="ignore"
					   value="${expressPage.sendsite}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">快递公司编码:</label>
		      <input class="inputxt" id="shippercode" name="shippercode" ignore="ignore"
					   value="${expressPage.shippercode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">快递单号:</label>
		      <input class="inputxt" id="logisticcode" name="logisticcode" ignore="ignore"
					   value="${expressPage.logisticcode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">第三方订单号:</label>
		      <input class="inputxt" id="thrordercode" name="thrordercode" ignore="ignore"
					   value="${expressPage.thrordercode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">订单号:</label>
		      <input class="inputxt" id="ordercode" name="ordercode" ignore="ignore"
					   value="${expressPage.ordercode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">月结编码:</label>
		      <input class="inputxt" id="monthcode" name="monthcode" ignore="ignore"
					   value="${expressPage.monthcode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">邮费支付方式::</label>
		      <input class="inputxt" id="paytype" name="paytype" ignore="ignore"
					   value="${expressPage.paytype}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">快递类型：1-标准快件:</label>
		      <input class="inputxt" id="exptype" name="exptype" ignore="ignore"
					   value="${expressPage.exptype}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0:</label>
		      <input class="inputxt" id="isnotice" name="isnotice" ignore="ignore"
					   value="${expressPage.isnotice}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">寄件费运费:</label>
		      <input class="inputxt" id="cost" name="cost" ignore="ignore"
					   value="${expressPage.cost}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">其他费用:</label>
		      <input class="inputxt" id="othercost" name="othercost" ignore="ignore"
					   value="${expressPage.othercost}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件人公司:</label>
		      <input class="inputxt" id="receivercompany" name="receivercompany" ignore="ignore"
					   value="${expressPage.receivercompany}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件人姓名:</label>
		      <input class="inputxt" id="receivername" name="receivername" ignore="ignore"
					   value="${expressPage.receivername}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电话与手机，必填一个:</label>
		      <input class="inputxt" id="receivertel" name="receivertel" ignore="ignore"
					   value="${expressPage.receivertel}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电话与手机，必填一个:</label>
		      <input class="inputxt" id="receivermobile" name="receivermobile" ignore="ignore"
					   value="${expressPage.receivermobile}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件人邮编:</label>
		      <input class="inputxt" id="receiverpostcode" name="receiverpostcode" ignore="ignore"
					   value="${expressPage.receiverpostcode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件省（如广东省，不要缺少“省”）:</label>
		      <input class="inputxt" id="receiverprovincename" name="receiverprovincename" ignore="ignore"
					   value="${expressPage.receiverprovincename}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件市（如深圳市，不要缺少“市”）:</label>
		      <input class="inputxt" id="receivercityname" name="receivercityname" ignore="ignore"
					   value="${expressPage.receivercityname}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件区（如福田区，不要缺少“区”或“县”）:</label>
		      <input class="inputxt" id="receiverexpareaname" name="receiverexpareaname" ignore="ignore"
					   value="${expressPage.receiverexpareaname}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件人详细地址:</label>
		      <input class="inputxt" id="receiveraddress" name="receiveraddress" ignore="ignore"
					   value="${expressPage.receiveraddress}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件人公司:</label>
		      <input class="inputxt" id="sendercompany" name="sendercompany" ignore="ignore"
					   value="${expressPage.sendercompany}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件人姓名:</label>
		      <input class="inputxt" id="sendername" name="sendername" ignore="ignore"
					   value="${expressPage.sendername}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电话与手机，必填一个:</label>
		      <input class="inputxt" id="sendertel" name="sendertel" ignore="ignore"
					   value="${expressPage.sendertel}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">电话与手机，必填一个:</label>
		      <input class="inputxt" id="sendermobile" name="sendermobile" ignore="ignore"
					   value="${expressPage.sendermobile}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件人邮编:</label>
		      <input class="inputxt" id="senderpostcode" name="senderpostcode" ignore="ignore"
					   value="${expressPage.senderpostcode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件省（如广东省，不要缺少“省”）:</label>
		      <input class="inputxt" id="senderprovincename" name="senderprovincename" ignore="ignore"
					   value="${expressPage.senderprovincename}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件市（如深圳市，不要缺少“市”）:</label>
		      <input class="inputxt" id="sendercityname" name="sendercityname" ignore="ignore"
					   value="${expressPage.sendercityname}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件区（如福田区，不要缺少“区”或“县”）:</label>
		      <input class="inputxt" id="senderexpareaname" name="senderexpareaname" ignore="ignore"
					   value="${expressPage.senderexpareaname}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发件详细地址:</label>
		      <input class="inputxt" id="senderaddress" name="senderaddress" ignore="ignore"
					   value="${expressPage.senderaddress}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同:</label>
		      <input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="startdate" name="startdate" ignore="ignore"
					     value="<fmt:formatDate value='${expressPage.startdate}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同:</label>
		      <input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="enddate" name="enddate" ignore="ignore"
					     value="<fmt:formatDate value='${expressPage.enddate}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">物品总重量kg:</label>
		      <input class="inputxt" id="weight" name="weight" ignore="ignore"
					   value="${expressPage.weight}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">件数/包裹数:</label>
		      <input class="inputxt" id="quantity" name="quantity" ignore="ignore"
					   value="${expressPage.quantity}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">物品总体积m3:</label>
		      <input class="inputxt" id="volume" name="volume" ignore="ignore"
					   value="${expressPage.volume}" datatype="d">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">备注:</label>
		      <input class="inputxt" id="remark" name="remark" ignore="ignore"
					   value="${expressPage.remark}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">增值服务json:</label>
		      <input class="inputxt" id="addservice" name="addservice" ignore="ignore"
					   value="${expressPage.addservice}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">商品信息json:</label>
		      <input class="inputxt" id="commodity" name="commodity" ignore="ignore"
					   value="${expressPage.commodity}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">返回电子面单模板：0-不需要；1-需要:</label>
		      <input class="inputxt" id="isreturnprinttemplate" name="isreturnprinttemplate" ignore="ignore"
					   value="${expressPage.isreturnprinttemplate}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">订单数据json:</label>
		      <input class="inputxt" id="order" name="order" ignore="ignore"
					   value="${expressPage.order}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">是否成功:</label>
		      <input class="inputxt" id="success" name="success" ignore="ignore"
					   value="${expressPage.success}" datatype="n">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">错误编码:</label>
		      <input class="inputxt" id="resultcode" name="resultcode" ignore="ignore"
					   value="${expressPage.resultcode}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">错误原因:</label>
		      <input class="inputxt" id="reason" name="reason" ignore="ignore"
					   value="${expressPage.reason}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">唯一标识:</label>
		      <input class="inputxt" id="uniquerrequestnumber" name="uniquerrequestnumber" ignore="ignore"
					   value="${expressPage.uniquerrequestnumber}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">面单打印模板:</label>
		      <input class="inputxt" id="printtemplate" name="printtemplate" ignore="ignore"
					   value="${expressPage.printtemplate}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">订单预计到货时间yyyy-mm-dd:</label>
		      <input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="width: 150px" id="estimateddeliverytime" name="estimateddeliverytime" ignore="ignore"
					     value="<fmt:formatDate value='${expressPage.estimateddeliverytime}' type="date" pattern="yyyy-MM-dd hh:mm:ss"/>">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">子单数量:</label>
		      <input class="inputxt" id="subcount" name="subcount" ignore="ignore"
					   value="${expressPage.subcount}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">子单号:</label>
		      <input class="inputxt" id="suborders" name="suborders" ignore="ignore"
					   value="${expressPage.suborders}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">子单模板:</label>
		      <input class="inputxt" id="subprinttemplates" name="subprinttemplates" ignore="ignore"
					   value="${expressPage.subprinttemplates}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">收件人安全电话:</label>
		      <input class="inputxt" id="receiversafephone" name="receiversafephone" ignore="ignore"
					   value="${expressPage.receiversafephone}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">寄件人安全电话:</label>
		      <input class="inputxt" id="sendersafephone" name="sendersafephone" ignore="ignore"
					   value="${expressPage.sendersafephone}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">拨号页面网址（转换成二维码可扫描拨号）:</label>
		      <input class="inputxt" id="dialpage" name="dialpage" ignore="ignore"
					   value="${expressPage.dialpage}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">发送信息json:</label>
		      <input class="inputxt" id="senddata" name="senddata" ignore="ignore"
					   value="${expressPage.senddata}">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">接收信息json:</label>
		      <input class="inputxt" id="recvdata" name="recvdata" ignore="ignore"
					   value="${expressPage.recvdata}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>