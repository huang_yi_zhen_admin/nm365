<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<z:base type="jquery,easyui,tools,DatePicker"></z:base>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <z:datagrid name="expressList" title="电子面单信息" actionUrl="expressController.do?datagrid" idField="id" fit="true"   queryMode="group"  fitColumns="false" >
   <z:dgCol title="编号" field="id" hidden="false"></z:dgCol>
   <z:dgCol title="用户自定义回调信息" field="callback" ></z:dgCol>
   <z:dgCol title="会员标识平台方与快递鸟统一用户标识的商家ID" field="memberid" ></z:dgCol>
   <z:dgCol title="电子面单客户账号" field="customername" ></z:dgCol>
   <z:dgCol title="电子面单密码" field="customerpwd" ></z:dgCol>
   <z:dgCol title="收件网点标识" field="sendsite" ></z:dgCol>
   <z:dgCol title="快递公司编码" field="shippercode" ></z:dgCol>
   <z:dgCol title="快递单号" field="logisticcode" ></z:dgCol>
   <z:dgCol title="第三方订单号" field="thrordercode" ></z:dgCol>
   <z:dgCol title="订单号" field="ordercode" ></z:dgCol>
   <z:dgCol title="月结编码" field="monthcode" ></z:dgCol>
   <z:dgCol title="邮费支付方式:" field="paytype" ></z:dgCol>
   <z:dgCol title="快递类型：1-标准快件" field="exptype" ></z:dgCol>
   <z:dgCol title="是否通知快递员上门揽件：0-通知；1-不通知；不填则默认为0" field="isnotice" ></z:dgCol>
   <z:dgCol title="寄件费运费" field="cost" ></z:dgCol>
   <z:dgCol title="其他费用" field="othercost" ></z:dgCol>
   <z:dgCol title="收件人公司" field="receivercompany" ></z:dgCol>
   <z:dgCol title="收件人姓名" field="receivername" ></z:dgCol>
   <z:dgCol title="电话与手机，必填一个" field="receivertel" ></z:dgCol>
   <z:dgCol title="电话与手机，必填一个" field="receivermobile" ></z:dgCol>
   <z:dgCol title="收件人邮编" field="receiverpostcode" ></z:dgCol>
   <z:dgCol title="收件省（如广东省，不要缺少“省”）" field="receiverprovincename" ></z:dgCol>
   <z:dgCol title="收件市（如深圳市，不要缺少“市”）" field="receivercityname" ></z:dgCol>
   <z:dgCol title="收件区（如福田区，不要缺少“区”或“县”）" field="receiverexpareaname" ></z:dgCol>
   <z:dgCol title="收件人详细地址" field="receiveraddress" ></z:dgCol>
   <z:dgCol title="发件人公司" field="sendercompany" ></z:dgCol>
   <z:dgCol title="发件人姓名" field="sendername" ></z:dgCol>
   <z:dgCol title="电话与手机，必填一个" field="sendertel" ></z:dgCol>
   <z:dgCol title="电话与手机，必填一个" field="sendermobile" ></z:dgCol>
   <z:dgCol title="发件人邮编" field="senderpostcode" ></z:dgCol>
   <z:dgCol title="发件省（如广东省，不要缺少“省”）" field="senderprovincename" ></z:dgCol>
   <z:dgCol title="发件市（如深圳市，不要缺少“市”）" field="sendercityname" ></z:dgCol>
   <z:dgCol title="发件区（如福田区，不要缺少“区”或“县”）" field="senderexpareaname" ></z:dgCol>
   <z:dgCol title="发件详细地址" field="senderaddress" ></z:dgCol>
   <z:dgCol title="上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同" field="startdate" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同" field="enddate" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="物品总重量kg" field="weight" ></z:dgCol>
   <z:dgCol title="件数/包裹数" field="quantity" ></z:dgCol>
   <z:dgCol title="物品总体积m3" field="volume" ></z:dgCol>
   <z:dgCol title="备注" field="remark" ></z:dgCol>
   <z:dgCol title="增值服务json" field="addservice" ></z:dgCol>
   <z:dgCol title="商品信息json" field="commodity" ></z:dgCol>
   <z:dgCol title="返回电子面单模板：0-不需要；1-需要" field="isreturnprinttemplate" ></z:dgCol>
   <z:dgCol title="订单数据json" field="order" ></z:dgCol>
   <z:dgCol title="是否成功" field="success" ></z:dgCol>
   <z:dgCol title="错误编码" field="resultcode" ></z:dgCol>
   <z:dgCol title="错误原因" field="reason" ></z:dgCol>
   <z:dgCol title="唯一标识" field="uniquerrequestnumber" ></z:dgCol>
   <z:dgCol title="面单打印模板" field="printtemplate" ></z:dgCol>
   <z:dgCol title="订单预计到货时间yyyy-mm-dd" field="estimateddeliverytime" formatter="yyyy-MM-dd hh:mm:ss"></z:dgCol>
   <z:dgCol title="子单数量" field="subcount" ></z:dgCol>
   <z:dgCol title="子单号" field="suborders" ></z:dgCol>
   <z:dgCol title="子单模板" field="subprinttemplates" ></z:dgCol>
   <z:dgCol title="收件人安全电话" field="receiversafephone" ></z:dgCol>
   <z:dgCol title="寄件人安全电话" field="sendersafephone" ></z:dgCol>
   <z:dgCol title="拨号页面网址（转换成二维码可扫描拨号）" field="dialpage" ></z:dgCol>
   <z:dgCol title="发送信息json" field="senddata" ></z:dgCol>
   <z:dgCol title="接收信息json" field="recvdata" ></z:dgCol>
   <z:dgCol title="操作" field="opt" width="100"></z:dgCol>
   <z:dgDelOpt title="删除" url="expressController.do?del&id={id}" />
   <z:dgToolBar title="录入" icon="icon-add" url="expressController.do?addorupdate" funname="add"></z:dgToolBar>
   <z:dgToolBar title="编辑" icon="icon-edit" url="expressController.do?addorupdate" funname="update"></z:dgToolBar>
   <z:dgToolBar title="查看" icon="icon-search" url="expressController.do?addorupdate" funname="detail"></z:dgToolBar>
  </z:datagrid>
  </div>
 </div>