<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>产品单位</title>
  <z:base type="jquery,easyui,tools,DatePicker"></z:base>
 </head>
 <body style="overflow-y:" scroll="no">
  <z:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="productUnitController.do?save">
		<input id="id" name="id" type="hidden" value="${productUnitPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">单位值:</label>
		      <input class="inputxt" id="code" name="code" 
					   value="${productUnitPage.code}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">单位名称:</label>
		      <input class="inputxt" id="name" name="name" 
					   value="${productUnitPage.name}" datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">说明:</label>
		      <input class="inputxt" id="remark" name="remark" ignore="ignore"
					   value="${productUnitPage.remark}">
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </z:formvalid>
 </body>