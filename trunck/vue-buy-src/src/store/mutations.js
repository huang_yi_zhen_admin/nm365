export function SET_ACTIVE_TYPE(state, { type }) {
    state.activeType = type
}
export function SET_STATE_LIST_DATA(state, { data, listName }) {
    if (data && (data.totalPage || data.total)) {
        state[listName]['totalPage'] = data['totalPage'] || Math.ceil(data['total'] / state[listName]['pageSize']);
        state[listName]['pageNo']++;
        state[listName]['data'] = state[listName]['data'].concat(data.rows);
    } else {
        state[listName]['data'] = data;
    }
}
export function SET_STATE_PROVINCE_DATA(state, { data }) {
    state['proviceData'] = data;
}
export function SET_STATE_LIST_EMPTY(state, { listName }) {
    state[listName]['pageNo'] = 1;
    state[listName]['data'] = [];
    state[listName]['totalPage'] = 0;
}
export function SET_STATE_PAGE_EMPTY(state, { pageName }) {
    for (let key in state) {
        let item = state[key];
        if (item['page'] == pageName) {
            SET_STATE_LIST_EMPTY(state, { listName: key });
            ClEAN_SEARCH_DATA(state, { listName: key });
        }
    }
}
export function SET_BUY_GOODSID(state, pid) {
    state['buyGoodsId'] = pid;
}
export function SET_BUY_INFO(state, { data }) {
    let id = data.pid;
    if (typeof(state['buyData'][id]) == "undefined") {
        state['buyData'][id] = {};
    }
    state['buyData'][id]['uid'] = data.uid;
    state['buyData'][id]['num'] = data.num;

}

export function SET_STATE_WEBSITE(state, { data }) {
    state['webSite'] = data;

}
export function SET_SEARCH_DATA(state, { listName, args, info, init }) {
    state['searchData'][listName] = {
        args,
        info,
        init
    }
}
export function SET_USER_POWER(state, { data = [] }) {
    let json = {},
        power = state['power'];

    data.forEach(item => {
        json[item] = 1;
    });
    for (var key in power) {
        if (typeof(json[power[key]]) != "undefined") {
            power[key] = 1;
        } else {
            power[key] = 0;
        }
    }
    state['power'] = power;

}
export function ClEAN_SEARCH_DATA(state, { listName }) {
    state['searchData'][listName] = {
        args: {},
        info: [],
        init: false
    }
}
export function UPDATE_LIST_DATA_BY_ID(state, { id, data, listName }) {
    let arr = state[listName]['data'],
        ok = false;
    if (arr) {
        arr.forEach(item => {
            if (item.id == id) {
                for (var key in data) {
                    item[key] = data[key];
                    ok = true;
                }
            }
        });

        if (ok) {
            state[listName]['data'] = arr;
        }
    }
}